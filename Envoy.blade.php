@servers(['production' => 'my.imsreport.com', 'staging' => 'staging.imsreport.com', 'website' => 'imsreport.com', 'drl' => 'my.drtvlab.com'])

@task('clear-cache', ['on' => 'production'])
    cd /home/forge/my.imsreport.com/current
    php artisan cache:clear --env=production
    php artisan varnish:flush --env=production
@endtask

@task('price-grabber', ['on' => 'production'])
    cd /var/www/myimsreport/code
    sudo su myimsreport
    php artisan p2s:pricegrabber --env=production
@endtask

@task('generate-spot-rankings', ['on' => 'production'])
    cd /home/forge/my.imsreport.com/current
    php artisan ims:tallyspotdetections "2020-05-15 16:00:00" "2020-05-08 16:00:00" --language=1 --env=production
@endtask

@task('generate-spot-spanish', ['on' => 'production'])
    cd /home/forge/my.imsreport.com/current
    php artisan ims:tallyspotdetections "2020-05-15 16:00:00" "2020-05-08 16:00:00" --language=2 --env=production
@endtask

@task('generate-show-rankings', ['on' => 'production'])
    cd /home/forge/my.imsreport.com/current
    php artisan ims:tallyshowdetections "2020-05-15 16:00:00" "2020-05-08 16:00:00" --env=production
@endtask
