<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('auto-boss', 'Admin\Autodetection\StatsController@getIndex');

Route::get('/', [function () {
    return Redirect::to('dashboard');
}]);

Route::get('upgrade', [ 'as' => 'upgrade', function () {
    return view('upgrade');
}]);

/*
|--------------------------------------------------------------------------
| Login/Logout/Register Routes
|--------------------------------------------------------------------------
|
|
*/

Route::get('user/login', [
    'as'   => 'user.login',
    'uses' => 'UserController@getLogin',
]);

Route::post('user/login', 'UserController@postLogin');

Route::get('user/logout', [
    'as'   => 'user.logout',
    'uses' => 'UserController@getLogout'
]);

Route::get('user/register', [
    'as'   => 'user.register',
    'uses' => 'UserController@getRegister',
]);
Route::get('user/register/winmo', [
    'as'   => 'user.register.winmo',
    'uses' => 'UserController@getRegisterWinmoCTA',
]);
Route::post('user/register', [
    'as' =>'user.registeruser',
    'uses' => 'UserController@postRegister',
]);
Route::post('user/register/ajax', [
    'as'   => 'user.register.ajax',
    'uses' => 'UserController@postRegisterAjax'
]);
Route::get('user/{id}/activate/{code}', 'UserController@activate')->where('id', '[0-9]+');

// Register via the Live Shopping Tracker onboarding
Route::get('user/register/lst', [
    'as'   => 'user.register.lst',
    'uses' => 'UserController@getRegisterLst',
]);

Route::get('user/reset', [
    'as'   => 'user.reset',
    'uses' => 'UserController@getReset',
]);
Route::post('user/reset', 'UserController@postReset');

Route::get('user/reset_finish', [
    'as'   => 'user.reset.finish',
    'uses' => 'UserController@getResetFinish'
]);
Route::post('user/reset_complete', [
    'as'   => 'user.reset.complete',
    'uses' => 'UserController@postResetComplete'
]);

Route::get('winmo', 'WinmoController@index')->name('winmo.embed');
Route::get('product/{product}', 'WinmoController@show')->name('product.embed');

/*
|--------------------------------------------------------------------------
| OAuth Routes
|--------------------------------------------------------------------------
|
|
*/
// Route::get('oauth/authorize', [
//     'before' => 'check-authorization-params|auth',
//     'uses' => 'Oauth\OAuthController@getAuthorize'
// ]);
// Route::post('oauth/authorize', [
//     'before' => 'check-authorization-params|auth|csrf',
//     'uses' => 'Oauth\OAuthController@postAuthorize'
// ]);
// Route::post('oauth/access_token', 'Oauth\OAuthController@postAccessToken');
// Route::get('oauth/secure', ['before' => 'oauth', function () {
//     return view('oauth.secure');
// }]);

// Route::get('oauth/user-details', ['before' => 'oauth', 'uses' => 'Oauth\OAuthController@getUserDetails']);
// Route::get('oauth/user-details/fixed', ['uses' => 'Oauth\OAuthController@getUserDetailsHardcoded']);

/*
|--------------------------------------------------------------------------
| User Secured Routes
|--------------------------------------------------------------------------
|
|
*/
Route::group([
    'middleware' => ['tracker', 'sentry', 'hasAccess', 'cacheable'],
], function () {

    Route::get('dashboard', [
        'as' => 'dashboard',
        'uses' => 'DashboardController@getIndex'
    ]);

    /* Start User Settings */
    Route::get('user/settings', [
        'as' => 'user.settings',
        'uses' => 'UserController@getSettings'
    ]);
    Route::post('user/settings', 'UserController@postSettings');
    /* End User Settings */

    /* Start User Alerts */
    Route::get('user/alerts', [
        'as' => 'user.alerts',
        'uses' => 'AlertController@getIndex'
    ]);
    Route::post('user/alerts', 'AlertController@postAlerts');
    Route::post('user/alerts/email', [
        'as' => 'user.alerts.email',
        'uses' => 'AlertController@postRankings'
    ]);
    Route::delete('user/alerts/destroy/{id}', [
        'as' => 'alerts.destroy',
        'uses' => 'AlertController@deleteDestroy'
    ]);
    /* End user alerts */

    /* Start User Billing */
    Route::group(['prefix' => 'user/billing'], function () {
        Route::get('/', [
            'as' => 'user.billing',
            'uses' => 'BillingController@getIndex'
        ]);
        Route::get('lst', [
            'as' => 'user.billing.lst',
            'uses' => 'BillingController@getLst'
        ]);
    });
    /* End User Billing*/

    /* Start Reports */
    Route::get('reports', [
        'as' => 'reports',
        'uses' => 'ReportController@index'
    ]);
    Route::get('reports/show/{report}', [
        'as' => 'reports.show',
        'uses' => 'ReportController@getShow'
    ]);
    /* End Reports */

    /* Category Report */
    Route::group(['prefix' => 'categories'], function () {
        Route::get('short-form', [
            'as' => 'reports.category.short',
            'uses' => 'CategoryController@shortForm'
        ]);

        Route::get('long-form', [
            'as' => 'reports.category.long',
            'uses' => 'CategoryController@longForm'
        ]);
    });
    /* End Category Report */

    /* Start Category Reports */
    Route::get('category-reports', [
        'as' => 'category-reports',
        'uses' => 'CategoryReportController@index'
    ]);
    Route::get('category-reports/show/{report}', [
        'as' => 'category-reports.show',
        'uses' => 'CategoryReportController@getShow'
    ]);
    Route::get('category-reports/purchase/{report}', [
        'as' => 'category-reports.purchase',
        'uses' => 'CategoryReportController@getPurchase'
    ]);
    Route::get('category-reports/products/{report}', [
        'as' => 'category-reports.products',
        'uses' => 'CategoryReportController@getProducts'
    ]);
    Route::get('category-reports/program-pivot/{report}', [
        'as' => 'category-reports.program-pivot',
        'uses' => 'CategoryReportController@getProgramPivot'
    ]);
    Route::get('category-reports/program-detail/{report}', [
        'as' => 'category-reports.program-detail',
        'uses' => 'CategoryReportController@getProgramDetail'
    ]);
    Route::get('category-reports/program-detail-full/{report}', [
        'as' => 'category-reports.program-detail-full',
        'uses' => 'CategoryReportController@getProgramDetailFull'
    ]);
    Route::get('category-reports/spot-pivot/{report}', [
        'as' => 'category-reports.spot-pivot',
        'uses' => 'CategoryReportController@getSpotPivot'
    ]);
    Route::get('category-reports/spot-detail/{report}', [
        'as' => 'category-reports.spot-detail',
        'uses' => 'CategoryReportController@getSpotDetail'
    ]);
    Route::get('category-reports/spot-detail-full/{report}', [
        'as' => 'category-reports.spot-detail-full',
        'uses' => 'CategoryReportController@getSpotDetailFull'
    ]);
    Route::get('category-reports/network-detail/{report}', [
        'as' => 'category-reports.network-detail',
        'uses' => 'CategoryReportController@getNetworkDetail'
    ]);
    Route::get('category-reports/program/{report}/{program}', [
        'as' => 'category-reports.program',
        'uses' => 'CategoryReportController@getProgram'
    ])->where(['report' => '[0-9]+', 'program' => '[0-9]+']);
    Route::get('category-reports/spot/{report}/{spot}', [
        'as' => 'category-reports.spot',
        'uses' => 'CategoryReportController@getSpot'
    ])->where(['report' => '[0-9]+', 'spot' => '[0-9]+']);
    Route::get('category-reports/spots-data/{report}', [
        'as' => 'category-reports.spots-data',
        'uses'=>'CategoryReportController@getSpotsData'
    ]);
    Route::get('category-reports/programs-data/{report}', [
        'as' => 'category-reports.programs-data',
        'uses'=>'CategoryReportController@getProgramsData'
    ]);
    /* End Category Reports */

    /* Start Spots */
    Route::get('spots', [
        'as' => 'spots.all',
        'uses' => 'SpotController@getIndex'
    ]);
    Route::get('spots/new', [
        'as' => 'spots.new',
        'uses' => 'SpotController@getNew'
    ]);
    Route::get('spots/data', [
        'as' => 'spots.data',
        'uses' => 'SpotController@getData'
    ]);
    Route::get('spots/parent/{show}', [
        'as' => 'spots.parent',
        'uses' => 'SpotController@getParentSpot'
    ]);
    Route::get('spots/schedule/{show}', [
        'as' => 'spots.schedule',
        'uses' => 'SpotController@getSchedule'
    ]);
    /* End Spots */

    /* Start Programs */
    Route::get('programs', [
        'as' => 'programs.all',
        'uses' => 'ProgramController@getIndex'
    ]);
    Route::get('programs/new', [
        'as' => 'programs.new',
        'uses' => 'ProgramController@getNew'
    ]);
    Route::get('programs/data', [
        'as' => 'programs.data',
        'uses' => 'ProgramController@getData'
    ]);
    Route::get('programs/parent/{program}', [
        'as' => 'programs.parent',
        'uses' => 'ProgramController@getParentProgram'
    ]);
    Route::get('programs/schedule/{program}', [
        'as' => 'programs.schedule',
        'uses' => 'ProgramController@getSchedule'
    ]);
    /* End Programs */

    /* Start Networks */
    Route::get('networks', [
        'as' => 'networks.all',
        'uses' => 'NetworkController@getIndex'
    ]);
    Route::get('networks/show/{network}', [
        'as' => 'networks.show',
        'uses' => 'NetworkController@getNetwork'
    ]);
    Route::get('networks/breakdown', [
        'as' => 'networks.breakdown',
        'uses' => 'NetworkBreakdownController@getBreakdown'
    ]);
    /* End Networks */

    /* Start Companies */
    Route::get('companies', [
        'as' => 'companies.all',
        'uses' => 'CompanyController@getIndex'
    ]);
    Route::get('companies/show/{company}', [
        'as' => 'companies.show',
        'uses' => 'CompanyController@getShow'
    ]);
    Route::get('companies/data', [
        'as' => 'companies.data',
        'uses' => 'CompanyController@getData'
    ]);
    /* End Companies */

    /* Start Agencies */
    Route::get('agencies', [
        'as' => 'agencies.all',
        'uses' => 'CompanyController@getIndex'
    ]);
    Route::get('agencies/show/{company}', [
        'as' => 'agencies.show',
        'uses' => 'CompanyController@getShow'
    ]);
    /* End Agencies */

    /* Grids */
    Route::get('grids', [
        'as' => 'grids.show',
        'uses' => 'GridController@getGrid'
    ]);
    /* End Grid */

    /* Start Support */

    Route::get('support', [
        'as' => 'support',
        'uses' => 'SupportController@index'
    ]);
    /* End Support */

    /* Start Products */
    Route::group([
        'prefix'=>'products',
        'hasAccess' => 'reports.products',
    ], function () {
        Route::get('/', [
            'as' => 'products',
            'uses' => 'ProductController@getIndex'
        ]);
        Route::get('/data', [
            'as' => 'products.data',
            'uses' => 'ProductController@getDatatables'
        ]);
        Route::get('/{slug}', [
            'as' => 'products.show',
            'uses' => 'ProductController@getProduct'
        ]);
    });

    /* Start Retailers */
    Route::group([
        'prefix'=>'retailers',
        'hasAccess' => 'reports.retailers',
    ], function () {
        Route::get('/', [
            'as' => 'retailers',
            'uses' => 'RetailerController@index'
        ]);
        Route::get('{retailer}', [
            'as' => 'retailers.show',
            'uses' => 'RetailerController@getRetailer'
        ]);
        Route::get('{retailer}/product/{product}', [
            'as' => 'retailers.product',
            'uses' => 'RetailerController@getProduct'
        ]);
        Route::get('categories/all', [
            'as' => 'retailers.categories',
            'uses' => 'RetailerController@getCategories'
        ]);
        Route::get('category/{categoryId}', [
            'as' => 'retailers.category',
            'uses' => 'RetailerController@getCategory'
        ]);
    });
    /* End Retailers */

    /* Retail Report Monthly */
    Route::group([
        'prefix'=>'retail-report/monthly',
        'hasAccess' => 'reports.retailers',
    ], function () {
        /* Dashboard */
        Route::get('/', [
            'as' => 'retail-report.monthly',
            'uses' => 'RetailReport\Monthly\RetailMonthlyController@getIndex'
        ]);
        /* Retailer Listing */
        Route::get('retailers', [
            'as' => 'retail-report.monthly.retailers',
            'uses' => 'RetailReport\Monthly\RetailerController@index'
        ]);
        /* Retailer Details */
        Route::get('retailers/{retailer}', [
            'as' => 'retail-report.monthly.retailers.show',
            'uses' => 'RetailReport\Monthly\RetailerController@getRetailer',
        ]);
        /* Retailer Categories */
        Route::get('retailers/categories/all', [
            'as' => 'retail-report.monthly.retailers.categories',
            'uses' => 'RetailReport\Monthly\RetailerController@getCategories'
        ]);
        /* Retailer Category Detail */
        Route::get('retailers/category/{categoryId}', [
            'as' => 'retail-report.monthly.retailers.category',
            'uses' => 'RetailReport\Monthly\RetailerController@getCategory'
        ]);

        Route::get('products', [
            'as' => 'retail-report.monthly.products',
            'uses' => 'RetailReport\Monthly\ProductController@getIndex',
        ]);
        Route::get('products/data', [
            'as' => 'retail-report.monthly.products.data',
            'uses' => 'RetailReport\Monthly\ProductController@getDatatables'
        ]);
        Route::get('products/{slug}', [
            'as' => 'retail-report.monthly.products.show',
            'uses' => 'RetailReport\Monthly\ProductController@getProduct'
        ]);
    });

    /* Retail Report Quarterly */
    Route::group(['prefix'=>'retail-report/quarterly'], function () {
        Route::get('/', [
            'as' => 'retail-report.quarterly',
            'uses' => 'RetailQuarterlyController@getIndex'
        ]);
    });
    /* End Retail Reports */

    /* Start Sales Optimizer */
    Route::group([
        'prefix' => 'sales-optimizer',
        'hasAccess' => 'reports.sales-optimizer',
    ], function () {
        Route::get('/', [
            'as' => 'sales-optimizer.home',
            'uses' => 'SalesOptimizerController@getIndex'
        ]);
        Route::get('/comparison', [
            'as' => 'sales-optimizer.comparison',
            'uses' => 'SalesOptimizerController@getComparison'
        ]);
        Route::get('/comparison/products', [
            'as' => 'sales-optimizer.products',
            'uses' => 'SalesOptimizerController@getProducts'
        ]);
    });
    /* End Sales Optimizer */

    /* Start Attribution Tracker */
    Route::group([
        'prefix' => 'attribution-tracker',
        'hasAccess' => 'reports.attribution-tracker',
    ], function () {
        Route::get('/', [
            'as' => 'attribution-tracker.index',
            'uses' => 'AttributionTrackerController@getIndex'
        ]);
        Route::get('/call', [
            'as' => 'attribution-tracker.call',
            'uses' => 'AttributionTrackerController@getCall'
        ]);
        Route::get('/web', [
            'as' => 'attribution-tracker.web',
            'uses' => 'AttributionTrackerController@getWeb'
        ]);
    });
    /* End Attribution Tracker */

    /* Start Network Analyzer */
    Route::group([
        'prefix'=>'network-analyzer',
        'hasAccess' => 'reports.network-report',
    ], function () {
        // by airings
        Route::get('/', [
            'as'     => 'network-analyzer.home',
            'uses'   => 'NetworkAnalyzerController@byAiringsShortForm'
        ]);
        Route::get('short-form', [
            'as'     => 'network-analyzer.short-form',
            'uses'   => 'NetworkAnalyzerController@byAiringsShortForm'
        ]);
        Route::get('long-form', [
            'as'     => 'network-analyzer.long-form',
            'uses'   => 'NetworkAnalyzerController@byAiringsLongForm'
        ]);
        // by category
        Route::group(['prefix'=>'categories'], function () {
            Route::get('/', [
                'as'   => 'network-analyzer.categories',
                'uses' => 'NetworkAnalyzerController@byCategoryShortForm'
            ]);
            Route::get('short-form', [
                'as'   => 'network-analyzer.categories.short-form',
                'uses' => 'NetworkAnalyzerController@byCategoryShortForm'
            ]);
            Route::get('long-form', [
                'as'   => 'network-analyzer.categories.long-form',
                'uses' => 'NetworkAnalyzerController@byCategoryLongForm'
            ]);
            Route::get('{id}', [
                'as'   => 'network-anaylizer.categories.show',
                'uses' => 'NetworkAnalyzerController@getCategoryShow'
            ]);
        });
        Route::group(['prefix'=>'products'], function () {
            Route::get('/', [
                'as'   => 'network-analyzer.products',
                'uses' => 'NetworkAnalyzerController@byProductsShortForm'
            ]);
            Route::get('short-form', [
                'as'   => 'network-analyzer.products.short-form',
                'uses' => 'NetworkAnalyzerController@byProductsShortForm'
            ]);
            Route::get('long-form', [
                'as'   => 'network-analyzer.products.long-form',
                'uses' => 'NetworkAnalyzerController@byProductsLongForm'
            ]);
        });
        Route::get('commercials', [
            'as'   => 'network-analyzer.categories-commercials',
            'uses' => 'NetworkAnalyzerController@getCommercialsByCategories'
        ]);
        Route::get('{id}', [
            'as'   => 'network-analyzer.show',
            'uses' => 'NetworkAnalyzerController@getNetworkShow'
        ]);
    });
    /* End Network Analyzer */

    /* Start LST */
    Route::group([
        'prefix' => 'lst',
        'hasAccess' => 'reports.lst',
    ], function () {
        Route::get('/', [
            'as' => 'lst.index',
            'uses' => 'LiveShoppingController@getIndex'
        ]);

        Route::get('categories', [
            'as' => 'lst.categories',
            'uses' => 'LiveShoppingController@getCategories'
        ]);

        Route::get('category/{categoryId}', [
            'as' => 'lst.category',
            'uses' => 'LiveShoppingController@getCategory'
        ]);

        Route::get('sub-category/{categoryId}', [
            'as' => 'lst.sub-category',
            'uses' => 'LiveShoppingController@getSubCategory'
        ]);

        Route::get('products', [
            'as' => 'lst.products',
            'uses' => 'LiveShoppingController@getProducts'
        ]);

        Route::get('product/{productId}', [
            'as' => 'lst.product',
            'uses' => 'LiveShoppingController@getProduct'
        ]);

        Route::get('show/{showId}', [
            'as' => 'lst.show',
            'uses' => 'LiveShoppingController@getShow'
        ]);

        Route::get('airtime/{airtimeId}', [
            'as' => 'lst.airtime',
            'uses' => 'LiveShoppingController@getAirtime'
        ]);

        Route::get('search', [
            'as' => 'lst.search',
            'uses' => 'LiveShoppingController@getSearch'
        ]);

        Route::get('datatables', [
            'as' => 'lst.datatables',
            'uses' => 'LiveShoppingController@getDatatables'
        ]);
    });
    /* End LST */
});

Route::group([
    'middleware' => ['tracker', 'sentry', 'hasAccess', 'cacheable:15'],
], function () {

    Route::get('spots/show/{show}', [
        'as' => 'spots.show',
        'uses' => 'SpotController@getSpot'
    ]);
    Route::get('programs/show/{program}', [
        'as' => 'programs.show',
        'uses' => 'ProgramController@getProgram'
    ]);
});

/* Admin Accessible API */
Route::group(['prefix'=>'api','middleware' => ['sentry', 'hasAccess']], function () {
    Route::get('categories/dropdown', 'Api\CategoryController@getCategories');
    Route::get('categories/search', 'Api\CategoryController@searchCategories');
    Route::get('categories/one', 'Api\CategoryController@getCategory');
    Route::get('lst/categories/dropdown', 'Api\Lst\CategoryController@getCategories');
    Route::get('lst/shows/search', 'Api\Lst\ShowController@getShows');
    Route::get('lst/shows/one', 'Api\Lst\ShowController@getShow');
    Route::post('lst/shows/add', 'Api\Lst\ShowController@add');
    Route::post('lst/shows/update/{showId}', 'Api\Lst\ShowController@update');
    Route::get('lst/products/search', 'Api\Lst\ProductController@getProducts');
    Route::get('lst/products/one', 'Api\Lst\ProductController@getProduct');
    Route::post('lst/products/add', 'Api\Lst\ProductController@add');
    Route::post('lst/products/update/{productId}', 'Api\Lst\ProductController@update');
    Route::get('lst/product-versions/dropdown', 'Api\Lst\ProductVersionController@getDropdown');
    Route::get('lst/product-versions/search', 'Api\Lst\ProductVersionController@getProducts');
    Route::get('lst/product-versions/one', 'Api\Lst\ProductVersionController@getVersion');
    Route::post('lst/product-versions/add', 'Api\Lst\ProductVersionController@add');
    Route::post('lst/product-versions/update/{versionId}', 'Api\Lst\ProductVersionController@update');
    Route::get('lst/airtime/get', 'Api\Lst\AirtimeController@get');
    Route::post('lst/airtime/add', 'Api\Lst\AirtimeController@add');
    Route::post('lst/airtime/update/{airtimeId}', 'Api\Lst\AirtimeController@update');
    Route::get('lst/airtime-products/get', 'Api\Lst\AirtimeProductController@get');
    Route::post('lst/airtime-products/add', 'Api\Lst\AirtimeProductController@add');
    Route::post('lst/airtime-products/delete/{airtimeId}', 'Api\Lst\AirtimeProductController@delete');
    Route::get('channels/search', 'Api\ChannelController@getChannels');
    Route::get('channels/dropdown', 'Api\ChannelController@getDropdown');
    Route::get('products/search', 'Api\ProductController@getProducts');
    Route::get('products/one', 'Api\ProductController@getProduct');
    Route::get('reports/months', 'ReportController@getMonths');
    Route::get('companies/search', 'CompanyController@getCompanies');
    Route::get('companies/one', 'CompanyController@getCompany');
    Route::get('programs/search', 'Api\ProgramController@getPrograms');
    Route::get('programs/one', 'Api\ProgramController@getProgram');
    Route::get('retailers/search', 'Api\RetailerController@getRetailers');
    Route::get('retailers/one', 'Api\RetailerController@getRetailer');
    Route::get('retailer-products/search', 'Api\RetailerProductController@getProducts');
    Route::get('retailer-products/one', 'Api\RetailerProductController@getProduct');
    Route::get('program-versions/search', 'Api\ProgramVersionController@getPrograms');
    Route::get('program-versions/one', 'Api\ProgramVersionController@getProgram');
    Route::get('spots/search', 'Api\SpotController@getSpots');
    Route::get('spots/one', 'Api\SpotController@getSpot');
    Route::get('spot-versions/search', 'Api\SpotVersionController@getSpots');
    Route::get('spot-versions/one', 'Api\SpotVersionController@getSpot');
    Route::get('keywords/search', 'Api\KeywordController@getKeywords');
});
//OAuth Client Listing
// Route::get('admin/oauth-clients', array(
//     'as' => 'admin.oauth-clients.index',
//     'uses' => 'Admin\OAuthClientController@index'
// ));
// Route::post('admin/oauth-clients', 'Admin\OAuthClientController@postCurator');


/* Publicly Accessible API */
Route::group(['prefix'=>'api'], function () {
    Route::get('dayparts', 'Api\DaypartsController@getDayparts');
    Route::get('dayparts/{daypartId}', 'Api\DaypartsController@getDaypart');
    Route::get('times/{date}', 'Admin\MonitorController@getTimes');
    Route::post('autodetect/updateAutoDetect/{type}', 'Api\AutoDetectController@updateAutoDetect');
    Route::post('autodetect/updateFilename/{type}', 'Api\AutoDetectController@updateFilename');
    Route::get('autodetect/mp4shows', 'Api\AutoDetectController@getMp4Shows');
    Route::get('autodetect/shows', 'Api\AutoDetectController@getShows');
    Route::post('autodetect/show', 'Api\AutoDetectController@postShow');
    Route::get('autodetect/mp4spots', 'Api\AutoDetectController@getMp4Spots');
    Route::get('autodetect/spots', 'Api\AutoDetectController@getSpots');
    Route::post('autodetect/spot', 'Api\AutoDetectController@postSpot');
    Route::get('autodetect/check', 'Api\AutoDetectController@getCheck');
    Route::get('thumbnail/check', 'Api\ChannelController@getCheckThumbnail');
    Route::post('autodetect/log', 'Api\AutoDetectController@postLog');
    Route::post('autodetect/exception', 'Api\AutoDetectController@postException');

    //TODO Secure these URLs
    Route::get('spot-versions/find/{spotVersionId}', 'Api\SpotVersionController@getSpotFind');
    Route::get('program-versions/find/{programVersionId}', 'Api\ProgramVersionController@getProgramFind');
});

/*
|--------------------------------------------------------------------------
| Email Campaign Reports
|--------------------------------------------------------------------------
|
|
*/

Route::get('rankings', [ function () {
    // Get them to the upsell
    return Redirect::route('email-campaign.upsell');
}]);

Route::get('rankings/upsell', [
    'as'   => 'email-campaign.upsell',
    'uses' => 'ReportController@upsell'
]);

Route::get('rankings/{report}', [
    'as'   => 'email-campaign.show',
    'uses' => 'ReportController@showEmailCampaign'
]);



/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
|
*/

Route::get('admin', [
    'as'     => 'admin.home',
    'uses'   => 'Admin\DashboardController@index',
    'middleware' => ['sentry', 'hasAccess'],
]);

Route::group([
    'prefix' => 'admin',
    'middleware' => ['sentry', 'hasAccess'],
    'as' => 'admin.',
    'namespace' => 'Admin'
], function () {
    Route::resource('users', 'UsersController');
    Route::resource('dashboard-types', 'DashboardTypeController', ['except' => ['show']]);
    Route::resource('dayparts', 'DaypartController', ['except' => ['show']]);
    Route::resource('groups', 'GroupsController', ['except' => ['show']]);
    Route::resource('permissions', 'PermissionController', ['except' => ['show']]);
    Route::resource('categories', 'CategoryController', ['except' => ['show']]);
    Route::resource('channels', 'ChannelController', ['except' => ['show']]);
    Route::resource('companies', 'CompanyController', ['except' => ['show']]);
    Route::resource('languages', 'LanguageController', ['except' => ['show']]);
    Route::resource('monitor', 'MonitorController');
    Route::resource('airtimes', 'AirtimeController', ['except' => ['show']]);
    Route::post('airtime/store', 'AirtimeController@postStore');
    Route::post('airtime/delete', 'AirtimeController@postDelete');
    Route::get('airtime/show', 'AirtimeController@getShow');

    Route::get('airtimes/data', [
        'as' => 'airtimes.data',
        'uses'=>'AirtimeController@getData'
    ]);
    Route::post('airtimes/verify', [
        'as' => 'airtimes.verify',
        'uses'=>'AirtimeController@postVerify'
    ]);
    Route::get('airtimes/table', [
        'as' => 'airtimes.table',
        'uses'=>'AirtimeController@getTable'
    ]);
    Route::get('airtimes/autodetection', [
        'as' => 'airtimes.autodetection',
        'uses'=>'AirtimeController@getAutodetect'
    ]);
    Route::get('airtimes/autodetection-breakdown', [
        'as' => 'airtimes.autodetection-breakdown',
        'uses'=>'AirtimeController@getAutodetectBreakdown'
    ]);
    Route::get('airtimes/autodetection-program-breakdown/{programVersionId}', [
        'as' => 'airtimes.autodetection-program-breakdown',
        'uses'=>'AirtimeController@getAutodetectProgramBreakdown'
    ]);
    Route::resource('programs', 'ProgramController', ['except' => ['show']]);
    Route::resource('servers', 'ServerController', ['except' => ['show']]);
    Route::resource('spots', 'SpotController', ['except' => ['show']]);
    Route::get('spots/pdf', [
        'as' => 'spots.pdf',
        'uses' => 'SpotController@getPdf'
    ]);
    Route::get('spots/new', [
        'as' => 'spots.new',
        'uses' => 'SpotController@getNewSpots'
    ]);
    Route::get('spots/unique', [
        'as' => 'spots.unique',
        'uses' => 'SpotController@getUniqueDetections'
    ]);
    Route::get('spots/rankings', [
        'as' => 'spots.rankings',
        'uses' => 'SpotController@getRankings'
    ]);
    Route::get('spotcosts/channel/{channelId}', [
        'as' => 'spotcosts.channel',
        'uses' => 'SpotCostController@getChannel'
    ]);
    Route::get('spotcosts/data', [
        'as' => 'spotcosts.data',
        'uses'=>'SpotCostController@getData'
    ]);
    Route::post('spotcosts/add', [
        'as' => 'spotcosts.add',
        'uses'=>'SpotCostController@postAdd'
    ]);
    Route::post('spotcosts/delete', [
        'as' => 'spotcosts.delete',
        'uses'=>'SpotCostController@postDelete'
    ]);

    Route::resource('reports', 'ReportController', ['except' => ['show']]);

    Route::post('spotairtime/store', 'SpotAirtimeController@postStore');
    Route::post('spotairtime/delete', 'SpotAirtimeController@postDelete');
    Route::get('spotairtime/data', [
        'as' => 'spotairtime.data',
        'uses'=>'SpotAirtimeController@getData'
    ]);
    Route::get('spotairtime/table', [
        'as' => 'spotairtime.table',
        'uses'=>'SpotAirtimeController@getTable'
    ]);
    Route::get('spotairtime/stats', [
        'as' => 'spotairtime.stats',
        'uses' => 'SpotAirtimeController@getStats'
    ]);
    Route::get('spotairtime/stats/channel/{channelId}', [
        'as' => 'spotairtime.stats.channel',
        'uses' => 'SpotAirtimeController@getChannelStats'
    ]);
    Route::get('spotairtime/autodetection', [
        'as' => 'spotairtime.autodetection',
        'uses'=>'SpotAirtimeController@getAutodetect'
    ]);
    Route::get('spotairtime/autodetection-breakdown', [
        'as' => 'spotairtime.autodetection-breakdown',
        'uses'=>'SpotAirtimeController@getAutodetectBreakdown'
    ]);
    Route::get('spotairtime/autodetection-spot-breakdown/{spotVersionId}', [
        'as' => 'spotairtime.autodetection-spot-breakdown',
        'uses'=>'SpotAirtimeController@getAutodetectSpotBreakdown'
    ]);

    Route::get('autodetection/details', [
        'as' => 'autodetection.details',
        'uses'=>'Autodetection\StatsController@getDetails'
    ]);
    Route::get('autodetection/network', [
        'as' => 'autodetection.network',
        'uses'=>'Autodetection\StatsController@getNetwork'
    ]);
    Route::get('autodetection/network/{id}', [
        'as' => 'autodetection.network.details',
        'uses'=>'Autodetection\StatsController@getNetworkDetails'
    ]);

    Route::resource('category-reports', 'CategoryReportController', ['except' => ['show']]);

    Route::resource('products', 'ProductController', ['except' => ['show']]);
    Route::group(['prefix' => 'products'], function () {
        Route::get('{productId}/retailer-products', [
            'as' => 'products.retailer',
            'uses' => 'ProductController@getRetailerProducts'
        ]);
        Route::get('data', [
            'as' => 'products.data',
            'uses'=>'ProductController@getData'
        ]);
    });

    Route::resource('manufacturers', 'ManufacturerController', ['except' => ['show']]);
    Route::group(['prefix' => 'manufacturers'], function () {
        Route::get('data', [
            'as' => 'manufacturers.data',
            'uses'=>'ManufacturerController@getData'
        ]);
    });

    Route::resource('retailers', 'RetailerController', ['except' => ['show']]);
    Route::group(['prefix' => 'retailers'], function () {
        Route::get('data', [
            'as' => 'retailers.data',
            'uses'=>'RetailerController@getData'
        ]);
        Route::get('import', [
            'as' => 'retailers.import',
            'uses' => 'RetailerProductController@importRetailers'
        ]);
        Route::post('import', [
            'as' => 'retailers.processImport',
            'uses' => 'RetailerProductController@postImportRetailCsv'
        ]);
    });

    Route::resource('retailer-products', 'RetailerProductController', ['except' => ['show']]);
    Route::group(['prefix' => 'retailer-products'], function () {
        Route::get('delete/{retailerProductId}', [
            'as' => 'retailer-products.delete',
            'uses'=>'RetailerProductController@getDestroy'
        ]);
        Route::get('import', [
            'as' => 'retailer-products.import',
            'uses' => 'RetailerProductController@importCsv'
        ]);
        Route::post('import', [
            'as' => 'retailer-products.processImport',
            'uses' => 'RetailerProductController@postImportCsv'
        ]);
        Route::get('data', [
            'as' => 'retailer-products.data',
            'uses' =>'RetailerProductController@getData'
        ]);
    });

    Route::group(['prefix' => 'lst', 'as' => 'lst.'], function () {
        Route::resource('categories', 'LiveShoppingCategoryController', ['except' => ['show']]);
        Route::resource('guides', 'LiveShoppingGuideController', ['except' => ['show']]);
        Route::resource('shows', 'LiveShoppingShowController', ['except' => ['show']]);
        Route::resource('products', 'LiveShoppingProductController', ['except' => ['show']]);

        Route::get('product-versions/add', [
            'as' => 'product-versions.add',
            'uses' => 'LiveShoppingProductVersionController@add'
        ]);
        Route::get('product-versions/edit/{versionId}', [
            'as' => 'product-versions.edit',
            'uses' => 'LiveShoppingProductVersionController@edit'
        ]);
        Route::post('product-versions/store', [
            'as' => 'product-versions.store',
            'uses' => 'LiveShoppingProductVersionController@store'
        ]);
        Route::post('product-versions/update', [
            'as' => 'product-versions.update',
            'uses' => 'LiveShoppingProductVersionController@update'
        ]);
        Route::delete('product-versions/destroy/{productVersionId}', [
            'as' => 'product-versions.destroy',
            'uses' => 'LiveShoppingProductVersionController@destroy'
        ]);
        Route::get('monitor', [
            'as' => 'monitor.index',
            'uses' => 'LiveShoppingMonitorController@index'
        ]);
        Route::get('categories/datatables', [
            'as' => 'categories.datatables',
            'uses' => 'LiveShoppingCategoryController@getDatatables'
        ]);
        Route::get('products/datatables', [
            'as' => 'products.datatables',
            'uses' => 'LiveShoppingProductController@getDatatables'
        ]);
        Route::get('shows/datatables', [
            'as' => 'shows.datatables',
            'uses' => 'LiveShoppingShowController@getDatatables'
        ]);
        Route::get('shows/reconcile', [
            'as' => 'shows.reconcile',
            'uses' => 'LiveShoppingShowController@reconcile'
        ]);
        Route::delete('airtime/destroy/{airtimeId}', [
            'as' => 'airtime.destroy',
            'uses' => 'LiveShoppingShowController@destroyAirtime'
        ]);
    });

    Route::group(['prefix' => 'reporting'], function () {
        Route::get('alerts', [
            'as' => 'reporting.alerts',
            'uses' => 'ReportingController@getAlerts'
        ]);
    });

    Route::group([
        'prefix' => 'email-campaign',
        'middleware' => ['sentry', 'hasAccess', 'inGroup'],
        'hasAccess' => 'admin.view',
        'inGroup' => ['Admin IMS'],
    ], function () {
        Route::get('/', [
            'as'   => 'email-campaigns.index',
            'uses' => 'EmailCampaignController@index'
        ]);
        Route::get('create', [
            'as'   => 'email-campaigns.create',
            'uses' => 'EmailCampaignController@create'
        ]);
        //Test Route NICEE
        Route::get('generateWeeklyCampaign', 'EmailCampaignController@generateWeeklyCampaign');

        Route::post('create', [
            'as'   => 'email-campaigns.create',
            'uses' => 'EmailCampaignController@store'
        ]);
        Route::post('create/month', [
            'as'   => 'email-campaigns.create-month',
            'uses' => 'EmailCampaignController@generateMonthlyCampaign'
        ]);
        Route::post('create/week', [
            'as'   => 'email-campaigns.create-week',
            'uses' => 'EmailCampaignController@generateWeeklyCampaign'
        ]);
        Route::delete('{id}', [
            'as'   => 'email-campaigns.destroy',
            'uses' => 'EmailCampaignController@destroy'
        ]);
        Route::post('{id}/edit', [
            'as'   => 'email-campaigns.update',
            'uses' => 'EmailCampaignController@update'
        ]);
    });
});
/* End of Admin Group */

/* User Routes */

Route::put('admin/users/{users}/activate', [
    'as'     => 'admin.users.activate',
    'uses'   => 'Admin\UsersController@putStatus',
    'middleware' => ['sentry', 'hasAccess', 'inGroup'],
    'hasAccess' => 'users.update',
    'inGroup' => ['Admin IMS'],
]);

Route::put('admin/users/{users}/deactivate', [
    'as'     => 'admin.users.deactivate',
    'uses'   => 'Admin\UsersController@putStatus',
    'middleware' => ['sentry', 'hasAccess', 'inGroup'],
    'hasAccess' => 'users.update',
    'inGroup' => ['Admin IMS'],
]);

/* User Permissions Routes */

Route::get('admin/users/{users}/permissions', [
    'as'     => 'admin.users.permissions',
    'uses'   => 'Admin\UsersPermissionsController@index',
    'middleware' => ['sentry', 'hasAccess', 'inGroup'],
    'hasAccess' => 'users.update',
    'inGroup' => ['Admin IMS'],
]);

Route::put('admin/users/{users}/permissions', [
    'uses'   => 'Admin\UsersPermissionsController@update',
    'middleware' => ['sentry', 'hasAccess', 'inGroup'],
    'hasAccess' => 'users.update',
    'inGroup' => ['Admin IMS'],
]);

/* Group Permissions Routes */

Route::get('admin/groups/{groups}/permissions', [
    'as'     => 'admin.groups.permissions',
    'uses'   => 'Admin\GroupsPermissionsController@index',
    'middleware' => ['sentry', 'hasAccess', 'inGroup'],
    'hasAccess' => 'groups.update',
    'inGroup' => ['Admin IMS'],
]);

Route::put('admin/groups/{groups}/permissions', [
    'uses'   => 'Admin\GroupsPermissionsController@update',
    'middleware' => ['sentry', 'hasAccess', 'inGroup'],
    'hasAccess' => 'groups.update',
    'inGroup' => ['Admin IMS'],
]);

/* Group Profile Routes */

Route::get('admin/groups/{groups}/profile', [
    'as'     => 'admin.groups.profile',
    'uses'   => 'Admin\GroupsProfileController@index',
    'middleware' => ['sentry', 'hasAccess', 'inGroup'],
    'hasAccess' => 'groups.update',
    'inGroup' => ['Admin IMS'],
]);

Route::put('admin/groups/{groups}/profile', [
    'uses'   => 'Admin\GroupsProfileController@update',
    'middleware' => ['sentry', 'hasAccess', 'inGroup'],
    'hasAccess' => 'groups.update',
    'inGroup' => ['Admin IMS'],
]);


/* Throttling Routes */

Route::get('admin/users/{user}/throttling', [
    'as'     => 'admin.users.throttling',
    'uses'   => 'Admin\UsersThrottlingController@getStatus',
    'middleware' => ['sentry', 'hasAccess', 'inGroup'],
    'hasAccess' => 'users.view',
    'inGroup' => ['Admin IMS'],
]);

Route::put('admin/users/{user}/throttling/{action}', [
    'as'     => 'admin.users.throttling.update',
    'uses'   => 'Admin\UsersThrottlingController@putStatus',
    'middleware' => ['sentry', 'hasAccess', 'inGroup'],
    'hasAccess' => 'users.update',
    'inGroup' => ['Admin IMS'],
]);

/* Login, Logout, Register Routes */

Route::get('admin/login', [
    'as'   => 'admin.login',
    'uses' => 'Admin\DashboardController@getLogin',
]);

Route::post('admin/login', 'Admin\DashboardController@postLogin');

Route::get('admin/logout', [
    'as'   => 'admin.logout',
    'uses' => 'Admin\DashboardController@getLogout'
]);

/* Contact Page and Routes */

Route::post('contact', 'ContactController@postContact');
Route::post('update-page-description', 'ContactController@updatePageDescription');

/**
 * Billing Routes
 */
Route::group(['prefix' => 'billing'], function () {
    Route::post('plans', [
        'as'   => 'billing.subscribe',
        'uses' => 'BillingController@subscribeToPlan'
    ]);
    Route::post('plans\{id}', [
        'as'   => 'billing.renew',
        'uses' => 'BillingController@renewPlan'
    ]);
    Route::delete('plans\{id}', [
        'as'   => 'billing.unsubscribe',
        'uses' => 'BillingController@unsubscribeFromPlan'
    ]);
});
