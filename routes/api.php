<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::prefix('winmo')->group(function () {
    Route::resource('presence', 'Api\Winmo\PresenceController')->only('index', 'show');
    Route::get('network', 'Api\Winmo\NetworkController@index');
    Route::resource('predictor', 'Api\Winmo\PredictorController')->only('show');
});

