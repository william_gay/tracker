<?php

Breadcrumbs::for('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('dashboard'));
});

Breadcrumbs::for('upgrade', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Upgrade');
});

Breadcrumbs::for('reports', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Reports', route('reports'));
});

Breadcrumbs::for('report', function ($breadcrumbs, $report) {
    $breadcrumbs->parent('reports');
    $breadcrumbs->push($report->name, route('reports.show', $report->id));
});

Breadcrumbs::for('spots', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Spots', route('spots.all'));
});

Breadcrumbs::for('spot', function ($breadcrumbs, $spot) {
    $breadcrumbs->parent('spots');
    $breadcrumbs->push($spot->title, route('spots.show', $spot->id));
});

Breadcrumbs::for('programs', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Shows', route('programs.all'));
});

Breadcrumbs::for('program', function ($breadcrumbs, $program) {
    $breadcrumbs->parent('programs');
    $breadcrumbs->push($program->title, route('programs.show', $program->id));
});

Breadcrumbs::for('networks', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Networks', route('networks.all'));
});

Breadcrumbs::for('network', function ($breadcrumbs, $network) {
    $breadcrumbs->parent('networks');
    $breadcrumbs->push($network->name, route('networks.show', $network->id));
});

Breadcrumbs::for('network-breakdown', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Network Breakdown', route('networks.breakdown'));
});


Breadcrumbs::for('companies', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Companies', route('companies.all'));
});

Breadcrumbs::for('company', function ($breadcrumbs, $company) {
    $breadcrumbs->parent('companies');
    $breadcrumbs->push($company->name, route('companies.show', $company->id));
});

Breadcrumbs::for('agencies', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Agencies', route('agencies.all'));
});

Breadcrumbs::for('agency', function ($breadcrumbs, $agency) {
    $breadcrumbs->parent('agencies');
    $breadcrumbs->push($agency->name, route('agencies.show', $company->id));
});

Breadcrumbs::for('category-reports', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Category Reports', route('category-reports'));
});

Breadcrumbs::for('category-report', function ($breadcrumbs, $report) {
    $breadcrumbs->parent('category-reports');
    $breadcrumbs->push($report->name, route('category-reports.show', $report->id));
});

Breadcrumbs::for('category-report-products', function ($breadcrumbs, $report) {
    $breadcrumbs->parent('category-report', $report);
    $breadcrumbs->push('Products', route('category-reports.products', $report->id));
});

Breadcrumbs::for('category-report-program', function ($breadcrumbs, $report, $program) {
    $breadcrumbs->parent('category-report-products', $report);
    $breadcrumbs->push($program->title, route('category-reports.program', [$report->id, $program->id]));
});

Breadcrumbs::for('category-report-spot', function ($breadcrumbs, $report, $spot) {
    $breadcrumbs->parent('category-report-products', $report);
    $breadcrumbs->push($spot->title, route('category-reports.program', [$report->id, $spot->id]));
});

Breadcrumbs::for('category-report-program-detail', function ($breadcrumbs, $report) {
    $breadcrumbs->parent('category-report', $report);
    $breadcrumbs->push('Programs Detail', route('category-reports.program-detail', $report->id));
});
Breadcrumbs::for('category-report-spot-detail', function ($breadcrumbs, $report) {
    $breadcrumbs->parent('category-report', $report);
    $breadcrumbs->push('Spots Detail', route('category-reports.spot-detail', $report->id));
});
Breadcrumbs::for('category-report-network-detail', function ($breadcrumbs, $report) {
    $breadcrumbs->parent('category-report', $report);
    $breadcrumbs->push('Network Detail', route('category-reports.network-detail', $report->id));
});

Breadcrumbs::for('retailers', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Retailers', route('retailers'));
});

Breadcrumbs::for('retailer', function ($breadcrumbs, $retailer) {
    $breadcrumbs->parent('retailers');
    $breadcrumbs->push($retailer->name, route('retailers.show', $retailer->id));
});

Breadcrumbs::for('retailer-categories', function ($breadcrumbs) {
    $breadcrumbs->parent('retailers');
    $breadcrumbs->push('Categories', route('retailers.categories'));
});

Breadcrumbs::for('retailer-category', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('retailer-categories');
    $breadcrumbs->push($category->name, route('retailers.category', $category->id));
});

Breadcrumbs::for('products', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Products', route('products'));
});

Breadcrumbs::for('product', function ($breadcrumbs, $product) {
    $breadcrumbs->parent('products');
    $breadcrumbs->push($product->name, route('products.show', $product->id));
});

Breadcrumbs::for('sales-optimizer', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Sales Optimizer', route('sales-optimizer.home'));
});
