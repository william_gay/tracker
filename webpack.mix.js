const mix = require('laravel-mix');
require('dotenv').config();

require('laravel-mix-tailwind');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

// Copy over the swfs
mix.copy('resources/swf', 'public/assets/swf');

// Copy over the fonts
mix.copy([
  'resources/fonts',
  'resources/vendor/video-js/font'
], 'public/assets/fonts/');
mix.copy([
  'resources/fonts',
  'resources/vendor/video-js/font'
], 'public/assets/fonts/');
mix.copy([
  'resources/img/select2'
], 'public/assets/css/');
mix.copy([
  'resources/admin-js/admin.monitor.doit-long.js',
  'resources/admin-js/admin.monitor.doit-short.js',
  'resources/admin-js/admin.spotcosts.js'
], 'public/assets/js/');

mix.styles([
  'resources/css/font-icons/entypo/css/entypo.css',
  'resources/admin-css/*.css',
  'resources/vendor/codemirror/codemirror.css'
], 'public/assets/css/admin.css');

mix.styles([
  'resources/vendor/rickshaw/rickshaw.min.css',
  'resources/vendor/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css',
  'resources/vendor/select2/select2-bootstrap.css',
  'resources/vendor/select2-3.5.4/select2.css',
  'resources/vendor/video-js/video-js.css',
  'resources/vendor/daterangepicker/daterangepicker-bs3.css',
  'resources/shared-css/bootstrap-datetimepicker.min.css'
], 'public/assets/css/shared.css');

mix.styles([
  'resources/css/font-icons/entypo/css/entypo.css',
  'resources/css/core.css',
  'resources/css/theme.css',
  'resources/css/forms.css',
  'resources/css/custom.css',
  'resources/css/skins/white.css',
  'resources/vendor/bootstrap-multiselect-0.9.13/dist/css/bootstrap-multiselect.css'
], 'public/assets/css/style.css');

mix.scripts([
  'resources/shared-js/jquery.slugify.js',
  'resources/admin-js/admin.js',
  'resources/vendor/codemirror/*.js',
  'resources/js/jquery.validate.min.js',
  '!resources/admin-js/admin.monitor.*'
], 'public/assets/js/admin.js');

mix.scripts([
  'resources/js/moment.js',
  'resources/shared-js/spin-js/*.js',
  'resources/shared-js/jquery.browser.js',
  'resources/shared-js/bootbox.min.js',
  'resources/vendor/select2-3.5.4/select2.js',
  'resources/vendor/amcharts/amcharts.js',
  'resources/vendor/amcharts/pie.js',
  'resources/vendor/amcharts/funnel.js',
  'resources/vendor/amcharts/gauge.js',
  'resources/vendor/amcharts/radar.js',
  'resources/vendor/amcharts/serial.js',
  'resources/vendor/amcharts/xy.js',
  'resources/vendor/amcharts/themes/light.js',
  'resources/vendor/amcharts/exporting/amexport.js',
  'resources/vendor/amcharts/exporting/canvg.js',
  'resources/vendor/amcharts/exporting/rgbcolor.js',
  'resources/vendor/amcharts/exporting/filesaver.js',
  'resources/vendor/amcharts/exporting/jspdf.js',
  'resources/vendor/amcharts/exporting/jspdf.plugin.addimage.js',
  'resources/vendor/amcharts/plugins/responsive/responsive.js',
  'resources/shared-js/pivot-js/pivot.js',
  'resources/shared-js/pivot-js/jquery_pivot.js',
  'resources/shared-js/bootstrap-datetimepicker.js',
  'resources/vendor/video-js/video.js',
  'resources/shared-js/bootstrap-datepicker.js',
], 'public/assets/js/shared.js');

mix.scripts([
  'resources/js/bootstrap-switch.min.js',
  'resources/js/bootstrap-tagsinput.min.js',
  'resources/vendor/jquery-ui/js/jquery-ui-1.10.3.custom.min.js',
  'resources/js/gsap/main-gsap.js',
  'resources/js/joinable/jquery.hoverIntent.js',
  'resources/js/joinable/jquery.blockUI.js',
  'resources/js/joinable/jquery.autosize.js',
  'resources/js/joinable/bootstrap.file-input.js',
  'resources/js/joinable/scrollMonitor.js',
  'resources/js/joinable/jquery.nicescroll.js',
  'resources/js/joinable/jquery.slimscroll.js',
  'resources/js/joinable/jquery.transit.js',
  'resources/js/joinable/template.js',
  'resources/js/resizeable.js',
  'resources/js/neon-api.js',
  'resources/js/cookies.min.js',
  'resources/js/neon-login.js',
  'resources/js/neon-custom.js',
  'resources/js/neon-demo.js',
  'resources/js/jquery.sparkline.min.js',
  'resources/vendor/rickshaw/vendor/d3.v3.js',
  'resources/vendor/rickshaw/rickshaw.min.js',
  'resources/js/raphael-min.js',
  'resources/js/jquery.nicescroll.min.js',
  'resources/js/jquery.validate.min.js',
  'resources/js/bootstrap-datepicker.js',
  'resources/js/bootstrap-timepicker.min.js',
  'resources/vendor/daterangepicker/daterangepicker.js',
  'resources/vendor/bootstrap-multiselect-0.9.13/dist/js/bootstrap-multiselect.js',
  'resources/js/cxp.js'
], 'public/assets/js/script.js');

mix.js('resources/js/app.js', 'public/js')
   .postCss('resources/css/app.css', 'public/css')
   .tailwind('./tailwind.config.js');

if (mix.inProduction()) {
  mix
   .version()
   .purgeCss();
}

// useful for editing blade files
// add to env
// BROWSERSYNC=true
if (process.env.BROWSERSYNC) {
    mix.browserSync(process.env.APP_URL)
}
