var costTbl;
$(function(){
    costTbl = $('#costTbl').dataTable( {
        "pageLength": 25,
        "buttons": [
            'copy', 'excel', 'pdf', 'csv'
        ],
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/admin/spotcosts/data",
            "data": {
                "channel_id": $('#hidden_channel_id').val()
            }
        },
        columns: [
            {data: 'id', name: 'id', searchable: true, orderable: true},
            {data: 'channel_name', name: 'channel.name', searchable: true, orderable: true},
            {data: 'daypart_name', name: 'daypart.name'},
            {data: 'time_slot', name: 'time_slot'},
            {data: 'amount', name: 'amount'},
            {data: 'created_at', name: 'created_at'},
        ],
        responsive: true,
        "order": [[ 2, "desc" ]],
    });

    var frm = $('#addCost');
    frm.submit(function(e){
        e.preventDefault();

        var request = $.ajax({
            url: "/admin/spotcosts/add",
            type: "post",
            data: $('#addCost').serialize(),
            async: true
        });
        request.done(function (response, textStatus, jqXHR){
            $('#daypart_id > option:selected').prop("selected", false).next().prop("selected", true);
            $('#amount').val('').focus();
            costTbl.fnDraw();
        });
        request.fail(function (response, textStatus, jqXHR){
            console.log(response);
            alert('An Error Occurred!');
        });

        return false;
    });

    jQuery('.numbersOnly').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });

    $('#costTbl').on('click', '.deleteCost', function(e){
        e.preventDefault();
        var costID = $(this).data("spotcostid");
        var msg  = '<i class="icon-warning-sign modal-icon"></i>&nbsp;Are you sure you want to delete this cost?&nbsp;';
        bootbox.dialog({
            "message": msg,
            "title": "Please Confirm",
            "buttons": {
                danger: {
                    label: "Confirm Delete",
                    className: "btn-danger",
                    callback: function() {
                        var delete_cost = $.ajax({
                            url: "/admin/spotcosts/delete",
                            type: "post",
                            data: { id: costID},
                            async: true
                        });
                        delete_cost.done(function (){
                            costTbl.fnDraw();
                        });
                        delete_cost.fail(function (){
                            console.log(response);
                            alert('An Error Occurred!');
                        });
                    }
                },
                close: {
                    label: "Cancel",
                    className: "btn"
                }
            }
        });
    });
});
