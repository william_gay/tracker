<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPredictorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_predictors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('air_month')->unsigned();
            $table->integer('month_airing_count')->unsigned();
            $table->integer('first_airing_year')->unsigned()->nullable();
            $table->integer('last_airing_year')->unsigned()->nullable();

            $table->integer('sf_month_airing_count')->unsigned();
            $table->integer('sf_first_airing_year')->unsigned()->nullable();
            $table->integer('sf_last_airing_year')->unsigned()->nullable();

            $table->integer('lf_month_airing_count')->unsigned();
            $table->integer('lf_first_airing_year')->unsigned()->nullable();
            $table->integer('lf_last_airing_year')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('product_id')
              ->references('id')->on('products')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_predictors');
    }
}
