<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToProgramVersions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_versions', function (Blueprint $table) {
            $table->dropIndex('programs_language_id_index');
            $table->index(['monitor_date', 'language_id', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_versions', function (Blueprint $table) {
            $table->dropIndex('program_versions_monitor_date_language_id_deleted_at_index');
        });
    }
}
