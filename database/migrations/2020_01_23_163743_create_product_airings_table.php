<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAiringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_airings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->unsigned();
            $table->datetime('date');
            $table->integer('airings')->default(0);
            $table->decimal('media_spend', 10, 2)->default(0);
            $table->integer('sf_airings')->default(0);
            $table->decimal('sf_media_spend', 10, 2)->default(0);
            $table->integer('lf_airings')->default(0);
            $table->decimal('lf_media_spend', 10, 2)->default(0);
            $table->integer('category_airings')->default(0);
            $table->decimal('category_avg_media_spend', 10, 2)->default(0);
            $table->integer('category_sf_airings')->default(0);
            $table->decimal('category_sf_avg_media_spend', 10, 2)->default(0);
            $table->integer('category_lf_airings')->default(0);
            $table->decimal('category_lf_avg_media_spend', 10, 2)->default(0);
            $table->timestamps();

            $table->foreign('product_id')
              ->references('id')->on('products')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_airings');
    }
}
