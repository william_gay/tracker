<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateSpotDetectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spot_detections', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('spot_version_id');
            $table->date('rank_date')->nullable();
            $table->tinyInteger('new_for_week')->default(1);
            $table->unsignedInteger('detections');
            $table->double('media_index', 8, 2)->default(0.00);
            $table->integer('language_id')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['spot_version_id', 'rank_date', 'new_for_week', 'detections'], 'spot_detections_spot_id_rank_date_new_for_week_detections_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_detections');
    }
}
