<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToSpotVersionsAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spot_versions', function (Blueprint $table) {
            $table->index(['air_date', 'language_id', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spot_versions', function (Blueprint $table) {
            $table->dropIndex('spot_versions_air_date_language_id_deleted_at_index');
        });
    }
}
