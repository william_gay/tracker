<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSpotAirtimesIndexes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spot_airtimes', function (Blueprint $table) {
            $table->dropIndex('spot_airtimes_user_id_index');
            $table->dropIndex('spot_airtimes_cost_id_index');
            $table->dropIndex('spot_airtimes_created_at_index');
            $table->dropIndex('spot_airtimes_daypart_id_index');
            $table->dropIndex('spot_airtimes_channel_id_index');
            $table->dropIndex('spot_airtimes_air_date_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
