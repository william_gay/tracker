<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->datetime('last_lf_airing')->after('winmo_brand_id')->nullable();
            $table->datetime('last_sf_airing')->after('winmo_brand_id')->nullable();
            $table->integer('sub_category_channel_count')->after('last_lf_airing')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('last_lf_airing');
            $table->dropColumn('last_sf_airing');
            $table->dropColumn('sub_category_channel_count');
        });
    }
}
