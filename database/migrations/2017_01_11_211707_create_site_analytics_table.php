<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateSiteAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_analytics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 100);
            $table->unsignedInteger('type_id')->nullable();
            $table->string('extra', 255)->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->index('user_id', 'user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_analytics');
    }
}
