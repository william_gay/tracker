<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateOauthSessionTokenScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth_session_token_scopes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('session_access_token_id');
            $table->unsignedInteger('scope_id');
            $table->timestamps();

            $table->unique(['session_access_token_id', 'scope_id'], 'oauth_session_token_scopes_satid_sid_unique');
            $table->index('scope_id', 'oauth_session_token_scopes_scope_id_index');

            $table->foreign('scope_id', 'oauth_session_token_scopes_scope_id_foreign')->references('id')->on('oauth_scopes')->onDelete('CASCADE
')->onUpdate('NO ACTION');
            $table->foreign('session_access_token_id', 'oauth_session_token_scopes_session_access_token_id_foreign')->references('id')->on('oauth_session_access_tokens')->onDelete('CASCADE
')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth_session_token_scopes');
    }
}
