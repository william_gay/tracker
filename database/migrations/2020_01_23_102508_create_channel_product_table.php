<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('channel_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->unsigned();
            $table->integer('channel_id')->unsigned();
            $table->integer('airings')->default(0);
            $table->integer('media_spend')->default(0);
            $table->integer('sf_airings')->default(0);
            $table->integer('sf_media_spend')->default(0);
            $table->integer('lf_airings')->default(0);
            $table->integer('lf_media_spend')->default(0);
            $table->timestamps();

            $table->foreign('product_id')
              ->references('id')->on('products')
              ->onDelete('cascade');

            $table->foreign('channel_id')
              ->references('id')->on('channels')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channel_product');
    }
}
