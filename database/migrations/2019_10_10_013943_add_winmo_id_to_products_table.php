<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWinmoIdToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->integer('winmo_company_id')->nullable()->after('sub_category_id');
            $table->integer('winmo_brand_id')->nullable()->after('winmo_company_id');
            $table->string('asin')->nullable()->change();
            $table->string('gtin')->nullable()->change();
            $table->string('sku')->nullable()->change();
            $table->string('model_number')->nullable()->change();
            $table->text('description')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('winmo_company_id');
            $table->dropColumn('winmo_brand_id');
        });
    }
}
