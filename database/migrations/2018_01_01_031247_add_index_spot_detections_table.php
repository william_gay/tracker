<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexSpotDetectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spot_detections', function (Blueprint $table) {
            $table->index(['spot_version_id', 'rank_date', 'detections'], 'spot_detections_ranking');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spot_detections', function (Blueprint $table) {
            $table->dropIndex('spot_detections_ranking');
        });
    }
}
