<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnsOnProductAiringsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_airings', function (Blueprint $table) {
            $table->renameColumn('category_avg_media_spend', 'category_media_spend');
            $table->renameColumn('category_sf_avg_media_spend', 'category_sf_media_spend');
            $table->renameColumn('category_lf_avg_media_spend', 'category_lf_media_spend');

            $table->integer('category_sf_brand_count')->default(0)->after('lf_media_spend');
            $table->integer('category_lf_brand_count')->default(0)->after('lf_media_spend');
            $table->integer('category_brand_count')->default(0)->after('lf_media_spend');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_airings', function (Blueprint $table) {
            $table->renameColumn('category_media_spend', 'category_avg_media_spend');
            $table->renameColumn('category_sf_media_spend', 'category_sf_avg_media_spend');
            $table->renameColumn('category_lf_media_spend', 'category_lf_avg_media_spend');

            $table->dropColumn('category_sf_brand_count');
            $table->dropColumn('category_lf_brand_count');
            $table->dropColumn('category_brand_count');
        });
    }
}
