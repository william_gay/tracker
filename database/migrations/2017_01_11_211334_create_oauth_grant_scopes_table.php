<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Migration auto-generated by Sequel Pro Laravel Export
 * @see https://github.com/cviebrock/sequel-pro-laravel-export
 */
class CreateOauthGrantScopesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth_grant_scopes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('grant_id');
            $table->unsignedInteger('scope_id');
            $table->timestamps();

            

            $table->foreign('grant_id', 'oauth_grant_scopes_grant_id_foreign')->references('id')->on('oauth_grants')->onDelete('CASCADE
')->onUpdate('RESTRICT');
            $table->foreign('scope_id', 'oauth_grant_scopes_scope_id_foreign')->references('id')->on('oauth_scopes')->onDelete('CASCADE
')->onUpdate('NO ACTION');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth_grant_scopes');
    }
}
