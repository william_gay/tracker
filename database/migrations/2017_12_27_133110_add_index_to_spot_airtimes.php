<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToSpotAirtimes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spot_airtimes', function (Blueprint $table) {
            $table->index(['channel_id', 'air_date', 'spot_version_id', 'deleted_at'], 'spot_airtimes_catch_all_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spot_airtimes', function (Blueprint $table) {
            $table->dropIndex('spot_airtimes_catch_all_index');
        });
    }
}
