<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPredictorColumnsToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('predictor_30')->default(false)->after('sub_category_channel_count');
            $table->boolean('predictor_60')->default(false)->after('sub_category_channel_count');
            $table->boolean('predictor_90')->default(false)->after('sub_category_channel_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('predictor_30');
            $table->dropColumn('predictor_60');
            $table->dropColumn('predictor_90');
        });
    }
}
