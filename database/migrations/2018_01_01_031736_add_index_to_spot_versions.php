<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexToSpotVersions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spot_versions', function (Blueprint $table) {
            // Duplicate key
            // $table->index(['spot_id', 'language_id', 'service', 'non_dr', 'brand_advert'], 'spot_versions_rankings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spot_versions', function (Blueprint $table) {
            $table->dropIndex('spot_versions_rankings');
        });
    }
}
