<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class RankingsCompleted extends Notification implements ShouldQueue
{
    use Queueable;

    public $weekEnding;
    public $weekStarting;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($weekEnding, $weekStarting)
    {
        $this->weekEnding = $weekEnding;
        $this->weekStarting = $weekStarting;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Rankings have been calculated from '.$this->weekStarting.' to '.$this->weekEnding)
                    ->action('View Them Now', url('/'));
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        return (new SlackMessage)
                ->from('Ghost', ':ghost:')
                ->to('#ma-alerts')
                ->content('Rankings have been calculated from '.$this->weekStarting.' to '.$this->weekEnding);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
