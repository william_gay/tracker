<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->view->composer(
            ['cxp.partials.menu', 'cxp.reports.index'],
            \App\Composers\ReportMenuComposer::class
        );

        $this->app->view->composer(
            ['cxp.dashboard.index'],
            \App\Composers\StatsComposer::class
        );
    }
}
