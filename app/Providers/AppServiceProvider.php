<?php

namespace App\Providers;

use Bugsnag;
use Sentry;
use Psr\Log\LoggerInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Sentry::getUser()) {
            $user = [
                'id' => $user->id,
                'name' => $user->first_name.' '.$user->last_name,
                'email' => $user->email,
                'username' => $user->username,
            ];

            Bugsnag::registerCallback(function ($report) use ($user) {
                $report->setUser($user);
            });
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
