<?php

namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class FormMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->setupDateRangePicker();
        $this->setupDateRangeMonthPicker();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function setupDateRangePicker()
    {
        Form::macro('dateRangePickerStart', function ($name, $start_date) {
            return '<input type="hidden" name="'. $name . '" id="' . $name . '" value="' . $start_date->format("F j, Y") . '">';
        });

        Form::macro('dateRangePickerEnd', function ($name, $end_date) {
            return '<input type="hidden" name="'. $name . '" id="' . $name . '" value="' . $end_date->format("F j, Y") . '">';
        });
    }

    private function setupDateRangeMonthPicker()
    {
        Form::macro('dateRangePickerMonth', function ($name, $month) {
            return '<input class="form-control" type="text" name="'. $name . '" id="' . $name . '" value="' . $month->format("F Y") . '">';
        });
    }
}
