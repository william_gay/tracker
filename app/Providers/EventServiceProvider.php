<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \App\Events\GroupCreated::class => [
            \App\Listeners\CreateGroupProfile::class,
        ],
        \App\Events\GroupDeleted::class => [
            \App\Listeners\DeleteGroupProfile::class,
        ],
        \App\Events\UserLogin::class => [
            \App\Listeners\LogCurrentUser::class,
        ],
        \App\Events\UserLogout::class => [
            \App\Listeners\RemoveCurrentUser::class,
        ],
        \App\Events\UserRegister::class => [
            \App\Listeners\SendNewUserAlert::class,
        ],
        \App\Events\UserUpdate::class => [],
        \App\Events\UserBan::class => [],
        \App\Events\UserUnBan::class => [],
        \App\Events\UserSuspend::class => [],
        \App\Events\UserUnSuspend::class => [],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
