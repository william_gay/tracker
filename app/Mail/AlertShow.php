<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AlertShow extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.alert_shows')
            ->text('emails.alert_shows_plain')
            ->subject($this->data['subject'])
            ->with([
                'user' => $this->user,
                'alert' => $this->data['alert'],
                'week_ending' => $this->data['week_ending'],
                'shows' => $this->data['shows'],
            ]);
    }
}
