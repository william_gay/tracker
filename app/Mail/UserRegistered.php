<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\User;
use App\Models\Product;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $request;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $request, $subject)
    {
        $this->user = $user;
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('support@imsreport.com', 'DRTV Tracker')
            ->to('sales@imsreport.com')
            ->subject($this->subject)
            ->view('emails.new_user')
            ->with([
                'user' => $this->user,
                'request' => $this->request,
                'product' => isset($this->request['product_id']) ? Product::find($this->request['product_id']) : null,
            ]);
    }
}
