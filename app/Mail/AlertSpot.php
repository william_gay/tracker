<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AlertSpot extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $data)
    {
        $this->user = $user;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.alert_spots')
            ->text('emails.alert_spots_plain')
            ->subject($this->data['subject'])
            ->with([
                'user' => $this->user,
                'alert' => $this->data['alert'],
                'week_ending' => $this->data['week_ending'],
                'spots' => $this->data['spots'],
            ]);
    }
}
