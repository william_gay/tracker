<?php

namespace App\Listeners;

use Mail;
use App\Events\UserRegister;
use App\Mail\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewUserAlert implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegister $event)
    {
        Mail::to('sales@imsreport.com')
            ->queue(new UserRegistered($event->user, $event->request, $event->subject));
    }
}
