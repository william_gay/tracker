<?php

namespace App\Listeners;

use App\Events\UserLogin;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Models\UserCurrent;
use App\Models\UserLogin as ULModel;

class LogCurrentUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogin  $event
     * @return void
     */
    public function handle(UserLogin $event)
    {
        // Clear out the current entries
        UserCurrent::where('user_id', $event->user->id)->delete();

        $logger = [
            'user_id' => $event->user->id,
            'ip_address' => $event->request->getClientIp(),
            'user_agent' => $event->request->header('user-agent'),
            'added_on' => date('Y-m-d H:i:s'),
            'type' => $event->type
        ];

        $login = new ULModel;
        $login->fill($logger);
        $login->save();

        $current = new UserCurrent;
        $current->fill($logger);
        $current->save();
    }
}
