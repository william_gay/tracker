<?php

namespace App\Listeners;

use App\Events\UserLogout;
use App\Models\UserCurrent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RemoveCurrentUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogout  $event
     * @return void
     */
    public function handle(UserLogout $event)
    {
        $users = UserCurrent::where('user_id', $event->user->id)->get();
        $users->each(function ($user) {
            $user->delete();
        });
    }
}
