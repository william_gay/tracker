<?php

namespace App\Listeners;

use App\Models\GroupsProfile;
use App\Events\GroupDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteGroupProfile implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GroupDeleted  $event
     * @return void
     */
    public function handle(GroupDeleted $event)
    {
        $profiles = GroupsProfile::where('group_id', $event->group->id)->get();
        $profiles->each(function ($profile) {
            $profile->delete();
        });
    }
}
