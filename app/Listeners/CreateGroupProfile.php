<?php

namespace App\Listeners;

use App\Models\GroupsProfile;
use App\Events\GroupCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateGroupProfile
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GroupCreated  $event
     * @return void
     */
    public function handle(GroupCreated $event)
    {
        $group_profile = new GroupsProfile;
        $group_profile->group_id = $event->group->id;
        $group_profile->data_since = date('Y-m-d').' 00:00:00';
        $group_profile->data_to = $event->endDate ? $event->endDate : date('Y-m-d').' 00:00:00';
        // if ($event->lstSince) {
        //     $group_profile->lst_since = $event->lstSince;
        // }
        $group_profile->save();
    }
}
