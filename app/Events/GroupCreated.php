<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GroupCreated
{
    use SerializesModels;

    public $group;
    public $endDate;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($group, Carbon $endDate)
    {
        $this->group = $group;
        $this->endDate = $endDate;
    }
}
