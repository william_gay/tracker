<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Models\User;

class UserRegister
{
    use SerializesModels;

    public $user;
    public $request;
    public $subject;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $request, $subject = 'New User Created!')
    {
        $this->user = $user;
        $this->request = $request;
        $this->subject = $subject;
    }
}
