<?php namespace App\Services\Validators;

class CategoryReportValidator extends Validator
{

    public static $rules = [
        'name' => 'required',
        'slug' => 'required',
    ];
}
