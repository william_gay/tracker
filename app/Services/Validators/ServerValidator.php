<?php namespace App\Services\Validators;

class ServerValidator extends Validator
{

    public static $rules = [
        'name' => 'required'
    ];
}
