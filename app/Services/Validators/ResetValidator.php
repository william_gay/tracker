<?php namespace App\Services\Validators;

class ResetValidator extends Validator
{

    /**
     * User validation rules
     * @var array
     */
    public static $rules = [
        'email'      => 'required|email'
    ];
}
