<?php namespace App\Services\Validators;

class SpotValidator extends Validator
{

    public static $rules = [
        'title' => 'required',
        'version' => 'required',

    ];
}
