<?php namespace App\Services\Validators;

class LiveShoppingProductValidator extends Validator
{

    public static $rules = [
        'name' => 'required',
        'msrp' => 'numeric',
    ];
}
