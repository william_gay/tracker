<?php namespace App\Services\Validators;

class LiveShoppingShowValidator extends Validator
{

    public static $rules = [
        'channel_id' => 'required',
        'title' => 'required',
    ];
}
