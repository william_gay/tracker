<?php namespace App\Services\Validators;

class ProductValidator extends Validator
{

    public static $rules = [
        'name' => 'required',
        'slug' => 'required',
    ];
}
