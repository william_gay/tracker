<?php namespace App\Services\Validators;

class ReportValidator extends Validator
{

    public static $rules = [
        'name' => 'required',
        'slug' => 'required',
        'type' => 'required'
    ];
}
