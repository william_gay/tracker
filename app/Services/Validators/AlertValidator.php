<?php namespace App\Services\Validators;

class AlertValidator extends Validator
{

    public static $rules = [
        'interval' => 'required',
        'type' => 'required',
        'type_extra' => 'numeric',
    ];
}
