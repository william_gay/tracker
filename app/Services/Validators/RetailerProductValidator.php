<?php namespace App\Services\Validators;

class RetailerProductValidator extends Validator
{

    public static $rules = [
        'product_id' => 'required',
        'date_recorded' => 'required'
    ];
}
