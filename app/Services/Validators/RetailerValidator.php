<?php namespace App\Services\Validators;

class RetailerValidator extends Validator
{

    public static $rules = [
        'name' => 'required',
        'slug' => 'required',
    ];
}
