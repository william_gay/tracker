<?php namespace App\Services\Validators;

class DashboardTypeValidator extends Validator
{

    public static $rules = [
        'name' => 'required',
        'slug' => 'required'
    ];
}
