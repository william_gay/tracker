<?php namespace App\Services\Validators;

class ChannelValidator extends Validator
{

    public static $rules = [
        'abbr' => 'required',
        'name' => 'required'
    ];
}
