<?php namespace App\Services\Validators;

class ProgramValidator extends Validator
{

    public static $rules = [
        'title' => 'required',
        'grid_title' => 'required',
        'version' => 'required',

    ];
}
