<?php namespace App\Services\Validators;

class DaypartValidator extends Validator
{

    public static $rules = [
        'name' => 'required',
        'abbr' => 'required'
    ];
}
