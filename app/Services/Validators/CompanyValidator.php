<?php namespace App\Services\Validators;

class CompanyValidator extends Validator
{

    public static $rules = [
        'name' => 'required|unique:companies'
    ];

    public function passes()
    {
        if (isset($this->data['id'])) {
            static::$rules['name'] = "required|unique:companies,name,{$this->data['id']}";
        }

        return parent::passes();
    }
}
