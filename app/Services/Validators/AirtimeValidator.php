<?php namespace App\Services\Validators;

class AirtimeValidator extends Validator
{

    public static $rules = [
        'channel_id' => 'required',
        'program_id' => 'required',
        'air_date' => 'required',

    ];
}
