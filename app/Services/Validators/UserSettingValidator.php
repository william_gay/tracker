<?php namespace App\Services\Validators;

class UserSettingValidator extends Validator
{

    /**
     * User validation rules
     * @var array
     */
    public static $rules = [
        'first_name' => 'required',
        'last_name'  => 'required',
        'password'   => 'required|confirmed',
    ];

    /**
     * Perform validation
     *
     * @author Steve Williamson
     * @link   http://www.imsreport.com
     *
     * @return Bool
     */
    public function passes()
    {
        
        if (isset($this->data['id'])) {
        /**
             *  if password and conf_pass are empty
             *  The user don't want to change is password
             *  so remove password rules
             */
            if (empty($this->data['password']) and empty($this->data['password_confirmation'])) {
                unset(static::$rules['password']);
                unset($this->data['password']);
            }
        }

        $status = parent::passes();
        unset($this->data['password_confirmation']);
        return $status;
    }
}
