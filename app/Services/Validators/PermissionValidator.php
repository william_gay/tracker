<?php namespace App\Services\Validators;

class PermissionValidator extends Validator
{

   /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'name'        => 'required|unique:permissions',
        'permissions' => 'required'
    ];


    public function passes()
    {
        if (isset($this->data['id'])) {
            static::$rules['name'] = "required|unique:permissions,name,{$this->data['id']}";
        }
       
        return parent::passes();
    }
}
