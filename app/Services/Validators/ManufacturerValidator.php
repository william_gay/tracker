<?php namespace App\Services\Validators;

class ManufacturerValidator extends Validator
{

    public static $rules = [
        'name' => 'required',
        'slug' => 'required',
    ];
}
