<?php namespace App\Services\Validators;

class CategoryValidator extends Validator
{

    public static $rules = [
        'parent_id' => 'required',
        'name' => 'required',
        'description' => 'required',
    ];
}
