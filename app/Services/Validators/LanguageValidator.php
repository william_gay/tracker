<?php namespace App\Services\Validators;

class LanguageValidator extends Validator
{

    public static $rules = [
        'name' => 'required',
        'abbr' => 'required'
    ];
}
