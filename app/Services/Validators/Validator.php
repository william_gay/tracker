<?php namespace App\Services\Validators;

abstract class Validator
{

    /**
     * Container for the data that needs to be validated
     * @var array
     */
    protected $data;

    /**
     * Validation errors
     * @var array
     */
    public $errors;

    /**
     * Validation rules
     * @var array
     */
    public static $rules;

    /**
     * Initialize validator
     * @param array $data
     */
    public function __construct($data = null)
    {
        $this->data = $data ?: \Request::all();
    }

    /**
     * Check if validation passes
     * @return bool
     */
    public function passes()
    {
        $validation = \Validator::make($this->data, static::$rules);

        if ($validation->passes()) {
            return true;
        }

        $this->errors = $validation->messages();

        return false;
    }

    /**
     * Get the validation errors
     *
     * @author Steve Williamson
     * @link   http://www.imsreport.com
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * get the validated data
     *
     * @author Steve Williamson
     * @link   http://www.imsreport.com
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
}
