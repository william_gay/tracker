<?php namespace App\Services\Validators;

class LiveShoppingProductVersionValidator extends Validator
{

    public static $rules = [
        'name' => 'required',
        'msrp' => 'numeric',
    ];
}
