<?php namespace App\Services\Validators;

class LiveShoppingGuideValidator extends Validator
{

    public static $rules = [
        'channel_id' => 'required',
        'name' => 'required',
        'url' => 'required'
    ];
}
