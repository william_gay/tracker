<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Product;
use App\Models\SpotVersion;
use App\Models\ProgramVersion;
use App\Models\ProductPredictor;

class WinmoPredictor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $product;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $spotVersions = $this->product->spotVersions;
        $programVersions = $this->product->programVersions;

        $spotQuery = SpotVersion::selectRaw('MONTH(air_date) as air_month, count(air_date) as sf_month_airing_count, YEAR(MAX(air_date)) as sf_last_airing, YEAR(MIN(air_date)) as sf_first_airing')
            ->whereIn('id', $spotVersions->pluck('id'))
            ->groupBy('air_month')
            ->get();

        $programQuery = ProgramVersion::selectRaw('MONTH(initial_date) as air_month, count(initial_date) as lf_month_airing_count, YEAR(MAX(initial_date)) as lf_last_airing, YEAR(MIN(initial_date)) as lf_first_airing')
            ->whereIn('id', $programVersions->pluck('id'))
            ->groupBy('air_month')
            ->get();

        $spotMonths = collect($spotQuery->toArray());
        $programMonths = collect($programQuery->toArray());

        $combined = $spotMonths->mergeRecursive($programMonths)->sortBy('air_month');
        $final = $combined->map(function ($item) {
            $sfAirCount = isset($item['sf_month_airing_count']) ? $item['sf_month_airing_count'] : 0;
            $lfAirCount = isset($item['lf_month_airing_count']) ? $item['lf_month_airing_count'] : 0;

            if (isset($item['lf_last_airing']) && isset($item['sf_last_airing'])) {
                $lastAiring = max($item['lf_last_airing'], $item['sf_last_airing']);
            } elseif (isset($item['lf_last_airing'])) {
                $lastAiring = $item['lf_last_airing'];
            } elseif (isset($item['sf_last_airing'])) {
                $lastAiring = $item['sf_last_airing'];
            } else {
                $lastAiring = null;
            }

            if (isset($item['lf_first_airing']) && isset($item['sf_first_airing'])) {
                $firstAiring = min($item['lf_first_airing'], $item['sf_first_airing']);
            } elseif (isset($item['lf_first_airing'])) {
                $firstAiring = $item['lf_first_airing'];
            } elseif (isset($item['sf_first_airing'])) {
                $firstAiring = $item['sf_first_airing'];
            } else {
                $firstAiring = null;
            }

            return [
                'air_month' => $item['air_month'],
                'month_airing_count' => $sfAirCount + $lfAirCount,
                'first_airing_year' => $firstAiring,
                'last_airing_year' => $lastAiring,
                'sf_month_airing_count' => $sfAirCount,
                'sf_first_airing_year' => isset($item['sf_first_airing']) ? $item['sf_first_airing'] : null,
                'sf_last_airing_year' => isset($item['sf_last_airing']) ? $item['sf_last_airing'] : null,
                'lf_month_airing_count' => $lfAirCount,
                'lf_first_airing_year' => isset($item['lf_first_airing']) ? $item['lf_first_airing'] : null,
                'lf_last_airing_year' => isset($item['lf_last_airing']) ? $item['lf_last_airing'] : null,
            ];
        });

        foreach ($final as $month) {
            $predictor = ProductPredictor::updateOrCreate(
                [
                    "product_id" => $this->product->id,
                    "air_month" => $month['air_month'],
                ],
                [
                    "month_airing_count" => $month['month_airing_count'],
                    "first_airing_year" => $month['first_airing_year'],
                    "last_airing_year" => $month['last_airing_year'],
                    "sf_month_airing_count" => $month['sf_month_airing_count'],
                    "sf_first_airing_year" => $month['sf_first_airing_year'],
                    "sf_last_airing_year" => $month['sf_last_airing_year'],
                    "lf_month_airing_count" => $month['lf_month_airing_count'],
                    "lf_first_airing_year" => $month['lf_first_airing_year'],
                    "lf_last_airing_year" => $month['lf_last_airing_year'],
                ]
            );
        }

        // dd($final);
        $threshold  = now()->subYears(2)->year;

        $thirty = now()->addDays(30)->month;
        $sixty = now()->addDays(60)->month;
        $ninety = now()->addDays(90)->month;

        $thirtyHistorical = $final->where('air_month', $thirty)->first();
        $sixtyHistorical = $final->where('air_month', $sixty)->first();
        $ninetyHistorical = $final->where('air_month', $ninety)->first();

        $this->product->predictor_30 = false;
        if ($thirtyHistorical && $thirtyHistorical['month_airing_count'] > 0
            && $thirtyHistorical['last_airing_year'] > $threshold) {
            $this->product->predictor_30 = true;
        }

        $this->product->predictor_60 = false;
        if ($sixtyHistorical && $sixtyHistorical['month_airing_count'] > 0
            && $sixtyHistorical['last_airing_year'] > $threshold) {
            $this->product->predictor_60 = true;
        }

        $this->product->predictor_90 = false;
        if ($ninetyHistorical && $ninetyHistorical['month_airing_count'] > 0
            && $ninetyHistorical['last_airing_year'] > $threshold) {
            $this->product->predictor_60 = true;
        }

        $this->product->save();
    }
}
