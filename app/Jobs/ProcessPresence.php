<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Product;
use App\Models\Airtime;
use App\Models\SpotAirtime;

class ProcessPresence implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $product;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $spotLastAir = $this->spotAirTime($this->product);
        $showLastAir = $this->showAirTime($this->product);

        if ($spotLastAir) {
            $this->product->last_sf_airing = $spotLastAir->air_date;
        }

        if ($showLastAir) {
            $this->product->last_lf_airing = $showLastAir->air_date;
        }

        $this->product->save();
    }

    private function spotAirTime($product)
    {
        return SpotAirtime::selectRaw('MAX(air_date) as air_date')
            ->whereIn('spot_version_id', $product->spotVersions->pluck('id'))
            ->where('air_date', '!=', '0000-00-00 00:00:00')
            ->orderBy('air_date', 'desc')
            ->first();
    }

    private function showAirTime($product)
    {
        return Airtime::selectRaw('MAX(air_date) as air_date')
            ->whereIn('program_version_id', $product->programVersions->pluck('id'))
            ->where('air_date', '!=', '0000-00-00 00:00:00')
            ->orderBy('air_date', 'desc')
            ->first();
    }
}
