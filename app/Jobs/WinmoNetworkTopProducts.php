<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Channel;
use App\Models\Product;
use App\Models\Airtime;
use App\Models\SpotAirtime;

class WinmoNetworkTopProducts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $channel;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Channel $channel)
    {
        $this->channel = $channel;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $winmoProducts = Product::orderBy('id')->get();

        $winmoProducts->each(function ($product) {
            $showVersions = $product->programVersions;
            $airtimes = Airtime::selectRaw('count(airtimes.id) as lf_airings, sum(airtimes.cost) as lf_media_spend')
                ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
                ->leftJoin('programs', 'programs.id', '=', 'program_versions.program_id')
                ->whereIn('program_version_id', $showVersions->pluck('id'))
                ->where('airtimes.channel_id', $this->channel->id)
                ->where('airtimes.air_date', '>=', now()->subMonths(6)->format('Y-m-d H:i:s'))
                ->groupBy('program_id')
                ->first();

            $spotVersions = $product->spotVersions;
            $spotAirtimes = SpotAirtime::selectRaw('count(spot_airtimes.id) as sf_airings, sum(spot_airtimes.cost) as sf_media_spend')
                ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
                ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
                ->whereIn('spot_version_id', $spotVersions->pluck('id'))
                ->where('spot_airtimes.channel_id', $this->channel->id)
                ->where('spot_airtimes.air_date', '>=', now()->subMonths(6)->format('Y-m-d H:i:s'))
                ->groupBy('spot_id')
                ->first();

            if (! $airtimes && ! $spotAirtimes) {
                // Remove old data
                $this->channel->products()->detach($product->id);
            }

            $sfAirings = 0;
            $sfMediaSpend = 0;
            if ($spotAirtimes) {
                $sfAirings = $spotAirtimes->sf_airings;
                $sfMediaSpend = $spotAirtimes->sf_media_spend;
            }

            $lfAirings = 0;
            $lfMediaSpend = 0;
            if ($airtimes) {
                $lfAirings = $airtimes->lf_airings;
                $lfMediaSpend = $airtimes->lf_media_spend;
            }

            if ($spotAirtimes || $airtimes) {
                $this->channel->products()->syncWithoutDetaching([$product->id => [
                    'airings' => $sfAirings + $lfAirings,
                    'media_spend' => $sfMediaSpend + $lfMediaSpend,
                    'sf_airings' => $sfAirings,
                    'sf_media_spend' => $sfMediaSpend,
                    'lf_airings' => $lfAirings,
                    'lf_media_spend' => $lfMediaSpend,
                ]]);
            }
        });
    }
}
