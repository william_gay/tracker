<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Database\Eloquent\Builder;

use App\Models\Product;
use App\Models\ProductAiring;
use App\Models\SpotAirtime;
use App\Models\Airtime;

class WinmoAggregateMediaSpend implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $product;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $spotAirtimes = $this->getSpotAirtimes($this->product);
        $spotAirtimes->each(function ($airtime) {
            ProductAiring::updateOrCreate(
                [
                    'product_id' => $this->product->id,
                    'date' => $airtime->date,
                ],
                [
                    'sf_airings' => $airtime->spot_airings,
                    'sf_media_spend' => $airtime->spot_media_spend,
                ]
            );
        });

        $showAirtimes = $this->getShowAirtimes($this->product);
        $showAirtimes->each(function ($airtime) {
            ProductAiring::updateOrCreate(
                [
                    'product_id' => $this->product->id,
                    'date' => $airtime->date,
                ],
                [
                    'lf_airings' => $airtime->show_airings,
                    'lf_media_spend' => $airtime->show_media_spend,
                ]
            );
        });

        $spotCategorySpend = $this->getSpotCategorySpend($this->product->category->id);
        $spotCategorySpend->each(function ($airtime) {
            ProductAiring::updateOrCreate(
                [
                    'product_id' => $this->product->id,
                    'date' => $airtime->date,
                ],
                [
                    'category_sf_airings' => $airtime->category_spot_airings,
                    'category_sf_media_spend' => $airtime->category_spot_media_spend,
                    'category_sf_brand_count' => $airtime->category_spot_brand_count,
                ]
            );
        });

        $showCategorySpend = $this->getShowCategorySpend($this->product->category->id);
        $showCategorySpend->each(function ($airtime) {
            ProductAiring::updateOrCreate(
                [
                    'product_id' => $this->product->id,
                    'date' => $airtime->date,
                ],
                [
                    'category_lf_airings' => $airtime->category_show_airings,
                    'category_lf_media_spend' => $airtime->category_show_media_spend,
                    'category_lf_brand_count' => $airtime->category_show_brand_count,
                ]
            );
        });

        // Add them up
        $airings = ProductAiring::where('product_id', $this->product->id)
            ->orderBy('id')
            ->get();

        $airings->each(function ($airing) {
            $airing->airings = $airing->sf_airings + $airing->lf_airings;
            $airing->media_spend = $airing->sf_media_spend + $airing->lf_media_spend;
            $airing->category_airings = $airing->category_lf_airings + $airing->category_sf_airings;
            $airing->category_brand_count = $airing->category_lf_brand_count + $airing->category_sf_brand_count;
            $airing->category_media_spend = ($airing->category_lf_media_spend + $airing->category_sf_media_spend)/($airing->category_brand_count);
            $airing->save();
        });
    }

    private function getSpotAirtimes($product)
    {
        $spotVersions = $product->spotVersions;
        return $networksByAirtime = SpotAirtime::selectRaw('DATE(air_date) as date, SUM(cost) as spot_media_spend, count(spot_airtimes.id) as spot_airings')
            ->where('spot_airtimes.air_date', '>=', now()->subMonths(2)->format('Y-m-d H:i:s'))
            ->whereIn(
                'spot_version_id',
                $spotVersions->pluck('id')
            )
            ->groupBy('date')
            ->orderBy('date')
            ->get();
    }

    private function getShowAirtimes($product)
    {
        $programVersions = $product->programVersions;
        return $networksByAirtime = Airtime::selectRaw('DATE(air_date) as date, SUM(cost) as show_media_spend, count(airtimes.id) as show_airings')
            ->where('airtimes.air_date', '>=', now()->subMonths(2)->format('Y-m-d H:i:s'))
            ->whereIn(
                'program_version_id',
                $programVersions->pluck('id')
            )
            ->groupBy('date')
            ->orderBy('date')
            ->get();
    }

    private function getSpotCategorySpend($categoryId)
    {
        return $networksByAirtime = SpotAirtime::selectRaw('spot_version_id, DATE(spot_airtimes.air_date) as date, SUM(spot_airtimes.cost) as category_spot_media_spend, count(spot_airtimes.id) as category_spot_airings, count(DISTINCT spot_versions.spot_id) as category_spot_brand_count')
            ->where('spot_airtimes.air_date', '>=', now()->subMonths(2)->format('Y-m-d H:i:s'))
            ->whereHas('spotVersion', function (Builder $query) use ($categoryId) {
                $query->where('category_id', $categoryId);
            })
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->groupBy('date')
            ->orderBy('date')
            ->get();
    }

    private function getShowCategorySpend($categoryId)
    {
        return $networksByAirtime = Airtime::selectRaw('program_version_id, DATE(airtimes.air_date) as date, SUM(airtimes.cost) as category_show_media_spend, count(airtimes.id) as category_show_airings, count(DISTINCT program_versions.program_id) as category_show_brand_count')
            ->where('airtimes.air_date', '>=', now()->subMonths(2)->format('Y-m-d H:i:s'))
            ->whereHas('programVersion', function (Builder $query) use ($categoryId) {
                $query->where('category_id', $categoryId);
            })
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->groupBy('date')
            ->orderBy('date')
            ->get();
    }
}
