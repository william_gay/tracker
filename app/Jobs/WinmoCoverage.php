<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\Product;
use App\Models\Airtime;
use App\Models\SpotAirtime;

class WinmoCoverage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $subCategoryId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($subCategoryId)
    {
        $this->subCategoryId = $subCategoryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Calculate Coverage by counting networks all sub categories run on
        $subCategoryLFByNetwork = $this->getLFNetworks($this->subCategoryId);
        $subCategorySFByNetwork = $this->getSFNetworks($this->subCategoryId);

        $lfNetworks = collect($subCategoryLFByNetwork->toArray());
        $sfNetworks = collect($subCategorySFByNetwork->toArray());
        $totalSubCategoryNetwork = $lfNetworks->flatten()
            ->merge($sfNetworks->flatten())->unique()->count();
        // End Calculate

        // Update all products with the same sub-category
        $products = Product::where('sub_category_id', $this->subCategoryId)->update([
            'sub_category_channel_count' => $totalSubCategoryNetwork,
        ]);
    }

    private function getLFNetworks($subCategoryId)
    {
        return $networksByAirtime = SpotAirtime::select('channel_id')
            ->whereHas('spotVersion', function ($query) use ($subCategoryId) {
                $query->where('sub_category_id', $subCategoryId);
            })->where('air_date', '>=', now()->subMonths(2)->format('Y-m-d H:i:s'))
            ->groupBy('channel_id')
            ->get();
    }

    private function getSFNetworks($subCategoryId)
    {
        return $networksByAirtime = Airtime::select('channel_id')
            ->whereHas('programVersion', function ($query) use ($subCategoryId) {
                $query->where('sub_category_id', $subCategoryId);
            })->where('air_date', '>=', now()->subMonths(2)->format('Y-m-d H:i:s'))
            ->groupBy('channel_id')
            ->get();
    }
}
