<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class CategoryReport extends Model
{

    use SoftDeletes;

    protected $dates = [
        'start_date', 'end_date'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category_reports';

    public $timestamps = true;

    protected $fillable = ['name','overview'];

    public function programs()
    {
        return $this->belongsToMany(\App\Models\Program::class, 'category_report_programs', 'category_report_id', 'program_id');
    }

    public function spots()
    {
        return $this->belongsToMany(\App\Models\Spot::class, 'category_report_spots', 'category_report_id', 'spot_id');
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public static function findIdBySlug($slug)
    {
        $categoryReport = CategoryReport::where('slug', $slug)->first();

        if ($categoryReport) {
            return $categoryReport->id;
        } else {
            return false;
        }
    }
}
