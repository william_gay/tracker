<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChannelMapping extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'channel_mappings';

    public $timestamps = true; //No timestamps on this one

    protected $fillable = ['channel_id', 'im2_channel_id'];
}
