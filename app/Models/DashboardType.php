<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DashboardType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dashboard_types';

    public $timestamps = true;

    protected $softDelete = true;

    protected $fillable = ['name', 'slug'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
