<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class ProgramVersion extends Model
{

    use SoftDeletes;

    protected $dates = [
        'initial_date', 'last_aired', 'monitor_date'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'program_versions';

    protected $guarded = ['id'];

    public $timestamps = true;

    public static function findByWeek($week, $language_id = 1)
    {
        return self::with('category', 'subCategory', 'marketingCompany')
            ->where('monitor_date', $week->format("Y-m-d"))
            ->where('language_id', $language_id)
            ->orderBy('title')
            ->get();
    }

    public static function findByProgramId($programId)
    {
        return self::where('program_id', $programId);
    }

    public static function findLatestByProgramId($programId)
    {
        return self::where('program_id', $programId)
            ->orderBy('program_versions.monitor_date', 'desc')
            ->first();
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function program()
    {
        return $this->belongsTo(\App\Models\Program::class);
    }

    public function language()
    {
        return $this->belongsTo(\App\Models\Language::class);
    }

    public function airtimes()
    {
        return $this->hasMany(\App\Models\Airtime::class)
            ->verified();
    }

    public function channel()
    {
        return $this->belongsTo(\App\Models\Channel::class);
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function subCategory()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function marketingCompany()
    {
        return $this->belongsTo(\App\Models\Company::class, 'marketing_company_id');
    }

    public function productionCompany()
    {
        return $this->belongsTo(\App\Models\Company::class, 'production_company_id');
    }

    public function clipsters()
    {
        return $this->morphMany(\App\Models\Autodetection\Clipster::class, 'clipsterable');
    }
}
