<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryMapping extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category_mappings';

    public $timestamps = true; //No timestamps on this one

    protected $fillable = ['category_id', 'im2_category_id'];
}
