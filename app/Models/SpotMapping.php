<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SpotMapping extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'spot_mappings';

    protected $fillable = ['spot_id', 'legacy_spot_id'];

    public $timestamps = true; //No timestamps on this one
}
