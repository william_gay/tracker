<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    use SoftDeletes;

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    public $timestamps = true;

    protected $fillable = ['parent_id', 'name','description'];

    public function parent()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function program()
    {
        return $this->belongsToMany(\App\Models\Program::class);
    }

    public function language()
    {
        return $this->belongsToMany('App\Models\Langauge');
    }

    public static function getCategories($selection, $languages)
    {
        return $selection ?: Category::whereNull('parent_id')->orderBy('name')->pluck('id')->toArray();
    }

    public static function getSubcategories($selection, $languages)
    {
        return $selection ?: Category::whereNotNull('parent_id')->orderBy('name')->pluck('id')->toArray();
    }
}
