<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramMapping extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'program_mappings';

    protected $fillable = ['program_id', 'im2_program_id'];

    public $timestamps = true; //No timestamps on this one
}
