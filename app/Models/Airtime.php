<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Airtime extends Model
{

    use SoftDeletes;

    protected $dates = [
        'air_dateable'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'airtimes';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public $timestamps = true;

    public static function findCountByProgramVersions($versions)
    {
        if (count($versions) == 0) {
            return 0;
        }

        return self::whereIn('program_version_id', $versions)->count();
    }

    public static function findMediaSpendByProgramVersions($versions)
    {
        if (count($versions) == 0) {
            return 0;
        }

        return self::whereIn('program_version_id', $versions)->sum('cost');
    }

    public function scopeVerified($query)
    {
        return $query->where('verified', 1);
    }

    public function channel()
    {
        return $this->belongsTo(\App\Models\Channel::class);
    }

    // TODO Make this a through relationship
    public function program()
    {
        return $this->belongsTo(\App\Models\Program::class);
    }

    public function programVersion()
    {
        return $this->belongsTo(\App\Models\ProgramVersion::class, 'program_version_id', 'id');
    }

    public function dayPart()
    {
        return $this->belongsTo(\App\Models\Daypart::class, 'daypart_id', 'id');
    }

    public function costs()
    {
        return $this->belongsTo(\App\Models\Cost::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
