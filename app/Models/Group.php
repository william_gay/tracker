<?php namespace App\Models;

use Eloquent;
use Cartalyst\Sentry\Groups\GroupInterface;

class Group extends \Cartalyst\Sentry\Groups\Eloquent\Group implements GroupInterface
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function profile()
    {
        return $this->hasOne('App\Models\GroupProfile');
    }

    public function users()
    {
        return $this->belongsToMany(\App\Models\User::class, 'users_groups', 'group_id', 'user_id');
    }
}
