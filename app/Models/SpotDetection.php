<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class SpotDetection extends Model
{

    use SoftDeletes;

    protected $dates = [
        'rank_date'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'spot_detections';

    public $timestamps = true;

    public function scopeRankdate($query, \Carbon\Carbon $date)
    {
        return $query->where('rank_date', $date->toDateString());
    }

    public function channels()
    {
        return $this->belongsToMany(\App\Models\Channel::class, 'spot_detections_channels', 'spot_detection_id', 'channel_id');
    }

    public function spotVersion()
    {
        return $this->hasOne(\App\Models\SpotVersion::class, 'id', 'spot_version_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
