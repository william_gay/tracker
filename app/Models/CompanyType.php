<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class CompanyType extends Model
{

    use SoftDeletes;

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'types';

    public $timestamps = true;

    protected $fillable = ['name'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class);
    }
}
