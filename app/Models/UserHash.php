<?php namespace App\Models;

use Eloquent;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class UserHash extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_hashes';

    /**@/**
     * Carbonated Columns
     * @return type
     */
    protected $dates = ['period_start', 'period_end'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    /**@/**
     * Creates a hash for a specific user and associates that user with an email campaign.
     * @param type $user
     * @param type $emailCampaignId
     * @return type
     */
    public static function createHash($user, $period, $emailCampaignId = null)
    {
        // The values for periodStart and periodEnd is dicated by the input
        // of the variable '$period'. $period is a string that is either
        // 'month' or 'week'.
        switch ($period) {
            case 'month':
                $periodStart = Carbon::now()->subMonth()->startOfMonth();
                $periodEnd   = Carbon::now()->subMonth()->endOfMonth();
                break;
            case 'week':
                $periodStart = lastFriday()->subWeek();
                $periodEnd   = lastFriday();
        }

        // Create the new Hash and store its values.
        $hash       = new UserHash;
        $hash->hash = Uuid::uuid4()->toString();
        $hash->period_start = $periodStart;
        $hash->period_end   = $periodEnd;
        $hash->period_type  = $period;

        if (isset($emailCampaignId)) {
            $hash->email_campaign_id = $emailCampaignId;
        }

        $hash->save();

        // Use eloquent to associate the hash with the user.
        $hash->user()->associate($user);
        $hash->save();

        // Serve the hashbrown
        return $hash;
    }

    /**@/**
     * Creates hashes for all users in a group and associates it with
     * a given email campaign.
     * @param type $group
     * @param type $emailCampaignId
     * @return type
     */
    public static function createHashesForAllUsersInGroup($group, $emailCampaignId = null)
    {
        // Get all users in the specified group
        $users = $group->users()->get();

        $userHashes = [];

        foreach ($users as $user) {
            // Create a hash for each User and associate it with the Email Campaign
            $userHashes[] = UserHash::createHash($user, $emailCampaignId);
        }

        return $userHashes;
    }
}
