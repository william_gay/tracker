<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OAuthClientEndpoint extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_client_endpoints';

    public $timestamps = true;

    protected $fillable = ['client_id','redirect_uri'];

    public function client()
    {
        return $this->belongsTo(\App\Models\OAuthClient::class);
    }
}
