<?php namespace App\Models\LiveShoppingTracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class LiveShoppingProduct extends Model
{

    use SoftDeletes;

    protected $dates = [
        'first_airdate'
    ];

    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'live_shopping_products';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $appends = [
        'network',
        'currency'
    ];

    protected $guarded = ['id'];


    public function getCurrencyAttribute()
    {
        if ($this->versions->first()) {
            if ($this->versions->first()->productAirtimes->first()) {
                if ($this->versions->first()->productAirtimes->first()->airtime) {
                    return $this->versions->first()->productAirtimes->first()->airtime->channel->currency;
                }
            }
        } else {
            return '$';
        }
    }
    public function getNetworkAttribute()
    {
        if ($this->versions->first()) {
            if ($this->versions->first()->productAirtimes->first()) {
                if ($this->versions->first()->productAirtimes->first()->airtime) {
                    return $this->versions->first()->productAirtimes->first()->airtime->channel->name;
                }
            }
        } else {
            return null;
        }
    }

    public function versions()
    {
        return $this->hasMany(\App\Models\LiveShoppingTracker\LiveShoppingProductVersion::class);
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\LiveShoppingTracker\LiveShoppingCategory::class, 'live_shopping_category_id');
    }

    public function subCategory()
    {
        return $this->belongsTo(\App\Models\LiveShoppingTracker\LiveShoppingCategory::class, 'live_shopping_sub_category_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
