<?php namespace App\Models\LiveShoppingTracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class LiveShoppingAirtimeProduct extends Model
{

    use SoftDeletes;

    protected $dates = [
        'start_time', 'end_time'
    ];

    protected $appends = ['graph_color'];

    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'live_shopping_airtimes_products';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function airtime()
    {
        return $this->belongsTo(\App\Models\LiveShoppingTracker\LiveShoppingAirtime::class, 'live_shopping_airtime_id');
    }

    public function productVersion()
    {
        return $this->belongsTo(\App\Models\LiveShoppingTracker\LiveShoppingProductVersion::class, 'live_shopping_product_version_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function getGraphColorAttribute()
    {
        // this will be generated differently with dynamic channel selection
        if ($this->channel_id == 208) {
            return '#0099FF';
        } elseif ($this->channel_id == 209) {
            return '#FF0000';
        } else {
            return '#FF9900';
        }
    }
}
