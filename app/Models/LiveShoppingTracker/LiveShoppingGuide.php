<?php namespace App\Models\LiveShoppingTracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class LiveShoppingGuide extends Model
{

    use SoftDeletes;

    protected $dates = [
        'air_dateable'
    ];

    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'live_shopping_guides';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function channel()
    {
        return $this->belongsTo(\App\Models\Channel::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
