<?php namespace App\Models\LiveShoppingTracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class LiveShoppingCategory extends Model
{

    use SoftDeletes;

    protected $dates = [
        'air_dateable'
    ];

    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'live_shopping_categories';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function products()
    {
        return $this->hasMany(\App\Models\LiveShoppingTracker\LiveShoppingProduct::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function parent_category()
    {
        return self::find($this->parent_id);
    }

    public static function getCategories($selection, $languages)
    {
        if ($selection) {
            $categoriesToQuery = $selection;
        } else {
            $categoriesToQuery = self::whereNull('parent_id')->orderBy('name')->pluck('id')->toArray();
        }

        return $categoriesToQuery;
    }
}
