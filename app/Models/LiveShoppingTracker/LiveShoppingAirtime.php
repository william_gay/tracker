<?php namespace App\Models\LiveShoppingTracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class LiveShoppingAirtime extends Model
{

    use SoftDeletes;

    protected $dates = [
        'start_time', 'end_time'
    ];

    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'live_shopping_airtimes';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function products()
    {
        return $this->hasMany(\App\Models\LiveShoppingTracker\LiveShoppingAirtimeProduct::class);
    }

    public function productCount()
    {
        return $this->products()
            ->selectRaw('live_shopping_airtime_id, count(*) as num_products')
            ->groupBy('live_shopping_airtime_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function channel()
    {
        return $this->belongsTo(\App\Models\Channel::class, 'channel_id');
    }

    public function show()
    {
        return $this->belongsTo(\App\Models\LiveShoppingTracker\LiveShoppingShow::class, 'live_shopping_show_id');
    }
}
