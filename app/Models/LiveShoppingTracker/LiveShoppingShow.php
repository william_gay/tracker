<?php namespace App\Models\LiveShoppingTracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class LiveShoppingShow extends Model
{

    use SoftDeletes;

    protected $dates = [
        'first_airdate'
    ];

    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'live_shopping_shows';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function airtimes()
    {
        return $this->hasMany(\App\Models\LiveShoppingTracker\LiveShoppingAirtime::class);
    }

    public function channel()
    {
        return $this->belongsTo(\App\Models\Channel::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
