<?php namespace App\Models\LiveShoppingTracker;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class LiveShoppingProductVersion extends Model
{

    use SoftDeletes;

    protected $dates = [
        'first_airdate'
    ];

    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'live_shopping_product_versions';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function productAirtimes()
    {
        return $this->hasMany(\App\Models\LiveShoppingTracker\LiveShoppingAirtimeProduct::class, 'live_shopping_product_version_id');
    }

    public function product()
    {
        return $this->belongsTo(\App\Models\LiveShoppingTracker\LiveShoppingProduct::class, 'live_shopping_product_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
