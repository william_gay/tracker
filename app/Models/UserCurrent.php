<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCurrent extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_current';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
