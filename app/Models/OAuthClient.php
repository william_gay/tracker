<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OAuthClient extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'oauth_clients';

    public $timestamps = true;

    protected $fillable = ['name','secret'];

    public function clientEndpoint()
    {
        return $this->hasOne(
            \App\Models\OAuthClientEndpoint::class,
            'client_id',
            'id'
        );
    }
}
