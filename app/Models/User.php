<?php namespace App\Models;

use Eloquent;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;

class User extends \Cartalyst\Sentry\Users\Eloquent\User
{

    use Billable, Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * One-to-One Relationship
     *
     * @return hasOne
     */
    public function profile()
    {
        return $this->hasOne(\App\Models\UserProfile::class);
    }

    /**
     * Categories Many-to-Many Relationship
     *
     * @return belongsToMany
     */
    public function favoriteCategories()
    {
        return $this->belongsToMany(\App\Models\Category::class, 'user_favorite_categories', 'user_id', 'category_id');
    }

    /**
     * Products Many-to-Many Relationship
     *
     * @return belongsToMany
     */
    public function favoriteProducts()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'user_favorite_products', 'user_id', 'product_id');
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function groups()
    {
        return $this->belongsToMany(\App\Models\Group::class, 'users_groups', 'user_id', 'group_id');
    }

    public function hashes()
    {
        return $this->hasMany(\App\Models\UserHash::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(\App\Models\Subscription::class);
    }

    public function subscriptionsGracePeriod()
    {
        $subscriptions = $this->subscriptions;
        $subscriptionsGracePeriod = new \Illuminate\Database\Eloquent\Collection();

        foreach ($subscriptions as $subscription) {
            if ($subscription) {
                if ($subscription->isOnGracePeriod()) {
                    $subscriptionsGracePeriod->push($subscription);
                }
            }
        }

        return $subscriptionsGracePeriod;
    }

    public function hasSubscription($planName)
    {
        $planName = substr($planName, 0, 25);
        $subscription = \App\Models\Subscription::where('stripe_plan', $planName)->where('user_id', $this->id)->first();

        if ($subscription) {
            return $subscription;
        }

        return false;
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        return config('horizon.slack_webhook');
    }
}
