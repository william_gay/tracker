<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AirtimeVerify extends Model
{

    protected $dates = ['airdate'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'airtime_verifies';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public $timestamps = true;

    public function channel()
    {
        return $this->belongsTo(\App\Models\Channel::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
