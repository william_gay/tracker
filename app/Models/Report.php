<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{

    use SoftDeletes;

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reports';

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function language()
    {
        return $this->belongsTo(\App\Models\Language::class);
    }
}
