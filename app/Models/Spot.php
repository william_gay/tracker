<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Spot extends Model
{

    use SoftDeletes;

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'spots';

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function spotVersions()
    {
        return $this->hasMany(\App\Models\SpotVersion::class);
    }

    public function language()
    {
        return $this->belongsTo(\App\Models\Language::class);
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function subCategory()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function categoryReport()
    {
        return $this->belongsTo(\App\Models\CategoryReport::class);
    }

    public function product()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'product_spots');
    }
}
