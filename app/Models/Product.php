<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use SoftDeletes;
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;

    protected $guarded = ['id'];

    protected $dates = [
        'last_sf_airing',
        'last_lf_airing',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    public $timestamps = true;

    public static function findBySlug($slug)
    {
        return self::with(['programs', 'spots'])
            ->with(['retailerProducts' => function ($query) {
                $query->groupBy('retailer_id')
                    ->orderBy('max_price');
            }])
            ->where('slug', $slug)
            ->first();
    }

    public static function findByName($name)
    {
        return self::where('name', $name)->first();
    }

    public function scopeWinmo($query)
    {
        return $query->whereNotNull('winmo_company_id')
                ->orWhereNotNull('winmo_brand_id');
    }

    public function airings()
    {
        return $this->hasMany(\App\Models\ProductAiring::class)
            ->orderBy('date');
    }

    public function predictor()
    {
        return $this->hasMany(\App\Models\ProductPredictor::class);
    }

    public function channels()
    {
        return $this->belongsToMany(\App\Models\Channel::class)
            ->withPivot([
                'airings',
                'media_spend',
            ])
            ->withTimestamps();
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function subCategory()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function programs()
    {
        return $this->belongsToMany(\App\Models\Program::class, 'product_programs', 'product_id', 'program_id');
    }

    public function spots()
    {
        return $this->belongsToMany(\App\Models\Spot::class, 'product_spots', 'product_id', 'spot_id');
    }

    public function retailerProducts()
    {
        return $this->hasMany(\App\Models\Retail\RetailerProduct::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function getLastAiringAttribute()
    {
        if ($this->last_sf_airing->gte($this->last_sf_airing)) {
            return $this->last_sf_airing;
        }

        return $this->last_lf_airing;
    }

    public function getProgramVersionsAttribute()
    {
        return ProgramVersion::whereIn('program_id', $this->programs->pluck('id'))->get();
    }

    public function getSpotVersionsAttribute()
    {
        return SpotVersion::whereIn('spot_id', $this->spots->pluck('id'))->get();
    }
}
