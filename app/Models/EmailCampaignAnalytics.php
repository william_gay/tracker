<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class EmailCampaignAnalytics extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email_campaign_analytics';

    public $timestamps = true;

    protected $fillable = ['hash', 'slug', 'access', 'ip_address', 'ip_addresses', 'user_agent', 'device', 'platform', 'browser', 'languages'];

    public function userHash()
    {
        return $this->belongsTo(\App\Models\UserHash::class);
    }
}
