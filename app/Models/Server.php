<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'servers';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public $timestamps = true;

    public function createdBy()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function modifiedBy()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
