<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{

    use SoftDeletes;

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'manufacturers';

    public $timestamps = true;

    public static function findBySlug($slug)
    {
        return self::where('slug', $slug)->first();
    }

    public function products()
    {
        return $this->hasMany(\App\Models\Product::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
