<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class ProgramWeekly extends Model
{

    use SoftDeletes;

    protected $dates = [
        'week_ending'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'program_weekly';

    public $timestamps = true;

    public function scopeWeekending($query, \Carbon\Carbon $date)
    {
        return $query->where('week_ending', $date->toDateString());
    }

    public static function findHighestRankByProgramId($programId)
    {
        $programRanking = self::where('program_id', $programId)
            ->orderBy('freq_rank', 'asc')
            ->first();

        if ($programRanking) {
            return $programRanking->freq_rank;
        } else {
            return 'N/A';
        }
    }

    public static function findWeeksRankedByProgramId($programId)
    {
        return self::where('program_id', $programId)->count();
    }

    public function program()
    {
        return $this->belongsTo(\App\Models\Program::class);
    }
}
