<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupsProfile extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups_profile';

    protected $dates = ['data_since', 'data_to'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function group()
    {
        return $this->belongsTo(\App\Models\Group::class);
    }
}
