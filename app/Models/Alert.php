<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'alerts';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $softDelete = false;

    public $timestamps = true;

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'type_extra', 'id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
