<?php namespace App\Models\Autodetection;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    protected $dates = ['segment_time', 'start_time', 'end_time'];

    protected $connection = 'autodetection';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'logs';

    public $timestamps = true;
}
