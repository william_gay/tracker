<?php namespace App\Models\Autodetection;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Clipster extends Model
{

    use SoftDeletes;

    protected $dates = ['date_entered'];

    protected $fillable = ['filename'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clipsters';

    public $timestamps = true;

    public function clipsterable()
    {
        return $this->morphTo();
    }
}
