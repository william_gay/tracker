<?php namespace App\Models\Autodetection;

use Illuminate\Database\Eloquent\Model;

class Exception extends Model
{

    protected $dates = ['date_entered'];

    protected $connection = 'autodetection';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'exceptions';

    public $timestamps = false;
}
