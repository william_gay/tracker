<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CostMapping extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cost_mappings';

    public $timestamps = true; //No timestamps on this one

    protected $fillable = ['cost_id', 'im2_cost_id'];
}
