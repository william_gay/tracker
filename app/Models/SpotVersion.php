<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class SpotVersion extends Model
{

    use SoftDeletes;

    protected $dates = [
        'air_date'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'spot_versions';

    public $timestamps = true;

    public static function findByWeek($week, $language_id = 1)
    {
        return self::with('category', 'subCategory', 'marketingCompany')
            ->where('air_date', $week->format("Y-m-d"))
            ->where('language_id', $language_id)
            ->where('brand_advert', 0) // Hide Brand Advertising
            ->orderBy('title')
            ->get();
    }

    public static function findBySpotId($spotId)
    {
        return self::where('spot_id', $spotId);
    }

    public static function findLatestBySpotId($spotId)
    {
        return self::where('spot_id', $spotId)
            ->orderBy('spot_versions.air_date', 'desc')
            ->first();
    }

    public function airtimes()
    {
        return $this->hasMany(\App\Models\SpotAirtime::class);
    }

    public function spot()
    {
        return $this->belongsTo(\App\Models\Spot::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function language()
    {
        return $this->belongsTo(\App\Models\Language::class);
    }

    public function channel()
    {
        return $this->belongsTo(\App\Models\Channel::class, 'channel_id');
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function subCategory()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function marketingCompany()
    {
        return $this->belongsTo(\App\Models\Company::class);
    }

    public function clipsters()
    {
        return $this->morphMany(\App\Models\Autodetection\Clipster::class, 'clipsterable');
    }
}
