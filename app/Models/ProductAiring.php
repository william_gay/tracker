<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAiring extends Model
{

    protected $guarded = ['id'];
    protected $dates = ['dates'];
    protected $table = 'product_airings';

    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class);
    }
}
