<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daypart extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'day_parts';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public static function getDayPart($datetime)
    {
        $daypart = Daypart::where(function ($query) use ($datetime) {
                $query->where('day_start', '<=', date('N', strtotime($datetime->format("Y-m-d H:i:s"))))
                    ->where('day_end', '>=', date('N', strtotime($datetime->format("Y-m-d H:i:s"))));
        })
            ->where(function ($query) use ($datetime) {
                $query->where('time_start', '<=', $datetime->format("H:i:s"))
                    ->where('time_end', '>', $datetime->format("H:i:s"));
            })
            ->first();

        if ($daypart) {
            return $daypart->id;
        } else {
            //The logic above is fucked up because Late Fringe Wraps around... 23:35 to 02:00
            return 9;
        }
    }
}
