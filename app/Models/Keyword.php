<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'keywords';

    public $timestamps = true;

    protected $fillable = ['keyword'];

    public static function processKeywords($keywords)
    {
        $words = explode(',', $keywords);

        foreach ($words as $word) {
            self::firstOrCreate(['keyword' => $word]);
        }
    }
}
