<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{

    use SoftDeletes;

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'costs';

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function channel()
    {
        return $this->belongsTo(\App\Models\Channel::class);
    }

    public static function getCostByChannelAndTime($channel_id, $time)
    {
        $air_date = Carbon::createFromFormat('Y-m-d H:i:s', $time);

        if ($air_date->dayOfWeek == 0) {
            $dayOfWeek = 7;
        } else {
            $dayOfWeek = $air_date->dayOfWeek;
        }

        $cost = Cost::where('archive', 0)
            ->where('channel_id', $channel_id)
            ->where('day', '<=', $dayOfWeek)
            ->where('time_slot', '<=', $air_date->format('H:i:s'))
            ->orderBy('time_slot', 'desc')
            ->orderBy('day', 'desc')
            ->first();

        if ($cost) {
            return $cost->amount;
        } else {
            return 0;
        }
    }
}
