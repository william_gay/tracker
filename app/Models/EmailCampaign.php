<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class EmailCampaign extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'email_campaigns';

    public function userHashes()
    {
        return $this->hasMany(\App\Models\UserHash::class);
    }

    public function createNewEmailCampaign($period_type, $notes)
    {
        $campaign              = new EmailCampaign;
        $campaign->period_type = $period_type;
        $campaign->notes       = $notes;
        $campaign->approved    = 0;
        $campaign->save();

        return $campaign;
    }
}
