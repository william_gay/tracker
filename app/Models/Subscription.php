<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use SoftDeletes, Billable;

    protected $dates = [
        'start_date', 'end_date'
    ];

    protected $fillable = ['user_id', 'stripe_active', 'stripe_id', 'stripe_subscription', 'stripe_plan', 'last_four', 'trial_ends_at', 'subscription_ends_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subscriptions';

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\Models\user');
    }

    public function isOnGracePeriod()
    {
        if (! is_null($endsAt = $this->getSubscriptionEndDate())) {
            return Carbon::now()->lt(Carbon::instance(new \DateTime($endsAt)));
        } else {
            return false;
        }
    }
}
