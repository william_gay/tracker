<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class SpotAirtime extends Model
{

    use SoftDeletes;

    protected $dates = [
        'updated_at', 'air_dateable'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'spot_airtimes';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $softDelete = true;

    public static function findCountBySpotVersions($versions)
    {
        if (count($versions) == 0) {
            return 0;
        }

        return self::whereIn('spot_version_id', $versions)->count();
    }

    public function channel()
    {
        return $this->belongsTo(\App\Models\Channel::class);
    }

    public function spotVersion()
    {
        return $this->belongsTo(\App\Models\SpotVersion::class);
    }

    public function costs()
    {
        return $this->belongsTo(\App\Models\Cost::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function daypart()
    {
        return $this->belongsTo(\App\Models\Daypart::class, 'daypart_id');
    }
}
