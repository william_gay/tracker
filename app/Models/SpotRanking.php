<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class SpotRanking extends Model
{

    use SoftDeletes;

    protected $dates = [
        'week_ending'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'spot_rankings';

    public $timestamps = true;

    public function scopeRankdate($query, \Carbon\Carbon $date)
    {
        return $query->where('rank_date', $date->toDateString());
    }

    public static function findHighestRankBySpotId($spotId)
    {
        $spotRanking = self::where('spot_id', $spotId)
            ->orderBy('rank', 'asc')
            ->first();

        if ($spotRanking) {
            return $spotRanking->rank;
        } else {
            return 'N/A';
        }
    }

    public static function findWeeksRankedBySpotId($spotId)
    {
        return self::where('spot_id', $spotId)->count();
    }

    public function spot()
    {
        return $this->belongsTo(\App\Models\Spot::class);
    }

    public function spotVersion()
    {
        return $this->hasOne(\App\Models\SpotVersion::class, 'id', 'spot_version_id');
    }

    public function channels()
    {
        return $this->belongsToMany(\App\Models\Channel::class, 'spot_detections_channels', 'spot_detection_id', 'channel_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
