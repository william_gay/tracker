<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Sentry;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    use SoftDeletes;
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;

    protected $appends = ['graph_color'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'channels';

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function language()
    {
        return $this->belongsTo(\App\Models\Language::class);
    }

    public function airtimes()
    {
        return $this->hasMany(\App\Models\Airtime::class);
    }

    public function costs()
    {
        return $this->hasMany(\App\Models\Cost::class);
    }

    public function server()
    {
        return $this->belongsTo(\App\Models\Server::class);
    }

    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class)
            ->withTimestamps();
    }

    public function scopeActive($query)
    {
        return $query->where('capturing', 1);
    }

    public function getGraphColorAttribute()
    {
        // this will be generated differently with dynamic channel selection
        if ($this->id == 208) {
            return '#0099FF';
        } elseif ($this->id == 209) {
            return '#FF0000';
        } else {
            return '#FF9900';
        }
    }

    public static function getChannelSelection($selection)
    {

        if ($selection) {
            $start = Carbon::createFromFormat("F j, Y", $start);
            $end = Carbon::createFromFormat("F j, Y", $end);
        } else {
            $selection = Channel::where('language_id', 1)->pluck('name', 'id')->toArray();
        }

        $dayDiff = $end->diffInDays($start);
        $prevStart = $start->copy()->subDays($dayDiff);
        $prevEnd = $start->copy();

        return [
            'start' => $start->startOfDay(),
            'end' => $end->endOfDay(),
            'prevStart' => $prevStart->startOfDay(),
            'prevEnd' => $prevEnd->endOfDay()
        ];
    }

    public static function getChannels($selection, $languages)
    {
        if ($selection != null) {
            $allowedLanguage[] = 1; // Everyone has National
            if (Sentry::check() and Sentry::getUser()->hasAccess('reports.spanish-shows')) {
                $allowedLanguage[] = 2; // some people have spanish
            }
            if (Sentry::check() and Sentry::getUser()->hasAccess('reports.australia')) {
                $allowedLanguage[] = 4; // some people have australia
            }

            $channelsToQuery = Channel::whereIn('language_id', $allowedLanguage)
                ->pluck('id')
                ->toArray();
        }

        if ($selection) {
            $channelsToQuery = $selection;
        } else {
            // Exclude WIZE by default
            //$channelsToQuery = Channel::where('language_id', $languages)->where('id', '!=', 78)->pluck('id');
            // Give them ALL!
            $channelsToQuery = Channel::where('language_id', $languages)->pluck('id')->toArray();
        }

        return $channelsToQuery;
    }
}
