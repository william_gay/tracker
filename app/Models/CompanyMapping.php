<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyMapping extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company_mappings';

    protected $fillable = ['company_id', 'im2_company_id','spot_mk_company_id','spot_fl_company_id'];

    public $timestamps = true; //No timestamps on this one
}
