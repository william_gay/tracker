<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChannelProduct extends Model
{
    protected $guarded = ['id'];

    protected $table = 'channel_product';
}
