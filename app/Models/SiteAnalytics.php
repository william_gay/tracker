<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiteAnalytics extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'site_analytics';

    public $timestamps = true;

    protected $fillable = ['type', 'type_id','user_id'];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
