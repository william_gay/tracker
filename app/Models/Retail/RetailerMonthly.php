<?php namespace App\Models\Retail;

use Sentry;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class RetailerMonthly extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'retail_monthly';

    use SoftDeletes;
    protected $dates = [
        'month'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public $timestamps = true;

    public static function findMonthsByActiveReports()
    {
        $reports = self::orderBy('month', 'desc')
            ->where(function ($query) {
                if (! Sentry::isSuperUser()) {
                    $query->where('active', 1);
                }
            })
            ->get();

        $monthSelect = [];
        foreach ($reports as $report) {
            $monthSelect[$report->id] = $report->month->startOfMonth()->format('m/d/Y');
        }

        return $monthSelect;
    }

    public static function findReportByRequest($input)
    {
        if (isset($input['report'])) {
            $dt = Carbon::parse('01 '. $input['report']);

            $report = RetailerMonthly::where('month', $dt)->first();
        } else {
            // If no months are selected, get the most recent month
            $report = RetailerMonthly::where(
                function ($query) {
                    if (! Sentry::isSuperUser()) {
                        $query->where('active', 1);
                    }
                }
            )
            ->orderBy('month', 'desc')->firstOrFail();
        }

        if ($report == null) {
            $report = RetailerMonthly::where('id', 1)->first();
        }

        return $report;
    }

    public static function prettifyDate($input)
    {
        $dt = Carbon::parse($input->month);
        $month = $dt->month;

        switch ($month) {
            case 1:
                $month = "January";
                break;
            case 2:
                $month = "February";
                break;
            case 3:
                $month = "March";
                break;
            case 4:
                $month = "April";
                break;
            case 5:
                $month = "May";
                break;
            case 6:
                $month = "June";
                break;
            case 7:
                $month = "July";
                break;
            case 8:
                $month = "August";
                break;
            case 9:
                $month = "September";
                break;
            case 10:
                $month = "October";
                break;
            case 11:
                $month = "November";
                break;
            case 12:
                $month = "December";
                break;
        }

        $year = $dt->year;

        return $month.' '.$year;
    }

    public static function getEndMonth()
    {
        $endMonth = RetailerMonthly::orderBy('month', 'desc')->first();

        return $endMonth;
    }

    public static function findByMonth($month)
    {
        return self::where('month', $month)->first();
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
