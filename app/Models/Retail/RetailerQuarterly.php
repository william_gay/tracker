<?php namespace App\Models\Retail;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class RetailerQuarterly extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'retail_quarterly';


    use SoftDeletes;
    protected $dates = [
        'quarter'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
