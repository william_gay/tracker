<?php namespace App\Models\Retail;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Retail\RetailerProduct;
use League\Url\Url;

use Illuminate\Database\Eloquent\Model;

class RetailerProduct extends Model
{

    use SoftDeletes;

    protected $dates = [
        'date_recorded'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'retailer_products';

    public $timestamps = true;

    public static function findByProductAndStoreName($productId, $retailerName)
    {
        return self::where('product_id', $productId)
            ->where('retailers.name', $retailerName)
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->orderBy('date_recorded', 'desc')
            ->first();
    }

    public static function findByProductAndUrl($productId, $url)
    {
        $lurl = Url::createFromUrl($url);

        return self::where('product_id', $productId)
            ->where('retailers.website', 'LIKE', '%'.$lurl->getHost().'%')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->orderBy('date_recorded', 'desc')
            ->first();
    }

    public static function findByRetailerId($retailerId)
    {
        return self::where('retailer_id', $retailerId)
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->orderBy('name')
            ->get();
    }

    public static function findByRetailerIdAndMonth($retailerId, $month)
    {
        return self::select(
            'retailer_products.id',
            'products.name',
            'products.slug',
            'categories.name as category_name',
            'retailer_products.website',
            'retailer_products.min_price',
            'retailer_products.max_price',
            'program_versions.price as program_price',
            'spot_versions.price as spot_price'
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->leftJoin('product_programs', 'product_programs.product_id', '=', 'products.id')
            ->leftJoin('program_versions', 'program_versions.program_id', '=', 'product_programs.program_id')
            ->leftJoin('product_spots', 'product_spots.product_id', '=', 'products.id')
            ->leftJoin('spot_versions', 'spot_versions.spot_id', '=', 'product_spots.spot_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->where('retailer_id', $retailerId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->groupBy('retailer_products.product_id')
            ->orderBy('name')
            ->get();
    }

    public static function findProductsByCategoryAndMonth($categoryId, $month)
    {
        return self::select(
            'products.id as product_id',
            'products.slug as product_slug',
            'products.name as product_name',
            DB::raw('GROUP_CONCAT(product_programs.program_id) as programs'),
            DB::raw('GROUP_CONCAT(product_spots.spot_id) as spots'),
            DB::raw('COUNT(DISTINCT retailer_products.retailer_id) as retailer_count'),
            DB::raw('MIN(retailer_products.min_price) as min_price'),
            DB::raw('MAX(retailer_products.max_price) as max_price'),
            DB::raw('AVG(retailer_products.min_price) as avg_price')
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('product_programs', 'products.id', '=', 'product_programs.product_id')
            ->leftJoin('product_spots', 'products.id', '=', 'product_spots.product_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->where('products.category_id', $categoryId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->groupBy('retailer_products.product_id')
            ->orderBy('retailer_count', 'desc')
            ->get();
    }

    public static function findLowPriceByCategoryAndMonth($categoryId, $month)
    {
        return self::select(
            DB::raw('MIN(retailer_products.min_price) as min_price')
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->where('products.category_id', $categoryId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->first();
    }

    public static function findHighPriceByCategoryAndMonth($categoryId, $month)
    {
        return self::select(
            DB::raw('MAX(retailer_products.max_price) as max_price')
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->where('products.category_id', $categoryId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->first();
    }

    public static function findLowPriceByProductAndMonth($productId, $month)
    {
        return self::select(
            DB::raw('MIN(retailer_products.min_price) as min_price')
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->where('products.id', $productId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->first();
    }

    public static function findHighPriceByProductAndMonth($productId, $month)
    {
        return self::select(
            DB::raw('MAX(retailer_products.max_price) as max_price')
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->where('products.id', $productId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->first();
    }

    public static function findRetailersByCategoryAndMonth($categoryId, $month)
    {
        return self::select(
            'retailers.id as retailer_id',
            'retailers.name as retailer_name',
            'retailers.slug as retailer_slug',
            'retailers.sqft',
            'retailers.website as retailer_website',
            DB::raw('GROUP_CONCAT(product_programs.program_id) as programs'),
            DB::raw('GROUP_CONCAT(product_spots.spot_id) as spots'),
            DB::raw('MIN(retailer_products.min_price) as min_price'),
            DB::raw('MAX(retailer_products.max_price) as max_price'),
            DB::raw('COUNT(DISTINCT retailer_products.product_id) as product_count'),
            DB::raw('COUNT(retailer_products.product_id) as total_product_count'),
            DB::raw('AVG(retailer_products.min_price) as avg_price')
        )
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->leftJoin('product_programs', 'products.id', '=', 'product_programs.product_id')
            ->leftJoin('product_spots', 'products.id', '=', 'product_spots.product_id')
            ->where('retailers.visible', 1)
            ->where('products.category_id', $categoryId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->whereNull('products.deleted_at')
            ->whereNull('retailers.deleted_at')
            ->whereNull('retailer_products.deleted_at')
            ->groupBy('retailer_products.retailer_id')
            ->orderBy('product_count', 'desc')
            ->get();
    }

    public static function findRetailersByCategoryAndMonthForGraph($categoryId, $month)
    {
        return self::select(
            'retailers.name as retailer',
            DB::raw('(retailers.sqft/10000) as sqft'),
            DB::raw('COUNT(DISTINCT retailer_products.product_id) as product_count')
        )
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->where('products.category_id', $categoryId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->where('retailers.visible', 1)
            ->groupBy('retailer_products.retailer_id')
            ->orderBy('product_count', 'desc')
            ->take(10)
            ->get();
    }

    public static function findRetailersByProductAndMonth($productId, $month)
    {
        return self::select(
            'retailers.id as retailer_id',
            'retailers.name as retailer_name',
            'retailers.slug as retailer_slug',
            'retailers.store_count',
            'retailers.sqft',
            'retailers.website as retailer_website',
            'retailer_products.website as retailer_product_website',
            'retailer_products.date_recorded',
            DB::raw('MIN(retailer_products.min_price) as min_price'),
            DB::raw('MAX(retailer_products.max_price) as max_price'),
            DB::raw('COUNT(DISTINCT retailer_products.retailer_id) as retailer_count'),
            DB::raw('AVG(retailer_products.min_price) as avg_price'),
            DB::raw('(SELECT count(retailer_products.id) FROM retailer_products
                        LEFT JOIN products on products.id = retailer_products.product_id
                        WHERE retailer_id = retailers.id
                        AND products.category_id = categories.id) as related_count')
        )
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->where('retailers.visible', 1)
            ->where('products.id', $productId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->groupBy('retailer_products.retailer_id')
            ->orderBy('retailer_count', 'desc')
            ->get();
    }

    public static function findRetailersByProductAndMonthForGraph($productId, $month)
    {
        return self::select(
            'retailers.name as store',
            DB::raw('(SELECT count(retailer_products.id) FROM retailer_products
                        LEFT JOIN products on products.id = retailer_products.product_id
                        WHERE retailer_id = retailers.id
                        AND products.category_id = categories.id) as related'),
            DB::raw('MIN(retailer_products.min_price) as price'),
            DB::raw('COUNT(retailer_products.retailer_id) as retailer_count')
        )
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->where('retailers.visible', 1)
            ->where('products.id', $productId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->groupBy('retailer_products.retailer_id')
            ->orderBy('retailer_count', 'desc')
            ->take(10)
            ->get();
    }

    public static function findCategoryByRetailerIdAndMonth($retailerId, $month)
    {
        return self::select(
            'categories.id as category_id',
            'categories.name as category_name',
            DB::raw('COUNT(DISTINCT retailer_products.product_id) as product_count'),
            DB::raw('AVG(retailer_products.min_price) as avg_price')
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->where('retailer_id', $retailerId)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->groupBy('products.category_id')
            ->orderBy('product_count', 'desc')
            ->get();
    }

    public static function findCategoryByMonth($month)
    {
        return self::select(
            'categories.id as category_id',
            'categories.name as category_name',
            DB::raw('COUNT(DISTINCT retailer_products.product_id) as product_count'),
            DB::raw('AVG(retailer_products.min_price) as avg_price')
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->groupBy('products.category_id')
            ->orderBy('product_count', 'desc')
            ->get();
    }

    public static function findCategoryByMonthWithLimit($month, $limit)
    {
        return self::select(
            'categories.id as category_id',
            'categories.name as category_name',
            DB::raw('COUNT(DISTINCT retailer_products.retailer_id) as retailer_count'),
            DB::raw('COUNT(DISTINCT retailer_products.product_id) as product_count'),
            DB::raw('AVG(retailer_products.min_price) as avg_price')
        )
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->groupBy('products.category_id')
            ->orderBy('product_count', 'desc')
            ->take($limit)
            ->get();
    }

    public static function findRetailersByMonthWithLimit($month, $limit)
    {
        return self::select(
            'retailers.id as retailer_id',
            'retailers.name as retailer_name',
            'retailers.slug as retailer_slug',
            'retailers.sqft',
            'retailers.website as retailer_website',
            DB::raw('COUNT(DISTINCT products.category_id) as category_count'),
            DB::raw('COUNT(DISTINCT retailer_products.product_id) as product_count'),
            DB::raw('AVG(retailer_products.min_price) as avg_price')
        )
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->leftJoin('product_programs', 'products.id', '=', 'product_programs.product_id')
            ->leftJoin('product_spots', 'products.id', '=', 'product_spots.product_id')
            ->where('retailers.visible', 1)
            ->groupBy('retailer_products.retailer_id')
            ->orderBy('product_count', 'desc')
            ->take($limit)
            ->get();
    }

    public static function findProductsByMonthWithLimit($month, $limit)
    {
        return self::select(
            'products.id as product_id',
            'products.slug as product_slug',
            'products.name as product_name',
            DB::raw('GROUP_CONCAT(product_programs.program_id) as programs'),
            DB::raw('GROUP_CONCAT(product_spots.spot_id) as spots'),
            DB::raw('COUNT(DISTINCT retailer_products.retailer_id) as retailer_count'),
            DB::raw('MIN(retailer_products.min_price) as min_price'),
            DB::raw('MAX(retailer_products.max_price) as max_price'),
            DB::raw('AVG(retailer_products.min_price) as avg_price')
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('product_programs', 'products.id', '=', 'product_programs.product_id')
            ->leftJoin('product_spots', 'products.id', '=', 'product_spots.product_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->groupBy('retailer_products.product_id')
            ->orderBy('retailer_count', 'desc')
            ->take($limit)
            ->get();
    }

    public static function findNewProductsByCategory($month, $limit)
    {
        /*
            TODO Make sure we figure out if they are new or not....
            might be able to use products.created_at
        */

        return self::select(
            'categories.name as category_name',
            DB::raw('COUNT(DISTINCT retailer_products.product_id) as product_count')
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->groupBy('products.category_id')
            ->orderBy('product_count', 'desc')
            ->take($limit)
            ->get();
    }

    public static function findNewProductsByRetailer($month, $limit)
    {
        /*
            TODO Make sure we figure out if they are new or not....
            might be able to use products.created_at
        */

        return self::select(
            'retailers.name as retailer_name',
            DB::raw('COUNT(DISTINCT retailer_products.product_id) as product_count')
        )
            ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->groupBy('retailer_products.retailer_id')
            ->orderBy('product_count', 'desc')
            ->take($limit)
            ->get();
    }

    public static function findMonthsByRetailer($retailerId)
    {
        $months = self::where('retailer_id', $retailerId)
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->groupBy(DB::raw('MONTH("date_recorded")'))
            ->orderBy('date_recorded')
            ->get();

        $monthSelect = [];
        foreach ($months as $month) {
            $monthSelect[$month->date_recorded->startOfMonth()->format('Y-m-d')] = $month->date_recorded->startOfMonth()->format('m/d/Y');
        }

        return $monthSelect;
    }

    public static function findMonths()
    {
        $months = self::groupBy(DB::raw('MONTH("date_recorded")'))
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->orderBy('date_recorded')
            ->get();

        $monthSelect = [];
        foreach ($months as $month) {
            $monthSelect[$month->date_recorded->startOfMonth()->format('Y-m-d')] = $month->date_recorded->startOfMonth()->format('m/d/Y');
        }

        return $monthSelect;
    }

    public static function findMonthsByCategory($categoryId)
    {
        $months = self::leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->where('products.category_id', $categoryId)
            ->groupBy(DB::raw('MONTH("date_recorded")'))
            ->orderBy('date_recorded')
            ->get();

        $monthSelect = [];
        foreach ($months as $month) {
            $monthSelect[$month->date_recorded->startOfMonth()->format('Y-m-d')] = $month->date_recorded->startOfMonth()->format('m/d/Y');
        }

        return $monthSelect;
    }

    public static function findByProductId($productId)
    {
        return self::where('product_id', $productId)->get();
    }

    public static function findByRetailerAndProduct($args)
    {
        return self::leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->where('retailers.visible', 1)
            ->where('product_id', $args['product_id'])
            ->where('retailer_id', $args['retailer_id'])
            ->orderBy('name')->get();
    }

    public function product()
    {
        return $this->hasOne(\App\Models\Product::class, 'id', 'product_id');
    }

    public function retailer()
    {
        return $this->hasOne(\App\Models\Retail\Retailer::class, 'id', 'retailer_id');
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function subCategory()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
