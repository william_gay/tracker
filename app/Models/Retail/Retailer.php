<?php namespace App\Models\Retail;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{

    use SoftDeletes;

    

    protected $fillable = ['name'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'retailers';

    public $timestamps = true;

    public static function findBySlug($slug)
    {
        return self::where('slug', $slug)->first();
    }

    public static function findByName($name)
    {
        return self::where('name', $name)->first();
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
