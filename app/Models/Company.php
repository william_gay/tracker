<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    use SoftDeletes;

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    public $timestamps = true;

    public function marketingProgramVersions()
    {
        return $this->hasMany(\App\Models\ProgramVersion::class, 'marketing_company_id');
    }

    public function productionProgramVersions()
    {
        return $this->hasMany(\App\Models\ProgramVersion::class, 'production_company_id');
    }

    public function spotVersions()
    {
        return $this->hasMany(\App\Models\SpotVersion::class, 'marketing_company_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function types()
    {
        return $this->hasMany(\App\Models\CompanyType::class);
    }
}
