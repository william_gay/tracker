<?php namespace App\Models;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class SpotCost extends Model
{

    use SoftDeletes;



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'spot_costs';

    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function channel()
    {
        return $this->belongsTo(\App\Models\Channel::class);
    }

    public function daypart()
    {
        return $this->belongsTo(\App\Models\Daypart::class, 'daypart_id');
    }

    public static function getCostByChannelAndTime($channel_id, $time, $duration)
    {
        $air_date = Carbon::createFromFormat('Y-m-d H:i:s', $time);
        $daypart_id = Daypart::getDayPart($air_date);

        $cost = SpotCost::where('channel_id', $channel_id)
            ->where('daypart_id', $daypart_id)
            ->where('time_slot', '<=', $air_date->format('H:i:s'))
            ->orderBy('time_slot', 'desc')
            ->orderBy('time_slot', 'desc')
            ->first();


        if ($cost) {
            $multiplier = $duration / 30;
            // 15 = amount * .5;
            // 30 =  amount;
            // 45 = amount * 1.5;
            // 60 = amount * 2;

            return $cost->amount * $multiplier;
        } else {
            return 0;
        }
    }
}
