<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','permissions'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Mutator for taking name
     *
     * @param  string $value
     * @return string
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucfirst($value);
    }

    /**
     * Mutator for taking permissions.
     *
     * @param  array $permissions_arr
     * @return string
     */
    public function setPermissionsAttribute($permissions)
    {
        $module = lcfirst($this->attributes['name']);
        //prefix the permission with the module name ex: user.create

        $roles = [];
        foreach ($permissions as $key => $value) {
            $roles[] = $module . '.' . $value;
        }

        $this->attributes['permissions'] = ( ! empty($roles)) ? json_encode($roles) : '';
    }

    /**
     * Mutator for giving permissions.
     *
     * @param  mixed  $permissions
     * @return array  $_permissions
     */
    public function getPermissionsAttribute($permissions)
    {
        if (! $permissions) {
            return [];
        }

        if (is_array($permissions)) {
            return $permissions;
        }

        if (! $_permissions = json_decode($permissions, true)) {
            throw new \InvalidArgumentException("Cannot JSON decode permissions [$permissions].");
        }

        return $_permissions;
    }

    /**
     * Format rules for checkbox in form
     *
     * @return array
     */
    public function getRulesAttribute()
    {
        $roles = [];
        foreach ($this->permissions as $role) {
            list($module, $rule) = explode('.', $role);
            $roles[] = $rule;
        }
        return $roles;
    }
}
