<?php namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Sentry;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'languages';

    public $timestamps = true;

    protected $fillable = ['name', 'abbr'];

    public static function getMarkets($selection)
    {
        if ($selection) {
            $marketSelection = $selection;
        } else {
            $marketSelection = Language::where('id', '=', '1')->first()->id;
        }

        if ($marketSelection == 2 and Sentry::getUser()->hasAccess('reports.spanish-shows')) {
            return $marketSelection;
        }

        if ($marketSelection == 4 and Sentry::getUser()->hasAccess('reports.australia')) {
            return $marketSelection;
        }

        return $marketSelection;
    }

    public static function marketFormPopulator()
    {

        if (Sentry::getUser()->hasAccess('reports.national-shows') and
            Sentry::getUser()->hasAccess('reports.spanish-shows') and
            Sentry::getUser()->hasAccess('reports.australia')) {
            $marketFormPopulator = Language::orderBy('name', 'asc')->pluck('name', 'id')->toArray();

            return $marketFormPopulator;
        }

        if (Sentry::getUser()->hasAccess('reports.national-shows') and
            Sentry::getUser()->hasAccess('reports.spanish-shows')) {
            $marketFormPopulator = Language::whereIn('id', [1, 2])->orderBy('name', 'asc')->pluck('name', 'id')->toArray();

            return $marketFormPopulator;
        }


        if (Sentry::getUser()->hasAccess('reports.national-shows') and
            Sentry::getUser()->hasAccess('reports.australia')) {
            $marketFormPopulator = Language::whereIn('id', [1, 4])->orderBy('name', 'asc')->pluck('name', 'id')->toArray();

            return $marketFormPopulator;
        }

        if (Sentry::getUser()->hasAccess('reports.national-shows')) {
            $marketFormPopulator = Language::where('id', 1)->pluck('name', 'id')->toArray();

            return $marketFormPopulator;
        }
    }

    public function channel()
    {
        return $this->hasMany(\App\Models\Channel::class);
    }

    public function program()
    {
        return $this->hasMany(\App\Models\Program::class);
    }

    public function programVersion()
    {
        return $this->belongsTo(\App\Models\ProgramVersion::class);
    }

    public function spot()
    {
        return $this->hasMany(\App\Models\Spot::class);
    }

    public function spotVersion()
    {
        return $this->hasMany(\App\Models\SpotVersion::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }
}
