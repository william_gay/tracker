<?php

if (! function_exists('subscription_welcome')) {
    /**
     * Generate a welcome message to subscription users.
     *
     * @return string
     */
    function subscription_welcome()
    {
        $user = Sentry::getUser();
        $subscriptions = \App\Models\Group::where('name', 'like', '%Subscription%')->get()->pluck('name', 'id');
        $groups = $user->getGroups()->pluck('name', 'id');

        $subscription = $subscriptions->only($groups);

        if ($subscription->count() > 0) {
            if ($subscription->filled(4)) {
                return 'You are currently signed up to the '.$subscription->first().'.';
            } else {
                return 'You are currently signed up to the '.$subscription->first().'. '.link_to_route('upgrade', 'Upgrade your plan today').'.';
            }
        } else {
            return;
        }
    }
}

if (! function_exists('subscription_profile')) {
    /**
     * Generate a welcome message to subscription users.
     *
     * @return array
     */
    function subscription_profile()
    {
        $user = Sentry::getUser();
        $groups = $user->getGroups()->pluck('name', 'id');
        $lastFriday = new Carbon\Carbon('last friday');
        $groupProfileId = 0;

        if (! $groups) {
            return abort('401', 'Access denied - You need a subscription to view this site.');
        }

        foreach ($groups as $id => $group) {
            if (strpos($group, 'Subscription') === false) {
                $groupProfileId = $id;
            }
        }

        $groupProfile = \App\Models\GroupsProfile::where('group_id', $groupProfileId)->first();
        if ($groupProfile) {
            $groupProfile = $groupProfile->toArray();

            return $groupProfile;
        }

        return [];
    }
}

if (! function_exists('alerts_configured')) {
    function alerts_configured()
    {
        $alerts = App\Models\Alert::where('user_id', Sentry::getUser()->id)->get();

        return $alerts->count();
    }
}

if (! function_exists('data_since')) {
    /**
     * Data they can see since
     *
     * @return datetime
     */
    function data_since()
    {
        $user = Sentry::getUser();
        $groups = $user->getGroups()->pluck('name', 'id')->toArray();

        $lastFriday = new Carbon\Carbon('last friday');

        if (! $groups) {
            return abort('401', 'Access denied - You need a subscription to view this site.');
        }

        foreach ($groups as $id => $group) {
            if (strpos($group, 'Subscription') === false) {
                $groupProfileId = $id;
            }
        }

        $groupProfile = App\Models\GroupsProfile::where('group_id', $id)->first()->toArray();

        return Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $groupProfile['data_since']);
    }
}

if (! function_exists('data_to')) {
    /**
     * Data they can see to
     *
     * @return datetime
     */
    function data_to()
    {
        $user = Sentry::getUser();
        $groups = $user->getGroups()->pluck('name', 'id');

        if (! $groups) {
            return abort('401', 'Access denied - You need a subscription to view this site.');
        }

        foreach ($groups as $id => $group) {
            if (strpos($group, 'Subscription') === false) {
                $groupProfileId = $id;
            }
        }

        $groupProfile = App\Models\GroupsProfile::where('group_id', $id)->first()->toArray();

        return Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $groupProfile['data_to']);
    }
}

if (! function_exists('data_allowed')) {
    /**
     * Check to see if user can see that data
     *
     * @return array
     */
    function data_allowed($request_date)
    {
        $start_date = data_since();
        $end_date = data_to();

        $videoSub = Sentry::findGroupByName('Video Subscription');

        // Find Video Subscription and over ride data range
        // Video subscription only can see current week
        if (Sentry::getUser()->inGroup($videoSub)) {
            if ($request_date->eq(lastFriday())) {
                return true;
            } else {
                return false;
            }
        } else {
            if ($start_date->lt($request_date) and $end_date->gt($request_date)) {
                return true;
            } else {
                return false;
            }
        }

        //Failsafe
        return false;
    }
}

if (! function_exists('getYears')) {
    /**
     * Return array of Years
     *
     * @return array
     */
    function getYears()
    {
        $start = data_since();
        $now = Carbon\Carbon::now()->startOfYear();
        $month_arr = [];
        while ($now > $start) {
            $month_arr[$now->format("Y")] = $now->format("Y");
            $now->subYear();
        }

        return $month_arr;
    }
}

if (! function_exists('getMonths')) {
    /**
     * Return array of Months
     *
     * @return array
     */
    function getMonths()
    {
        $start = data_since();
        $now = Carbon\Carbon::now();
        $month_arr = [];
        while ($now > $start) {
            $month_arr[$now->format("Y-m")] = $now->format("F Y");
            $now->subMonth();
        }

        return $month_arr;
    }
}

if (! function_exists('monthRange')) {
    /**
     * Return array of Beginning date and Ending date
     *
     * @return array
     */
    function monthRange($yearMonth = null)
    {
        if ($yearMonth == null) {
            $yearMonth = date("Y-m");
        }

        $carbonDate = \Carbon\Carbon::createFromFormat("Y-m", $yearMonth);

        $firstDay = new \DateTime('first day of '.$carbonDate->format("F Y"));
        $carbonFirstDay = \Carbon\Carbon::instance($firstDay);
        $lastDay = new \DateTime('last day of '.$carbonDate->format("F Y"));
        $carbonLastDay = \Carbon\Carbon::instance($lastDay);

        if ($carbonFirstDay->format("N") > 2) {
            if ($carbonFirstDay->format("N") > 5) {
                $difference = 6;
            } else {
                $difference = 5 - $carbonFirstDay->format("N") + 7;
            }
            $firstFriday = $carbonFirstDay->addDays($difference);
        } else {
            $difference = 5 - $carbonFirstDay->format("N");
            $firstFriday = $carbonFirstDay->addDays($difference);
        }

        if ($carbonLastDay->format("N") <= 2) {
            $difference = 5 - $carbonLastDay->format("N");
            $lastFriday = $carbonLastDay->addDays($difference);
        } else {
            $difference = 5 - $carbonLastDay->format("N");
            $lastFriday = $carbonLastDay->addDays($difference);
        }

        return [
            'month' => $carbonDate->format("Y-m"),
            'first_friday' => $firstFriday->format("Y-m-d"),
            'last_friday' => $lastFriday->format("Y-m-d"),
            'first_day' => $firstFriday->subDays(7)->format("Y-m-d"),
            'first_friday_full' => $firstFriday->format("Y-m-d"). ' 16:00:00',
            'last_friday_full' => $lastFriday->format("Y-m-d") . ' 16:00:00',
            'first_day_full' => $firstFriday->subDays(7)->format("Y-m-d").' 16:00:00'
        ];
    }
}

if (! function_exists('getWeeks')) {
    /**
     * Return array of weeks
     *
     * @return array
     */
    function getWeeks($month)
    {
        $weeks = [];
        foreach (getFridays($month) as $friday) {
            $current_date = Carbon\Carbon::now();
            $carbon_friday = Carbon\Carbon::instance($friday);
            //Only show if before today
            if ($carbon_friday->lt($current_date)) {
                $weeks[$carbon_friday->format("Y-m-d")] = $carbon_friday->format('l \t\h\e jS');
            }
        }

        return $weeks;
    }
}

if (! function_exists('getFridays')) {
    function getFridays($month)
    {
        return new DatePeriod(
            new DateTime("first friday of $month"),
            DateInterval::createFromDateString('next friday'),
            new DateTime("last day of $month")
        );
    }
}

if (! function_exists('fridayRange')) {
    function fridayRange($start_date, $end_date)
    {
        $fridays = [];
        $date_iterator = $start_date->copy()->next(Carbon\Carbon::FRIDAY);

        while ($date_iterator->lte($end_date)) {
            $fridays[] = $date_iterator->format('Y-m-d');
            $date_iterator->next(Carbon\Carbon::FRIDAY);
        }

        return $fridays;
    }
}

if (! function_exists('lastFriday')) {
    /**
     * Return array of weeks
     *
     * @return Carbon Object of last Friday
     */
    function lastFriday()
    {
        return new Carbon\Carbon('last friday');
    }
}

if (! function_exists('getWeekEnding')) {
    /**
     * Return WeekEnding of particular date
     *
     * @return Carbon Object weekending
     */
    function getWeekEnding($date, $datetime = true)
    {
        if ($datetime) {
            $date = \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $date);
        } else {
            $date = \Carbon\Carbon::createFromFormat("Y-m-d", $date);
        }

        if ($date->format("N") < 5) {
            //Get Next Friday of this week
            $difference = 5 - $date->format("N");
            $date->addDays($difference);
        } elseif ($date->format("N") > 5) {
            //Get Following Friday of next week
            $difference = 5 - $date->format("N") + 7;
            $date->addDays($difference);
        } elseif ($date->format("N") == 5) {
            //It's Friday BITCHES
        }

        return $date->format("Y-m-d");
    }
}

if (! function_exists('getNewPrograms')) {
    /**
     * Return new programs from last week
     *
     * @return string
     */
    function getNewPrograms($language_id = 1)
    {
        $last_friday = lastFriday();

        return App\Models\ProgramVersion::where('monitor_date', $last_friday->format('Y-m-d'))
            ->where('language_id', $language_id)
            ->count();
    }
}

if (! function_exists('getNewSpots')) {
    /**
     * Return new spots from last week
     *
     * @return string
     */
    function getNewSpots($language_id = 1)
    {
        $last_friday = lastFriday();

        return App\Models\SpotVersion::where('air_date', $last_friday->format('Y-m-d'))
            ->where('language_id', $language_id)
            ->count();
    }
}

if (! function_exists('getReportCompany')) {
    /**
     * Return name of product marketing company from specified week
     *
     * @return string
     */
    function getReportCompany($master_program_id, $week_ending)
    {

        $program = App\Models\ProgramVersion::with('marketingCompany')
            ->where('program_id', $master_program_id)
            ->where('monitor_date', '<', $week_ending)
            ->orderBy('monitor_date', 'desc')
            ->first();

        if ($program and $program->marketingCompany) {
            return $program->marketingCompany->name;
        }
    }
}

if (! function_exists('getWeekEndVideos')) {
    function getWeekEndVideos($week_ending, $type, $short_form = false)
    {
        if (! $week_ending) {
            return;
        }

        if (! is_object($week_ending)) {
            $week_ending = Carbon\Carbon::createFromFormat("Y-m-d", $week_ending);
        }

        $videodateformat = $week_ending->format("mdy");
        $prefixFlv = 'flv/'.$videodateformat;
        $prefixMp4 = 'mp4/'.$videodateformat;

        $bucket = config('filesystems.disks.s3-videos.bucket');
        $s3 = Storage::disk('s3')->getDriver()->getAdapter()->getClient();
        $iteratorFlv = $s3->getIterator(
            'ListObjects',
            [
                'Bucket' => $bucket,
                'Prefix' => $prefixFlv
            ]
        );

        $iteratorMp4 = $s3->getIterator(
            'ListObjects',
            [
                'Bucket' => $bucket,
                'Prefix' => $prefixMp4
            ]
        );

        $videos = [];
        foreach ($iteratorMp4 as $file) {
            if (substr($file['Key'], -4) == '.mp4') {
                $prep = str_replace('.mp4', '', $file['Key']);
                $prep = str_replace('mp4/', '', $prep);
                if ($short_form) {
                    if (substr($prep, 0, 8) == $videodateformat."SF") {
                        $videos[$prep] = $prep. ' (mp4)';
                    }
                } else {
                    if (substr($prep, 0, 8) != $videodateformat."SF" and substr($prep, 0, 6) == $videodateformat) {
                        $videos[$prep] = $prep. ' (mp4)';
                    }
                }
            }
        }
        foreach ($iteratorFlv as $file) {
            if (substr($file['Key'], -4) == '.flv') {
                $prep = str_replace('.flv', '', $file['Key']);
                $prep = str_replace('flv/', '', $prep);
                if ($short_form) {
                    if (substr($prep, 0, 8) == $videodateformat."SF") {
                        $videos[$prep] = $prep. ' (flv)';
                    }
                } else {
                    if (substr($prep, 0, 8) != $videodateformat."SF" and substr($prep, 0, 6) == $videodateformat) {
                        $videos[$prep] = $prep. ' (flv)';
                    }
                }
            }
        }

        return $videos;
    }
}

if (! function_exists('makeClickable')) {
    function makeClickable($ret)
    {
        if (substr($ret, 0, 4) == 'http' || substr($ret, 0, 5) == 'https') {
            return '<a href="'.$ret.'" target="_blank">'.$ret.'</a>';
        } else {
            return '<a href="http://'.$ret.'" target="_blank">'.$ret.'</a>';
        }
    }
}

if (! function_exists('slugify')) {
    function slugify($str, $replace = [], $delimiter = '-')
    {
        setlocale(LC_ALL, 'en_US.UTF8');
        if (!empty($replace)) {
            $str = str_replace((array) $replace, ' ', $str);
        }

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }
}

if (! function_exists('thirty_minute_times')) {
    function thirty_minute_times($day)
    {
        $date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $day->startOfDay());
        $end = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $day->endOfDay());

        $times = [];
        while ($date->lte($end)) {
            $times[$date->format("H:i:s")] = $date->format("h:i a");
            $date->addMinutes(30);
        }

        return $times;
    }
}

if (! function_exists('recursive_array_search')) {
    function recursive_array_search($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            $current_key=$key;
            if ($needle===$value or (is_array($value) && recursive_array_search($needle, $value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }
}

if (! function_exists('set_active')) {
    function set_active($path, $active = 'active')
    {

        return call_user_func_array('Request::is', (array)$path) ? $active : '';
    }
}

if (! function_exists('proper_parse_str')) {
    function proper_parse_str($str)
    {
        if (!$str) {
            return [];
        }

        // result array
        $arr = [];

        // split on outer delimiter
        $pairs = explode('&', $str);

        // loop through each pair
        foreach ($pairs as $i) {
            // split into name and value
            list($name,$value) = explode('=', $i, 2);

            // if name already exists
            if (isset($arr[$name])) {
                // stick multiple values into an array
                if (is_array($arr[$name])) {
                    $arr[$name][] = $value;
                } else {
                    $arr[$name] = [$arr[$name], $value];
                }
            } // otherwise, simply stick it in a scalar
            else {
                $arr[$name] = $value;
            }
        }

        // return result array
        return $arr;
    }
}

if (! function_exists('proper_request_find')) {
    // example ?channels1&channels=2
    function proper_request_find($name, $default = null)
    {
        $request = proper_parse_str($_SERVER['QUERY_STRING']);

        return isset($request[$name]) ? $request[$name] : $default;
    }
}

if (! function_exists('set_nav_open')) {
    function set_nav_open($path, $nav_open = 'opened')
    {

        return call_user_func_array('Request::is', (array)$path) ? $nav_open : '';
    }
}

if (! function_exists('multidimensional_search')) {
    function multidimensional_search($parents, $searched)
    {
        if (empty($searched) || empty($parents)) {
            return false;
        }

        foreach ($parents as $key => $value) {
            $exists = true;
            foreach ($searched as $skey => $svalue) {
                $exists = ($exists && isset($parents[$key][$skey]) && $parents[$key][$skey] == $svalue);
            }
            if ($exists) {
                return $key;
            }
        }

        return false;
    }
}
