<?php namespace App\Composers;

class DateRangePickerComposer
{

    public function compose($view)
    {
        if (Request::filled('rangeStart') and Request::filled('rangeEnd')) {
            $week_start = Carbon::createFromFormat("F j, Y", Request::get('rangeStart'));
            $week_end = Carbon::createFromFormat("F j, Y", Request::get('rangeEnd'));
        } else {
            $week_start = Carbon::now()->previous(Carbon::FRIDAY);
            $week_end = $week_start->copy()->subWeek();
        }

        $longForms = Airtime::select(
            'channels.name as channel_name',
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        )
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->whereBetween('airtimes.air_date', [$week_start->startOfDay(), $week_end->endOfDay()])
            ->groupBy('airtimes.channel_id')
            ->orderBy('air_count', 'desc')
            ->take(10)
            ->get();

        $shortForms = SpotAirtime::select(
            'channels.name as channel_name',
            DB::raw('COUNT(spot_airtimes.id) AS air_count')
        )
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->whereBetween('spot_airtimes.air_date', [$week_start->startOfDay(), $week_end->endOfDay()])
            ->groupBy('spot_airtimes.channel_id')
            ->orderBy('air_count', 'desc')
            ->take(10)
            ->get();

            $view->with(
                'longForms',
                $longForms,
                'shortForms',
                $shortForms,
                'week_start',
                $week_start,
                'week_end',
                $week_end
            );
    }
}
