<?php namespace App\Composers;

use App\Models\Report;

class ReportMenuComposer
{

    public function compose($view)
    {
        $reports = Report::where('active', 1)
            ->orderBy('name', 'asc')
            ->get();

        $reportsMenu = new \stdClass;
        $reportsMenu->long_form = [];
        $reportsMenu->short_form = [];
        $reportsMenu->spanish_long_form = [];
        $reportsMenu->spanish_short_form = [];
        $reportsMenu->aus_long_form = [];
        $reportsMenu->aus_short_form = [];
        $i = 0;
        foreach ($reports as $report) {
            if ($report->type == 'long' and $report->language_id == 1) {
                $reportsMenu->long_form[$i]['id'] = $report->id;
                $reportsMenu->long_form[$i]['name'] = $report->name;
                $reportsMenu->long_form[$i]['slug'] = $report->slug;
                $reportsMenu->long_form[$i]['interval'] = $report->interval;
                $reportsMenu->long_form[$i]['slug'] = $report->slug;
                $reportsMenu->long_form[$i]['created_at'] = $report->created_at;
            } elseif ($report->type == 'short' and $report->language_id == 1) {
                $reportsMenu->short_form[$i]['id'] = $report->id;
                $reportsMenu->short_form[$i]['name'] = $report->name;
                $reportsMenu->short_form[$i]['slug'] = $report->slug;
                $reportsMenu->short_form[$i]['interval'] = $report->interval;
                $reportsMenu->short_form[$i]['slug'] = $report->slug;
                $reportsMenu->short_form[$i]['created_at'] = $report->created_at;
            } elseif ($report->type == 'long' and $report->language_id == 2) {
                $reportsMenu->spanish_long_form[$i]['id'] = $report->id;
                $reportsMenu->spanish_long_form[$i]['name'] = $report->name;
                $reportsMenu->spanish_long_form[$i]['slug'] = $report->slug;
                $reportsMenu->spanish_long_form[$i]['interval'] = $report->interval;
                $reportsMenu->spanish_long_form[$i]['slug'] = $report->slug;
                $reportsMenu->spanish_long_form[$i]['created_at'] = $report->created_at;
            } elseif ($report->type == 'short' and $report->language_id == 2) {
                $reportsMenu->spanish_short_form[$i]['id'] = $report->id;
                $reportsMenu->spanish_short_form[$i]['name'] = $report->name;
                $reportsMenu->spanish_short_form[$i]['slug'] = $report->slug;
                $reportsMenu->spanish_short_form[$i]['interval'] = $report->interval;
                $reportsMenu->spanish_short_form[$i]['slug'] = $report->slug;
                $reportsMenu->spanish_short_form[$i]['created_at'] = $report->created_at;
            } elseif ($report->type == 'long' and $report->language_id == 4) {
                $reportsMenu->aus_long_form[$i]['id'] = $report->id;
                $reportsMenu->aus_long_form[$i]['name'] = $report->name;
                $reportsMenu->aus_long_form[$i]['slug'] = $report->slug;
                $reportsMenu->aus_long_form[$i]['interval'] = $report->interval;
                $reportsMenu->aus_long_form[$i]['slug'] = $report->slug;
                $reportsMenu->aus_long_form[$i]['created_at'] = $report->created_at;
            } elseif ($report->type == 'short' and $report->language_id == 4) {
                $reportsMenu->aus_short_form[$i]['id'] = $report->id;
                $reportsMenu->aus_short_form[$i]['name'] = $report->name;
                $reportsMenu->aus_short_form[$i]['slug'] = $report->slug;
                $reportsMenu->aus_short_form[$i]['interval'] = $report->interval;
                $reportsMenu->aus_short_form[$i]['slug'] = $report->slug;
                $reportsMenu->aus_short_form[$i]['created_at'] = $report->created_at;
            }
            $i++;
        }

        $view->with('reportsMenu', $reportsMenu);
    }
}
