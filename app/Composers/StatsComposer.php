<?php namespace App\Composers;

use Cache;
use App\Models\SpotVersion;
use App\Models\ProgramVersion;
use Carbon\Carbon;

class StatsComposer
{

    public function compose($view)
    {
        $ssStats = Cache::remember('ssStats', 7200, function () {
            $last_friday = new Carbon('last friday');
            $stats = collect();

            $stats->newSpotCount = SpotVersion::where('air_date', $last_friday->format('Y-m-d'))
                ->where('language_id', 1)
                ->count();
            $stats->newSpots = SpotVersion::with(['category'])
                ->where('air_date', $last_friday->format('Y-m-d'))
                ->where('language_id', 1)
                ->take(5)
                ->orderBy('title')
                ->get();

            $stats->newShowCount = ProgramVersion::where('monitor_date', $last_friday->format('Y-m-d'))
                ->where('language_id', 1)
                ->count();
            $stats->newShows = ProgramVersion::with(['category'])
                ->where('monitor_date', $last_friday->format('Y-m-d'))
                ->where('language_id', 1)
                ->take(5)
                ->orderBy('title')
                ->get();

            $stats->totalSpotCount = SpotVersion::count();

            $stats->totalShowCount = ProgramVersion::count();

            return $stats;
        });

        $view->with('ssStats', $ssStats);
    }
}
