<?php namespace DateRangePicker\Libraries;

use Carbon\Carbon;

class Range
{

    public static function getStartAndEnd($start, $end)
    {

        if ($start and $end) {
            $start = Carbon::createFromFormat("F j, Y", $start);
            $end = Carbon::createFromFormat("F j, Y", $end);
        } else {
            $end = Carbon::now()->previous(Carbon::FRIDAY);
            $start = $end->copy()->subWeek();
        }

        $dayDiff = $end->diffInDays($start);
        $prevStart = $start->copy()->subDays($dayDiff);
        $prevEnd = $start->copy();

        return [
            'start' => $start->startOfDay(),
            'end' => $end->endOfDay(),
            'prevStart' => $prevStart->startOfDay(),
            'prevEnd' => $prevEnd->endOfDay()
        ];
    }
}
