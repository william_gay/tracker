<?php namespace App\Libraries;

use App\Models\Airtime;
use App\Models\Category;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\ProgramWeekly;
use App\Models\Report;
use App\Models\Spot;
use App\Models\SpotAirtime;
use App\Models\SpotDetection;
use App\Models\SpotRanking;
use App\Models\SiteAnalytics;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Response;
use Request;
use Redirect;
use Sentry;
use View;

class ReportGenerator
{

    public static function weeklyFrequencyLongForm($week_ending, $report)
    {
        $program_weekly = ProgramWeekly::with('program', 'program.programVersions.marketingCompany')
            ->select(
                'program_weekly.id',
                'program_weekly.program_id',
                'program_weekly.week_ending',
                'program_weekly.freq_rank',
                'program_weekly.media_rank',
                'program_weekly.streak',
                'program_weekly.airs',
                'program_weekly.media_index',
                'program_weekly.stations',
                'program_weekly.prev_freq_rank',
                'program_weekly.prev_media_rank',
                DB::raw('(SELECT max(program_versions.id) FROM ims.program_versions
                WHERE program_versions.program_id = program_weekly.program_id) as program_version_id')
            )
            ->where('week_ending', $week_ending->format("Y-m-d"))
            ->orderBy('freq_rank')
            ->take($report->limit)
            ->get();

        View::share('data', $program_weekly);
        View::share('week_ending', $week_ending);
        View::share('report', $report);
    }

    public static function weeklyMediaIndexLongForm($week_ending, $report)
    {
        $program_weekly = ProgramWeekly::with('program')
            ->select(
                'program_weekly.id',
                'program_weekly.program_id',
                'program_weekly.week_ending',
                'program_weekly.freq_rank',
                'program_weekly.media_rank',
                'program_weekly.streak',
                'program_weekly.airs',
                'program_weekly.media_index',
                'program_weekly.stations',
                'program_weekly.prev_freq_rank',
                'program_weekly.prev_media_rank',
                DB::raw('(SELECT max(program_versions.id) FROM ims.program_versions
                WHERE program_versions.program_id = program_weekly.program_id) as program_version_id')
            )
            ->where('week_ending', $week_ending->format("Y-m-d"))
            ->orderBy('media_rank')
            ->take($report->limit)
            ->get();

        View::share('data', $program_weekly);
        View::share('week_ending', $week_ending);
        View::share('report', $report);
    }

    public static function weeklyDirectResponseShortForm($week_ending, $report)
    {
        $data = SpotDetection::with('spotVersion.category', 'spotVersion.subCategory', 'spotVersion.marketingCompany')
            ->select(
                DB::raw('SUM(spot_detections.detections) as airings'),
                'spot_versions.id as spot_version_id',
                'spot_versions.title',
                'spot_versions.price',
                'spot_versions.shipping_cost',
                'spot_versions.category_id',
                'spot_versions.sub_category_id',
                'spot_versions.marketing_company_id',
                'spot_detections.new_for_week'
            )
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
            ->where('rank_date', $week_ending->format("Y-m-d"))
            ->where('spot_versions.language_id', $report->language_id)
            ->where('spot_versions.non_dr', '=', 0) //Make sure it is marked as DR
            ->groupBy('spot_versions.spot_id')
            ->orderBy(DB::raw('SUM(detections)'), 'desc')
            ->take($report->limit)
            ->get();

        View::share('report', $report);
        View::share('week_ending', $week_ending);
        View::share('data', $data);
    }

    public static function weeklyProductRankingShortForm($week_ending, $report)
    {
        $data = SpotRanking::where('rank_date', $week_ending->format("Y-m-d"))
            ->orderBy('rank', 'asc')
            ->take($report->limit)
            ->get();

        View::share('report', $report);
        View::share('week_ending', $week_ending);
        View::share('data', $data);
    }

    public static function monthlyFrequencyLongForm($month, $report)
    {
        // View: top-ranking
        $airings = Airtime::with('ProgramVersion', 'ProgramVersion.category', 'ProgramVersion.marketingCompany')
            ->join('program_versions', 'airtimes.program_version_id', '=', 'program_versions.id')
            ->select(
                DB::raw('count(`airtimes`.`id`) as airs'),
                'airtimes.program_version_id',
                DB::raw('sum(airtimes.cost)/2000 as media_index')
            )
            ->whereBetween(
                'airtimes.air_date',
                [$month->copy()->startOfMonth()->toDateTimeString(),
                      $month->copy()->endOfMonth()->toDateTimeString()]
            )
            ->where('program_versions.language_id', $report->language_id)
            ->whereNull('airtimes.deleted_at')
            ->groupBy('program_versions.program_id')
            ->orderBy('airs', 'desc')
            ->take($report->limit)
            ->get();

        View::share('data', $airings);
        View::share('month', $month);
        View::share('report', $report);
    }

    public static function monthlyMediaIndexLongForm($month, $report)
    {
        $airings = Airtime::with('ProgramVersion', 'ProgramVersion.category', 'ProgramVersion.marketingCompany')
            ->join('program_versions', 'airtimes.program_version_id', '=', 'program_versions.id')
            ->select(
                DB::raw('count(`airtimes`.`id`) as airs'),
                'airtimes.program_version_id',
                DB::raw('sum(airtimes.cost)/2000 as media_index')
            )
            ->whereBetween(
                'airtimes.air_date',
                [$month->copy()->startOfMonth()->toDateTimeString(),
                      $month->copy()->endOfMonth()->toDateTimeString()]
            )
            ->where('program_versions.language_id', $report->language_id)
            ->whereNull('airtimes.deleted_at')
            ->groupBy('program_versions.program_id')
            ->orderBy('media_index', 'desc')
            ->take($report->limit)
            ->get();

        View::share('report', $report);
        View::share('month', $month);
        View::share('data', $airings);
    }

    public static function monthlyDirectResponseShortForm($month, $report)
    {
        $spot = SpotAirtime::with([
            'spotVersion',
            'spotVersion.category',
            'spotVersion.subCategory',
            'spotVersion.marketingCompany',
        ])
            ->select([
                DB::raw('count(spot_airtimes.id) as airings'),
                'spot_version_id',
            ])
            ->whereBetween(
                'spot_airtimes.air_date',
                [$month->copy()->startOfMonth()->toDateTimeString(),
                              $month->copy()->endOfMonth()->toDateTimeString()]
            )
            ->whereHas('spotVersion', function ($query) use ($report) {
                $query->where('language_id', $report->language_id)
                    ->where('non_dr', 0); //Make sure it is marked as DR
            })
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->groupBy('spot_versions.spot_id')
            ->orderBy('airings', 'desc')
            ->take($report->limit)
            ->get();

        View::share('month', $month);
        View::share('report', $report);
        View::share('data', $spot);
    }

    public static function monthlyProductRankingShortForm($month, $report)
    {
        $spot = SpotDetection::select(
            DB::raw('SUM(spot_detections.detections) as airings'),
            'spot_versions.id as spot_version_id',
            'spot_versions.title',
            'spot_versions.price',
            'spot_versions.shipping_cost',
            'spot_detections.new_for_week',
            'c.name as category',
            'sc.name as subcategory'
        )
            ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
            ->leftJoin('categories as c', 'spot_versions.category_id', '=', 'c.id')
            ->leftJoin('categories as sc', 'spot_versions.sub_category_id', '=', 'sc.id')
            ->leftJoin('companies', 'spot_versions.marketing_company_id', '=', 'companies.id')
            ->whereBetween(
                'rank_date',
                [$month->copy()->startOfMonth()->toDateTimeString(),
                      $month->copy()->endOfMonth()->toDateTimeString()]
            )
            ->where('spot_detections.language_id', $report->language_id)
            ->where('spot_versions.service', 0)
            ->where('spot_versions.non_dr', '=', 0) //Don't include non-dr
            ->groupBy('spot_versions.spot_id')
            ->orderBy('airings', 'desc')
            ->take($report->limit)
            ->get();

        View::share('month', $month);
        View::share('report', $report);
        View::share('data', $spot);
    }
}
