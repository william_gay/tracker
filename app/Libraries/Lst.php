<?php namespace App\Libraries;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\LiveShoppingTracker\LiveShoppingAirtimeProduct;
use App\Models\LiveShoppingTracker\LiveShoppingCategory;

class LST
{
    // Used on Index Page
    // airs grouped by Channel, Category
    public static function topCategoriesByChannel($categories, $channels, $range, $DSV = 0)
    {
        $results = LiveShoppingAirtimeProduct::with([
            // eager loaded relations not currently used
            // 'productVersion',
            // 'productVersion.productAirtimes',
            // 'productVersion.productAirtimes.airtime',
            // 'productVersion.productAirtimes.airtime.products',
        ])
            ->leftJoin('live_shopping_airtimes AS airtimes', 'live_shopping_airtimes_products.live_shopping_airtime_id', '=', 'airtimes.id')
            ->leftJoin('live_shopping_product_versions AS product_versions', 'live_shopping_airtimes_products.live_shopping_product_version_id', '=', 'product_versions.id')
            ->leftJoin('live_shopping_products AS products', 'product_versions.live_shopping_product_id', '=', 'products.id')
            ->leftJoin('channels AS channels', 'airtimes.channel_id', '=', 'channels.id')
            ->leftJoin('live_shopping_categories AS categories', 'products.live_shopping_category_id', '=', 'categories.id')

            ->select([
                'airtimes.channel_id AS channel_id',
                'channels.name AS channel_name',
                'channels.currency AS channel_currency',
                'products.live_shopping_category_id AS category_id',
                'categories.name AS category_name',
                DB::raw('COUNT(live_shopping_airtimes_products.id) as airs'),
                DB::raw('ROUND(AVG(product_versions.price), 2) as avg_price'),
                DB::raw('MIN(product_versions.price) as min_price'),
                DB::raw('MAX(product_versions.price) as max_price'),
            ])
            ->where(function ($query) use ($DSV) {
                if ($DSV) {
                    $query->where('product_versions.daily_special_value', '=', 1);
                }
            })
            ->where('airtimes.start_time', '>=', $range['start'])
            ->where('airtimes.start_time', '<', $range['end'])
            ->whereIn('channel_id', $channels)
            ->whereNotNull('categories.id')
            ->groupBy('channel_id')
            ->groupBy('category_id')
            ->orderBy('channel_id', 'asc')
            ->orderBy('airs', 'desc')
            ->get();

        return $results;
    }

    // For Network and Index page retrieve the following
    // channel name/id, category name/id, min/max/avg price, number of airs
    // group by channel and category
    public static function topCategories($categories, $channels, $range, $DSV = 0)
    {

        $results = LiveShoppingAirtimeProduct::with([
            // eager loaded relations not currently used
            // 'productVersion',
            // 'productVersion.productAirtimes',
            // 'productVersion.productAirtimes.airtime',
            // 'productVersion.productAirtimes.airtime.products',
        ])
            ->leftJoin('live_shopping_airtimes AS airtimes', 'live_shopping_airtimes_products.live_shopping_airtime_id', '=', 'airtimes.id')
            ->leftJoin('live_shopping_product_versions AS product_versions', 'live_shopping_airtimes_products.live_shopping_product_version_id', '=', 'product_versions.id')
            ->leftJoin('live_shopping_products AS products', 'product_versions.live_shopping_product_id', '=', 'products.id')
            ->leftJoin('channels AS channels', 'airtimes.channel_id', '=', 'channels.id')
            ->leftJoin('live_shopping_categories AS categories', 'products.live_shopping_category_id', '=', 'categories.id')

            ->select([
                'airtimes.channel_id AS channel_id',
                // 'channels.name AS channel_name',
                'products.live_shopping_category_id AS category_id',
                'categories.name AS category_name',
                DB::raw('COUNT(live_shopping_airtimes_products.id) as airs'),
                DB::raw('FORMAT(AVG(product_versions.price), 2) as avg_price'),
                DB::raw('FORMAT(MIN(product_versions.price), 2) as min_price'),
                DB::raw('FORMAT(MAX(product_versions.price), 2) as max_price'),
            ])
            ->where(function ($query) use ($DSV) {
                if ($DSV) {
                    $query->where('product_versions.daily_special_value', '=', 1);
                }
            })
            ->where('product_versions.price', '>', '0')
            ->where('airtimes.start_time', '>=', $range['start'])
            ->where('airtimes.start_time', '<', $range['end'])
            ->whereIn('channel_id', $channels)
            ->whereIn('categories.id', $categories)
            ->groupBy('category_id')
            ->orderBy('airs', 'desc')
            // TODO figure out how to take a limit of categories, but not result rows
            ->get();

        return $results;
    }

    //works the same as products(), but date range looks for any first_airdate
    //prodcts() looks for any airtime in the date range_
    public static function newProducts($categories, $channels, $range, $limit = 5, $dsv = false)
    {
        $products = LiveShoppingAirtimeProduct::with([
                'airtime',
                'airtime.channel',
                'productVersion',
                'productVersion.product',
                'productVersion.product.versions',
                'productVersion.product.category',
                'productVersion.product.subCategory'
            ])
            ->select([
                    'live_shopping_airtimes_products.id',
                    'live_shopping_airtimes.id as airtime_id',
                    'live_shopping_airtimes_products.live_shopping_airtime_id',
                    'live_shopping_airtimes_products.live_shopping_product_version_id',
                    'live_shopping_products.id as product_id',
                    'live_shopping_product_versions.id as product_version_id',
                    'live_shopping_product_versions.name as product_version_name',
                    'live_shopping_product_versions.price as product_price',
                    'live_shopping_categories.id as category_id',
                    'live_shopping_categories.name as category_name',
                    'sub_cat.id as sub_category_id',
                    'sub_cat.name as sub_category_name',
                    'channels.name as channel_name',
                    'channels.currency as channel_currency',
                    DB::raw('COUNT(live_shopping_airtimes_products.id) as airs'),
                    DB::raw('AVG(live_shopping_product_versions.price) as avg'),
            ])
            ->leftJoin('live_shopping_airtimes', 'live_shopping_airtimes.id', '=', 'live_shopping_airtimes_products.live_shopping_airtime_id')
            ->leftJoin('live_shopping_product_versions', 'live_shopping_product_versions.id', '=', 'live_shopping_airtimes_products.live_shopping_product_version_id')
            ->leftJoin('live_shopping_products', 'live_shopping_products.id', '=', 'live_shopping_product_versions.live_shopping_product_id')
            ->leftJoin('live_shopping_categories', 'live_shopping_categories.id', '=', 'live_shopping_products.live_shopping_category_id')
            ->leftJoin('live_shopping_categories as sub_cat', 'sub_cat.id', '=', 'live_shopping_products.live_shopping_sub_category_id')
            ->leftJoin('channels', 'channels.id', '=', 'live_shopping_airtimes.channel_id')
            ->whereIn('live_shopping_airtimes.channel_id', $channels)
            ->where(function ($query) use ($categories) {
                $query->whereIn('live_shopping_products.live_shopping_category_id', $categories)
                ->orWhereIn('live_shopping_products.live_shopping_sub_category_id', $categories);
            })
            ->where(function ($query) use ($dsv) {
                if ($dsv) {
                    $query->where('live_shopping_product_versions.daily_special_value', 1);
                }
            })
            ->whereBetween(
                'live_shopping_products.first_airdate',
                [$range['start'], $range['end']]
            )
            ->whereNull('live_shopping_airtimes.deleted_at')
            ->whereNull('live_shopping_airtimes_products.deleted_at')
            ->whereNull('live_shopping_products.deleted_at')
            ->whereNull('live_shopping_product_versions.deleted_at')
            ->whereNotNull('live_shopping_product_versions.price')
            ->groupBy('live_shopping_product_versions.live_shopping_product_id')
            ->orderBy('airs', 'desc')
            ->take($limit)
            //->remember(120)
            ->get();

        return $products;
    }

    public static function newProductsByCategory($categories, $channels, $range, $limit = 5, $dsv = false)
    {
        $products = LiveShoppingAirtimeProduct::with(
            [
                'airtime',
                'productVersion',
                'productVersion.product',
                'productVersion.product.category'
            ]
        )
            ->select(
                [
                    'live_shopping_categories.id as category_id',
                    'live_shopping_categories.name',
                    DB::raw('COUNT(DISTINCT live_shopping_products.id) as products')
                ]
            )
            ->leftJoin('live_shopping_airtimes', 'live_shopping_airtimes.id', '=', 'live_shopping_airtimes_products.live_shopping_airtime_id')
            ->leftJoin('live_shopping_product_versions', 'live_shopping_product_versions.id', '=', 'live_shopping_airtimes_products.live_shopping_product_version_id')
            ->leftJoin('live_shopping_products', 'live_shopping_products.id', '=', 'live_shopping_product_versions.live_shopping_product_id')
            ->leftJoin('live_shopping_categories', 'live_shopping_categories.id', '=', 'live_shopping_products.live_shopping_category_id')
            ->whereIn('live_shopping_airtimes.channel_id', $channels)
            ->whereIn('live_shopping_products.live_shopping_category_id', $categories)
            ->where(function ($query) use ($dsv) {
                if ($dsv) {
                    $query->where('live_shopping_product_versions.daily_special_value', 1);
                }
            })
            ->where(
                'live_shopping_products.first_airdate',
                '>=',
                $range['start']
            )
            ->where(
                'live_shopping_products.first_airdate',
                '<',
                $range['end']
            )
            ->whereNull('live_shopping_airtimes.deleted_at')
            ->whereNull('live_shopping_airtimes_products.deleted_at')
            ->whereNull('live_shopping_products.deleted_at')
            ->whereNull('live_shopping_product_versions.deleted_at')
            ->groupBy('live_shopping_products.live_shopping_category_id')
            ->orderBy('products', 'desc')
            ->take($limit)
            ->get();

        return $products;
    }

    // group by channel
    // currently only used by category page. one category at a time
    public static function byChannel($categories, $channels, $range, $limit = 5, $DSV)
    {
        $channels = LiveShoppingAirtimeProduct::with([
                'airtime',
                'airtime.channel',
                'productVersion',
                'productVersion.product',
                'productVersion.product.category'
            ])
            ->select([
                'channels.id as channel_id',
                'channels.name as channel_name',
                'channels.currency as channel_currency',
                'live_shopping_categories.id as category_id',
                'live_shopping_categories.name as category_name',
                DB::raw('COUNT(live_shopping_airtimes_products.id) as airs'),
                DB::raw('FORMAT(AVG(live_shopping_product_versions.price), 2) as avg'),
                DB::raw('FORMAT(MIN(live_shopping_product_versions.price), 2) as min_price'),
                DB::raw('FORMAT(MAX(live_shopping_product_versions.price), 2) as max_price'),
            ])
            ->leftJoin('live_shopping_airtimes', 'live_shopping_airtimes.id', '=', 'live_shopping_airtimes_products.live_shopping_airtime_id')
            ->leftJoin('live_shopping_product_versions', 'live_shopping_product_versions.id', '=', 'live_shopping_airtimes_products.live_shopping_product_version_id')
            ->leftJoin('live_shopping_products', 'live_shopping_products.id', '=', 'live_shopping_product_versions.live_shopping_product_id')
            ->leftJoin('live_shopping_categories', 'live_shopping_categories.id', '=', 'live_shopping_products.live_shopping_category_id')
            ->leftJoin('channels', 'live_shopping_airtimes.channel_id', '=', 'channels.id')
            ->whereIn('live_shopping_airtimes.channel_id', $channels)
            ->where(function ($query) use ($categories) {
                $query->whereIn('live_shopping_products.live_shopping_category_id', $categories)
                    ->orWhereIn('live_shopping_products.live_shopping_sub_category_id', $categories);
            })
            ->where(
                'live_shopping_airtimes_products.start_time',
                '>=',
                $range['start']
            )
            ->where(
                'live_shopping_airtimes_products.start_time',
                '<',
                $range['end']
            )
            ->where(function ($query) use ($DSV) {
                if ($DSV) {
                    $query->where('live_shopping_product_versions.daily_special_value', '=', 1);
                }
            })
            ->whereNull('live_shopping_airtimes.deleted_at')
            ->whereNull('live_shopping_airtimes_products.deleted_at')
            ->whereNull('live_shopping_products.deleted_at')
            ->whereNull('live_shopping_product_versions.deleted_at')
            ->groupBy('live_shopping_airtimes.channel_id')
            ->orderBy('airs', 'desc')
            ->take($limit)
            ->get();

        return $channels;
    }

    //works the same as newProducts, but date range looks for any airtime.
    //newProdcts() looks for first_airdate in the date range_
    public static function products($categories, $channels, $range, $limit = 5, $dsv = false)
    {
        $products = LiveShoppingAirtimeProduct::with([
                'airtime',
                'airtime.channel',
                'productVersion',
                'productVersion.product',
                'productVersion.product.versions',
                'productVersion.product.category',
                'productVersion.product.subCategory'
            ])
            ->select([
                    'live_shopping_airtimes_products.id',
                    'live_shopping_airtimes.id as airtime_id',
                    'live_shopping_airtimes_products.live_shopping_airtime_id',
                    'live_shopping_airtimes_products.live_shopping_product_version_id',
                    'live_shopping_products.id as product_id',
                    'live_shopping_product_versions.id as product_version_id',
                    'live_shopping_product_versions.name as product_version_name',
                    'live_shopping_product_versions.price as product_price',
                    'live_shopping_categories.id as category_id',
                    'live_shopping_categories.name as category_name',
                    'sub_cat.id as sub_category_id',
                    'sub_cat.name as sub_category_name',
                    'channels.name as channel_name',
                    'channels.currency as channel_currency',
                    DB::raw('COUNT(live_shopping_airtimes_products.id) as airs'),
                    DB::raw('AVG(live_shopping_product_versions.price) as avg'),
            ])
            ->leftJoin('live_shopping_airtimes', 'live_shopping_airtimes.id', '=', 'live_shopping_airtimes_products.live_shopping_airtime_id')
            ->leftJoin('live_shopping_product_versions', 'live_shopping_product_versions.id', '=', 'live_shopping_airtimes_products.live_shopping_product_version_id')
            ->leftJoin('live_shopping_products', 'live_shopping_products.id', '=', 'live_shopping_product_versions.live_shopping_product_id')
            ->leftJoin('live_shopping_categories', 'live_shopping_categories.id', '=', 'live_shopping_products.live_shopping_category_id')
            ->leftJoin('live_shopping_categories as sub_cat', 'sub_cat.id', '=', 'live_shopping_products.live_shopping_sub_category_id')
            ->leftJoin('channels', 'channels.id', '=', 'live_shopping_airtimes.channel_id')
            ->whereIn('live_shopping_airtimes.channel_id', $channels)
            ->where(function ($query) use ($categories) {
                $query->whereIn('live_shopping_products.live_shopping_category_id', $categories)
                 ->orWhereIn('live_shopping_products.live_shopping_sub_category_id', $categories);
            })
            ->where(function ($query) use ($dsv) {
                if ($dsv) {
                    $query->where('live_shopping_product_versions.daily_special_value', 1);
                }
            })
            ->whereBetween(
                'live_shopping_airtimes_products.start_time',
                [$range['start']->toDateTimeString(), $range['end']->toDateTimeString()]
            )
            ->whereNull('live_shopping_airtimes.deleted_at')
            ->whereNull('live_shopping_airtimes_products.deleted_at')
            ->whereNull('live_shopping_products.deleted_at')
            ->whereNull('live_shopping_product_versions.deleted_at')
            ->whereNotNull('live_shopping_product_versions.price')
            ->groupBy('live_shopping_product_versions.live_shopping_product_id')
            ->orderBy('airs', 'desc')
            ->take($limit)
            ->get();

        return $products;
    }

    public static function byDate($categoryId, $channels, $range, $limit = 5, $DSV = 0)
    {
        $airtimes = LiveShoppingAirtimeProduct::with(
            [
                //'airtime',
                //'productVersion',
                //'productVersion.product',
                //'productVersion.product.category'
            ]
        )
            ->select(
                [
                    // 'live_shopping_airtimes.start_time as airdate',
                    'live_shopping_airtimes.channel_id as channel_id',
                    'channels.name as channel_name',
                    'channels.currency as channel_currency',
                    //DB::raw('YEARWEEK(live_shopping_airtimes.start_time) as date'),
                    DB::raw('DATE_FORMAT(live_shopping_airtimes.start_time, "%Y%m%d") as date'),
                    DB::raw('COUNT(live_shopping_airtimes.start_time) as total_airs'),
                    //do this dynamically
                ]
            )
            ->leftJoin('live_shopping_airtimes', 'live_shopping_airtimes.id', '=', 'live_shopping_airtimes_products.live_shopping_airtime_id')
            ->leftJoin('channels AS channels', 'live_shopping_airtimes.channel_id', '=', 'channels.id')
            ->leftJoin('live_shopping_product_versions', 'live_shopping_product_versions.id', '=', 'live_shopping_airtimes_products.live_shopping_product_version_id')
            ->leftJoin('live_shopping_products', 'live_shopping_products.id', '=', 'live_shopping_product_versions.live_shopping_product_id')
            ->leftJoin('live_shopping_categories', 'live_shopping_categories.id', '=', 'live_shopping_products.live_shopping_category_id')
            ->whereIn('live_shopping_airtimes.channel_id', $channels)
            ->where(function ($query) use ($categoryId) {
                $query->whereIn('live_shopping_products.live_shopping_category_id', $categoryId)
                    ->orWhereIn('live_shopping_products.live_shopping_sub_category_id', $categoryId);
            })
            ->where(
                'live_shopping_airtimes_products.start_time',
                '>=',
                $range['start']
            )
            ->where(
                'live_shopping_airtimes_products.start_time',
                '<',
                $range['end']
            )
            ->where(function ($query) use ($DSV) {
                if ($DSV) {
                    $query->where('live_shopping_product_versions.daily_special_value', '=', 1);
                }
            })
            ->whereNull('live_shopping_products.deleted_at')
            ->whereNull('live_shopping_airtimes.deleted_at')
            ->whereNull('live_shopping_airtimes_products.deleted_at')
            ->whereNull('live_shopping_product_versions.deleted_at')
            ->groupBy('date', 'channel_id')
            ->orderBy('date', 'asc')
            ->take($limit)
            ->get();

        return $airtimes;
    }
}
