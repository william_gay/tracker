<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use Sentry;
use App\Models\CategoryReport;
use App\Models\Report;

class Tracker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Sentry::check()) {
            return redirect()->guest('user/login');
        } else {
            //See if they are a member of only the category report subscription
            $user = Sentry::getUser();
            $categoryReportGroup = Sentry::findGroupByName('Category Report Subscription');
            if ($user->inGroup($categoryReportGroup)) {
                $permissions = $user->getMergedPermissions();
                foreach ($permissions as $key => $value) {
                    if (strpos($key, 'category-report') !== false) {
                        if (Route::current()->getName() == 'dashboard') {
                            $slug = str_replace('category-reports.', '', $key);
                            $categoryReportId = CategoryReport::findIdBySlug($slug);
                            if ($categoryReportId) {
                                return redirect()->route('category-reports.show', [$categoryReportId]);
                            }
                        }
                    }
                }
            }
            $top25ReportGroup = Sentry::findGroupByName('Top 25 Subscription');
            if ($user->inGroup($top25ReportGroup)) {
                $report = Report::where('slug', 'top-25-report')->first();
                if (Route::current()->getName() == 'dashboard') {
                    return redirect()->route('reports.show', [$report->id]);
                }
            }
            $retailReportGroup = Sentry::findGroupByName('Retail Report Subscription');
            if ($user->inGroup($retailReportGroup)) {
                $permissions = $user->getMergedPermissions();
                if (Route::current()->getName() == 'dashboard') {
                    return redirect()->route('retail-report.monthly');
                }
            }
        }

        return $next($request);
    }
}
