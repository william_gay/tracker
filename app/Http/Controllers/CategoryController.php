<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Sentry;
use Carbon\Carbon;

use App\Models\Airtime;
use App\Models\Category;
use App\Models\Channel;
use App\Models\Language;
use App\Models\Report;
use DateRangePicker\Libraries\Range;

class CategoryController extends Controller
{
    private $channelFormPopulator;
    private $channelsToQuery;
    private $categoryFormPopulator;
    private $categoriesToQuery;
    private $singleCategory;
    private $marketFormPopulator;
    private $marketToQuery;
    private $range;

    public function sharedVariables(Request $request)
    {
        if (! Sentry::getUser()->hasAccess('reports.category-report')) {
            return redirect()->route('dashboard')
                ->with('error', 'Upgrade Your Subscription to Access the Category Report.');
        }

        if ($request->filled('rangeStart') and ! $request->filled('rangeEnd')) {
            $lastFriday = Carbon::now()->previous(Carbon::FRIDAY);

            $this->range = Range::getStartAndEnd($lastFriday->copy()->subDay()->format("F j, Y"), $lastFriday->format("F j, Y"));
        } else {
            $this->range = Range::getStartAndEnd($request->get('rangeStart'), $request->get('rangeEnd'));
        }

        // dd($this->range);

        $report = Report::where('slug', 'category-report')->first();

        $this->marketToQuery = Language::getMarkets($request->get('market_id'));
        if (! Sentry::getUser()->hasAccess('reports.spanish-shows') and $request->get('market_id') == 2) {
            $this->marketToQuery = 1;
            return redirect()->route('network-analyzer.home')
                ->with('error', 'Upgrade Your Subscription to Access the Spanish Demographic.');
        }
        if (! Sentry::getUser()->hasAccess('reports.australia') and $request->get('market_id') == 4) {
            $this->marketToQuery = 1;
            return redirect()->route('network-analyzer.home')
                ->with('error', 'Upgrade Your Subscription to Access the Australian Demographic.');
        }

        $this->channelFormPopulator = Channel::where('language_id', $this->marketToQuery)
                                    ->orderBy('name', 'asc')
                                    //->where('capturing', 1)
                                    ->where(function ($query) {
                                        if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                                            $query->where('channels.pi', 0);
                                        }
                                    })
                                    ->pluck('name', 'id')->toArray();

        $this->channelsToQuery = Channel::getChannels($request->input('channels'), [1]);
        $this->categoryFormPopulator = Category::whereNull('parent_id')->pluck('name', 'id')->toArray();
        $this->categoriesToQuery = Category::getCategories($request->get('categories'), [1]);
        $this->marketFormPopulator = Language::marketFormPopulator();

        view()->share('range', $this->range);
        view()->share('report', $report);
        view()->share('channelFormPopulator', $this->channelFormPopulator);
        view()->share('channelsToQuery', $this->channelsToQuery);
        view()->share('categoryFormPopulator', $this->categoryFormPopulator);
        view()->share('categoriesToQuery', $this->categoriesToQuery);
        view()->share('marketFormPopulator', $this->marketFormPopulator);
    }

    public function shortForm(Request $request)
    {
        $this->sharedVariables($request);

        $categories = $this->shortFrequency();

        $countPerCategory = static::flatGroup($categories, 'date', 'count', 'category_name');
        $costPerCategory = static::flatGroup($categories, 'date', 'cost', 'category_name');

        return view('cxp.categories.short')
            ->with('costPerCategory', $costPerCategory)
            ->with('countPerCategory', $countPerCategory);
    }

    public function longForm(Request $request)
    {
        $this->sharedVariables($request);

        $categories = $this->longFrequency();

        $countPerCategory = static::flatGroup($categories, 'date', 'count', 'category_name');
        $costPerCategory = static::flatGroup($categories, 'date', 'cost', 'category_name');

        return view('cxp.categories.long')
            ->with('costPerCategory', $costPerCategory)
            ->with('countPerCategory', $countPerCategory);
    }

    public static function flatGroup($collection, $group, $value, $key)
    {
        return collect($collection->reduce(function ($acc, $item) use ($group, $value, $key) {
            $groupData = [
                $group => $item->$group,
                $item->$key => $item->$value,
            ];

            if (isset($acc[$item->$group])) {
                $existing = $acc[$item->$group];
                $acc[$item->$group] = array_merge($existing, $groupData);
                return $acc;
            }

            $acc[$item->$group] = $groupData;
            return $acc;
        }, []))->values();
    }

    public function longFrequency()
    {
        $categories = DB::table('airtimes')->select([
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(airtimes.id) AS count'),
            DB::raw('SUM(airtimes.cost) AS cost'),
            DB::raw('DATE(airtimes.air_date) as date'),
        ])
                    ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
                    ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
                    ->whereIn('airtimes.channel_id', $this->channelsToQuery)
                    ->whereIn('program_versions.category_id', $this->categoriesToQuery)
                    ->where('airtimes.air_date', '>=', $this->range['start'])
                    ->where('airtimes.air_date', '<', $this->range['end'])
                    ->where('verified', 1)
                    ->groupBy(DB::raw('WEEK(date)'), 'program_versions.category_id')
                    ->orderBy('date')
                    ->get();

        return $categories;
    }

    public function shortFrequency()
    {
        $categories = DB::table('spot_airtimes')->select([
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS count'),
            DB::raw('SUM(spot_airtimes.cost) AS cost'),
            DB::raw('DATE(spot_airtimes.air_date) as date'),
        ])
                    ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
                    ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
                    ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
                    ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
                    ->where('spot_airtimes.air_date', '>=', $this->range['start'])
                    ->where('spot_airtimes.air_date', '<', $this->range['end'])
                    ->groupBy(DB::raw('WEEK(date)'), 'spot_versions.category_id')
                    ->orderBy('date')
                    ->get();

        return $categories;
    }
}
