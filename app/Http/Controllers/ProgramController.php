<?php

namespace App\Http\Controllers;

use App\Models\Airtime;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\ProgramWeekly;
use App\Models\SiteAnalytics;
use App\Models\Language;
use Carbon\Carbon;
use App\Jobs\LogSiteAnalytics;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Sentry;
use Storage;

class ProgramController extends Controller
{
    public function getIndex(Builder $builder)
    {
        $user = Sentry::getUser();

        $site_analytics = [
            'type' => 'program search',
            'user_id' => $user->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $language_id = request()->get('language_id') ?: 1;

        $language = Language::findOrFail($language_id);

        if (! $user->hasAccess('reports.'.strtolower($language->name).'-shows')) {
            return abort('401', 'Access denied - You do not have access to this report.');
        }

        if (request()->ajax()) {
            return $this->getData();
        }

        $html = $builder->columns([
            ['data' => 'title', 'name' => 'title', 'title' => 'Title'],
            ['data' => 'grid_title', 'name' => 'grid_title', 'title' => 'Grid Title'],
            ['data' => 'marketingCompany', 'name' => 'marketingCompany', 'title' => 'Company'],
            ['data' => 'category', 'name' => 'category', 'title' => 'Category'],
            ['data' => 'subCategory', 'name' => 'subCategory', 'title' => 'Sub-Category'],
            ['data' => 'monitor_date', 'name' => 'monitor_date', 'title' => 'First Detected'],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() {}',
            'autoWidth' => false,
            'responsive' => true,
            "order" => [[ 5, "desc" ]],
        ]);

        return view('cxp.programs.all')
            ->with('language_id', $language_id)
            ->with('language', $language)
            ->with('html', $html);
    }

    public function getNew()
    {
        $user = Sentry::getUser();

        if (request()->filled('week_ending')) {
            $week_ending = Carbon::createFromFormat("F j, Y", request()->get('week_ending'));
            $week_ending->hour = 0;
            $week_ending->minute = 0;
            $week_ending->second = 0;
        } else {
            $week_ending = lastFriday();
        }

        $language_id = request()->get('language_id') ?: 1;
        $language = Language::findOrFail($language_id);

        $allowed = data_allowed($week_ending);

        if (! $allowed) {
            $data_since = data_since();
            $data_since->addDays(7);
            return redirect()->route('programs.new', ['week_ending'=> $data_since->format('F j, Y'), 'language_id' => $language_id])->with('success', 'You have been redirected to shows that fit in your data range.');

            return abort('401', 'Access denied - Date outside of range');
        }

        $month = $week_ending->format("Y-m");
        $months = getMonths();
        $weeks = getWeeks($month);

        $site_analytics = [
            'type' => 'programs',
            'extra' => $week_ending->format("Y-m-d"),
            'user_id' => Sentry::getUser()->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        if (! $user->hasAccess('reports.'.strtolower($language->name).'-shows-new')) {
            return abort('401', 'Access denied - You do not have access to this report.');
        }

        $programs = ProgramVersion::findByWeek($week_ending, $language_id);

        return view('cxp.programs.index', compact('programs', 'months', 'weeks', 'month', 'week_ending', 'language_id', 'language'));
    }

    /**
     * Redirect to Program Version
     *
     * @return Redirect
    */
    public function getParentProgram($programId)
    {
        $programVersion = ProgramVersion::findByProgramId($programId)
            ->orderBy('version', 'desc')
            ->first();

        return redirect()->route('programs.show', ['show' => $programVersion->id]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getProgram($programId)
    {
        try {
            $program = ProgramVersion::with(
                'marketingCompany',
                'category',
                'subCategory',
                'channel'
            )->findOrFail($programId);
        } catch (ModelNotFoundException $e) {
            $program = ProgramVersion::where('program_id', $programId)->orderBy('monitor_date')->first();
            if ($program) {
                return redirect()->route('programs.show', $program->id);
            }

            return abort('404', 'This show does not exist. '. $e);
        }

        if (request()->filled('week_ending')) {
            $week_ending = Carbon::createFromFormat("F j, Y", request()->get('week_ending'));
            $week_ending->setTime(0, 0, 0);
        } else {
            $week_ending = lastFriday();
            $week_ending->setTime(0, 0, 0);
        }

        $start_date = data_since();
        $end_date = data_to();

        $initial_date = $program->monitor_date->startOfDay();
        $allowed = data_allowed($initial_date);
        if (! $allowed) {
            //Lookup the newest version they have access to and redirect
            $programAllowed = ProgramVersion::where('program_id', $program->program_id)
                ->whereBetween('monitor_date', [$start_date->startOfDay()->format('Y-m-d'),$end_date->endOfDay()->format('Y-m-d')])
                ->orderBy('monitor_date', 'asc')
                ->first();

            if ($programAllowed) {
                return redirect()->route('programs.show', $programAllowed->id)->with('success', 'You have been redirected to a newer version of this show that fits in your data range.');
            }

            return abort('401', 'Access denied - Date outside of range');
        }

        $programVersions = ProgramVersion::with(['airtimes'])
            ->where('program_versions.program_id', $program->program_id)
            ->where('program_versions.language_id', $program->language_id)
            ->whereBetween('monitor_date', [$start_date,$end_date])
            ->orderBy('monitor_date', 'desc')

            ->get();

        $versions = [];
        if ($programVersions->count() > 0) {
            foreach ($programVersions as $version) {
                $versions[] = $version->id;
            }
        }

        $site_analytics = [
            'type' => 'program',
            'type_id' => $program->id,
            'user_id' => Sentry::getUser()->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $overTime = ProgramWeekly::select([
                DB::raw('SUM(media_index) as media'),
                DB::raw('SUM(airs) as airs'),
                'freq_rank',
                'media_rank',
                DB::raw('week_ending as date')
            ])
            ->whereBetween('week_ending', [$week_ending->copy()->subMonths(6)->format('Y-m-d'), $week_ending->format('Y-m-d')])
            ->where('program_id', $program->program_id)
            ->groupBy('week_ending')

            ->get();

        $programWeekly = ProgramWeekly::where('week_ending', $week_ending->format("Y-m-d"))
            ->where('program_id', $program->program_id)
            ->first();

        //52 Week stats
        $fivetwofreq = ProgramWeekly::where('program_id', $program->program_id)
            ->where('week_ending', '<=', $week_ending->format('Y-m-d'))
            ->avg('airs');

        $fivetwomedia = ProgramWeekly::where('program_id', $program->program_id)
            ->where('week_ending', '<=', $week_ending->format('Y-m-d'))
            ->avg('media_index');

        $fivetwofreqmin = ProgramWeekly::where('program_id', $program->program_id)
            ->where('week_ending', '<=', $week_ending->format('Y-m-d'))
            ->min('airs');

        $fivetwofreqmax = ProgramWeekly::where('program_id', $program->program_id)
            ->where('week_ending', '<=', $week_ending->format('Y-m-d'))
            ->max('airs');

        $fivetwomediamin = ProgramWeekly::where('program_id', $program->program_id)
            ->where('week_ending', '<=', $week_ending->format('Y-m-d'))
            ->min('media_index');

        $fivetwomediamax = ProgramWeekly::where('program_id', $program->program_id)
            ->where('week_ending', '<=', $week_ending->format('Y-m-d'))
            ->max('media_index');

        //Total Channels
        $totalChannels = Airtime::select(DB::raw('COUNT(DISTINCT channel_id) as channels'))
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->where('airtimes.verified', 1)
            ->whereIn('program_version_id', $versions)
            ->whereBetween(
                'air_date',
                [$week_ending->copy()->subWeek()->setTime(16, 0, 0)->format('Y-m-d H:i:s'), $week_ending->setTime(16, 0, 0)->format('Y-m-d H:i:s')]
            )
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })

            ->first();

        $channelList = Airtime::select([
                'channels.abbr',
                'channels.logo_location',
                DB::raw('count(airtimes.channel_id) as chancount'),
                DB::raw('sum(airtimes.cost)/2000 as media_index')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->where('airtimes.verified', 1)
            ->whereIn('program_version_id', $versions)
            ->whereBetween(
                'air_date',
                [
                    $week_ending->copy()->subWeek()->setTime(16, 0, 0)->format('Y-m-d H:i:s'),
                    $week_ending->setTime(16, 0, 0)->format('Y-m-d H:i:s')
                ]
            )
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->verified()
            ->groupBy('airtimes.channel_id')
            ->orderBy('chancount', 'desc')

            ->get();

        $bucket = config('filesystems.disks.s3-videos.bucket');
        $s3Disk = Storage::disk('s3-videos');
        $client = $s3Disk->getDriver()->getAdapter()->getClient();
        $expiry = "+30 minutes";

        $signed_flv = null;
        if ($program->flv == 1) {
            $key = 'flv/'.$program->file_name.'.flv';
            $command = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key'    => $key
            ]);
            $request = $client->createPresignedRequest($command, $expiry);

            $query = $request->getUri()->getQuery();
            $signed_flv = $s3Disk->url($key).'?'.$query;
        }

        $signed_mp4 = null;
        if ($program->mp4 == 1) {
            $key = 'mp4/'.$program->file_name.'.mp4';
            $command = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key'    => $key
            ]);
            $request = $client->createPresignedRequest($command, $expiry);

            $query = $request->getUri()->getQuery();
            $signed_mp4 = $s3Disk->url($key).'?'.$query;
        }

        $signed_poster = null;
        if ($program->poster == 1) {
            $key = 'poster/'.$program->file_name.'.jpg';
            $command = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key'    => $key
            ]);
            $request = $client->createPresignedRequest($command, $expiry);

            $query = $request->getUri()->getQuery();
            $signed_poster = $s3Disk->url($key).'?'.$query;
        }

        return view(
            'cxp.programs.show',
            compact(
                'program',
                'signed_flv',
                'signed_mp4',
                'signed_poster',
                'overTime',
                'programWeekly',
                'programVersions',
                'fivetwofreq',
                'fivetwomedia',
                'fivetwofreqmin',
                'fivetwofreqmax',
                'fivetwomediamin',
                'fivetwomediamax',
                'totalChannels',
                'channelList',
                'week_ending'
            )
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getSchedule($programId)
    {
        $user = Sentry::getUser();

        if (! $user->hasAccess('reports.product-scheduler')) {
            return abort('401', 'Access denied - You do not have access to this report.');
        }

        if (request()->filled('start-date') and request()->filled('end-date')) {
            $start_date = Carbon::createFromFormat("F j, Y", request()->get('start-date'))->startOfDay();
            $end_date = Carbon::createFromFormat("F j, Y", request()->get('end-date'))->endOfDay();

            if ($start_date->gte($end_date)) {
                return redirect()->route('programs.schedule', $programId)->with('error', 'Start date must be before end date.');
            }
        } else {
            $start_date = lastFriday()->subWeek()->addDay()->startOfDay();
            $end_date = lastFriday()->endOfDay();
        }

        $allowed_start = data_allowed($start_date);
        $allowed_end = data_allowed($end_date);
        if (! $allowed_start or ! $allowed_end) {
            $end_date = data_to();
            $start_date = $end_date->copy()->subWeek();
        }

        try {
            $program = ProgramVersion::with('marketingCompany', 'category', 'subCategory', 'channel')
                ->findOrFail($programId);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This show does not exist. '. $e);
        }

        $programVersions = ProgramVersion::where('program_id', $program->program_id)->orderBy('version')->get();
        $versionA = [];
        foreach ($programVersions as $version) {
            $versionA[$version->id] = $version->title.' v'.$version->version;
        }

        if (request()->filled('versions')) {
            $vSelected = request()->get('versions');
        } else {
            $vSelected = ProgramVersion::where('program_id', $program->program_id)->orderBy('version')->pluck('id')->toArray();
        }

        foreach ($vSelected as $v) {
            $site_analytics = [
                'type' => 'program_scheduler',
                'type_id' => $v,
                'user_id' => Sentry::getUser()->id
            ];
            dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));
        }

        $airtimes = Airtime::select([
                DB::raw('COUNT(airtimes.id) as airs'),
                DB::raw('SUM(airtimes.cost)/2000 as media_index'),
                DB::raw('SUM(cost) as total_cost'),
                DB::raw('COUNT(DISTINCT channel_id) as channels')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->whereIn('program_version_id', $vSelected)
            ->whereNull('airtimes.deleted_at')
            ->whereBetween(
                'airtimes.air_date',
                [$start_date->format("Y-m-d H:i:s"), $end_date->format("Y-m-d H:i:s")]
            )
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->verified()
            ->first();

        $airtimes_raw = Airtime::with('channel', 'programVersion')
            ->select(
                'airtimes.air_date as air_dateable',
                'airtimes.channel_id',
                'airtimes.program_version_id'
            )
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->where('airtimes.verified', 1)
            ->whereIn('program_version_id', $vSelected)
            ->whereBetween(
                'air_date',
                [
                    $start_date->format("Y-m-d H:i:s"),
                    $end_date->format("Y-m-d H:i:s")
                ]
            )
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->verified()
            ->orderBy('air_date', 'asc')
            ->get();

        $channels = Airtime::select([
                'channels.name as channel_name',
                DB::raw('count(airtimes.id) as airs')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->where('airtimes.verified', 1)
            ->whereIn('program_version_id', $vSelected)
            ->whereBetween(
                'air_date',
                [
                    $start_date->format("Y-m-d H:i:s"),
                    $end_date->format("Y-m-d H:i:s")
                ]
            )
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->groupBy('channel_id')
            ->orderBy('airs', 'desc')
            ->get();

        $overTime = ProgramWeekly::select([
                DB::raw('SUM(media_index) as media'),
                DB::raw('SUM(airs) as airs'),
                'freq_rank',
                'media_rank',
                'week_ending'
            ])
            ->whereBetween(
                'week_ending',
                [$start_date->format("Y-m-d H:i:s"), $end_date->format("Y-m-d H:i:s")]
            )
            ->where('program_id', $program->program_id)
            ->groupBy('week_ending')

            ->get();

        return view(
            'cxp.programs.schedule',
            compact(
                'program',
                'versionA',
                'vSelected',
                'start_date',
                'end_date',
                'airtimes',
                'channels',
                'airtimes_raw',
                'overTime'
            )
        );
    }

    public function getData()
    {
        $start_date = data_since();
        $end_date = data_to();

        $programs = ProgramVersion::with([
                'category',
                'subCategory',
                'marketingCompany',
            ])
            ->select([
                'program_versions.*',

            ])
            ->where('language_id', request('language_id', 1))
            ->whereBetween('monitor_date', [$start_date->startOfDay()->format('Y-m-d H:i:s'),$end_date->endOfDay()->format('Y-m-d H:i:s')]);

        return Datatables::of($programs)
            ->editColumn('title', '<a href="{{ route("programs.show", array($id)) }}">{{ $title }}</a> @if(Sentry::getUser()->isSuperUser()) <small><a href="{{ route("admin.programs.edit",array($id)) }}" target="_blank">Edit</a></small> @endif')
            ->addColumn('marketingCompany', function ($program) {
                if ($program->marketingCompany) {
                    return $program->marketingCompany->name;
                }
            })
            ->addColumn('category', function ($program) {
                if ($program->category) {
                    return $program->category->name;
                }
            })
            ->addColumn('subCategory', function ($program) {
                if ($program->subCategory) {
                    return $program->subCategory->name;
                }
            })
            ->editColumn('monitor_date', function ($program) {
                if ($program->monitor_date) {
                    return Carbon::createFromFormat('Y-m-d H:i:s', $program->monitor_date)->format('m/d/Y');
                }
            })
            ->rawColumns(['title'])
            ->orderColumn('monitor_date', 'monitor_date $1')
            ->toJson();
    }

    public function printBucket()
    {
        $bucket = config('filesystems.disks.s3-videos.bucket');
        $s3Disk = Storage::disk('s3-videos');
        $s3 = $s3Disk->getDriver()->getAdapter()->getClient();

        $iterator = $s3->getIterator('ListObjects', [
            'Bucket' => $bucket
        ]);

        $table = '<table>';
        foreach ($iterator as $object) {
            $table .= '<tr>';
            $table .= '<td>'.$object['Key'] . '</td>';
            $table .= '</tr>';
        }
        $table .= '</table>';

        echo $table;
    }
}
