<?php

namespace App\Http\Controllers;

use App\Models\Airtime;
use App\Models\SpotAirtime;
use App\Models\Channel;
use App\Models\SiteAnalytics;
use App\Models\Language;
use Carbon\Carbon;
use App\Jobs\LogSiteAnalytics;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Sentry;

class NetworkController extends Controller
{
    public function getIndex()
    {
        $user = Sentry::getUser();

        $site_analytics = [
            'type' => 'network search',
            'user_id' => $user->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $language_id = request()->get('language_id') ?: 1;
        $language = Language::findOrFail($language_id);

        if (! $user->hasAccess('reports.'.strtolower($language->name).'-networks')) {
            return abort('401', 'Access denied - You do not have access to this report.');
        }

        $networks = Channel::where(function ($query) {
                $query->where('spot_active', 1)
                    ->orWhere('info_active', 1)
                    ->orWhere('capturing', 1);
        })
            ->where('language_id', $language_id)

            ->orderBy('name')
            ->get();


        return view('cxp.networks.all', compact('language_id', 'networks', 'language'));
    }

    public function getNetwork($networkId)
    {
        $user = Sentry::getUser();

        if (! $user->hasAccess('reports.network-scheduler')) {
            return abort('401', 'Access denied - You do not have access to this report.');
        }

        if (request()->filled('start-date') and request()->filled('end-date')) {
            $start_date = Carbon::createFromFormat("F j, Y", request()->get('start-date'))->startOfDay();
            $end_date = Carbon::createFromFormat("F j, Y", request()->get('end-date'))->endOfDay();

            if ($start_date->gte($end_date)) {
                return redirect()->route('networks.show', $networkId)->with('error', 'Start date must be before end date.');
            }
        } else {
            $start_date = lastFriday()->subWeek()->addDay()->startOfDay();
            $end_date = lastFriday()->endOfDay();
        }

        $allowed_start = data_allowed($start_date);
        $allowed_end = data_allowed($end_date);
        if (! $allowed_start or ! $allowed_end) {
            return abort('401', 'Access denied - Date outside of range');
        }

        //Get Fridays
        $fridays = fridayRange($start_date, $end_date);

        $site_analytics = [
            'type' => 'network',
            'type_id' => $networkId,
            'user_id' => $user->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $language_id = request()->get('language_id') ?: 1;
        $language = Language::findOrFail($language_id);
        if (! $user->hasAccess('reports.'.strtolower($language->name).'-networks')) {
            return abort('401', 'Access denied - You do not have access to this report.');
        }

        $network = Channel::findOrFail($networkId);

        $airtimes = $this->airtimes($network->id, $start_date, $end_date);
        $spotAirtimes = $this->spotAirtimes($network->id, $start_date, $end_date);
        $showAirings = $this->showAirings($network->id, $start_date, $end_date);
        $spotAirings = $this->spotAirings($network->id, $start_date, $end_date);
        $showPie = $this->showPie($network->id, $start_date, $end_date);
        $spotPie = $this->spotPie($network->id, $start_date, $end_date);
        $weeklySpotBreakdown = $this->weeklySpotBreakdown($network->id, $start_date, $end_date, $fridays);
        $weeklyShowBreakdown = $this->weeklyShowBreakdown($network->id, $fridays);

        return view(
            'cxp.networks.show',
            compact(
                'language_id',
                'network',
                'start_date',
                'end_date',
                'airtimes',
                'showPie',
                'spotPie',
                'showAirings',
                'spotAirings',
                'spotAirtimes',
                'weeklySpotBreakdown',
                'weeklyShowBreakdown'
            )
        );
    }

    private function airtimes($channel_id, $start_date, $end_date)
    {
        return Airtime::select(
            DB::raw('COUNT(airtimes.id) as airs'),
            DB::raw('sum(airtimes.cost)/2000 as media_index'),
            DB::raw('sum(cost) as total_cost'),
            DB::raw('count(DISTINCT channel_id) as channels')
        )
            ->where('airtimes.channel_id', $channel_id)
            ->where('airtimes.verified', 1)
            ->whereBetween(
                'air_date',
                [$start_date->format("Y-m-d H:i:s"), $end_date->format("Y-m-d H:i:s")]
            )
            ->first();
    }

    private function spotAirtimes($channel_id, $start_date, $end_date)
    {
        return SpotAirtime::select(
            DB::raw('COUNT(spot_airtimes.id) as airs'),
            DB::raw('COUNT(DISTINCT spot_version_id) as unique_spots')
        )
            ->where('spot_airtimes.channel_id', $channel_id)
            ->whereBetween(
                'spot_airtimes.air_date',
                [$start_date->format("Y-m-d"), $end_date->format("Y-m-d")]
            )
            ->first();
    }

    private function showPie($channel_id, $start_date, $end_date)
    {
        $shows = Airtime::select(
            'program_versions.grid_title',
            DB::raw('count(airtimes.id) as freq')
        )
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->where('airtimes.channel_id', $channel_id)
            ->where('airtimes.verified', 1)
            ->whereBetween(
                'air_date',
                [$start_date->format("Y-m-d H:i:s"), $end_date->format("Y-m-d H:i:s")]
            )
            ->groupBy('program_versions.program_id')
            ->orderBy('freq', 'desc')
            ->take(10)

            ->get();

        return $shows;
    }

    private function spotPie($channel_id, $start_date, $end_date)
    {
        $spots = SpotAirtime::select(
            'spot_versions.title',
            DB::raw('count(spot_airtimes.id) as freq')
        )
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->where('spot_airtimes.channel_id', $channel_id)
            ->whereBetween(
                'spot_airtimes.air_date',
                [$start_date->format("Y-m-d"), $end_date->format("Y-m-d")]
            )
            ->groupBy('spot_versions.spot_id')
            ->orderBy('freq', 'desc')
            ->take(10)

            ->get();

        return $spots;
    }

    private function weeklyShowBreakdown($channel_id, $fridays)
    {
        $weeklyBreakdown = [];
        $i = 0;
        foreach ($fridays as $friday) {
            $end_friday = Carbon::createFromFormat("Y-m-d", $friday)->endOfDay();
            $start_saturday = $end_friday->copy()->previous(Carbon::SATURDAY)->startOfDay();

            $breakdown = Airtime::select(
                DB::raw('COUNT(airtimes.id) as airs'),
                DB::raw('SUM(airtimes.cost)/2000 as media_index'),
                DB::raw('SUM(airtimes.cost) as total_cost'),
                DB::raw('COUNT(DISTINCT program_version_id)  as unique_programs')
            )
                ->where('airtimes.channel_id', $channel_id)
                ->where('airtimes.verified', 1)
                ->whereBetween(
                    'air_date',
                    [$start_saturday->format('Y-m-d H:i:s'), $end_friday->format('Y-m-d H:i:s')]
                )
                ->first();

            $weeklyBreakdown[$i]['end_date'] = $end_friday;
            $weeklyBreakdown[$i]['airs'] = $breakdown->airs;
            $weeklyBreakdown[$i]['media_index'] = $breakdown->media_index;
            $weeklyBreakdown[$i]['total_cost'] = $breakdown->total_cost;
            $weeklyBreakdown[$i]['unique_shows'] = $breakdown->unique_programs;
            $i++;
        }

        return $weeklyBreakdown;
    }

    private function weeklySpotBreakdown($channel_id, $start_date, $end_date, $fridays)
    {
        $weeklyBreakdown = [];
        $i = 0;
        foreach ($fridays as $friday) {
            $end_friday = Carbon::createFromFormat("Y-m-d", $friday)->endOfDay();
            $start_saturday = $end_friday->copy()->previous(Carbon::SATURDAY)->startOfDay();

            $breakdown = SpotAirtime::select(
                DB::raw('COUNT(spot_airtimes.id) as airs'),
                DB::raw('COUNT(DISTINCT spot_version_id)  as unique_spots')
            )
                ->where('spot_airtimes.channel_id', $channel_id)
                ->whereBetween(
                    'air_date',
                    [$start_saturday->format('Y-m-d'), $end_friday->format('Y-m-d')]
                )
                ->first();

            $weeklyBreakdown[$i]['end_date'] = $end_friday;
            $weeklyBreakdown[$i]['airs'] = $breakdown->airs;
            $weeklyBreakdown[$i]['unique_spots'] = $breakdown->unique_spots;
            $i++;
        }

        return $weeklyBreakdown;
    }

    private function showAirings($channel_id, $start_date, $end_date)
    {
        $shows = Airtime::select(
            'program_versions.title',
            'program_versions.grid_title',
            'airtimes.air_date'
        )
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->where('airtimes.channel_id', $channel_id)
            ->where('airtimes.verified', 1)
            ->whereBetween(
                'air_date',
                [$start_date->format("Y-m-d H:i:s"), $end_date->format("Y-m-d H:i:s")]
            )
            ->whereNull('airtimes.deleted_at')
            ->orderBy('air_date', 'asc')

            ->get();

        return $shows;
    }

    private function spotAirings($channel_id, $start_date, $end_date)
    {
        $spots = SpotAirtime::select(
            'spot_versions.title',
            'spot_airtimes.air_date',
            'day_parts.name as daypart_name'
        )
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('day_parts', 'day_parts.id', '=', 'spot_airtimes.daypart_id')
            ->where('spot_airtimes.channel_id', $channel_id)
            ->whereBetween(
                'spot_airtimes.air_date',
                [$start_date->format("Y-m-d"), $end_date->format("Y-m-d")]
            )
            ->orderBy('spot_airtimes.air_date', 'asc')

            ->get();

        return $spots;
    }
}
