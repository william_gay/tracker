<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;
use Carbon\Carbon;
use App\Models\Category;
use App\Models\Channel;
use App\Models\Daypart;
use App\Models\Market;
use App\Models\Language;
use App\Models\SpotAirtime;
use DateRangePicker\Libraries\Range;

class AttributionTrackerController extends Controller
{

    private $channelFormPopulator;
    private $channelsToQuery;
    private $marketFormPopulator;
    private $marketToQuery;
    private $daypartFormPopulator;
    private $daypartToQuery;
    private $range;

    public function sharedVariables()
    {

        $this->marketToQuery = Language::getMarkets(Request::get('market_id'));
        if (! Sentry::getUser()->hasAccess('reports.spanish-shows') and Request::get('market_id') == 2) {
            $this->marketToQuery = 1;
            Redirect::route('network-analyzer.home')
                            ->with('error', 'Upgrade Your Subscription to Access the Spanish Demographic.');
        }
        if (! Sentry::getUser()->hasAccess('reports.australia') and Request::get('market_id') == 4) {
            $this->marketToQuery = 1;
            Redirect::route('network-analyzer.home')
                            ->with('error', 'Upgrade Your Subscription to Access the Australian Demographic.');
        }
        $this->marketFormPopulator = Language::marketFormPopulator();
        $this->channelFormPopulator = Channel::where('language_id', $this->marketToQuery)
            ->orderBy('name', 'asc')
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->pluck('name', 'id')
            ->toArray();

        $this->channelsToQuery = Channel::getChannels(Request::get('channels'), [1]);
        $this->daypartFormPopulator = Daypart::pluck('name', 'id')->toArray();
        $this->daypartToQuery = Daypart::pluck('id')->toArray();

        if (Request::filled('rangeStart')) {
            $startDate = Request::get('rangeStart');
            $endDate = Request::get('rangeEnd');
        } else {
            $startDate = 'May 9, 2015';
            $endDate = 'May 10, 2015';
        }

        $this->range = Range::getStartAndEnd($startDate, $endDate);

        View::share('channelFormPopulator', $this->channelFormPopulator);
        View::share('channelsToQuery', $this->channelsToQuery);
        View::share('network', Channel::find($this->channelsToQuery));
        View::share('marketFormPopulator', $this->marketFormPopulator);
        View::share('marketToQuery', $this->marketToQuery);
        View::share('daypartFormPopulator', $this->daypartFormPopulator);
        View::share('daypartToQuery', $this->daypartToQuery);
        View::share('range', $this->range);
        View::share('start_date', $this->range['start']);
        View::share('end_date', $this->range['end']);
    }

    public function getIndex()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $callAtt = $this->callAtt();
        $amazonAtt = $this->amazonAtt();

        return view('cxp.attribution-tracker.index')
            ->with('callAtt', $callAtt)
            ->with('amazonAtt', $amazonAtt);
    }

    public function getCall()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $drillDown = $this->generateDrilldown();

        return view('cxp.attribution-tracker.call')
            ->with('drillDown', $drillDown);
    }

    public function getWeb()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $drillDown = $this->generateDrilldown();

        return view('cxp.attribution-tracker.web')
            ->with('drillDown', $drillDown);
    }

    public function callAtt()
    {
        $startTime = Carbon::create('2015', '05', '09', '00', '00', '00');
        $endTime = Carbon::create('2015', '05', '10', '23', '59', '59');

        $data= new \Illuminate\Database\Eloquent\Collection;
        while ($startTime->lte($endTime)) {
            // Add random aound of minutes, add random network, and airs
            $data[] = [
                'date' => $startTime->format("m/d/Y h:i A"),
                'media' => rand(15, 30),
                'airs' => rand(15, 30)
            ];

            $startTime->addMinutes(rand(30, 60));
        }

        return $data;
    }

    public function amazonAtt()
    {
        $startTime = Carbon::create('2015', '05', '09', '00', '00', '00');
        $endTime = Carbon::create('2015', '05', '10', '23', '59', '59');

        $data= new \Illuminate\Database\Eloquent\Collection;
        while ($startTime->lte($endTime)) {
            // Add random aound of minutes, add random network, and airs
            $data[] = [
                'date' => $startTime->format("m/d/Y h:i A"),
                'media' => rand(15, 30),
                'airs' => rand(15, 30)
            ];

            $startTime->addMinutes(rand(30, 60));
        }

        return $data;
    }

    public function generateDrilldown()
    {
        $startTime = Carbon::create('2015', '04', '06', '23', '35', '00');
        $endTime = Carbon::create('2015', '04', '07', '05', '00', '00');

        $networks = new \Illuminate\Database\Eloquent\Collection(['Lifetime', 'Bravo', 'Pivot', 'SyFy', 'American Movie Channel']);

        $data= new \Illuminate\Database\Eloquent\Collection;
        while ($startTime->lte($endTime)) {
            // Add random aound of minutes, add random network, and airs
            $data[] = [
                'date' => $startTime->toDateTimeString(),
                strtolower($networks->random()) => rand(2, 50)
            ];

            $startTime->addMinutes(rand(2, 15));
        }

        return $data;
    }
}
