<?php

namespace App\Http\Controllers;

use App;
use Config;
use Illuminate\Support\Facades\DB;
use Request;
use Sentry;
use Queue;
use View;
use Carbon\Carbon;
use App\Models\Airtime;
use App\Models\Category;
use App\Models\Channel;
use App\Models\DashboardType;
use App\Models\Language;
use App\Models\Product;
use App\Models\Program;
use App\Models\Retail\Retailer;
use App\Models\Retail\RetailerProduct;
use App\Models\Retail\RetailerMonthly;
use App\Models\SpotDetection;
use App\Models\SpotRanking;
use App\Models\UserProfile;
use App\Models\Spot;
use App\Models\SpotAirtime;
use DateRangePicker\Libraries\Range;
use Illuminate\Support\Collection;
use App\Jobs\LogSiteAnalytics;

class NetworkAnalyzerController extends Controller
{
    private $channelFormPopulator;
    private $channelsToQuery;
    private $categoryFormPopulator;
    private $subcategoryFormPopulator;
    private $categoriesToQuery;
    private $subcategoriesToQuery;
    private $singleCategory;
    private $marketFormPopulator;
    private $marketToQuery;
    private $programsToQuery;
    private $spotsToQuery;
    private $spotLengthsToQuery;
    private $range;

    public function sharedVariables()
    {
        $this->marketToQuery = Language::getMarkets(Request::get('market_id'));
        if (! Sentry::getUser()->hasAccess('reports.spanish-shows') and Request::get('market_id') == 2) {
            $this->marketToQuery = 1;
            return redirect()->route('network-analyzer.home')
                ->with('error', 'Upgrade Your Subscription to Access the Spanish Demographic.');
        }
        if (! Sentry::getUser()->hasAccess('reports.australia') and Request::get('market_id') == 4) {
            $this->marketToQuery = 1;
            return redirect()->route('network-analyzer.home')
                ->with('error', 'Upgrade Your Subscription to Access the Australian Demographic.');
        }
        $this->marketFormPopulator = Language::marketFormPopulator();
        $this->channelFormPopulator = Channel::where('language_id', $this->marketToQuery)
            ->orderBy('name', 'asc')
            //->where('capturing', 1)
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->pluck('name', 'id')->toArray();

        $this->channelsToQuery = Channel::getChannels(request('ch'), [1]);
        $this->singleCategory = Category::find('singleCategory');
        $this->categoryFormPopulator = Category::whereNull('parent_id')->pluck('name', 'id')->toArray();
        $this->categoriesToQuery = Category::getCategories(request('ct'), [1]);
        // subcategories
        $this->subcategoriesToQuery = request('sct') ?: Category::whereIn('parent_id', $this->categoriesToQuery)
            ->orderBy('name')->pluck('id')->toArray();
        $this->subcategoryFormPopulator = Category::with('parent')->whereIn('parent_id', $this->categoriesToQuery)->get()
            ->mapWithKeys(function ($category) {
                return [$category->id => $category->parent->name . ' - ' . $category->name];
            })->sort()->toArray();

        $this->spotLengthsToQuery = request('spot_length', [15,30,60,120]);

        if (request()->path() == 'network-analyzer/products' and
            ! Request::filled('rangeStart') and ! Request::filled('rangeStart')) {
            $lastFriday = Carbon::now()->previous(Carbon::FRIDAY);

            $this->range = Range::getStartAndEnd($lastFriday->copy()->subDay()->format("F j, Y"), $lastFriday->format("F j, Y"));
        } else {
            $this->range = Range::getStartAndEnd(Request::get('rangeStart'), Request::get('rangeEnd'));
        }

        if (Request::filled('spt')) {
            $this->spotsToQuery = explode(',', request('spt'));
        } else {
            // Default to 3381 - Total Gym
            $this->spotsToQuery = [3381];
        }

        if (Request::filled('prgm')) {
            $this->programsToQuery = explode(',', request('prgm'));
        } else {
            // Default to 12404 - Wise Company Emergency Food
            $this->programsToQuery = [12404];
        }

        View::share('channelFormPopulator', $this->channelFormPopulator);
        View::share('channelsToQuery', $this->channelsToQuery);
        View::share('categoryFormPopulator', $this->categoryFormPopulator);
        View::share('subcategoryFormPopulator', $this->subcategoryFormPopulator);
        View::share('categoriesToQuery', $this->categoriesToQuery);
        View::share('subcategoriesToQuery', $this->subcategoriesToQuery);
        View::share('spotLengthsToQuery', $this->spotLengthsToQuery);
        View::share('singleCategory', $this->singleCategory);
        View::share('marketFormPopulator', $this->marketFormPopulator);
        View::share('marketToQuery', $this->marketToQuery);
        View::share('range', $this->range);
        View::share('spotsToQuery', $this->spotsToQuery);
        View::share('programsToQuery', $this->programsToQuery);
    }

    public function byAiringsShortForm()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $site_analytics = [
            'type' => 'Network Analyzer - Airings',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $ShortFormAiringsOverTime = SpotAirtime::select([
            DB::raw('DATE(spot_airtimes.air_date) AS air_date'),
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
            ->whereIn('spot_versions.sub_category_id', $this->subcategoriesToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy(DB::raw('DATE(spot_airtimes.air_date)'))
            ->orderBy('air_date', 'asc')
            ->get();

        $shortForms = SpotAirtime::select([
            'channels.name as channel_name',
            'channels.id as channel_id',
            'spot_versions.id as spot_id',
            'spot_versions.title as spot_title',
            'spot_versions.length',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
            ->whereIn('spot_versions.sub_category_id', $this->subcategoriesToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy('spot_airtimes.channel_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $shortFormProgramsByAirings = SpotAirtime::select([
            'spot_versions.title as spot_title',
            'spot_versions.id as spot_id',
            'spot_versions.length',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
            ->whereIn('spot_versions.sub_category_id', $this->subcategoriesToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy('spot_versions.spot_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $newShortFormSpotsByNetwork = SpotAirtime::select([
            'spot_versions.title as spot_title',
            'spot_versions.id as spot_id',
            'spot_versions.length',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
            ->whereIn('spot_versions.sub_category_id', $this->subcategoriesToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->where('spot_versions.initial_date', '>=', $this->range['start'])
            ->where('spot_versions.initial_date', '<', $this->range['end'])
            ->groupBy('spot_versions.spot_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $profileData = subscription_profile();
        $userProfile = UserProfile::firstOrCreate(['user_id' => Sentry::getUser()->id]);
        $dashboardType = DashboardType::find($userProfile->dashboardtype_id ?: 1);


        if (Sentry::getUser()->hasAccess('reports.network-report')) {
            return view(
                'cxp.networkanalyzer.short-form',
                compact(
                    'profileData',
                    'dashboardType',
                    'shortForms',
                    'shortFormProgramsByAirings',
                    'ShortFormAiringsOverTime',
                    'newShortFormSpotsByNetwork'
                )
            )
                ->with('start_date', $this->range['start'])
                ->with('end_date', $this->range['end']);
        } else {
            return abort('401', 'You do not have permissions to view this page.')
                ->with('warning', 'Call or email to upgrade your subscription.');
        }
    }

    public function byAiringsLongForm()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $site_analytics = [
            'type' => 'Network Analyzer - Airings',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $longFormAiringsOverTime = Airtime::select([
            DB::raw('DATE(air_date) as air_date'),
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->whereIn('program_versions.sub_category_id', $this->subcategoriesToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->verified()
            ->groupBy(DB::raw('DATE(air_date)'))
            ->orderBy('air_date', 'asc')

            ->get();

        $longForms = Airtime::select([
            'channels.name as channel_name',
            'channels.id as channel_id',
            'program_versions.title as program_title',
            'program_versions.id as program_id',
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->whereIn('program_versions.sub_category_id', $this->subcategoriesToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy('airtimes.channel_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $newLongFormProgramsBynetwork = Airtime::with(['programVersion', 'programVersion.marketingCompany'])
            ->select([
                'airtimes.program_version_id',
                'program_versions.title as program_title',
                'program_versions.id as program_id',
                'categories.name as category_name',
                'categories.id as category_id',
                DB::raw('COUNT(airtimes.id) AS air_count'),
                DB::raw('SUM(airtimes.cost) AS air_cost')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->whereIn('program_versions.sub_category_id', $this->subcategoriesToQuery)
            ->where('program_versions.initial_date', '>=', $this->range['start'])
            ->where('program_versions.initial_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy('program_versions.program_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $longFormProgramsByAirings = Airtime::select([
            'program_versions.title as program_title',
            'program_versions.id as program_id',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(DISTINCT program_versions.id) as program_versions'),
            DB::raw('COUNT(DISTINCT airtimes.channel_id) as channel_count'),
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        ])
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('programs', 'programs.id', '=', 'program_versions.program_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->whereIn('program_versions.sub_category_id', $this->subcategoriesToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy('program_versions.program_id')
            ->orderBy('air_count', 'desc')
            ->get();


        $profileData = subscription_profile();
        $userProfile = UserProfile::firstOrCreate(['user_id' => Sentry::getUser()->id]);
        $dashboardType = DashboardType::find($userProfile->dashboardtype_id ?: 1);


        if (Sentry::getUser()->hasAccess('reports.network-report')) {
            return view(
                'cxp.networkanalyzer.long-form',
                compact(
                    'profileData',
                    'dashboardType',
                    'longForms',
                    'longFormProgramsByAirings',
                    'longFormAiringsOverTime',
                    'newLongFormProgramsBynetwork'
                )
            )
                ->with('start_date', $this->range['start'])
                ->with('end_date', $this->range['end']);
        } else {
            return abort('401', 'You do not have permissions to view this page.')
                ->with('warning', 'Call or email to upgrade your subscription.');
        }
    }

    public function getNetworkShow($id)
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $network = Channel::find($id);
        $marketToQuery = $network->language_id;
        $allNetworks = Channel::where('capturing', 1)
            ->where('language_id', $network->language_id)
            ->orderBy('name', 'asc')
            ->get();

        $site_analytics = [
            'type'    => 'Network Analyzer',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all()),
            'type_id' => $network->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $longFormAiringsOverTime = Airtime::select(
            'program_versions.title as title',
            DB::raw('DATE(airtimes.air_date) as air_date'),
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        )
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->where('airtimes.verified', 1)
            ->where('airtimes.channel_id', $network->id)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->verified()
            ->groupBy(DB::raw('DATE(air_date)'))
            ->orderBy('air_date', 'asc')

            ->get();

        $longFormCategoriesByNetwork = Airtime::select(
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        )
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->where('airtimes.channel_id', $network->id)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->verified()
            ->groupBy('program_versions.category_id')
            ->orderBy('air_count', 'desc')

            ->get();

        $longFormProgramsBynetwork = Airtime::with('programVersion', 'programVersion.marketingCompany')
            ->select(
                'airtimes.program_version_id',
                'program_versions.title as program_title',
                'program_versions.id as program_id',
                'categories.name as category_name',
                'categories.id as category_id',
                DB::raw('COUNT(airtimes.id) AS air_count'),
                DB::raw('SUM(airtimes.cost) AS air_cost')
            )
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->where('airtimes.channel_id', $network->id)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->verified()
            ->groupBy('program_versions.program_id')
            ->orderBy('air_count', 'desc')

            ->get();

        $newLongFormProgramsBynetwork = Airtime::with('programVersion', 'programVersion.marketingCompany')
            ->select(
                'airtimes.program_version_id',
                'program_versions.title as program_title',
                'program_versions.id as program_id',
                'categories.name as category_name',
                'categories.id as category_id',
                DB::raw('COUNT(airtimes.id) AS air_count'),
                DB::raw('SUM(airtimes.cost) AS air_cost')
            )
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->where('airtimes.channel_id', $network->id)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->where('program_versions.initial_date', '>=', $this->range['start'])
            ->where('program_versions.initial_date', '<', $this->range['end'])
            ->verified()
            ->groupBy('program_versions.program_id')
            ->orderBy('air_count', 'desc')

            ->get();

        $shortFormAiringsOverTime = SpotAirtime::select([
            'spot_versions.title as title',
            'spot_versions.length',
            DB::raw('DATE(spot_airtimes.air_date) as air_date'),
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->where('spot_airtimes.channel_id', $network->id)
            ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy(DB::raw('DATE(spot_airtimes.air_date)'))
            ->orderBy('spot_airtimes.air_date', 'asc')
            ->get();

        $shortFormCategoriesByNetwork = SpotAirtime::select([
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->where('spot_airtimes.channel_id', $network->id)
            ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy('spot_versions.category_id')
            ->orderBy('air_count', 'desc')

            ->get();

        $shortFormSpotsBynetwork = SpotAirtime::select([
            'spot_versions.title as spot_title',
            'spot_versions.id as spot_id',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->where('spot_airtimes.channel_id', $network->id)
            ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy('spot_versions.spot_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $newShortFormSpotsByNetwork = SpotAirtime::select([
            'spot_versions.title as spot_title',
            'spot_versions.id as spot_id',
            'spot_versions.length',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->where('spot_airtimes.channel_id', $network->id)
            ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
            ->where('spot_versions.initial_date', '>=', $this->range['start'])
            ->where('spot_versions.initial_date', '<', $this->range['end'])
            ->groupBy('spot_versions.spot_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $productsByNetwork = null;

        return view(
            'cxp.networkanalyzer.show',
            compact(
                'network',
                'longFormCategoriesByNetwork',
                'shortFormCategoriesByNetwork',
                'longFormProgramsBynetwork',
                'shortFormSpotsBynetwork',
                'productsByNetwork',
                'allNetworks',
                'longFormAiringsOverTime',
                'shortFormAiringsOverTime',
                'newLongFormProgramsBynetwork',
                'shortFormSpotsBynetwork',
                'newShortFormSpotsByNetwork'
            )
        )
            ->with('start_date', $this->range['start'])
            ->with('end_date', $this->range['end']);
    }

    public function byCategoryShortForm()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $site_analytics = [
            'type'    => 'Network Analyzer - Categories',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $shortFormAiringsOverTime = SpotAirtime::select([
            DB::raw('DATE(spot_airtimes.air_date) as air_date'),
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('category_id', $this->categoriesToQuery)
            ->whereIn('sub_category_id', $this->subcategoriesToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy(DB::raw('DATE(spot_airtimes.air_date)'))
            ->orderBy('air_date', 'asc')
            ->get();

        $shortForms = SpotAirtime::select([
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('spot_versions as ppv', 'ppv.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'ppv.category_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('category_id', $this->categoriesToQuery)
            ->whereIn('sub_category_id', $this->subcategoriesToQuery)
            ->whereIn('length', $this->spotLengthsToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy('ppv.category_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $shortFormNetworksByCategory = SpotAirtime::select([
            'channels.name as channel_name',
            'channels.id as channel_id',
            'spot_versions.title as spot_title',
            'spot_versions.length',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('category_id', $this->categoriesToQuery)
            ->whereIn('sub_category_id', $this->subcategoriesToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy('channels.id')
            ->orderBy('air_count', 'desc')
            ->get();

        $newShortFormSpotsByNetwork = SpotAirtime::select([
            'spot_versions.title as spot_title',
            'spot_versions.id as spot_id',
            'spot_versions.length',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('category_id', $this->categoriesToQuery)
            ->whereIn('sub_category_id', $this->subcategoriesToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->where('spot_versions.initial_date', '>=', $this->range['start'])
            ->where('spot_versions.initial_date', '<', $this->range['end'])
            ->groupBy('spot_versions.spot_id')
            ->orderBy('air_count', 'desc')
            ->get();

        return view(
            'cxp.networkanalyzer.categories.short-form',
            compact(
                'shortForms',
                'shortFormNetworksByCategory',
                'shortFormAiringsOverTime',
                'newShortFormSpotsByNetwork'
            )
        )
            ->with('start_date', $this->range['start'])
            ->with('end_date', $this->range['end']);
    }

    public function byCategoryLongForm()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $site_analytics = [
            'type'    => 'Network Analyzer - Categories',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $longFormAiringsOverTime = Airtime::select([
            DB::raw('DATE(airtimes.air_date) as air_date'),
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('category_id', $this->categoriesToQuery)
            ->whereIn('sub_category_id', $this->subcategoriesToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->verified()
            ->groupBy(DB::raw('DATE(airtimes.air_date)'))
            ->orderBy('air_date', 'asc')
            ->get();

        $longForms = Airtime::select([
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        ])
            ->leftJoin('program_versions as ppv', 'ppv.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'ppv.category_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('category_id', $this->categoriesToQuery)
            ->whereIn('sub_category_id', $this->subcategoriesToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy('ppv.category_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $newLongFormProgramsBynetwork = Airtime::with([
            'programVersion',
            'programVersion.marketingCompany'
        ])
            ->select([
                'airtimes.program_version_id',
                'program_versions.title as program_title',
                'program_versions.id as program_id',
                'categories.name as category_name',
                'categories.id as category_id',
                DB::raw('COUNT(airtimes.id) AS air_count'),
                DB::raw('SUM(airtimes.cost) AS air_cost')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->whereIn('program_versions.sub_category_id', $this->subcategoriesToQuery)
            ->where('program_versions.initial_date', '>=', $this->range['start'])
            ->where('program_versions.initial_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy('program_versions.program_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $longFormNetworksByCategory = Airtime::select([
            'channels.name as channel_name',
            'channels.id as channel_id',
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('category_id', $this->categoriesToQuery)
            ->whereIn('sub_category_id', $this->subcategoriesToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->verified()
            ->groupBy('channels.id')
            ->orderBy('air_count', 'desc')
            ->get();

        return view(
            'cxp.networkanalyzer.categories.long-form',
            compact(
                'longForms',
                'longFormNetworksByCategory',
                'longFormAiringsOverTime',
                'newLongFormProgramsBynetwork'
            )
        )
            ->with('start_date', $this->range['start'])
            ->with('end_date', $this->range['end']);
    }

    public function getCategoryShow($id)
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $category      = Category::find($id);
        $allCategories = Category::orderBy('name', 'asc')->whereNull('parent_id')->get();

        $site_analytics = [
            'type'    => 'Network Analyzer - Categories' ,
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all()),
            'type_id' => $category->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $longFormAiringsOverTime = Airtime::select([
            DB::raw('DATE(airtimes.air_date) as air_date'),
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        ])
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->where('airtimes.verified', 1)
            ->where('categories.id', $category->id)
            ->whereIn('channels.id', $this->channelsToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy(DB::raw('DATE(airtimes.air_date)'))
            ->orderBy('air_date', 'asc')
            ->get();

        $longFormNetworksByCategory = Airtime::select([
            'channels.name as channel_name',
            'channels.id as channel_id',
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost'),
        ])
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->where('airtimes.verified', 1)
            ->where('category_id', $category->id)
            ->whereIn('channels.id', $this->channelsToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy('channels.id')
            ->orderBy('air_count', 'desc')
            ->get();

        $longFormProgramsByCategory = Airtime::select([
            'program_versions.title as program_title',
            'program_versions.id as program_id',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost'),
        ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->where('category_id', $category->id)
            ->whereIn('channels.id', $this->channelsToQuery)
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy('program_versions.program_id')
            ->orderBy('air_count', 'desc')
            ->get();


        $shortFormAiringsOverTime = SpotAirtime::select([
            DB::raw('DATE(spot_airtimes.air_date) as air_date'),
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->whereIn('channels.id', $this->channelsToQuery)
            ->where('categories.id', $category->id)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->whereNull('spot_airtimes.deleted_at')
            ->groupBy(DB::raw('DATE(spot_airtimes.air_date)'))
            ->orderBy('spot_airtimes.air_date', 'asc')
            ->get();

        $shortFormNetworksByCategory = SpotAirtime::select([
            'channels.name as channel_name',
            'channels.id as channel_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->where('category_id', $category->id)
            ->whereIn('channels.id', $this->channelsToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->whereNull('spot_airtimes.deleted_at')
            ->groupBy('channels.id')
            ->orderBy('air_count', 'desc')
            ->get();

        $shortFormSpotsByCategory = SpotAirtime::select([
            'spot_versions.title as spot_title',
            'spot_versions.id as spot_id',
            'spot_versions.length',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->where('category_id', $category->id)
            ->whereIn('channels.id', $this->channelsToQuery)
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->whereNull('spot_airtimes.deleted_at')
            ->groupBy('spot_versions.spot_id')
            ->orderBy('air_count', 'desc')

            ->get();

        $newLongFormProgramsBynetwork = Airtime::with('programVersion', 'programVersion.marketingCompany')
            ->select([
                'airtimes.program_version_id',
                'program_versions.title as program_title',
                'program_versions.id as program_id',
                'categories.name as category_name',
                'categories.id as category_id',
                DB::raw('COUNT(airtimes.id) AS air_count'),
                DB::raw('SUM(airtimes.cost) AS air_cost')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->where('category_id', $category->id)
            ->whereIn('channels.id', $this->channelsToQuery)
            ->where('program_versions.initial_date', '>=', $this->range['start'])
            ->where('program_versions.initial_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy('program_versions.program_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $newShortFormSpotsByNetwork = SpotAirtime::select([
            'spot_versions.title as spot_title',
            'spot_versions.id as spot_id',
            'spot_versions.length',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->where('category_id', $category->id)
            ->whereIn('channels.id', $this->channelsToQuery)
            ->where('spot_versions.initial_date', '>=', $this->range['start'])
            ->where('spot_versions.initial_date', '<', $this->range['end'])
            ->whereNull('spot_airtimes.deleted_at')
            ->groupBy('spot_versions.spot_id')
            ->orderBy('air_count', 'desc')
            ->get();

        return view(
            'cxp.networkanalyzer.categories.show',
            compact(
                'category',
                'longFormNetworksByCategory',
                'shortFormNetworksByCategory',
                'longFormProgramsByCategory',
                'shortFormSpotsByCategory',
                'allCategories',
                'longFormAiringsOverTime',
                'shortFormAiringsOverTime',
                'newShortFormSpotsByNetwork',
                'newLongFormProgramsBynetwork'
            )
        )
            ->with('start_date', $this->range['start'])
            ->with('end_date', $this->range['end']);
    }

    public function byProductsShortForm()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $site_analytics = [
            'type'    => 'Network Analyzer - Products',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $shortFormAiringsOverTime = SpotAirtime::select([
            DB::raw('DATE(spot_airtimes.air_date) as air_date'),
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->when($this->spotsToQuery, function ($query) {
                return $query->whereIn('spot_versions.spot_id', $this->spotsToQuery);
            })
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy('air_date')
            ->orderBy('air_date', 'asc')
            ->get();

        $shortForms = SpotAirtime::select([
            'spot_versions.title as spot_title',
            'spot_versions.id as spot_id',
            'spot_versions.length',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->when($this->spotsToQuery, function ($query) {
                return $query->whereIn('spot_versions.spot_id', $this->spotsToQuery);
            })
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy('spot_versions.spot_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $shortFormNetworksByCategory = SpotAirtime::select([
            'channels.name as channel_name',
            'channels.id as channel_id',
            'spot_versions.title as spot_title',
            'spot_versions.length',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->when($this->spotsToQuery, function ($query) {
                return $query->whereIn('spot_versions.spot_id', $this->spotsToQuery);
            })
            ->where('spot_airtimes.air_date', '>=', $this->range['start'])
            ->where('spot_airtimes.air_date', '<', $this->range['end'])
            ->groupBy('channels.id')
            ->orderBy('air_count', 'desc')
            ->get();

        $newShortFormSpotsByNetwork = SpotAirtime::select([
            'spot_versions.title as spot_title',
            'spot_versions.id as spot_id',
            'spot_versions.length',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(spot_airtimes.id) AS air_count'),
            DB::raw('SUM(spot_airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('spot_versions.length', $this->spotLengthsToQuery)
            ->when($this->spotsToQuery, function ($query) {
                return $query->whereIn('spot_versions.spot_id', $this->spotsToQuery);
            })
            ->where('spot_versions.initial_date', '>=', $this->range['start'])
            ->where('spot_versions.initial_date', '<', $this->range['end'])
            ->groupBy('spot_versions.spot_id')
            ->orderBy('air_count', 'desc')
            ->get();


        return view(
            'cxp.networkanalyzer.products.short-form',
            compact(
                'shortForms',
                'shortFormNetworksByCategory',
                'shortFormAiringsOverTime',
                'newShortFormSpotsByNetwork'
            )
        )
            ->with('start_date', $this->range['start'])
            ->with('end_date', $this->range['end']);
    }

    public function byProductsLongForm()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $site_analytics = [
            'type'    => 'Network Analyzer - Products',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $longFormAiringsOverTime = Airtime::select([
            DB::raw('DATE(airtimes.air_date) as air_date'),
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->when($this->programsToQuery, function ($query) {
                $query->whereIn('program_versions.program_id', $this->programsToQuery);
            })
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->verified()
            ->groupBy(DB::raw('DATE(airtimes.air_date)'))
            ->orderBy('air_date', 'asc')
            ->get();

        $longForms = Airtime::select([
            'program_versions.title as program_title',
            'program_versions.id as program_id',
            'categories.name as category_name',
            'categories.id as category_id',
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        ])
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->when($this->programsToQuery, function ($query) {
                $query->whereIn('program_versions.program_id', $this->programsToQuery);
            })
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy('program_versions.program_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $newLongFormProgramsBynetwork = Airtime::with('programVersion', 'programVersion.marketingCompany')
            ->select([
                'airtimes.program_version_id',
                'program_versions.title as program_title',
                'program_versions.id as program_id',
                'categories.name as category_name',
                'categories.id as category_id',
                DB::raw('COUNT(airtimes.id) AS air_count'),
                DB::raw('SUM(airtimes.cost) AS air_cost')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->when($this->programsToQuery, function ($query) {
                $query->whereIn('program_versions.program_id', $this->programsToQuery);
            })
            ->where('program_versions.initial_date', '>=', $this->range['start'])
            ->where('program_versions.initial_date', '<', $this->range['end'])
            ->whereNull('airtimes.deleted_at')
            ->verified()
            ->groupBy('program_versions.program_id')
            ->orderBy('air_count', 'desc')
            ->get();

        $longFormNetworksByCategory = Airtime::select([
            'channels.name as channel_name',
            'channels.id as channel_id',
            DB::raw('COUNT(airtimes.id) AS air_count'),
            DB::raw('SUM(airtimes.cost) AS air_cost')
        ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->where('airtimes.verified', 1)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->when($this->programsToQuery, function ($query) {
                $query->whereIn('program_versions.program_id', $this->programsToQuery);
            })
            ->where('airtimes.air_date', '>=', $this->range['start'])
            ->where('airtimes.air_date', '<', $this->range['end'])
            ->verified()
            ->groupBy('channels.id')
            ->orderBy('air_count', 'desc')
            ->get();

        return view(
            'cxp.networkanalyzer.products.long-form',
            compact(
                'longForms',
                'longFormNetworksByCategory',
                'longFormAiringsOverTime',
                'newLongFormProgramsBynetwork'
            )
        )
            ->with('start_date', $this->range['start'])
            ->with('end_date', $this->range['end']);
    }
}
