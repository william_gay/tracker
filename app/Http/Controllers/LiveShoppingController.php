<?php

namespace App\Http\Controllers;

use DataTables;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Facades\DB;
use Sentry;
use Carbon\Carbon;
use App\Models\Channel;
use App\Models\LiveShoppingTracker\LiveShoppingCategory;
use App\Models\LiveShoppingTracker\LiveShoppingProduct;
use App\Models\LiveShoppingTracker\LiveShoppingProductVersion;
use App\Models\LiveShoppingTracker\LiveShoppingAirtimeProduct;
use App\Models\LiveShoppingTracker\LiveShoppingAirtime;
use App\Models\LiveShoppingTracker\LiveShoppingShow;
use DateRangePicker\Libraries\Range;
use App\Libraries\Lst;
use App\Jobs\LogSiteAnalytics;

use App\Http\Controllers\Controller;

class LiveShoppingController extends Controller
{
    protected $categories;
    protected $channels;
    protected $range;
    protected $dsv;

    public function __construct()
    {
        $this->categories = LiveShoppingCategory::whereNull('parent_id')->orderBy('name')->pluck('name', 'id')->toArray();
        view()->share('categories', $this->categories);
        $this->categoriesToQuery = LiveShoppingCategory::getCategories(request('categories'), [1]);
        view()->share('categoriesToQuery', $this->categoriesToQuery);

        $this->channels = Channel::whereHas('language', function ($q) {
                $q->where('slug', 'live-shopping');
        })
            ->orderBy('name')
            ->pluck('name', 'id')
            ->toArray();
        view()->share('channels', $this->channels);

        // TODO: Remove hardcoded to 6
        $this->channelsToQuery = Channel::getChannels(request('channels'), [6]);
        view()->share('channelsToQuery', $this->channelsToQuery);

        $channelsSelected = Channel::whereIn('id', $this->channelsToQuery)->pluck('name');
        view()->share('channelsSelected', $channelsSelected);

        $range = [];
        $yesterday = Carbon::yesterday();
        $range['start'] = request()->filled('rangeStart') ? request('rangeStart'): $yesterday->subWeeks(1)->toFormattedDateString();
        $range['end'] = request()->filled('rangeEnd') ? request('rangeEnd'): $yesterday->addWeeks(1)->toFormattedDateString();
        //for debugging ****************
        //$range['start'] = Carbon::createFromDate(2015, 12, 1);

        if (request()->filled('DSV')) {
            $this->dsv = true;
        } else {
            $this->dsv = false;
        }

        $this->range = Range::getStartAndEnd($range['start'], $range['end']);
        view()->share('range', $this->range);
    }

    public function getIndex()
    {
        $site_analytics = [
            'type' => 'Live Shopping Tracker - Index',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(request()->all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $allCategories = LiveShoppingCategory::orderBy('name', 'asc')->whereNull('parent_id')->get();
        $queryCollection = Lst::topCategoriesByChannel($this->categoriesToQuery, $this->channelsToQuery, $this->range, request('DSV'));

        // get all top categories for chart/table reference
        $chartCategories = $queryCollection->pluck('category_id', 'category_name')->unique();

        // put channel data into new array that AmCharts can parse
        $chartChannels = [];
        $queryCollection = $queryCollection->groupBy('channel_name');
        foreach ($queryCollection as $channel => $channelData) {
            $subCollection = collect($channelData);
            $channelCollection = collect();
            $channelCollection->put('channelAirs', $subCollection->sum('airs'));
            // loop only contains one channels data per iteration so [0]->id/name works
            $channelCollection->put('channelName', $subCollection->first()->channel_name);
            $channelCollection->put('channelId', $subCollection->first()->channel_id);
            $subCollection->each(function ($item) use ($channelCollection) {
                $channelCollection->put($item->category_name, (int)$item->airs);
            });
            $chartChannels[$subCollection->first()->channel_id] = $channelCollection->toArray();
        }

        return view('cxp.lst.index')
            ->with('topCategoriesByChannel', array_values($chartChannels))
            ->with('topCategories', $chartCategories)
            ->with('allCategories', $allCategories);
    }

    public function getCategories()
    {
        $site_analytics = [
            'type' => 'Live Shopping Tracker - Networks',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(request()->all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $topCategories = LST::topCategories($this->categoriesToQuery, $this->channelsToQuery, $this->range);

        $topCategories->each(function ($item) {
            $item->url = route('lst.category', ['categoryId' => $item->category_id, 'channels' => $this->channelsToQuery, 'rangeStart' => $this->range['start']->format('F j, Y'), 'rangeEnd' => $this->range['end']->format('F j, Y')]);
        });

        return view('cxp.lst.categories')
            ->with('topCategories', $topCategories);
    }

    public function getCategory($categoryId)
    {
        // Lookup Category
        $category = LiveShoppingCategory::find($categoryId);
        // Check if it is a subscategory
        if ($category->parent_id != null) {
            $subCategory = $category;
            $category = $category->parent_category();
        } else {
            $subCategory = null;
        }
        $allCategories = LiveShoppingCategory::orderBy('name', 'asc')->whereNull('parent_id')->get();
        $allSubCategories = LiveShoppingCategory::orderBy('name', 'asc')->where('parent_id', '=', $categoryId)->get();

        $site_analytics = [
            'type' => 'Live Shopping Tracker - Category',
            'type_id' => $categoryId,
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(request()->all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $byChannel = LST::byChannel([$categoryId], $this->channelsToQuery, $this->range, 100000, \request('DSV'));

        // by airtime for am line chart of airs by date
        $byDate = LST::byDate([$categoryId], $this->channelsToQuery, $this->range, 100000, \request('DSV'));

        //format data for AM charts
        $airsPerDay = [];
        foreach ($byDate as $item) {
            if (isset($airsPerDay[$item->date])) {
                $airsPerDay[$item->date][$item->channel_name] = $item->total_airs;
            } else {
                $airsPerDay[$item->date] = [
                    'date' => $item->date,
                    $item->channel_name => $item->total_airs
                ];
            }
        }

        $products = LST::products([ $categoryId ], $this->channelsToQuery, $this->range, 10000, $this->dsv);

        return view('cxp.lst.category')
            ->with('allCategories', $allCategories)
            ->with('allSubCategories', $allSubCategories)
            ->with('category', $category)
            ->with('subCategory', $subCategory)
            ->with('byChannel', $byChannel)
            ->with('byDate', array_values($airsPerDay))
            ->with('products', $products);
    }

    public function getSubCategory($categoryId)
    {
        // Lookup Category
        $category = LiveShoppingCategory::find($categoryId);
        // Check if it is a subscategory
        if ($category->parent_id != null) {
            $subCategory = $category;
            $category = $category->parent_category();
        } else {
            $subCategory = null;
        }
        $allCategories = LiveShoppingCategory::orderBy('name', 'asc')->whereNull('parent_id')->get();
        $allSubCategories = LiveShoppingCategory::orderBy('name', 'asc')->where('parent_id', '=', $category->id)->get();

        $site_analytics = [
            'type' => 'Live Shopping Tracker - Category',
            'type_id' => $categoryId,
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(request()->all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $byChannel = LST::byChannel([$categoryId], $this->channelsToQuery, $this->range, 0, \request('DSV'));

        // by airtime for am line chart of airs by date
        $byDate = LST::byDate([$categoryId], $this->channelsToQuery, $this->range, 0, \request('DSV'));

        //format data for AM charts
        $airsPerDay = [];
        foreach ($byDate as $item) {
            if (isset($airsPerDay[$item->date])) {
                $airsPerDay[$item->date][$item->channel_name] = $item->total_airs;
            } else {
                $airsPerDay[$item->date] = [
                    'date' => $item->date,
                    $item->channel_name => $item->total_airs
                ];
            }
        }

        $products = LST::products([ $categoryId ], $this->channelsToQuery, $this->range, 10000, $this->dsv);

        return view('cxp.lst.sub-category')
            ->with('allCategories', $allCategories)
            ->with('allSubCategories', $allSubCategories)
            ->with('category', $category)
            ->with('subCategory', $subCategory)
            ->with('byChannel', $byChannel)
            ->with('products', $products)
            ->with('byDate', array_values($airsPerDay));
    }

    public function getProducts()
    {
        $site_analytics = [
            'type' => 'Live Shopping Tracker - Products',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(request()->all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $newProducts = LST::newProductsByCategory($this->categoriesToQuery, $this->channelsToQuery, $this->range, 10000, $this->dsv);
        $products = LST::newProducts($this->categoriesToQuery, $this->channelsToQuery, $this->range, 10000, $this->dsv);

        $newProducts->each(function ($item) {
            $item->url = route('lst.category', ['categoryId' => $item->category_id, 'channels' => $this->channelsToQuery, 'rangeStart' => $this->range['start']->format('F j, Y'), 'rangeEnd' => $this->range['end']->format('F j, Y')]);
        });

        return view('cxp.lst.products')
            ->with('newProducts', $newProducts)
            ->with('products', $products);
    }

    public function getProduct($productId)
    {
        $product = LiveShoppingProduct::find($productId);

        if (! $product) {
            return abort('404', 'This product does not exist.');
        }

        $site_analytics = [
            'type' => 'Live Shopping Tracker - Product',
            'type_id' => $product->id,
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(request()->all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        if (! $product) {
            return abort('404', 'This product does not exist.');
        }

        $versions = LiveShoppingProductVersion::where('live_shopping_product_id', $product->id)->get();

        $versionarr = [];
        $newId = 1;
        foreach ($versions as $version) {
            $versionarr[] = $version->id;
            $version->newId = $newId++;
        }

        $airings = LiveShoppingAirtimeProduct::with(
            [
                'airtime',
                'airtime.show'
            ]
        )
            ->select(
                [
                'live_shopping_airtime_id',
                'live_shopping_product_version_id',
                'live_shopping_airtimes.start_time as start_time',
                ]
            )
            ->leftJoin('live_shopping_airtimes', 'live_shopping_airtimes.id', '=', 'live_shopping_airtimes_products.live_shopping_airtime_id')
            ->whereIn('live_shopping_product_version_id', $versionarr)
            ->orderBy('live_shopping_airtimes_products.start_time', 'asc')
            ->get();

        foreach ($airings as $air) {
            $newId = array_search($air->productVersion->id, $versionarr) + 1;
            if ($newId) {
                $air->productVersion->newId = $newId;
            }
        }

        $channel = $airings->first()->airtime->channel;
            return view('cxp.lst.product')
                ->with('product', $product)
                ->with('versions', $versions)
                ->with('channel', $channel)
                ->with('airings', $airings);
    }

    public function getShow($showId)
    {
        $show = LiveShoppingShow::with('channel')->find($showId);

        $site_analytics = [
            'type' => 'Live Shopping Tracker - Show',
            'type_id' => $show->id,
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(request()->all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        return view('cxp.lst.show')
            ->with('show', $show);
    }

    public function getAirtime($airtimeId)
    {
        $airtime = LiveShoppingAirtime::with('channel', 'products', 'show')->find($airtimeId);

        $site_analytics = [
            'type' => 'Live Shopping Tracker - Airtime',
            'type_id' => $airtime->id,
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(request()->all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        return view('cxp.lst.airtime')
            ->with('airtime', $airtime);
    }

    public function getSearch(Builder $builder)
    {
        if (request()->ajax()) {
            return $this->getDatatables();
        }

        $html = $builder->columns([
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'network', 'name' => 'network', 'title' => 'Network'],
            ['data' => 'sku', 'name' => 'sku', 'title' => 'SKU'],
            ['data' => 'msrp', 'name' => 'msrp', 'title' => 'MSRP'],
            ['data' => 'category', 'name' => 'category', 'title' => 'Category'],
            ['data' => 'subCategory', 'name' => 'subCategory', 'title' => 'Sub-Category'],
            ['data' => 'first_airdate', 'name' => 'first_airdate', 'title' => 'First Detected'],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() {}',
            'autoWidth' => false,
            'responsive' => true,
            "order" => [[ 6, "desc" ]],
        ]);

        return view('cxp.lst.search')
            ->with('html', $html);
    }

    public function getDatatables()
    {
        $start_date = data_since();
        $end_date = data_to();

        $programs = LiveShoppingProduct::with(['versions', 'category', 'subCategory'])
            ->where('first_airdate', '>=', $start_date->startOfDay()->toDateTimeString())
            ->where('first_airdate', '<', $end_date->endOfDay()->toDateTimeString());

        return Datatables::of($programs)
            ->editColumn('name', '<a href="{{ route("lst.product", array($id)) }}">{{ $name }}</a> @if(Sentry::getUser()->isSuperUser()) <small><a href="{{ route("admin.lst.products.edit",array($id)) }}" target="_blank">Edit</a></small> @endif')
            ->addColumn('network', function ($program) {
                return $program->network;
            })
            ->editColumn('msrp', '<span title="{{ number_format($msrp, 2) }}">{{ $currency }}{{ $msrp }}</span>')
            ->editColumn('first_airdate', function ($program) {
                if ($program->first_airdate) {
                    return Carbon::createFromFormat('Y-m-d H:i:s', $program->first_airdate)->format('m/d/Y');
                }
            })
            ->addColumn('category', function ($program) {
                if ($program->category) {
                    return $program->category->name;
                }
            })
            ->addColumn('subCategory', function ($program) {
                if ($program->subCategory) {
                    return $program->subCategory->name;
                }
            })
            ->rawColumns(['name', 'msrp'])
            ->make();
    }
}
