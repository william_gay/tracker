<?php

namespace App\Http\Controllers;

use App\Models\Alert;
use App\Models\Category;
use App\Services\Validators\AlertValidator;
use Request;
use Redirect;
use Sentry;
use View;

class AlertController extends Controller
{
    public function __construct()
    {
        view()->share('intervals', ['weekly' => 'Weekly']);
        view()->share('types', ['shows'=>'New Shows','spots'=>'New Spots','category'=>'New In Category']);
        view()->share('categories', Category::whereNull('parent_id')->pluck('name', 'id')->toArray());
    }

    public function getIndex()
    {
        $user = Sentry::getUser();

        $alerts = Alert::where('user_id', $user->id)
            ->where('type', 'category')
            ->pluck('id')
            ->toArray();

        if ($alerts) {
            $alertCategories = Alert::whereIn('id', $alerts)->pluck('type_extra');
            $categories = Category::whereNull('parent_id')
                ->whereNotIn('id', $alertCategories)
                ->pluck('name', 'id')
                ->toArray();

            view()->share('categoriesContained', $categories);
        } else {
            view()->share('categoriesContained', Category::whereNull('parent_id')->pluck('name', 'id')->toArray());
        }

        $profile = subscription_profile();

        if ($profile['subscription_id'] == 5) {
            return redirect()->route('upgrade')->with('error', 'Your plan does not include alerts, please contact sales if you wish to upgrade.');
        }

        $alerts = Alert::where('user_id', Sentry::getUser()->id)->get();

        if ($alerts->count() > 0) {
            if ($profile['subscription_id'] == 4) { //Platinum
                $create_new_alert = true;
            } elseif ($profile['subscription_id'] == 3 && $alerts->count() < 3) { //Gold
                $create_new_alert = true;
            } elseif ($profile['subscription_id'] == 6 && $alerts->count() < 1) { //Silver
                $create_new_alert = true;
            } else {
                $create_new_alert = false;
            }
        } else {
            $create_new_alert = true;
        }

        return view('cxp.alerts.index', compact('alerts', 'create_new_alert'));
    }

    public function postAlerts()
    {
        $validation = new AlertValidator;

        if ($validation->passes()) {
            $alert = new Alert;
            $alert->user_id = Sentry::getUser()->id;
            $alert->interval = Request::get('interval');
            $alert->type = Request::get('type');
            $alert->type_extra = Request::get('type_extra');
            $alert->save();

            return redirect()->route('user.alerts')->with('success', 'Alert saved.');
        }

        return redirect()->back()->withInput()->withErrors($validation->errors);
    }

    public function postRankings()
    {
        $user = Sentry::getuser();
        $user->email_monthly = Request::filled('email_monthly') ? 1 : 0;
        $user->email_weekly = Request::filled('email_weekly') ? 1 : 0;
        $user->save();

        return redirect()
            ->route('user.alerts')
            ->with('success', 'Email Rankings Saved.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function deleteDestroy($alertId)
    {

        $alert = Alert::findOrFail($alertId);
        if ($alert->user_id == Sentry::getUser()->id) {
            $alert->delete();
        } else {
            return redirect()
                ->route('user.alerts')
                ->with('error', 'You tried to delete an alert that did not belong to you.');
        }

        return redirect()->route('user.alerts')->with('success', 'Alert deleted.');
    }
}
