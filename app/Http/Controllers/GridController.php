<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Agent;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Response;
use Request;
use Redirect;
use Sentry;
use View;
use App\Models\Airtime;
use App\Models\Language;

class GridController extends Controller
{
    public function getGrid()
    {
        if (Request::filled('report-date')) {
            $grid_date = Carbon::createFromFormat("F j, Y", Request::get('report-date'));
            $allowed = data_allowed($grid_date);
            if (! $allowed) {
                return abort('401', 'Access denied - Date outside of range');
            }
        } else {
            $grid_date = Carbon::now();
        }

        if (Request::filled('language_id')) {
            $language_id = Request::get('language_id');
        } else {
            $language_id = 1;
        }

        $language = Language::find($language_id);

        $times = thirty_minute_times($grid_date);

        $dbGetter = Carbon::createFromFormat("Y-m-d H:i:s", $grid_date->format("Y-m-d H:i:s"));
        $betweenStart = $dbGetter->startOfDay()->format('Y-m-d H:i:s');
        $betweenEnd = $dbGetter->endOfDay()->format('Y-m-d H:i:s');

        //Get the Channels Captured:
        $channels = Airtime::select('channel_id', 'channels.name', 'channels.logo_location')
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->whereBetween('air_date', [$betweenStart, $betweenEnd])
            ->where('channels.language_id', $language_id)
            ->verified()
            // ->where(function ($query) use ($report) {
            //     if ($report->slug == 'national-paid-programming-grids') {
            //         return $query->where('channels.pi', 1)
            //             ->orWhere('channels.id', 78); //WIZE
            //     } else {
            //         return $query->where('channels.pi', 0);
            //     }
            // })
            ->orderBy('channels.abbr')
            ->distinct('channel_id')
            ->get();

        $gridSet = [];

        $gridChannels = [];
        foreach ($channels as $channel) {
            $airtimes = Airtime::select([
                    'program_versions.grid_title',
                    DB::raw('TIME(airtimes.air_date) as air_date')
                ])
                ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
                ->whereBetween('air_date', [$betweenStart, $betweenEnd])
                ->where('airtimes.channel_id', $channel->channel_id)
                ->verified()
                ->orderBy('air_date')
                ->get();

            $gridChannels[$channel->channel_id] = [
                'name' => $channel->name,
                'logo' => $channel->logo_location
            ];

            if ($channel->logo_location) {
                $gridSet[$channel->channel_id] = ['<img src="https://'.$channel->logo_location.'" class="gridlogo" title="'.$channel->name.'" data-toggle="tooltip" title="'.$channel->name.'" /><br>'.$channel->name];
            } else {
                $gridSet[$channel->channel_id] = ['<p class="text-center" title="'.$channel->name.'">'.$channel->name.'</p>'];
            }

            $airtimesArr = $airtimes->toArray();

            foreach ($times as $key => $time) {
                $airtime = recursive_array_search($key, $airtimesArr);
                if ($airtime !== false) {
                    array_push($gridSet[$channel->channel_id], $airtimesArr[$airtime]['grid_title']);
                } else {
                    array_push($gridSet[$channel->channel_id], "");
                }
            }
        }

        $times = array_values($times);

        return view(
            'cxp.grids.index',
            compact(
                'grid_date',
                'language',
                'times',
                'gridSet',
                'gridChannels'
            )
        );
    }
}
