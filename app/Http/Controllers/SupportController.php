<?php

namespace App\Http\Controllers;

use App;
use View;

class SupportController extends Controller
{
    public function index()
    {
        return view('cxp.support.index');
    }
}
