<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Cache;

use App\Models\Product;
use App\Models\Airtime;
use App\Models\SpotAirtime;
use App\Models\ProgramVersion;
use App\Models\SpotVersion;

class WinmoController extends Controller
{

    public function index(Request $request)
    {
        $request->validate([
            'winmo_company_id' => 'numeric',
            'winmo_brand_id' => 'numeric',
        ]);

        try {
            $product = Product::with([
                    'programs',
                    'spots',
                    'channels',
                    'airings',
                    'predictor',
                ])
                ->when(($request->has('winmo_company_id') && $request->input('winmo_company_id') == 0), function ($query, $request) {
                    return $query->where('winmo_company_id', 0);
                })
                ->when(($request->has('winmo_brand_id') && $request->input('winmo_brand_id') == 0), function ($query, $request) {
                    return $query->where('winmo_brand_id', 0);
                })
                ->where(function ($query) use ($request) {
                    $query->when($request->input('winmo_company_id'), function ($query, $winmoCompanyId) {
                        return $query->orWhere('winmo_company_id', $winmoCompanyId);
                    })
                    ->when($request->input('winmo_brand_id'), function ($query, $winmoBrandId) {
                        return $query->orWhere('winmo_brand_id', $winmoBrandId);
                    });
                })
                ->firstOrFail();
        } catch (\Exception $e) {
            return view('winmo.missing');
        }

        return $this->drawPage($product, $request);
    }

    public function show(Request $request, Product $product)
    {
        try {
            $product->load([
                'programs',
                'spots',
                'channels',
                'airings',
                'predictor',
            ]);
        } catch (\Exception $e) {
            return view('winmo.missing');
        }

        return $this->drawPage($product, $request);
    }

    public function drawPage($product, $request)
    {
        $totalSubCategoryNetwork = $product->sub_category_channel_count;

        // Total networks
        $channelTotal = $product->channels->count();
        // TODO Get Sub Cat Networks
        $coverage = 0;
        if ($totalSubCategoryNetwork != 0) {
            $coverage =  $channelTotal/$totalSubCategoryNetwork;
        }

        // Top 5 Networks
        $lfsfAirtimes = $product->channels()->orderBy('airings', 'desc')->take(5)->get();

        $mediaSpend = $product->airings;

        $predictor = collect($product->predictor)->filter(function ($item) {
            $thirty = now()->addDays(30)->month;
            $sixty = now()->addDays(60)->month;
            $ninety = now()->addDays(90)->month;
            if (in_array($item->air_month, [$thirty, $sixty, $ninety])) {
                return true;
            }
        })->filter(function ($item) {
            $recent = now()->subYear()->year;
            if ($item->last_airing_year >= $recent) {
                return true;
            }
        })->sortBy('month_airing_count');

        $predictorDays = 0;
        if ($predictor->count()) {
            $monthsOut = now()->month($predictor->first()->air_month);
            $predictorDays = now()->diffInDays($monthsOut);
        }

        return view('winmo.index')
            ->with('product', $product)
            ->with('airtimesByNetwork', $lfsfAirtimes)
            ->with('mediaSpend', $mediaSpend)
            ->with('coverage', $coverage)
            ->with('predictorDays', $predictorDays);
    }
}
