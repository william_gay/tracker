<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\DB;
use Sentry;
use Queue;
use View;
use App\Models\ProgramWeekly;
use App\Models\SpotRanking;
use App\Models\SpotDetection;
use App\Models\UserProfile;
use App\Models\DashboardType;
use App\Jobs\LogSiteAnalytics;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function getIndex()
    {
        $lastFriday =  Carbon::now()->previous(CARBON::FRIDAY);

        $site_analytics = [
            'type' => 'dashboard',
            'user_id' => Sentry::getUser()->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $freqWeekly = ProgramWeekly::with('program')
            ->select(
                'programs.title as program_name',
                'program_weekly.airs as airs'
            )
            ->leftJoin('programs', 'programs.id', '=', 'program_weekly.program_id')
            ->weekending($lastFriday)  //Scope defined in model ProgramWeekly
            ->orderBy('freq_rank', 'asc')
            ->take(15)
            ->get();

        $spotWeekly = SpotDetection::select([
                DB::raw('SUM(spot_detections.detections) as frequency'),
                'spot_versions.title'
            ])
            ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
            ->rankdate($lastFriday)  //Scope defined in model SpotDetection
            ->where('spot_versions.language_id', 1)
            ->groupBy('spot_versions.spot_id')
            ->orderBy(DB::raw('SUM(detections)'), 'desc')
            ->take(15)
            ->get();

        $overTime = ProgramWeekly::select(
            DB::raw('SUM(airs) as airs'),
            DB::raw('SUM(media_index) as media'),
            DB::raw('week_ending as date')
        )
            ->where('week_ending', '>=', DB::raw('DATE_SUB(NOW(),INTERVAL 3 MONTH)'))
            ->where('media_rank', '<=', 25)
            ->groupBy('week_ending')
            ->get();

        $showfreq5Weekly = ProgramWeekly::with('program')
            ->select(
                'airs',
                'media_index',
                'programs.title',
                DB::raw('(SELECT max(program_versions.id) FROM '.DB::connection()->getDatabaseName().'.program_versions
                        WHERE program_versions.program_id = program_weekly.program_id) as program_version_id'),
                'stations'
            )
            ->leftJoin('programs', 'programs.id', '=', 'program_weekly.program_id')
            ->weekending($lastFriday)
            ->orderBy('freq_rank', 'asc')
            ->take(5)
            ->get();

        $spotfreq5Weekly = SpotRanking::select(
            'spot_versions.title',
            'spot_rankings.spot_version_id',
            'spot_rankings.frequency',
            'spot_rankings.streak'
        )
            ->join('spot_versions', 'spot_versions.id', '=', 'spot_rankings.spot_version_id')
            ->rankdate($lastFriday)
            ->orderBy('frequency', 'desc')
            ->take(5)
            ->get();

        $profileData = subscription_profile();
        $userProfile = UserProfile::firstOrCreate(['user_id' => Sentry::getUser()->id]);
        $dashboardType = DashboardType::find($userProfile->dashboardtype_id ?: 1);

        return view(
            'cxp.dashboard.index',
            compact(
                'showfreq5Weekly',
                'spotfreq5Weekly',
                'freqWeekly',
                'spotWeekly',
                'profileData',
                'lastFriday',
                'overTime',
                'dashboardType'
            )
        );
    }
}
