<?php

namespace App\Http\Controllers;

use Lang;
use Log;
use Redirect;
use Request;
use Response;
use Sentry;
use Session;
use View;
use Mail;
use App\Models\User;
use App\Models\Subscription;
use App\Mail\NewSubscription;

class BillingController extends Controller
{
    public function getIndex()
    {
        $user = User::find(Sentry::getUser()->id);

        return view('cxp.billing.index', compact('user'));
    }

    public function getLst()
    {
        $user = User::find(Sentry::getUser()->id);

        return view('cxp.billing.lst', compact('user'));
    }

    public function subscribeToPlan()
    {
        $stripeObject = Request::json('stripe');

        $userId = Request::json('user-id');
        $user = User::find($userId);
        $plan = Request::json('plan');
        $planGroup = Sentry::getGroupprovider()->findByName(Request::json('sentry-plan-group-name'));
        $coupon = false;
        if (Request::filled('coupon')) {
            $coupon = Request::json('coupon');
        }

        $user->addGroup($planGroup);

        $subscription = $user->hasSubscription($plan);
        if ($subscription) {
            if ($subscription->stripe_active == 1) {
                return response()->json(['error' => 'User is already subscribed to this plan']);
            }
            if ($coupon) {
                $subscription->subscription($plan)->withCoupon($coupon)->resume($stripeObject['id']);
            } else {
                $subscription->subscription($plan)->resume($stripeObject['id']);
            }
        } else {
            $subscription = Subscription::create(['user_id' => $user->id]);

            if ($coupon) {
                $subscription->subscription($plan)->withCoupon($coupon)->create($stripeObject['id'], [
                    'email' => $user->email
                ]);
            } else {
                $subscription->subscription($plan)->create($stripeObject['id'], [
                    'email' => $user->email
                ]);
            }
        }

        $data = [
            'user' => [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email
            ],
            'input' => ['plan' => $plan]
        ];

        Mail::to('sales@imsreport.com')
            ->queue(new NewSubscription($data));

        Session::flash('success', Lang::get('users.subscription_create_success'));
        Session::flash('warning', Lang::get('users.register_success'));

        return response()->json(['status' => 200]);
    }

    public function unsubscribeFromPlan($subscriptionId)
    {
        Subscription::find($subscriptionId)->subscription()->cancel();

        return Redirect::intended('user/billing')->with('success', Lang::get('users.subscription_cancel_success'));
    }

    public function renewPlan($subscriptionId)
    {
        // Subscription::find($subscriptionId)->subscription()->renew();
    }

    public function addGroupToAllStaffInGroup($staffGroup, $groupToAdd)
    {
        $staffMembers = Sentry::findAllUsersInGroup($staffGroup);
        foreach ($staffMembers as $staff) {
            $staff->addGroup($groupToAdd);
        }
    }
}
