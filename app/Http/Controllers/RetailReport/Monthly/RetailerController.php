<?php

namespace App\Http\Controllers\RetailReport\Monthly;

use App\Models\Category;
use App\Models\Retail\Retailer;
use App\Models\Retail\RetailerProduct;
use App\Models\Retail\RetailerMonthly;
use App\Models\SpotVersion;
use App\Models\ProgramVersion;
use App\Models\Airtime;
use App\Models\SpotAirtime;
use Carbon\Carbon;
use App;
use Illuminate\Support\Facades\DB;
use Redirect;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class RetailerController extends Controller
{

    public function index()
    {
        // Generates array to build month select form
        $monthSelect = RetailerMonthly::findMonthsByActiveReports();
        // Month that is current
        $report = RetailerMonthly::findReportByRequest(Request::all());
        $month = $report->month->copy();
        $endMonth = RetailerMonthly::getEndMonth();

        $retailers = Retailer::select(
            'retailers.id',
            'retailers.name',
            'retailers.slug',
            'retailers.website',
            'retailers.sqft',
            'retailers.store_count',
            'retailers.logo_location',
            DB::raw('COUNT(DISTINCT product_id) as product_count')
        )
            ->leftJoin('retailer_products', 'retailer_products.retailer_id', '=', 'retailers.id')
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->where('retailers.visible', 1)
            ->whereNull('retailer_products.deleted_at')
            ->groupBy('retailer_products.retailer_id')
            ->orderBy('name')->get();

        return view('cxp.retail-report.monthly.retailers.index', compact(
            'retailers',
            'monthSelect',
            'report',
            'month',
            'endMonth'
        ));
    }

    public function getRetailer($slug)
    {
        $retailer = Retailer::findBySlug($slug);

        if (! $retailer) {
            return redirect('retailers');
        }

        // Generates array to build month select form
        $monthSelect = RetailerMonthly::findMonthsByActiveReports();
        // Month that is current
        $report = RetailerMonthly::findReportByRequest(Request::all());
        $month = $report->month->copy();
        $endMonth = RetailerMonthly::getEndMonth();

        $products = RetailerProduct::findByRetailerIdAndMonth($retailer->id, $month);
        $categories = RetailerProduct::findCategoryByRetailerIdAndMonth($retailer->id, $month);

        return view('cxp.retail-report.monthly.retailers.show', compact(
            'retailer',
            'products',
            'report',
            'month',
            'endMonth',
            'monthSelect',
            'categories',
            'categoriesChart'
        ));
    }

    public function getCategories()
    {
        // Generates array to build month select form
        $monthSelect = RetailerMonthly::findMonthsByActiveReports();
        // Month that is current
        $report = RetailerMonthly::findReportByRequest(Request::all());
        $month = $report->month->copy();
        $endMonth = RetailerMonthly::getEndMonth();

        $categories = RetailerProduct::findCategoryByMonth($month);

        return view('cxp.retail-report.monthly.retailers.categories', compact(
            'categories',
            'month',
            'endMonth',
            'monthSelect',
            'report'
        ));
    }

    public function getCategory($categoryId)
    {
        $category = Category::find($categoryId);

        // Generates array to build month select form
        $monthSelect = RetailerMonthly::findMonthsByActiveReports();
        // Month that is current
        $report = RetailerMonthly::findReportByRequest(Request::all());
        $month = $report->month->copy();
        $endMonth = RetailerMonthly::getEndMonth();

        $retailers = RetailerProduct::findRetailersByCategoryAndMonth($category->id, $month);
        $retailersGraph = RetailerProduct::findRetailersByCategoryAndMonthForGraph($category->id, $month);
        $products = RetailerProduct::findProductsByCategoryAndMonth($category->id, $month);
        $lowestPrice = RetailerProduct::findLowPriceByCategoryAndMonth($category->id, $month);
        $highestPrice = RetailerProduct::findHighPriceByCategoryAndMonth($category->id, $month);

        $productsWithDR = new \Illuminate\Support\Collection;
        foreach ($products as $product) {
            $spotArr = $this->getSpotVersions(explode(',', $product->spots));
            $showArr = $this->getProgramVersions(explode(',', $product->programs));

            $productsWithDR[] = [
                'product_slug' => $product->product_slug,
                'product_name' => $product->product_name,
                'retailer_count' => $product->retailer_count,
                'min_price' => $product->min_price,
                'max_price' => $product->max_price,
                'dr_cost' => $this->getDrPriceByProductId($product->product_id, $spotArr, $showArr),
                'dr_airs' => $this->getAirsByProductId($product->product_id, $spotArr, $showArr),
                'media_spend' => $this->getMediaSpendByProductId($product->product_id, $spotArr, $showArr)
            ];
        }

        $spotIds = [];
        $programIds = [];
        /* Get Program and Spot Versions for Airing and Media Data */
        foreach ($retailers as $retailer) {
            //dd(explode(',', $retailer->spots));
            $spotIds = $spotIds + explode(',', $retailer->spots);
            $programIds = $programIds + explode(',', $retailer->programs);
        }

        $spotVersionIds = SpotVersion::whereIn('spot_id', $spotIds)->pluck('id')->toArray();
        $programVersionIds = ProgramVersion::whereIn('program_id', $programIds)->pluck('id')->toArray();

        $spotAirtimes = SpotAirtime::findCountBySpotVersions($spotVersionIds);
        $showAirtimes = Airtime::findCountByProgramVersions($programVersionIds);
        $totalMediaSpend = Airtime::findMediaSpendByProgramVersions($programVersionIds);

        $totalAirtimes = $spotAirtimes + $showAirtimes;

        return view('cxp.retail-report.monthly.retailers.category', compact(
            'category',
            'month',
            'endMonth',
            'monthSelect',
            'report',
            'retailers',
            'retailersGraph',
            'products',
            'lowestPrice',
            'highestPrice',
            'totalAirtimes',
            'totalMediaSpend',
            'productsWithDR'
        ));
    }

    public function getProduct($retailerSlug, $productId)
    {
        $retailer = Retailer::findBySlug($retailerSlug);
        $versions = RetailerProduct::findByRetailerAndProduct(['retailer_id' => $retailer->id, 'product_id' => $productId]);

        return view('cxp.retailers.product', compact(
            'retailer',
            'versions'
        ));
    }

    private function getDrPriceByProductId($productId, $spots, $programs)
    {
        if (count($programs) > 0) {
            $program = ProgramVersion::whereIn('id', $programs)
                ->orderBy('monitor_date', 'desc')
                ->first();

            return $program->price;
        }

        if (count($spots) > 0) {
            $spot = SpotVersion::whereIn('id', $spots)
                ->orderBy('air_date', 'desc')
                ->first();

            return $spot->price;
        }

        return 'N/A';
    }

    private function getAirsByProductId($productId, $spots, $programs)
    {
        $spotAirs = 0;
        $showAirs = 0;

        if (count($spots) > 0) {
            $spotAirs = SpotAirtime::findCountBySpotVersions($spots);
        }

        if (count($programs) > 0) {
            $showAirs = Airtime::findCountByProgramVersions($programs);
        }

        return $spotAirs + $showAirs;
    }

    private function getMediaSpendByProductId($productId, $spots, $programs)
    {
        if (count($programs) > 0) {
            return Airtime::findMediaSpendByProgramVersions($programs);
        }

        return 'N/A';
    }

    private function getProgramVersions($programIds)
    {
        if (count($programIds) > 0) {
            return ProgramVersion::whereIn('program_id', $programIds)->pluck('id')->toArray();
        } else {
            return null;
        }
    }

    private function getSpotVersions($spotIds)
    {
        if (count($spotIds) > 0) {
            return SpotVersion::whereIn('spot_id', $spotIds)->pluck('id')->toArray();
        } else {
            return null;
        }
    }
}
