<?php

namespace App\Http\Controllers\RetailReport\Monthly;

use App\Models\Product;
use App\Models\Retail\Retailer;
use App\Models\Retail\RetailerProduct;
use App\Models\Retail\RetailerMonthly;
use Carbon\Carbon;
use Sentry;
use Redirect;
use Request;
use View;

use App\Http\Controllers\Controller;

class RetailMonthlyController extends Controller
{
    public function getIndex()
    {
        /*
         *  Get the report based off of the User's form input.
         *  Defaults to the first report if there is no user input
         *  or if the user chose a report that is not active.
         */
        $report = RetailerMonthly::findReportByRequest(Request::all());

        // Show this error if report is not active and user is not a superuser
        if ($report->active == 0 and ! Sentry::isSuperUser()) {
            return Redirect::route('retail-report.monthly')
                ->with('error', 'The report you are trying to access is not ready yet, you have been redirected to our latest report!');
        }

        // Create a month object for generting the dropdown
        $month    = $report->month->copy();
        $endMonth = RetailerMonthly::getEndMonth();

        // Generates an array of all months with active reports.
        // This is used to populate the month-select-form in the view.
        $monthSelect = RetailerMonthly::findMonthsByActiveReports();

        $categories  = RetailerProduct::findCategoryByMonthWithLimit($month, 30);
        $retailers   = RetailerProduct::findRetailersByMonthWithLimit($month, 30);
        $products    = RetailerProduct::findProductsByMonthWithLimit($month, 30);

        $newProdByCategory = RetailerProduct::findNewProductsByCategory($month, 20);
        $newProdByRetailer = RetailerProduct::findNewProductsByRetailer($month, 20);

        return view('cxp.retail-report.monthly.index')
            ->with('month', $month)
            ->with('endMonth', $endMonth)
            ->with('monthSelect', $monthSelect)
            ->with('report', $report)
            ->with('categories', $categories)
            ->with('retailers', $retailers)
            ->with('products', $products)
            ->with('newProductsByCategory', $newProdByCategory)
            ->with('newProductsByRetailer', $newProdByRetailer);
    }
}
