<?php

namespace App\Http\Controllers\RetailReport\Monthly;

use App\Models\Product;
use App\Models\Retail\RetailerProduct;
use App\Models\Retail\RetailerMonthly;
use App\Models\ProgramVersion;
use App\Models\SpotVersion;
use App\Models\ProgramWeekly;
use App\Models\SpotRanking;
use App\Models\Airtime;
use App\Models\SpotAirtime;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function getIndex()
    {
        /*
         *  Get the report based off of the User's form input.
         *  Defaults to the first report if there is no user input
         *  or if the user chose a report that is not active.
         */
        $report = RetailerMonthly::findReportByRequest(Request::all());

        // Show this error if report is not active and user is not a superuser
        if ($report->active == 0 and ! Sentry::isSuperUser()) {
            return Redirect::route('retail-report.monthly')
                ->with('error', 'The report you are trying to access is not ready yet, you have been redirected to our latest report!');
        }

        // Create a month object for generting the dropdown
        $month    = $report->month->copy();
        $endMonth = RetailerMonthly::getEndMonth();

        // Generates an array of all months with active reports.
        // This is used to populate the month-select-form in the view.
        $monthSelect = RetailerMonthly::findMonthsByActiveReports();


        $products = Product::with('spots', 'programs')
            ->select(
                'products.name',
                'cat.name as category',
                'subcat.name as subcategory',
                DB::raw('COUNT(DISTINCT retailer_products.retailer_id) as retailers'),
                'products.slug',
                'products.id',
                DB::raw('MIN(retailer_products.min_price) as min_price'),
                DB::raw('MAX(retailer_products.max_price) as max_price')
            )
            ->leftJoin('categories as cat', 'products.category_id', '=', 'cat.id')
            ->leftJoin('categories as subcat', 'products.sub_category_id', '=', 'subcat.id')
            ->leftJoin('retailer_products', 'retailer_products.product_id', '=', 'products.id')
            ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
            ->whereBetween('date_recorded', [$month->startOfMonth(), $month->copy()->endOfMonth()])
            ->where('retailers.visible', 1) //Make sure the retailer is visible
            ->whereNull('retailer_products.deleted_at')
            ->whereNull('products.deleted_at')
            ->groupBy('retailer_products.product_id')
            ->orderBy('products.name')
            ->get();

        return view('cxp.retail-report.monthly.products.index')
            ->with('products', $products)
            ->with('monthSelect', $monthSelect)
            ->with('report', $report)
            ->with('month', $month)
            ->with('endMonth', $endMonth);
    }

    public function getProduct($slug)
    {
        $product = Product::findBySlug($slug);

        if (! $product) {
            return redirect('products');
        }

        // Generates array to build month select form
        $monthSelect = RetailerMonthly::findMonthsByActiveReports();
        // Month that is current
        $report = RetailerMonthly::findReportByRequest(Request::all());
        $month = $report->month->copy();
        $endMonth = RetailerMonthly::getEndMonth();

        $retailers = RetailerProduct::findRetailersByProductAndMonth($product->id, $month);
        $retailersGraph = RetailerProduct::findRetailersByProductAndMonthForGraph($product->id, $month);
        $lowestPrice = RetailerProduct::findLowPriceByProductAndMonth($product->id, $month);
        $highestPrice = RetailerProduct::findHighPriceByProductAndMonth($product->id, $month);

        $hasSpot = $product->spots()->where('language_id', 1)->first();
        $hasProgram = $product->programs()->where('language_id', 1)->first();

        if ($hasSpot) {
            $spotVersionIds = SpotVersion::where('spot_id', $hasSpot->id)->pluck('id')->toArray();
            $spot = SpotVersion::findLatestBySpotId($hasSpot->id);
            $drPrice = isset($spot->price) or 'N/A';
            $maxRank = SpotRanking::findHighestRankBySpotId($hasSpot->id);
            $streak = SpotRanking::findWeeksRankedBySpotId($hasSpot->id);
            $airings = SpotAirtime::findCountBySpotVersions($spotVersionIds);
            $mediaSpend = 'N/A';
        } else if ($hasProgram) {
            $programVersionIds = ProgramVersion::where('program_id', $hasProgram->id)->pluck('id')->toArray();
            $program = ProgramVersion::findLatestByProgramId($hasProgram->id);
            $drPrice = $program->cost;
            $maxRank = ProgramWeekly::findHighestRankByProgramId($hasProgram->id);
            $streak = ProgramWeekly::findWeeksRankedByProgramId($hasProgram->id);
            $airings = Airtime::findCountByProgramVersions($programVersionIds);
            $mediaSpend = '$'.number_format(Airtime::findMediaSpendByProgramVersions($programVersionIds));
        } else {
            $drPrice = 'N/A';
        }

        return view('cxp.retail-report.monthly.products.view', compact(
            'product',
            'monthSelect',
            'month',
            'endMonth',
            'report',
            'retailers',
            'retailersGraph',
            'lowestPrice',
            'highestPrice',
            'drPrice',
            'maxRank',
            'streak',
            'airings',
            'mediaSpend',
            'spot',
            'program'
        ));
    }

    public function getDatatables()
    {
        $products = Product::with('spots', 'programs')
            ->select(
                'products.name',
                'cat.name as category',
                'subcat.name as subcategory',
                DB::raw('count(retailer_products.product_id) as retailers'),
                'products.slug',
                'products.id'
            )
            ->leftJoin('categories as cat', 'products.category_id', '=', 'cat.id')
            ->leftJoin('categories as subcat', 'products.sub_category_id', '=', 'subcat.id')
            ->leftJoin('retailer_products', 'retailer_products.product_id', '=', 'products.id')
            ->whereNull('retailer_products.deleted_at')
            ->groupBy('retailer_products.product_id')
            ->orderBy('products.name');

        return Datatables::of($products)
            ->editColumn('name', '<a href="{{ route("products.show", array($slug)) }}">{{ $name }}</a>  @if(Sentry::getUser()->isSuperUser()) <small><a href="{{ route("admin.products.edit",array($id)) }}" target="_blank">Edit</a></small> @endif')
            ->editColumn('programs', '{{ count($programs) }}')
            ->editColumn('spots', '{{ count($spots) }}')
            ->remove_column('id')
            ->remove_column('slug')
            ->make();
    }
}
