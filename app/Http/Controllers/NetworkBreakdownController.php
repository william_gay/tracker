<?php namespace App\Http\Controllers;

use App\Models\Airtime;
use App\Models\Category;
use App\Models\Channel;
use App\Models\SiteAnalytics;
use App\Models\SpotAirtime;
use App\Models\Language;
use Carbon\Carbon;
use DateRangePicker\Libraries\Range;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Sentry;

class NetworkBreakdownController extends Controller
{

    private $channelFormPopulator;
    private $channelsToQuery;
    private $categoryFormPopulator;
    private $categoriesToQuery;
    private $singleCategory;
    private $marketFormPopulator;
    private $marketToQuery;
    private $range;

    public function sharedVariables()
    {
        if (! request()->filled('rangeStart') and ! request()->filled('rangeStart')) {
            $lastFriday = Carbon::now()->previous(Carbon::FRIDAY);

            $this->range = Range::getStartAndEnd($lastFriday->copy()->subMonth()->format("F j, Y"), $lastFriday->format("F j, Y"));
        } else {
            $this->range = Range::getStartAndEnd(request()->get('rangeStart'), request()->get('rangeEnd'));
        }

        $this->marketToQuery = Language::getMarkets(request()->get('market_id'));
        $this->marketFormPopulator = Language::marketFormPopulator();
        $this->channelFormPopulator = Channel::where('language_id', $this->marketToQuery)
            ->orderBy('name', 'asc')
            //->where('capturing', 1)
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->pluck('name', 'id');
        $this->channelsToQuery = Channel::getChannels(request()->get('channels'), [1]);
        $this->singleCategory = Category::find('singleCategory');
        $this->categoryFormPopulator = Category::whereNull('parent_id')->pluck('name', 'id');
        $this->categoriesToQuery = Category::getCategories(request()->get('categories'), [1]);

        if (count($this->channelFormPopulator) == count($this->channelsToQuery)) {
            $channelsSelected = "All Networks Selected";
        } else {
            $channelsSelected = implode(', ', Channel::whereIn('id', $this->channelsToQuery)->pluck('abbr'));
        }

        view()->share('channelsSelected', $channelsSelected);
        view()->share('channelFormPopulator', $this->channelFormPopulator);
        view()->share('channelsToQuery', $this->channelsToQuery);
        view()->share('categoryFormPopulator', $this->categoryFormPopulator);
        view()->share('categoriesToQuery', $this->categoriesToQuery);
        view()->share('singleCategory', $this->singleCategory);
        view()->share('marketFormPopulator', $this->marketFormPopulator);
        view()->share('marketToQuery', $this->marketToQuery);
        view()->share('start_date', $this->range['start']);
        view()->share('end_date', $this->range['end']);
        view()->share('range', $this->range);
    }

    public function getBreakdown()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $showPie = $this->showPie();
        $spotPie = $this->spotPie();
        $showHourBar = $this->showHourBar();
        $spotHourBar = $this->spotHourBar();
        $weeklyShowBreakdown = $this->weeklyShowBreakdown();
        $weeklySpotBreakdown = $this->weeklySpotBreakdown();

        return view(
            'cxp.networks.breakdown',
            compact(
                'showPie',
                'spotPie',
                'showHourBar',
                'spotHourBar',
                'weeklyShowBreakdown',
                'weeklySpotBreakdown'
            )
        );
    }

    private function showPie()
    {
        $shows = Airtime::select([
                DB::raw('categories.name as category'),
                DB::raw('count(airtimes.id) as freq'),
                DB::raw('(SUM(airtimes.cost)/2000) as media'),
                'airtimes.channel_id',
            ])
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->where('airtimes.air_date', '>', $this->range['start']->format("Y-m-d"))
            ->where('airtimes.air_date', '<=', $this->range['end']->format("Y-m-d"))
            ->where('channels.language_id', $this->marketToQuery)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->groupBy('program_versions.category_id')
            ->orderBy('freq', 'desc')
            ->get();

        return $shows;
    }

    private function spotPie()
    {
        $spots = SpotAirtime::select([
                DB::raw('categories.name as category'),
                DB::raw('count(spot_airtimes.id) as freq'),
                DB::raw('(SUM(spot_airtimes.cost)/2000) as media'),
                'spot_airtimes.channel_id',
            ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->where('spot_airtimes.air_date', '>', $this->range['start']->format("Y-m-d"))
            ->where('spot_airtimes.air_date', '<=', $this->range['end']->format("Y-m-d"))
            ->where('channels.language_id', $this->marketToQuery)
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
            ->groupBy('spot_versions.category_id')
            ->orderBy('freq', 'desc')
            ->get();

        return $spots;
    }

    private function showHourBar()
    {
        $shows = Airtime::select([
                DB::raw('HOUR(airtimes.air_date) as hour'),
                DB::raw('count(DISTINCT categories.id) as category_count'),
                DB::raw('count(airtimes.id) as freq'),
                DB::raw('(SUM(airtimes.cost)/2000) as media'),
                'airtimes.channel_id',
            ])
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'program_versions.category_id')
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->where('airtimes.air_date', '>', $this->range['start']->format("Y-m-d"))
            ->where('airtimes.air_date', '<=', $this->range['end']->format("Y-m-d"))
            ->where('channels.language_id', $this->marketToQuery)
            ->whereIn('airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('program_versions.category_id', $this->categoriesToQuery)
            ->groupBy(DB::raw('HOUR(airtimes.air_date)'))
            ->orderBy(DB::raw('HOUR(airtimes.air_date)'))
            ->get();

        return $shows;
    }

    private function spotHourBar()
    {
        $spots = SpotAirtime::select([
                DB::raw('HOUR(spot_airtimes.air_date) as hour'),
                DB::raw('count(DISTINCT categories.id) as category_count'),
                DB::raw('count(spot_airtimes.id) as freq'),
                DB::raw('(SUM(spot_airtimes.cost)/2000) as media'),
                'spot_airtimes.channel_id',
            ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->where('spot_airtimes.air_date', '>', $this->range['start']->format("Y-m-d"))
            ->where('spot_airtimes.air_date', '<=', $this->range['end']->format("Y-m-d"))
            ->where('channels.language_id', $this->marketToQuery)
            ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
            ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
            ->groupBy(DB::raw('HOUR(spot_airtimes.air_date)'))
            ->orderBy(DB::raw('HOUR(spot_airtimes.air_date)'))
            ->get();

        return $spots;
    }

    private function weeklyShowBreakdown()
    {
        $fridays = fridayRange($this->range['start'], $this->range['end']);

        $weeklyBreakdown = [];
        $i = 0;
        foreach ($fridays as $friday) {
            $end_friday = Carbon::createFromFormat("Y-m-d", $friday)->endOfDay();
            $start_saturday = $end_friday->copy()->previous(Carbon::SATURDAY)->startOfDay();

            $breakdown = Airtime::select([
                    DB::raw('COUNT(airtimes.id) as airs'),
                    DB::raw('SUM(airtimes.cost)/2000 as media_index'),
                    DB::raw('SUM(airtimes.cost) as total_cost'),
                    DB::raw('COUNT(DISTINCT program_version_id)  as unique_programs'),
                    'airtimes.channel_id',
                ])
                ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
                ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
                ->where('airtimes.air_date', '>', $start_saturday->format('Y-m-d H:i:s'))
                ->where('airtimes.air_date', '<=', $end_friday->format('Y-m-d H:i:s'))
                ->whereIn('airtimes.channel_id', $this->channelsToQuery)
                ->where('channels.language_id', $this->marketToQuery)
                ->whereIn('program_versions.category_id', $this->categoriesToQuery)
                ->first();

            $weeklyBreakdown[$i]['end_date'] = $end_friday;
            $weeklyBreakdown[$i]['date'] = $end_friday->format('Y-m-d h:i:s');
            $weeklyBreakdown[$i]['airs'] = $breakdown->airs;
            $weeklyBreakdown[$i]['media_index'] = $breakdown->media_index;
            $weeklyBreakdown[$i]['total_cost'] = $breakdown->total_cost;
            $weeklyBreakdown[$i]['unique_shows'] = $breakdown->unique_programs;
            $i++;
        }

        return $weeklyBreakdown;
    }

    private function weeklySpotBreakdown()
    {
        $fridays = fridayRange($this->range['start'], $this->range['end']);

        $weeklyBreakdown = [];
        $i = 0;
        foreach ($fridays as $friday) {
            $end_friday = Carbon::createFromFormat("Y-m-d", $friday)->endOfDay();
            $start_saturday = $end_friday->copy()->previous(Carbon::SATURDAY)->startOfDay();

            $breakdown = SpotAirtime::select([
                    DB::raw('COUNT(spot_airtimes.id) as airs'),
                    DB::raw('SUM(spot_airtimes.cost)/2000 as media_index'),
                    DB::raw('SUM(spot_airtimes.cost) as total_cost'),
                    DB::raw('COUNT(DISTINCT spot_version_id)  as unique_spots'),
                    'spot_airtimes.channel_id',
                ])
                ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
                ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
                ->where('spot_airtimes.air_date', '>', $start_saturday->format('Y-m-d H:i:s'))
                ->where('spot_airtimes.air_date', '<=', $end_friday->format('Y-m-d H:i:s'))
                ->whereIn('spot_airtimes.channel_id', $this->channelsToQuery)
                ->where('channels.language_id', $this->marketToQuery)
                ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
                ->first();

            $weeklyBreakdown[$i]['end_date'] = $end_friday;
            $weeklyBreakdown[$i]['date'] = $end_friday->format('Y-m-d h:i:s');
            $weeklyBreakdown[$i]['airs'] = $breakdown->airs;
            $weeklyBreakdown[$i]['media_index'] = $breakdown->media_index;
            $weeklyBreakdown[$i]['total_cost'] = $breakdown->total_cost;
            $weeklyBreakdown[$i]['unique_spots'] = $breakdown->unique_spots;
            $i++;
        }

        return $weeklyBreakdown;
    }
}
