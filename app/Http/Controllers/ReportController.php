<?php

namespace App\Http\Controllers;

use App\Models\Airtime;
use App\Models\Category;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\ProgramWeekly;
use App\Models\Report;
use App\Models\Spot;
use App\Models\SpotAirtime;
use App\Models\SpotDetection;
use App\Models\SpotRanking;
use App\Jobs\LogSiteAnalytics;
use App\Jobs\LogEmailAnalytics;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Agent;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Response;
use Request;
use Redirect;
use Sentry;
use View;
use App\Models\UserHash;
use App\Libraries\ReportGenerator;

class ReportController extends Controller
{
    public function index()
    {
        /* Report Menu exists in view composer: App\Composers\ReportMenuComposer */
        return view('cxp.reports.index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getShow($reportID)
    {
        if (Request::filled('week_ending')) {
            $week_ending = Carbon::createFromFormat("F j, Y", Request::get('week_ending'));
            $week_ending->hour = 0;
            $week_ending->minute = 0;
            $week_ending->second = 0;
        } else {
            $week_ending = lastFriday();
        }


        try {
            $report = Report::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        $allowed = data_allowed($week_ending);
        //Don't check for the year if they are looking at the top 25
        if ($report->slug == 'top-25-report') {
            $allowed = true;
        }

        if (! $allowed) {
            $data_since = data_since();
            $data_since->addDays(7);
            return redirect('reports/show/'.$reportID.'?week_ending='.urlencode($data_since->format('F j, Y')))->with('success', 'You have been redirected to reports that fit in your data range.');

            return abort('401', 'Access denied - Date outside of range');
        }

        if (Request::filled('month')) {
            $month = Carbon::createFromFormat("Y-m-d", Request::get('month').'-01');
        } else {
            $month = $week_ending;
        }

        if (Request::filled('year')) {
            $year = Carbon::createFromFormat("Y", Request::get('year'));
        } else {
            $year = $week_ending->copy();
        }

        $user = Sentry::getUser();
        $years = getYears();
        $months = getMonths();
        $monthRange = monthRange($month->format("Y-m"));
        $weeks = getWeeks($month);

        $site_analytics = [
            'type' => 'report',
            'type_id' => $report->id,
            'user_id' => Sentry::getUser()->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        if ($user->hasAccess('reports.'.$report->slug)) {
            if ($report->slug == 'weekly-top-25-frequency-lf' or
                $report->slug == 'weekly-top-50-frequency-lf' or
                $report->slug == 'weekly-top-100-frequency-lf') {
                $program_weekly = ProgramWeekly::with([
                    'program',
                    'program.programVersions',
                ])
                    ->select(
                        'program_weekly.id',
                        'program_weekly.program_id',
                        'program_weekly.week_ending',
                        'program_weekly.freq_rank',
                        'program_weekly.media_rank',
                        'program_weekly.streak',
                        'program_weekly.airs',
                        'program_weekly.media_index',
                        'program_weekly.stations',
                        'program_weekly.prev_freq_rank',
                        'program_weekly.prev_media_rank',
                        DB::raw('(SELECT max(program_versions.id) FROM ims.program_versions
                        WHERE program_versions.program_id = program_weekly.program_id AND deleted_at IS NULL) as program_version_id')
                    )
                    ->where('week_ending', $week_ending->format("Y-m-d"))
                    ->orderBy('freq_rank')
                    ->take($report->limit)

                    ->get();

                View::share('data', $program_weekly);
            } elseif ($report->slug == 'weekly-top-25-media-index-lf' or
                      $report->slug == 'weekly-top-50-media-index-lf' or
                      $report->slug == 'weekly-top-100-media-index-lf') {
                $program_weekly = ProgramWeekly::with([
                    'program',
                    'program.programVersions'
                ])
                    ->select(
                        'program_weekly.id',
                        'program_weekly.program_id',
                        'program_weekly.week_ending',
                        'program_weekly.freq_rank',
                        'program_weekly.media_rank',
                        'program_weekly.streak',
                        'program_weekly.airs',
                        'program_weekly.media_index',
                        'program_weekly.stations',
                        'program_weekly.prev_freq_rank',
                        'program_weekly.prev_media_rank',
                        DB::raw('(SELECT max(program_versions.id) FROM ims.program_versions
                        WHERE program_versions.program_id = program_weekly.program_id AND deleted_at IS NULL) as program_version_id')
                    )
                    ->where('week_ending', $week_ending->format("Y-m-d"))
                    ->orderBy('media_rank')
                    ->take($report->limit)
                    ->get();

                View::share('data', $program_weekly);
            } elseif ($report->slug == 'weekly-top-100-paid-programming-frequency-lf' or
                      $report->slug == 'weekly-top-50-paid-programming-frequency-lf' or
                      $report->slug == 'weekly-top-25-paid-programming-frequency-lf') {
                $programs = Airtime::with('programVersion', 'programVersion.category')
                    ->select(
                        'airtimes.program_version_id',
                        DB::raw('count(DISTINCT airtimes.channel_id) as stations'),
                        DB::raw('count(airtimes.program_version_id) as airs')
                    )
                    ->leftJoin('program_versions as p', 'p.id', '=', 'airtimes.program_version_id')
                    ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
                    ->where('p.language_id', $report->language_id)
                    ->where(function ($query) {
                        return $query->where('channels.pi', 1)
                            ->orWhere('channels.id', 78); //WIZE
                    })
                    ->whereBetween(
                        'air_date',
                        [$week_ending->copy()->subDays(7)->setTime(16, 0, 0)->toDateTimeString(),
                            $week_ending->copy()->setTime(16, 0, 0)->toDateTimeString()]
                    )
                    ->groupBy('p.program_id')
                    ->orderBy('airs', 'desc')
                    ->take($report->limit)
                    ->get();


                View::share('data', $programs);
            } elseif ($report->slug == 'weekly-top-100-paid-and-standard-frequency-lf' or
                      $report->slug == 'weekly-top-50-paid-and-standard-frequency-lf' or
                      $report->slug == 'weekly-top-25-paid-and-standard-frequency-lf') {
                $programs = Airtime::with('programVersion', 'programVersion.category')
                    ->select(
                        'airtimes.program_version_id',
                        DB::raw('count(DISTINCT airtimes.channel_id) as stations'),
                        DB::raw('count(airtimes.id) as airs'),
                        DB::raw('SUM(airtimes.cost)/2000 as media_index')
                    )
                    ->leftJoin('program_versions as p', 'p.id', '=', 'airtimes.program_version_id')
                    ->verified()
                    ->where('p.language_id', $report->language_id) //Spanish
                    ->whereBetween(
                        'air_date',
                        [$week_ending->copy()->subDays(7)->setTime(16, 0, 0)->toDateTimeString(),
                            $week_ending->copy()->setTime(16, 0, 0)->toDateTimeString()]
                    )
                    ->groupBy('p.program_id')
                    ->orderBy('airs', 'desc')
                    ->take($report->limit)
                    ->get();

                View::share('data', $programs);
            } elseif ($report->slug == 'annual-top-100-standard') {
                $programs = Airtime::with([
                    'programVersion',
                    'programVersion.category',
                    'programVersion.subCategory',
                    'programVersion.marketingCompany',
                    ])
                    ->select(
                        'airtimes.program_version_id',
                        DB::raw('count(DISTINCT airtimes.channel_id) as stations'),
                        DB::raw('count(airtimes.id) as airs'),
                        DB::raw('SUM(airtimes.cost)/2000 as media_index')
                    )
                    ->leftJoin('program_versions as p', 'p.id', '=', 'airtimes.program_version_id')
                    ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
                    ->verified()
                    ->where('p.language_id', $report->language_id) //Spanish
                    ->where('channels.pi', '=', 0)
                    ->whereBetween(
                        'air_date',
                        [$year->copy()->startOfYear(),
                            $year->copy()->endOfYear()]
                    )
                    ->groupBy('p.program_id')
                    ->orderBy('airs', 'desc')
                    ->take($report->limit)
                    ->get();

                View::share('data', $programs);
            } elseif ($report->slug == 'annual-top-100-standard-media-index') {
                $programs = Airtime::with([
                    'programVersion',
                    'programVersion.category',
                    'programVersion.subCategory',
                    'programVersion.marketingCompany',
                    ])
                    ->select(
                        'airtimes.program_version_id',
                        DB::raw('count(DISTINCT airtimes.channel_id) as stations'),
                        DB::raw('count(airtimes.id) as airs'),
                        DB::raw('SUM(airtimes.cost)/2000 as media_index')
                    )
                    ->leftJoin('program_versions as p', 'p.id', '=', 'airtimes.program_version_id')
                    ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
                    ->verified()
                    ->where('p.language_id', $report->language_id) //Spanish
                    ->where('channels.pi', '=', 0)
                    ->whereBetween(
                        'air_date',
                        [$year->copy()->startOfYear(),
                            $year->copy()->endOfYear()]
                    )
                    ->groupBy('p.program_id')
                    ->orderBy('media_index', 'desc')
                    ->take($report->limit)
                    ->get();

                View::share('data', $programs);
            } elseif ($report->slug == 'weekly-top-100-spot-ranking' ||
                        $report->slug == 'weekly-top-50-spot-ranking' ||
                        $report->slug == 'weekly-top-25-spot-ranking' ||
                        $report->slug == 'weekly-all-spot-retail-product-ranking') { //Report ID: 25/36/38/72

                $spot = SpotRanking::where('rank_date', $week_ending->format("Y-m-d"))
                    ->orderBy('rank', 'asc')
                    ->take($report->limit)
                    ->get();

                View::share('data', $spot);
            } elseif ($report->slug == 'weekly-top-25-spot-direct-response-ranking' or
                      $report->slug == 'weekly-top-50-spot-direct-response-ranking') {
                $spot = SpotAirtime::select([
                        DB::raw('count(spot_airtimes.id) as airings'),
                        DB::raw('sum(spot_airtimes.cost)/2000 as media_index'),
                        'spot_versions.id as spot_version_id',
                        'spot_versions.title',
                        'spot_versions.price',
                        'spot_versions.shipping_cost',
                        'c.name as category',
                        'sc.name as subcategory',
                        'companies.name as marketingCompany'
                    ])
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
                    ->leftJoin('categories as c', 'spot_versions.category_id', '=', 'c.id')
                    ->leftJoin('categories as sc', 'spot_versions.sub_category_id', '=', 'sc.id')
                    ->leftJoin('companies', 'spot_versions.marketing_company_id', '=', 'companies.id')
                    ->whereBetween(
                        'spot_airtimes.air_date',
                        [$week_ending->copy()->subDays(7)->setTime(16, 0, 0)->toDateTimeString(),
                            $week_ending->copy()->setTime(16, 0, 0)->toDateTimeString()]
                    )
                    ->where('spot_versions.language_id', $report->language_id)
                    ->where('spot_versions.non_dr', '=', 0) //Make sure it is marked as DR
                    ->groupBy('spot_versions.spot_id')
                    ->orderBy(DB::raw('airings'), 'desc')
                    ->take($report->limit)

                    ->get();

                View::share('data', $spot);
            } elseif ($report->slug == 'weekly-top-50-spanish-spot-ranking' or
                      $report->slug == 'weekly-top-25-spanish-spot-ranking' or
                      $report->slug == 'weekly-top-100-australian-spot-ranking' or
                      $report->slug == 'weekly-top-50-australian-spot-ranking' or
                      $report->slug == 'weekly-top-25-australian-spot-ranking') {
                //Report ID: 34/35
                $spot = SpotDetection::select(
                    DB::raw('SUM(spot_detections.detections) as airings'),
                    DB::raw('AVG(spot_versions.length) as average_length'),
                    'spot_versions.id as spot_version_id',
                    'spot_versions.title',
                    'spot_versions.price',
                    'spot_versions.shipping_cost',
                    'spot_detections.new_for_week',
                    'c.name as category',
                    'sc.name as subcategory',
                    'companies.name as company_name'
                )
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
                    ->leftJoin('categories as c', 'spot_versions.category_id', '=', 'c.id')
                    ->leftJoin('categories as sc', 'spot_versions.sub_category_id', '=', 'sc.id')
                    ->leftJoin('companies', 'spot_versions.marketing_company_id', '=', 'companies.id')
                    ->where('rank_date', $week_ending->format("Y-m-d"))
                    ->where('spot_versions.language_id', $report->language_id)
                    ->orderBy(DB::raw('SUM(spot_detections.detections)'), 'desc')
                    ->groupBy('spot_versions.spot_id')
                    ->take($report->limit)

                    ->get();

                View::share('data', $spot);
            } elseif ($report->slug == 'top-25-report') {
                //---------------------------------------------------------------
                // Slug: top-25-report
                //---------------------------------------------------------------
                //Get the first day of last month, and the last day of last month
                $freeMonthStart = Carbon::now()->subMonth()->startOfMonth();
                $freeMonthEnd = Carbon::now()->subMonth()->endOfMonth();

                $airings = Airtime::with('programVersion')
                    ->join('program_versions as pv', 'airtimes.program_version_id', '=', 'pv.id')
                    ->select(
                        DB::raw('count(`airtimes`.`id`) as airs'),
                        'airtimes.program_version_id'
                    )
                    ->whereBetween(
                        'airtimes.air_date',
                        [$freeMonthStart,$freeMonthEnd]
                    )
                    ->verified()
                    ->where('pv.language_id', $report->language_id)
                    ->groupBy('pv.program_id')
                    ->orderBy('airs', 'desc')
                    ->take($report->limit)

                    ->get();

                // Slug: top-25-report
                View::share('data', $airings);
                View::share('count', 1);
            } elseif ($report->slug == 'monthly-top-100-frequency-rankings-lf' or
                      $report->slug == 'monthly-top-25-long-form-frequency-rankings') {
                //---------------------------------------------------------------
                // Slug: Monthly top 25 and 100
                //---------------------------------------------------------------

                $airings = Airtime::with([
                    'programVersion',
                    'programVersion.marketingCompany',
                    'programVersion.category',
                    'programVersion.subCategory',
                ])
                    ->select(
                        DB::raw('count(`airtimes`.`id`) as airs'),
                        'airtimes.program_version_id',
                        DB::raw('sum(airtimes.cost)/2000 as media_index')
                    )
                    ->join('program_versions', 'airtimes.program_version_id', '=', 'program_versions.id')
                    ->join('channels', 'airtimes.channel_id', '=', 'channels.id')
                    ->whereBetween(
                        'airtimes.air_date',
                        [$month->copy()->startOfMonth()->toDateTimeString(),
                              $month->copy()->endOfMonth()->toDateTimeString()]
                    )
                    ->verified()
                    ->where('program_versions.language_id', $report->language_id)
                    ->where('channels.pi', 0)
                    ->whereNull('airtimes.deleted_at')
                    ->groupBy('program_versions.program_id')
                    ->orderBy('airs', 'desc')
                    ->take($report->limit)

                    ->get();

                // Slug: Monthly top 25 and 100
                // View: top-rankings.blade.php
                View::share('data', $airings);
            } elseif ($report->slug == 'monthly-top-100-paid-programming-frequency-lf' or
                      $report->slug == 'monthly-top-25-paid-programming-frequency-lf') {
                //---------------------------------------------------------------
                // Slug: Monthly top 25 and 100
                //---------------------------------------------------------------

                $airings = Airtime::with([
                    'programVersion',
                    'programVersion.marketingCompany',
                    'programVersion.category',
                    'programVersion.subCategory',
                ])
                    ->select(
                        DB::raw('count(`airtimes`.`id`) as airs'),
                        'airtimes.program_version_id',
                        DB::raw('sum(airtimes.cost)/2000 as media_index')
                    )
                    ->join('program_versions', 'airtimes.program_version_id', '=', 'program_versions.id')
                    ->join('channels', 'airtimes.channel_id', '=', 'channels.id')
                    ->whereBetween(
                        'airtimes.air_date',
                        [$month->copy()->startOfMonth()->toDateTimeString(),
                              $month->copy()->endOfMonth()->toDateTimeString()]
                    )
                    ->verified()
                    ->where('program_versions.language_id', $report->language_id)
                    ->where(function ($query) {
                        return $query->where('channels.pi', 1)
                            ->orWhere('channels.id', 78); //WIZE
                    })
                    ->whereNull('airtimes.deleted_at')
                    ->groupBy('program_versions.program_id')
                    ->orderBy('airs', 'desc')
                    ->take($report->limit)

                    ->get();

                // Slug: Monthly top 25 and 100
                // View: top-rankings.blade.php
                View::share('data', $airings);
            } elseif ($report->slug == 'monthly-top-100-paid-and-standard-frequency-lf' or
                      $report->slug == 'monthly-top-25-paid-and-standard-frequency-lf') {
                //---------------------------------------------------------------
                // Slug: Monthly top 25 and 100
                //---------------------------------------------------------------

                $airings = Airtime::with([
                    'programVersion',
                    'programVersion.marketingCompany',
                    'programVersion.category',
                    'programVersion.subCategory',
                ])
                    ->select(
                        DB::raw('count(`airtimes`.`id`) as airs'),
                        'airtimes.program_version_id',
                        DB::raw('sum(airtimes.cost)/2000 as media_index')
                    )
                    ->join('program_versions', 'airtimes.program_version_id', '=', 'program_versions.id')
                    ->whereBetween(
                        'airtimes.air_date',
                        [$month->copy()->startOfMonth()->toDateTimeString(),
                              $month->copy()->endOfMonth()->toDateTimeString()]
                    )
                    ->verified()
                    ->where('program_versions.language_id', $report->language_id)
                    ->whereNull('airtimes.deleted_at')
                    ->groupBy('program_versions.program_id')
                    ->orderBy('airs', 'desc')
                    ->take($report->limit)

                    ->get();

                // Slug: Monthly top 25 and 100
                // View: top-rankings.blade.php
                View::share('data', $airings);
            } elseif ($report->slug == 'monthly-top-100-media-index-rankings-lf') {
                //---------------------------------------------------------------
                // Slug: Monthly 100 media index ranking lf
                //---------------------------------------------------------------
                $airings = Airtime::with([
                    'programVersion',
                    'programVersion.marketingCompany',
                    'programVersion.category',
                    'programVersion.subCategory',
                ])
                    ->join('program_versions', 'airtimes.program_version_id', '=', 'program_versions.id')
                    ->join('channels', 'airtimes.channel_id', '=', 'channels.id')
                    ->select(
                        DB::raw('count(`airtimes`.`id`) as airs'),
                        'airtimes.program_version_id',
                        DB::raw('sum(airtimes.cost)/2000 as media_index')
                    )
                    ->whereBetween(
                        'airtimes.air_date',
                        [$month->copy()->startOfMonth()->toDateTimeString(),
                            $month->copy()->endOfMonth()->toDateTimeString()]
                    )
                    ->verified()
                    ->where('program_versions.language_id', $report->language_id)
                    ->where('channels.pi', 0)
                    ->whereNull('airtimes.deleted_at')
                    ->groupBy('program_versions.program_id')
                    ->orderBy('media_index', 'desc')
                    ->take($report->limit)

                    ->get();

                View::share('data', $airings);

                // Slug: Monthly 100 media index ranking sf
            } elseif ($report->slug == 'monthly-top-100-spot-rankings-sf' or
                $report->slug == 'monthly-top-25-spot-rankings') {
                $spot = SpotAirtime::select(
                    DB::raw('count(spot_airtimes.id) as airings'),
                    DB::raw('AVG(spot_versions.length) as average_length'),
                    DB::raw('sum(spot_airtimes.cost)/2000 as media_index'),
                    'spot_versions.id as spot_version_id',
                    'spot_versions.title',
                    'spot_versions.price',
                    'spot_versions.shipping_cost',
                    'c.name as category',
                    'sc.name as subcategory'
                )
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
                    ->leftJoin('categories as c', 'spot_versions.category_id', '=', 'c.id')
                    ->leftJoin('categories as sc', 'spot_versions.sub_category_id', '=', 'sc.id')
                    ->leftJoin('companies', 'spot_versions.marketing_company_id', '=', 'companies.id')
                    ->whereBetween(
                        'spot_airtimes.air_date',
                        [$month->copy()->startOfMonth()->toDateTimeString(),
                              $month->copy()->endOfMonth()->toDateTimeString()]
                    )
                    ->where('spot_versions.language_id', $report->language_id) // English
                    ->where('spot_versions.non_dr', '=', 0) //Don't include non-dr
                    ->whereNull('spot_airtimes.deleted_at')
                    ->groupBy('spot_versions.spot_id')
                    ->orderBy('airings', 'desc')
                    ->take($report->limit)
                    ->get();

                View::share('data', $spot);
            } elseif ($report->slug == 'monthly-top-100-spot-retail-product-rankings' or
                $report->slug == 'monthly-top-25-spot-retail-product-rankings') {
                $spot = SpotDetection::select(
                    DB::raw('SUM(spot_detections.detections) as airings'),
                    DB::raw('AVG(spot_versions.length) as average_length'),
                    DB::raw('sum(spot_detections.media_index) as media_index'),
                    'spot_versions.id as spot_version_id',
                    'spot_versions.title',
                    'spot_versions.price',
                    'spot_versions.shipping_cost',
                    'spot_detections.new_for_week',
                    'c.name as category',
                    'sc.name as subcategory'
                )
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
                    ->leftJoin('categories as c', 'spot_versions.category_id', '=', 'c.id')
                    ->leftJoin('categories as sc', 'spot_versions.sub_category_id', '=', 'sc.id')
                    ->leftJoin('companies', 'spot_versions.marketing_company_id', '=', 'companies.id')
                    ->whereBetween(
                        'rank_date',
                        [$month->copy()->startOfMonth()->toDateTimeString(),
                              $month->copy()->endOfMonth()->toDateTimeString()]
                    )
                    ->where('spot_detections.language_id', $report->language_id)
                    ->where('spot_versions.service', 0)
                    ->where('spot_versions.non_dr', '=', 0) //Don't include non-dr
                    ->where('spot_versions.brand_advert', '=', 0)
                    ->groupBy('spot_versions.spot_id')
                    ->orderBy('airings', 'desc')
                    ->take($report->limit)
                    ->get();

                View::share('data', $spot);
            } elseif ($report->slug == 'annual-top-100-spot-direct-response') {
                $spot = SpotAirtime::select(
                    DB::raw('count(spot_airtimes.id) as airings'),
                    DB::raw('AVG(spot_versions.length) as average_length'),
                    DB::raw('sum(spot_airtimes.cost)/2000 as media_index'),
                    'spot_versions.id as spot_version_id',
                    'spot_versions.title',
                    'spot_versions.price',
                    'spot_versions.shipping_cost',
                    'c.name as category',
                    'sc.name as subcategory'
                )
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
                    ->leftJoin('categories as c', 'spot_versions.category_id', '=', 'c.id')
                    ->leftJoin('categories as sc', 'spot_versions.sub_category_id', '=', 'sc.id')
                    ->leftJoin('companies', 'spot_versions.marketing_company_id', '=', 'companies.id')
                    ->whereBetween(
                        'spot_airtimes.air_date',
                        [$year->copy()->startOfYear(),
                              $year->copy()->endOfYear()]
                    )
                    ->where('spot_versions.language_id', $report->language_id) // English
                    ->where('spot_versions.non_dr', '=', 0) //Don't include non-dr
                    ->whereNull('spot_airtimes.deleted_at')
                    ->groupBy('spot_versions.spot_id')
                    ->orderBy('airings', 'desc')
                    ->take($report->limit)
                    ->get();

                View::share('data', $spot);
            } elseif ($report->slug == 'annual-top-100-spot-product') {
                $spot = SpotDetection::select(
                    DB::raw('SUM(spot_detections.detections) as airings'),
                    DB::raw('AVG(spot_versions.length) as average_length'),
                    DB::raw('sum(spot_detections.media_index) as media_index'),
                    'spot_versions.id as spot_version_id',
                    'spot_versions.title',
                    'spot_versions.price',
                    'spot_versions.shipping_cost',
                    'spot_detections.new_for_week',
                    'c.name as category',
                    'sc.name as subcategory'
                )
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
                    ->leftJoin('categories as c', 'spot_versions.category_id', '=', 'c.id')
                    ->leftJoin('categories as sc', 'spot_versions.sub_category_id', '=', 'sc.id')
                    ->leftJoin('companies', 'spot_versions.marketing_company_id', '=', 'companies.id')
                    ->whereBetween(
                        'rank_date',
                        [$year->copy()->startOfYear(),
                              $year->copy()->endOfYear()]
                    )
                    ->where('spot_detections.language_id', $report->language_id)
                    ->where('spot_versions.service', 0)
                    ->where('spot_versions.non_dr', '=', 0) //Don't include non-dr
                    ->where('spot_versions.brand_advert', '=', 0)
                    ->groupBy('spot_versions.spot_id')
                    ->orderBy('airings', 'desc')
                    ->take($report->limit)

                    ->get();

                View::share('data', $spot);
            } elseif ($report->slug == 'monthly-spanish-top-25-frequency-sf' or
                      $report->slug == 'monthly-spanish-top-50-frequency-sf' or
                      $report->slug == 'monthly-australian-top-25-short-form' or
                      $report->slug == 'monthly-australian-top-50-short-form' or
                      $report->slug == 'monthly-australian-top-100-short-form') {
                //Report ID: 37/24
                $spot = SpotDetection::select(
                    DB::raw('SUM(spot_detections.detections) as airings'),
                    DB::raw('AVG(spot_versions.length) as average_length'),
                    'spot_versions.id as spot_version_id',
                    'spot_versions.title',
                    'spot_versions.price',
                    'spot_versions.shipping_cost',
                    'spot_detections.new_for_week',
                    'c.name as category',
                    'sc.name as subcategory',
                    'companies.name as company_name'
                )
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
                    ->leftJoin('categories as c', 'spot_versions.category_id', '=', 'c.id')
                    ->leftJoin('categories as sc', 'spot_versions.sub_category_id', '=', 'sc.id')
                    ->leftJoin('companies', 'spot_versions.marketing_company_id', '=', 'companies.id')
                    ->whereBetween(
                        'rank_date',
                        [$month->copy()->startOfMonth()->toDateTimeString(),
                              $month->copy()->endOfMonth()->toDateTimeString()]
                    )
                    ->where('spot_detections.language_id', $report->language_id)
                    ->groupBy('spot_versions.spot_id')
                    ->orderBy('airings', 'desc')
                    ->take($report->limit)

                    ->get();

                View::share('data', $spot);
            } elseif ($report->slug == 'monthly-spanish-top-25-frequency-lf' or
                      $report->slug == 'monthly-spanish-top-50-frequency-lf' or
                      $report->slug == 'monthly-spanish-top-100-frequency-lf') {
             //Report ID: 32/33/23
                //---------------------------------------------------------------
                // Slug: Monthly top Spanish 25/50/100 frequency lf
                //---------------------------------------------------------------
                $programs = Airtime::with([
                        'programVersion',
                        'programVersion.category',
                        'programVersion.marketingCompany',
                    ])
                    ->select([
                        'airtimes.program_version_id',
                        DB::raw('count(DISTINCT airtimes.channel_id) as stations'),
                        DB::raw('count(airtimes.program_version_id) as airs'),
                    ])
                    ->leftJoin('program_versions as p', 'p.id', '=', 'airtimes.program_version_id')
                    ->whereHas('programVersion', function ($query) use ($report) {
                        $query->where('language_id', $report->language_id)
                            ->where('id', '!=', 11560); //Regular Programming
                    })
                    ->whereBetween(
                        'airtimes.air_date',
                        [$month->copy()->startOfMonth()->toDateTimeString(),
                              $month->copy()->endOfMonth()->toDateTimeString()]
                    )
                    ->verified()
                    ->groupBy('p.program_id')
                    ->orderBy('airs', 'desc')
                    ->take($report->limit)
                    ->get();

                View::share('data', $programs);
            } elseif ($report->slug == 'weekly-spanish-top-25-frequency-lf' or
                      $report->slug == 'weekly-spanish-top-50-frequency-lf') {
                $programs = Airtime::with('ProgramVersion', 'ProgramVersion.category')
                    ->select(
                        'airtimes.program_version_id',
                        DB::raw('count(DISTINCT airtimes.channel_id) as stations'),
                        DB::raw('count(airtimes.program_version_id) as airs')
                    )
                    ->leftJoin('program_versions as p', 'p.id', '=', 'airtimes.program_version_id')
                    ->where('p.language_id', $report->language_id) //Spanish
                    ->where('p.id', '!=', 11560) //Regular Programming
                    ->whereBetween(
                        'air_date',
                        [$week_ending->copy()->subDays(7)->startOfDay()->toDateTimeString(),
                            $week_ending->copy()->endOfDay()->toDateTimeString()]
                    )
                    ->verified()
                    ->groupBy('p.program_id')
                    ->orderBy('airs', 'desc')
                    ->take($report->limit)
                    ->get();

                View::share('data', $programs);
            } elseif ($report->slug == 'new-product-rollout-report') {
                $firstDate = $week_ending->copy()->subWeeks(15)->startOfDay();

                $programs = ProgramVersion::with('category', 'subCategory', 'marketingCompany')
                    ->select([
                        'program_versions.id',
                        'program_versions.program_id',
                        'program_versions.title',
                        'program_versions.initial_date',
                        'program_versions.category_id',
                        'program_versions.sub_category_id',
                        'program_versions.marketing_company_id',
                        DB::raw('MAX(airtimes.air_date) as last_aired'),
                        DB::raw('COUNT(airtimes.id) as airs'),
                        DB::raw('COUNT(DISTINCT airtimes.channel_id) as stations'),
                        DB::raw('SUM(airtimes.cost) as media')
                    ])
                    ->leftJoin('airtimes', 'program_versions.id', '=', 'airtimes.program_version_id')
                    ->whereBetween(
                        'monitor_date',
                        [$firstDate->format("Y-m-d"),
                            $week_ending->format("Y-m-d")
                        ]
                    )
                    ->where('program_versions.language_id', 1)
                    ->where('version', 1)
                    ->orderBy('title')
                    ->groupBy('program_versions.program_id')
                    ->get();

                $rollout = [];
                $i = 0;
                foreach ($programs as $program) {
                    $rollout[$i]['id'] = $program->id;
                    $rollout[$i]['program_id'] = $program->program_id;
                    $rollout[$i]['title'] = $program->title;
                    $rollout[$i]['initial_date'] = $program->initial_date;
                    $rollout[$i]['category_name'] = $program->category->name;
                    $rollout[$i]['subcategory_name'] = $program->subCategory->name;
                    $rollout[$i]['marketingCompany'] = isset($program->marketingCompany) ? $program->marketingCompany->name : '';
                    $rollout[$i]['last_aired'] = $program->last_aired;
                    $rollout[$i]['airs'] = $program->airs;
                    $rollout[$i]['stations'] = $program->stations;
                    $rollout[$i]['media'] = $program->media;

                    $pro = Airtime::select(DB::raw('count(*) as aggregate'))
                        ->where('program_version_id', $program->id)
                        ->whereBetween('air_date', [$firstDate->format("Y-m-d H:i:s"),$week_ending->format("Y-m-d H:i:s")])
                        ->verified()
                        ->groupBy(DB::raw('YEARWEEK(air_date)'))
                        ->get();

                    $rollout[$i]['pro'] = count($pro);
                    $i++;
                }

                return view(
                    'cxp.reports.reports.'.$report->template,
                    compact(
                        'report',
                        'week_ending',
                        'year',
                        'years',
                        'month',
                        'months',
                        'weeks',
                        'rollout'
                    )
                );
            } elseif ($report->slug == 'category-report') {
                return view('cxp.reports.reports.category-report-periscope')
                    ->with('report', $report);

                //Get the range
                if (Request::filled('start-date') and Request::filled('end-date')) {
                    $start_date = Carbon::createFromFormat("F j, Y", Request::get('start-date'));
                    $end_date = Carbon::createFromFormat("F j, Y", Request::get('end-date'));

                    if ($start_date->gte($end_date)) {
                        return Redirect::route('reports.show', $reportID)
                            ->with('error', 'Start date must be before end date.');
                    }
                } else {
                    $end_date = lastFriday();
                    $start_date = lastFriday()->subWeeks(4);
                }

                $categories = Category::whereNull('parent_id')->orderBy('name')->get();
                $resultset = [];

                foreach ($categories as $category) {
                    //Initialize this date for each category
                    $loop_date = Carbon::createFromFormat('Y-m-d', $start_date->format('Y-m-d'));
                    while ($loop_date->lt($end_date)) {
                        $results = DB::table('airtimes')
                            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
                            ->leftJoin('categories', 'program_versions.category_id', '=', 'categories.id')
                            ->select(
                                DB::raw('unix_timestamp(\''.$loop_date->format('Y-m-d').'\')*1000 as month'),
                                DB::raw('SUM(airtimes.cost) as spend')
                            )
                            ->where(
                                function ($query) use ($loop_date) {
                                    $sub_week = Carbon::createFromFormat('Y-m-d', $loop_date->format('Y-m-d'));
                                    $query->whereBetween('air_date', [$sub_week->subWeek()->startOfDay()->format('Y-m-d H:i:s'), $loop_date->endOfDay()->format('Y-m-d H:i:s')]);
                                }
                            )
                            ->where('categories.id', $category->id)
                            ->where('airtimes.cost', '<>', '')

                            ->get();

                        $resultset[$category->id]['category_name'] = $category->name;
                        $resultset[$category->id]['category_slug'] = slugify($category->name);

                        if ($results) {
                            foreach ($results as $result) {
                                //print_r($result);
                                if ($result->spend == null) {
                                    $resultset[$category->id]['data'][$loop_date->format('Y-m-d')] = [$result->month,0];
                                } else {
                                    $resultset[$category->id]['data'][$loop_date->format('Y-m-d')] = [$result->month,$result->spend];
                                }
                            }
                        } else {
                            $resultset[$category->id]['data'][$loop_date->format('Y-m-d')] = [];
                        }
                        $loop_date->addWeek();
                    }
                }

                //dd(DB::getQueryLog());
                //dd($resultset);
                return view(
                    'cxp.reports.reports.'.$report->template,
                    compact(
                        'report',
                        'week_ending',
                        'year',
                        'years',
                        'month',
                        'months',
                        'weeks',
                        'categories',
                        'resultset',
                        'start_date',
                        'end_date'
                    )
                );
            } elseif ($report->slug == 'national-infomercial-grids' or
                    $report->slug == 'spanish-infomercial-grids' or
                    $report->slug == 'national-paid-programming-grids') {
                if (Request::filled('report-date')) {
                    $report_date = Carbon::createFromFormat("F j, Y", Request::get('report-date'));
                    $allowed = data_allowed($report_date);
                    if (! $allowed) {
                        return abort('401', 'Access denied - Date outside of range');
                    }
                } else {
                    $report_date = $week_ending;
                }

                $times = thirty_minute_times($report_date);

                $dbGetter = Carbon::createFromFormat("Y-m-d H:i:s", $report_date->format("Y-m-d H:i:s"));
                $betweenStart = $dbGetter->startOfDay()->format('Y-m-d H:i:s');
                $betweenEnd = $dbGetter->endOfDay()->format('Y-m-d H:i:s');

                //Get the Channels Captured:
                $channels = Airtime::select('channel_id', 'channels.name', 'channels.logo_location')
                    ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
                    ->whereBetween('air_date', [$betweenStart, $betweenEnd])
                    ->where('channels.language_id', $report->language_id)
                    ->verified()
                    ->where(function ($query) use ($report) {
                        if ($report->slug == 'national-paid-programming-grids') {
                            return $query->where('channels.pi', 1)
                                ->orWhere('channels.id', 78); //WIZE
                        } else {
                            return $query->where('channels.pi', 0);
                        }
                    })
                    ->orderBy('channels.abbr')

                    ->distinct('channel_id')
                    ->get();

                $gridSet = [];

                $gridChannels = [];
                foreach ($channels as $channel) {
                    $airtimes = Airtime::select(
                        'program_versions.grid_title',
                        DB::raw('TIME(airtimes.air_date) as air_date')
                    )
                        ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
                        ->whereBetween('air_date', [$betweenStart, $betweenEnd])
                        ->where('airtimes.channel_id', $channel->channel_id)
                        ->verified()
                        ->orderBy('air_date')

                        ->get();

                    $gridChannels[$channel->channel_id] = [
                        'name' => $channel->name,
                        'logo' => $channel->logo_location
                    ];

                    if ($channel->logo_location) {
                        $gridSet[$channel->channel_id] = ['<img src="https://s3-us-west-2.amazonaws.com/ims-logos/'.$channel->logo_location.'" class="gridlogo" title="'.$channel->name.'" data-toggle="tooltip" title="'.$channel->name.'" /><br>'.$channel->name];
                    } else {
                        $gridSet[$channel->channel_id] = ['<p class="text-center" title="'.$channel->name.'">'.$channel->name.'</p>'];
                    }

                    $airtimesArr = $airtimes->toArray();

                    foreach ($times as $key => $time) {
                        $airtime = recursive_array_search($key, $airtimesArr);
                        if ($airtime !== false) {
                            array_push($gridSet[$channel->channel_id], $airtimesArr[$airtime]['grid_title']);
                        } else {
                            array_push($gridSet[$channel->channel_id], "");
                        }
                    }
                }

                $times = array_values($times);

                return view(
                    'cxp.reports.reports.'.$report->template,
                    compact(
                        'report',
                        'report_date',
                        'times',
                        'gridSet',
                        'gridChannels'
                    )
                );
            } else {
                return abort('404', 'This report doesn\'t exist yet!');
            }

            return view('cxp.reports.show')
                ->with('report', $report)
                ->with('week_ending', $week_ending)
                ->with('year', $year)
                ->with('years', $years)
                ->with('month', $month)
                ->with('months', $months)
                ->with('weeks', $weeks);
        } else {
            return abort('401', 'Access denied - Report not allowed');
        }
    }

    public function upsell()
    {
        return view('cxp.reports.email-campaigns.upsell');
    }

    public function showEmailCampaign($reportSlug = null)
    {
        if (Request::filled('hash')) {
            $hash = UserHash::where('hash', Request::get('hash'))->first();

            if ($hash) {
                $this->logView(Request::get('hash'), $reportSlug, true);
            } else {
                $this->logView(Request::get('hash'), $reportSlug, false);

                return Redirect::route('email-campaign.upsell');
            }
        } else {
            return Redirect::route('email-campaign.upsell');
        }

        if ($hash->period_type == 'week') {
            switch ($reportSlug) {
                case 'weekly-100-frequency-lf':
                    $report = Report::findOrFail(3);
                    ReportGenerator::weeklyFrequencyLongForm($hash->period_end, $report);
                    break;
                case 'weekly-100-media-index-lf':
                    $report = Report::findOrFail(1);
                    ReportGenerator::weeklyMediaIndexLongForm($hash->period_end, $report);
                    break;
                case 'weekly-50-frequency-lf':
                    $report = Report::findOrFail(17);
                    ReportGenerator::weeklyFrequencyLongForm($hash->period_end, $report);
                    break;
                case 'weekly-50-media-index-lf':
                    $report = Report::findOrFail(19);
                    ReportGenerator::weeklyMediaIndexLongForm($hash->period_end, $report);
                    break;
                case 'weekly-25-frequency-lf':
                    $report = Report::findOrFail(4);
                    ReportGenerator::weeklyFrequencyLongForm($hash->period_end, $report);
                    break;
                case 'weekly-25-media-index-lf':
                    $report = Report::findOrFail(18);
                    ReportGenerator::weeklyMediaIndexLongForm($hash->period_end, $report);
                    break;
                case 'weekly-50-direct-response-sf':
                    $report = Report::findOrFail(68);
                    ReportGenerator::weeklyDirectResponseShortForm($hash->period_end, $report);
                    break;
                case 'weekly-50-product-ranking-sf':
                    $report = Report::findOrFail(25);
                    ReportGenerator::weeklyProductRankingShortForm($hash->period_end, $report);
                    break;
                case 'weekly-25-direct-response-sf':
                    $report = Report::findOrFail(67);
                    ReportGenerator::weeklyDirectResponseShortForm($hash->period_end, $report);
                    break;
                case 'weekly-25-product-ranking-sf':
                    $report = Report::findOrFail(36);
                    ReportGenerator::weeklyProductRankingShortForm($hash->period_end, $report);
                    break;
                default:
                    return Redirect::route('email-campaign.upsell');
            }
        } else {
            switch ($reportSlug) {
                case 'monthly-100-frequency-lf':
                    $report = Report::findOrFail(20);
                    ReportGenerator::monthlyFrequencyLongForm($hash->period_end, $report);
                    break;
                case 'monthly-100-media-index-lf':
                    $report = Report::findOrFail(27);
                    ReportGenerator::monthlyMediaIndexLongForm($hash->period_end, $report);
                    break;
                case 'monthly-50-direct-response-sf':
                    $report = Report::findOrFail(22);
                    ReportGenerator::monthlyDirectResponseShortForm($hash->period_end, $report);
                    break;
                case 'monthly-50-product-ranking-sf':
                    $report = Report::findOrFail(70);
                    ReportGenerator::monthlyProductRankingShortForm($hash->period_end, $report);
                    break;
                default:
                    return Redirect::route('email-campaign.upsell');
            }
        }

        return view('cxp.reports.email-campaigns.show');
    }

    private function logView($hash, $slug, $active = true)
    {
        $data = [
            'hash' => $hash,
            'slug' => $slug,
            'access' => $active,
            'ip_address' => Request::getClientIp(),
            'ip_addresses' => implode(Request::getClientIps(), ', '),
            'user_agent' => Request::server('HTTP_USER_AGENT'),
            'device' => Agent::device(),
            'platform' => Agent::platform(),
            'browser' => Agent::browser(),
            'languages' => implode(Agent::languages(), ', ')
        ];
        dispatch((new LogEmailAnalytics($data))->onQueue('analytics'));
    }
}
