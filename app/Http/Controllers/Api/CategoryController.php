<?php namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function getCategories(Request $request)
    {
        if ($request->filled('parent_id')) {
            $categories = Category::where('parent_id', $request->input('parent_id'))
                ->pluck('name', 'id')
                ->toArray();
        } else {
            $categories = Category::whereNull('parent_id')
                ->pluck('name', 'id')
                ->toArray();
        }

        return response()->json(
            [
            'error' => false,
            'categories' => $categories],
            200
        );
    }

    public function searchCategories(Request $request)
    {
        $categories = Category::select(['id', 'name'])
            ->whereNull('parent_id')
            ->where(function ($query) use ($request) {
                if ($request->filled('q')) {
                    $query->where('name', 'LIKE', '%'.$request->input('q').'%');
                }
            })
            ->get();

        $return_array = [
            'error' => false,
            'total' => $categories->count(),
            'categories' => $categories
        ];

        if ($request->filled('callback')) {
            return response()->json($return_array)->setCallback($request->input('callback'));
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getCategory(Request $request)
    {
        if ($request->filled('category_id') and $request->input('category_id') != 0) {
            $category = Category::select([
                    'id',
                    'name',
                ])
                ->where('id', $request->get('category_id'))
                ->first();

            $return_array = [
                'error' => false,
                'category' => $category
            ];

            if ($request->filled('callback')) {
                return response()->json($category)->setCallback($request->get('callback'));
            } else {
                return response()->json(
                    $return_array,
                    200
                );
            }
        } else {
            $return_array = [
                'error' => false,
                'category' => []
            ];

            return response()->json(
                $return_array,
                200
            );
        }
    }
}
