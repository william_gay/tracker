<?php namespace App\Http\Controllers\Api;

use App\Models\SpotVersion;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class SpotVersionController extends Controller
{
    public function getSpots()
    {
        if (Request::filled('q') and Request::filled('language_id')) {
            $spots = SpotVersion::select(
                'id',
                DB::raw('CONCAT(title, " v", version, " ", DATE_FORMAT(air_date,"%m/%d/%y"), " :", length, " ", notes) as title')
            )
                ->where('title', 'LIKE', '%'.Request::get('q').'%')
                ->where('language_id', Request::get('language_id'))
                ->orderBy('air_date', 'desc')
                ->take(Request::get('page_limit'))
                ->get();
        } else if (Request::filled('q')) {
            $spots = SpotVersion::select(
                'spot_versions.id',
                DB::raw('CONCAT(title, " v", version, " ", DATE_FORMAT(air_date,"%m/%d/%y"), " :", length, " ", notes, " -- Market: ", languages.name) as title')
            )
                ->leftJoin('languages', 'languages.id', '=', 'spot_versions.language_id')
                ->where('title', 'LIKE', '%'.Request::get('q').'%')
                ->orderBy('air_date', 'desc')
                ->take(Request::get('page_limit'))
                ->get();
        } else {
            $spots = SpotVersion::select(
                'id',
                DB::raw('CONCAT(title, " v", version, " ", DATE_FORMAT(air_date,"%m/%d/%y"), " :", length, " ", notes) as title')
            )
                ->where('language_id', Request::get('language_id'))
                ->orderBy('air_date', 'desc')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'spots' => $spots->toArray()
        ];

        if (Request::filled('callback')) {
            return response()->json($return_array)->setCallback(Request::get('callback'));
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getSpot()
    {
        if (Request::filled('spot_version_id') and Request::get('spot_version_id') != 0) {
            $spot = SpotVersion::select(
                'id',
                DB::raw('CONCAT(title, " v", version, " ", DATE_FORMAT(air_date,"%m/%d/%y"), " :", length, " ", notes) as title')
            )
                ->find(Request::get('spot_version_id'));

            $return_array = [
                'error' => false,
                'spot' => $spot->toArray()
            ];

            if (Request::filled('callback')) {
                return Request::get('callback').'('
                    .json_encode($spot->toArray()).
                ')';
            } else {
                return response()->json(
                    $return_array,
                    200
                );
            }
        } else {
            $return_array = [
                'error' => false,
                'spot' => []
            ];

            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getSpotFind($spotVersionId)
    {
        return SpotVersion::findOrFail($spotVersionId);
    }
}
