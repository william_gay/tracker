<?php namespace App\Http\Controllers\Api;

use App\Models\Program;
use App\Models\ProgramVersion;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class ProgramController extends Controller
{
    public function getPrograms()
    {
        if (Request::filled('q') and Request::filled('language_id')) {
            $programs = ProgramVersion::select([
                    'program_id as id',
                    DB::raw('CONCAT(MIN(grid_title), " (", title, ")") as title')
                ])
                ->where('language_id', Request::get('language_id'))
                ->where(function ($query) {
                    return $query->where('title', 'like', '%'.Request::get('q').'%')
                        ->orWhere('grid_title', 'like', '%'.Request::get('q').'%');
                })
                ->orderBy('title', 'asc')
                ->groupBy('program_id')
                ->take(Request::get('page_limit'))
                ->get();
        } elseif (Request::filled('q')) {
            $programs = ProgramVersion::select([
                    'program_id as id',
                    DB::raw('CONCAT(MIN(grid_title), " (", title, ") -- Market: ", languages.name) as title')
                ])
                ->leftJoin('languages', 'languages.id', '=', 'program_versions.language_id')
                ->where(function ($query) {
                    return $query->where('title', 'like', '%'.Request::get('q').'%')
                        ->orWhere('grid_title', 'like', '%'.Request::get('q').'%');
                })
                ->orderBy('title', 'asc')
                ->groupBy('program_id')
                ->take(Request::get('page_limit'))
                ->get();
        } elseif (Request::filled('language_id')) {
            $programs = ProgramVersion::select([
                    'program_id as id',
                    DB::raw('CONCAT(MIN(grid_title), " (", title, ")") as title')
                ])
                ->where('language_id', Request::get('language_id'))
                ->orderBy('title', 'asc')
                ->groupBy('program_id')
                ->take(Request::get('page_limit'))
                ->get();
        } else {
            $programs = ProgramVerison::select(
                'program_id as id',
                DB::raw('CONCAT(title, " -- Market: ", languages.name) as title')
            )
                ->leftJoin('languages', 'languages.id', '=', 'program_versions.language_id')
                ->orderBy('title', 'asc')
                ->groupBy('program_id')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'total' => $programs->count(),
            'programs' => $programs
        ];

        if (Request::filled('callback')) {
            return response()->json($return_array)->setCallback(Request::get('callback'));
            ')';
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getProgram()
    {
        if (Request::filled('program_id') and Request::get('program_id') != 0) {
            $program = Program::select(
                'programs.id',
                DB::raw('CONCAT(title, " -- Market: ", languages.name) as title')
            )
                ->leftJoin('languages', 'languages.id', '=', 'programs.language_id')
                ->where('programs.id', Request::get('program_id'))
                ->first();

            $return_array = [
                'error' => false,
                'program' => $program
            ];

            if (Request::filled('callback')) {
                return response()->json($program)->setCallback(Request::get('callback'));
            } else {
                return response()->json(
                    $return_array,
                    200
                );
            }
        } else {
            $return_array = [
                'error' => false,
                'program' => []
            ];

            return response()->json(
                $return_array,
                200
            );
        }
    }
}
