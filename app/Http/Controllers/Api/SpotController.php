<?php namespace App\Http\Controllers\Api;

use App\Models\Spot;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class SpotController extends Controller
{
    public function getSpots()
    {
        if (Request::filled('q') and Request::filled('language_id')) {
            $spots = Spot::select([
                    'id',
                    'title'
                ])
                ->where('title', 'LIKE', '%'.Request::get('q').'%')
                ->where('language_id', Request::get('language_id'))
                ->orderBy('title', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        } elseif (Request::filled('q')) {
            $spots = Spot::select([
                    'spots.id',
                    DB::raw('CONCAT(title, " -- Market: ", languages.name) as title')
                ])
                ->leftJoin('languages', 'languages.id', '=', 'spots.language_id')
                ->where('title', 'LIKE', '%'.Request::get('q').'%')
                ->orderBy('title', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        } elseif (Request::filled('language_id')) {
            $spots = Spot::select([
                    'id',
                    'title'
                ])
                ->where('language_id', Request::get('language_id'))
                ->orderBy('title', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        } else {
            $spots = Spot::select([
                    'spots.id',
                    DB::raw('CONCAT(title, " -- Market: ", languages.name) as title')
                ])
                ->leftJoin('languages', 'languages.id', '=', 'spots.language_id')
                ->orderBy('title', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'spots' => $spots->toArray()
        ];

        if (Request::filled('callback')) {
            return response()->json($return_array)->setCallback(Request::get('callback'));
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getSpot()
    {
        if (Request::filled('spot_id') and Request::get('spot_id') != 0) {
            $spot = Spot::select([
                    'spots.id',
                    DB::raw('CONCAT(title, " -- Market: ", languages.name) as title')
                ])
                ->leftJoin('languages', 'languages.id', '=', 'spots.language_id')
                ->where('spots.id', Request::get('spot_id'))
                ->first();

            if ($spot) {
                $return_array = [
                    'error' => false,
                    'spot' => $spot->toArray()
                ];
            } else {
                $return_array = [
                    'error' => false,
                    'spot' => []
                ];
            }

            if (Request::filled('callback')) {
                return response()->json($spot)->setCallback(Request::get('callback'));
            } else {
                return response()->json(
                    $return_array,
                    200
                );
            }
        } else {
            $return_array = [
                'error' => false,
                'spot' => []
            ];

            return response()->json(
                $return_array,
                200
            );
        }
    }
}
