<?php namespace App\Http\Controllers\Api;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function getProducts()
    {
        if (Request::filled('q')) {
            $products = Product::select(
                'id',
                'name'
            )
                ->where('name', 'like', '%'.Request::get('q').'%')
                ->orderBy('name', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        } else {
            $products = Product::select(
                'id',
                'name'
            )
                ->orderBy('name', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'total' => $products->count(),
            'products' => $products->toArray()
        ];

        if (Request::filled('callback')) {
            return Request::get('callback').'('
                .json_encode($return_array).
            ')';
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getProduct()
    {
        if (Request::filled('product_id') and Request::get('product_id') != 0) {
            $product = Product::select(
                'id',
                'name'
            )
                ->where('products.id', Request::get('product_id'))
                ->first();

            $return_array = [
                'error' => false,
                'product' => $product->toArray()
            ];

            if (Request::filled('callback')) {
                return Request::get('callback').'('
                    .json_encode($product->toArray()).
                ')';
            } else {
                return response()->json(
                    $return_array,
                    200
                );
            }
        } else {
            $return_array = [
                'error' => false,
                'product' => []
            ];

            return response()->json(
                $return_array,
                200
            );
        }
    }
}
