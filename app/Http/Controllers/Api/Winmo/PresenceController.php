<?php

namespace App\Http\Controllers\Api\Winmo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Product;
use App\Http\Resources\Winmo\Presence as PresenceResource;
use Illuminate\Support\Facades\Cache;

class PresenceController extends Controller
{

    public function index(Request $request)
    {
        $request->validate([
            'winmo_company_id' => 'numeric',
            'winmo_brand_id' => 'numeric'
        ]);
        $only = http_build_query($request->only('winmo_company_id', 'winmo_brand_id'));

        $products = Cache::remember('winmo-products-with-count-'.$only, 3600, function () use ($request) {
            return Product::winmo()
                ->when(($request->has('winmo_company_id') && $request->input('winmo_company_id') == 0), function ($query, $winmoBrandId) {
                    return $query->where('winmo_company_id', 0);
                })
                ->when(($request->has('winmo_brand_id') && $request->input('winmo_brand_id') == 0), function ($query, $winmoBrandId) {
                    return $query->where('winmo_brand_id', 0);
                })
                ->where(function ($query) use ($request) {
                    $query->when($request->input('winmo_company_id'), function ($query, $winmoCompanyId) {
                        return $query->orWhere('winmo_company_id', $winmoCompanyId);
                    })
                    ->when($request->input('winmo_brand_id'), function ($query, $winmoBrandId) {
                        return $query->orWhere('winmo_brand_id', $winmoBrandId);
                    });
                })
                ->get();
        });

        return PresenceResource::collection($products);
    }

    public function show(Request $request, $productId)
    {
        $product = Cache::remember('winmo-product-with-count-'.$productId, 3600, function () use ($productId) {
            return Product::withCount(['spots', 'programs'])
                ->whereNotNull('winmo_company_id')
                ->orWhereNotNull('winmo_brand_id')
                ->findOrFail($productId);
        });

        return new PresenceResource($product);
    }
}
