<?php

namespace App\Http\Controllers\Api\Winmo;

use App\Http\Controllers\Controller;
use App\Http\Resources\Winmo\Predictor as PredictorResource;
use App\Models\Product;
use Illuminate\Http\Request;


class PredictorController extends Controller
{
    public function show(Request $request, $productId)
    {
        $product = Product::with('predictor')->findOrFail($productId);

        return new PredictorResource($product);
    }
}
