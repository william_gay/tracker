<?php

namespace App\Http\Controllers\Api\Winmo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

use App\Models\Channel;
use App\Models\Product;
use App\Models\Airtime;
use App\Models\SpotAirtime;
use App\Models\ProgramVersion;
use App\Models\SpotVersion;
use App\Http\Resources\Winmo\Network as NetworkResource;

class NetworkController extends Controller
{

    public function index(Request $request)
    {
        $channels = Channel::with(['products' => function ($query) {
            $query->orderBy('airings', 'desc')->limit(5);
        }])->active()->get();

        return NetworkResource::collection($channels);
    }
}
