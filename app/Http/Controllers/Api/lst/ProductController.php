<?php namespace App\Http\Controllers\Api\Lst;

use App\Models\LiveShoppingTracker\LiveShoppingProduct;
use Carbon\Carbon;
use Response;
use Sentry;
use Former;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function getProducts(Request $request)
    {
        if ($request->filled('q')) {
            $products = LiveShoppingProduct::with('versions')
                ->orderBy('name')
                ->where('name', 'LIKE', '%'.$request->get('q').'%')
                ->get();
        } else {
            $products = LiveShoppingProduct::with('versions')->orderBy('name')->get();
        }

        $return_array = [
            'error' => false,
            'total' => $products->count(),
            'products' => $products->toArray()
        ];

        if ($request->filled('callback')) {
            return response()->json(
                $return_array,
                200
            )->setCallback($request->get('callback'));
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getProduct(Request $request)
    {
        if ($request->filled('lst_product_id') and $request->get('lst_product_id') != 0) {
            $product = LiveShoppingProduct::with('versions')->find($request->get('lst_product_id'));

            return response()->json(
                $product,
                200
            )->setCallback($request->get('callback'));
        } else {
            return response()->json(
                [], //Nothing!
                200
            )->setCallback($request->get('callback'));
        }
    }

    public function add(Request $request)
    {
        $product = LiveShoppingProduct::where('name', trim($request->get('name')))->first();

        // If product, doesn't exist, create it
        if (! $product) {
            $air_date = Carbon::createFromFormat('Y-m-d h:i A', $request->get('air_date'));

            $product = new LiveShoppingProduct;
            $product->name = trim($request->get('name'));
            $product->msrp = $request->get('msrp');
            $product->sku = $request->get('sku');
            $product->website = $request->get('website');
            $product->first_airdate = $air_date;
            $product->live_shopping_category_id = $request->get('category_id');
            $product->live_shopping_sub_category_id = $request->get('sub_category_id');
            $product->notes = $request->get('notes');
            $product->user_id = Sentry::getUser()->id;
            $product->save();

            $this->s3Magic($request, $product);
        }

        return response()->json($product);
    }

    public function update(Request $request, $productId)
    {
        $product = LiveShoppingProduct::find($productId);
        $product->msrp = $request->get('msrp');
        $product->sku = $request->get('sku');
        $product->website = $request->get('website');
        $product->live_shopping_category_id = $request->get('category_id');
        $product->live_shopping_sub_category_id = $request->get('sub_category_id');
        $product->notes = $request->get('notes');
        $product->user_id = Sentry::getUser()->id;
        $product->save();

        $this->s3Magic($request, $product);

        return response()->json($product);
    }

    public function s3Magic($request, $product)
    {
        if ($request->hasFile('product_img')) {
            $path = $request->product_img
                ->storeAs('products/'.$product->id, 'logo.png', 's3-lst');

            $product->img_location = $path;
        }
    }
}
