<?php namespace App\Http\Controllers\Api\Lst;

use App\Models\LiveShoppingTracker\LiveShoppingCategory;
use Request;
use Response;

use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function getCategories()
    {
        if (Request::filled('parent_id')) {
            $categories = LiveShoppingCategory::where('parent_id', Request::get('parent_id'))->pluck('name', 'id')->toArray();
        } else {
            $categories = LiveShoppingCategory::whereNull('parent_id')->pluck('name', 'id')->toArray();
        }

        return response()->json(
            [
            'error' => false,
            'categories' => $categories],
            200
        );
    }
}
