<?php namespace App\Http\Controllers\Api\Lst;

use App\Models\LiveShoppingTracker\LiveShoppingShow;
use Carbon\Carbon;
use Request;
use Response;
use Sentry;

use App\Http\Controllers\Controller;

class ShowController extends Controller
{
    public function add()
    {
        //TO DO Validator Goes HERE

        $startTime = Carbon::createFromFormat('Y-m-d h:i A', Request::get('air_date'));
        if (Request::filled('end_time')) {
            $endTime = Carbon::createFromFormat('Y-m-d h:i A', Request::get('end_time'));
            $duration = $startTime->diffInMinutes($endTime);
        } else {
            $endTime = null;
            $duration = null;
        }

        $show = new LiveShoppingShow;
        $show->title = Request::get('custom_show_name');
        $show->channel_id = Request::get('channel_id');
        $show->first_airdate = $startTime;
        $show->duration = $duration;
        $show->notes = Request::get('show_notes');
        $show->user_id = Sentry::getUser()->id;
        $show->save();

        return response()->json($show);
    }

    public function update($showId)
    {
        $show = LiveShoppingShow::find($showId);
        $show->notes = Request::get('show_notes');
        $show->user_id = Sentry::getUser()->id;
        $show->save();

        return response()->json($show);
    }

    public function getShows()
    {
        if (Request::filled('q')) {
            $shows = LiveShoppingShow::orderBy('title')
                ->where('title', 'LIKE', '%'.Request::get('q').'%')
                ->take(Request::get('page_limit'))
                ->get();
        } else {
            $shows = LiveShoppingShow::orderBy('title')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'total' => $shows->count(),
            'shows' => $shows->toArray()
        ];

        return response()->json(
            $return_array,
            200
        )->setCallback(Request::get('callback'));
    }

    public function getShow()
    {
        if (Request::filled('lst_show_id') and Request::get('lst_show_id') != 0) {
            $show = LiveShoppingShow::select('id', 'title', 'notes')->find(Request::get('lst_show_id'));

            return response()->json(
                $show,
                200
            )->setCallback(Request::get('callback'));
        } else {
            return response()->json(
                [], // Empty Array
                200
            )->setCallback(Request::get('callback'));
        }
    }
}
