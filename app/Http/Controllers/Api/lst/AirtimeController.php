<?php namespace App\Http\Controllers\Api\Lst;

use App\Models\LiveShoppingTracker\LiveShoppingAirtime;
use Carbon\Carbon;
use Request;
use Response;
use Sentry;

use App\Http\Controllers\Controller;

class AirtimeController extends Controller
{

    public function get()
    {
        $startTime = Carbon::createFromFormat('Y-m-d h:i A', Request::get('air_date'));
        $channel_id = Request::get('channel_id');

        $airtime = LiveShoppingAirtime::where('channel_id', $channel_id)
            ->where('start_time', $startTime->toDateTimeString())
            ->first();

        if ($airtime) {
            $ret_arr = [
                'id' => $airtime->id,
                'live_shopping_show_id' => $airtime->live_shopping_show_id,
                'start_time' => $airtime->start_time->format('Y-m-d h:i A'),
                'end_time' => isset($airtime->end_time) ? $airtime->end_time->format('Y-m-d h:i A') : '',
                'air_date' => $airtime->start_time->format('Y-m-d h:i A'),
            ];

            return response()->json($ret_arr);
        }

        return response()->json($airtime);
    }

    public function add()
    {
        //TO DO Validator Goes HERE

        $startTime = Carbon::createFromFormat('Y-m-d h:i A', Request::get('air_date'));
        if (Request::filled('end_time')) {
            $endTime = Carbon::createFromFormat('Y-m-d h:i A', Request::get('end_time'));
        } else {
            $endTime = null;
        }

        $airtime = new LiveShoppingAirtime;
        $airtime->channel_id = Request::get('channel_id');
        $airtime->start_time = $startTime;
        $airtime->end_time = $endTime;
        $airtime->live_shopping_show_id = Request::get('live_shopping_show_id');
        $airtime->user_id = Sentry::getUser()->id;
        $airtime->save();

        return response()->json($airtime);
    }

    public function update($airtimeId)
    {
        //TO DO Validator Goes HERE

        $startTime = Carbon::createFromFormat('Y-m-d h:i A', Request::get('air_date'));
        if (Request::filled('end_time')) {
            $endTime = Carbon::createFromFormat('Y-m-d h:i A', Request::get('end_time'));
        } else {
            $endTime = null;
        }

        $airtime = LiveShoppingAirtime::find($airtimeId);
        $airtime->start_time = $startTime;
        $airtime->end_time = $endTime;
        $airtime->live_shopping_show_id = Request::get('live_shopping_show_id');
        $airtime->user_id = Sentry::getUser()->id;
        $airtime->save();

        return response()->json($airtime);
    }
}
