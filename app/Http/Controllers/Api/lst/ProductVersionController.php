<?php namespace App\Http\Controllers\Api\Lst;

use App\Models\LiveShoppingTracker\LiveShoppingProduct;
use App\Models\LiveShoppingTracker\LiveShoppingProductVersion;
use Carbon\Carbon;
use Request;
use Response;
use Sentry;

use App\Http\Controllers\Controller;

class ProductVersionController extends Controller
{
    public function getDropdown()
    {
        if (Request::filled('lst_product_id')) {
            $versions = LiveShoppingProductVersion::where('live_shopping_product_id', Request::get('lst_product_id'))->pluck('name', 'id')->toArray();

            return response()->json(
                [
                'error' => false,
                'products' => $versions],
                200
            );
        }

        return response()->json(
            [
            'error' => false,
            'products' => []],
            200
        );
    }

    public function getProducts()
    {
        if (Request::filled('q')) {
            $products = LiveShoppingProductVersion::with(['product'])
                ->whereHas('product', function ($query) {
                    $query->where('sku', 'LIKE', '%'.Request::get('q').'%');
                })
                ->orWhere('name', 'LIKE', '%'.Request::get('q').'%')
                ->orderBy('name')
                ->take(Request::get('page_limit'))
                ->get();
        } else {
            $products = LiveShoppingProductVersion::with(['product'])
                ->orderBy('name')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'total' => $products->count(),
            'products' => $products->toArray()
        ];

        if (Request::filled('callback')) {
            return response()->json(
                $return_array,
                200
            )->setCallback(Request::get('callback'));
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getVersion()
    {
        if (Request::filled('product_version_id') and Request::get('product_version_id') != 0) {
            $product = LiveShoppingProductVersion::with('product')->find(Request::get('product_version_id'));

            return response()->json(
                $product,
                200
            )->setCallback(Request::get('callback'));
        } else {
            return response()->json(
                [], //Nothing!
                200
            )->setCallback(Request::get('callback'));
        }
    }

    public function add()
    {
        $air_date = Carbon::createFromFormat('Y-m-d h:i A', Request::get('air_date'));
        if (Request::filled('name')) {
            $name = trim(Request::get('name'));
        } else {
            $product = LiveShoppingProduct::find(Request::get('live_shopping_product_id'));

            $name = $product->name;
        }

        $version = new LiveShoppingProductVersion;
        $version->name = $name;
        $version->live_shopping_product_id = Request::get('live_shopping_product_id');
        $version->product_configuration = Request::get('product_configuration');
        $version->first_airdate = $air_date;
        $version->price = Request::get('price');
        $version->max_price = Request::get('max_price');
        $version->shipping_and_handling = Request::get('shipping_and_handling');
        $version->payment_plan = Request::get('payment_plan');
        $version->payment_plan_notes = Request::get('payment_plan_notes');
        $version->daily_special_value = Request::get('daily_special_value');
        $version->units_sold = Request::get('units_sold');
        $version->notes = Request::get('notes');
        $version->user_id = Sentry::getUser()->id;
        $version->save();

        return response()->json($version);
    }

    public function update($versionId)
    {

        $version = LiveShoppingProductVersion::find($versionId);
        $version->live_shopping_product_id = Request::get('live_shopping_product_id');
        $version->product_configuration = Request::get('product_configuration');
        $version->price = Request::get('price');
        $version->max_price = Request::get('max_price');
        $version->shipping_and_handling = Request::get('shipping_and_handling');
        $version->payment_plan = Request::get('payment_plan');
        $version->payment_plan_notes = Request::get('payment_plan_notes');
        $version->daily_special_value = Request::get('daily_special_value');
        $version->units_sold = Request::get('units_sold');
        $version->notes = Request::get('notes');
        $version->user_id = Sentry::getUser()->id;
        $version->save();

        return response()->json($version);
    }
}
