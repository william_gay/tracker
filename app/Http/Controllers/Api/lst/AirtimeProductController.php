<?php namespace App\Http\Controllers\Api\Lst;

use App\Models\LiveShoppingTracker\LiveShoppingAirtimeProduct;
use Carbon\Carbon;
use Request;
use Response;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class AirtimeProductController extends Controller
{

    public function get()
    {
        $airtime_id = Request::get('show_airtime_id');

        $products = LiveShoppingAirtimeProduct::with(['airtime.channel', 'productVersion', 'productVersion.product', 'productVersion.product.category', 'productVersion.product.subCategory'])
            ->where('live_shopping_airtime_id', $airtime_id)
            ->orderBy('created_at')
            ->get();

        $channel = isset($products->first()->airtime->channel) ? $products->first()->airtime->channel : null;

        return view('admin.lst.ajax.airtime-product')
            ->with('products', $products)
            ->with('channel', $channel);
    }

    public function add()
    {
        //TO DO Validator Goes HERE
        if (Request::filled('product_start_time')) {
            $startTime = Carbon::createFromFormat('Y-m-d h:i A', Request::get('product_start_time'));
        } else {
            $startTime = null;
        }

        if (Request::filled('product_end_time')) {
            $endTime = Carbon::createFromFormat('Y-m-d h:i A', Request::get('product_end_time'));
        } else {
            $endTime = null;
        }

        $airtime = new LiveShoppingAirtimeProduct;
        $airtime->live_shopping_airtime_id = Request::get('show_airtime_id');
        $airtime->start_time = $startTime;
        $airtime->end_time = $endTime;
        $airtime->live_shopping_product_version_id = Request::get('lst_product_version_id');
        $airtime->user_id = Sentry::getUser()->id;
        $airtime->save();

        return response()->json($airtime);
    }

    public function update($airtimeId)
    {
        //TO DO Validator Goes HERE

        // $startTime = Carbon::createFromFormat('Y-m-d g:i A', Request::get('air_date').' '.Request::get('start_time'));
        // if (Request::filled('air_date') and Request::filled('end_time')) {
        //  $endTime = Carbon::createFromFormat('Y-m-d g:i A', Request::get('air_date').' '.Request::get('end_time'));
        // } else {
        //  $endTime = null;
        // }

        // $airtime = LiveShoppingAirtime::find($airtimeId);
        // $airtime->start_time = $startTime;
        // $airtime->end_time = $endTime;
        // $airtime->live_shopping_show_id = Request::get('live_shopping_show_id');
        // $airtime->user_id = Sentry::getUser()->id;
        // $airtime->save();

        // return response()->json($airtime);
    }

    public function delete($airtimeId)
    {
        $airtime = LiveShoppingAirtimeProduct::find($airtimeId);
        $airtime->delete();

        return response()->json($airtime);
    }
}
