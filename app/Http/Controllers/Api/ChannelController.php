<?php namespace App\Http\Controllers\Api;

use App\Models\Channel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class ChannelController extends Controller
{
    public function getCheckThumbnail()
    {
        if (Request::filled('channel')) {
            $channel_id = $this->getChannelId(Request::get('channel'));
        } else {
            return ['status' => 404, 'error' => 'You must supply a channel.'];
        }

        $channel = Channel::findOrFail($channel_id);

        if ($channel->spot_active == 1) {
            return ['status' => 200, 'error' => false, 'thumbnails' => true];
        } else {
            return ['status' => 200, 'error' => false, 'thumbnails' => false];
        }
    }

    private function getChannelId($channel)
    {
        return is_numeric($channel) ? $channel : Channel::where('abbr', '=', $channel)->first()->id;
    }

    public function getChannels()
    {
        if (Request::filled('q')) {
            $channels = Channel::select('channel as id', 'channel as text')
                ->where('channel', 'like', '%'.Request::get('q').'%')
                ->orderBy('channel', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        } else {
            $channels = Channel::select('channel as id', 'channel as text')
                ->orderBy('channel', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'total' => $channels->count(),
            'channels' => $channels->toArray()
        ];

        if (Request::filled('callback')) {
            return Request::get('callback').'('
                .json_encode($return_array).
            ')';
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getDropdown()
    {
        if (Request::filled('language_id')) {
            $channels = Channel::where('language_id', Request::get('language_id'))->where('capturing', 1)->pluck('name', 'id')->toArray();
        } else {
            $channels = Channel::pluck('name', 'id')->toArray();
        }

        return response()->json(
            [
            'error' => false,
            'channels' => $channels],
            200
        );
    }
}
