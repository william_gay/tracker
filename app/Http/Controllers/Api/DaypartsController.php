<?php namespace App\Http\Controllers\Api;

use App\Models\Daypart;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class DaypartsController extends Controller
{
    public function getDayparts()
    {
        if (! Request::filled('day')) {
            $day = Carbon::now();
        } else {
            $day = Carbon::createFromFormat('Y-m-d', Request::get('day'));
        }

        return Daypart::where('day_start', '<=', date('N', strtotime($day->toDateTimeString())))
            ->where('day_end', '>=', date('N', strtotime($day->toDateTimeString())))
            ->orderBy('order', 'asc')

            ->get();
    }

    public function getDaypart($daypartId = 0)
    {
        if ($daypartId > 0) {
            return Daypart::findOrFail($daypartId);
        }
    }
}
