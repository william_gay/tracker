<?php namespace App\Http\Controllers\Api;

use App\Models\Keyword;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class KeywordController extends Controller
{
    public function getKeywords()
    {
        if (Request::filled('q')) {
            $keywords = Keyword::select('keyword as id', 'keyword as text')
                ->where('keyword', 'like', '%'.Request::get('q').'%')
                ->orderBy('keyword', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        } else {
            $keywords = Keyword::select('keyword as id', 'keyword as text')
                ->orderBy('keyword', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'total' => $keywords->count(),
            'keywords' => $keywords->toArray()
        ];

        if (Request::filled('callback')) {
            return Request::get('callback').'('
                .json_encode($return_array).
            ')';
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }
}
