<?php namespace App\Http\Controllers\Api;

use App\Models\Airtime;
use App\Models\Channel;
use App\Models\Cost;
use App\Models\SpotCost;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\SpotAirtime;
use App\Models\Spot;
use App\Models\SpotVersion;
use App\Models\Daypart;
use App\Models\Autodetection\Clipster;
use App\Models\Autodetection\Log;
use App\Models\Autodetection\Exception;
use Carbon\Carbon;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class AutoDetectController extends Controller
{
    public function getShows()
    {
        if (Request::filled('channel')) {
            $channel = $this->getChannelId(Request::get('channel'));
        } else {
            return ['status' => 404, 'error' => 'You must supply a channel.'];
        }

        $shows = Airtime::with(['programVersion' => function ($query) {
                $query->whereNotNull('autodetect');
        }])
            ->select(DB::raw('count(*) as program_count, program_version_id'))
            ->where('channel_id', $channel)
            ->where('air_date', '>', Carbon::now()->subWeeks(4)->format('Y-m-d H:i:s'))
            ->whereNotNull('program_version_id')
            ->groupBy('program_version_id')
            ->orderBy('program_count', 'desc')
            ->limit(10)
            ->get();

        $autoDetect = [];
        foreach ($shows as $show) {
            if ($show->programVersion) {
                $autoDetect[] = $show->programVersion->autodetect;
            }
        }

        return $autoDetect;
    }

    public function updateAutoDetect($type)
    {
        $id = Request::get('id');
        $filename = Request::get('autodetect');

        echo "ID: ". $id, "\n";
        echo "Filename: ".$filename, "\n";

        $exists = Clipster::where('filename', $filename)->first();
        $clipster = new Clipster(['filename' => $filename]);

        if ($type == 'show') {
            $show = ProgramVersion::find($id);
            if (! $show) {
                return 'Delete me... '.$filename;
            }
            if (! $exists and $show) {
                $show->clipsters()->save($clipster);
            }

            return $show;
        }

        if ($type == 'spot') {
            $spot = SpotVersion::find($id);
            if (!$spot) {
                return 'Delete me... '.$filename;
            }
            if (! $exists) {
                $spot->clipsters()->save($clipster);
            }

            return $spot;
        }
    }

    public function updateFilename($type)
    {
        $id = Request::get('id');
        $filename = Request::get('file_name');

        echo "ID: ". $id, "\n";
        echo "Filename: ".$filename, "\n";

        if ($type == 'long') {
            $show = ProgramVersion::find($id);
            $show->file_name = $filename;
            $show->mp4 = 1;
            $show->flv = 0;
            $show->save();

            return $show;
        }

        if ($type == 'short') {
            $spot = SpotVersion::find($id);
            $spot->file_name = $filename;
            $spot->mp4 = 1;
            $spot->flv = 0;
            $spot->save();

            return $spot;
        }
    }

    public function getMp4Shows()
    {
        $shows = ProgramVersion::select('id', 'grid_title', 'file_name')
            ->where('mp4', 1)
            ->whereNotNull('file_name')
            ->whereNull('autodetect')
            ->where('language_id', '!=', 4)
            ->orderBy('title')
            ->get();

        return $shows;
    }

    public function getMp4Spots()
    {
        $spots = SpotVersion::select('id', 'title', 'length', 'file_name')
            ->where('mp4', 1)
            ->whereNotNull('file_name')
            ->whereNull('autodetect')
            ->where('language_id', '!=', 4)
            ->orderBy('title')
            ->get();

        return $spots;
    }

    public function getSpots()
    {
        if (Request::filled('channel')) {
            $channel = $this->getChannelId(Request::get('channel'));
        } else {
            return ['status' => 404, 'error' => 'You must supply a channel.'];
        }

        $spots = SpotAirtime::with(['spot' => function ($query) {
                $query->whereNotNull('autodetect');
        }])
            ->select(DB::raw('count(*) as spot_count'))
            ->where('channel_id', $channel)
            ->where('air_date', '>', Carbon::now()->subWeeks(4)->format('Y-m-d'))
            ->whereNotNull('spot_id')
            ->groupBy('spot_id')
            ->orderBy('spot_count', 'desc')
            ->get();

        $autoDetect = [];
        foreach ($spots as $spot) {
            if ($spot->spot) {
                $autoDetect[] = $show->spot->autodetect;
            }
        }

        return $autoDetect;
    }

    public function getCheck()
    {
        if (Request::filled('channel')) {
            $channel_id = $this->getChannelId(Request::get('channel'));
        } else {
            return ['status' => 404, 'error' => 'You must supply a channel.'];
        }

        $channel = Channel::findOrFail($channel_id);

        if ($channel->autodetect == 1) {
            return ['status' => 200, 'error' => false, 'autodetect' => true];
        } else {
            return ['status' => 200, 'error' => false, 'autodetect' => false];
        }
    }

    public function postLog()
    {
        if (Request::filled('secret') and Request::get('secret') == 'ims-autodetection-boss') {
            $log = new Log;
            $log->network = Request::get('network');
            $log->server = Request::get('server');
            $log->path = Request::get('path');
            $log->log_url = Request::get('log_url');
            $log->detected = Request::get('detected');
            $log->count = Request::get('count');
            $log->total_count = Request::get('total_count');
            $log->duration = Request::get('duration');
            $log->threshold = Request::get('threshold');
            $log->segment_time = Request::get('segment_time');
            $log->start_time = Request::get('start_time');
            $log->end_time = Request::get('end_time');
            $log->save();

            if ($log) {
                return ['status' => 200, 'error' => false, 'message' => "Autodetection Logged!"];
            }
        }
    }

    public function postException()
    {
        if (Request::filled('secret') and Request::get('secret') == 'ims-autodetection-boss') {
            $exception = new Exception;
            $exception->process = Request::get('process');
            $exception->process_server = Request::get('process_server');
            $exception->tube = Request::get('tube');
            $exception->job_id = Request::get('job_id');
            $exception->job_data = Request::get('job_data');
            $exception->server = Request::get('server');
            $exception->path = Request::get('path');
            $exception->ip_address = Request::get('ip_address');
            $exception->return_code = Request::get('return_code');
            $exception->output = Request::get('output');
            $exception->extra = Request::get('extra');
            $exception->date_entered = Request::get('date_entered');
            $exception->save();

            if ($exception) {
                return ['status' => 200, 'error' => false, 'message' => "Exception Logged!"];
            }
        }
    }

    public function postShow()
    {
        if (Request::filled('secret') and Request::get('secret') == 'ims-autodetection-boss') {
            if (! Request::filled('program_id') or ! Request::filled('air_date') or ! Request::filled('channel')) {
                abort(403, 'Unauthorized action.');
            }

            $channel_id = $this->getChannelId(Request::get('channel'));
            $program_id = Request::get('program_id');
            $air_date = Request::get('air_date');
            $url = Request::get('url');

            $exists = Airtime::where('channel_id', $channel_id)
                ->where('air_date', $air_date)
                ->first();

            if ($exists) {
                return ['status' => 409, 'error' => true, 'message' => "Detection already Logged!"];
            }

            $programVersion = ProgramVersion::find($program_id);
            $channel = Channel::find($channel_id);

            if (! $programVersion or ! $channel) {
                return ['status' => 404, 'error' => true, 'message' => "Cannot find channel or show!"];
            }

            if ($channel->language_id != $programVersion->language_id) {
                //Look up program with proper language
                $updatedProgramVersion = ProgramVersion::where('language_id', $channel->language_id)
                    ->where('title', $programVersion->title)
                    ->orderBy('version', 'desc')
                    ->first();

                if (! $updatedProgramVersion) {
                    $message = "<strong><a href='https://my.imsreport.com/programs/show/".$programVersion->id."'>".$programVersion->title."</a></strong> airtime could not be added at ".$air_date." on ".$channel->abbr." the language <strong>".$programVersion->language->name."</strong> does not match the market of the channel detected of <strong>".$channel->language->name."</strong> URL: <a href='http://".$url."'>".$url."</a>";
                    // \HipchatNotifier::message($message);

                    return ['status' => 404, 'error' => true, 'message' => "Cannot find proper language!"];
                }

                $program_id = $updatedProgramVersion->id;
            }

            $airtime = new Airtime;
            $airtime->channel_id = $channel_id;
            $airtime->program_version_id = $program_id;
            $airtime->air_date = $air_date;
            $airtime->cost = Cost::getCostByChannelAndTime($channel_id, $air_date);
            $airtime->type = 'P'; //Paid Programming
            $airtime->url = $url;
            $airtime->save();

            return ['status' => 200, 'error' => false, 'message' => "Detection Logged!"];
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    public function postSpot()
    {
        if (Request::filled('secret') and Request::get('secret') == 'ims-autodetection-boss') {
            if (! Request::filled('spot_id') or ! Request::filled('air_date') or ! Request::filled('channel')) {
                abort(403, 'Unauthorized action.');
            }

            $channel_id = $this->getChannelId(Request::get('channel'));
            $spot_id = Request::get('spot_id');
            $air_date = Carbon::createFromFormat("Y-m-d H:i:s", Request::get('air_date'));
            $url = Request::get('url');

            $spotVersion = SpotVersion::find($spot_id);
            $channel = Channel::find($channel_id);

            if (! $spotVersion or ! $channel) {
                return ['status' => 404, 'error' => true, 'message' => "Cannot find channel or spot!"];
            }

            if ($channel->language_id != $spotVersion->language_id) {
                //Look up program with proper language
                $updatedSpotVersion = SpotVersion::where('language_id', $channel->language_id)
                    ->where('title', $spotVersion->title)
                    ->orderBy('version', 'desc')
                    ->first();

                if (! $updatedSpotVersion) {
                    $message = "<strong><a href='https://my.imsreport.com/spots/show/".$spotVersion->id."'>".$spotVersion->title."</a></strong> airtime could not be added at ".$air_date." on ".$channel->abbr." the language <strong>".$spotVersion->language->name."</strong> does not match the market of the channel detected of <strong>".$channel->language->name."</strong> URL: <a href='http://".$url."'>".$url."</a>";
                    // \HipchatNotifier::message($message);

                    return ['status' => 404, 'error' => true, 'message' => "Cannot find proper language!"];
                }

                $spot_id = $updatedSpotVersion->id;
                $spotVersion = SpotVersion::find($spot_id);
            }

            $spotLength = $spotVersion->length;
            $startBetween = $air_date->subSeconds(ceil($spotLength/2))->format('Y-m-d H:i:s');
            $endBetween = $air_date->addSeconds($spotLength)->format('Y-m-d H:i:s');

            $daypart_id = $this->getDayPart($air_date);

            $exists = SpotAirtime::where('channel_id', $channel_id)
                ->whereBetween('air_date', [$startBetween, $endBetween])
                ->where('spot_version_id', $spot_id)
                ->first();

            if ($exists) {
                return ['status' => 409, 'error' => true, 'message' => "Detection already Logged!"];
            }

            $spotairtime = new SpotAirtime;
            $spotairtime->channel_id = $channel_id;
            $spotairtime->spot_version_id = $spot_id;
            $spotairtime->air_date = $air_date;
            $spotairtime->daypart_id = $daypart_id;
            $spotairtime->cost = SpotCost::getCostByChannelAndTime($channel_id, $air_date, $spotVersion->length);
            $spotairtime->url = $url;
            $spotairtime->save();

            return ['status' => 200, 'error' => false, 'message' => "Detection Logged!"];
        } else {
            abort(403, 'Unauthorized action.');
        }
    }

    private function getDayPart($air_date)
    {
        $daypart = Daypart::where(function ($query) use ($air_date) {
                $query->where('day_start', '<=', date('N', strtotime($air_date->format("Y-m-d H:i:s"))))
                    ->where('day_end', '>=', date('N', strtotime($air_date->format("Y-m-d H:i:s"))));
        })
            ->where(function ($query) use ($air_date) {
                $query->where('time_start', '<=', $air_date->format("H:i:s"))
                    ->where('time_end', '>', $air_date->format("H:i:s"));
            })
            ->first();

        if ($daypart) {
            return $daypart->id;
        } else {
            //The logic above is fucked up because Late Fringe Wraps around... 23:35 to 02:00
            return 9;
        }
    }

    private function getChannelId($channel)
    {
        if (is_numeric($channel)) {
            $c = Channel::findOrFail($channel);
        } else {
            $c = Channel::where('abbr', '=', $channel)->first();
        }

        if ($c) {
            return $c->id;
        } else {
            abort(404, 'I can\'t find channel '.$channel);
        }
    }
}
