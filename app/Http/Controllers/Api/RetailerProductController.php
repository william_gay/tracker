<?php namespace App\Http\Controllers\Api;

use App\Models\Retail\RetailerProduct;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class RetailerProductController extends Controller
{
    public function getProducts()
    {
        if (Request::filled('q')) {
                $products = RetailerProduct::select(
                    'retailer_products.id',
                    DB::raw('CONCAT(products.name, " (", retailers.name,")") as title')
                )
                    ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
                    ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
                    ->where('products.name', 'like', '%'.Request::get('q').'%')
                    ->orderBy('products.name', 'asc')
                    ->take(Request::get('page_limit'))
                    ->get();
        } else {
            $products = RetailerProduct::select(
                'retailer_products.id',
                DB::raw('CONCAT(products.name, " (", retailers.name,")") as title')
            )
                ->leftJoin('retailers', 'retailers.id', '=', 'retailer_products.retailer_id')
                ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
                ->orderBy('products.name', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'total' => $products->count(),
            'products' => $products->toArray()
        ];

        if (Request::filled('callback')) {
            return Request::get('callback').'('
                .json_encode($return_array).
            ')';
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getProduct()
    {
        if (Request::filled('retailer_product_id') and Request::get('retailer_product_id') != 0) {
            $product = RetailerProduct::select(
                'id',
                'products.name as title'
            )
                ->leftJoin('products', 'products.id', '=', 'retailer_products.product_id')
                ->find(Request::get('retailer_product_id'));

            $return_array = [
                'error' => false,
                'product' => $product->toArray()
            ];

            if (Request::filled('callback')) {
                return Request::get('callback').'('
                    .json_encode($product->toArray()).
                ')';
            } else {
                return response()->json(
                    $return_array,
                    200
                );
            }
        } else {
            $return_array = [
                'error' => false,
                'product' => []
            ];

            return response()->json(
                $return_array,
                200
            );
        }
    }
}
