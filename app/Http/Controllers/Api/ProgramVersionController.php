<?php namespace App\Http\Controllers\Api;

use App\Models\ProgramVersion;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class ProgramVersionController extends Controller
{
    public function getPrograms()
    {
        if (Request::filled('q') and Request::filled('language_id')) {
            $programs = ProgramVersion::select([
                    'id',
                    DB::raw('CONCAT(grid_title, " - ", DATE_FORMAT(initial_date,"%m/%d/%y")) as title')
                ])
                ->where('language_id', Request::get('language_id'))
                ->where(function ($query) {
                    $query->where('title', 'like', '%'.Request::get('q').'%')
                        ->orWhere('grid_title', 'like', '%'.Request::get('q').'%');
                })
                ->orderBy('initial_date', 'desc')
                ->take(Request::get('page_limit'))
                ->get();
        } else if (Request::filled('q')) {
            $programs = ProgramVersion::select([
                    'program_versions.id',
                    DB::raw('CONCAT(grid_title, " - ", DATE_FORMAT(initial_date,"%m/%d/%y"), " -- Market: ",languages.name) as title')
                ])
                ->leftJoin('languages', 'languages.id', '=', 'program_versions.language_id')
                ->where(function ($query) {
                    $query->where('title', 'like', '%'.Request::get('q').'%')
                        ->orWhere('grid_title', 'like', '%'.Request::get('q').'%');
                })
                ->orderBy('initial_date', 'desc')
                ->take(Request::get('page_limit'))
                ->get();
        } else {
            $programs = ProgramVersion::select([
                    'id',
                    DB::raw('CONCAT(grid_title, " - ", DATE_FORMAT(initial_date,"%m/%d/%y")) as title')
                ])
                ->where('language_id', Request::get('language_id'))
                ->where(function ($query) {
                    $query->where('title', 'like', '%'.Request::get('q').'%')
                        ->orWhere('grid_title', 'like', '%'.Request::get('q').'%');
                })
                ->orderBy('initial_date', 'desc')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'total' => $programs->count(),
            'programs' => $programs->toArray()
        ];

        if (Request::filled('callback')) {
            return response()->json($return_array)->setCallback(Request::get('callback'));
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getProgram()
    {
        if (Request::filled('program_version_id') and Request::get('program_version_id') != 0) {
            $program = ProgramVersion::select([
                    'id',
                    DB::raw('CONCAT(grid_title, " - ", DATE_FORMAT(initial_date,"%m/%d/%y")) as title')
                ])
                ->find(Request::get('program_version_id'));

            $return_array = [
                'error' => false,
                'program' => $program->toArray()
            ];

            if (Request::filled('callback')) {
                return response()->json($program)->setCallback(Request::get('callback'));
            } else {
                return response()->json(
                    $return_array,
                    200
                );
            }
        } else {
            $return_array = [
                'error' => false,
                'program' => []
            ];

            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getProgramFind($programVersionId)
    {
        return ProgramVersion::findOrFail($programVersionId);
    }
}
