<?php namespace App\Http\Controllers\Api;

use App\Models\Retail\Retailer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class RetailerController extends Controller
{
    public function getRetailers()
    {
        if (Request::filled('q')) {
                $retailers = Retailer::select(
                    'id',
                    'name as title'
                )
                    ->where('name', 'like', '%'.Request::get('q').'%')
                    ->orderBy('name', 'asc')
                    ->take(Request::get('page_limit'))
                    ->get();
        } else {
            $retailers = Retailer::select(
                'id',
                'name as title'
            )
                ->orderBy('name', 'asc')
                ->take(Request::get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'total' => $retailers->count(),
            'retailers' => $retailers->toArray()
        ];

        if (Request::filled('callback')) {
            return Request::get('callback').'('
                .json_encode($return_array).
            ')';
        } else {
            return response()->json(
                $return_array,
                200
            );
        }
    }

    public function getRetailer()
    {
        if (Request::filled('retailer_id') and Request::get('retailer_id') != 0) {
            $retailer = Retailer::select(
                'id',
                'name as title'
            )
                ->find(Request::get('retailer_id'));

            $return_array = [
                'error' => false,
                'retailer' => $retailer->toArray()
            ];

            if (Request::filled('callback')) {
                return Request::get('callback').'('
                    .json_encode($retailer->toArray()).
                ')';
            } else {
                return response()->json(
                    $return_array,
                    200
                );
            }
        } else {
            $return_array = [
                'error' => false,
                'retailer' => []
            ];

            return response()->json(
                $return_array,
                200
            );
        }
    }
}
