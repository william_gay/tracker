<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App;
use Auth;
use Config;
use Event;
use Lang;
use Mail;
use Sentry;
use Response;
use View;
use App\Services\Validators\ResetValidator;
use App\Services\Validators\UserValidator;
use App\Services\Validators\UserSettingValidator;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Users\UserExistsException;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\PasswordRequiredException;
use Cartalyst\Sentry\Users\WrongPasswordException;
use Cartalyst\Sentry\Users\UserNotActivatedException;
use Cartalyst\Sentry\Throttling\UserSuspendedException;
use Cartalyst\Sentry\Throttling\UserBannedException;
use App\Models\Category;
use App\Models\DashboardType;
use App\Models\Language;
use App\Models\Product;
use App\Models\User;
use App\Models\UserProfile;
use App\Events\GroupCreated;
use App\Events\UserLogin;
use App\Events\UserLogout;
use App\Events\UserRegister;
use App\Events\UserUpdate;
use App\Mail\Welcome;
use App\Mail\WelcomeWinmo;
use App\Mail\ResetPassword;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Show the dashboard
     *
     * @author Steve Williamson
     * @link   http://www.imsreport.com/
     *
     * @return Response
     */
    public function index()
    {
        return view('cxp.dashboard.index');
    }

    /**
     * Show the login form
     *
     * @author Steve Williamson
     * @link   http://www.imsreport.com/
     *
     * @return Response
     */
    public function getLogin()
    {
        if (Sentry::check()) {
            return redirect()->intended('dashboard');
        }

        $login_attribute = config('cartalyst/sentry::users.login_attribute');

        return view('cxp.user.login', compact('login_attribute'));
    }

    /**
     * Display the reset form
     *
     * @author Steve Williamson
     * @link   http://www.imsreport.com/
     *
     * @return Response
     */
    public function getReset()
    {
        $login_attribute = config('cartalyst/sentry::users.login_attribute');

        return view('cxp.user.reset', compact('login_attribute'));
    }

    public function postReset(Request $request)
    {
        try {
            $validation = new ResetValidator;

            if ($validation->passes()) {
                // Find the user using the user email address
                $user = Sentry::getUserProvider()->findByLogin($request->get('email'));

                // Get the password reset code
                $resetCode = $user->getResetPasswordCode();

                $data = [
                    'resetCode' => $resetCode,
                    'user' => $user
                ];

                Mail::to($user->email, $user->first_name.' '.$user->last_name)
                    ->queue(new ResetPassword($data));

                // Password reset passed
                return redirect()->route('user.reset')->with('success', __('users.password_reset_success'));
            }

            return redirect()->back()->withInput()->withErrors($validation->getErrors());
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return redirect()->back()->withInput()->withErrors(__('users.not_found'));
        }
    }

    public function getResetFinish(Request $request)
    {
        if ($request->filled('userEmail') and $request->filled('resetCode')) {
            $userEmail = $request->get('userEmail');
            $resetCode = $request->get('resetCode');

            try {
                $user = Sentry::getUserProvider()->findByLogin($userEmail);
            } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
                return redirect()->route('user.login')->with('login_error', $e->getMessage());
            }

            $login_attribute = config('cartalyst/sentry::users.login_attribute');

            return view('cxp.user.reset_finish', compact('login_attribute', 'user', 'resetCode'));
        }

        return redirect()->route('user.login');
    }

    public function postResetComplete(Request $request)
    {
        try {
            // Find the user using the user id
            $user = Sentry::getUserProvider()->findByLogin($request->get('login_attribute'));

            // Check if the reset password code is valid
            if ($user->checkResetPasswordCode($request->get('reset_code'))) {
                // Attempt to reset the user password
                if ($user->attemptResetPassword($request->get('reset_code'), $request->get('password'))) {
                    // Password reset passed
                    return redirect()->route('user.login')->with('success', __('users.password_reset'));
                } else {
                    // Password reset failed
                    return redirect()->back()->withInput()->withErrors(__('users.password_reset_failed'));
                }
            } else {
                // The provided password reset code is Invalid
                return redirect()->back()->withInput()->withErrors(__('users.password_reset_code_invalid'));
            }
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return redirect()->back()->withInput()->withErrors(__('users.not_found'));
        }
    }

    /**
     * Logs out the current user
     *
     * @author Steve Williamson
     * @link   http://www.imsreport.com/
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        if (Sentry::check()) {
            $user = Sentry::getUser();
            Sentry::logout();
            event(new UserLogout($user));

            $userLaravel = \App\Models\UserLaravel::find($user->id);
            if ($userLaravel) {
                Auth::logout($userLaravel);
            }

            return redirect()->route('user.login')->with('success', __('users.logout'));
        }

        return redirect()->route('user.login');
    }

    /**
     * Authenticate the user
     *
     * @author Steve Williamson
     * @link   http://www.imsreport.com/
     *
     *
     * @return Response
     */
    public function postLogin(Request $request)
    {
        try {
            $remember = $request->get('remember_me', false);
            $userdata = [
                'email' => $request->get('email'),
                'password' => $request->get('password')
            ];

            $user = Sentry::authenticate($userdata, $remember);
            event(new UserLogin($user, $request, 'user'));

            $userLaravel = \App\Models\UserLaravel::find($user->id);
            if ($userLaravel) {
                Auth::login($userLaravel);
            }

            return redirect()->intended('dashboard')->with('success', __('users.login_success'));
        } catch (LoginRequiredException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (PasswordRequiredException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (WrongPasswordException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (UserNotActivatedException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (UserNotFoundException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (UserSuspendedException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (UserBannedException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        }
    }

    /**
     * Display the registration form
     *
     * @author Steve Williamson
     * @link   http://www.imsreport.com/
     *
     * @return Response
     */
    public function getRegister()
    {
        if (Sentry::check()) {
            return redirect('/dashboard');
        }

        $login_attribute = config('cartalyst/sentry::users.login_attribute');

        return view('cxp.user.register', compact('login_attribute'));
    }

    public function getRegisterWinmoCTA()
    {
        if (Sentry::check()) {
            return redirect('/dashboard');
        }

        $login_attribute = config('cartalyst/sentry::users.login_attribute');

        return view('cxp.user.register-winmo', compact('login_attribute'));
    }

    /**
     * Register user
     *
     * @author Steve Williamson
     * @link   http://www.imsreport.com/
     *
     * @return Response
     */
    public function postRegister(Request $request)
    {
        try {
            $validation = new UserValidator;

            if ($validation->passes()) {
                $user = Sentry::register($request->except('password_confirmation', 'company', 'find_us', 'submit', 'product_id', 'phone', 'winmo'));

                $activationCode = $user->getActivationCode();
                //Send out the email to activate their account
                $data = [
                    'user' => $user,
                    'activationCode' => $activationCode
                ];
                if ($request->has('winmo')) {
                    Mail::to($user->email, $user->first_name)
                        ->queue(new WelcomeWinmo($data));
                } else {
                    Mail::to($user->email, $user->first_name)
                        ->queue(new Welcome($data));
                }
                event(new UserRegister($user, $request->only('company', 'find_us', 'position', 'product_id', 'phone')));

                return redirect()->route('user.login')->with('success', __('users.register_success'));
            }

            return redirect()->back()->withInput()->withErrors($validation->getErrors());
        } catch (LoginRequiredException $e) {
            return redirect()->back()->withInput()->with('error', $e->getMessage());
        } catch (PasswordRequiredException $e) {
            return redirect()->back()->withInput()->with('error', $e->getMessage());
        } catch (UserExistsException $e) {
            return redirect()->back()->withInput()->with('error', $e->getMessage());
        }
    }

    /**
     * Register user through Ajax
     *
     * @return Response
     */
    public function postRegisterAjax(Request $request)
    {
        try {
            $validation = new UserValidator;

            if ($validation->passes()) {
                $user = Sentry::register($request->except('password_confirmation', 'company', 'find_us', 'submit'));

                $activationCode = $user->getActivationCode();
                //Send out the email to activate their account
                $data = [
                    'user' => $user,
                    'activationCode' => $activationCode
                ];

                Mail::to($user->email, $user->first_name)
                    ->queue(new Welcome($data));

                event(new UserRegister($user, $request->only('company', 'find_us'), 'LST New User'));

                try {
                    $companyGroup = Sentry::getGroupProvider()->create(['name' => $request->only('company')['company']]);
                } catch (\Cartalyst\Sentry\Groups\GroupExistsException $e) {
                    $companyGroup = Sentry::getGroupProvider()->create(['name' => $request->only('company')['company'].Str::random(10)]);
                }
                $user->addGroup($companyGroup);

                event(new GroupCreated($companyGroup, \Carbon\Carbon::now()->addYear()->toDateTimeString()));

                return response()->json([
                    'status'  => 200,
                    'user-id' => $user->id,
                    'company-group' => $companyGroup->name,
                    'message' => __('users.register_success')
                ]);
            }

            return response()->json([
                'status'  => 400,
                'message' => $validation->getErrors()
            ]);
        } catch (LoginRequiredException $e) {
            return response()->json([
                'status'  => 400,
                'message' => [$e->getMessage()]
            ]);
        } catch (PasswordRequiredException $e) {
            return response()->json([
                'status'  => 400,
                'message' => [$e->getMessage()]
            ]);
        } catch (UserExistsException $e) {
            return response()->json([
                'status'  => 400,
                'message' => [$e->getMessage()]
            ]);
        }
    }

    /**
     * Display the registration form for the Live Shopping Tracker
     *
     * @return Response
     */
    public function getRegisterLst()
    {
        if (Sentry::check()) {
            return redirect('/dashboard');
        }

        $login_attribute = config('cartalyst/sentry::users.login_attribute');

        return view('cxp.user.register_lst', compact('login_attribute'));
    }

    /**
    * Activate a new user
    * @param int $id
    * @param string $code
    * @return Response
    */
    public function activate($id, $code)
    {
        if (!is_numeric($id)) {
            return abort(404);
        }

        try {
            // Find the user using the user id
            $user = Sentry::findUserById($id);

            // Attempt to activate the user
            if ($user->attemptActivation($code)) {
                //Add user to the Top 25 group
                $group = Sentry::findGroupByName('Top 25 Subscription');
                $user->addGroup($group);
                // User activation passed
                return redirect()->route('user.login')->with('success', __('users.activation_success'));
            } else {
                // User activation failed
                return redirect()->route('user.login')->with('error', __('users.activation_fail'));
            }
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return redirect()->route('user.login')->with('error', __('users.activation_fail'));
        } catch (\Cartalyst\Sentry\Users\UserAlreadyActivatedException $e) {
            return redirect()->route('user.login')->with('error', 'User is already activated.');
        }
    }

    public function getSettings()
    {
        $current = User::find(Sentry::getUser()->id);

        $user = [
            'id' => $current->id,
            'first_name' => $current->first_name,
            'last_name' => $current->last_name,
            'categories' => $current->favoriteCategories->pluck('id')->implode(','),
            'products' => $current->favoriteProducts->pluck('id')->implode(','),
            'dashboardtype_id' => ($current->profile) ? $current->profile->dashboardtype_id : 1,
            'language_id' => ($current->profile) ? $current->profile->language_id : 1,
            'advert_type' => ($current->profile) ? $current->profile->advert_type : 'both'
        ];

        $dashboards = DashboardType::orderBy('name')->pluck('name', 'id')->toArray();
        $languages = Language::orderBy('name')->pluck('name', 'id')->toArray();
        $advertisings = ['both'=>'Short and Long Form', 'short' => 'Short Form', 'long' => 'Long Form'];

        return view(
            'cxp.user.settings',
            compact(
                'user',
                'dashboards',
                'languages',
                'advertisings'
            )
        );
    }

    public function postSettings(Request $request)
    {
        try {
            $settings = $request->except('product_ids', 'category_ids', 'dashboardtype_id', 'language_id', 'advert_type');
            $settings['id'] = Sentry::getUser()->id;

            $validation = new UserSettingValidator($settings);

            if ($validation->passes()) {
                $user = User::find(Sentry::getUser()->id);
                $user->fill($validation->getData());
                $user->save();

                $profile = UserProfile::firstOrCreate(['user_id' => Sentry::getUser()->id]);
                $profile->dashboardtype_id = $request->get('dashboardtype_id');
                //Removed this since we probably want to make market, a multi-select
                //$profile->language_id = $request->get('language_id');
                //$profile->advert_type = $request->get('advert_type');
                $profile->save();

                $this->processProducts($user, $request->get('product_ids'));
                $this->processCategories($user, $request->get('category_ids'));

                event(new UserUpdate($user));

                return redirect()->route('user.settings')->with('success', __('users.update_success'));
            }

            return redirect()->back()->withInput()->withErrors($validation->getErrors());
        } catch (UserExistsException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        } catch (UserNotFoundException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        } catch (LoginRequiredException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    private function processProducts($user, $product_ids)
    {
        $ids = explode(',', $product_ids);
        $user->favoriteProducts()->sync($ids);
    }

    private function processCategories($user, $category_ids)
    {
        $ids = explode(',', $category_ids);
        $user->favoriteCategories()->sync($ids);
    }
}
