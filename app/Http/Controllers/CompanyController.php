<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Jobs\LogSiteAnalytics;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Sentry;
use Response;

class CompanyController extends Controller
{

    public function getIndex(Builder $builder)
    {
        $user = Sentry::getUser();

        $site_analytics = [
            'type' => 'Company Search',
            'user_id' => $user->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        if (! $user->hasAccess('reports.companies')) {
            return abort('401', 'Access denied - You do not have access to this page.');
        }

        if (request()->ajax()) {
            $companies = Company::query();

            return Datatables::of($companies)
                ->editColumn('name', '<a href="{{ route("companies.show", array($id)) }}">{{ $name }}</a>  @if(Sentry::getUser()->isSuperUser()) <small><a href="{{ route("admin.companies.edit",array($id)) }}" target="_blank">Edit</a></small> @endif')
                ->rawColumns(['name'])
                ->toJson();
        }

        $html = $builder->columns([
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'address', 'name' => 'address', 'title' => 'Address'],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() {}',
            'dom'          => 'Bfltip',
            'buttons'      => ['copy', 'excel', 'pdf', 'csv'],
        ]);

        return view('cxp.companies.all')
            ->with('html', $html);
    }

    public function getShow($companyId)
    {
        try {
            $company = Company::with([
                    'marketingProgramVersions',
                    'productionProgramVersions',
                    'spotVersions'
                ])->findOrFail($companyId);
        } catch (ModelNotFoundException $e) {
            abort('404', 'This company does not exist.');
        }

        $user = Sentry::getUser();

        $site_analytics = [
            'type' => 'Company '.$company->name,
            'user_id' => $user->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        if (request()->filled('start-date') and request()->filled('end-date')) {
            $start_date = Carbon::createFromFormat("F j, Y", request()->get('start-date'))->startOfDay();
            $end_date = Carbon::createFromFormat("F j, Y", request()->get('end-date'))->endOfDay();
        } else {
            $start_date = lastFriday()->subWeek()->addDay()->startOfDay();
            $end_date = lastFriday()->endOfDay();
        }

        $productionProgramVersions = $company->productionProgramVersions;
        $marketingProgramVersions = $company->marketingProgramVersions;
        $marketingSpotVersions = $company->spotVersions;

        return view(
            'cxp.companies.show',
            compact(
                'company',
                'start_date',
                'end_date',
                'productionProgramVersions',
                'marketingProgramVersions',
                'marketingSpotVersions'
            )
        );
    }

    // AJAX Lookup
    public function getCompanies()
    {
        if (request()->filled('q')) {
            $companies = Company::select('id', 'name')->where('name', 'LIKE', '%'.request()->get('q').'%')
                ->take(request()->get('page_limit'))
                ->get();
        } else {
            $companies = Company::select('id', 'name')
                ->take(request()->get('page_limit'))
                ->get();
        }

        $return_array = [
            'error' => false,
            'companies' => $companies->toArray()
        ];

        if (request()->filled('callback')) {
            return request()->get('callback').'('
                .json_encode($return_array).
            ')';
        } else {
            return Response::json(
                $return_array,
                200
            );
        }
    }

    // AJAX Lookup
    public function getCompany()
    {
        if (request()->filled('company_id') and request()->get('company_id') != 0) {
            $company = Company::select('id', 'name')->find(request()->get('company_id'));

            $return_array = [
                'error' => false,
                'company' => $company->toArray()
            ];

            if (request()->filled('callback')) {
                return request()->get('callback').'('
                    .json_encode($company->toArray()).
                ')';
            } else {
                return Response::json(
                    $return_array,
                    200
                );
            }
        } else {
            $return_array = [
                'error' => false,
                'company' => []
            ];

            return Response::json(
                $return_array,
                200
            );
        }
    }
}
