<?php

namespace App\Http\Controllers;

use App;
use Sentry;
use View;
use Redirect;
use Request;
use Mail;
use Validator;
use App\Mail\ContactSales;
use App\Mail\UpdateDescription;

class ContactController extends Controller
{

    //Contact Form
    public function postContact()
    {
        //dd(Request::all());
        //Get all the data and store it inside Store Variable
        $data = Request::all();
        $data['user'] = Sentry::getUser();
        $data['email'] = Sentry::getUser()->email;
        $data['name'] = Sentry::getUser()->first_name;
        $data['ip'] = Request::getClientIp();
        $data['date_time'] = date("F j, Y, g:i a");
        $data['message_body'] = Request::input('message');

        //Send email using Laravel send function
        Mail::to('support@imsreport.com', 'Support')
            ->queue(new ContactSales($data));

        if (! request()->ajax()) {
            return redirect('/dashboard')
                ->with('success', 'Thank you for contacting us. Our team will contact you shortly.');
        }
    }

    public function updatePageDescription()
    {
        $data = Request::all();
        $data['email'] = Sentry::getUser()->email;
        $data['name'] = Sentry::getUser()->first_name;
        $data['user'] = Sentry::getUser();
        $data['userIpAddress'] = request()->getClientIp();
        $data['url'] = url()->previous();

        //Send email using Laravel send function
        Mail::to('support@imsreport.com', 'Support')
            ->queue(new UpdateDescription($data));

        return redirect()
            ->back()
            ->with('success', 'Thank you for contacting us. Our team will contact you shortly.');
    }
}
