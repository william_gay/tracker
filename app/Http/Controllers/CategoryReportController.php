<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use App\Models\Airtime;
use App\Models\SpotAirtime;
use App\Models\CategoryReport;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\Spot;
use App\Models\SpotVersion;
use App\Models\SpotDetection;
use App\Jobs\LogSiteAnalytics;
use Carbon\Carbon;
use App;
use Illuminate\Support\Facades\DB;
use DataTables;
use Queue;
use Response;
use Request;
use Redirect;
use Sentry;
use View;

class CategoryReportController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {
        $category_reports = CategoryReport::with(['spots', 'programs'])
            ->orderBy('name')->get();

        return view('cxp.category-reports.index')
            ->with('category_reports', $category_reports);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getShow($reportID)
    {
        $user = Sentry::getUser();

        try {
            $report = CategoryReport::with(['spots', 'programs'])->findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        if (Request::filled('start-date') and Request::filled('end-date')) {
            $start_date = Carbon::createFromFormat("F j, Y", Request::get('start-date'))->startOfDay();
            $end_date = Carbon::createFromFormat("F j, Y", Request::get('end-date'))->endOfDay();

            if ($start_date->gte($end_date)) {
                return Redirect::route('category-report.program', $reportID)
                    ->with('error', 'Start date must be before end date.');
            }
        } else {
            $start_date = $report->start_date->startOfDay();
            $end_date = $report->end_date->endOfDay();
        }

        $this->allowedDates($report, $start_date, $end_date);

        $this->checkPermissions($report);

        $analData = ['type' => 'category-report', 'report_id' => $report->id, 'user_id' => $user->id];
        dispatch((new LogSiteAnalytics($analData))->onQueue('analytics'));

        $program_versions = $this->getProgramVersions($report);
        $spot_versions = $this->getSpotVersions($report);

        $parent_programs = ProgramVersion::whereIn('id', $program_versions)
            ->groupBy('program_id')
            ->pluck('program_id')
            ->toArray();

        $program_count = count($parent_programs);

        $parent_spots = SpotVersion::whereIn('id', $spot_versions)
            ->groupBy('spot_id')
            ->pluck('spot_id')
            ->toArray();

        $spot_count = count($parent_spots);

        if ($program_versions) {
            $program_airings = Airtime::whereIn('program_version_id', $program_versions)
                ->whereBetween('air_date', [$start_date->startOfDay(), $end_date->endOfDay()])
                ;

            $top_media_spend = Airtime::select([
                    'programs.title',
                    DB::raw('SUM(airtimes.cost) as cost'),
                ])
                ->whereIn('program_version_id', $program_versions)
                ->whereBetween('air_date', [$start_date->startOfDay(), $end_date->endOfDay()])
                ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
                ->leftJoin('programs', 'programs.id', '=', 'program_versions.program_id')
                ->groupBy('program_versions.program_id')
                ->orderBy(DB::raw('SUM(airtimes.cost)'), 'desc')
                ->take(5)
                ->get();

            $top_networks = Airtime::select([
                    'channels.name',
                    DB::raw('COUNT(airtimes.id) as airings'),
                    DB::raw('SUM(airtimes.cost) as cost'),
                ])
                ->whereIn('program_version_id', $program_versions)
                ->whereBetween('air_date', [$start_date->startOfDay(), $end_date->endOfDay()])
                ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
                ->groupBy('airtimes.channel_id')
                ->orderBy(DB::raw('SUM(airtimes.cost)'), 'desc')
                ->take(5)
                ->get();

            $top_frequency = Airtime::select([
                    'programs.title',
                    DB::raw('COUNT(airtimes.id) as airings'),
                ])
                ->whereIn('program_version_id', $program_versions)
                ->whereBetween('air_date', [$start_date->startOfDay(), $end_date->endOfDay()])
                ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
                ->leftJoin('programs', 'programs.id', '=', 'program_versions.program_id')
                ->groupBy('program_versions.program_id')
                ->orderBy('airings', 'desc')
                ->take(5)
                ->get();

            $over_time = $this->overTime($program_versions, $start_date, $end_date);
            $channelPie = $this->channelPie($program_versions, $start_date, $end_date);
        }

        if ($spot_versions) {
            $spot_airings = SpotAirtime::whereIn('spot_version_id', $spot_versions)
                ->whereBetween('air_date', [$start_date->startOfDay(), $end_date->endOfDay()]);
        }

        return view('cxp.category-reports.show', compact(
            'report',
            'spot_versions',
            'spot_count',
            'program_versions',
            'program_count',
            'spot_airings',
            'program_airings',
            'spot_airings',
            'top_media_spend',
            'top_networks',
            'top_frequency',
            'over_time',
            'channelPie',
            'start_date',
            'end_date'
        ));
    }

    private function allowedDates($report, $start_date, $end_date)
    {
        if ($start_date->lt($report->start_date->startOfDay()) or $end_date->gt($report->end_date->endOfDay())) {
            return abort('401', 'Access denied - Date outside of range');
        }
    }

    private function getSpotVersions($report)
    {
        $spot_versions = [];
        if ($report->spots()->count() > 0) {
            foreach ($report->spots()->pluck('spot_id')->toArray() as $spot) {
                $spot_versions[] = SpotVersion::where('spot_id', $spot)->pluck('id');
            }
        } else {
            $spot_versions[] = SpotVersion::whereBetween('air_date', [$report->start_date, $report->end_date])
                ->pluck('id')
                ->toArray();
        }

        $spot_versions = Arr::flatten($spot_versions);

        return $spot_versions;
    }

    private function getProgramVersions($report)
    {
        $program_versions = [];
        if ($report->programs()->count() > 0) {
            foreach ($report->programs()->pluck('program_id')->toArray() as $program) {
                $program_versions[] = ProgramVersion::where('program_id', $program)->pluck('id')->toArray();
            }
        } else {
            $program_versions[] = ProgramVersion::whereBetween('monitor_date', [$report->start_date, $report->end_date])
                ->pluck('id')
                ->toArray();
        }

        $program_versions = Arr::flatten($program_versions);

        return $program_versions;
    }

    private function channelPie($program_versions, $start_date, $end_date)
    {
        $channels = Airtime::select([
                'channels.abbr as title',
                DB::raw('count(airtimes.id) as chanfreq')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->whereIn('program_version_id', $program_versions)
            ->whereBetween('air_date', [$start_date->startOfDay(), $end_date->endOfDay()])
            ->groupBy('channel_id')
            ->orderBy('chanfreq', 'desc')
            ->take(10)
            ->get();

        return $channels;
    }

    private function overTime($program_versions, $start_date, $end_date)
    {
        $airtimes = Airtime::select([
                DB::raw('COUNT(airtimes.id) as airs'),
                DB::raw('SUM(airtimes.cost) as media'),
                DB::raw('airtimes.air_date as date')
            ])
            ->whereIn('program_version_id', $program_versions)
            ->whereBetween('air_date', [$start_date->startOfDay(), $end_date->endOfDay()])
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('programs', 'programs.id', '=', 'program_versions.program_id')
            ->groupBy(DB::raw('YEARWEEK(airtimes.air_date)'))
            ->orderBy('airtimes.air_date')
            ->get();

        return $airtimes;
    }

    private function overTimeProgram($programID, $start_date, $end_date)
    {
        $airtimes = Airtime::select([
                DB::raw('COUNT(airtimes.id) as airs'),
                DB::raw('SUM(airtimes.cost) as media'),
                DB::raw('airtimes.air_date as date')
            ])
            ->where('program_versions.program_id', $programID)
            ->whereBetween('air_date', [$start_date->startOfDay(), $end_date->endOfDay()])
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('programs', 'programs.id', '=', 'program_versions.program_id')
            ->groupBy(DB::raw('YEARWEEK(airtimes.air_date)'))
            ->orderBy('airtimes.air_date')
            ->get();

        return $airtimes;
    }

    private function overTimeSpot($spotID, $start_date, $end_date)
    {
        $airtimes = SpotDetection::select([
                DB::raw('SUM(spot_detections.detections) as airs'),
                DB::raw('spot_detections.rank_date date')
            ])
            ->where('spot_versions.spot_id', $spotID)
            ->whereBetween('rank_date', [$start_date->startOfDay(), $end_date->endOfDay()])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
            ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
            ->groupBy(DB::raw('YEARWEEK(spot_detections.rank_date)'))
            ->orderBy('spot_detections.rank_date')
            ->get();

        return $airtimes;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getPurchase($reportID)
    {
        //TODO Make some form that tells someone important to buy this fucking report
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        return view('cxp.category-reports.purchase')
            ->with('report', $report);
    }

    public function getProducts($reportID)
    {
        try {
            $report = CategoryReport::with(['spots', 'programs'])->findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        $this->checkPermissions($report);

        $program_versions = $this->getProgramVersions($report);
        $spot_versions = $this->getSpotVersions($report);

        $program_ids = ProgramVersion::whereIn('id', $program_versions)
            ->groupBy('program_id')
            ->pluck('program_id')
            ->toArray();

        $spot_ids = SpotVersion::whereIn('id', $spot_versions)
            ->groupBy('spot_id')
            ->pluck('spot_id')
            ->toArray();

        $programs = Program::select([
                'programs.id',
                'programs.title',
                DB::raw('COUNT(airtimes.id) as airs'),
                DB::raw('SUM(airtimes.cost) as media')
            ])
            ->whereIn('programs.id', $program_ids)
            ->whereBetween(
                'airtimes.air_date',
                [
                    $report->start_date->startOfDay(),
                    $report->end_date->endOfDay()
                ]
            )
            ->leftJoin('program_versions', 'programs.id', '=', 'program_versions.program_id')
            ->leftJoin('airtimes', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->groupBy('programs.id')
            ->orderBy('programs.title')
            ->get();

        $spots = Spot::select([
                'spots.id',
                'spots.title',
                DB::raw('SUM(spot_detections.detections) as airs')
            ])
            ->whereIn('spots.id', $spot_ids)
            ->whereBetween(
                'spot_detections.rank_date',
                [
                    $report->start_date->startOfDay(),
                    $report->end_date->endOfDay()
                ]
            )
            ->leftJoin('spot_versions', 'spots.id', '=', 'spot_versions.spot_id')
            ->leftJoin('spot_detections', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
            ->groupBy('spots.id')
            ->orderBy('spots.title')
            ->get();

        return view('cxp.category-reports.products')
            ->with('report', $report)
            ->with('spots', $spots)
            ->with('programs', $programs);
    }

    public function getProgramPivot($reportID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        if (Request::filled('start-date') and Request::filled('end-date')) {
            $start_date = Carbon::createFromFormat("F j, Y", Request::get('start-date'))->startOfDay();
            $end_date = Carbon::createFromFormat("F j, Y", Request::get('end-date'))->endOfDay();

            if ($start_date->gte($end_date)) {
                return Redirect::route('category-report.program', $reportID)
                    ->with('error', 'Start date must be before end date.');
            }
        } else {
            $start_date = $report->start_date->startOfDay();
            $end_date = $report->end_date->endOfDay();
        }

        $this->checkPermissions($report);

        $program_versions = $this->getProgramVersions($report);

        $program_airings = Airtime::whereIn('program_version_id', $program_versions)
            ->whereBetween('air_date', [$start_date->startOfDay(), $end_date->endOfDay()])
            ->get();

        return view('cxp.category-reports.programs-pivot')
            ->with('report', $report)
            ->with('airtimes', $program_airings);
    }

    public function getProgramDetail($reportID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        $this->checkPermissions($report);

        return view('cxp.category-reports.programs-details')
            ->with('report', $report);
    }

    public function getProgramDetailFull($reportID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        $this->checkPermissions($report);

        return view('cxp.category-reports.programs-details-full')
            ->with('report', $report);
    }

    public function getSpotPivot($reportID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        if (Request::filled('start-date') and Request::filled('end-date')) {
            $start_date = Carbon::createFromFormat("F j, Y", Request::get('start-date'))->startOfDay();
            $end_date = Carbon::createFromFormat("F j, Y", Request::get('end-date'))->endOfDay();

            if ($start_date->gte($end_date)) {
                return Redirect::route('category-report.program', $reportID)
                    ->with('error', 'Start date must be before end date.');
            }
        } else {
            $start_date = $report->start_date->startOfDay();
            $end_date = $report->end_date->endOfDay();
        }

        $this->checkPermissions($report);

        $spot_versions = $this->getSpotVersions($report);

        $spot_airings = SpotAirtime::whereIn('spot_version_id', $spot_versions)
            ->whereBetween('air_date', [$start_date->startOfDay(), $end_date->endOfDay()])

            ->get();

        return view('cxp.category-reports.spots-pivot')
            ->with('report', $report)
            ->with('airtimes', $spot_airings);
    }

    public function getSpotDetail($reportID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        $this->checkPermissions($report);

        return view('cxp.category-reports.spots-details')
            ->with('report', $report);
    }

    public function getSpotDetailFull($reportID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        $this->checkPermissions($report);

        return view('cxp.category-reports.spots-details-full')
            ->with('report', $report);
    }

    public function getNetworkDetail($reportID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        $this->checkPermissions($report);

        return view('cxp.category-reports.network-details')
            ->with('report', $report);
    }

    public function getProgram($reportID, $programID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        if (Request::filled('start-date') and Request::filled('end-date')) {
            $start_date = Carbon::createFromFormat("F j, Y", Request::get('start-date'))->startOfDay();
            $end_date = Carbon::createFromFormat("F j, Y", Request::get('end-date'))->endOfDay();

            if ($start_date->gte($end_date)) {
                return redirect()->route('category-report.program', [$reportID, $programID])
                    ->with('error', 'Start date must be before end date.');
            }
        } else {
            $start_date = $report->start_date->startOfDay();
            $end_date = $report->end_date->endOfDay();
        }

        $this->allowedDates($report, $start_date, $end_date);

        $program = Program::findOrFail($programID);

        $over_time = $this->overTimeProgram($programID, $start_date, $end_date);

        $airtimes = Airtime::select([
                'program_versions.grid_title',
                'airtimes.air_date as air_dateable',
                'airtimes.cost',
                'channels.name',
                'channels.logo_location'
            ])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->where('program_versions.program_id', $program->id)
            ->whereBetween(
                'air_date',
                [
                    $start_date->startOfDay(),
                    $end_date->endOfDay()
                ]
            )
            ->orderBy('air_date', 'asc')

            ->get();

        return view('cxp.category-reports.program')
            ->with('report', $report)
            ->with('program', $program)
            ->with('airtimes', $airtimes)
            ->with('over_time', $over_time)
            ->with('start_date', $start_date)
            ->with('end_date', $end_date);
    }

    public function getSpot($reportID, $spotID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        if (Request::filled('start-date') and Request::filled('end-date')) {
            $start_date = Carbon::createFromFormat("F j, Y", Request::get('start-date'))->startOfDay();
            $end_date = Carbon::createFromFormat("F j, Y", Request::get('end-date'))->endOfDay();

            if ($start_date->gte($end_date)) {
                return Redirect::route('category-report.spot', $spotID)
                    ->with('error', 'Start date must be before end date.');
            }
        } else {
            $start_date = $report->start_date->startOfDay();
            $end_date = $report->end_date->endOfDay();
        }

        $this->allowedDates($report, $start_date, $end_date);

        $spot = Spot::findOrFail($spotID);

        $over_time = $this->overTimeSpot($spotID, $start_date, $end_date);

        $airtimes = SpotAirtime::select([
                'spot_versions.title',
                'spot_versions.version',
                'spot_versions.length',
                'spot_airtimes.air_date as air_dateable',
                'channels.name',
                'channels.logo_location',
                'day_parts.name as daypart_name'
            ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('day_parts', 'day_parts.id', '=', 'spot_airtimes.daypart_id')
            ->where('spot_versions.spot_id', $spot->id)
            ->whereBetween(
                'spot_airtimes.air_date',
                [
                    $start_date->format('Y-m-d'),
                    $end_date->format('Y-m-d')
                ]
            )
            ->orderBy('spot_airtimes.air_date', 'asc')

            ->get();

        return view('cxp.category-reports.spot')
            ->with('report', $report)
            ->with('spot', $spot)
            ->with('over_time', $over_time)
            ->with('airtimes', $airtimes)
            ->with('start_date', $start_date)
            ->with('end_date', $end_date);
    }

    public function getProgramsData($reportID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        $this->checkPermissions($report);

        $program_versions = $this->getProgramVersions($report);

        $programs = Program::select([
                DB::raw($report->id. ' as report_id'),
                'programs.id',
                'programs.title',
                'categories.name as category_name',
                'subcat.name as subcategory_name'
            ])
            ->whereIn('programs.id', $report->programs()->pluck('program_id')->toArray())
            ->leftJoin('categories', 'programs.category_id', '=', 'categories.id')
            ->leftJoin(DB::raw('categories as subcat'), 'programs.sub_category_id', '=', 'subcat.id');

        return Datatables::of($programs)
            ->editColumn('title', '<a href="{{ route("category-reports.program", array($report_id, $id)) }}">{{ $title }}</a>')
            ->remove_column('id')
            ->remove_column('report_id')
            ->make();
    }

    public function getSpotsData($reportID)
    {
        try {
            $report = CategoryReport::findOrFail($reportID);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This report does not exist.'. $e);
        }

        $this->checkPermissions($report);

        $spots = Spot::select([
                DB::raw($report->id. ' as report_id'),
                'spots.id',
                'spots.title',
                'categories.name as category',
                'subcat.name as subcategory'
            ])
            ->whereIn('spots.id', $report->spots()->pluck('spot_id')->toArray())
            ->leftJoin('categories', 'spots.category_id', '=', 'categories.id')
            ->leftJoin(DB::raw('categories as subcat'), 'spots.sub_category_id', '=', 'subcat.id');

        return Datatables::of($spots)
            ->editColumn('title', '<a href="{{ route("category-reports.spot", array($report_id, $id)) }}">{{ $title }}</a>')
            ->remove_column('id')
            ->remove_column('report_id')
            ->make();
    }

    private function checkPermissions($report)
    {
        $user = Sentry::getUser();

        if (! $user->hasAccess('category-reports.'.$report->slug)) {
            return abort('403', 'You do not have access to this report!');
        } else {
            return;
        }
    }
}
