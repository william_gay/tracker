<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Retailer;
use App\Models\RetailerProduct;
use App\Models\Retail\RetailerQuarterly;
use View;

class RetailQuarterlyController extends Controller
{
    public function getIndex()
    {
        $quarters = Quarterly::orderBy('quarter')->get();
        $quarterSelect = [];
        foreach ($quarters as $quarter) {
            $quarterSelect[$quarter->id] = $quarter->quarter->format('Y').' Quarter '.$quarter->quarter->quarter;
        }

        $quarter = Quarterly::orderBy('quarter')->first();

        $retailers = Retailer::count();
        $retailer_products = RetailerProduct::count();

        return view('cxp.retail.quarterly.index', compact(
            'quarter',
            'quarterSelect',
            'retailers',
            'retailer_products'
        ));
    }
}
