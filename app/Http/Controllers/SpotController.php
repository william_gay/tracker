<?php

namespace App\Http\Controllers;

use App\Models\Spot;
use App\Models\SpotVersion;
use App\Models\SpotAirtime;
use App\Models\SpotDetection;
use App\Models\SpotRanking;
use App\Models\Language;
use App\Jobs\LogSiteAnalytics;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Sentry;
use Storage;

class SpotController extends Controller
{
    public function getIndex(Builder $builder)
    {
        $user = Sentry::getUser();

        $site_analytics = [
            'type' => 'spot search',
            'user_id' => Sentry::getUser()->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $language_id = request()->get('language_id') ?: 1;
        $language = Language::findOrFail($language_id);

        if (! $user->hasAccess('reports.'.strtolower($language->name).'-spots')) {
            return abort('401', 'Access denied - You do not have access to this report.');
        }

        if (request()->ajax()) {
            return $this->getData();
        }

        $html = $builder->columns([
            ['data' => 'title', 'name' => 'title', 'title' => 'Title'],
            ['data' => 'version', 'name' => 'version', 'title' => 'Version'],
            ['data' => 'length', 'name' => 'length', 'title' => 'Length'],
            ['data' => 'marketingCompany', 'name' => 'marketingCompany', 'title' => 'Company'],
            ['data' => 'category', 'name' => 'category', 'title' => 'Category'],
            ['data' => 'subCategory', 'name' => 'subCategory', 'title' => 'Sub-Category'],
            ['data' => 'air_date', 'name' => 'air_date', 'title' => 'First Detected', 'searchable' => false],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() {}',
            'autoWidth' => false,
            'responsive' => true,
            "order" => [[ 6, "desc" ]],
        ]);

        return view('cxp.spots.all')
            ->with('language_id', $language_id)
            ->with('language', $language)
            ->with('html', $html);
    }

    public function getNew()
    {
        $user = Sentry::getUser();

        if (request()->filled('week_ending')) {
            $week_ending = Carbon::createFromFormat("F j, Y", request()->get('week_ending'));
            $week_ending->hour = 0;
            $week_ending->minute = 0;
            $week_ending->second = 0;
        } else {
            $week_ending = lastFriday();
        }

        $language_id = request()->get('language_id') ?: 1;
        $language = Language::findOrFail($language_id);

        $allowed = data_allowed($week_ending);
        if (! $allowed) {
            $data_since = data_since();
            $data_since->addDays(7);
            return redirect()->route('spots.new', ['week_ending'=> $data_since->format('F j, Y'), 'language_id' => $language_id])
                ->with('success', 'You have been redirected to spots that fit in your data range.');

            return abort('401', 'Access denied - Date outside of range');
        }

        $month = $week_ending->format("Y-m");
        $months = getMonths();
        $weeks = getWeeks($month);

        $site_analytics = [
            'type' => 'spots',
            'extra' => $week_ending->format("Y-m-d"),
            'user_id' => Sentry::getUser()->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        if (! $user->hasAccess('reports.'.strtolower($language->name).'-spots')) {
            return abort('401', 'Access denied - You do not have access to this report.');
        }

        $spots = SpotVersion::findByWeek($week_ending, $language_id);

        return view('cxp.spots.index', compact('spots', 'months', 'weeks', 'month', 'week_ending', 'language_id', 'language'));
    }

    /**
     * Redirect to Spot Version
     *
     * @return Redirect
    */

    public function getParentSpot($spotId)
    {
        $spotVersion = SpotVersion::findBySpotId($spotId)
            ->orderBy('version', 'desc')
            ->first();

        return redirect()->route('spots.show', ['show' => $spotVersion->id]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getSpot($spotId)
    {
        try {
            $spot = SpotVersion::findOrFail($spotId);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This spot does not exist. '. $e);
        }

        if (request()->filled('week_ending')) {
            $week_ending = Carbon::createFromFormat("F j, Y", request()->get('week_ending'));
            $week_ending->setTime(0, 0, 0);
        } else {
            $week_ending = lastFriday();
        }

        $week_beginning = $week_ending->copy()->subDays(7)->endOfDay();
        $week_ending = $week_ending->endOfDay();

        //Data range user has access to.
        $start_date = data_since();
        $end_date = data_to();
        $cache_timeout = 120; //How long queries should be cached

        $initial_date = $spot->air_date;
        $initial_date->startOfDay();

        $allowed = data_allowed($initial_date);
        if (! $allowed) {
            //Get the newest version they have access to and redirect
            $spotAllowed = SpotVersion::where('title', $spot->title)
                ->whereBetween('air_date', [$start_date, $end_date])
                ->orderBy('air_date', 'asc')
                ->first();

            if ($spotAllowed) {
                return redirect()->route('spots.show', $spotAllowed->id)->with('success', 'You have been redirected to a newer version of this spot that fits in your data range.');
            }

            return abort('401', 'Access denied - Date outside of range');
        }

        $site_analytics = [
            'type' => 'spot',
            'type_id' => $spot->id,
            'user_id' => Sentry::getUser()->id
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $spotVersions = SpotVersion::select([
                'id',
                'title',
                'version',
                'length',
                'air_date',
                'initial_date',
                DB::raw('(SELECT max(spot_airtimes.air_date) FROM spot_airtimes WHERE deleted_at IS NULL AND spot_version_id = spot_versions.id) as last_detected')
            ])
            ->where('spot_id', $spot->spot_id)
            ->orderBy('air_date', 'desc')
            ->get();

        $versions = [];
        if ($spotVersions->count() > 0) {
            foreach ($spotVersions as $version) {
                $versions[] = $version->id;
            }
        } else {
            $versions = [$spotId];
        }

        //Total Channels
        $totalChannels = SpotAirtime::select(DB::raw('COUNT(DISTINCT channel_id) as channels'))
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->whereIn('spot_version_id', $versions)
            ->whereBetween('air_date', [$week_beginning, $week_ending])
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })

            ->first();

        $channelList = SpotAirtime::select([
                'channels.abbr',
                'channels.name',
                'channels.logo_location',
                DB::raw('count(spot_airtimes.channel_id) as chancount')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->whereIn('spot_airtimes.spot_version_id', $versions)
            ->whereBetween('air_date', [$week_beginning, $week_ending])
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->groupBy('spot_airtimes.channel_id')
            ->orderBy('chancount', 'desc')
            ->get();

        $bucket = config('filesystems.disks.s3-videos.bucket');
        $s3Disk = Storage::disk('s3-videos');
        $client = $s3Disk->getDriver()->getAdapter()->getClient();
        $expiry = "+30 minutes";

        $signed_flv = null;
        if ($spot->flv == 1) {
            $key = 'flv/'.$spot->file_name.'.flv';
            $command = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key'    => $key
            ]);
            $request = $client->createPresignedRequest($command, $expiry);

            $query = $request->getUri()->getQuery();
            $signed_flv = $s3Disk->url($key).'?'.$query;
        }
        $signed_mp4 = null;
        if ($spot->mp4 == 1) {
            $key = 'mp4/'.$spot->file_name.'.mp4';
            $command = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key'    => $key
            ]);
            $request = $client->createPresignedRequest($command, $expiry);

            $query = $request->getUri()->getQuery();
            $signed_mp4 = $s3Disk->url($key).'?'.$query;
        }
        $signed_poster = null;
        if ($spot->poster == 1) {
            $key = 'poster/'.$spot->file_name.'.jpg';
            $command = $client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key'    => $key
            ]);
            $request = $client->createPresignedRequest($command, $expiry);

            $query = $request->getUri()->getQuery();
            $signed_poster = $s3Disk->url($key).'?'.$query;
        }

        $spotRanking = SpotRanking::where('rank_date', $week_ending->format("Y-m-d"))
            ->where('spot_id', $spot->spot_id)
            ->first();

        if ($spotRanking) {
            $overTime = SpotRanking::select([
                DB::raw('SUM(frequency) as airs'),
                'rank',
                DB::raw('SUM(media_index) as media_index'),
                DB::raw('rank_date as week_ending')
            ])
            ->whereBetween('rank_date', [$week_ending->copy()->subMonths(6)->format('Y-m-d'), $week_ending->format('Y-m-d')])
            ->where('spot_id', $spot->spot_id)
            ->groupBy('rank_date')
            ->get();

            //Life AVG stats
            $avgStats = SpotRanking::select([
                    DB::raw('AVG(frequency) as freq_avg'),
                    DB::raw('AVG(media_index) as mi_avg'),
                    DB::raw('MIN(frequency) as freq_min'),
                    DB::raw('MAX(frequency) as freq_max'),
                ])
                ->where('spot_id', $spot->spot_id)
                ->where('rank_date', '<=', $week_ending->format('Y-m-d'))
                ->first();
        } else {
            // Not Ranked do it manually
            $overTime = SpotAirtime::select([
                DB::raw('COUNT(id) as airs'),
                DB::raw('"0" as rank'),
                DB::raw('(SUM(cost)/2000) as media_index'),
                DB::raw('DATE(air_date) as week_ending')
            ])
            ->where('air_date', '>', $week_ending->copy()->subWeeks(52)->format('Y-m-d'))
            ->where('air_date', '<=', $week_ending->format('Y-m-d'))
            ->whereIn('spot_version_id', $versions)
            ->groupBy(DB::raw('YEARWEEK(air_date)'))
            ->get();

            //Avg Week stats
            $avgStats = SpotAirtime::select([
                    DB::raw('COUNT(id) as frequency'),
                    DB::raw('(SUM(cost)/2000) as media_index'),
                    'air_date'
                ])
                ->where('air_date', '<=', $week_ending->format('Y-m-d'))
                ->whereIn('spot_version_id', $versions)
                ->groupBy(DB::raw('YEARWEEK(air_date)'))
                ->get();
        }

        return view('cxp.spots.show', compact(
            'spot',
            'spotVersions',
            'signed_flv',
            'signed_mp4',
            'signed_poster',
            'totalChannels',
            'channelList',
            'week_ending',
            'overTime',
            'spotRanking',
            'avgStats'
        ));
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getSchedule($spotId)
    {
        $user = Sentry::getUser();

        if (! $user->hasAccess('reports.spot-scheduler')) {
            return abort('401', 'Access denied - You do not have access to this report.');
        }

        if (request()->filled('start-date') and request()->filled('end-date')) {
            $start_date = Carbon::createFromFormat("F j, Y", request()->get('start-date'))->startOfDay();
            $end_date = Carbon::createFromFormat("F j, Y", request()->get('end-date'))->endOfDay();

            if ($start_date->gt($end_date)) {
                return redirect()->route('spots.schedule', $spotId)->with('error', 'Start date must be before end date.');
            }
        } else {
            $start_date = lastFriday()->subWeek()->addDay()->startOfDay();
            $end_date = lastFriday()->endOfDay();
        }

        $allowed_start = data_allowed($start_date);
        $allowed_end = data_allowed($end_date);
        if (! $allowed_start or ! $allowed_end) {
            $end_date = data_to();
            $start_date = $end_date->copy()->subWeek();
        }

        //Get Fridays
        $fridays = fridayRange($start_date, $end_date);

        // need to update
        try {
            $spot = SpotVersion::with('marketingCompany', 'category', 'subCategory', 'channel')
                ->findOrFail($spotId);
        } catch (ModelNotFoundException $e) {
            return abort('404', 'This show does not exist. '. $e);
        }

        $spotVersions = SpotVersion::where('spot_id', $spot->spot_id)->orderBy('version')->get();
        $versionA = [];
        foreach ($spotVersions as $version) {
            $versionA[$version->id] = $version->title.' v'.$version->version;
        }

        if (request()->filled('versions')) {
            $vSelected = request()->get('versions');
        } else {
            $vSelected = SpotVersion::where('spot_id', $spot->spot_id)->orderBy('version')->pluck('id')->toArray();
        }

        foreach ($vSelected as $v) {
            $site_analytics = [
                'type' => 'spot_scheduler',
                'type_id' => $v,
                'user_id' => Sentry::getUser()->id
            ];
            dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));
        }

        $airtimes = SpotAirtime::select([
                DB::raw('COUNT(spot_airtimes.id) as airs'),
                DB::raw('sum(spot_airtimes.cost)/2000 as media_index'),
                DB::raw('sum(cost) as total_cost'),
                DB::raw('count(DISTINCT channel_id)  as channels')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->whereIn('spot_version_id', $vSelected)
            ->whereBetween('spot_airtimes.air_date', [$start_date, $end_date])
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->first();

        $airtimes_raw = SpotAirtime::with('channel', 'spotVersion', 'daypart')
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->whereIn('spot_version_id', $vSelected)
            ->whereBetween(
                'spot_airtimes.air_date',
                [$start_date, $end_date]
            )
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->orderBy('spot_airtimes.air_date', 'asc')
            ->get();

        $channels = SpotAirtime::select([
                'channels.name as channel_name',
                DB::raw('count(spot_airtimes.id) as airs')
            ])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->whereIn('spot_version_id', $vSelected)
            ->whereBetween(
                'spot_airtimes.air_date',
                [$start_date, $end_date]
            )
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->groupBy('channel_id')
            ->orderBy('airs', 'desc')
            ->get();

        $overTime = SpotRanking::select([
                'frequency as airs',
                'rank',
                DB::raw('rank_date as week_ending')
            ])
            ->whereBetween(
                'spot_rankings.rank_date',
                [$start_date, $end_date]
            )
            ->where('spot_id', $spot->spot_id)

            ->get();

        return view(
            'cxp.spots.schedule',
            compact(
                'spot',
                'versionA',
                'vSelected',
                'start_date',
                'end_date',
                'airtimes',
                'channels',
                'airtimes_raw',
                'overTime'
            )
        );
    }

    public function getData()
    {
        $start_date = data_since();
        $end_date = data_to();

        $spots = SpotVersion::with([
                'category',
                'subCategory',
                'marketingCompany',
            ])
            ->where('language_id', request('language_id', 1))
            ->whereBetween('air_date', [$start_date, $end_date]);

        return Datatables::of($spots)
            ->editColumn('title', '<a href="{{ route("spots.show", array($id)) }}">{{ $title }}</a>  @if(Sentry::getUser()->isSuperUser()) <small><a href="{{ route("admin.spots.edit",array($id)) }}" target="_blank">Edit</a></small> @endif')
            ->editColumn('length', ':{{ $length }}')
            ->addColumn('marketingCompany', function ($spot) {
                if ($spot->marketingCompany) {
                    return $spot->marketingCompany->name;
                }
            })
            ->addColumn('category', function ($spot) {
                if ($spot->category) {
                    return $spot->category->name;
                }
            })
            ->addColumn('subCategory', function ($spot) {
                if ($spot->subCategory) {
                    return $spot->subCategory->name;
                }
            })
            ->editColumn('air_date', function ($spot) {
                if ($spot->air_date) {
                    return Carbon::createFromFormat('Y-m-d H:i:s', $spot->air_date)->format('m/d/Y');
                } elseif ($spot->initial_date) {
                    return Carbon::createFromFormat('Y-m-d H:i:s', $spot->air_date)->format('m/d/Y');
                }
            })
            ->rawColumns(['title'])
            // ->orderColumn('monitor_date', 'monitor_date $1')
            ->toJson();
    }
}
