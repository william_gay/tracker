<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use App;
use Illuminate\Support\Facades\DB;
use Queue;
use Redirect;
use Response;
use Request;
use Sentry;
use View;
use App\Models\Category;
use App\Models\Channel;
use App\Models\Market;
use App\Models\Language;
use App\Models\SpotAirtime;
use DateRangePicker\Libraries\Range;
use App\Jobs\LogSiteAnalytics;

class SalesOptimizerController extends Controller
{

    private $channelFormPopulator;
    private $channelToQuery;
    private $channelsCompareToQuery;
    private $marketFormPopulator;
    private $marketToQuery;
    private $range;

    public function getIndex()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $site_analytics = [
            'type' => 'Sales Optimizer',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $byDays = SpotAirtime::select([
            DB::raw('DATE_FORMAT(spot_airtimes.air_date, "%W") as dayofweek'),
            'spots.title',
            'spots.id as spot_id',
            DB::raw('COUNT(spot_airtimes.id) as airs')
        ])
        ->where('spot_airtimes.channel_id', $this->channelToQuery)
        ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
        ->whereBetween('spot_airtimes.air_date', [$this->range['start'], $this->range['end']])
        ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
        ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
        ->groupBy(DB::raw('DAYOFWEEK(spot_airtimes.air_date)'), 'spots.id')
        ->orderBy('airs')
        ->get();

        $byDaysUnique = SpotAirtime::select(DB::raw('DISTINCT spots.title'))
        ->where('spot_airtimes.channel_id', $this->channelToQuery)
        ->whereIn('spot_versions.category_id', $this->categoriesToQuery)
        ->whereBetween('spot_airtimes.air_date', [$this->range['start'], $this->range['end']])
        ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
        ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
        ->groupBy('spot_versions.spot_id')
        ->get();

        $forDays = [
            'Monday' => ['dayofweek' => 'Monday'],
            'Tuesday' => ['dayofweek' => 'Tuesday'],
            'Wednesday' => ['dayofweek' => 'Wednesday'],
            'Thursday' => ['dayofweek' => 'Thursday'],
            'Friday' => ['dayofweek' => 'Friday'],
            'Saturday' => ['dayofweek' => 'Saturday'],
            'Sunday' => ['dayofweek' => 'Sunday'],
        ];

        foreach ($byDays as $day) {
            Arr::set($forDays, $day->dayofweek.'.'.slugify($day->title), $day->airs);
        }

        $newDays = collect();
        foreach ($forDays as $key => $value) {
            $newDays[] = $value;
        }

        $byDaysGraph = collect();
        foreach ($byDaysUnique as $days) {
            $byDaysGraph[] = [
                "balloonText"=> "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[percents]]%</b></span>",
                "fillAlphas" => 0.8,
                "labelText" => "[[percents]]%",
                "lineAlpha" => 0.3,
                "title" => $days->title,
                "type" => "column",
                "valueField" => slugify($days->title)
            ];
        }

        $byDayParts = SpotAirtime::select([
                'day_parts.name as daypart',
                'spots.title',
                'spots.id as spot_id',
                DB::raw('COUNT(spot_airtimes.id) as airs')
            ])
            ->where('spot_airtimes.channel_id', $this->channelToQuery)
            ->whereBetween('spot_airtimes.air_date', [$this->range['start'], $this->range['end']])
            ->leftJoin('day_parts', 'day_parts.id', '=', 'spot_airtimes.daypart_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
            ->groupBy('spot_airtimes.daypart_id', 'spots.id')
            ->get();

        $forDayParts = [
            'Early Morning' => ['daypart' => 'Early Morning'],
            'Early Morning Week End' => ['daypart' => 'Early Morning Week End'],
            'Daytime' => ['daypart' => 'Daytime'],
            'Daytime Weekend' => ['daypart' => 'Daytime Weekend'],
            'Early News' => ['daypart' => 'Early News'],
            'Early Fringe' => ['daypart' => 'Early Fringe'],
            'Primetime' => ['daypart' => 'Primetime'],
            'Late News' => ['daypart' => 'Late News'],
            'Late Fringe' => ['daypart' => 'Late Fringe'],
            'Overnight' => ['daypart' => 'Overnight'],
        ];

        foreach ($byDayParts as $daypart) {
            Arr::set($forDayParts, $daypart->daypart.'.'.slugify($daypart->title), $daypart->airs);
        }

        $newDayParts = collect();
        foreach ($forDayParts as $key => $value) {
            $newDayParts[] = $value;
        }

        $byDayPartsGraph = collect();
        foreach ($byDaysUnique as $daypart) {
            $byDayPartsGraph[] = [
                "balloonText"=> "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[percents]]%</b></span>",
                "fillAlphas" => 0.8,
                "labelText" => "[[percents]]%",
                "lineAlpha" => 0.3,
                "title" => $daypart->title,
                "type" => "column",
                "valueField" => slugify($daypart->title)
            ];
        }

        return view('cxp.sales-optimizer.index')
            ->with('byDays', $byDays)
            ->with('newDays', $newDays)
            ->with('byDaysGraph', $byDaysGraph)
            ->with('byDayParts', $byDayParts)
            ->with('newDayParts', $newDayParts)
            ->with('byDayPartsGraph', $byDayPartsGraph);
    }

    public function getComparison()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $site_analytics = [
            'type' => 'Sales Optimizer Comparison',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        // Get the sports by channel
        $channelByCategory = SpotAirtime::select([
            'spots.title',
            'categories.name as category_name',
            DB::raw('COUNT(spot_airtimes.id) as airings')
        ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
            ->leftJoin('categories', 'categories.id', '=', 'spots.category_id')
            ->whereBetween('spot_airtimes.air_date', [$this->range['start'], $this->range['end']])
            ->where('spot_airtimes.channel_id', $this->channelToQuery)
            ->whereIn('spots.category_id', $this->categoriesToQuery)
            ->where(function ($query) {
                if (Request::filled('spot_ids')) {
                    return $query->whereIn('spots.id', explode(',', Request::get('spot_ids')));
                }
            })
            ->groupBy('categories.id')
            ->get();

        $channelBySpot = SpotAirtime::select([
            'spots.title',
            'categories.name as category_name',
            DB::raw('COUNT(spot_airtimes.id) as airings')
        ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
            ->leftJoin('categories', 'categories.id', '=', 'spots.category_id')
            ->whereBetween('spot_airtimes.air_date', [$this->range['start'], $this->range['end']])
            ->where('spot_airtimes.channel_id', $this->channelToQuery)
            ->whereIn('spots.category_id', $this->categoriesToQuery)
            ->where(function ($query) {
                if (Request::filled('spot_ids')) {
                    return $query->whereIn('spots.id', explode(',', Request::get('spot_ids')));
                }
            })
            ->groupBy('spots.id')
            ->get();

        $compareByCategory = SpotAirtime::select([
            'spots.title',
            'categories.name as category_name',
            DB::raw('COUNT(spot_airtimes.id) as airings')
        ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
            ->leftJoin('categories', 'categories.id', '=', 'spots.category_id')
            ->whereBetween('spot_airtimes.air_date', [$this->range['start'], $this->range['end']])
            ->whereIn('spot_airtimes.channel_id', $this->channelsCompareToQuery)
            ->whereIn('spots.category_id', $this->categoriesToQuery)
            ->where(function ($query) {
                if (Request::filled('spot_ids')) {
                    return $query->whereIn('spots.id', explode(',', Request::get('spot_ids')));
                }
            })
            ->groupBy('categories.id')
            ->get();

        $compareBySpot = SpotAirtime::select([
            'spots.title',
            'categories.name as category_name',
            DB::raw('COUNT(spot_airtimes.id) as airings')
        ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
            ->leftJoin('categories', 'categories.id', '=', 'spots.category_id')
            ->whereBetween('spot_airtimes.air_date', [$this->range['start'], $this->range['end']])
            ->whereIn('spot_airtimes.channel_id', $this->channelsCompareToQuery)
            ->whereIn('spots.category_id', $this->categoriesToQuery)
            ->where(function ($query) {
                if (Request::filled('spot_ids')) {
                    return $query->whereIn('spots.id', explode(',', Request::get('spot_ids')));
                }
            })
            ->groupBy('spots.id')
            ->get();

        return view('cxp.sales-optimizer.comparison')
            ->with('products', Request::get('spot_ids'))
            ->with('channelByCategory', $channelByCategory)
            ->with('channelBySpot', $channelBySpot)
            ->with('compareByCategory', $compareByCategory)
            ->with('compareBySpot', $compareBySpot);
    }

    public function getProducts()
    {
        // Shared view variables, remove them from __construct for user access
        $this->sharedVariables();

        $site_analytics = [
            'type' => 'Sales Optimizer Product Comparison',
            'user_id' => Sentry::getUser()->id,
            'extra'   => json_encode(Request::all())
        ];
        dispatch((new LogSiteAnalytics($site_analytics))->onQueue('analytics'));

        $byDays = SpotAirtime::select([
                DB::raw('DATE_FORMAT(spot_airtimes.air_date, "%W") as dayofweek'),
                'spots.title',
                'spots.id as spot_id',
                DB::raw('COUNT(spot_airtimes.id) as airs')
            ])
            ->whereIn('spot_airtimes.channel_id', $this->channelsCompareToQuery)
            ->where(function ($query) {
                if (Request::filled('spot_ids')) {
                    return $query->whereIn('spots.id', explode(',', Request::get('spot_ids')));
                }
            })
            ->whereBetween('spot_airtimes.air_date', [$this->range['start'], $this->range['end']])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
            ->groupBy(DB::raw('DAYOFWEEK(spot_airtimes.air_date)'), 'spots.id')
            ->orderBy('airs')
            ->get();

        $byDaysUnique = SpotAirtime::select(DB::raw('DISTINCT spots.title'))
            ->whereIn('spot_airtimes.channel_id', $this->channelsCompareToQuery)
            ->where(function ($query) {
                if (Request::filled('spot_ids')) {
                    return $query->whereIn('spots.id', explode(',', Request::get('spot_ids')));
                }
            })
            ->whereBetween('spot_airtimes.air_date', [$this->range['start'], $this->range['end']])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
            ->groupBy('spot_versions.spot_id')
            ->get();

        $forDays = collect();
        $forDays = [
            'Monday' => ['dayofweek' => 'Monday'],
            'Tuesday' => ['dayofweek' => 'Tuesday'],
            'Wednesday' => ['dayofweek' => 'Wednesday'],
            'Thursday' => ['dayofweek' => 'Thursday'],
            'Friday' => ['dayofweek' => 'Friday'],
            'Saturday' => ['dayofweek' => 'Saturday'],
            'Sunday' => ['dayofweek' => 'Sunday'],
        ];
        foreach ($byDays as $day) {
            Arr::set($forDays, $day->dayofweek.'.'.slugify($day->title), $day->airs);
        }

        $newDays = collect();
        foreach ($forDays as $key => $value) {
            $newDays[] = $value;
        }

        $byDaysGraph = collect();
        foreach ($byDaysUnique as $days) {
            $byDaysGraph[] = [
                "balloonText"=> "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[percents]]%</b></span>",
                "fillAlphas" => 0.8,
                "labelText" => "[[percents]]%",
                "lineAlpha" => 0.3,
                "title" => $days->title,
                "type" => "column",
                "valueField" => slugify($days->title)
            ];
        }

        $byDayParts = SpotAirtime::select([
                'day_parts.name as daypart',
                'spots.title',
                'spots.id as spot_id',
                DB::raw('COUNT(spot_airtimes.id) as airs')
            ])
            ->whereIn('spot_airtimes.channel_id', $this->channelsCompareToQuery)
            ->where(function ($query) {
                if (Request::filled('spot_ids')) {
                    return $query->whereIn('spots.id', explode(',', Request::get('spot_ids')));
                }
            })
            ->whereBetween('spot_airtimes.air_date', [$this->range['start'], $this->range['end']])
            ->leftJoin('day_parts', 'day_parts.id', '=', 'spot_airtimes.daypart_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
            ->groupBy('spot_airtimes.daypart_id', 'spots.id')
            ->get();

        $forDayParts = collect();
        $forDayParts = [
            'Early Morning' => ['daypart' => 'Early Morning'],
            'Early Morning Week End' => ['daypart' => 'Early Morning Week End'],
            'Daytime' => ['daypart' => 'Daytime'],
            'Daytime Weekend' => ['daypart' => 'Daytime Weekend'],
            'Early News' => ['daypart' => 'Early News'],
            'Early Fringe' => ['daypart' => 'Early Fringe'],
            'Primetime' => ['daypart' => 'Primetime'],
            'Late News' => ['daypart' => 'Late News'],
            'Late Fringe' => ['daypart' => 'Late Fringe'],
            'Overnight' => ['daypart' => 'Overnight'],
        ];
        foreach ($byDayParts as $daypart) {
            Arr::set($forDayParts, $daypart->daypart.'.'.slugify($daypart->title), $daypart->airs);
        }

        $newDayParts = collect();
        foreach ($forDayParts as $key => $value) {
            $newDayParts[] = $value;
        }

        $byDayPartsGraph = collect();
        foreach ($byDaysUnique as $daypart) {
            $byDayPartsGraph[] = [
                "balloonText"=> "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[percents]]%</b></span>",
                "fillAlphas" => 0.8,
                "labelText" => "[[percents]]%",
                "lineAlpha" => 0.3,
                "title" => $daypart->title,
                "type" => "column",
                "valueField" => slugify($daypart->title)
            ];
        }

        return view('cxp.sales-optimizer.products')
            ->with('products', Request::get('spot_ids'))
            ->with('byDays', $byDays)
            ->with('newDays', $newDays)
            ->with('byDaysGraph', $byDaysGraph)
            ->with('byDayParts', $byDayParts)
            ->with('newDayParts', $newDayParts)
            ->with('byDayPartsGraph', $byDayPartsGraph);
    }

    private function sharedVariables()
    {
        $this->marketToQuery = Language::getMarkets(Request::get('market_id'));
        if (! Sentry::getUser()->hasAccess('reports.spanish-shows') and Request::get('market_id') == 2) {
            $this->marketToQuery = 1;
            Redirect::route('network-analyzer.home')
                            ->with('error', 'Upgrade Your Subscription to Access the Spanish Demographic.');
        }
        if (! Sentry::getUser()->hasAccess('reports.australia') and Request::get('market_id') == 4) {
            $this->marketToQuery = 1;
            Redirect::route('network-analyzer.home')
                            ->with('error', 'Upgrade Your Subscription to Access the Australian Demographic.');
        }
        $this->marketFormPopulator = Language::marketFormPopulator();
        $this->channelFormPopulator = Channel::where('language_id', $this->marketToQuery)
            ->orderBy('name', 'asc')
            ->where(function ($query) {
                if (! Sentry::getUser()->hasAccess('reports.paid-programming-networks')) {
                    $query->where('channels.pi', 0);
                }
            })
            ->pluck('name', 'id')
            ->toArray();

        $this->channelToQuery = Request::filled('channels') ? Request::get('channels') : 22;  // Default to ABC Family
        $this->channelsCompareToQuery = Request::filled('channels_compare') ? Channel::getChannels(Request::get('channels_compare'), Request::get('language_id')) : [22];
        $this->categoryFormPopulator = Category::whereNull('parent_id')->pluck('name', 'id')->toArray();
        $this->categoriesToQuery = Category::getCategories(Request::get('categories'), [1]);
        $this->range = Range::getStartAndEnd(Request::get('rangeStart'), Request::get('rangeEnd'));

        View::share('channelFormPopulator', $this->channelFormPopulator);
        View::share('channelToQuery', $this->channelToQuery);
        View::share('channelsCompareToQuery', $this->channelsCompareToQuery);
        View::share('network', Channel::find($this->channelToQuery));
        View::share('networkCompare', Channel::whereIn('id', $this->channelsCompareToQuery)->pluck('abbr'));
        View::share('marketFormPopulator', $this->marketFormPopulator);
        View::share('marketToQuery', $this->marketToQuery);
        View::share('categoryFormPopulator', $this->categoryFormPopulator);
        View::share('categoriesToQuery', $this->categoriesToQuery);
        View::share('range', $this->range);
        View::share('start_date', $this->range['start']);
        View::share('end_date', $this->range['end']);
    }
}
