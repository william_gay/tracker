<?php namespace App\Http\Controllers\Admin;

use Illuminate\Support\Arr;
use View;
use Request;
use Redirect;
use Lang;
use Event;
use Sentry;
use App\Services\Validators\UserValidator;
use Cartalyst\Sentry\Users\UserAlreadyActivatedException;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Users\UserExistsException;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\PasswordRequiredException;
use App\Events\UserCreated;

use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    /**
     * Show all the users
     *
     * @return Response
     */
    public function index()
    {
        $users = Sentry::getUserProvider()->createModel()->with('groups')->get();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show a user profile
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $user = Sentry::getUserProvider()->findById($id);

            return view('admin.users.show', compact('user'));
        } catch (UserNotFoundException $e) {
            return Redirect::route('admin.users.index')->with('error', $e->getMessage());
        }
    }

    /**
     * Display add user form
     *
     */
    public function create()
    {
        $groups = Sentry::getGroupProvider()->findAll();

        return view('admin.users.create', compact('groups'));
    }

    /**
     * Display the user edit form
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($id)
    {
        try {
            $user   = Sentry::getUserProvider()->findById($id);
            $groups = Sentry::getGroupProvider()->findAll();

            //get only the group id the user belong to
            $userGroupsId = Arr::pluck($user->getGroups()->toArray(), 'id');

            return view('admin.users.edit', compact('user', 'groups', 'userGroupsId'));
        } catch (UserNotFoundException $e) {
            return Redirect::route('admin.users.index')->with('error', $e->getMessage());
        }
    }

    /**
     * Create a new user
     *
     * @return Response
     */
    public function store()
    {
        try {
            $credentials = Request::except('groups');

            $validation = new UserValidator($credentials);

            if ($validation->passes()) {
                //create the user
                $user = Sentry::register($validation->getData(), true);

                $user->groups()->detach();
                $user->groups()->sync(Request::get('groups', []));

                event(new UserCreated($user));

                return Redirect::route('admin.users.index')->with('success', Lang::get('users.create_success'));
            }

            return Redirect::back()->withInput()->withErrors($validation->getErrors());
        } catch (LoginRequiredException $e) {
            return Redirect::back()->withInput()->with('error', $e->getMessage());
        } catch (PasswordRequiredException $e) {
            return Redirect::back()->withInput()->with('error', $e->getMessage());
        } catch (UserExistsException $e) {
            return Redirect::back()->withInput()->with('error', $e->getMessage());
        }
    }

    /**
     * Update user information
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        try {
            $credentials = Request::except('groups');
            $credentials['id'] = $id;

            $validation = new UserValidator($credentials);

            if ($validation->passes()) {
                $user = Sentry::getUserProvider()->findById($id);
                $user->fill($validation->getData());
                $user->save();

                //update groups
                $user->groups()->detach();
                $user->groups()->sync(Request::get('groups', []));

                return Redirect::route('admin.users.index')->with('success', Lang::get('users.update_success'));
            }

            return Redirect::back()->withInput()->withErrors($validation->getErrors());
        } catch (UserExistsException $e) {
            return Redirect::back()->with('error', $e->getMessage());
        } catch (UserNotFoundException $e) {
            return Redirect::back()->with('error', $e->getMessage());
        } catch (LoginRequiredException $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    /**
     * Delete a user
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $currentUser = Sentry::getUser();

        if ($currentUser->id === (int) $id) {
            return Redirect::back()->with('error', Lang::get('users.delete_denied'));
        }

        try {
            $user = Sentry::getUserProvider()->findById($id);
            $eventData = $user;
            $user->delete();

            return Redirect::route('admin.users.index')->with('success', Lang::get('users.delete_success'));
        } catch (UserNotFoundException $e) {
            return Redirect::route('admin.users.index')->with('error', $e->getMessage());
        }
    }

    /**
     * activate or deactivate a user
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function putStatus($id)
    {
        try {
            $user = Sentry::getUserProvider()->findById($id);

            if ($user->isActivated()) {
                $user->activated = 0;
                $user->activated_at = null;
                $user->save();

                return Redirect::route('admin.users.index')->with('success', Lang::get('users.deactivation_success'));
            } else {
                $code = $user->getActivationCode();

                if ($user->attemptActivation($code)) {
                    // User activation passed
                    return Redirect::route('admin.users.index')->with('success', Lang::get('users.activation_success'));
                } else {
                    // User activation failed
                    return Redirect::route('admin.users.index')->with('error', Lang::get('users.activation_fail'));
                }
            }
        } catch (UserNotFoundException $e) {
            return Redirect::route('admin.users.index')->with('error', $e->getMessage());
        } catch (UserAlreadyActivatedException $e) {
            return Redirect::route('admin.users.index')->with('error', $e->getMessage());
        }
    }
}
