<?php namespace App\Http\Controllers\Admin;

use Illuminate\Support\Str;
use App;
use Config;
use Event;
use Lang;
use Mail;
use Sentry;
use Redirect;
use View;
use App\Services\Validators\ResetValidator;
use App\Services\Validators\UserValidator;
use App\Services\Validators\UserSettingValidator;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Users\UserExistsException;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\PasswordRequiredException;
use Cartalyst\Sentry\Users\WrongPasswordException;
use Cartalyst\Sentry\Users\UserNotActivatedException;
use Cartalyst\Sentry\Throttling\UserSuspendedException;
use Cartalyst\Sentry\Throttling\UserBannedException;
use App\Models\Category;
use App\Models\DashboardType;
use App\Models\Language;
use App\Models\Product;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\OAuthClient;
use App\Models\OAuthClientEndpoint;

use App\Http\Controllers\Controller;

class CuratorController extends Controller
{
    public function index()
    {
        $curators = OAuthClient::with('endpoint')->get();

        return view('admin.curators.index', ['curators' => $curators]);
    }

    public function postCurator()
    {
        $clientIDRandom = Str::random(5);
        $clientSecretRandom = Str::random(8);
        $clientName = Request::get('client_name');
        $redirectURI = Request::get('redirect_uri');

        $curator = new OAuthclient();
        $curator->secret = $clientSecretRandom;
        $curator->name = $clientName;
        $curator->id = $clientIDRandom;

        $curatorEndpoint = new OAuthClientEndpoint();
        $curatorEndpoint->client_id = $clientIDRandom;
        $curatorEndpoint->redirect_uri = $redirectURI;

        $curator->save();
        $curatorEndpoint->save();

        if ($curatorEndpoint->save()) {
            return redirect('admin/curators')->with('success', 1);
        } else {
            return redirect('admin/curators')->with('error', 1);
        }
    }
}
