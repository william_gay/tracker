<?php namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Language;
use App\Models\Product;
use App\Models\Retail\RetailerProduct;
use App\Services\Validators\ProductValidator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Redirect;
use Sentry;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * [__construct description]
     *
     * @param PermissionProvider $permissions
     */
    public function __construct()
    {
        view()->share('categories', Category::whereNull('parent_id')->orderBy('name')->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $products = Product::select(
                [
                    'name',
                    'slug',
                    'winmo_company_id',
                    'winmo_brand_id',
                    'products.created_at',
                    'products.updated_at',
                    'products.id'
                ]
            )->groupBy('products.id');

            return Datatables::of($products)
                ->editColumn('id', '
                    <a href="{{ route("admin.products.edit", array($id)) }}"
                        class="btn btn-default" rel="tooltip" title="Edit Category Report">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route("admin.products.destroy", array($id)) }}"
                        class="btn btn-danger" rel="tooltip" title="Delete Group" data-method="delete"
                        data-modal-text="delete this product?">
                        <i class="fa fa-trash-o"></i>
                    </a>
                ')
                ->rawColumns(['spyd', 'id'])
                ->toJson();
        }

        $html = $builder->columns([
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'slug', 'name' => 'slug', 'title' => 'Slug'],
            ['data' => 'winmo_company_id', 'name' => 'winmo_company_id', 'title' => 'Winmo Company ID'],
            ['data' => 'winmo_brand_id', 'name' => 'winmo_brand_id', 'title' => 'Winmo Brand ID'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'created_at'],
            ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At'],
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() { laravel.initialize(); }',
        ]);

        return view('admin.products.index')
            ->with('html', $html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.products.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('admin.products.edit')
            ->with('product', $product)
            ->with(
                'subcats',
                Category::where('parent_id', $product->category_id)->pluck('name', 'id')->toArray()
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = new ProductValidator;

        if ($validation->passes()) {
            $product = new Product;
            $product->name = $request->get('name');
            $product->slug = $request->get('slug');
            $product->category_id = $request->get('category_id');
            $product->sub_category_id = $request->get('sub_category_id');
            $product->winmo_company_id = $request->get('winmo_company_id');
            $product->winmo_brand_id = $request->get('winmo_brand_id');
            $product->asin = $request->get('asin');
            $product->gtin = $request->get('gtin');
            $product->sku = $request->get('sku');
            $product->model_number = $request->get('model_number');
            $product->manufacturer_id = $request->get('manufacturer_id');
            $product->website = $request->get('website');
            $product->description = $request->get('description');
            $product->spyd = $request->filled('spyd') ? 1 : 0;
            $product->user_id = Sentry::getUser()->id;

            $this->s3Magic($request, $product);

            $product->save();

            $this->processPrograms($product, $request->get('program_ids'));
            $this->processSpots($product, $request->get('spot_ids'));

            return Redirect::route('admin.products.edit', $product->id)
                ->with('success', 'The product '.$product->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = new ProductValidator;

        if ($validation->passes()) {
            $product = Product::find($id);
            $product->name = $request->get('name');
            $product->slug = $request->get('slug');
            $product->category_id = $request->get('category_id');
            $product->sub_category_id = $request->get('sub_category_id');
            $product->winmo_company_id = $request->get('winmo_company_id');
            $product->winmo_brand_id = $request->get('winmo_brand_id');
            $product->asin = $request->get('asin');
            $product->gtin = $request->get('gtin');
            $product->sku = $request->get('sku');
            $product->model_number = $request->get('model_number');
            $product->manufacturer_id = $request->get('manufacturer_id');
            $product->website = $request->get('website');
            $product->description = $request->get('description');
            $product->spyd = $request->filled('spyd') ? 1 : 0;
            $product->user_id = Sentry::getUser()->id;

            $this->s3Magic($request, $product);

            $product->save();

            $this->processPrograms($product, $request->get('program_ids'));
            $this->processSpots($product, $request->get('spot_ids'));

            return Redirect::route('admin.products.edit', $product->id)
                ->with('success', 'The product '.$product->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    public function getRetailerProducts($productId)
    {
        $product = Product::find($productId);

        $retailer_products = RetailerProduct::findByProductId($productId);

        return view('admin.products.retailer-products')
            ->with('product', $product)
            ->with('retailer_products', $retailer_products);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->programs()->detach();
        $product->spots()->detach();
        $product->retailerProducts()->delete();
        $product->delete();

        return Redirect::route('admin.products.index')->with('success', 'The product '.$product->name.' was deleted.');
    }

    private function processPrograms($product, $program_ids)
    {
        $ids = explode(',', $program_ids);
        $product->programs()->sync($ids);
    }

    private function processSpots($product, $spot_ids)
    {
        $ids = explode(',', $spot_ids);
        $product->spots()->sync($ids);
    }

    private function s3Magic($request, $product)
    {
        if ($request->hasFile('product_logo')) {
            $path = $request->product_logo
                ->storeAs('products/'.$product->id, 'logo.png', 's3-logos');

            $product->logo_location = $path;
        }
    }
}
