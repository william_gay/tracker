<?php namespace App\Http\Controllers\Admin;

use App\Models\Language;
use App\Services\Validators\LanguageValidator;
use Former;
use Redirect;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.languages.index')->with('languages', Language::paginate(20));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new LanguageValidator;

        if ($validation->passes()) {
            $language = new Language;
            $language->name = Request::get('name');
            $language->abbr = Request::get('abbr');
            $language->user_id = Sentry::getUser()->id;
            $language->save();

            return Redirect::route('admin.languages.edit', $language->id)->with('success', 'The language '.$language->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.languages.show')->with('language', Language::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.languages.edit')->with('language', Language::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new LanguageValidator;

        if ($validation->passes()) {
            $language = Language::find($id);
            $language->name = Request::get('name');
            $language->abbr = Request::get('abbr');
            $language->user_id = Sentry::getUser()->id;
            $language->save();

            return Redirect::route('admin.languages.edit', $language->id)->with('success', 'The language '.$language->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $language = Language::find($id);
        $language->delete();

        return Redirect::route('admin.languages.index')->with('success', 'The language '.$language->name.' was deleted.');
    }
}
