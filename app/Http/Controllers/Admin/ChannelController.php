<?php namespace App\Http\Controllers\Admin;

use App\Models\Channel;
use App\Models\Language;
use App\Models\Server;
use App\Services\Validators\ChannelValidator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Redirect;
use Sentry;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChannelController extends Controller
{
    public function __construct()
    {
        view()->share('languages', Language::all()->pluck('name', 'id')->toArray());
        view()->share('status', [0=>'Inactive',1=>'Active']);
        view()->share('servers', Server::all()->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $channels = Channel::with(['language', 'server', 'user']);

            return Datatables::eloquent($channels)
                ->editColumn('logo_location', '<img src="https://s3-us-west-2.amazonaws.com/ims-logos/{{ $logo_location }}" style="max-width: 75px;" />')
                ->editColumn('info_active', '{{ $info_active == 1 ? "YES" : "NO" }}')
                ->editColumn('spot_active', function ($channel) {
                    if ($channel) {
                        $return = $channel->spot_active == 1 ? "YES" : "NO";

                        if ($channel->id) {
                            return  $return ." ". link_to_route("admin.spotcosts.channel", "Costs", $channel->id);
                        }

                        return $return;
                    }
                })
                ->editColumn('id', function ($channel) {
                    if ($channel->id) {
                        return '<a href="'.route("admin.channels.edit", $channel->id).'" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>
                    <a href="'.route("admin.channels.destroy", array($channel->id)).'" data-method="delete" data-modal-text="Delete this Channel?" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>';
                    }
                })
                ->editColumn('capturing', '{{ $capturing == 1 ? "YES" : "NO" }}')
                ->editColumn('autodetect', '{{ $autodetect == 1 ? "YES" : "NO" }}')
                ->rawColumns(['logo_location', 'spot_active', 'id'])
                ->toJson();
        }

        $html = $builder->columns([
            ['data' => 'abbr', 'name' => 'abbr', 'title' => 'abbr'],
            ['data' => 'logo_location', 'name' => 'logo_location', 'title' => 'Logo'],
            ['data' => 'name', 'name' => 'name', 'title' => 'Channel'],
            ['data' => 'server.name', 'name' => 'server.name', "defaultContent" => '--', 'title' => 'Server'],
            ['data' => 'currency', 'name' => 'currency', 'title' => 'Currency'],
            ['data' => 'info_active', 'name' => 'info_active', 'title' => 'info_active'],
            ['data' => 'spot_active', 'name' => 'spot_active', 'title' => 'spot_active'],
            ['data' => 'language.name', 'name' => 'language.name', 'title' => 'Language'],
            ['data' => 'capturing', 'name' => 'capturing', 'title' => 'capturing'],
            ['data' => 'autodetect', 'name' => 'autodetect', 'title' => 'autodetect'],
            ['data' => 'id', 'name' => 'id', 'title' => 'ID', 'orderable' => false],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() { laravel.initialize(); }',
        ]);

        return view('admin.channels.index')
            ->with('html', $html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.channels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = new ChannelValidator;

        if ($validation->passes()) {
            $channel = new Channel;
            $channel->abbr = request('abbr');
            $channel->name = request('name');
            $channel->hhs = request('hhs');
            $channel->currency = request('currency');
            $channel->receiver = request('receiver');
            $channel->receiver_serial = request('receiver_serial');
            $channel->server_id = request('server_id') == 0 ? null : request('server_id');
            $channel->server_device = request('server_device');
            $channel->carrier = request('carrier');
            $channel->station = request('station') == 0 ? null : request('station');
            $channel->info_active = request('info_active');
            $channel->spot_active = request('spot_active');
            $channel->capturing = request('capturing');
            $channel->autodetect = request('autodetect');
            $channel->pi = request('pi');
            $channel->logical_dish = request('logical_dish');
            $channel->logical_direct = request('logical_direct');
            $channel->logical_fios = request('logical_fios');
            $channel->logical_comcast = request('logical_comcast');
            $channel->broadcast_channel = request('broadcast_channel');
            $channel->xmlid = request('xmlid');
            $channel->call_sign = request('call_sign');
            $channel->paid_programming_start = request('paid_programming_start');
            $channel->paid_programming_end = request('paid_programming_end');
            $channel->language_id = request('language_id');
            $channel->user_id = Sentry::getUser()->id;
            $channel->save();

            $this->s3Magic($request, $channel);

            return Redirect::route('admin.channels.edit', $channel->id)->with('success', 'The channel '.$channel->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.channels.show')->with('channel', Channel::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.channels.edit')->with('channel', Channel::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = new ChannelValidator;

        if ($validation->passes()) {
            $channel = Channel::find($id);
            $channel->abbr = request('abbr');
            $channel->name = request('name');
            $channel->hhs = request('hhs');
            $channel->receiver = request('receiver');
            $channel->receiver_serial = request('receiver_serial');
            $channel->server_id = request('server_id') == 0 ? null : request('server_id');
            $channel->server_device = request('server_device');
            $channel->carrier = request('carrier');
            $channel->station = request('station') == 0 ? null : request('station');
            $channel->info_active = request('info_active');
            $channel->spot_active = request('spot_active');
            $channel->capturing = request('capturing');
            $channel->autodetect = request('autodetect');
            $channel->pi = request('pi');
            $channel->logical_dish = request('logical_dish');
            $channel->logical_direct = request('logical_direct');
            $channel->logical_fios = request('logical_fios');
            $channel->logical_comcast = request('logical_comcast');
            $channel->broadcast_channel = request('broadcast_channel');
            $channel->xmlid = request('xmlid');
            $channel->call_sign = request('call_sign');
            $channel->paid_programming_start = request('paid_programming_start');
            $channel->paid_programming_end = request('paid_programming_end');
            $channel->language_id = request('language_id');
            $channel->user_id = Sentry::getUser()->id;
            $channel->save();

            $this->s3Magic($request, $channel);

            return Redirect::route('admin.channels.edit', $channel->id)->with('success', 'The channel '.$channel->name.' was updated.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $channel = Channel::find($id);
        $channel->delete();

        return Redirect::route('admin.channels.index')->with('success', 'The channel '.$channel->name.' was deleted.');
    }

    private function s3Magic($request, $channel)
    {
        if ($request->hasFile('channel_logo')) {
            $path = $request->channel_logo
                ->storeAs('channels/'.$channel->id, 'logo.png', 's3-logos');

            $channel->logo_location = $path;
            $channel->save();
        }
    }
}
