<?php namespace App\Http\Controllers\Admin;

use App\Models\Channel;
use App\Models\Daypart;
use App\Models\Spot;
use App\Models\SpotCost;
use App\Models\SpotAirtime;
use App\Models\SpotVersion;
use App\Models\Server;
use App\Models\Language;
use App\Services\Validators\AirtimeValidator;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\DB;
use Redirect;
use Response;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class SpotAirtimeController extends Controller
{
    public function __construct()
    {
        $languages = Language::all()->pluck('name', 'id')->toArray();
        View::share('languages', $languages);
        View::share('days', [1 => 'Sunday', 2 => 'Monday', 3 => 'Tuesday', 4 => 'Wednesday', 5 => 'Thursday', 6 => 'Friday', 7 => 'Saturday']);
    }

    public function getAutodetect()
    {
        if (request()->filled('endDate') and request()->filled('startDate')) {
            $endDate = Carbon::createFromFormat('Y-m-d', request()->get('endDate'));
            $startDate = Carbon::createFromFormat('Y-m-d', request()->get('startDate'));
        } else {
            $endDate = Carbon::now();
            $startDate = Carbon::now()->subDays(7);
        }

        $airtimes = SpotAirtime::with(['channel', 'spotVersion', 'channel.server'])
            ->whereNull('spot_airtimes.user_id')
            ->whereBetween('spot_airtimes.air_date', [$startDate->startOfDay()->format('Y-m-d H:i:s'), $endDate->endOfDay()->format('Y-m-d H:i:s')])
            ->orderBy('spot_airtimes.air_date', 'desc')
            ->limit(10000)
            ->get();

        return view('admin.spotairtimes.autodetect')
            ->with('airtimes', $airtimes)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }

    public function getAutodetectBreakdown()
    {
        if (request()->filled('endDate') and request()->filled('startDate')) {
            $endDate = Carbon::createFromFormat('Y-m-d', request()->get('endDate'))->endOfDay();
            $startDate = Carbon::createFromFormat('Y-m-d', request()->get('startDate'))->startOfDay();
        } else {
            $endDate = Carbon::now()->endOfDay();
            $startDate = Carbon::now()->subDays(7)->startOfDay();
        }

        $airtimes = SpotVersion::has('clipsters')
            ->with('language', 'clipsters')
            ->select(
                'spot_versions.id',
                'spot_versions.id as spot_version_id',
                'spot_versions.title',
                'spot_versions.version',
                'spot_versions.length',
                'spot_versions.language_id',
                DB::raw("(SELECT count(id) FROM spot_airtimes
                WHERE spot_airtimes.air_date BETWEEN '".$startDate."' and '".$endDate."'
                and spot_airtimes.spot_version_id = spot_versions.id
                and spot_airtimes.user_id is null) as auto_count"),
                DB::raw("(SELECT count(id) FROM spot_airtimes
                WHERE spot_airtimes.air_date BETWEEN '".$startDate."' and '".$endDate."'
                and spot_airtimes.spot_version_id = spot_versions.id ) as all_count"),
                DB::raw("(SELECT count(DISTINCT spot_airtimes.channel_id) FROM spot_airtimes
                WHERE spot_airtimes.air_date BETWEEN '".$startDate."' and '".$endDate."'
                and spot_airtimes.spot_version_id = spot_versions.id GROUP BY spot_airtimes.spot_version_id) as total_networks"),
                DB::raw("(SELECT count(DISTINCT spot_airtimes.channel_id) FROM spot_airtimes
                WHERE spot_airtimes.air_date BETWEEN '".$startDate."' and '".$endDate."'
                and spot_airtimes.spot_version_id = spot_versions.id
                and spot_airtimes.user_id is null GROUP BY spot_airtimes.spot_version_id) as auto_networks")
            )
            ->groupBy('spot_versions.id')
            ->orderBy('all_count', 'desc')
            ->get();

        return view('admin.spotairtimes.autobreakdown')
            ->with('airtimes', $airtimes)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }

    public function getAutodetectSpotBreakdown($spotVersionId)
    {
        if (request()->filled('endDate') and request()->filled('startDate')) {
            $endDate = Carbon::createFromFormat('Y-m-d', request()->get('endDate'))->endOfDay();
            $startDate = Carbon::createFromFormat('Y-m-d', request()->get('startDate'))->startOfDay();
        } else {
            $endDate = Carbon::now()->endOfDay();
            $startDate = Carbon::now()->subDays(7)->startOfDay();
        }

        $spotVersion = SpotVersion::find($spotVersionId);

        $airtimes = SpotAirtime::select(
            'spot_airtimes.id',
            'spot_airtimes.air_date as air_dateable',
            'channels.name as channel_name',
            'channels.abbr as channel_abbr',
            'users.email',
            'servers.name as server_name',
            'spot_airtimes.url',
            'day_parts.name as daypart_name'
        )
            ->leftJoin('channels', 'spot_airtimes.channel_id', '=', 'channels.id')
            ->leftJoin('users', 'spot_airtimes.user_id', '=', 'users.id')
            ->leftJoin('servers', 'channels.server_id', '=', 'servers.id')
            ->leftJoin('day_parts', 'day_parts.id', '=', 'spot_airtimes.daypart_id')
            ->where('spot_version_id', $spotVersionId)
            ->whereBetween('air_date', [$startDate->format('Y-m-d H:i:s'), $endDate->format('Y-m-d H:i:s')])
            ->orderBy('air_date')
            ->get();

        return view('admin.spotairtimes.autospotbreakdown')
            ->with('airtimes', $airtimes)
            ->with('spotVersion', $spotVersion)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }

    /* Used by Ajax in monitor page */
    public function postStore()
    {
        if (request()->ajax()) {
            $language_id = request()->get('language_id');
            $air_date = Carbon::createFromFormat('Y-m-d H:i:s', request()->get('air_date').' '.request()->get('air_time'));
            $channel_id = request()->get('channel_id');
            $daypart_id = Daypart::getDayPart($air_date);
            $spot_version_id = request()->get('spot_version_id');
            $newSpot = request()->get('new_spot');
            $flagSpot = request()->get('flag_spot');
            $week_ending = $air_date->copy();

            if ($week_ending->dayOfWeek != Carbon::FRIDAY) {
                //If it's not Friday, skip to the next one, otherwise don't
                $week_ending->next(Carbon::FRIDAY);
            }

            /* Return 404 if nothing is submitted */
            if ($spot_version_id == '' and !request()->filled('new_version') and !request()->filled('new_spot_ck') and !request()->filled('flag_spot_ck')) {
                return response()->json(
                    ['error' => true],
                    404
                );
            }

            /* Add a new version of this spot */
            if (request()->filled('new_version') and $spot_version_id != '') {
                //Lookup Current Spot:
                $oldSpot = SpotVersion::find($spot_version_id);
                $latestSpot = SpotVersion::where('spot_id', $oldSpot->spot_id)
                    ->where('language_id', $language_id)
                    ->orderBy('version', 'desc')
                    ->first();

                if ($latestSpot) {
                    // Create that badger
                    $spot_version = new SpotVersion;
                    $spot_version->title = $latestSpot->title;
                    $spot_version->version = $latestSpot->version + 1;
                    $spot_version->spot_id = $latestSpot->spot_id;
                    $spot_version->description = $latestSpot->description;
                    $spot_version->category_id = $latestSpot->category_id;
                    $spot_version->sub_category_id = $latestSpot->sub_category_id;
                    $spot_version->channel_id = $channel_id;
                    $spot_version->marketing_company_id = $latestSpot->marketing_company_id;
                    $spot_version->website = $latestSpot->website;
                    $spot_version->service = $latestSpot->service;
                    $spot_version->length = 0; // Mark a zero because this needs to be updated
                    $spot_version->initial_date = $air_date->toDateTimeString();
                    $spot_version->air_date = $week_ending->format('Y-m-d');
                    $spot_version->keywords = $latestSpot->keywords;
                    $spot_version->language_id = $language_id;
                    $spot_version->user_id = Sentry::getUser()->id;
                    $spot_version->save();

                    $spot_version_id = $spot_version->id;
                }
            }

            /* Add a new version of this spot */
            if (request()->filled('new_spot_ck') and $newSpot != '') {
                $spot = new Spot;
                $spot->title = $newSpot;
                $spot->language_id = $language_id;
                $spot->created_at = Carbon::now()->format('Y-m-d H:i:s');
                $spot->updated_at = Carbon::now()->format('Y-m-d H:i:s');
                $spot->save();

                $spot_version = new SpotVersion;
                $spot_version->title = $newSpot;
                $spot_version->version = 1;
                $spot_version->spot_id = $spot->id;
                $spot_version->length = 0; // Mark a zero because this needs to be updated
                $spot_version->initial_date = $air_date->toDateTimeString();
                $spot_version->air_date = $week_ending->format('Y-m-d');
                $spot_version->channel_id = $channel_id;
                $spot_version->language_id = $language_id;
                $spot_version->user_id = Sentry::getUser()->id;
                $spot_version->created_by = Sentry::getUser()->id;
                $spot_version->save();

                $spot_version_id = $spot_version->id;
            }

            if ($spot_version_id) {
                $spotVersion = SpotVersion::find($spot_version_id);
                if ($spotVersion) {
                    $length = $spotVersion->length;
                } else {
                    $length = 30;
                }
            } else {
                $length = $spot_version->length;
            }

            $spotairtime = new SpotAirtime;
            $spotairtime->channel_id = $channel_id;
            $spotairtime->spot_version_id = $spot_version_id;
            $spotairtime->air_date = $air_date->toDateTimeString();
            $spotairtime->daypart_id = $daypart_id;
            $spotairtime->cost = SpotCost::getCostByChannelAndTime($channel_id, $air_date->toDateTimeString(), $length);
            $spotairtime->user_id = Sentry::getUser()->id;
            $spotairtime->save();

            return response()->json(
                ['error' => false],
                200
            );
        }
    }

    public function postDelete()
    {
        if (request()->filled('airtime_id')) {
            $airtime_id = request()->get('airtime_id');

            $airtime = SpotAirtime::find($airtime_id);

            if ($airtime) {
                $airtime->delete();

                return response()->json(
                    ['error' => false],
                    200
                );
            }

            return response()->json(
                ['error' => true],
                404
            );
        }
    }

    public function getStats()
    {
        if (request()->filled('endDate') and request()->filled('startDate') and request()->filled('language_id')) {
            $endDate = Carbon::createFromFormat('Y-m-d', request()->get('endDate'));
            $startDate = Carbon::createFromFormat('Y-m-d', request()->get('startDate'))->startOfDay();
            $languageId = request()->get('language_id');
        } else {
            $endDate = Carbon::now();
            $startDate = Carbon::now()->subDays(7);
            $languageId = 1;
        }

        $stats = SpotAirtime::select(
            DB::raw('count(spot_airtimes.id) as detection_count'),
            'users.email',
            'spot_airtimes.user_id'
        )
            ->leftJoin('users', 'users.id', '=', 'spot_airtimes.user_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->where('spot_versions.language_id', $languageId)
            ->whereBetween('spot_airtimes.air_date', [$startDate->startOfDay(), $endDate->endOfDay()])
            ->groupBy('user_id')
            ->orderBy('detection_count', 'desc')
            ->get();

        $stats_combined = [];
        foreach ($stats as $stat) {
            $stats_combined[] =  [
                'email' => $stat->email,
                'new_spots' => $this->getNewSpots($stat->user_id, $startDate, $endDate),
                'channels' => $this->getUniqueChannels($stat->user_id, $startDate, $endDate),
                'detection_count' => $stat->detection_count
            ];
        }

        $channel_stats = SpotAirtime::select(
            'channels.id as channel_id',
            'channels.name as channel_name',
            DB::raw('count(spot_airtimes.id) as detection_count')
        )
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->where('spot_versions.language_id', $languageId)
            ->whereBetween('spot_airtimes.air_date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
            ->groupBy('channel_id')
            ->orderBy('detection_count', 'desc')
            ->get();

        $spot_stats = SpotAirtime::select(
            DB::raw('CONCAT(spot_versions.title," " ,spot_versions.version) as spot_name'),
            DB::raw('count(spot_airtimes.id) as detection_count')
        )
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->where('spot_versions.language_id', $languageId)
            ->whereBetween('spot_airtimes.air_date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
            ->groupBy('spot_id')
            ->orderBy('detection_count', 'desc')
            ->get();

        $daypart_stats = SpotAirtime::select(
            DB::raw('CONCAT(day_parts.name, " ", day_parts.abbr, " Time: ", day_parts.time_start, " - ", day_parts.time_end) as daypart_name'),
            DB::raw('count(spot_airtimes.id) as detection_count')
        )
            ->leftJoin('day_parts', 'day_parts.id', '=', 'spot_airtimes.daypart_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->where('spot_versions.language_id', $languageId)
            ->whereBetween('spot_airtimes.air_date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
            ->groupBy('daypart_id')
            ->orderBy('detection_count', 'desc')
            ->get();

        $day_stats = SpotAirtime::select(
            DB::raw('dayofweek(spot_airtimes.air_date) as day'),
            DB::raw('count(spot_airtimes.id) as detection_count')
        )
            ->leftJoin('day_parts', 'day_parts.id', '=', 'spot_airtimes.daypart_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->where('spot_versions.language_id', $languageId)
            ->whereBetween('spot_airtimes.air_date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
            ->groupBy(DB::raw('dayofweek(spot_airtimes.air_date)'))
            ->orderBy('detection_count', 'desc')
            ->get();

        return view('admin.spotairtimes.stats')
            ->with('stats', $stats_combined)
            ->with('channel_stats', $channel_stats)
            ->with('spot_stats', $spot_stats)
            ->with('daypart_stats', $daypart_stats)
            ->with('day_stats', $day_stats)
            ->with('languageId', $languageId)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }

    public function getChannelStats($channelId)
    {
        $channel = Channel::findOrFail($channelId);

        if (request()->filled('endDate') and request()->filled('startDate') and request()->filled('language_id')) {
            $endDate = Carbon::createFromFormat('Y-m-d', request()->get('endDate'));
            $startDate = Carbon::createFromFormat('Y-m-d', request()->get('startDate'));
            $languageId = request()->get('language_id');
        } else {
            $endDate = Carbon::now();
            $startDate = Carbon::now()->subDays(7);
            $languageId = 1;
        }

        $daypart_stats = SpotAirtime::select(
            DB::raw('CONCAT(day_parts.name, " ", day_parts.abbr, " Time: ", day_parts.time_start, " - ", day_parts.time_end) as daypart_name'),
            'day_parts.minutes',
            'day_parts.days',
            DB::raw('count(spot_airtimes.id) as detection_count')
        )
            ->leftJoin('day_parts', 'day_parts.id', '=', 'spot_airtimes.daypart_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->where('spot_versions.language_id', $languageId)
            ->where('spot_airtimes.channel_id', $channelId)
            ->whereBetween('spot_airtimes.air_date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
            ->groupBy('daypart_id')
            ->orderBy('detection_count', 'desc')
            ->get();

        return view('admin.spotairtimes.channelstats')
            ->with('channel', $channel)
            ->with('daypart_stats', $daypart_stats)
            ->with('languageId', $languageId)
            ->with('totalDays', $startDate->diffInDays($endDate))
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }

    private function getUniqueChannels($userId, $startDate, $endDate)
    {
        $channelCount = SpotAirtime::whereBetween('air_date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
            ->where('user_id', $userId)
            ->groupBy('channel_id')
            ->get();

        return count($channelCount->toArray());
    }

    private function getNewSpots($userId, $startDate, $endDate)
    {
        $newSpots = SpotVersion::whereBetween('initial_date', [$startDate->format('Y-m-d'), $endDate->format('Y-m-d')])
            ->where('user_id', $userId);

        return $newSpots->count();
    }

    public function getData()
    {
        $channel_id = request()->get('channel_id');
        $air_date = Carbon::createFromFormat('Y-m-d', request()->get('air_date'));

        $airtimes = SpotAirtime::select(
            'spot_airtimes.id',
            'spot_versions.id as spot_id',
            'spot_versions.title',
            'spot_versions.version',
            'spot_versions.price',
            'spot_versions.length',
            'spot_versions.notes',
            'day_parts.name',
            'users.email',
            'spot_airtimes.created_at'
        )
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('users', 'users.id', '=', 'spot_airtimes.user_id')
            ->leftJoin('day_parts', 'day_parts.id', '=', 'spot_airtimes.daypart_id')
            ->where('spot_airtimes.channel_id', $channel_id)
            ->whereBetween('spot_airtimes.air_date', [$air_date->startOfDay()->format('Y-m-d H:i:s'), $air_date->endOfDay()->format('Y-m-d H:i:s')]);

        return Datatables::of($airtimes)
            ->editColumn('title', '{{ $title }} @if(Sentry::getUser()->isSuperUser()) <small><a href="{{ route("admin.spots.edit",array($spot_id)) }}" data-spotid="{{ $spot_id }}" target="_blank">Edit Spot</a></small> @endif')
            ->editColumn('price', '${{ $price }}')
            ->editColumn('length', ':{{ $length }}')
            ->remove_column('spot_id')
            ->make();
    }

    public function getTable()
    {
        $air_date = Carbon::createFromFormat('Y-m-d', request('air_date', now()->format('Y-m-d')));
        $channel = Channel::where('id', request('channel_id'))->first();
        $server = Server::where('id', $channel->server_id)->first();

        $path = '/?directory='.urlencode('/capture/'.$channel->abbr.'/'.$air_date->format('Y').'/'.$air_date->format('m').'/'.$air_date->format('d'));

        $airtimes = SpotAirtime::with(['user', 'dayPart', 'spotVersion', 'spotVersion.clipsters'])
            ->where('spot_airtimes.channel_id', $channel->id)
            ->whereBetween('spot_airtimes.air_date', [$air_date->copy()->startOfDay()->subSecond()->format('Y-m-d H:i:s'), $air_date->endOfDay()->format('Y-m-d H:i:s')])
            ->orderBy('spot_airtimes.air_date', 'desc')
            ->get();

        return view('admin.spotairtimes.table')
            ->with('airtimes', $airtimes)
            ->with('channel', $channel)
            ->with('server', $server)
            ->with('path', $path)
            ->with('air_date', $air_date);
    }
}
