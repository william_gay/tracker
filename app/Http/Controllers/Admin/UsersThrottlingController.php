<?php namespace App\Http\Controllers\Admin;

use Cartalyst\Sentry\Users\UserNotFoundException;
use Redirect;
use View;
use Event;
use Sentry;
use Lang;
use App\Events\UserBan;
use App\Events\UserUnBan;
use App\Events\UserSuspend;
use App\Events\UserUnSuspend;

use App\Http\Controllers\Controller;

class UsersThrottlingController extends Controller
{
    /**
     * Show the user Throttling status
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getStatus($id)
    {
        try {
            $throttle = Sentry::getThrottleProvider()->findByUserId($id);
            $user = $throttle->getUser();

            return view('admin.throttle.index', compact('throttle', 'user'));
        } catch (UserNotFoundException $e) {
            return Redirect::route('admin.users.index')->with('error', $e->getMessage());
        }
    }

    public function putStatus($id, $action)
    {
        try {
            $throttle = Sentry::getThrottleProvider()->findByUserId($id);
            $user = $throttle->getUser();

            switch ($action) {
                case 'ban':
                    $throttle->ban();
                    event(new UserBan($user));
                    break;
                case 'unban':
                    $throttle->unBan();
                    event(new UserUnBan($user));
                    break;
                case 'suspend':
                    $throttle->suspend();
                    event(new UserSuspend($user));
                    break;
                case 'unsuspend':
                    $throttle->unsuspend();
                    event(new UserUnSuspend($user));
                    break;
                default:
                    return Redirect::route('admin.users.index')
                        ->with('error', Lang::get('throttle.action_not_found', ['action' => $action]));
                    break;
            }

            return Redirect::route('admin.users.index')
                ->with('success', Lang::get('throttle.success', ['action' => $action]));
        } catch (UserNotFoundException $e) {
            return Redirect::route('admin.users.index')->with('error', $e->getMessage());
        }
    }
}
