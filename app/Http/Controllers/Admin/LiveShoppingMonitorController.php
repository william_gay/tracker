<?php namespace App\Http\Controllers\Admin;

use Config;
use Illuminate\Support\Facades\DB;
use Redirect;
use Request;
use Sentry;
use View;
use Carbon\Carbon;
use App\Models\Channel;
use App\Models\LiveShoppingTracker\LiveShoppingCategory;
use App\Models\LiveShoppingTracker\LiveShoppingGuide;
use App\Models\LiveShoppingTracker\LiveShoppingShow;
use App\Services\Validators\LiveShoppingGuideValidator;

use App\Http\Controllers\Controller;

class LiveShoppingMonitorController extends Controller
{
    public function __construct()
    {
        $channels = Channel::whereHas('language', function ($q) {
            $q->where('slug', 'live-shopping');
        })->pluck('name', 'id')->toArray();
        View::share('channels', $channels);
        View::share('categories', LiveShoppingCategory::whereNull('parent_id')->orderBy('name')->pluck('name', 'id')->toArray());
    }

    public function index()
    {
        $channel_id = Request::get('channel_id') ?: 0;
        $air_date = Request::filled('date') ? Carbon::createFromFormat('Y-m-d', Request::get('date')) : Carbon::now();
        // Set the time to the beginning of the day
        $air_date->startOfDay();
        $start_time = Request::filled('start_time') ?
            Carbon::createFromFormat('g:i A', Request::get('start_time'))
                ->setDate($air_date->copy()->year, $air_date->copy()->month, $air_date->copy()->day) :
            $air_date->copy();

        $shows = LiveShoppingShow::orderBy('title', 'asc')->get();

        return view('admin.lst.monitor.index')
            ->with('channel_id', $channel_id)
            ->with('air_date', $air_date)
            ->with('start_time', $start_time)
            ->with('shows', $shows);
    }
}
