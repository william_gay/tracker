<?php namespace App\Http\Controllers\Admin;

use Config;
use Event;
use Lang;
use Sentry;
use Redirect;
use View;
use App\Models\UserCurrent;
use App\Models\Server;
use App\Services\Validators\ServerValidator;

use App\Http\Controllers\Controller;

class ServerController extends Controller
{
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $servers = Server::orderBy('name', 'asc')->paginate(25);

        return view('admin.servers.index')
            ->with('servers', $servers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.servers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new ServerValidator;

        if ($validation->passes()) {
            $server = new Server;
            $server->name = Request::get('name');
            $server->ip = Request::get('ip');
            $server->mac = Request::get('mac');
            $server->mobo = Request::get('mobo');
            $server->processor = Request::get('processor');
            $server->watts = Request::get('watts');
            $server->ram = Request::get('ram');
            $server->location = Request::get('location');
            $server->active = 1;
            $server->modified_by = Sentry::getUser()->id;
            $server->created_by = Sentry::getUser()->id;
            $server->save();

            return Redirect::route('admin.servers.edit', $server->id)->with('success', 'The server '.$server->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.servers.show')->with('server', Server::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.servers.edit')->with('server', Server::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new ServerValidator;

        if ($validation->passes()) {
            $server = Server::find($id);
            $server->name = Request::get('name');
            $server->ip = Request::get('ip');
            $server->mac = Request::get('mac');
            $server->mobo = Request::get('mobo');
            $server->processor = Request::get('processor');
            $server->watts = Request::get('watts');
            $server->ram = Request::get('ram');
            $server->location = Request::get('location');
            $server->active = 1;
            $server->modified_by = Sentry::getUser()->id;
            $server->created_by = Sentry::getUser()->id;
            $server->save();

            return Redirect::route('admin.servers.edit', $server->id)->with('success', 'The server '.$server->name.' was updated.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $server = Server::find($id);
        $server->delete();

        return Redirect::route('admin.servers.index')->with('success', 'The server '.$server->name.' was deleted.');
    }
}
