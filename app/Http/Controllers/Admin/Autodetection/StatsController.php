<?php

namespace App\Http\Controllers\Admin\Autodetection;

use Config;
use Illuminate\Support\Facades\DB;
use Request;
use View;
use Carbon\Carbon;
use App\Models\Airtime;
use App\Models\Channel;
use App\Models\SpotAirtime;
use App\Models\SpotVersion;
use App\Models\ProgramVersion;
use App\Models\Language;

use App\Http\Controllers\Controller;

class StatsController extends Controller
{

    protected $startDate;
    protected $endDate;
    protected $language_id;

    public function __construct()
    {
        if (Request::filled('endDate') and Request::filled('startDate')) {
            $this->endDate = Carbon::createFromFormat('Y-m-d', Request::get('endDate'));
            $this->startDate = Carbon::createFromFormat('Y-m-d', Request::get('startDate'));
            $this->language_id = Request::get('language_id');
        } else {
            $this->endDate = Carbon::now();
            $this->startDate = Carbon::now()->subDays(7);
            $this->language_id = 1;
        }

        View::share('languages', Language::pluck('name', 'id')->toArray());
    }

    public function getIndex()
    {
        $startDate = Carbon::now()->subMonth()->startOfDay()->format('Y-m-d H:i:s');
        $endDate = Carbon::now()->endOfDay()->format('Y-m-d H:i:s');

        $shows = Airtime::select([
                DB::raw('count(id) as count'),
                DB::raw('DATE(air_date) as day'),
                DB::raw('count(distinct channel_id) as channels')
            ])
            ->whereBetween('air_date', [$startDate, $endDate])
            ->whereNull('user_id')
            ->groupBy(DB::raw('DATE(air_date)'))
            ->orderBy('air_date', 'asc')
            ->get();

        $spots = SpotAirtime::select([
                DB::raw('count(id) as count'),
                DB::raw('DATE(air_date) as day'),
                DB::raw('count(distinct channel_id) as channels')
            ])
            ->whereBetween('air_date', [$startDate, $endDate])
            ->whereNull('user_id')
            ->groupBy(DB::raw('DATE(air_date)'))
            ->orderBy('air_date', 'desc')
            ->get();

        return view('auto-boss')
            ->with('spots', $spots)
            ->with('shows', $shows);
    }

    public function getNetwork()
    {
        $longform = $this->getLfByNetwork();
        $shortform = $this->getSfByNetwork();

        return view('admin.autodetection.network')
            ->with('startDate', $this->startDate)
            ->with('endDate', $this->endDate)
            ->with('language_id', $this->language_id)
            ->with('longform', $longform)
            ->with('shortform', $shortform);
    }

    public function getNetworkDetails($id)
    {
        $network = Channel::find($id);
        $longform = $this->getLfByNetworkDetails($id);
        $shortform = $this->getSfByNetworkDetails($id);

        $shows = ProgramVersion::whereBetween('initial_date', [$this->startDate, $this->endDate])
            ->where('channel_id', $id)
            ->get();

        $spots = SpotVersion::whereBetween('air_date', [$this->startDate, $this->endDate])
            ->where('channel_id', $id)
            ->get();

        return view('admin.autodetection.network-details')
            ->with('startDate', $this->startDate)
            ->with('endDate', $this->endDate)
            ->with('channel_id', $id)
            ->with('language_id', $this->language_id)
            ->with('network', $network)
            ->with('longform', $longform)
            ->with('shortform', $shortform)
            ->with('shows', $shows)
            ->with('spots', $spots);
    }

    public function getDetails()
    {
        $filename = Request::get('filename');
        if (Request::filled('filename')) {
            $parsed = $this->parse($filename);
        } else {
            $parsed = false;
        }

        if ($parsed === false) {
            return view('admin.autodetection.details')
                ->with('startDate', $this->startDate)
                ->with('endDate', $this->endDate)
                ->with('filename', '');
        }

        switch ($parsed['type']) {
            case 'LF':
                $details = $this->getLF($parsed['type_id']);
                $raw     = $this->getRawLF($parsed['type_id']);
                $ad      = ProgramVersion::find($parsed['type_id']);
                break;
            case 'SF':
                $details = $this->getSF($parsed['type_id']);
                $raw     = $this->getRawSF($parsed['type_id']);
                $ad      = SpotVersion::find($parsed['type_id']);
                break;
        }

        return view('admin.autodetection.details')
            ->with('startDate', $this->startDate)
            ->with('endDate', $this->endDate)
            ->with('filename', $filename)
            ->with('details', $details)
            ->with('raw', $raw)
            ->with('ad', $ad);
    }

    private function getLfByNetwork()
    {
        return Airtime::with('channel')
            ->select(
                DB::raw('SUM(IF(airtimes.deleted_at is null, 1, 0)) as total_count'),
                DB::raw('SUM(IF(airtimes.user_id is null, 1, 0)) as auto_count'),
                DB::raw('SUM(IF(airtimes.user_id is not null, 1, 0)) as manual_count'),
                DB::raw('SUM(IF(program_versions.initial_date BETWEEN "'.$this->startDate.'" AND "'.$this->endDate.'" AND program_versions.channel_id = channels.id, 1, 0)) as new_count'),
                'airtimes.channel_id'
            )
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->whereBetween('air_date', [$this->startDate, $this->endDate])
            ->where('channels.language_id', $this->language_id)
            ->groupBy('airtimes.channel_id')
            ->orderBy('total_count', 'desc')
            ->get();
    }

    private function getSfByNetwork()
    {
        return SpotAirtime::with('channel')
            ->select(
                DB::raw('SUM(IF(spot_airtimes.deleted_at is null, 1, 0)) as total_count'),
                DB::raw('SUM(IF(spot_airtimes.user_id is null, 1, 0)) as auto_count'),
                DB::raw('SUM(IF(spot_airtimes.user_id is not null, 1, 0)) as manual_count'),
                DB::raw('SUM(IF(spot_versions.air_date BETWEEN "'.$this->startDate.'" AND "'.$this->endDate.'" AND spot_versions.channel_id = channels.id, 1, 0)) as new_count'),
                'spot_airtimes.channel_id'
            )
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->whereBetween('spot_airtimes.air_date', [$this->startDate, $this->endDate])
            ->where('channels.language_id', $this->language_id)
            ->groupBy('spot_airtimes.channel_id')
            ->orderBy('total_count', 'desc')
            ->get();
    }

    private function getLfByNetworkDetails($channel_id)
    {
        return Airtime::with('channel')
            ->select(
                'airtimes.air_date as air_dateable',
                DB::raw('SUM(IF(airtimes.deleted_at is null, 1, 0)) as total_count'),
                DB::raw('SUM(IF(airtimes.user_id is null, 1, 0)) as auto_count'),
                DB::raw('SUM(IF(airtimes.user_id is not null, 1, 0)) as manual_count'),
                'airtimes.channel_id'
            )
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->whereBetween('air_date', [$this->startDate, $this->endDate])
            ->where('airtimes.channel_id', $channel_id)
            ->groupBy(DB::raw('DATE(airtimes.air_date)'))
            ->orderBy('total_count', 'desc')
            ->get();
    }

    private function getSfByNetworkDetails($channel_id)
    {
        return SpotAirtime::with('channel')
            ->select(
                DB::raw('DISTINCT spot_version_id'),
                'spot_airtimes.air_date as air_dateable',
                DB::raw('SUM(IF(spot_airtimes.deleted_at is null, 1, 0)) as total_count'),
                DB::raw('SUM(IF(spot_airtimes.user_id is null, 1, 0)) as auto_count'),
                DB::raw('SUM(IF(spot_airtimes.user_id is not null, 1, 0)) as manual_count'),
                'spot_airtimes.channel_id'
            )
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->whereBetween('spot_airtimes.air_date', [$this->startDate, $this->endDate])
            ->where('spot_airtimes.channel_id', $channel_id)
            ->groupBy(DB::raw('DATE(spot_airtimes.air_date)'))
            ->orderBy('total_count', 'desc')
            ->get();
    }

    private function getRawLF($lfId)
    {
        return Airtime::withTrashed()
            ->whereBetween('air_date', [$this->startDate, $this->endDate])
            ->where('program_version_id', $lfId)
            ->get();
    }

    private function getLF($lfId)
    {
        $auto = Airtime::whereBetween('air_date', [$this->startDate, $this->endDate])
            ->whereNull('user_id')
            ->where('program_version_id', $lfId)
            ->get();

        $manu = Airtime::whereBetween('air_date', [$this->startDate, $this->endDate])
            ->whereNotNull('user_id')
            ->where('program_version_id', $lfId)
            ->get();

        $deleted = Airtime::onlyTrashed()
            ->whereBetween('air_date', [$this->startDate, $this->endDate])
            ->whereNull('user_id')
            ->where('program_version_id', $lfId)
            ->get();

        $total = Airtime::withTrashed()
            ->whereBetween('air_date', [$this->startDate, $this->endDate])
            ->where('program_version_id', $lfId)
            ->get();

        return [
            'autodetected' => $auto->count(),
            'manualdetected' => $manu->count(),
            'deleted' => $deleted->count(),
            'total' => $total->count()
        ];
    }

    private function getRawSF($sfId)
    {
        return SpotAirtime::withTrashed()
            ->whereBetween('air_date', [$this->startDate, $this->endDate])
            ->where('spot_version_id', $sfId)
            ->get();
    }

    private function getSF($sfId)
    {
        $auto = SpotAirtime::whereBetween('air_date', [$this->startDate, $this->endDate])
            ->whereNull('user_id')
            ->where('spot_version_id', $sfId)
            ->get();

        $manu = SpotAirtime::whereBetween('air_date', [$this->startDate, $this->endDate])
            ->whereNotNull('user_id')
            ->where('spot_version_id', $sfId)
            ->get();

        $deleted = SpotAirtime::onlyTrashed()
            ->whereBetween('air_date', [$this->startDate, $this->endDate])
            ->whereNull('user_id')
            ->where('spot_version_id', $sfId)
            ->get();

        $total = SpotAirtime::withTrashed()
            ->whereBetween('air_date', [$this->startDate, $this->endDate])
            ->where('spot_version_id', $sfId)
            ->get();

        return [
            'autodetected' => $auto->count(),
            'manualdetected' => $manu->count(),
            'deleted' => $deleted->count(),
            'total' => $total->count()
        ];
    }

    private function parse($filename)
    {
        $pieces = explode('_', $filename);

        if (count($pieces) < 4) {
            return false;
        }
        if (count($pieces) == 4 or count($pieces) == 5) {
            return [
                'type'    => $pieces[0],
                'type_id' => $pieces[2]
            ];
        }

        if (count($pieces) > 4) {
            return [
                'type'    => $pieces[0],
                'type_id' => $pieces[4]
            ];
        }

        return false;
    }
}
