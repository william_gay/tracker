<?php namespace App\Http\Controllers\Admin;

use Config;
use Event;
use Lang;
use Sentry;
use Redirect;
use View;
use App\Models\UserCurrent;
use App\Models\Daypart;
use App\Services\Validators\DaypartValidator;

use App\Http\Controllers\Controller;

class DaypartController extends Controller
{
    public function __construct()
    {
        View::share(
            'days',
            [
                0 => '-- Select One --',
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $dayparts = Daypart::orderBy('name', 'asc')->paginate(25);

        return view('admin.dayparts.index')
            ->with('dayparts', $dayparts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.dayparts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new DaypartValidator;

        if ($validation->passes()) {
            $daypart = new daypart;
            $daypart->name = Request::get('name');
            $daypart->abbr = Request::get('abbr');
            $daypart->day_start = Request::get('day_start');
            $daypart->day_end = Request::get('day_end');
            $daypart->time_start = Request::get('time_start');
            $daypart->time_end = Request::get('time_end');
            $daypart->week_end = Request::get('week_end') ? 1 : 0;
            $daypart->user_id = Sentry::getUser()->id;
            $daypart->save();

            return Redirect::route('admin.dayparts.edit', $daypart->id)->with('success', 'The daypart '.$daypart->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.dayparts.show')->with('daypart', Daypart::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.dayparts.edit')->with('daypart', Daypart::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new DaypartValidator;

        if ($validation->passes()) {
            $daypart = Daypart::find($id);
            $daypart->name = Request::get('name');
            $daypart->abbr = Request::get('abbr');
            $daypart->day_start = Request::get('day_start');
            $daypart->day_end = Request::get('day_end');
            $daypart->time_start = Request::get('time_start');
            $daypart->time_end = Request::get('time_end');
            $daypart->week_end = Request::get('week_end');
            $daypart->user_id = Sentry::getUser()->id;
            $daypart->save();

            return Redirect::route('admin.dayparts.edit', $daypart->id)->with('success', 'The daypart '.$daypart->name.' was updated.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $daypart = Daypart::find($id);
        $daypart->delete();

        return Redirect::route('admin.dayparts.index')->with('success', 'The daypart '.$daypart->name.' was deleted.');
    }
}
