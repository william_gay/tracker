<?php namespace App\Http\Controllers\Admin;

use App\Models\Airtime;
use App\Models\Category;
use App\Models\Company;
use App\Models\Channel;
use App\Models\Keyword;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\ProgramWeekly;
use App\Models\Language;
use Carbon\Carbon;
use App\Services\Validators\ProgramValidator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Facades\DB;
use Redirect;
use Response;
use Sentry;

use App\Http\Controllers\Controller;

class ProgramController extends Controller
{
    public function __construct()
    {
        view()->share('languages', Language::pluck('name', 'id')->toArray());
        view()->share('categories', Category::whereNull('parent_id')->orderBy('name')->pluck('name', 'id')->toArray());
        view()->share('channels', Channel::orderBy('name')->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            if (request()->filled('language_id') and request()->filled('air_date')) {
                $air_date = Carbon::createFromFormat('Y-m-d', request()->get('air_date'));
                if ($air_date->dayOfWeek != Carbon::FRIDAY) {
                    //If it's not Friday, skip to the next one, otherwise don't
                    $air_date->next(Carbon::FRIDAY);
                }

                $programs = ProgramVersion::with([
                        'channel',
                        'user',
                        'language',
                        'category',
                        'subCategory',
                        'marketingCompany',
                    ])
                    ->where('program_versions.language_id', request()->get('language_id'))
                    ->where('program_versions.monitor_date', $air_date->format('Y-m-d'));
            } else {
                $programs = ProgramVersion::with([
                        'channel',
                        'user',
                        'language',
                        'category',
                        'subCategory',
                        'marketingCompany',
                    ]);
            }

            return Datatables::of($programs)
                ->editColumn('id', '<a href="{{ route("admin.programs.edit", array($id)) }}" class="btn btn-sm btn-default"><i class="fa fa-edit"></i></a>
                    @if(Sentry::getUser()->isSuperUser())<a href="{{ route("admin.programs.destroy", array($id)) }}" data-method="delete" data-modal-text="Delete this Program?" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>@endif')
                ->editColumn('mp4', '@if($mp4 == 1) <i class="fa fa-play"></i> @endif')
                ->addColumn('user', function ($program) {
                    if ($program->user) {
                        return str_replace('@imsreport.com', '', $program->user->email);
                    }
                })
                ->addColumn('channel', function ($program) {
                    if ($program->channel) {
                        return $program->channel->abbr;
                    }
                })
                ->addColumn('category', function ($program) {
                    if ($program->category) {
                        return $program->category->name;
                    }
                })
                ->addColumn('subCategory', function ($program) {
                    if ($program->subCategory) {
                        return $program->subCategory->name;
                    }
                })
                ->addColumn('marketingCompany', function ($program) {
                    if ($program->marketingCompany) {
                        return $program->marketingCompany->name;
                    }
                })
                ->rawColumns(['email', 'id', 'mp4'])
                ->toJson();
        }

        if (request()->filled('language_id') and request()->filled('air_date')) {
            $html = $builder->columns([
                ['data' => 'title', 'name' => 'title', 'title' => 'Title'],
                ['data' => 'grid_title', 'name' => 'grid_title', 'title' => 'Grid Title'],
                ['data' => 'version', 'name' => 'version', 'title' => 'Version'],
                ['data' => 'abbr', 'name' => 'abbr', 'title' => 'ABBR'],
                ['data' => 'mp4', 'name' => 'mp4', 'title' => 'MP4'],
                ['data' => 'user', 'name' => 'user', 'title' => 'email'],
                ['data' => 'id', 'name' => 'id', 'title' => 'id'],
                ['data' => 'description', 'name' => 'description', 'title' => 'description'],
                ['data' => 'host', 'name' => 'host', 'title' => 'host'],
                ['data' => 'host_extra', 'name' => 'host_extra', 'title' => 'host_extra'],
                ['data' => 'marketingCompany', 'name' => 'marketingCompany', 'title' => 'marketingCompany'],
            ]);
        } else {
            $html = $builder->columns([
                ['data' => 'title', 'name' => 'title', 'title' => 'Title'],
                ['data' => 'grid_title', 'name' => 'grid_title', 'title' => 'Grid Title'],
                ['data' => 'version', 'name' => 'version', 'title' => 'Version'],
                ['data' => 'mp4', 'name' => 'mp4', 'title' => 'MP4'],
                ['data' => 'language.name', 'name' => 'language.name', 'title' => 'Language'],
                ['data' => 'monitor_date', 'name' => 'monitor_date', 'title' => 'monitor_date'],
                ['data' => 'category', 'name' => 'category', 'title' => 'Category'],
                ['data' => 'subCategory', 'name' => 'subCategory', 'title' => 'Sub Cat'],
                ['data' => 'user', 'name' => 'user', 'title' => 'email'],
                ['data' => 'id', 'name' => 'id', 'title' => 'id'],
                ['data' => 'description', 'name' => 'description', 'title' => 'description', 'visible' => false],
                ['data' => 'host', 'name' => 'host', 'title' => 'host', 'visible' => false],
                ['data' => 'host_extra', 'name' => 'host_extra', 'title' => 'host_extra', 'visible' => false],
                ['data' => 'marketingCompany', 'name' => 'marketingCompany', 'title' => 'marketingCompany', 'visible' => false],
            ]);
        }

        $builder->parameters([
            'drawCallback' => 'function() { laravel.initialize(); }',
        ]);

        return view('admin.programs.index')
            ->with('html', $html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.programs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new ProgramValidator;

        $program_id = $this->programId();

        if ($validation->passes()) {
            $program = new ProgramVersion;
            $program->title = request()->get('title');
            $program->grid_title = request()->get('grid_title');
            $program->version = request()->get('version');
            $program->program_id = $program_id;
            $program->category_id = request()->get('category_id');
            $program->sub_category_id = request()->get('sub_category_id');
            $program->channel_id = request()->get('channel_id');
            $program->marketing_company_id = request()->get('marketing_company_id');
            $program->production_company_id = request()->get('production_company_id');
            $program->initial_date = request()->get('initial_date');
            $program->monitor_date = request()->get('monitor_date');
            $program->product_name = request()->get('product_name');
            $program->description = request()->get('description');
            $program->currency = request()->get('currency');
            $program->price = request()->get('price');
            $program->cost = request()->get('cost');
            $program->shipping_cost = request()->get('shipping_cost');
            $program->host = request()->get('host');
            $program->host_extra = request()->get('host_extra');
            $program->summary = request()->get('summary');
            $program->remarks = request()->get('remarks');
            $program->upsell = request()->get('upsell');
            $program->keywords = request()->get('keywords');
            $program->order_name = request()->get('order_name');
            $program->order_address = request()->get('order_address');
            $program->order_address_extra = request()->get('order_address_extra');
            $program->order_city = request()->get('order_city');
            $program->order_state = request()->get('order_state');
            $program->order_zip = request()->get('order_zip');
            $program->order_phone = request()->get('order_phone');
            $program->website = request()->get('website');
            $program->customer_svc_phone = request()->get('customer_svc_phone');
            $program->language_id = request()->get('language_id');
            $program->file_name = request()->get('file_name');
            $program->flv = request()->get('flv');
            $program->mp4 = request()->get('mp4');
            $program->poster = request()->get('poster');
            $program->user_id = Sentry::getUser()->id;
            $program->created_by = Sentry::getUser()->id;
            $program->save();

            Keyword::processKeywords(request()->get('keywords'));

            return Redirect::route('admin.programs.edit', $program->id)->with('success', 'The program '.$program->title.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $program = ProgramVersion::with('user')->findOrFail($id);

        $monitor_date = $program->monitor_date;
        $mp4_date = Carbon::create(2013, 10, 5);

        if ($monitor_date->lt($mp4_date) and $program->language_id != 2) {
            $videos = getWeekEndVideos($program->monitor_date, 'flv');
        } else {
            $videos = getWeekEndVideos($program->monitor_date, 'mp4');
        }

        return view('admin.programs.edit')
            ->with('program', $program)
            ->with('videos', $videos)
            ->with(
                'subcats',
                Category::where('parent_id', $program->category_id)->pluck('name', 'id')->toArray()
            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new ProgramValidator;

        if ($validation->passes()) {
            $program = ProgramVersion::find($id);
            $program->title = request()->get('title');
            $program->grid_title = request()->get('grid_title');
            $program->version = request()->get('version');
            $program->program_id = request()->get('program_id');
            $program->category_id = request()->get('category_id');
            $program->sub_category_id = request()->get('sub_category_id');
            $program->channel_id = request()->get('channel_id');
            $program->marketing_company_id = request()->get('marketing_company_id');
            $program->production_company_id = request()->get('production_company_id');
            $program->initial_date = request()->get('initial_date');
            $program->monitor_date = request()->get('monitor_date');
            $program->product_name = request()->get('product_name');
            $program->description = request()->get('description');
            $program->currency = request()->get('currency');
            $program->price = request()->get('price');
            $program->cost = request()->get('cost');
            $program->shipping_cost = request()->get('shipping_cost');
            $program->host = request()->get('host');
            $program->host_extra = request()->get('host_extra');
            $program->summary = request()->get('summary');
            $program->remarks = request()->get('remarks');
            $program->upsell = request()->get('upsell');
            $program->keywords = request()->get('keywords');
            $program->order_name = request()->get('order_name');
            $program->order_address = request()->get('order_address');
            $program->order_address_extra = request()->get('order_address_extra');
            $program->order_city = request()->get('order_city');
            $program->order_state = request()->get('order_state');
            $program->order_zip = request()->get('order_zip');
            $program->order_phone = request()->get('order_phone');
            $program->website = request()->get('website');
            $program->customer_svc_phone = request()->get('customer_svc_phone');
            $program->language_id = request()->get('language_id');
            $program->file_name = (request()->get('file_name') == 0)? null : request()->get('file_name');
            $program->flv = request()->get('flv');
            $program->mp4 = request()->get('mp4');
            $program->poster = request()->get('poster');
            $program->user_id = Sentry::getUser()->id;
            $program->save();

            Keyword::processKeywords(request()->get('keywords'));

            /* Make sure the parent program is correct */
            $parent = Program::find($program->program_id);
            $parent->title = request()->get('title');
            $parent->category_id = request()->get('category_id');
            $parent->sub_category_id = request()->get('sub_category_id');
            $parent->language_id = request()->get('language_id');
            $parent->save();

            $this->propagateCategories($program->program_id, request()->get('category_id'), request()->get('sub_category_id'));

            return Redirect::route('admin.programs.edit', $program->id)->with('success', 'The program '.$program->title.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    private function programId()
    {
        if (! request()->filled('program_id')) {
            $program = new Program;
            $program->title = request()->get('title');
            $program->category_id = request()->get('category_id');
            $program->sub_category_id = request()->get('sub_category_id');
            $program->language_id = request()->get('language_id');
            $program->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $program->updated_at = Carbon::now()->format('Y-m-d H:i:s');

            $program->save();

            return $program->id;
        }

        return request()->get('program_id');
    }

    public function propagateCategories($programId, $categoryId, $subCategoryId)
    {
        $versions = ProgramVersion::where('program_id', $programId)
            ->update([
                'category_id' => $categoryId,
                'sub_category_id' => $subCategoryId
            ]);

        if ($versions) {
            return true;
        }

        return false;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $program = ProgramVersion::findOrFail($id);

        $airtimes = Airtime::where('program_version_id', $id)->get();
        $rankings = ProgramWeekly::where('program_id', $program->program_id)->get();

        if ($rankings->count() == 0) {
            if ($program->version == 1 and $rankings->count() == 0) {
                //Delete the parent program too since there isn't anything
                $program->program()->delete();
            }
            if ($program->airtimes->count() > 0 and $rankings->count() == 0) {
                $program->airtimes()->delete();
            }

            $program->delete();
        } elseif ($rankings->count() > 0 and $airtimes->count() == 0) {
            $program->delete();
        } else {
            return Redirect::route('admin.programs.index')
                ->with('error', 'The program '.$program->title.' could not be deleted. There are '.$airtimes->count(). ' airtimes and '.$rankings->count().' weekly rankings!');
        }

        return Redirect::route('admin.programs.index')->with('success', 'The program '.$program->title.' was deleted including '.$airtimes->count().' airtimes.');
    }
}
