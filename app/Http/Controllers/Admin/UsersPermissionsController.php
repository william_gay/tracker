<?php namespace App\Http\Controllers\Admin;

use View;
use Redirect;
use Lang;
use Event;
use Sentry;
use App\Services\PermissionProvider;
use Cartalyst\Sentry\Users\UserNotFoundException;

use App\Http\Controllers\Controller;

class UsersPermissionsController extends Controller
{
    /**
     * @var PermissionProvider
     */
    protected $permissions;

    public function __construct(PermissionProvider $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * Display the user permissins
     *
     * @param  int      $userId
     * @return Response
     */
    public function index($userId)
    {
        try {
            $user = Sentry::getUserProvider()->findById($userId);
            $modulePerm = $this->permissions->getMergePermissions($user->getPermissions());

            $roles = [['name' => 'generic', 'permissions' => ['view','create','update','delete']]];
            $genericPerm = $this->permissions->getMergePermissions($user->getPermissions(), $roles);

            return view('admin.users.permission', compact('user', 'modulePerm', 'genericPerm'));
        } catch (UserNotFoundException $e) {
            return Redirect::route('admin.users.index')->with('error', $e->getMessage());
        }
    }

    /**
     * Update user permissions
     *
     * @param  int      $userId
     * @return Response
     */
    public function update($userId)
    {
        try {
            $user = Sentry::getUserProvider()->findById($userId);

            $user->permissions = request('rules', []);
            $user->save();

            return Redirect::route('admin.users.index')->with('success', Lang::get('users.permissions_update_success'));
        } catch (UserNotFoundException $e) {
            return Redirect::route('admin.users.permissions')->with('error', $e->getMessage());
        }
    }
}
