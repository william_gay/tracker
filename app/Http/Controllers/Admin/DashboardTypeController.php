<?php namespace App\Http\Controllers\Admin;

use Config;
use Event;
use Lang;
use Sentry;
use Redirect;
use View;
use App\Models\DashboardType;
use App\Services\Validators\DashboardTypeValidator;

use App\Http\Controllers\Controller;

class DashboardTypeController extends Controller
{
    public function __construct()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $types = DashboardType::orderBy('name', 'asc')->paginate(25);

        return view('admin.dashboard.types.index')
            ->with('types', $types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.dashboard.types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new DashboardTypeValidator;

        if ($validation->passes()) {
            $type = new DashboardType;
            $type->name = Request::get('name');
            $type->slug = Request::get('slug');
            $type->user_id = Sentry::getUser()->id;
            $type->save();

            return Redirect::route('admin.dashboard-types.edit', $type->id)->with('success', 'The dashboard type '.$type->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.dashboard.types.show')->with('types', DashboardType::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.dashboard.types.edit')->with('type', DashboardType::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new DashboardTypeValidator;

        if ($validation->passes()) {
            $type = DashboardType::find($id);
            $type->name = Request::get('name');
            $type->slug = Request::get('slug');
            $type->user_id = Sentry::getUser()->id;
            $type->save();

            return Redirect::route('admin.dashboard-types.edit', $type->id)->with('success', 'The dashboard type '.$type->name.' was updated.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $type = DashboardType::find($id);
        $type->delete();

        return Redirect::route('admin.dashboard-types.index')->with('success', 'The dashboard type '.$type->name.' was deleted.');
    }
}
