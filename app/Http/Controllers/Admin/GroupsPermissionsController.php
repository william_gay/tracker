<?php namespace App\Http\Controllers\Admin;

use View;
use Request;
use Redirect;
use Lang;
use Sentry;
use Event;
use App\Services\PermissionProvider;
use Cartalyst\Sentry\Groups\GroupNotFoundException;

use App\Http\Controllers\Controller;

class GroupsPermissionsController extends Controller
{
    /**
     * @var PermissionProvider
     */
    protected $permissions;

    /**
     * [__construct description]
     *
     * @param GroupRepository    $groups
     * @param PermissionProvider $permissions
     */
    public function __construct(PermissionProvider $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * Display the group permissions
     *
     * @param  int      $groupID
     * @return Response
     */
    public function index($groupId)
    {
        try {
            $group = Sentry::getGroupProvider()->findById($groupId);

            // Get the group permissions
            $groupPermissions = $group->getPermissions();

            $permissions = $this->permissions->all(['name','permissions']);

            $modulePerm = $this->permissions->getMergePermissions($groupPermissions, $permissions->toArray());

            $roles = [['name' => 'generic', 'permissions' => ['view','create','update','delete']]];

            $genericPerm = $this->permissions->getMergePermissions($groupPermissions, $roles);

            return view('admin.groups.permission', compact('modulePerm', 'group', 'genericPerm'));
        } catch (GroupNotFoundException $e) {
            return Redirect::route('admin.groups.index')->with('error', $e->getMessage());
        }
    }

    /**
     * Update the group permissions.
     *
     * @param  int      $groupId
     * @return Response
     */
    public function update($groupId)
    {
        try {
            $group = Sentry::getGroupProvider()->findById($groupId);
            $group->permissions = Request::get('rules');
            $group->save();

            return Redirect::route('admin.groups.index')->with('success', Lang::get('groups.update_success'));
        } catch (GroupNotFoundException $e) {
            return Redirect::back()->withInput()->with('error', $e->getMessage());
        }
    }
}
