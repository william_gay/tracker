<?php namespace App\Http\Controllers\Admin;

use Event;
use Input;
use Redirect;
use Sentry;
use View;
use DataTables;
use Yajra\DataTables\Html\Builder;
use App\Models\Category;
use App\Models\Language;
use App\Models\Permission;
use App\Models\CategoryReport;
use App\Services\PermissionProvider;
use App\Services\Validators\CategoryReportValidator;

use App\Http\Controllers\Controller;

class CategoryReportController extends Controller
{
    /**
     * @var PermissionProvider
     */
    protected $permissions;

    /**
     * [__construct description]
     *
     * @param PermissionProvider $permissions
     */
    public function __construct(PermissionProvider $permissions)
    {
        $this->permissions = $permissions;

        View::share('categories', Category::whereNull('parent_id')->orderBy('name')->pluck('name', 'id')->toArray());
        View::share('languages', Language::all()->pluck('name', 'id')->toArray());
        View::share('status', [0 => 'Disabled', 1 => 'Enabled']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $category_reports = CategoryReport::select('name', 'slug', 'created_at', 'updated_at', 'id');

            return  Datatables::of($category_reports)
                ->editColumn('id', '
                    <a href="{!! route("admin.category-reports.edit", array($id)) !!}"
                        class="btn btn-default" rel="tooltip" title="Edit Category Report">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{!! route("admin.category-reports.destroy", array($id)) !!}"
                        class="btn btn-danger" rel="tooltip" title="Delete Group" data-method="delete"
                        data-modal-text="delete this Category Report?">
                        <i class="fa fa-trash-o"></i>
                    </a>
                ')
                ->rawColumns(['id'])
                ->toJson();
        }

        $html = $builder->columns([
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'slug', 'name' => 'slug', 'title' => 'Email'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Created At'],
            ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At'],
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() { laravel.initialize(); }',
        ]);

        return view('admin.category-reports.index')
            ->with('html', $html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.category-reports.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.category-reports.edit')
            ->with('category_report', CategoryReport::find($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new CategoryReportValidator;

        if ($validation->passes()) {
            $report = new CategoryReport;
            $report->name = request('name');
            $report->slug = request('slug');
            $report->overview = request('overview');
            $report->category_id = request('category_id');
            $report->start_date = request('start_date');
            $report->end_date = request('end_date');
            $report->language_id = request('language_id');
            $report->user_id = Sentry::getUser()->id;
            $report->enabled = request('enabled');
            $report->save();

            $this->processPrograms($report, request('program_ids'));
            $this->processSpots($report, request('spot_ids'));

            // Add or update available Report permissions
            $this->updateAvailablePermissions();

            return Redirect::route('admin.category-reports.edit', $report->id)->with('success', 'The category report '.$report->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new CategoryReportValidator;

        if ($validation->passes()) {
            $report = CategoryReport::find($id);
            $report->name = request('name');
            $report->slug = request('slug');
            $report->overview = request('overview');
            $report->category_id = request('category_id');
            $report->start_date = request('start_date');
            $report->end_date = request('end_date');
            $report->language_id = request('language_id');
            $report->user_id = Sentry::getUser()->id;
            $report->enabled = request('enabled');
            $report->save();

            $this->processPrograms($report, request('program_ids'));
            $this->processSpots($report, request('spot_ids'));

            // Add or update available Report permissions
            $this->updateAvailablePermissions();

            return Redirect::route('admin.category-reports.edit', $report->id)->with('success', 'The category report '.$report->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $report = CategoryReport::find($id);
        $report->programs()->detach();
        $report->spots()->detach();
        $report->delete();

        return Redirect::route('admin.category-reports.index')->with('success', 'The category report '.$report->name.' was deleted.');
    }

    private function processPrograms($report, $program_ids)
    {
        $ids = explode(',', $program_ids);
        $report->programs()->sync($ids);
    }

    private function processSpots($report, $spot_ids)
    {
        $ids = explode(',', $spot_ids);
        $report->spots()->sync($ids);
    }

    private function updateAvailablePermissions()
    {
        $permission = Permission::where('name', 'Category-reports')->firstOrFail();

        $reports = CategoryReport::all()->pluck('slug')->toArray();
        $reports_perm = ['view' => 'view', 'create' => 'create', 'update' => 'update', 'delete' => 'delete'];
        foreach ($reports as $report) {
            $reports_perm[$report] = $report;
        }
        $perm = $this->permissions->update($permission->id, ['permissions'=>$reports_perm]);
    }
}
