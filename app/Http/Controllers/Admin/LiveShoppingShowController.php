<?php namespace App\Http\Controllers\Admin;

use Config;
use Illuminate\Support\Facades\DB;
use DataTables;
use Redirect;
use Request;
use Sentry;
use View;
use Carbon\Carbon;
use App\Models\Channel;
use App\Models\Language;
use App\Models\LiveShoppingTracker\LiveShoppingShow;
use App\Models\LiveShoppingTracker\LiveShoppingAirtime;
use App\Models\LiveShoppingTracker\LiveShoppingAirtimeProduct;
use App\Models\LiveShoppingTracker\LiveShoppingProductVersion;
use App\Services\Validators\LiveShoppingShowValidator;

use App\Http\Controllers\Controller;

class LiveShoppingShowController extends Controller
{
    public function __construct()
    {
        View::share('channels', Channel::orderBy('name')->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.lst.shows.index')->with('shows', LiveShoppingShow::paginate(20));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.lst.shows.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new LiveShoppingShowValidator;

        if ($validation->passes()) {
            $show = new LiveShoppingShow;
            $show->channel_id = (Request::get('channel_id') == 0)? null : Request::get('channel_id');
            $show->title = Request::get('title');
            $show->first_airdate = Carbon::createFromFormat('m/d/Y g:i:s A', Request::get('first_airdate'))->toDateTimeString();
            $show->duration = Request::get('duration');
            $show->notes = Request::get('notes');
            $show->user_id = Sentry::getUser()->id;
            $show->save();

            return Redirect::route('admin.lst.shows.edit', $show->id)->with('success', 'The show '.$show->title.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.lst.shows.show')->with('show', LiveShoppingShow::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.lst.shows.edit')->with('show', LiveShoppingShow::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new LiveShoppingShowValidator;

        if ($validation->passes()) {
            $show = LiveShoppingShow::find($id);
            $show->channel_id = (Request::get('channel_id') == 0)? null : Request::get('channel_id');
            $show->title = Request::get('title');
            $show->first_airdate = Carbon::createFromFormat('m/d/Y g:i:s A', Request::get('first_airdate'))->toDateTimeString();
            $show->duration = Request::get('duration');
            $show->notes = Request::get('notes');
            $show->user_id = Sentry::getUser()->id;
            $show->save();

            return Redirect::route('admin.lst.shows.edit', $show->id)->with('success', 'The show '.$show->title.' was updated.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $show = LiveShoppingShow::with(['airtimes'])->find($id);
        $airtime_count = $show->airtimes->count();
        $airtime_prod_count = 0;
        foreach ($show->airtimes as $airtime) {
            // Delete all product airtimes
            $product_airtimes = LiveShoppingAirtimeProduct::where('live_shopping_airtime_id', $airtime->id);
            if ($product_airtimes) {
                $airtime_prod_count = $airtime_prod_count + $product_airtimes->count();
                $product_airtimes->delete();
            }
            // Delete Show Airitme
            $airtime->delete();
        }

        // Delete Show
        $show->delete();

        return Redirect::route('admin.lst.shows.index')->with('success', 'The show '.$show->name.' was deleted, along with '.$airtime_count.' airtime, and '.$airtime_prod_count.' product airtimes!');
    }

    public function getDatatables()
    {
        $shows = LiveShoppingShow::with(['channel',])
            ->select(
                'live_shopping_shows.*',
                DB::raw('DATE_FORMAT(live_shopping_shows.first_airdate, "%Y-%m-%d") as first_airdate'),
                'live_shopping_shows.duration',
                DB::raw('DATE_FORMAT(live_shopping_shows.created_at, "%Y-%m-%d") as created_at'),
                'live_shopping_shows.id'
            );

        return Datatables::of($shows)
            ->addColumn('actions', '<a href="{{ route("admin.lst.shows.edit", array($id)) }}" class="btn btn-sm btn-default"><i class="fa fa-edit"></i></a>
                @if(Sentry::getUser()->isSuperUser())<a href="{{ route("admin.lst.shows.destroy", array($id)) }}" data-method="delete" data-modal-text="Delete this Show? All associated airing data will be removed as well!" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>@endif')
                        //link show title column to UI show page
                        // ->editColumn('title', '<a href="{{ route("lst.show", array($id)) }}">{{ $name }}</a> @if(Sentry::getUser()->isSuperUser()) <small><a href="{{ route("admin.lst.products.edit",array($id)) }}" target="_blank">Edit</a></small> @endif')
            ->rawColumns(['actions'])
            ->make();
    }

    public function reconcile()
    {
        $language = Language::where('slug', 'live-shopping')->first();
        $channels = Channel::where('language_id', $language->id)->pluck('abbr', 'id');

        if (\Request::filled('network')) {
            $channel = Channel::find(\Request::get('network'));
        } else {
            $channel = Channel::where('language_id', $language->id)->first();
        }

        if (! $channel) {
            return Redirect::route('admin.lst.shows.reconcile')->with('error', 'Network not found...');
        }

        if (\Request::filled('day')) {
            $day = Carbon::createFromFormat('Y-m-d', \Request::get('day'));
        } else {
            $day = Carbon::now();
        }

        $shows = LiveShoppingAirtime::with([
                'show',
                'products',
                'products.productVersion',
                'products.productVersion.product'
            ])
            ->where('channel_id', $channel->id)
            ->where('start_time', '>=', $day->copy()->startOfDay()->toDateTimeString())
            ->where('start_time', '<=', $day->copy()->endOfDay()->toDateTimeString())
            ->get();

        return view('admin.lst.shows.reconcile')
            ->with('channels', $channels)
            ->with('shows', $shows)
            ->with('channel', $channel)
            ->with('moreData', \Request::get('moreData'))
            ->with('day', $day);
    }

    public function destroyAirtime($airtimeId)
    {
        $airtime = LiveShoppingAirtime::find($airtimeId);

        foreach ($airtime->products as $airtimeProduct) {
            $deleteVersion = $this->deleteProgramVersion($airtimeProduct->live_shopping_product_version_id);
            $deleteProduct = $this->deleteProgram($airtimeProduct->live_shopping_product_version_id);
            if ($deleteVersion) {
                if ($deleteProduct) {
                    $airtimeProduct->productVersion->product->delete();
                }

                $airtimeProduct->productVersion->delete();
            }

            $airtimeProduct->delete();
        }

        $deleteShow = $this->deleteShow($airtime->live_shopping_show_id);
        if ($deleteShow) {
            $airtime->show->delete();
        }

        $airtime->delete();

        return Redirect::back()->withSuccess('Airtime Deleted!');
    }

    private function deleteProgramVersion($programVersionId)
    {
        $airtimeProducts = LiveShoppingAirtimeProduct::where('live_shopping_product_version_id', $programVersionId)->get();

        // Don't delete more detections exist
        if ($airtimeProducts->count() > 1) {
            return false;
        }

        return true;
    }

    private function deleteProgram($programVersionId)
    {
        $version = LiveShoppingProductVersion::find($programVersionId);

        // Don't delete product more versions exist
        if ($version->product->versions->count() > 1) {
            return false;
        }

        return true;
    }

    private function deleteShow($showId)
    {
        $shows = LiveShoppingAirtime::where('live_shopping_show_id', $showId)->get();

        // Don't delete more detections exist
        if ($shows->count() > 1) {
            return false;
        }

        return true;
    }
}
