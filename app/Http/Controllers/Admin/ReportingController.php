<?php namespace App\Http\Controllers\Admin;

use App\Models\Alert;

use App\Http\Controllers\Controller;

class ReportingController extends Controller
{
    public function getAlerts()
    {
        $alerts = Alert::all();

        return \view('admin.reporting.alerts')
            ->with('alerts', $alerts);
    }
}
