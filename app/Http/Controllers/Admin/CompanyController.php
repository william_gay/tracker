<?php namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Company;
use App\Services\Validators\CompanyValidator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Redirect;
use Sentry;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function __construct()
    {
        view()->share('categories', Category::whereNull('parent_id')->orderBy('name')->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $companies = Company::query();

                //->orderBy('channels.name');
            return Datatables::eloquent($companies)
                ->editColumn('id', '<a href="{{ route("admin.companies.edit", array($id)) }}" class="btn btn-sm btn-default"><i class="fa fa-edit"></i></a>
                    <a href="{{ route("admin.companies.destroy", array($id)) }}" data-method="delete" data-modal-text="Delete this Channel?" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>')
                ->rawColumns(['id'])
                ->toJson();
        }

        $html = $builder->columns([
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'website', 'name' => 'website', 'title' => 'website'],
            ['data' => 'city', 'name' => 'city', 'title' => 'city'],
            ['data' => 'state', 'name' => 'state', 'title' => 'state'],
            ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At'],
            ['data' => 'id', 'name' => 'id', 'title' => 'actions', 'orderable' => false],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() { laravel.initialize(); }',
        ]);

        return view('admin.companies.index')
            ->with('html', $html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = new CompanyValidator;

        if ($validation->passes()) {
            $company = new Company;
            $company->name = $request->get('name');
            $company->address = $request->get('address');
            $company->address_extra = $request->get('address_extra');
            $company->city = $request->get('city');
            $company->state = $request->get('state');
            $company->zip = $request->get('zip');
            $company->phone = $request->get('phone');
            $company->fax = $request->get('fax');
            $company->agency = $request->get('agency');
            $company->contact = $request->get('contact');
            $company->title = $request->get('title');
            $company->email = $request->get('email');
            $company->website = $request->get('website');
            $company->remarks = $request->get('remarks');
            $company->user_id = Sentry::getUser()->id;
            $company->save();

            $this->s3Magic($request, $company);

            return Redirect::route('admin.companies.edit', $company->id)
                ->with('success', 'The company '.$company->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.companies.edit')
            ->with('company', Company::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = new CompanyValidator($request->all());

        $company = Company::find($id);

        if (! $company) {
            return Redirect::route('admin.companies.index')
                ->with('error', 'The company does not exist.');
        }

        if ($validation->passes()) {
            $company->name = $request->get('name');
            $company->address = $request->get('address');
            $company->address_extra = $request->get('address_extra');
            $company->city = $request->get('city');
            $company->state = $request->get('state');
            $company->zip = $request->get('zip');
            $company->phone = $request->get('phone');
            $company->fax = $request->get('fax');
            $company->agency = $request->get('agency');
            $company->contact = $request->get('contact');
            $company->title = $request->get('title');
            $company->email = $request->get('email');
            $company->website = $request->get('website');
            $company->remarks = $request->get('remarks');
            $company->user_id = Sentry::getUser()->id;
            $company->save();

            $this->s3Magic($request, $company);

            return Redirect::route('admin.companies.edit', $company->id)->with('success', 'The company '.$company->name.' was updated.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();

        return Redirect::route('admin.companies.index')->with('success', 'The company '.$company->name.' was deleted.');
    }

    private function s3Magic($request, $company)
    {
        if ($request->hasFile('company_logo')) {
            $path = $request->company_logo
                ->storeAs('companies/'.$company->id, 'logo.png', 's3-logos');

            $company->logo_location = $path;
            $company->save();
        }
    }
}
