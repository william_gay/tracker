<?php namespace App\Http\Controllers\Admin;

use Config;
use Illuminate\Support\Facades\DB;
use Redirect;
use Response;
use Request;
use Sentry;
use Session;
use View;
use Carbon\Carbon;
use App\Models\LiveShoppingTracker\LiveShoppingProduct;
use App\Models\LiveShoppingTracker\LiveShoppingProductVersion;
use App\Services\Validators\LiveShoppingProductVersionValidator;

use App\Http\Controllers\Controller;

class LiveShoppingProductVersionController extends Controller
{

    public function add()
    {
        $productId = Request::get('productId');

        $product = LiveShoppingProduct::find($productId);

        $version = LiveShoppingProductVersion::where(['live_shopping_product_id' => $productId])
            ->orderBy('first_airdate', 'desc')
            ->first();

        return view('admin.lst.product-versions.add')
            ->with('product', $product)
            ->with('version', $version);
    }

    public function edit($versionId)
    {
        $productId = Request::get('productId');

        $product = LiveShoppingProduct::find($productId);
        $version = LiveShoppingProductVersion::find($versionId);

        return view('admin.lst.product-versions.edit')
            ->with('product', $product)
            ->with('version', $version);
    }

    public function store()
    {
        $validation = new LiveShoppingProductVersionValidator;

        if ($validation->passes()) {
            $version = new LiveShoppingProductVersion;
            $version->name = Request::get('name');
            $version->live_shopping_product_id = Request::get('live_shopping_product_id');
            $version->product_configuration = Request::get('product_configuration');
            $version->price = Request::get('price');
            $version->shipping_and_handling = Request::get('shipping_and_handling');
            $version->payment_plan = $this->isPayentPlan();
            $version->payment_plan_notes = Request::get('payment_plan_notes');
            $version->daily_special_value = $this->isDSV();
            $version->units_sold = Request::get('units_sold');
            $version->first_airdate = Carbon::createFromFormat("Y-m-d h:i A", Request::get('first_airdate'));
            $version->notes = Request::get('notes');
            $version->user_id = Sentry::getUser()->id;
            $version->save();
        } else {
            $messages = explode("\n\r", $validation->errors);

            return response()->json(['result' => 'error', 'message' => $messages]);
        }

        return response()->json(['result' => 'success', 'version_id' => $version->id, 'message' => 'Product version added successfully!']);
    }

    public function update()
    {
        $validation = new LiveShoppingProductVersionValidator;

        if ($validation->passes()) {
            $version = LiveShoppingProductVersion::find(Request::get('id'));
            $version->name = Request::get('name');
            $version->live_shopping_product_id = Request::get('live_shopping_product_id');
            $version->product_configuration = Request::get('product_configuration');
            $version->price = Request::get('price');
            $version->shipping_and_handling = Request::get('shipping_and_handling');
            $version->payment_plan = $this->isPayentPlan();
            $version->payment_plan_notes = Request::get('payment_plan_notes');
            $version->daily_special_value = $this->isDSV();
            $version->units_sold = Request::get('units_sold');
            $version->first_airdate = Carbon::createFromFormat("Y-m-d h:i A", Request::get('first_airdate'));
            $version->notes = Request::get('notes');
            $version->user_id = Sentry::getUser()->id;
            $version->save();
        } else {
            $messages = explode("\n\r", $validation->errors);

            return response()->json(['result' => 'error', 'message' => $messages]);
        }

        return response()->json(['result' => 'success', 'message' => 'Product version updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $productVersion = LiveShoppingProductVersion::with(['productAirtimes'])->find($id);
        $productAirtimes = $productVersion->productAirtimes->count();

        // Delete Airtimes
        $productVersion->productAirtimes()->delete();

        // Delete product
        $productVersion->delete();

        return Redirect::route('admin.lst.products.index')
            ->with('success', 'The product '.$productVersion->name.' was deleted, along with '.$productAirtimes.' airings!');
    }

    public function isPayentPlan()
    {
        return Request::filled('payment_plan') ? 1 : 0;
    }

    public function isDSV()
    {
        return Request::filled('daily_special_value') ? 1 : 0;
    }
}
