<?php namespace App\Http\Controllers\Admin;

use App\Models\Channel;
use App\Models\Daypart;
use App\Models\Language;
use App\Models\SpotCost;
use Carbon\Carbon;
use App\Services\Validators\SpotValidator;
use DataTables;
use Illuminate\Support\Facades\DB;
use Input;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class SpotCostController extends Controller
{
    public function __construct()
    {
        View::share('languages', Language::pluck('name', 'id')->toArray());
    }

    public function getChannel($channelId)
    {
        $channel = Channel::findOrFail($channelId);
        $cost_date = Carbon::now()->format('Y-m-d');
        $dayparts = Daypart::orderBy('order', 'asc')->get();

        $daypart_select = [];
        foreach ($dayparts as $part) {
            $daypart_select[$part->id] = $part->name .' ('.$part->abbr.') Time: '.$part->time_start.' - '.$part->time_end;
        }

        return view('admin.spotcosts.channel', compact(
            'daypart_select',
            'channel',
            'cost_date'
        ));
    }

    public function postAdd()
    {
        if (Request::filled('channel_id') and Request::filled('daypart_id') and Request::filled('cost_date') and Request::filled('amount')) {
            $channel_id = Request::get('channel_id');
            $daypart_id = Request::get('daypart_id');
            $costDate = Carbon::createFromFormat('Y-m-d', Request::get('cost_date'))->setTime(0, 0, 0);
            $amount = Request::get('amount');

            $daypart = Daypart::find($daypart_id);

            $stParts = explode(':', $daypart->time_start);
            $etParts = explode(':', $daypart->time_end);

            if ($daypart_id == 9) {
                $startTime = $costDate->copy()->subDay()->addHours($stParts[0])->addMinutes(($stParts[1] > 30) ? 30 : $stParts[1]);
            } else {
                $startTime = $costDate->copy()->setTime($stParts[0], ($stParts[1] > 30) ? 30 : $stParts[1], $stParts[2]);
            }

            if ($daypart_id == 8) {
                $endTime = $costDate->copy()->setTime($etParts[0], $etParts[1], $etParts[2])->addMinutes(25);
            } else {
                $endTime = $costDate->copy()->setTime($etParts[0], $etParts[1], $etParts[2]);
            }

            // var_dump($startTime);
            // var_dump($endTime);

            while ($startTime->lte($endTime)) {
                $cost = new SpotCost;
                $cost->channel_id = $channel_id;
                $cost->daypart_id = $daypart_id;
                $cost->time_slot = $startTime->format('H:i:s');
                $cost->amount = $amount;
                $cost->user_id = Sentry::getUser()->id;
                $cost->save();

                $startTime->addMinutes(30);
            }

            return response()->json(
                ['error' => false],
                200
            );
        } else {
            return response()->json(
                ['error' => true],
                500
            );
        }
    }

    public function postDelete()
    {
        if (Request::filled('id')) {
            $cost = SpotCost::findOrFail(Request::get('id'));
            $cost->delete();

            return response()->json(
                ['error' => false],
                200
            );
        } else {
            return response()->json(
                ['error' => true],
                500
            );
        }
    }

    public function getData()
    {
        $costs = SpotCost::select(
            'spot_costs.id',
            'channels.name as channel_name',
            'day_parts.name as daypart_name',
            'time_slot',
            'amount',
            'spot_costs.created_at'
        )->with(['channel', 'daypart'])
            ->leftJoin('channels', 'channels.id', '=', 'spot_costs.channel_id')
            ->leftJoin('day_parts', 'day_parts.id', '=', 'spot_costs.daypart_id')
            ->where('channel_id', Request::get('channel_id'))
            ->orderBy('spot_costs.created_at', 'desc');

        return Datatables::of($costs)
            ->editColumn('id', '<a href="#" class="btn btn-sm btn-danger deleteCost" data-spotcostid="{{ $id }}"> <i class="fa fa-trash-o"></i></a>')
            ->rawColumns(['id'])
            ->make();
    }
}
