<?php namespace App\Http\Controllers\Admin;

use App\Models\Airtime;
use App\Models\AirtimeVerify;
use App\Models\Channel;
use App\Models\Cost;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\Server;
use App\Services\Validators\AirtimeValidator;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\DB;
use Input;
use Redirect;
use Response;
use Request;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class AirtimeController extends Controller
{
    public function __construct()
    {
        View::share('channels', Channel::all()->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.airtimes.index')->with('airtimes', Airtime::paginate(20));
    }

    public function getAutodetect()
    {
        if (Request::filled('endDate') and Request::filled('startDate')) {
            $endDate = Carbon::createFromFormat('Y-m-d', Request::get('endDate'));
            $startDate = Carbon::createFromFormat('Y-m-d', Request::get('startDate'));
        } else {
            $endDate = Carbon::now();
            $startDate = Carbon::now()->subDays(7);
        }

        $airtimes = Airtime::select(
            'airtimes.program_version_id',
            'airtimes.channel_id',
            'airtimes.air_date',
            'channels.name as channel_name',
            'channels.abbr as channel_abbr',
            'program_versions.grid_title as grid_title',
            'program_versions.title as title',
            'servers.name as server_name'
        )
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->leftJoin('servers', 'servers.id', '=', 'channels.server_id')
            ->whereNull('airtimes.user_id')
            ->whereBetween('airtimes.air_date', [$startDate->startOfDay()->format('Y-m-d H:i:s'), $endDate->endOfDay()->format('Y-m-d H:i:s')])
            ->orderBy('airtimes.air_date', 'desc')
            ->get();

        return view('admin.airtimes.autodetect')
            ->with('airtimes', $airtimes)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }

    public function getAutodetectBreakdown()
    {
        if (Request::filled('endDate') and Request::filled('startDate')) {
            $endDate = Carbon::createFromFormat('Y-m-d', Request::get('endDate'));
            $startDate = Carbon::createFromFormat('Y-m-d', Request::get('startDate'));
        } else {
            $endDate = Carbon::now();
            $startDate = Carbon::now()->subDays(7);
        }

        $airtimes = ProgramVersion::filled('clipsters')
            ->with(['clipsters', 'language'])
            ->select(
                'program_versions.id',
                'program_versions.title',
                'program_versions.version',
                'program_versions.grid_title',
                'program_versions.language_id',
                DB::raw("(SELECT count(id) FROM airtimes
                WHERE airtimes.air_date BETWEEN '".$startDate."' and '".$endDate."'
                and airtimes.program_version_id = program_versions.id
                and airtimes.user_id is null) as auto_count"),
                DB::raw("(SELECT count(id) FROM airtimes
                WHERE airtimes.air_date BETWEEN '".$startDate."' and '".$endDate."'
                and airtimes.program_version_id = program_versions.id ) as all_count"),
                DB::raw("(SELECT count(DISTINCT airtimes.channel_id) FROM airtimes
                WHERE airtimes.air_date BETWEEN '".$startDate."' and '".$endDate."'
                and airtimes.program_version_id = program_versions.id GROUP BY airtimes.program_version_id) as total_networks"),
                DB::raw("(SELECT count(DISTINCT airtimes.channel_id) FROM airtimes
                WHERE airtimes.air_date BETWEEN '".$startDate."' and '".$endDate."'
                and airtimes.program_version_id = program_versions.id
                and airtimes.user_id is null GROUP BY airtimes.program_version_id) as auto_networks")
            )
            ->groupBy('program_versions.id')
            ->orderBy('all_count', 'desc')
            ->get();

        return view('admin.airtimes.autobreakdown')
            ->with('airtimes', $airtimes)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }

    public function getAutodetectProgramBreakdown($programVersionId)
    {
        if (Request::filled('endDate') and Request::filled('startDate')) {
            $endDate = Carbon::createFromFormat('Y-m-d', Request::get('endDate'))->endOfDay();
            $startDate = Carbon::createFromFormat('Y-m-d', Request::get('startDate'))->startOfDay();
        } else {
            $endDate = Carbon::now()->endOfDay();
            $startDate = Carbon::now()->subDays(7)->startOfDay();
        }

        $programVersion = ProgramVersion::find($programVersionId);

        $airtimes = Airtime::with(['channel', 'channel.server','user'])
            ->select(['airtimes.*', 'air_date as air_dateable'])
            ->where('program_version_id', $programVersionId)
            ->whereBetween('air_date', [$startDate->format('Y-m-d H:i:s'), $endDate->format('Y-m-d H:i:s')])
            ->orderBy('air_date')
            ->get();

        return view('admin.airtimes.autoprogrambreakdown')
            ->with('airtimes', $airtimes)
            ->with('programVersion', $programVersion)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $date = new \DateTime(date('Y-m-d'));
        $count = 24 * 60 / 30;
        $times = [];
        while ($count--) {
            $newDate = $date->add(new \DateInterval("P0Y0DT0H30M"));
            $times[$newDate->format("H:i:s")] = $newDate->format("h:i a");
        }

        return view('admin.airtimes.create')->with('programs', Program::all()->pluck('grid_title', 'id')->toArray())
                                                ->with('times', $times);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new AirtimeValidator;

        if ($validation->passes()) {
            $airtime = new Airtime;
            $airtime->channel_id = Request::get('channel_id');
            $airtime->program_version_id = Request::get('program_version_id');
            $airtime->air_date = Request::get('air_date').' '.Request::get('air_time');
            $airtime->user_id = Sentry::getUser()->id;
            $airtime->save();

            return Redirect::route('admin.airtimes.edit', $airtime->id)->with('success', 'Airtime saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /* Used by Ajax in monitor page */

    public function postStore()
    {
        if (Request::ajax()) {
            $program_version_id = Request::get('program_version_id');
            $channel_id = Request::get('channel_id');
            $language_id = Request::get('language_id');
            $air_date = Carbon::createFromFormat('Y-m-d H:i:s', Request::get('air_date'));
            $week_ending = Carbon::createFromFormat('Y-m-d H:i:s', Request::get('air_date'));

            if ($week_ending->dayOfWeek != Carbon::FRIDAY) {
                //If it's not Friday, skip to the next one, otherwise don't
                $week_ending->next(Carbon::FRIDAY);
            }

            /* Add a new version of this spot */
            if (Request::filled('new_version') and $program_version_id != '') {
                //Lookup Current Spot:
                $oldProgram = ProgramVersion::find($program_version_id);
                $mId = $oldProgram->id;

                $latestProgram = ProgramVersion::where('program_id', $oldProgram->program_id)
                    ->where('language_id', $language_id)
                    ->orderBy('version', 'desc')
                    ->first();

                if ($latestProgram) {
                    // Create that badger
                    $program_version = new ProgramVersion;
                    $program_version->title = $latestProgram->title;
                    $program_version->grid_title = $latestProgram->grid_title." (".($latestProgram->version + 1).")";
                    $program_version->version = $latestProgram->version + 1;
                    $program_version->program_id = $latestProgram->program_id;
                    $program_version->description = $latestProgram->description;
                    $program_version->category_id = $latestProgram->category_id;
                    $program_version->sub_category_id = $latestProgram->sub_category_id;
                    $program_version->channel_id = $channel_id;
                    $program_version->marketing_company_id = $latestProgram->marketing_company_id;
                    $program_version->website = $latestProgram->website;
                    $program_version->initial_date = $air_date->format('Y-m-d H:i:s');
                    $program_version->monitor_date = $week_ending->format('Y-m-d');
                    $program_version->keywords = $latestProgram->keywords;
                    $program_version->language_id = $language_id;
                    $program_version->user_id = Sentry::getUser()->id;
                    $program_version->save();

                    $program_version_id = $program_version->id;
                }
            }

            if ($program_version_id == '') {
                // Create that badger
                $program = new Program;
                $program->title = trim(Request::get('custom_program'));
                $program->language_id = $language_id;
                $program->save();

                $program_version = new ProgramVersion;
                $program_version->title = trim(Request::get('custom_program'));
                $program_version->grid_title = trim(Request::get('custom_program'));
                $program_version->program_id = $program->id;
                $program_version->initial_date = $air_date->format('Y-m-d H:i:s');
                $program_version->monitor_date = $week_ending->format('Y-m-d');
                $program_version->channel_id = $channel_id;
                $program_version->language_id = $language_id;
                $program_version->user_id = Sentry::getUser()->id;
                $program_version->created_by = Sentry::getUser()->id;
                $program_version->save();

                $program_version_id = $program_version->id;
            }

            //Check to see if this airtime
            if (Request::get('airtime_id') == '') {
                $airtime = Airtime::where('channel_id', $channel_id)
                    ->where('air_date', $air_date->format('Y-m-d H:i:s'))
                    ->first();

                if (!$airtime) {
                    $airtime = new Airtime;
                }
            } else {
                $airtime = Airtime::find(Request::get('airtime_id'));
            }

            $airtime->channel_id = $channel_id;
            $airtime->program_version_id = $program_version_id;
            $airtime->air_date = $air_date->format('Y-m-d H:i:s');
            $airtime->verified = 1;
            $airtime->cost = Cost::getCostByChannelAndTime($channel_id, $air_date->toDateTimeString());
            $airtime->user_id = Sentry::getUser()->id;
            $airtime->type = (Request::filled('regular_programming') and Request::get('regular_programming') == 1) ? 'R' : 'P';
            $airtime->save();

            return response()->json(
                ['error' => false],
                200
            );
        }
    }

    public function postDelete()
    {
        if (Request::filled('airtime_id')) {
            $airtime_id = Request::get('airtime_id');

            $airtime = Airtime::find($airtime_id);

            if ($airtime) {
                $airtime->delete();

                return response()->json(
                    ['error' => false],
                    200
                );
            }

            return response()->json(
                ['error' => true],
                404
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.airtimes.show')->with('airtime', Airtime::find($id));
    }

    /**
     * Get airtime info for auto populating based on channel/time
     */

    public function getShow()
    {
        $channel_id = Request::get('channel_id');
        // $language_id = Request::get('language_id');
        $airdate = Request::get('air_date');

        $airtime = Airtime::where('air_date', $airdate)
            ->where('channel_id', $channel_id)->first();

        if ($airtime) {
            return response()->json(
                [
                    'error' => false,
                    'airtime' => $airtime->toArray(),
                    'program' => $airtime->programVersion->toArray()
                ],
                200
            );
        }

        return response()->json(
            [
                'error' => false,
                'airtime' => null
            ],
            200
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $airtime = Airtime::find($id);
        $date = new \DateTime(date('Y-m-d', strtotime($airtime->air_date)));
        $count = 24 * 60 / 30;
        $times = [];
        while ($count--) {
            $newDate = $date->add(new \DateInterval("P0Y0DT0H30M"));
            $times[$newDate->format("H:i:s")] = $newDate->format("h:i a");
        }

        return view('admin.airtimes.edit')->with('airtime', $airtime)
            ->with('programs', Program::all()->pluck('grid_title', 'id')->toArray())
            ->with('times', $times);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new AirtimeValidator;

        if ($validation->passes()) {
            $airtime = Airtime::find($id);
            $airtime->channel_id = Request::get('channel_id');
            $airtime->program_id = Request::get('program_id');
            $airtime->air_date = Request::get('air_date').' '.Request::get('air_time');
            $airtime->user_id = Sentry::getUser()->id;
            $airtime->save();

            return Redirect::route('admin.airtimes.edit', $airtime->id)
                ->with('success', 'Airtime saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $airtime = Airtime::find($id);
        $airtime->delete();

        return Redirect::route('admin.airtimes.index')->with('success', 'Airtime deleted.');
    }

    public function postVerify()
    {
        $channel = Channel::find(Request::get('channel_id'));
        if (! $channel) {
            return response()->json([
                'error' => true,
                'message' => 'Channel not Found!',
            ]);
        }

        $airdate = Carbon::parse(Request::get('air_date'));

        $verify = AirtimeVerify::firstOrCreate([
            'channel_id' => $channel->id,
            'user_id' => Sentry::getUser()->id,
            'airdate' => $airdate->toDateString(),
        ]);

        $airtimes = Airtime::where('channel_id', $channel->id)
            ->where('air_date', '>=', $airdate->copy()->startOfDay()->toDateTimeString())
            ->where('air_date', '<', $airdate->copy()->endOfDay()->toDateTimeString())
            ->get();

        foreach ($airtimes as $airday) {
            $airday->verified = 1;
            $airday->save();
        }

        return response()->json($verify);
    }

    public function getData()
    {
        $channel_id = Request::get('channel_id');
        $air_date = Carbon::createFromFormat('Y-m-d', Request::get('air_date'));

        $airtimes = Airtime::select([
                'airtimes.id',
                'programs.id as program_id',
                'programs.title',
                'programs.grid_title',
                'programs.version',
                'programs.price',
                'users.email',
                'airtimes.air_date'
            ])
            ->leftJoin('programs', 'programs.id', '=', 'airtimes.program_id')
            ->leftJoin('users', 'users.id', '=', 'airtimes.user_id')
            ->whereNotNull('airtimes.program_id')
            ->where('airtimes.channel_id', $channel_id)
            ->whereBetween('air_date', [$air_date->startOfDay()->format('Y-m-d H:i:s'), $air_date->endOfDay()->format('Y-m-d H:i:s')]);

        return Datatables::of($airtimes)
            ->editColumn('title', '{{ $title }} @if(Sentry::getUser()->isSuperUser()) <small><a href="{{ route("admin.programs.edit",array($program_id)) }}" data-programid="{{ $program_id }}" target="_blank">Edit Show</a></small> @endif')
            ->editColumn('price', '{{ $price }}')
            ->remove_column('program_id')
            ->make();
    }

    public function getTable()
    {
        $channel_id = Request::get('channel_id');
        $air_date = Carbon::createFromFormat('Y-m-d H:i:s', Request::get('air_date'));
        $channel = Channel::where('id', $channel_id)->first();
        $server = Server::where('id', $channel->server_id)->first();

        $path = '/?directory='.urlencode('/capture/'.$channel->abbr.'/'.$air_date->format('Y').'/'.$air_date->format('m').'/'.$air_date->format('d'));

        $airtimes = Airtime::with('user', 'programVersion', 'programVersion.clipsters')
            ->whereNotNull('airtimes.program_version_id')
            ->where('airtimes.channel_id', $channel_id)
            ->whereBetween('air_date', [$air_date->copy()->startOfDay()->subSecond()->format('Y-m-d H:i:s'), $air_date->endOfDay()->format('Y-m-d H:i:s')])
            ->orderBy('air_date', 'desc')
            ->get();

        return view('admin.airtimes.table')
            ->with('channel', $channel)
            ->with('server', $server)
            ->with('path', $path)
            ->with('airtimes', $airtimes)
            ->with('air_date', $air_date);
    }
}
