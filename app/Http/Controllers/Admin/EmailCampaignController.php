<?php namespace App\Http\Controllers\Admin;

use App\Models\EmailCampaign;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\Spot;
use App\Models\SpotDetection;
use App\Models\SpotRanking;
use App\Models\SpotVersion;
use App\Models\UserHash;
use App\Mail\EmailCampaignMonthly;
use App\Mail\EmailCampaignWeekly;
use Carbon\Carbon;
use Mail;
use Request;
use Sentry;

use App\Http\Controllers\Controller;

class EmailCampaignController extends Controller
{
    private $emailCampaign;

    public function __construct(EmailCampaign $emailCampaign)
    {
        $this->emailCampaign = $emailCampaign;
    }

    public function index()
    {
        $emailCampaigns = EmailCampaign::orderBy('created_at', 'desc')
            ->paginate(20);

        return view('admin.email-campaigns.index', compact([
            'emailCampaigns'
        ]));
    }


    public function create()
    {
        return view('admin.email-campaigns.create');
    }

    /**@/**
     * Create a hash for every user.
     * Send each user an email containing links to the monthly report.
     * Each link has the user's hash embedded in the URL.
     * @return type
     */
    public function generateMonthlyCampaign()
    {
        $emailCampaign = EmailCampaign::find(\Request::get('emailCampaignId'));

        // Get All the users
        $users = Sentry::getUserProvider()->createModel()
            //->whereIn('id', array(2, 3, 646, 114)) // Remove this when we go live
            //->whereNotIn('id', array(217, 990)) // ID of user that wanted off the list
            ->where('email_monthly', 1)
            ->where('id', '>', $emailCampaign->last_sent)
            ->where('activated', 1)
            ->orderBy('id', 'asc')
            ->get();

        // Month
        if (Request::filled('month')) {
            $month = Carbon::createFromFormat('Y-m', Request::get('month'))->startOfMonth();
        } else {
            $month = Carbon::now()->subMonth()->startOfMonth();
        }

        foreach ($users as $user) {
            // Generate the hash
            $hash = UserHash::createHash($user, 'month', $emailCampaign->id);

            // Disable emails by default
            $sendEmail = false;

            $platinum = Sentry::findGroupByName('Platinum Subscription');
            $gold     = Sentry::findGroupByName('Gold Subscription');
            $silver   = Sentry::findGroupByName('Silver Subscription');
            $bronze   = Sentry::findGroupByName('Bronze Subscription');

            $data = [
                'hash' => $hash->hash,
                'subject' => 'Your IMS Report for the Month of '.$month->format('F Y'),
                'heading' => 'Top Monthly Rankings',
                'user' => [
                    'email' => $user->email,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name
                ]
            ];

            if ($user->inGroup($platinum)) {
                // Platinum Folks
                $data['subscription'] = 'platinum';
                $sendEmail = false;
            } elseif ($user->inGroup($gold)) {
                // Gold Folks
                $data['subscription'] = 'gold';
                $sendEmail = false;
            } elseif ($user->inGroup($silver)) {
                // Silver Folks
                $data['subscription'] = 'silver';
                $sendEmail = false;
            } elseif ($user->inGroup($bronze)) {
                // Bronze Folks
                $data['subscription'] = 'bronze';
                $sendEmail = true;
            } elseif ($user->isSuperUser()) {
                $data['subscription'] = 'bronze';
                $sendEmail = true;
            } else {
                //Everyone else... (misfits)
                $data['subscription'] = 'bronze';
                $sendEmail = true;
            }

            if ($sendEmail) {
                Mail::to($data['user']['email'], $data['user']['first_name'].' '.$data['user']['last_name'])
                    ->queue(new EmailCampaignMonthly($data));

                // Save the last ID of who this was sent to
                $emailCampaign->send_count = $emailCampaign->send_count + 1;
                $emailCampaign->last_sent = $user->id;
                $emailCampaign->save();
            }
        }

        // Mark it as finished!
        $emailCampaign->approved = 1;
        $emailCampaign->save();

        return redirect()->route('admin.email-campaigns.index')
            ->with('success', 'The montly email campaign has been sent!');
    }

    /**@/**
     * Generate a hash for platinum and gold users.
     * Send users from platinum and gold an email containing links to the weekly report.
     * Each link has the user's hash embedded in the URL.
     * @return type
     */
    public function generateWeeklyCampaign()
    {
        $emailCampaign = EmailCampaign::find(\Request::get('emailCampaignId'));

        // Get all of the users
        $users = Sentry::getUserProvider()->createModel()
            //->whereIn('id', array(2, 3, 646, 114)) // Remove this when we go live
            //->whereNotIn('id', array(217, 990)) // ID of user that wanted off the list
            ->where('email_weekly', 1)
            ->where('id', '>', $emailCampaign->last_sent)
            ->where('activated', 1)
            ->orderBy('id', 'asc')
            ->get();

        // Week
        if (Request::filled('week')) {
            $week = Carbon::createFromFormat('Y-m-d', Request::get('week'))->startOfDay();
        } else {
            $week = Carbon::now()->previous(Carbon::FRIDAY)->startOfDay();
        }

        $spots = SpotVersion::findByWeek($week);
        $shows = ProgramVersion::findByWeek($week);

        foreach ($users as $user) {
            // Generate the hash
            $hash = UserHash::createHash($user, 'week', $emailCampaign->id);

            // Disable emails by default
            $sendEmail = false;

            $platinum = Sentry::findGroupByName('Platinum Subscription');
            $gold     = Sentry::findGroupByName('Gold Subscription');
            $silver   = Sentry::findGroupByName('Silver Subscription');
            $bronze   = Sentry::findGroupByName('Bronze Subscription');

            $data = [
                'hash' => $hash->hash,
                'subject' => 'Your IMS Report for the Week of '.$week->format('F jS Y'),
                'heading' => 'Top Weekly Rankings and New Shows and Spots',
                'user' => [
                    'email' => $user->email,
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name
                ]
            ];

            if ($user->inGroup($platinum)) {
                // Platinum Folks
                $data['subscription'] = 'platinum';
                $sendEmail = true;
            } elseif ($user->inGroup($gold)) {
                // Gold Folks
                $data['subscription'] = 'gold';
                $sendEmail = true;
            } elseif ($user->inGroup($silver)) {
                // Silver Folks
                $data['subscription'] = 'silver';
                $sendEmail = true;
            } elseif ($user->inGroup($bronze)) {
                // Bronze Folks
                $data['subscription'] = 'bronze';
                $sendEmail = false;
            } elseif ($user->isSuperUser()) {
                $data['subscription'] = 'platinum';
                $sendEmail = true;
            } else {
                //Everyone else... (misfits)
                $data['subscription'] = 'none';
                $sendEmail = false;
            }

            if ($sendEmail) {
                Mail::to($data['user']['email'], $data['user']['first_name'].' '.$data['user']['last_name'])
                    ->queue(new EmailCampaignWeekly($data));

                $emailCampaign->send_count = $emailCampaign->send_count + 1;
                $emailCampaign->last_sent = $user->id;
                $emailCampaign->save();
            }
        }

        // Mark it as finished!
        $emailCampaign->approved = 1;
        $emailCampaign->save();

        return redirect()->route('admin.email-campaigns.index')
            ->with('success', 'The weekly email campaign has been sent!');
    }

    public function store()
    {
        $emailCampaign = $this->emailCampaign->createNewEmailCampaign(\Request::get('period_type'), \Request::get('notes'));

        return redirect()->route('admin.email-campaigns.index');
    }

    public function edit($id)
    {
        $emailCampaign = EmailCampaign::find($id);

        return view('admin.email-campaigns.edit', compact([
            'emailCampaign'
        ]));
    }

    public function update()
    {
        $emailCampaign = EmailCampaign::find(\Request::get('emailCampaignId'));
        $emailCampaign->notes = \Request::get('notes');
        $emailCampaign->save();

        return redirect()->route('admin.email-campaigns.index');
    }

    public function destroy($id)
    {
        $emailCampaign = EmailCampaign::find($id);

        $emailCampaign->delete();

        return redirect()->route('admin.email-campaigns.index');
    }
}
