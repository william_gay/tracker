<?php namespace App\Http\Controllers\Admin;

use App\Models\Channel;
use App\Models\Language;
use App\Models\ProgramVersion;
use App\Models\SpotAirtime;
use App\Models\SpotVersion;
use App\Models\Daypart;
use Carbon\Carbon;
use Sentry;

use App\Http\Controllers\Controller;

class MonitorController extends Controller
{
    public function __construct()
    {
        $languages = Language::whereNotIn('id', [6])->pluck('name', 'id');
        view()->share('languages', $languages);
        view()->share(
            'days',
            [
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday'
            ]
        );
    }

    /**
     * Start of Monitor Wizard
     *
     * @return Response
     */
    public function index()
    {
        if (request('language_id') && request('monitor_date')) {
            $rows = request('rows');
            $language_id = request('language_id');
            $type = request('type');
            $monitor_date = Carbon::createFromFormat('Y-m-d', request('monitor_date'));

            if ($type == 'long') {
                $channels_array = $this->getChannels($language_id, 'long');
                $channel_keys = collect();

                $times = $this->getTimes($monitor_date);
                $dayparts = collect();
                $daypart_select = collect();
                $newSpots = collect();
                $spotInputs = collect();
            } elseif ($type == 'short') {
                $channels_array = $this->getChannels($language_id);
                $channel_keys = array_keys($channels_array);

                $times = $this->getTimes($monitor_date);

                $dayparts = Daypart::where('day_start', '<=', date('N', strtotime($monitor_date->toDateTimeString())))
                    ->where('day_end', '>=', date('N', strtotime($monitor_date->toDateTimeString())))
                    ->orderBy('order', 'asc')
                    ->get();

                $daypart_select = [];
                foreach ($dayparts as $part) {
                    $daypart_select[$part->id] = $part->name .' ('.$part->abbr.') Time: '.$part->time_start.' - '.$part->time_end;
                }

                //Get Users Spots for the day
                $spotInputs = SpotAirtime::where('air_date', $monitor_date->format('Y-m-d'))
                    ->orderBy('created_at', 'desc')
                    ->get();

                //Get New Spots this week
                $newSpots = SpotVersion::with('channel')
                    ->whereNotNull('user_id')
                    ->where('initial_date', '>', lastFriday())
                    ->orderBy('initial_date', 'desc')
                    ->get();
            }

            return view(
                'admin.monitor.doit_' . $type,
                compact(
                    'rows',
                    'times',
                    'dayparts',
                    'daypart_select',
                    'newSpots',
                    'spotInputs',
                    'monitor_date',
                    'language_id',
                    'channel_keys',
                    'channels_array'
                )
            );
        }

        return view('admin.monitor.index');
    }

    private function getChannels($language_id, $type = 'short')
    {
        if ($type == 'long') {
            $channels = Channel::with('language')
                ->where('language_id', $language_id)
                ->where('info_active', 1)
                ->orderBy('name', 'asc')
                ->get();
        } else {
            $channels = Channel::with('language')
                ->where('language_id', $language_id)
                ->where(function ($query) {
                    $query->where('spot_active', 1)
                        ->orWhere('info_active', 1);
                })
                ->orderBy('name', 'asc')
                ->get();
        }

        $channels_array = [];
        foreach ($channels as $channel) {
            $channels_array[$channel->id] = $channel->name .' -- '.$channel->abbr .' ('.$channel->language->name.')';
        }

        return $channels_array;
    }

    public function getTimes($monitorDate)
    {
        if (! is_object($monitorDate)) {
            $monitorDate = Carbon::createFromFormat('Y-m-d', $monitorDate);
        }
        $date = Carbon::createFromFormat('Y-m-d H:i:s', $monitorDate->startOfDay());
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $monitorDate->endOfDay());

        while ($date->lte($end)) {
            $times[$date->format("Y-m-d H:i:s")] = $date->format("h:i a");
            $date->addMinutes(30);
        }

        return $times;
    }

    public function missingMethod($parameters = [])
    {
        abort(404);
    }
}
