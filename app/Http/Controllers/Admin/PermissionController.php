<?php namespace App\Http\Controllers\Admin;

use View;
use Redirect;
use Lang;
use App\Services\Validators\PermissionValidator;
use App\Services\PermissionProvider;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    /**
     * @var PermissionProvider
     */
    protected $permissions;

    /**
     * [__construct description]
     *
     * @param PermissionProvider $permissions
     */
    public function __construct(PermissionProvider $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * Display all the permissions
     *
     * @return Response
     */
    public function index()
    {
        $permissions = $this->permissions->all();
        $roles = $this->permissions->getRoles();

        return view('admin.permissions.index', compact('permissions', 'roles'));
    }

    /**
     * Display new permission form
     *
     *@return \Illuminate\Http\RedirectResponse
     */
    public function create()
    {
        $roles = $this->permissions->getRoles();

        return view('admin.permissions.create', compact('roles'));
    }

    /**
     * Display the edit permission form
     *
     * @param $id
     *
     * @return Response
     */
    public function edit($id)
    {
        try {
            $permission = $this->permissions->findOrFail($id);
            $roles = $this->permissions->getRoles();

            return view('admin.permissions.edit', compact('permission', 'roles'));
        } catch (ModelNotFoundException $e) {
            return Redirect::route('admin.permissions.index')->with('error', Lang::get('admin::permission.model_not_found'));
        }
    }

    /**
     * Save new permissions into the database
     *
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validation = new PermissionValidator;

        if ($validation->passes()) {
            $perm = $this->permissions->create($validation->getData());

            return Redirect::route('admin.permissions.index')->with('success', Lang::get('permissions.create_success'));
        }

        return Redirect::back()->withInput()->withErrors($validation->getErrors());
    }

    /**
     * Update permissions into the database
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        try {
            $data = Request::all();
            $validation = new PermissionValidator($data);

            if ($validation->passes()) {
                $perm = $this->permissions->update($id, $validation->getData());

                return Redirect::route('admin.permissions.index')->with('success', Lang::get('permissions.update_success'));
            }

            return Redirect::back()->withInput()->withErrors($validation->getErrors());
        } catch (ModelNotFoundException $e) {
            return Redirect::route('admin.permissions.index')->with('error', Lang::get('permissions.model_not_found'));
        }
    }

   /**
     * Delete a permission
     *
     * @param  int                               $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $eventData = $this->permissions->delete($id);

            return Redirect::route('admin.permissions.index')->with('success', Lang::get('permissions.delete_success'));
        } catch (ModelNotFoundException $e) {
            return Redirect::route('admin.permissions.index')->with('error', Lang::get('permissions.model_not_found'));
        }
    }
}
