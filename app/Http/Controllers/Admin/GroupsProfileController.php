<?php namespace App\Http\Controllers\Admin;

use Config;
use Event;
use Lang;
use Request;
use Redirect;
use Sentry;
use View;
use App\Models\Company;
use App\Models\Group;
use App\Models\GroupsProfile;
use App\Services\PermissionProvider;
use Cartalyst\Sentry\Groups\GroupNotFoundException;

use App\Http\Controllers\Controller;

class GroupsProfileController extends Controller
{
    /**
     * @var PermissionProvider
     */
    protected $permissions;

    /**
     * [__construct description]
     *
     * @param GroupRepository    $groups
     * @param PermissionProvider $permissions
     */
    public function __construct(PermissionProvider $permissions)
    {
        $this->permissions = $permissions;
    }

    /**
     * Display the group permissions
     *
     * @param  int      $groupID
     * @return Response
     */
    public function index($groupId)
    {
        try {
            $group = Sentry::getGroupProvider()->findById($groupId);
            $group_profile = GroupsProfile::where('group_id', $groupId)->first();

            // Get Subscription Groups
            $subscriptions = Group::where('name', 'like', '%Subscription%')->get()->pluck('name', 'id')->toArray();

            // Get the group permissions
            $groupPermissions = $group->getPermissions();

            $permissions = $this->permissions->all(['name','permissions']);

            $modulePerm = $this->permissions->getMergePermissions($groupPermissions, $permissions->toArray());

            $roles = [['name' => 'generic', 'permissions' => ['view','create','update','delete']]];

            $genericPerm = $this->permissions->getMergePermissions($groupPermissions, $roles);

            return view('admin.groups.profile', compact('modulePerm', 'group', 'genericPerm', 'group_profile', 'subscriptions'));
        } catch (GroupNotFoundException $e) {
            return Redirect::route('admin.groups.index')->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $groupID
     * @return Response
     */
    public function update($groupId)
    {
        try {
            $group = Sentry::getGroupProvider()->findById($groupId);
            $group->permissions = Request::get('rules');
            $group->save();

            $groups_profile = GroupsProfile::where('group_id', $groupId)->first();
            $groups_profile->data_since = Request::get('data_since');
            $groups_profile->data_to = Request::get('data_to');
            $groups_profile->subscription_id = Request::get('subscription_id');
            $groups_profile->company_id = Request::get('company_id');
            $groups_profile->save();

            return Redirect::route('admin.groups.index')->with('success', Lang::get('groups.update_success'));
        } catch (GroupNotFoundException $e) {
            return Redirect::back()->withInput()->with('error', $e->getMessage());
        }
    }
}
