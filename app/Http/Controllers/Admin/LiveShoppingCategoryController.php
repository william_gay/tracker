<?php namespace App\Http\Controllers\Admin;

use App\Models\LiveShoppingTracker\LiveShoppingCategory;
use App\Services\Validators\CategoryValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class LiveShoppingCategoryController extends Controller
{
    public function __construct()
    {
        view()->share('pcats', ['0' => 'Primary Category'] + LiveShoppingCategory::whereNull('parent_id')->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.lst.categories.index')->with('categories', LiveShoppingCategory::paginate(20));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.lst.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = new CategoryValidator;

        if ($validation->passes()) {
            $category = new LiveShoppingCategory;
            $category->parent_id = ($request->get('parent_id') == 0)? null : $request->get('parent_id');
            $category->name = $request->get('name');
            $category->description = $request->get('description');
            $category->user_id = Sentry::getUser()->id;
            $category->save();

            return redirect()->route('admin.lst.categories.edit', $category->id)->with('success', 'The category '.$category->name.' was saved.');
        }

        return redirect()->back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.lst.categories.show')->with('category', LiveShoppingCategory::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.lst.categories.edit')->with('category', LiveShoppingCategory::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = new CategoryValidator;

        if ($validation->passes()) {
            $category = LiveShoppingCategory::find($id);
            $category->parent_id = ($request->get('parent_id') == 0)? null : $request->get('parent_id');
            $category->name = $request->get('name');
            $category->description = $request->get('description');
            $category->user_id = Sentry::getUser()->id;
            $category->save();

            return redirect()->route('admin.lst.categories.edit', $category->id)->with('success', 'The category '.$category->name.' was updated.');
        }

        return redirect()->back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = LiveShoppingCategory::find($id);
        $category->delete();

        return redirect()->route('admin.lst.categories.index')->with('success', 'The category '.$category->name.' was deleted.');
    }

    public function getDatatables()
    {
        $categories = LiveShoppingCategory::select(
            'live_shopping_categories.id as category_id',
            'parent_category.name as parent_category_name',
            'live_shopping_categories.name as category_name',
            DB::raw('DATE_FORMAT(live_shopping_categories.created_at, "%Y-%m-%d") as created_at'),
            'live_shopping_categories.id'
        )
            ->leftJoin('live_shopping_categories as parent_category', 'parent_category.id', '=', 'live_shopping_categories.parent_id');

        return Datatables::of($categories)
            ->editColumn('id', '<a href="{{ route("admin.lst.categories.edit", array($id)) }}" class="btn btn-sm btn-default"><i class="fa fa-edit"></i></a>
                @if(Sentry::getUser()->isSuperUser())<a href="{{ route("admin.lst.categories.destroy", array($id)) }}" data-method="delete" data-modal-text="Delete this Category?" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>@endif')
            ->rawColumns(['id'])
            ->make();
    }
}
