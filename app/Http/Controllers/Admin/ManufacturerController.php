<?php namespace App\Http\Controllers\Admin;

use App\Models\Manufacturer;
use App\Services\Validators\ManufacturerValidator;
use Aws\S3\Enum\CannedAcl;
use App;
use DataTables;
use Event;
use File;
use Redirect;
use Sentry;
use View;

use App\Http\Controllers\Controller;

class ManufacturerController extends Controller
{
    /**
     * [__construct description]
     *
     * @param PermissionProvider $permissions
     */
    public function __construct()
    {
        // Do we need this?
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.manufacturers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.manufacturers.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $manufacturer = Manufacturer::find($id);

        return view('admin.manufacturers.edit')
            ->with('manufacturer', $manufacturer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new ManufacturerValidator;

        if ($validation->passes()) {
            $manufacturer = new Manufacturer;
            $manufacturer->name = Request::get('name');
            $manufacturer->slug = Request::get('slug');
            $manufacturer->gln = Request::get('gln');
            $manufacturer->upc_prefix = Request::get('upc_prefix');
            $manufacturer->gs1_prefix = Request::get('gs1_prefix');
            $manufacturer->website = Request::get('website');
            $manufacturer->user_id = Sentry::getUser()->id;

            $manufacturer->save();

            return Redirect::route('admin.manufacturers.edit', $manufacturer->id)
                ->with('success', 'The manufacturer '.$manufacturer->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new ManufacturerValidator;

        if ($validation->passes()) {
            $manufacturer = Manufacturer::find($id);
            $manufacturer->name = Request::get('name');
            $manufacturer->slug = Request::get('slug');
            $manufacturer->gln = Request::get('gln');
            $manufacturer->upc_prefix = Request::get('upc_prefix');
            $manufacturer->gs1_prefix = Request::get('gs1_prefix');
            $manufacturer->website = Request::get('website');
            $manufacturer->user_id = Sentry::getUser()->id;

            $manufacturer->save();

            return Redirect::route('admin.manufacturers.edit', $manufacturer->id)
                ->with('success', 'The manufacturer '.$manufacturer->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $manufacturer = Manufacturer::find($id);
        $manufacturer->delete();

        return Redirect::route('admin.manufacturers.index')->with('success', 'The manufacturer '.$manufacturer->name.' was deleted.');
    }

    /**
     * Return json array of Datatables Object
     *
     * @return Datatables
     */
    public function getData()
    {
        $manufacturers = Manufacturer::select('name', 'slug', 'created_at', 'updated_at', 'id')
            ->orderBy('name');

        return Datatables::of($manufacturers)
            ->editColumn('id', '
                <a href="{{ route("admin.manufacturers.edit", array($id)) }}"
                    class="btn btn-default" rel="tooltip" title="Edit manufacturer">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{ route("admin.manufacturers.destroy", array($id)) }}"
                    class="btn btn-danger" rel="tooltip" title="Delete Manufacturer" data-method="delete"
                    data-modal-text="delete this manufacturer?">
                    <i class="fa fa-trash-o"></i>
                </a>
            ')
            ->make();
    }
}
