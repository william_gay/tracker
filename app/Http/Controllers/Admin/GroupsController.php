<?php namespace App\Http\Controllers\Admin;

use Request;
use Redirect;
use Lang;
use Sentry;
use Event;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Carbon\Carbon;
use Cartalyst\Sentry\Groups\NameRequiredException;
use Cartalyst\Sentry\Groups\GroupExistsException;
use Cartalyst\Sentry\Groups\GroupNotFoundException;
use App\Events\GroupCreated;
use App\Events\GroupDeleted;
use App\Models\Group;

use App\Http\Controllers\Controller;

class GroupsController extends Controller
{
    /**
     * Display all the groups
     *
     * @return Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $groups = Group::select([
                'groups.name', 'groups.created_at', 'groups.updated_at', 'groups.id'
            ]);

            return Datatables::of($groups)
                ->editColumn('id', '
                    <a href="{{ route("admin.groups.edit", array($id)) }}"
                        class="btn btn-default" rel="tooltip" title="Edit Group">
                        <i class="fa fa-edit"></i>
                    </a>
                    @if(strpos($name,"Subscription") !== false)
                    <a href="{{ route("admin.groups.permissions", array($id)) }}"
                        class="btn btn-default" rel="tooltip" title="Edit Group Permissions">
                        Permissions <i class="fa fa-arrow-right"></i>
                    </a>
                    @else
                    <a href="{{ route("admin.groups.profile", array($id)) }}"
                        class="btn btn-default" rel="tooltip" title="Edit Group Profile">
                        Profile <i class="fa fa-arrow-right"></i>
                    </a>
                    @endif
                    <a href="{{ route("admin.groups.destroy", array($id)) }}"
                        class="btn btn-danger" rel="tooltip" title="Delete Group" data-method="delete"
                        data-modal-text="delete this group?">
                        <i class="fa fa-trash-o"></i>
                    </a>
                ')
                ->rawColumns(['id'])
                ->toJson();
        }

        $html = $builder->columns([
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'created_at'],
            ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At'],
            ['data' => 'id', 'name' => 'id', 'title' => 'Id'],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() { laravel.initialize(); }',
        ]);

        return view('admin.groups.index')
            ->with('html', $html);
    }

    /**
     * Display create a new group form
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.groups.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit($id)
    {
        try {
            $group = Sentry::getGroupProvider()->findById($id);

            return view('admin.groups.edit', compact('group'));
        } catch (GroupNotFoundException $e) {
            return Redirect::route('admin.groups.index')->with('error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        try {
            $group = Sentry::getGroupProvider()->create(Request::only('name'));

            event(new GroupCreated($group, Carbon::now()));

            return Redirect::route('admin.groups.index')->with('success', Lang::get('groups.create_success'));
        } catch (NameRequiredException $e) {
            return Redirect::back()->withInput()->with('error', $e->getMessage());
        } catch (GroupExistsException $e) {
            return Redirect::back()->withInput()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     */
    public function update($id)
    {
        try {
            $group = Sentry::getGroupProvider()->findById($id);
            $group->name = Request::get('name');
            $group->save();

            return Redirect::route('admin.groups.index')->with('success', Lang::get('groups.update_success'));
        } catch (GroupNotFoundException $e) {
            return Redirect::back()->withInput()->with('error', $e->getMessage());
        } catch (GroupExistsException $e) {
            return Redirect::back()->withInput()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return Response
     */
    public function destroy($id)
    {
        try {
            $group = Sentry::getGroupProvider()->findById($id);
            $eventData = $group;
            $group->delete();
            event(new GroupDeleted($group));

            return Redirect::route('admin.groups.index')->with('success', Lang::get('groups.delete_success'));
        } catch (GroupNotFoundException $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }
}
