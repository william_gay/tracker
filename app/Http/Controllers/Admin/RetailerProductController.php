<?php namespace App\Http\Controllers\Admin;

use Illuminate\Support\Arr;
use App\Models\Category;
use App\Models\Product;
use App\Models\Retail\Retailer;
use App\Models\Retail\RetailerProduct;
use App\Services\Validators\RetailerProductValidator;
use Aws\S3\Enum\CannedAcl;
use Carbon\Carbon;
use DataTables;
use Event;
use Redirect;
use Sentry;
use View;
use League\Csv\Reader;

use App\Http\Controllers\Controller;

class RetailerProductController extends Controller
{
    /**
     * [__construct description]
     *
     * @param PermissionProvider $permissions
     */
    public function __construct()
    {
        View::share('categories', Category::whereNull('parent_id')->orderBy('name')->pluck('name', 'id')->toArray());
        View::share('retailers', Retailer::orderBy('name')->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $retailer_products = RetailerProduct::select(
            'products.name as product_name',
            'products.id as product_id',
            'products.slug as product_slug',
            'retailers.name as retailer_name',
            'retailer_products.website as retailer_website',
            'categories.name as category_name',
            'min_price',
            'max_price',
            'retailer_products.id'
        )
            ->leftJoin('products', 'retailer_products.product_id', '=', 'products.id')
            ->leftJoin('retailers', 'retailer_products.retailer_id', '=', 'retailers.id')
            ->leftJoin('categories', 'products.category_id', '=', 'categories.id')
            ->orderBy('products.name')
            ->paginate();

        return view('admin.retailer-products.index', compact(
            'retailer_products'
        ));
    }

    public function importRetailers()
    {
        return view('admin.retailers.import');
    }

    public function postImportRetailCsv()
    {
        $file = Request::file('csv');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();

            if ($extension != 'csv') {
                return Redirect::route('admin.retailers.import')
                    ->withInput()
                    ->with(['error' => 'File type invalid, please upload a csv file!']);
            }

            $destination = storage_path().'/uploads/retail-data';
            $upload_success = $file->move($destination, $filename);
            $newFile = $destination.'/'.$filename;

            if ($upload_success) {
                //TODO Write the logic that imports this data
                $this->processRetailCSV($newFile);
            }
        } else {
            return Redirect::route('admin.retailers.import')
                ->withInput()
                ->with(['error' => 'File type invalid, please upload a csv file!']);
        }
    }

    private function processRetailCSV($file)
    {
        $reader = new Reader($file);

        $header = $reader->fetchOne(0);
        $header = Arr::where($header, function ($key, $value) {
            if ($value === 'Product' or strlen($value) == 0) {
                return false;
            }
            return true;
        });

        var_dump($header);

        $data = $reader
            ->setOffset(1)
            ->fetchAll();

        //var_dump($data);

        foreach ($data as $row) {
            if (strlen($row[0]) == 0) {
                var_dump($row);

                $exists = Retailer::where('name', $row)->first();

                if (! $exists) {
                    $retailer = new Retailer;
                    $retailer->name = $row[2];
                    $retailer->store_count = $row[3];
                    $retailer->website = $row[4];
                    $retailer->visible = 1;
                    $retailer->user_id = 2;
                    $retailer->save();
                }
            }
        }
    }

    public function importCsv()
    {
        return view('admin.retailer-products.import');
    }

    public function postImportCsv()
    {
        $file = Request::file('csv');
        if ($file) {
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();

            if ($extension != 'csv') {
                return Redirect::route('admin.retailer-products.import')
                    ->withInput()
                    ->with(['error' => 'File type invalid, please upload a csv file!']);
            }

            $destination = storage_path().'/uploads/retail-data';
            $upload_success = $file->move($destination, $filename);
            $newFile = $destination.'/'.$filename;

            if ($upload_success) {
                //TODO Write the logic that imports this data
                $this->processCSV($newFile, Request::get('date_recorded'));
            }
        }
    }

    private function processCSV($newFile, $date_recorded)
    {
        $reader = new Reader($newFile);

        $header = $reader->fetchOne(0);
        $header = Arr::where($header, function ($key, $value) {
            if ($value === 'Product' or strlen($value) == 0) {
                return false;
            }
            return true;
        });

        //var_dump($header);

        $data = $reader
            ->setOffset(1)
            ->fetchAll();

        $products = [];
        $count = 0;
        foreach ($data as $row) {
            $stores = [];
            //echo "Running loop count: ".$count, "\n";
            // Before Empty items are filtered out
            // var_dump($row);
            $row = Arr::where($row, function ($key, $value) {
                if (strlen($value) == 0 or trim($value) == '') {
                    return false;
                }
                return true;
            });

            foreach ($row as $colIdx => $colVal) {
                //var_dump($row);
                if (strlen(trim($colVal)) > 0 and $colIdx != 0) {
                    //var_dump($colIdx);
                    if ($header[$colIdx] != 'URL') {
                        $prices = $this->getPrices($row[$colIdx]);

                        $stores[][$header[$colIdx]] = [
                            'min_price' => $prices['min_price'],
                            'max_price' => $prices['max_price'],
                            'website' => array_key_exists($colIdx + 1, $row) ? $row[$colIdx + 1] : null
                        ];
                    }
                }
            }

            //var_dump($stores);

            //echo "After row forloop", "\n";
            //var_dump($row);

            if (count($row) > 0) {
                $products[$row[0]] = $stores;
                echo "<hr />";
                echo "<h1>".$row[0]."</h1>";
                var_dump($stores);
            }

            $count ++;
        }

        $this->insertProductsFromCsv($products, $date_recorded);
    }

    private function getPrices($priceColumn)
    {
        $prices = array_map('trim', explode('-', $priceColumn));
        $prices = str_replace('$', '', $prices);

        if (count($prices) > 1) {
            return [
                'min_price' => trim($prices[0]),
                'max_price' => trim($prices[1])
            ];
        } else {
            return [
                'min_price' => trim($prices[0]),
                'max_price' => trim($prices[0])
            ];
        }
    }

    private function insertProductsFromCsv($products, $date_recorded)
    {
        foreach ($products as $productKey => $productValue) {
            $product = Product::firstOrCreate(['name' => $productKey]);

            if ($product) {
                foreach ($productValue as $retailer) {
                    foreach ($retailer as $retailerKey => $retailerValue) {
                        $retailer = Retailer::firstOrCreate(['name' => $retailerKey]);

                        if ($retailer) {
                            $insert = new RetailerProduct;
                            $insert->product_id = $product->id;
                            $insert->retailer_id = $retailer->id;
                            $insert->date_recorded = $date_recorded;
                            $insert->min_price = $retailerValue['min_price'];
                            $insert->max_price = $retailerValue['max_price'];
                            $insert->website = $retailerValue['website'];
                            $insert->save();
                        } else {
                            echo "  Cannot Find Retailer! ". $retailerKey, "<br>";
                        }
                    }
                }
            } else {
                echo "Cannot Find Product! ". $productKey, "<br>";
            }
        }

        return Redirect::route('admin.retailer-products.import')
            ->with(['success' => 'Retailer Product data imported!']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.retailer-products.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $retailer_product = RetailerProduct::with('retailer')->findOrFail($id);

        return view('admin.retailer-products.edit')
            ->with('retailer_product', $retailer_product)
            ->with(
                'subcats',
                Category::where('parent_id', $retailer_product->category_id)->pluck('name', 'id')->toArray()
            );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new RetailerProductValidator;

        if ($validation->passes()) {
            $retailer_product = new RetailerProduct;
            $retailer_product->retailer_id = Request::get('retailer_id');
            $retailer_product->product_id = Request::get('product_id');
            $retailer_product->date_recorded = Request::get('date_recorded');
            $retailer_product->min_price = Request::get('min_price');
            $retailer_product->max_price = Request::get('max_price') ?: Request::get('min_price');
            $retailer_product->website = Request::get('website');
            $retailer_product->general_configuration = Request::get('general_configuration');
            $retailer_product->attributes = Request::get('attributes');
            $retailer_product->user_id = Sentry::getUser()->id;
            $retailer_product->save();

            return Redirect::route('admin.retailer-products.edit', $retailer_product->id)
                ->with('success', 'The retailer product '.$retailer_product->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new RetailerProductValidator;

        if ($validation->passes()) {
            $retailer_product = RetailerProduct::find($id);
            $retailer_product->retailer_id = Request::get('select_retailer_id');
            $retailer_product->product_id = Request::get('product_id');
            $retailer_product->date_recorded = Request::get('date_recorded');
            $retailer_product->min_price = Request::get('min_price');
            $retailer_product->max_price = Request::get('max_price') ?: Request::get('min_price');
            $retailer_product->website = Request::get('website');
            $retailer_product->general_configuration = Request::get('general_configuration');
            $retailer_product->attributes = Request::get('attributes');
            $retailer_product->user_id = Sentry::getUser()->id;
            $retailer_product->save();

            return Redirect::route('admin.retailer-products.edit', $retailer_product->id)
                ->with('success', 'The retailer product '.$retailer_product->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $retailer_product = RetailerProduct::find($id);
        $retailer_product->delete();

        return Redirect::route('admin.retailer-products.index')
            ->with('success', 'The retailer product '.$retailer_product->name.' was deleted.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function getDestroy($id)
    {
        $retailer_product = RetailerProduct::find($id);
        $retailer_product->delete();

        return view('admin.retailer-products.dd');
    }

    /**
     * Return json array of Datatables Object
     *
     * @return Datatables
     */
    public function getData()
    {
        $retailers = RetailerProduct::select(
            'products.name as product_name',
            'retailers.name as retailer_name',
            'retailer_products.website as retailer_website',
            'min_price',
            'max_price',
            'retailer_products.id'
        )
            ->leftJoin('products', 'retailer_products.product_id', '=', 'products.id')
            ->leftJoin('retailers', 'retailer_products.retailer_id', '=', 'retailers.id')
            ->orderBy('products.name');

        return Datatables::of($retailers)
            ->editColumn('retailer_website', '
                <a href="{{$retailer_website}}" target="_blank">{{ $retailer_website }}</a>
            ')
            ->editColumn('min_price', '${{ number_format($min_price,2) }}')
            ->editColumn('max_price', '${{ number_format($max_price,2) }}')
            ->editColumn('id', '
                <a href="{{ route("admin.retailer-products.edit", array($id)) }}"
                    class="btn btn-default" rel="tooltip" title="Edit Retailer Product">
                    <i class="fa fa-pencil-square-o"></i>
                </a>
                <a href="{{ route("admin.retailer-products.destroy", array($id)) }}"
                    class="btn btn-danger" rel="tooltip" title="Delete Group" data-method="delete"
                    data-modal-text="delete this retailer product?">
                    <i class="fa fa-trash-o"></i>
                </a>
            ')
            ->make();
    }
}
