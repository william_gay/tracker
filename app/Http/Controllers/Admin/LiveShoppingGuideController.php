<?php namespace App\Http\Controllers\Admin;

use Config;
use Illuminate\Support\Facades\DB;
use Redirect;
use Request;
use Sentry;
use View;
use Carbon\Carbon;
use App\Models\Channel;
use App\Models\LiveShoppingTracker\LiveShoppingGuide;
use App\Services\Validators\LiveShoppingGuideValidator;

use App\Http\Controllers\Controller;

class LiveShoppingGuideController extends Controller
{
    public function __construct()
    {
        View::share('channels', Channel::orderBy('name')->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.lst.guides.index')->with('guides', LiveShoppingGuide::paginate(20));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.lst.guides.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new LiveShoppingGuideValidator;

        if ($validation->passes()) {
            $guide = new LiveShoppingGuide;
            $guide->channel_id = (Request::get('channel_id') == 0)? null : Request::get('channel_id');
            $guide->name = Request::get('name');
            $guide->url = Request::get('url');
            $guide->user_id = Sentry::getUser()->id;
            $guide->save();

            return Redirect::route('admin.lst.guides.edit', $guide->id)->with('success', 'The guides '.$guide->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.lst.guides.show')->with('guide', LiveShoppingGuide::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.lst.guides.edit')->with('guide', LiveShoppingGuide::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new LiveShoppingGuideValidator;

        if ($validation->passes()) {
            $guide = LiveShoppingGuide::find($id);
            $guide->channel_id = (Request::get('channel_id') == 0)? null : Request::get('channel_id');
            $guide->name = Request::get('name');
            $guide->url = Request::get('url');
            $guide->user_id = Sentry::getUser()->id;
            $guide->save();

            return Redirect::route('admin.lst.guides.edit', $guide->id)->with('success', 'The guide '.$guide->name.' was updated.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $guide = LiveShoppingGuide::find($id);
        $guide->delete();

        return Redirect::route('admin.lst.guides.index')->with('success', 'The guide '.$guide->name.' was deleted.');
    }
}
