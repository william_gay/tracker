<?php namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Language;
use App\Models\Retail\Retailer;
use App\Models\Retail\RetailerProduct;
use App\Services\Validators\RetailerValidator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Event;
use File;
use Redirect;
use Sentry;
use View;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RetailerController extends Controller
{
    /**
     * [__construct description]
     *
     * @param PermissionProvider $permissions
     */
    public function __construct()
    {
        View::share('categories', Category::whereNull('parent_id')->orderBy('name')->pluck('name', 'id')->toArray());
        View::share('languages', Language::all()->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $retailers = Retailer::get();

            return Datatables::of($retailers)
                ->editColumn('id', '
                    <a href="{{ route("admin.retailers.edit", array($id)) }}"
                        class="btn btn-default" rel="tooltip" title="Edit Retailer">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route("admin.retailers.destroy", array($id)) }}"
                        class="btn btn-danger" rel="tooltip" title="Delete Retailer" data-method="delete"
                        data-modal-text="delete this retailer?">
                        <i class="fa fa-trash-o"></i>
                    </a>
                ')
                ->rawColumns(['id'])
                ->toJson();
        }

        $html = $builder->columns([
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'slug', 'name' => 'slug', 'title' => 'Slug'],
            ['data' => 'sqft', 'name' => 'sqft', 'title' => 'sqft'],
            ['data' => 'store_count', 'name' => 'store_count', 'title' => 'store_count'],
            ['data' => 'visible', 'name' => 'visible', 'title' => 'visible'],
            ['data' => 'id', 'name' => 'id', 'title' => 'id'],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() { laravel.initialize(); }',
        ]);

        return view('admin.retailers.index')
            ->with('html', $html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.retailers.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.retailers.edit')
            ->with('retailer', Retailer::find($id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = new RetailerValidator;

        $visible = $this->isVisible($request);

        if ($validation->passes()) {
            $retailer = new Retailer;
            $retailer->name = $request->get('name');
            $retailer->slug = $request->get('slug');
            $retailer->website = $request->get('website');
            $retailer->sqft = $request->get('sqft');
            $retailer->store_count = $request->get('store_count');
            $retailer->notes = $request->get('notes');
            $retailer->visible = $visible;
            $retailer->user_id = Sentry::getUser()->id;

            $this->s3Magic($request, $retailer);

            $retailer->save();

            return Redirect::route('admin.retailers.edit', $retailer->id)
                ->with('success', 'The retailer '.$retailer->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = new RetailerValidator;

        $visible = $this->isVisible($request);

        if ($validation->passes()) {
            $retailer = Retailer::find($id);
            $retailer->name = $request->get('name');
            $retailer->slug = $request->get('slug');
            $retailer->website = $request->get('website');
            $retailer->sqft = $request->get('sqft');
            $retailer->store_count = $request->get('store_count');
            $retailer->notes = $request->get('notes');
            $retailer->visible = $visible;
            $retailer->user_id = Sentry::getUser()->id;

            $this->s3Magic($request, $retailer);

            $retailer->save();


            return Redirect::route('admin.retailers.edit', $retailer->id)
                ->with('success', 'The retailer '.$retailer->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $retailer_products = RetailerProduct::where('retailer_id', $id)->delete();

        $retailer = Retailer::find($id);
        $retailer->delete();

        return Redirect::route('admin.retailers.index')
            ->with('success', 'The retailer '.$retailer->name.' was deleted.');
    }

    private function s3Magic($request, $retailer)
    {
        if ($request->hasFile('retailer_logo')) {
            $path = $request->retailer_logo
                ->storeAs('retailers/'.$retailer->id, 'logo.png', 's3-logos');

            $retailer->logo_location = $path;
        }
    }

    public function isVisible($request)
    {
        return $request->filled('visible') ? 1 : 0;
    }
}
