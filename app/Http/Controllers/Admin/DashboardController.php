<?php namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use Lang;
use Sentry;
use Carbon\Carbon;
use App\Models\UserCurrent;
use App\Models\SiteAnalytics;
use App\Events\UserLogin;
use App\Events\UserLogout;
use Cartalyst\Sentry\Users\UserNotFoundException;
use Cartalyst\Sentry\Users\LoginRequiredException;
use Cartalyst\Sentry\Users\PasswordRequiredException;
use Cartalyst\Sentry\Users\WrongPasswordException;
use Cartalyst\Sentry\Users\UserNotActivatedException;
use Cartalyst\Sentry\Throttling\UserSuspendedException;
use Cartalyst\Sentry\Throttling\UserBannedException;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Show the dashboard
     *
     * @return Response
     */
    public function index()
    {
        $lastFriday = new Carbon('last friday');
        $reports = $this->getPopularReports();
        $weekReports = $this->getPopularReportsByWeek($lastFriday->toDateTimeString());
        $users = $this->getPopularReportsUsers();
        $weekUsers = $this->getPopularReportsUsersByWeek($lastFriday->toDateTimeString());
        $programs = $this->getPopularPrograms();
        $spots = $this->getPopularSpots();

        return view('admin.dashboard.index')
            ->with('reports', $reports)
            ->with('weekReports', $weekReports)
            ->with('users', $users)
            ->with('weekUsers', $weekUsers)
            ->with('programs', $programs)
            ->with('spots', $spots);
    }

    /**
     * Show the login form
     *
     * @return Response
     */
    public function getLogin()
    {
        $login_attribute = config('cartalyst/sentry::users.login_attribute');

        return view('admin.dashboard.login', compact('login_attribute'));
    }

    /**
     * Logs out the current user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        if (Sentry::check()) {
            $user = Sentry::getUser();
            Sentry::logout();

            event(new UserLogout($user));

            return redirect()->route('admin.login')->with('success', Lang::get('users.logout'));
        }

        return redirect()->route('admin.login');
    }

    /**
     * Authenticate the user
     *
     * @return Response
     */
    public function postLogin(Request $request)
    {
        try {
            $remember = $request->get('remember_me', false);
            $userdata = [
                config('cartalyst/sentry::users.login_attribute') => $request->get('login_attribute'),
                'password' => $request->get('password')
            ];

            $user = Sentry::authenticate($userdata, $remember);

            event(new UserLogin($user, $request, 'admin'));

            return redirect()->intended('admin')->with('success', Lang::get('users.login_success'));
        } catch (LoginRequiredException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (PasswordRequiredException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (WrongPasswordException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (UserNotActivatedException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (UserNotFoundException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (UserSuspendedException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        } catch (UserBannedException $e) {
            return redirect()->back()->withInput()->with('login_error', $e->getMessage());
        }
    }

    private function getPopularPrograms()
    {
        $reports = SiteAnalytics::select([
                'program_versions.title',
                'program_versions.id as program_version_id',
                DB::raw('COUNT(DISTINCT site_analytics.user_id) as users'),
                DB::raw('COUNT(site_analytics.id) as views')
            ])
            ->leftJoin('program_versions', 'program_versions.id', '=', 'site_analytics.type_id')
            ->where('site_analytics.type', 'program')
            ->orderBy('views', 'desc')
            ->groupBy('type_id')
            ->take('10')
            ->get();

        return $reports;
    }

    private function getPopularSpots()
    {
        $reports = SiteAnalytics::select([
                'spot_versions.title',
                'spot_versions.id as spot_version_id',
                DB::raw('COUNT(DISTINCT site_analytics.user_id) as users'),
                DB::raw('COUNT(site_analytics.id) as views')
            ])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'site_analytics.type_id')
            ->where('site_analytics.type', 'spot')
            ->orderBy('views', 'desc')
            ->groupBy('type_id')
            ->take('10')
            ->get();

        return $reports;
    }

    private function getPopularReportsUsers()
    {
        $reports = SiteAnalytics::select([
                'users.first_name',
                'users.last_name',
                'users.email',
                DB::raw('COUNT(site_analytics.id) as views')
            ])
            ->leftJoin('users', 'users.id', '=', 'site_analytics.user_id')
            ->where('site_analytics.type', 'report')
            ->orderBy('views', 'desc')
            ->groupBy('user_id')
            ->take('10')
            ->get();

        return $reports;
    }

    private function getPopularReportsUsersByWeek($date_time)
    {
        $reports = SiteAnalytics::select([
                'users.first_name',
                'users.last_name',
                'users.email',
                DB::raw('COUNT(site_analytics.id) as views')
            ])
            ->leftJoin('users', 'users.id', '=', 'site_analytics.user_id')
            ->where('site_analytics.type', 'report')
            ->where('site_analytics.created_at', '>', $date_time)
            ->orderBy('views', 'desc')
            ->groupBy('user_id')
            ->take('10')
            ->get();

        return $reports;
    }

    private function getPopularReports()
    {
        $reports = SiteAnalytics::select([
                'reports.name',
                'reports.id as report_id',
                DB::raw('COUNT(DISTINCT site_analytics.user_id) as users'),
                DB::raw('COUNT(site_analytics.id) as views')
            ])
            ->leftJoin('reports', 'reports.id', '=', 'site_analytics.type_id')
            ->where('site_analytics.type', 'report')
            ->orderBy('views', 'desc')
            ->groupBy('type_id')
            ->take('10')
            ->get();

        return $reports;
    }

    private function getPopularReportsByWeek($date_time)
    {
        $reports = SiteAnalytics::select([
                'reports.name',
                'reports.id as report_id',
                DB::raw('COUNT(DISTINCT site_analytics.user_id) as users'),
                DB::raw('COUNT(site_analytics.id) as views')
            ])
            ->leftJoin('reports', 'reports.id', '=', 'site_analytics.type_id')
            ->where('site_analytics.type', 'report')
            ->where('site_analytics.created_at', '>', $date_time)
            ->orderBy('views', 'desc')
            ->groupBy('type_id')
            ->take('10')
            ->get();

        return $reports;
    }
}
