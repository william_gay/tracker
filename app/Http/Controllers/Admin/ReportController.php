<?php namespace App\Http\Controllers\Admin;

use Sentry;
use App\Models\Language;
use App\Models\Permission;
use App\Models\Report;
use App\Services\PermissionProvider;
use App\Services\Validators\ReportValidator;
use DataTables;
use Yajra\DataTables\Html\Builder;

use App\Http\Controllers\Controller;

class ReportController extends Controller
{
    /**
     * @var PermissionProvider
     */
    protected $permissions;

    /**
     * [__construct description]
     *
     * @param PermissionProvider $permissions
     */
    public function __construct(PermissionProvider $permissions)
    {
        $this->permissions = $permissions;

        view()->share('types', [
            'long' => 'Long Form',
            'short' => 'Short Form'
        ]);

        view()->share('intervals', [
            'daily' => 'Daily',
            'weekly' => 'Weekly',
            'monthly' => 'Monthly',
            'yearly' => 'Yearly'
        ]);

        view()->share('format', [
            'grid' => 'Grid',
            'graph' => 'Graph'
        ]);

        view()->share('templates', [
            'media-index-ranking' => 'Media Index Ranking',
            'frequency-ranking' => 'Frequency Index Ranking',
            'dr-spots' => 'DR Spots',
            'spot-ranking' => 'Spot Ranking',
            'spot-dr-ranking' => 'Spot DR Ranking',
            'top-rankings' => 'Top Rankings',
            'top-rankings-frequency' => 'Top Rankings Frequency',
            'top-spot-rankings' => 'Top Spot Rankings',
            'spanish-frequency' => 'Spanish Frequency',
            'spanish-spot-frequency' => 'Spanish Spot Frequency',
            'spanish-spot-frequency-monthly' => 'Spanish Spot Monthly Frequency',
            'new-product-rollout' => 'New Product Rollout',
            'category-report' => 'Category Report',
            'national-infomercial-grids' => 'National Infomercial Grids',
            'top-25-ranking' => 'Free Top 25 Ranking'
        ]);

        view()->share('languages', Language::all()->pluck('name', 'id')->toArray());
        view()->share('active', [0 => 'Inactive', 1 => 'Active']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $reports = Report::with(['language']);

            return Datatables::of($reports)
                ->addColumn('actions', '
                    <a href="{{ route("admin.reports.edit", array($id)) }}"
                        class="btn btn-default" rel="tooltip" title="Edit Report">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route("admin.reports.destroy", array($id)) }}"
                        class="btn btn-danger" rel="tooltip" title="Delete Report" data-method="delete"
                        data-modal-text="delete this report?">
                        <i class="fa fa-trash-o"></i>
                    </a>
                ')
                ->rawColumns(['actions'])
                ->toJson();
        }

        $html = $builder->columns([
            ['data' => 'name', 'name' => 'name', 'title' => 'Name'],
            ['data' => 'slug', 'name' => 'slug', 'title' => 'Slug'],
            ['data' => 'type', 'name' => 'type', 'title' => 'type'],
            ['data' => 'format', 'name' => 'format', 'title' => 'Format'],
            ['data' => 'language.name', 'name' => 'language.name', 'title' => 'Language'],
            ['data' => 'active', 'name' => 'active', 'title' => 'Active'],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'created_at'],
            ['data' => 'updated_at', 'name' => 'updated_at', 'title' => 'Updated At'],
            ['data' => 'actions', 'name' => 'actions', 'title' => 'actions'],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() { laravel.initialize(); }',
        ]);

        return view('admin.reports.index')
            ->with('html', $html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.reports.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new ReportValidator;

        if ($validation->passes()) {
            $report = new Report;
            $report->name = request()->get('name');
            $report->slug = request()->get('slug');
            $report->template = request()->get('template');
            $report->type = request()->get('type');
            $report->format = request()->get('format');
            $report->interval = request()->get('interval');
            $report->js = request()->get('js');
            $report->limit = request()->get('limit');
            $report->active = request()->get('active');
            $report->language_id = request()->get('language_id');
            $report->user_id = Sentry::getUser()->id;
            $report->save();

            // Add or update available Report permissions
            $this->updateAvailablePermissions();

            return redirect()->route('admin.reports.edit', $report->id)->with('success', 'The report '.$report->name.' was saved.');
        }

        return redirect()->back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.reports.edit')->with('report', Report::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $validation = new ReportValidator;

        if ($validation->passes()) {
            $report = Report::find($id);
            $report->name = request()->get('name');
            $report->slug = request()->get('slug');
            $report->template = request()->get('template');
            $report->type = request()->get('type');
            $report->format = request()->get('format');
            $report->interval = request()->get('interval');
            $report->js = request()->get('js');
            $report->limit = request()->get('limit');
            $report->active = request()->get('active');
            $report->language_id = request()->get('language_id');
            $report->user_id = Sentry::getUser()->id;
            $report->save();

            // Add or update available Report permissions
            $this->updateAvailablePermissions();

            return redirect()->route('admin.reports.edit', $report->id)->with('success', 'The report '.$report->name.' was saved.');
        }

        return redirect()->back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $report = Report::find($id);
        $report->delete();

        return redirect()->route('admin.reports.index')->with('success', 'The report '.$report->name.' was deleted.');
    }

    private function updateAvailablePermissions()
    {
        $permission = Permission::where('name', 'Reports')->firstOrFail();

        $reports = Report::all()->pluck('slug')->toArray();
        $reports_perm = ['view' => 'view', 'create' => 'create', 'update' => 'update', 'delete' => 'delete'];
        foreach ($reports as $report) {
            $reports_perm[$report] = $report;
        }
        $perm = $this->permissions->update($permission->id, ['permissions'=>$reports_perm]);
    }
}
