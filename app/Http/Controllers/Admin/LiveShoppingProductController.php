<?php namespace App\Http\Controllers\Admin;

use DataTables;
use Config;
use Illuminate\Support\Facades\DB;
use Redirect;
use Sentry;
use View;
use Carbon\Carbon;
use App\Models\LiveShoppingTracker\LiveShoppingCategory;
use App\Models\LiveShoppingTracker\LiveShoppingProduct;
use App\Models\LiveShoppingTracker\LiveShoppingProductVersion;
use App\Models\LiveShoppingTracker\LiveShoppingAirtimeProduct;
use App\Services\Validators\LiveShoppingProductValidator;
use File;
use Former;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LiveShoppingProductController extends Controller
{
    public function __construct()
    {
        View::share('categories', LiveShoppingCategory::whereNull('parent_id')->orderBy('name')->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.lst.products.index')->with('products', LiveShoppingProduct::paginate(20));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.lst.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = new LiveShoppingProductValidator;

        if ($validation->passes()) {
            $product = new LiveShoppingProduct;
            $product->name = $request->get('name');
            $product->msrp = $request->get('msrp');
            $product->sku = $request->get('sku');
            $product->website = $request->get('website');
            $product->live_shopping_category_id = $request->get('live_shopping_category_id');
            $product->live_shopping_sub_category_id = $request->get('live_shopping_sub_category_id');
            $product->first_airdate = Carbon::createFromFormat("Y-m-d h:i:s A", $request->get('first_airdate'));
            $product->notes = $request->get('notes');
            $product->user_id = Sentry::getUser()->id;
            $product->save();

            $this->s3Magic($request, $product);

            return Redirect::route('admin.lst.products.edit', $product->id)->with('success', 'The product '.$product->name.' was saved.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.lst.products.show')->with('product', LiveShoppingProduct::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $product = LiveShoppingProduct::find($id);

        if (! $product) {
            return Redirect::route('admin.lst.products.index')->with('error', 'The product cannot be found!');
        }

        if ($product->live_shopping_category_id) {
            $subCategories = LiveShoppingCategory::where('parent_id', $product->live_shopping_category_id)
                ->orderBy('name')
                ->pluck('name', 'id')
                ->toArray();
        } else {
            $subCategories = [];
        }

        if ($request->filled('version_id')) {
            $version_id = $request->get('version_id');
        } else {
            $version_id = 0;
        }

        return view('admin.lst.products.edit')
            ->with('product', $product)
            ->with('subCategories', $subCategories)
            ->with('version_id', $version_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = new LiveShoppingProductValidator;

        if ($validation->passes()) {
            $product = LiveShoppingProduct::find($id);
            if (! $product) {
                return Redirect::route('admin.lst.products.index')->with('error', 'The product cannot be found!');
            }

            $product->name = $request->get('name');
            $product->msrp = $request->get('msrp');
            $product->sku = $request->get('sku');
            $product->website = $request->get('website');
            $product->live_shopping_category_id = $request->get('live_shopping_category_id');
            $product->live_shopping_sub_category_id = $request->get('live_shopping_sub_category_id');
            $product->first_airdate = Carbon::createFromFormat("Y-m-d h:i:s A", $request->get('first_airdate'));
            $product->notes = $request->get('notes');
            $product->user_id = Sentry::getUser()->id;
            $product->save();

            $this->s3Magic($request, $product);

            return Redirect::route('admin.lst.products.edit', $product->id)->with('success', 'The product '.$product->name.' was updated.');
        }

        return Redirect::back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = LiveShoppingProduct::with(['versions'])->find($id);
        if (! $product) {
            return Redirect::route('admin.lst.products.index')->with('error', 'The product cannot be found!');
        }

        $airtime_count = 0;
        $version_count = 0;
        if ($product->versions) {
            $version_count = $product->versions->count();
        }

        foreach ($product->versions as $version) {
            $airtimes = LiveShoppingAirtimeProduct::where('live_shopping_product_version_id', $version->id);
            $airtime_count = $airtimes->count();
            foreach ($airtimes as $airtime) {
                $airtime->delete();
            }
            $version->delete();
        }

        // Delete product
        $product->delete();

        return Redirect::route('admin.lst.products.index')->with('success', 'The product '.$product->name.' was deleted, along with '.$version_count.' versions, and '.$airtime_count.' airings!');
    }

    public function s3Magic($request, $product)
    {
        if ($request->hasFile('product_img')) {
            $path = $request->product_img
                ->storeAs('products/'.$product->id, 'logo.png', 's3-lst');

            $product->img_location = $path;
        }
    }

    public function getDatatables()
    {
        $programs = LiveShoppingProduct::select(
            'live_shopping_products.name',
            'live_shopping_products.msrp',
            'live_shopping_products.sku',
            'live_shopping_categories.name as category_name',
            'subcat.name as subcategory_name',
            DB::raw('DATE_FORMAT(live_shopping_products.first_airdate, "%Y-%m-%d") as first_airdate'),
            'live_shopping_products.id'
        )
            ->leftJoin('live_shopping_categories', 'live_shopping_products.live_shopping_category_id', '=', 'live_shopping_categories.id')
            ->leftJoin(DB::raw('live_shopping_categories as subcat'), 'live_shopping_products.live_shopping_sub_category_id', '=', 'subcat.id');

        return Datatables::of($programs)
            ->editColumn('name', '<a href="{{ route("lst.product", array($id)) }}">{{ $name }}</a> @if(Sentry::getUser()->isSuperUser()) <small><a href="{{ route("admin.lst.products.edit",array($id)) }}" target="_blank">Edit</a></small> @endif')
            ->editColumn('msrp', '${{ number_format($msrp, 2) }}')
            ->editColumn('id', '<a href="{{ route("admin.lst.products.edit", array($id)) }}" class="btn btn-sm btn-default"><i class="fa fa-edit"></i></a>
                @if(Sentry::getUser()->isSuperUser())<a href="{{ route("admin.lst.products.destroy", array($id)) }}" data-method="delete" data-modal-text="Delete this Product? All product versions will be deleted as well!" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>@endif')
            ->rawColumns(['id', 'name'])
            ->make();
    }
}
