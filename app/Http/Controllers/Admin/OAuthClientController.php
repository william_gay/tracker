<?php namespace App\Http\Controllers\Admin;

use Illuminate\Support\Str;
use App;
use Config;
use Event;
use Lang;
use Mail;
use Sentry;
use Redirect;
use View;
use Validator;
use Session;
use App\Models\OAuthClient;
use App\Models\OAuthClientEndpoint;

use App\Http\Controllers\Controller;

class OAuthClientController extends Controller
{


    // public function index()
    // {
    //     $curators = OAuthClient::with('endpoint')->get();

    //     return view('admin.oauth-clients.index', array('curators' => $curators));

    // }

  //   public function postCurator()
  //   {
  //   	$clientIDRandom = Str::random(5);
  //   	$clientSecretRandom = Str::random(8);
  //   	$clientName = Request::get('client_name');
  //   	$redirectURI = Request::get('redirect_uri');

  //   	$curator = new OAuthclient();
  //   	$curator->secret = $clientSecretRandom;
  //   	$curator->name = $clientName;
  //   	$curator->id = $clientIDRandom;

  //   	$curatorEndpoint = new OAuthClientEndpoint();
  //   	$curatorEndpoint->client_id = $clientIDRandom;
  //   	$curatorEndpoint->redirect_uri = $redirectURI;

  //   	$curator->save();
  //   	$curatorEndpoint->save();

  //   	if($curatorEndpoint->save()){
        // 	return redirect('admin/oauth-clients')->with('success', 1);
        // } else {
        //     return redirect('admin/oauth-clients')->with('error', 1);
        // }

  //   }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $clients = OAuthClient::all();

        return view('admin.oauth-clients.index')
            ->with('clients', $clients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.oauth-clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = [
            'client_name'  => 'required',
            'redirect_uri' => 'required'
        ];
        $validator = Validator::make(Request::all(), $rules);

        //Process the login
        if ($validator->fails()) {
            return redirect('admin/oauth-clients/create')
                ->withErrors($validator)
                ->withInput(Request::except('password'));
        } else {
            //We're saving OAuth Clients just below!

            //Instantiate new objects
            $client = new OAuthClient();
            $clientEndpoint = new OAuthClientEndpoint();

            //create a reusable random string
            $clientIDRandom = Str::random(5);
            $clientSecretRandom = Str::random(8);

            //Assign the object's attributes to form input and the reusable random strings
            $client->name           = Request::get('client_name');
            $client->id             = $clientIDRandom;
            $client->secret         = $clientSecretRandom;

            //Attribute the client endpoint with the client along with assinging the redirect_uri
            $clientEndpoint->client_id    = $clientIDRandom;
            $clientEndpoint->redirect_uri = Request::get('redirect_uri');


            //Save the objects
            $client->save();
            $clientEndpoint->save();

            //Redirect
            Session::flash('message', 'Successfully created a Client');
            return redirect('admin/oauth-clients/');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //Get the Client
        $client = OAuthClient::find($id);

        //Show the edit form and pass the nerd
        return view::make('admin.oauth-clients.edit')
            ->with('client', $client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $rules = [
            'client_name'  => 'required',
            'redirect_uri' => 'required'
        ];
        $validator = Validator::make(Request::all(), $rules);

        //Process the login
        if ($validator->fails()) {
            return redirect('admin/oauth-clients/create')
                ->withErrors($validator)
                ->withInput(Request::except('password'));
        } else {
            //store
            $client = OAuthClient::find($id);

            $client->name                         = Request::get('client_name');
            $client->clientEndpoint->redirect_uri = Request::get('redirect_uri');

            $client->save();
            $client->clientEndpoint->save();

            //redirect
            Session::flash('message', 'Successfully updated Client!');
            return redirect('admin/oauth-clients');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //delete the client
        $client = OAuthClient::find($id);
        $client->clientEndpoint->delete();
        $client->delete();

        //redirect
        Session::flash('message', 'Successfully deleted the Client!');
        return redirect('admin/oauth-clients');
    }
}
