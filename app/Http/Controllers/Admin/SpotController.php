<?php namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Channel;
use App\Models\Company;
use App\Models\Keyword;
use App\Models\Language;
use App\Models\Spot;
use App\Models\SpotAirtime;
use App\Models\SpotDetection;
use App\Models\SpotRanking;
use App\Models\SpotVersion;
use Carbon\Carbon;
use App\Services\Validators\SpotValidator;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Facades\DB;
use Sentry;

use App\Http\Controllers\Controller;

class SpotController extends Controller
{
    public function __construct()
    {
        view()->share('languages', Language::pluck('name', 'id')->toArray());

        $channels = Channel::with('language')
            ->orderBy('name', 'asc')
            ->get();

        $channels_array = [];
        foreach ($channels as $channel) {
            $channels_array[$channel->id] = $channel->name .' -- '.$channel->abbr .' ('.$channel->language->name.')';
        }

        view()->share('channels', $channels_array);
        view()->share('categories', Category::whereNull('parent_id')->orderBy('name')->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            $spots = SpotVersion::with(['language', 'channel', 'user', 'category', 'subCategory']);

            return Datatables::of($spots)
                ->editColumn('title', '<a href="{{ route("spots.show", array($id)) }}" target="_blank">{{ $title }}</a>  @if(Sentry::getUser()->isSuperUser()) <small><a href="{{ route("admin.spots.edit",array($id)) }}" target="_blank">Edit Spot</a></small> @endif')
                ->editColumn('length', ':{{ $length }}')
                ->editColumn('mp4', '@if($mp4 == 1) <i class="fa fa-play"></i> @endif')
                ->addColumn('id', '<a href="{{ route("admin.spots.edit", array($id)) }}" class="btn btn-sm btn-default"><i class="fa fa-edit"></i></a>
-                    <a href="{{ route("admin.spots.destroy", array($id)) }}" data-method="delete" data-modal-text="Delete this Channel?" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></a>')
                ->editColumn('email', function ($spot) {
                    if (isset($spot->user)) {
                        return $spot->user->email;
                    }
                })
                ->addColumn('channel', function ($spot) {
                    if ($spot->channel) {
                        return $spot->channel->abbr;
                    }
                })
                ->addColumn('marketingCompany', function ($spot) {
                    if ($spot->marketingCompany) {
                        return $spot->marketingCompany->name;
                    }
                })
                ->addColumn('category', function ($spot) {
                    if ($spot->category) {
                        return $spot->category->name;
                    }
                })
                ->addColumn('subCategory', function ($spot) {
                    if ($spot->subCategory) {
                        return $spot->subCategory->name;
                    }
                })
                ->rawColumns(['title', 'mp4', 'id'])
                ->toJson();
        }

        $html = $builder->columns([
            ['data' => 'title', 'name' => 'title', 'title' => 'title'],
            ['data' => 'version', 'name' => 'version', 'title' => 'Version'],
            ['data' => 'length', 'name' => 'length', 'title' => 'length'],
            ['data' => 'mp4', 'name' => 'mp4', 'title' => 'mp4'],
            ['data' => 'language.name', 'name' => 'language.name', 'title' => 'Language'],
            ['data' => 'channel', 'name' => 'channel', 'title' => 'Channel'],
            ['data' => 'air_date', 'name' => 'air_date', 'title' => 'air_date'],
            ['data' => 'email', 'name' => 'user.email', 'title' => 'User'],
            ['data' => 'id', 'name' => 'id', 'title' => 'Actions'],
        ]);

        $builder->parameters([
            'drawCallback' => 'function() { laravel.initialize(); }',
        ]);

        return view('admin.spots.index')
            ->with('html', $html);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.spots.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = new SpotValidator;

        $company_id = $this->newCompany();

        $spot_id = $this->spotId();

        if ($validation->passes()) {
            $service = $this->isService();
            $non_dr = $this->isNonDr();
            $brand_advert = $this->isBrandAdvert();

            $spot = new SpotVersion;
            $spot->title = request()->get('title');
            $spot->version = request()->get('version');
            $spot->spot_id = $spot_id;
            $spot->category_id = request()->get('category_id');
            $spot->sub_category_id = request()->get('sub_category_id');
            $spot->service = $service;
            $spot->non_dr = $non_dr;
            $spot->brand_advert = $brand_advert;
            $spot->channel_id = request()->get('channel_id');
            $spot->marketing_company_id = $company_id;
            $spot->air_date = request()->get('air_date');
            $spot->initial_date = request()->get('initial_date');
            $spot->description = request()->get('description');
            $spot->price = request()->get('price');
            $spot->currency = request()->get('currency');
            $spot->price_extra = request()->get('price_extra');
            $spot->shipping_cost = request()->get('shipping_cost');
            $spot->offer = request()->get('offer');
            $spot->website = request()->get('website');
            $spot->phone = request()->get('phone');
            $spot->cs_phone = request()->get('cs_phone');
            $spot->remarks = request()->get('remarks');
            $spot->keywords = request()->get('keywords');
            $spot->length = request()->get('length');
            $spot->notes = request()->get('notes');
            $spot->page_number = request()->get('page_number');
            $spot->oddball = request()->get('oddball');
            $spot->corporate = request()->get('corporate');
            $spot->language_id = request()->get('language_id');
            $spot->file_name = request()->get('file_name');
            $spot->flv = request()->get('flv');
            $spot->mp4 = request()->get('mp4');
            $spot->poster = request()->get('poster');
            $spot->user_id = Sentry::getUser()->id;
            $spot->created_by = Sentry::getUser()->id;
            $spot->save();

            if ($spot->version > 1) {
                $this->updateService($spot->spot_id, $service);
                $this->updateBrand($spot->spot_id, $brand_advert);
            }

            Keyword::processKeywords(request()->get('keywords'));

            return redirect()->route('admin.spots.edit', $spot->id)->with('success', 'The spot '.$spot->title.' was saved.');
        }

        return redirect()->back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $spotId
     * @return Response
     */
    public function show($spotId)
    {
        return view('admin.spots.show')->with('spot', Spot::find($spotId));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $spotId
     * @return Response
     */
    public function edit($spotId)
    {
        $spot = SpotVersion::with('user')->findOrFail($spotId);

        $monitor_date = $spot->air_date;
        $mp4_date = Carbon::create(2013, 10, 5);
        if ($monitor_date->lt($mp4_date)) {
            $videos = getWeekEndVideos($spot->air_date, 'flv', true);
        } else {
            $videos = getWeekEndVideos($spot->air_date, 'mp4', true);
        }

        return view('admin.spots.edit')
            ->with('spot', $spot)
            ->with('videos', $videos)
            ->with('subcats', Category::where('parent_id', $spot->category_id)->pluck('name', 'id')->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $spotId
     * @return Response
     */
    public function update($spotId)
    {
        $validation = new SpotValidator;

        $company_id = $this->newCompany();

        if ($validation->passes()) {
            $service = $this->isService();
            $non_dr = $this->isNonDr();
            $brand_advert = $this->isBrandAdvert();

            $spot = SpotVersion::find($spotId);
            $spot->title = request()->get('title');
            $spot->version = request()->get('version');
            $spot->spot_id = request()->get('spot_id');
            $spot->category_id = request()->get('category_id');
            $spot->sub_category_id = request()->get('sub_category_id');
            $spot->service = $service;
            $spot->non_dr = $non_dr;
            $spot->brand_advert = $brand_advert;
            $spot->channel_id = request()->get('channel_id');
            $spot->marketing_company_id = $company_id;
            $spot->air_date = request()->get('air_date');
            $spot->initial_date = request()->get('initial_date');
            $spot->description = request()->get('description');
            $spot->currency = request()->get('currency');
            $spot->price = request()->get('price');
            $spot->price_extra = request()->get('price_extra');
            $spot->shipping_cost = request()->get('shipping_cost');
            $spot->offer = request()->get('offer');
            $spot->website = request()->get('website');
            $spot->phone = request()->get('phone');
            $spot->cs_phone = request()->get('cs_phone');
            $spot->remarks = request()->get('remarks');
            $spot->keywords = request()->get('keywords');
            $spot->length = request()->get('length');
            $spot->notes = request()->get('notes');
            $spot->page_number = request()->get('page_number');
            $spot->oddball = request()->get('oddball');
            $spot->corporate = request()->get('corporate');
            $spot->language_id = request()->get('language_id');
            $spot->file_name = request()->get('file_name');
            $spot->flv = (request()->get('flv') == null) ? 0 : 1;
            $spot->mp4 = request()->get('mp4');
            $spot->poster = request()->get('poster');
            $spot->user_id = Sentry::getUser()->id;
            $spot->save();

            if ($spot->version > 1) {
                $this->updateService($spot->spot_id, $service);
                $this->updateBrand($spot->spot_id, $brand_advert);
            }

            Keyword::processKeywords(request()->get('keywords'));

            /* Make sure the parent spot is correct */
            $parent = Spot::find($spot->spot_id);
            $parent->title = request()->get('title');
            $parent->category_id = request()->get('category_id');
            $parent->sub_category_id = request()->get('sub_category_id');
            $parent->language_id = request()->get('language_id');
            $parent->save();

            $this->propagateCategories($spot->spot_id, request()->get('category_id'), request()->get('sub_category_id'));

            return redirect()->route('admin.spots.edit', $spot->id)->with('success', 'The spot '.$spot->title.' was saved.');
        }

        return redirect()->back()->withInput()->withErrors($validation->errors);
    }

    public function isService()
    {
        return request()->filled('service') ? 1 : 0;
    }

    public function isNonDr()
    {
        return request()->filled('non_dr') ? 1 : 0;
    }

    public function isBrandAdvert()
    {
        return request()->filled('brand_advert') ? 1 : 0;
    }


    public function updateService($spotId, $service)
    {
        $update = SpotVersion::where('spot_id', $spotId)
            ->update(['service' => $service]);

        if ($update) {
            return true;
        }
    }

    public function updateBrand($spotId, $brand)
    {
        $update = SpotVersion::where('spot_id', $spotId)
            ->update(['brand_advert' => $brand]);

        if ($update) {
            return true;
        }
    }

    public function propagateCategories($spotId, $categoryId, $subCategoryId)
    {
        $versions = SpotVersion::where('spot_id', $spotId)
            ->update([
                'category_id' => $categoryId,
                'sub_category_id' => $subCategoryId
            ]);

        if ($versions) {
            return true;
        }

        return false;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $spotId
     * @return Response
     */
    public function destroy($spotId)
    {
        $spot = SpotVersion::findOrFail($spotId);

        $spot_detection = SpotDetection::where('spot_version_id', $spotId)->get();
        $spot_airtimes = SpotAirtime::where('spot_version_id', $spotId)->get();
        $spot_rankings = SpotRanking::where('spot_id', $spot->spot_id)->get();

        if ($spot_detection->count() == 0) {
            //DELETE THIS NOW
            if ($spot->version == 1) {
                //$spot->spot()->delete();
                $parent = Spot::find($spot->spot_id);
                if ($parent) {
                    $parent->delete();
                }
            }
            if ($spot->airtimes->count() > 0) {
                $spot->airtimes()->delete();
                $airtimes = SpotAirtime::where('spot_version_id', $spot->id)->delete();
            }

            $spot->delete();
        } else {
            return redirect()->route('admin.spots.index')
                ->with('error', 'The spot '.$spot->title.' could not be deleted. There are '.$spot_detection->count().' weekly detections and '.$spot_rankings->count().' weekly rankings! Totaling '.$spot_airtimes->count(). ' airings detected...');
        }

        return redirect()->route('admin.spots.index')->with('success', 'The spot '.$spot->title.' was deleted.');
    }

    private function spotId()
    {
        if (! request()->filled('spot_id')) {
            $spot = new Spot;
            $spot->title = request()->get('title');
            $spot->category_id = request()->get('category_id');
            $spot->sub_category_id = request()->get('sub_category_id');
            $spot->language_id = request()->get('language_id');
            $spot->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $spot->updated_at = Carbon::now()->format('Y-m-d H:i:s');

            $spot->save();

            return $spot->id;
        }

        return request()->get('spot_id');
    }

    private function newCompany()
    {
        if (request()->filled('new_company_ck')) {
            //make sure the company doesn't already exist
            $company = Company::where('name', request()->get('new_company'))->first();
            if ($company) {
                return $company->id;
            } else {
                //Create new Company
                $new_company = new Company;
                $new_company->name = request('new_company');
                $new_company->user_id = Sentry::getUser()->id;
                $new_company->save();
                return $new_company->id;
            }
        }

        return request()->get('marketing_company_id');
    }

    public function getPdf()
    {
        if (! request()->filled('page_number') && ! request()->filled('weekEnding') && ! request()->filled('report_type')) {
            return view('admin.spots.print');
        } else {
            $download = request()->get('download');
            $weekEnding = Carbon::createFromFormat('Y-m-d', request()->get('weekEnding'));
            $page_number = request()->get('page_number');
            $report_type = request()->get('report_type');

            if ($report_type == 'top-50') {
                $rank25 = SpotRanking::select(
                    'spots.title',
                    'companies.name as marketing_company',
                    'spot_versions.price',
                    'categories.name as category'
                )
                    ->where('rank_date', $weekEnding->format('Y-m-d'))
                    ->leftJoin('spots', 'spots.id', '=', 'spot_rankings.spot_id')
                    ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_rankings.spot_version_id')
                    ->leftJoin('companies', 'companies.id', '=', 'spot_versions.marketing_company_id')
                    ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
                    ->take(25)
                    ->orderBy('rank')
                    ->get();
                $rank50 = SpotRanking::select(
                    'spots.title',
                    'companies.name as marketing_company',
                    'spot_versions.price',
                    'categories.name as category'
                )
                    ->where('rank_date', $weekEnding->format('Y-m-d'))
                    ->leftJoin('spots', 'spots.id', '=', 'spot_rankings.spot_id')
                    ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_rankings.spot_version_id')
                    ->leftJoin('companies', 'companies.id', '=', 'spot_versions.marketing_company_id')
                    ->leftJoin('categories', 'categories.id', '=', 'spot_versions.category_id')
                    ->skip(25)
                    ->take(25)
                    ->orderBy('rank')
                    ->get();

                $data = [
                    'absolute' => '/',
                    'weekEnding' => $weekEnding->format('F jS, Y'),
                    'rank25' => $rank25,
                    'rank50' => $rank50,
                    'page_number' => $page_number
                ];

                if ($download == 1) {
                    $data['absolute'] = '';
                    $pdf = null;
                    return $pdf->download('spot-ranking.pdf');
                }

                return view('pdfs.spot-rankings', $data);
            } elseif ($report_type == 'new-spots') {
                if (!request()->filled('take') && !request()->filled('skip')) {
                    $spotCount = $newSpots = SpotVersion::where('language_id', 1)
                    ->whereBetween('air_date', [$weekEnding->subDays(6)->format('Y-m-d'), $weekEnding->addDays(6)->format('Y-m-d')])
                    ->count();

                    $perpage = 25;
                    $pages = ceil($spotCount/$perpage);

                    $data = [
                        'report_type' => $report_type,
                        'weekEnding' => $weekEnding->format('Y-m-d'),
                        'page_number' => $page_number,
                        'count' => $spotCount,
                        'pages' => $pages,
                        'perpage' => $perpage
                    ];
                    return view('pdfs.new-spots', $data);
                }

                $take = request()->get('take');
                $skip = request()->get('skip');

                $newSpots = SpotVersion::where('language_id', 1)
                    ->whereBetween('air_date', [$weekEnding->subDays(6)->format('Y-m-d'), $weekEnding->addDays(6)->format('Y-m-d')])
                    ->skip($skip)
                    ->take($take)
                    ->orderBy('title', 'asc')
                    ->get();

                $data = [
                    'absolute' => '/',
                    'weekEnding' => $weekEnding->format('F jS, Y'),
                    'page_number' => $page_number,
                    'spots' => $newSpots,
                    'take' => $take,
                    'skip' => $skip
                ];

                return view('pdfs.new-spots', $data);
            } elseif ($report_type == 'top-100') {
                $startDay = Carbon::createFromFormat('Y-m-d', request()->get('startDay'));
                $month = request()->get('month');

                $spots25 = SpotDetection::select(DB::raw('SUM(`detections`) as airings'), 'spot_versions.title')
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
                    ->whereBetween(
                        'rank_date',
                        [$startDay->format('Y-m-d'),$weekEnding->format('Y-m-d')]
                    )
                    ->where('spot_detections.language_id', 1) // English
                    ->groupBy('spot_versions.spot_id')
                    ->orderBy('airings', 'desc')
                    ->take(25)
                    ->skip(0)
                    ->get();
                $spots50 = SpotDetection::select(DB::raw('SUM(`detections`) as airings'), 'spot_versions.title')
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
                    ->whereBetween(
                        'rank_date',
                        [$startDay->format('Y-m-d'),$weekEnding->format('Y-m-d')]
                    )
                    ->where('spot_detections.language_id', 1) // English
                    ->groupBy('spot_versions.spot_id')
                    ->orderBy('airings', 'desc')
                    ->take(25)
                    ->skip(25)
                    ->get();
                $spots75 = SpotDetection::select(DB::raw('SUM(`detections`) as airings'), 'spot_versions.title')
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
                    ->whereBetween(
                        'rank_date',
                        [$startDay->format('Y-m-d'),$weekEnding->format('Y-m-d')]
                    )
                    ->where('spot_detections.language_id', 1) // English
                    ->groupBy('spot_versions.spot_id')
                    ->orderBy('airings', 'desc')
                    ->take(25)
                    ->skip(50)
                    ->get();
                $spots100 = SpotDetection::select(DB::raw('SUM(`detections`) as airings'), 'spot_versions.title')
                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
                    ->whereBetween(
                        'rank_date',
                        [$startDay->format('Y-m-d'),$weekEnding->format('Y-m-d')]
                    )
                    ->where('spot_detections.language_id', 1) // English
                    ->groupBy('spot_versions.spot_id')
                    ->orderBy('airings', 'desc')
                    ->take(25)
                    ->skip(75)
                    ->get();

                $data = [
                    'absolute' => '/',
                    'month' => $month,
                    'year' => $startDay->format('Y'),
                    'startDay' => $startDay->format('F jS, Y'),
                    'weekEnding' => $weekEnding->format('F jS, Y'),
                    'page_number' => $page_number,
                    'spots25' => $spots25,
                    'spots50' => $spots50,
                    'spots75' => $spots75,
                    'spots100' => $spots100
                ];

                return view('pdfs.top-100', $data);
            } elseif ($report_type == 'top-5-category') {
                //TODO: This logic needs to be tweaked to add up the dections.

                $cats = [11, 2, 8, 12, 13, 16];
                $startDay = Carbon::createFromFormat('Y-m-d', request()->get('startDay'));
                $month = request()->get('month');

                $category_report = [];
                foreach ($cats as $category) {
                    $detections = SpotDetection::select(
                        DB::raw('SUM(`detections`) as airings'),
                        'spot_versions.title',
                        DB::raw('(SELECT price FROM spot_versions WHERE spot_versions.spot_id = spot_detections.spot_version_id ORDER BY air_date desc LIMIT 1) as price'),
                        DB::raw('(SELECT shipping_cost FROM spot_versions WHERE spot_versions.spot_id = spot_detections.spot_version_id ORDER BY air_date desc LIMIT 1) as shipping_cost'),
                        'categories.name as category_name',
                        'companies.name as marketing_company'
                    )
                                    ->join('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
                                    ->join('categories', 'spot_versions.category_id', '=', 'categories.id')
                                    ->join('companies', 'spot_versions.marketing_company_id', '=', 'companies.id')
                                    ->whereBetween(
                                        'rank_date',
                                        [$startDay->format('Y-m-d'),$weekEnding->format('Y-m-d')]
                                    )
                                    ->where('spot_detections.language_id', 1) // English
                                    ->where('spot_versions.category_id', $category)
                                    ->where('spot_versions.service', '!=', 1)
                                    ->groupBy('spot_versions.title')
                                    ->orderBy('airings', 'desc')
                                    ->take(5)
                                    ->get();

                    $category_report[$category] = [
                        'spots' => $detections
                    ];
                }

                $data = [
                    'absolute' => '/',
                    'month' => $month,
                    'year' => $startDay->format('Y'),
                    'startDay' => $startDay->format('F jS, Y'),
                    'weekEnding' => $weekEnding->format('F jS, Y'),
                    'page_number' => $page_number,
                    'categories' => $category_report,
                ];

                return view('pdfs.top-5-category', $data);
            }
        }
    }

    public function getRankings()
    {
        if (request()->filled('endDate') and request()->filled('startDate') and request()->filled('language_id')) {
            $endDate = Carbon::createFromFormat('Y-m-d', request()->get('endDate'))->setTime(16, 0, 0);
            $startDate = Carbon::createFromFormat('Y-m-d', request()->get('startDate'))->setTime(16, 0, 0);
            $language_id = request()->get('language_id');
        } else {
            $endDate = Carbon::now()->setTime(16, 0, 0);
            $startDate = Carbon::now()->subDays(7)->setTime(16, 0, 0);
            $language_id = request()->get('language_id');
        }

        $spots = SpotAirtime::select(
            [
                DB::raw('max(spot_versions.id)'),
                'spot_versions.version',
                'spot_versions.service',
                'spot_versions.non_dr',
                'spot_versions.brand_advert',
                'spot_versions.title',
                'spot_versions.length',
                'spot_versions.id',
                DB::raw('COUNT(spot_airtimes.id) as detections'),
                'category.name as category',
                'sub_category.name as sub_category'
            ]
        )
            ->whereBetween('spot_airtimes.air_date', [$startDate->toDateTimeString(),$endDate->toDateTimeString()])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('spots', 'spots.id', '=', 'spot_versions.spot_id')
            ->leftJoin('categories as category', 'category.id', '=', 'spots.category_id')
            ->leftJoin('categories as sub_category', 'sub_category.id', '=', 'spots.sub_category_id')
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->where('spot_versions.language_id', $language_id)
            ->where('spot_versions.non_dr', 0) // Hard coded so we don't see the non-dr shit
            ->where('channels.pi', 0) // Don't include the PI Channels
            ->groupBy('spot_versions.spot_id')
            ->orderBy('detections', 'desc')
            ->orderBy('spot_versions.air_date', 'desc')
            ->get();

        return view('admin.spots.ranking')
            ->with('spots', $spots)
            ->with('language_id', $language_id)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }

    public function getUniqueDetections()
    {
        if (request()->filled('endDate') and request()->filled('startDate') and request()->filled('language_id')) {
            $endDate = Carbon::createFromFormat('Y-m-d', request()->get('endDate'));
            $startDate = Carbon::createFromFormat('Y-m-d', request()->get('startDate'));
            $language_id = request()->get('language_id');
        } else {
            $endDate = Carbon::now();
            $startDate = Carbon::now()->subDays(7);
            $language_id = request()->get('language_id');
        }

        $spots = SpotAirtime::select(['spot_versions.*', DB::raw('COUNT(spot_airtimes.id) as detections')])
            ->whereBetween('spot_airtimes.air_date', [$startDate->startOfDay(),$endDate->endOfDay()])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->where('language_id', $language_id)
            ->groupBy('spot_airtimes.spot_version_id')
            ->orderBy('title', 'asc')
            ->get();

        return view('admin.spots.unique')
            ->with('spots', $spots)
            ->with('language_id', $language_id)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }

    public function getNewSpots()
    {
        if (request()->filled('endDate') and request()->filled('startDate') and request()->filled('language_id')) {
            $endDate = Carbon::createFromFormat('Y-m-d', request()->get('endDate'));
            $startDate = Carbon::createFromFormat('Y-m-d', request()->get('startDate'));
            $language_id = request()->get('language_id');
        } else {
            $endDate = Carbon::now();
            $startDate = Carbon::now()->subDays(7);
            $language_id = request()->get('language_id');
        }

        $spots = SpotVersion::whereBetween('initial_date', [$startDate->startOfDay(),$endDate->endOfDay()])
            ->where('language_id', $language_id)
            ->orderBy('title', 'asc')
            ->get();

        return view('admin.spots.new')
            ->with('spots', $spots)
            ->with('language_id', $language_id)
            ->with('startDate', $startDate)
            ->with('endDate', $endDate);
    }
}
