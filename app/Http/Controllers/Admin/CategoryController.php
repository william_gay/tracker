<?php namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Services\Validators\CategoryValidator;
use Former;
use Input;
use Sentry;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function __construct()
    {
        view()->share('pcats', ['0' => 'Primary Category'] + Category::whereNull('parent_id')->pluck('name', 'id')->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = Category::orderBy('name')->get();

        return view('admin.categories.index')
            ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = new CategoryValidator;

        if ($validation->passes()) {
            $category = new Category;
            $category->parent_id = ($request->get('parent_id') == 0)? null : $request->get('parent_id');
            $category->name = $request->get('name');
            $category->description = $request->get('description');
            $category->user_id = Sentry::getUser()->id;
            $category->save();

            return redirect()->route('admin.categories.edit', $category->id)->with('success', 'The category '.$category->name.' was saved.');
        }

        return redirect()->back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin.categories.show')->with('category', Category::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.categories.edit')->with('category', Category::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $validation = new CategoryValidator;

        if ($validation->passes()) {
            $category = Category::find($id);
            $category->parent_id = ($request->get('parent_id') == 0)? null : $request->get('parent_id');
            $category->name = $request->get('name');
            $category->description = $request->get('description');
            $category->user_id = Sentry::getUser()->id;
            $category->save();

            return redirect()->route('admin.categories.edit', $category->id)->with('success', 'The category '.$category->name.' was updated.');
        }

        return redirect()->back()->withInput()->withErrors($validation->errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        return redirect()->route('admin.categories.index')->with('success', 'The category '.$category->name.' was deleted.');
    }
}
