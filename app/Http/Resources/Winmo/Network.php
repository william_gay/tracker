<?php

namespace App\Http\Resources\Winmo;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Winmo\Presence as PresenceResource;
use App\Models\Product;

class Network extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'abbr' => $this->abbr,
            'top' => PresenceResource::collection($this->whenLoaded('products')),
        ];
    }
}
