<?php

namespace App\Http\Resources\Winmo;

use Illuminate\Http\Resources\Json\JsonResource;

class Presence extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'winmo_brand_id' => $this->winmo_brand_id,
            'winmo_company_id' => $this->winmo_company_id,
            'name' => $this->name,
            'long_form' => $this->last_lf_airing ? true : false,
            'short_form' => $this->last_sf_airing ? true : false,
            'embed' => route('winmo.embed', [
                'winmo_company_id' => $this->winmo_company_id,
                'winmo_brand_id' => $this->winmo_brand_id,
            ]),
            // TODO: make this work
            'predictor' => [
                '30-days' => $this->predictor_30,
                '60-days' => $this->predictor_60,
                '90-days' => $this->predictor_90,
            ],
        ];
    }
}
