<?php

namespace App\Http\Resources\Winmo;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Winmo\PredictorValuesResource as PREDICTORVALS;

class Predictor extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            "last_airing" =>$this->last_airing,
            "predictor" => PREDICTORVALS::collection($this->whenLoaded('predictor')),
        ];
    }
}
