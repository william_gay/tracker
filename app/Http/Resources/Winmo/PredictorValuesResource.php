<?php

namespace App\Http\Resources\Winmo;

use Illuminate\Http\Resources\Json\JsonResource;

class PredictorValuesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'air_month' => $this->air_month,
            'month_airing_count' => $this->month_airing_count,
            'last_airing_year' => $this->last_airing_year,
        ];
    }
}
