<?php

namespace App\Console\Commands;

use App\Models\Airtime;
use App\Models\Channel;
use App\Models\Program;
use Illuminate\Console\Command;

class AssociateFirstChannelCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:associatechannel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to associate channel with first channel aired.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $programs = Program::whereNull('channel_id')->orderBy('title')->get();

        foreach ($programs as $program) {
            //Get eariliest airtime
            $airtime = Airtime::where('program_id', $program->id)->orderBy('air_date')->first();
            if ($airtime) {
                $channel = Channel::find($airtime->channel_id);
                if ($channel) {
                    $this->info('Show: '.$program->title.' New Channel ID: '.$airtime->channel_id);
                    $program->channel_id = $airtime->channel_id;
                    $program->save();
                } else {
                    $this->info('Show: '.$program->title.' Channel does not exist');
                }
            } else {
                $this->info('No Airtimes for: '.$program->title);
            }
        }
    }
}
