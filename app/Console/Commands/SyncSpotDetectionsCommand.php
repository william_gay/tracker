<?php

namespace App\Console\Commands;

use App\Models\Channel;
use App\Models\Spot;
use App\Models\SpotDetection;
use App\Models\SpotMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncSpotDetectionsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:syncspotdetections';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Spot detections.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Disable query logging on this big db
        DB::connection()->disableQueryLog();
        DB::connection('legacy_spot')->disableQueryLog();

        $at_a_time = 10000;

        $total_rows = DB::connection('legacy_spot')->select('select count(ID) as count FROM legacy_spot.`Detections` WHERE DATE_SUB(CURDATE(),INTERVAL 2 WEEK) <= `Rank Date`');
        $number_queries = ceil($total_rows[0]->count / $at_a_time);
        $this->info('This script will run '.$number_queries.' queries!');
        $query = 0;
        $limit = 0;

        while ($query < $number_queries) {
            $lspot_detections = DB::connection('legacy_spot')->select('SELECT  * FROM legacy_spot.`Detections` WHERE DATE_SUB(CURDATE(),INTERVAL 1 WEEK) <= `Rank Date` ORDER BY ID asc LIMIT '.$limit.', '.$at_a_time);

            $this->comment('Query: '.$query.' Limit: '.$limit);

            foreach ($lspot_detections as $detection) {
                $original_date = new DateTime($detection->{'Rank Date'});
                $just_date = $original_date->format('Y-m-d');
                //$this->info($just_date);

                $channel_arr = [];

                //Do Some channel Lookups
                if ($detection->Channels != '') {
                    //Replace all periods with comma:
                    $channels = str_replace('.', ',', $detection->Channels);
                    //Replace all slashes with comma:
                    $channels = str_replace('/', ',', $channels);
                    //Replace all single quotes with nothing:
                    $channels = str_replace("'", '', $channels);

                    $channels = explode(',', $channels);

                    foreach ($channels as $channel) {
                        $channel = trim($channel);
                        $chan = Channel::where('abbr', $channel)->first();
                        if ($chan) {
                            $channel_arr[] = $chan->id;
                        } else {
                            $chanmap = [
                                'E' => 24, ' E' => 24,
                                'LIF' => 6,
                                'NICK' => 8,'NICk' => 8, 'NIC' => 8,
                                'TVLD' => 86,'TVVL' => 86,
                                'STYL' => 69,
                                'HIST' => 65,'HIST-I' => 65,
                                'AE' => 68,
                                'G4' => 74,'G$' => 74,'G4TV' => 74,'G Channel' => 74,'G channel' => 74,
                                'DSCH' => 10,'DSCT' => 10,'DISX' => 10, 'DST' => 10, 'DSCJ' => 10,
                                'FITTV' => 133,
                                'DSCK' => 134,'DSK' => 134,
                                'DHOM' => 73,'DH' => 73,
                                'VH-1' => 18,'VH1 CLA' => 18, 'VHI' => 18,
                                'FLN' => 79,
                                'CN' => 160,
                                'TOON' => 132,'TOON NICK' => 132,'CAR' => 132, 'TDV' => 132,
                                'TVFOOD' => 30,
                                'Broadcast' => 112,
                                'SCIFI' => 14,
                                'GAMEGOOD' => 127,
                                'ION' => 58,
                                'INSP GOOD' => 34,'INSP NICK' => 34, 'ISNP' => 34,
                                'CNBC GOOD' => 23,'CNBCW' => 23,'CNBC-W' => 23, 'CNCBW' => 23,
                                'DIS. KIDS' => 134,
                                ' FNC' => 128,
                                'OLN' => 123,
                                'TDIS' => 10,'DSC' => 10,
                                'FXNW' => 28,
                                'HDLN' => 120,
                                'STY' => 69, 'STLY' => 69,
                                'HISTI' => 65,'HISTi' => 65,'HISIT' => 65, 'HSIT' => 65,
                                'HI' => 65,
                                'MSN' => 114,'MSBNC' => 114,
                                'HN' => 120,
                                'SCI-CH' => 124, ' SC' => 124, 'SCICH' => 124,
                                'TVLAND' => 86, 'TLVD' => 86,
                                'VS`' => 131,
                                'Nick' => 8,
                                'SPIK' => 70,
                                'ESPNC' => 4, 'ESPNN' => 4,
                                'ES2' => 5, 'EESPN2' => 5,
                                'DHF' => 73, 'HEALTH' => 73,
                                'TGC' => 135, ' TGC' => 135, 'TCG' => 135,
                                'BRT' => 25,
                                'PINTBS' => 9,
                                'TV1' => 152, 'TVOEN' => 152,
                                'CNNH' => 118,
                                'TWC`' => 138,
                                'FUEK' => 85,
                                'SNBC' => 161,
                                'CBC' => 142,
                                'FDIY' => 63
                            ];

                            if (array_key_exists($channel, $chanmap)) {
                                $channel_arr[] = $chanmap[$channel];
                            } else {
                                $this->error('Channel Not found! '. $channel);
                            }
                        }
                    }
                }

                //Look up spot_id
                $spotmap = SpotMapping::where('legacy_spot_id', $detection->ID)->first();

                if (!$spotmap) {
                    $this->error('Spot Map Not Found! '.$detection->ID);
                } else {
                    //Check to see if it exists already:
                    $exists = SpotDetection::where('rank_date', $just_date)
                                    ->where('detections', $detection->Detections)
                                    ->where('new_for_week', $detection->{'New for Week'})
                                    ->where('spot_id', $spotmap->spot_id)->first();

                    //$this->info('Rank Date: '.$just_date.' Detections: '.$detection->Detections.' New: '.$detection->{'New for Week'}.' Spot ID: '.$spotmap->spot_id);

                    if (! $exists) {
                        //Insert it since it doesn't exist.
                        $spotdetection = new SpotDetection;
                        $spotdetection->spot_id = $spotmap->spot_id;
                        $spotdetection->rank_date = $just_date;
                        $spotdetection->detections = $detection->Detections;
                        $spotdetection->new_for_week = $detection->{'New for Week'};
                        $spotdetection->save();

                        if (count($channel_arr) > 0) {
                            $spotdetection->channels()->sync($channel_arr);
                        }
                    }
                }

                unset($spotdetection);
                unset($just_date);
                unset($spotmap);
            }

            $query++;
            $limit = $limit + $at_a_time;
        }
    }
}
