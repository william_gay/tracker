<?php

namespace App\Console\Commands;

use App\Models\Program;
use Illuminate\Console\Command;

class AddDescriptionsToSpanish extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:updatespanish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Take Program info from English and bring it over to Spanish.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $spanishes = Program::where('language_id', 2)->where('id', '!=', 11560)->orderBy('title', 'asc')->get();

        foreach ($spanishes as $spanish) {
            $this->info('Spanish: '.$spanish->title);
            $program = Program::where('language_id', 1)->where('title', $spanish->title)->orderBy('monitor_date', 'desc')->first();

            if ($program) {
                $this->info('Program Found: ID: '.$spanish->id.' Title: '.$spanish->title.' English ID: ' .$program->id);

                $spanish->category_id = $program->category_id;
                $spanish->sub_category_id = $program->sub_category_id;
                $spanish->channel_id = $program->channel_id;
                $spanish->marketing_company_id = $program->marketing_company_id;
                $spanish->production_company_id = $program->product_company_id;
                $spanish->product_name = $program->product_name;
                $spanish->description = $program->description;
                $spanish->price = $program->price;
                $spanish->cost = $program->cost;
                $spanish->shipping_cost = $program->shipping_cost;
                $spanish->host = $program->host;
                $spanish->host_extra = $program->host_extra;
                $spanish->summary = $program->summary;
                $spanish->remarks = $program->remarks;
                $spanish->upsell = $program->upsell;
                $spanish->keywords = $program->keywords;
                $spanish->order_name = $program->order_name;
                $spanish->order_address = $program->order_address;
                $spanish->order_address_extra = $program->order_address_extra;
                $spanish->order_city = $program->order_city;
                $spanish->order_state = $program->order_state;
                $spanish->order_zip = $program->order_zip;
                $spanish->order_phone = $program->order_phone;
                $spanish->website = $program->website;
                $spanish->customer_svc_phone = $program->customer_svc_phone;
                $spanish->save();
            } else {
                $this->error('Program not found: '. $spanish->title);
            }
        }
    }
}
