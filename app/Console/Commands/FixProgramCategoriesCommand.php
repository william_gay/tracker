<?php

namespace App\Console\Commands;

use App\Models\CategoryMapping;
use App\Models\Program;
use App\Models\ProgramMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixProgramCategoriesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:fixprogramcats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix program Cats.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Disable query logging on this big db
        DB::connection()->disableQueryLog();
        DB::connection('legacy_im2')->disableQueryLog();

        $at_a_time = 10000;

        $number_queries = ceil(Program::whereNull('category_id')->count() / $at_a_time);
        $this->info('This script will run '.$number_queries.' queries!');
        $query = 0;
        $limit = 0;

        while ($query < $number_queries) {
            //Select the first batch
            $programs = Program::whereNull('category_id')->skip($limit)->take($at_a_time)->get();

            $this->comment('Query: '.$query.' Limit: '.$limit);

            foreach ($programs as $program) {
                //Get Program Mapping:
                $programmap = ProgramMapping::where('program_id', $program->id)->first();
                if (!$programmap) {
                    $this->error('Program Map Not Found! ID: '.$program->id.' Title:'.$program->title);
                } else {
                    //Lookup legacy Entry:
                    $oldprogram = DB::connection('legacy_im2')->select('SELECT legacy_im2.Program.Prog_ID, legacy_im2.Program.Master_Prog_ID, legacy_im2.Program.GridTitle, legacy_im2.Program_Detail.* FROM legacy_im2.Program JOIN legacy_im2.Program_Detail ON legacy_im2.Program_Detail.Prog_ID = legacy_im2.Program.Prog_ID WHERE legacy_im2.Program.Prog_ID = '.$programmap->im2_program_id);
                    $oldcat = $oldprogram[0]->Cat_ID;
                    $oldsubcat = $oldprogram[0]->SubCat_ID;

                    $catmap = CategoryMapping::where('im2_category_id', $oldcat)->whereNull('parent_id')->first();
                    $subcatmap = CategoryMapping::where('im2_category_id', $oldsubcat)->where('parent_id', '>', 0)->first();

                    if ($catmap and $subcatmap) {
                        //$this->info('New Cat: '.$catmap->category_id.' Old Cat: '.$oldcat.' New Sub Cat: '.$subcatmap->category_id.' Old Sub Cat: '.$oldsubcat);

                        $updatepg = Program::find($programmap->program_id);
                        $updatepg->category_id = $catmap->category_id;
                        $updatepg->sub_category_id = $subcatmap->category_id;
                        $updatepg->save();
                    } else {
                        $this->error('SUM TING WONG - ID: '.$program->id.' Title: '.$program->title.' Category ID: '. $oldcat.' Sub Category ID: '.$oldsubcat);
                    }
                }
            }

            $query++;
            $limit = $limit + $at_a_time;
        }
    }
}
