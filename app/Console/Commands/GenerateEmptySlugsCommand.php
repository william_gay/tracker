<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class GenerateEmptySlugsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:slugger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Sluggers Yo....';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $table = $this->argument('table');

        $this->info('Slugifying '.$table.'...');

        $rows = DB::table($table)->whereNull('slug')
            ->orWhere('slug', '')
            ->get();

        foreach ($rows as $row) {
            $slug = slugify($row->name);
            $this->info('Making Slug for '.$row->name.' Slug: '.$slug);
            DB::table($table)
                ->where('id', $row->id)
                ->update(['slug' => $slug]);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['table', InputArgument::REQUIRED, 'A database table to fix the slugs yo.'],
        ];
    }
}
