<?php

namespace App\Console\Commands;

use App\Models\OAuthClient;
use App\Models\OAuthClientEndpoint;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class DeleteAllOAuthClientsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'oauth:DeleteAllOAuthClients';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes all OAuth clients.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $clients = OAuthClient::all();
        $clientEndpoints = OAuthClientEndpoint::all();

        foreach ($clients as $client) {
            $client->clientEndpoint->delete();
            $client->delete();
        }
    }
}
