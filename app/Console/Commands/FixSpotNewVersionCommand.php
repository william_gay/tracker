<?php

namespace App\Console\Commands;

use App\Models\Spot;
use App\Models\SpotMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixSpotNewVersionCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:fixspotnewversion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Disable query logging on this big db
        DB::connection()->disableQueryLog();
        DB::connection('legacy_spot')->disableQueryLog();

        $at_a_time = 10000;

        $number_queries = ceil(Spot::where('new_version', 0)->orderBy('id')->count() / $at_a_time);
        $this->info('This script will run '.$number_queries.' queries!');
        $query = 0;
        $limit = 0;

        while ($query < $number_queries) {
            //Select the first batch
            $spots = Spot::where('new_version', 0)->orderBy('id')->skip($limit)->take($at_a_time)->get();
            $this->comment('Query: '.$query.' Limit: '.$limit);

            foreach ($spots as $spot) {
                //Get Program Mapping:
                $spotmap = SpotMapping::where('spot_id', $spot->id)->first();
                if (!$spotmap) {
                    $this->error('Spot Map Not Found! ID: '.$spot->id.' Title:'.$spot->title);
                } else {
                    //Lookup legacy Entry:
                    $oldspot = DB::connection('legacy_spot')->select('SELECT * FROM legacy_spot.`Spot Log` WHERE legacy_spot.`Spot Log`.ID = '.$spotmap->legacy_spot_id);

                    if ($oldspot) {
                        //$this->info('New Cat: '.$catmap->category_id.' Old Cat: '.$oldcat.' New Sub Cat: '.$subcatmap->category_id.' Old Sub Cat: '.$oldsubcat);

                        $updatespot = Spot::find($spotmap->spot_id);
                        $updatespot->new_version = $oldspot[0]->{'New Version'};
                        $updatespot->save();
                    } else {
                        $this->error('SUM TING WONG - ID: '.$spot->id.' Title: '.$spot->title);
                    }
                }
            }

            $query++;
            $limit = $limit + $at_a_time;
        }
    }
}
