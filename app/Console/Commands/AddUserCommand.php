<?php

namespace App\Console\Commands;

use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class AddUserCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sentry:add-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Welcome to the user generator.');

        $userdata['first_name']  = $this->ask('What is your First Name?');
        $userdata['last_name']   = $this->ask('What is your Last Name?');
        $userdata['email']       = $this->ask('What is your email?');
        $userdata['password']    = $this->secret('Enter a password?');
        $userdata['permissions'] = ['superuser' => 1];

        $user = Sentry::register($userdata, true);
        $this->info('<info>User ' . $user->first_name . ' added.</info>');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
