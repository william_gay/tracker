<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Product;
use App\Jobs\ProcessPresence;
use App\Jobs\WinmoCoverage;

class ProductFormatPresence extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:presence';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loop through products and update the last air date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::orderBy('id')->get();
        $products->each(function ($product) {
            ProcessPresence::dispatch($product)->onQueue('winmo');
        });

        $products = Product::groupBy('sub_category_id')->get();
        $products->each(function ($product) {
            WinmoCoverage::dispatch($product->sub_category_id)->onQueue('winmo-converage');
        });
    }
}
