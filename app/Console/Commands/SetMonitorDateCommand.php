<?php

namespace App\Console\Commands;

use App\Models\Program;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SetMonitorDateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:setmonitordate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the monitor date for programs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $programs = Program::whereNull('monitor_date')->get();

        foreach ($programs as $program) {
            $initial_date = $program->initial_date;

            $initialCarbon = Carbon::createFromFormat("Y-m-d H:i:s", $initial_date);

            $this->info($initialCarbon->format("Y-m-d H:i:s").' Week Ending: '.getWeekEnding($initialCarbon->format("Y-m-d H:i:s")));

            $program->monitor_date = getWeekEnding($initialCarbon->format("Y-m-d H:i:s"));
            $program->save();
        }
    }
}
