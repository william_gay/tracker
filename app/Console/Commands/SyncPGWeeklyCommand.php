<?php

namespace App\Console\Commands;

use App\Models\ProgramMapping;
use App\Models\ProgramWeekly;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncPGWeeklyCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:syncpgweekly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Program Weekly table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Disable query logging on this big db
        DB::connection()->disableQueryLog();
        DB::connection('legacy_im2')->disableQueryLog();

        $lim2_pgweekly = DB::connection('legacy_im2')->select('select * FROM `Program_History` WHERE DATE_SUB(CURDATE(),INTERVAL 2 WEEK) <= WeekEnding');

        foreach ($lim2_pgweekly as $lpgweekly) {
            $old_date = new DateTime($lpgweekly->WeekEnding);
            $just_date = $old_date->format('Y-m-d');
            $new_date = $just_date;

            $program_map = ProgramMapping::where('im2_program_id', $lpgweekly->Prog_ID)->first();
            if (! $program_map) {
                $this->error('No Program mapping found...');
                print_r($lpgweekly);
                exit;
            }

            //Check to see if it exists already:
            $exists = ProgramWeekly::where('program_id', $program_map->program_id)
                            ->where('week_ending', $new_date)->first();

            if (! $exists) {
                $program_weekly = new ProgramWeekly;
                $program_weekly->program_id = $program_map->program_id;
                $program_weekly->week_ending = $new_date;
                $program_weekly->freq_rank = $lpgweekly->FreqRank;
                $program_weekly->media_rank = $lpgweekly->MediaRank;
                $program_weekly->streak = $lpgweekly->Streak;
                $program_weekly->airs = $lpgweekly->Airs;
                $program_weekly->media_index = $lpgweekly->MediaIdx;
                $program_weekly->stations = $lpgweekly->Stations;
                $program_weekly->prev_freq_rank = $lpgweekly->PW_FreqRank;
                $program_weekly->prev_media_rank = $lpgweekly->PW_MediaRank;
                $program_weekly->save();
            }

            unset($program_map);
            unset($exists);
        }
    }
}
