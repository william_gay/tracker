<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class DownloadDatabasesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:dldbs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download the lastest Access Databases to import.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $bucket = 'ims-access';
        $friday = date('Y-m-d', strtotime("last friday"));

        $this->info("Downloading DBs from ".$friday);

        $s3 = App::make('aws')->get('s3');

        // $spotCommand = $s3->getCommand('GetObject',array(
        //     'Bucket' => $bucket,
        //     'Key' => $friday.'/Spot.mdb',
        //     'ResponseContentDisposition' => 'attachment; filename="Spot.mdb"'
        // ));
        $im2Command = $s3->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key' => $friday.'/IM2.mdb',
            'ResponseContentDisposition' => 'attachment; filename="IM2.mdb"'
        ]);

        //$spotUrl = $spotCommand->createPresignedUrl('+10 minutes');
        $im2Url = $im2Command->createPresignedUrl('+10 minutes');

        //$spotresult = file_put_contents(storage_path().'/ms-access/Spot.mdb',fopen($spotUrl, 'r'));

        //$this->info('Spot database downloaded!');

        $im2result = file_put_contents(storage_path().'/ms-access/IM2.mdb', fopen($im2Url, 'r'));

        $this->info('IM2 database downloaded!');
    }
}
