<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use League\Csv\Reader;
use League\Csv\Statement;

use App\Models\Product;
use App\Models\Spot;
use App\Models\Program;

class ImportWinmo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:winmo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Winmo IDs, create product mapping if missing.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reader = Reader::createFromPath(resource_path('imports/spot_airtimes_with_marketing-2020-01-02.csv'), 'r');

        $reader->setHeaderOffset(0);

        $header = collect($reader->getHeader()); //returns the CSV header record
        $header = $header->map(function ($item, $key) {
            return trim(preg_replace('/\s+/', ' ', $item));
        })->filter();

        $records = $reader->getRecords($header->toArray());

        foreach ($records as $offset => $record) {
            $this->line('Company: '.$record['company_name']);
            $this->line('Winmo Data -- Company ID: '.$record['Company ID'].' Brand ID: '.$record['Brand ID']);
            $this->line('Spot Name: '.$record['spot_name']);

            if ($record['Company ID'] == "" && $record['Brand ID'] == "") {
                $this->error('No Winmo ID');
                continue;
            }

            if ($record['Company ID'] == "NA" || $record['Brand ID'] == "NA") {
                $this->error('Company or Brand ID is NA');
                continue;
            }

            if (count(explode(',', $record['Brand ID'])) > 1) {
                // Cannot import these automatically
                \Log::channel('winmo')->info($record);
                continue;
            }

            $products = Product::with('spots')->where('winmo_company_id', $record['Company ID'])
                ->orWhere('winmo_brand_id', $record['Brand ID'])
                ->whereNotNull('winmo_brand_id')
                ->whereNotNull('winmo_company_id')
                ->where('winmo_brand_id', '!=', 0)
                ->where('winmo_company_id', '!=', 0)
                ->get();

            $spotList = explode(',', $record['spot_name']);

            $spots = Spot::whereIn('title', $spotList)->get();
            $programs = Program::whereIn('title', $spotList)->get();

            if ($spots->isEmpty() && $programs->isEmpty()) {
                // Don't create the product because we don't have it
                continue;
            }

            if (!$products->isEmpty()) {
                $this->info('Already Mapped!');

                $this->line('Product Count: '.$products->count());

                $product = $products->first();
                $this->line('Product Spot Count: '.$product->spots->count());

                if ($spots) {
                    $product->spots()->syncWithoutDetaching($spots->pluck('id'));
                }

                if ($programs) {
                    $product->programs()->syncWithoutDetaching($programs->pluck('id'));
                }

                foreach ($product->spots as $spot) {
                    $this->line('Spot: '.$spot->id.' '.$spot->title);
                }
                foreach ($product->programs as $program) {
                    $this->line('Program: '.$program->id.' '.$program->title);
                }
            } else {
                if ($spots->isEmpty() && $programs->isEmpty()) {
                    $this->error('Program and Spot Not Found!');
                    continue;
                }

                if ($spots->first()) {
                    $category_id = $spots->first()->category_id;
                    $sub_category_id = $spots->first()->sub_category_id;
                } else {
                    $category_id = $programs->first()->category_id;
                    $sub_category_id = $programs->first()->sub_category_id;
                }

                $product = Product::create([
                    'name' => $record['spot_name'],
                    'slug' => slugify($record['spot_name']),
                    'winmo_company_id' => $record['Company ID'],
                    'winmo_brand_id' => $record['Brand ID'],
                    'category_id' => $category_id,
                    'sub_category_id' => $sub_category_id,
                ]);

                if ($spots) {
                    $product->spots()->syncWithoutDetaching($spots->pluck('id'));
                }

                if ($programs) {
                    $product->programs()->syncWithoutDetaching($programs->pluck('id'));
                }

                $this->info('Product Created -- ID: '.$product->id.' Name: '.$product->name);
            }

            $this->line('-------------------------------------');
        }

    }
}
