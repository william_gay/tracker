<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Alert;
use App\Models\Category;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\Spot;
use App\Models\SpotVersion;
use App\Mail\AlertCategory;
use App\Mail\AlertShow;
use App\Mail\AlertSpot;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class SendOutAlertsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:sendalerts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alerts MOTHER FUCKER.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $alerts = Alert::where('interval', 'weekly')->get();

        if (Carbon::now()->isFriday()) {
            $week_ending = Carbon::now();
        } else {
            $week_ending = new Carbon('last friday');
        }

        foreach ($alerts as $alert) {
            try {
                $user = Sentry::getUserProvider()->findById($alert->user_id);
            } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
                //Delete this alert cause the user doesn't exist
                $alert->delete();

                //Skip to the next iteration...
                continue;
            }

            switch ($alert->type) {
                case "category":
                    $this->category($user, $alert, $week_ending);
                    break;
                case "shows":
                    $this->shows($user, $alert, $week_ending);
                    break;
                case "spots":
                    $this->spots($user, $alert, $week_ending);
                    break;
            }
        }
    }

    public function category($user, $alert, $week_ending)
    {
        $category = Category::where('id', $alert->type_extra)->first();

        $shows = ProgramVersion::select([
                'id',
                'title',
                'version',
            ])
            ->where('category_id', $alert->type_extra)
            ->where('monitor_date', $week_ending->format("Y-m-d"))
            ->where('language_id', 1)
            ->get();

        $spots = SpotVersion::select([
                'id',
                'title',
                'version',
                'service',
                'non_dr',
            ])
            ->where('category_id', $alert->type_extra)
            ->where('air_date', $week_ending->format("Y-m-d"))
            ->where('language_id', 1)
            ->get();

        $data = [
            'user' => $user,
            'alert' => $alert,
            'week_ending' => $week_ending,
            'category' => $category,
            'shows' => $shows,
            'spots' => $spots,
            'subject' => __('alerts.category_subject')." / ".$week_ending->format("m-d-y"),
        ];

        if ($shows->count() > 0 or $spots->count() > 0) {
            //Send the email...
            Mail::to($user->email, $user->first_name.' '.$user->last_name)
                ->queue(new AlertCategory($user, $data));
        }
    }

    public function shows($user, $alert, $week_ending)
    {
        $shows = ProgramVersion::select([
                'id',
                'title',
                'version',
            ])
            ->where('category_id', $alert->type_extra)
            ->where('monitor_date', $week_ending->format("Y-m-d"))
            ->where('language_id', 1)
            ->orderBy('title')
            ->get();

        $data = [
            'user' => $user,
            'alert' => $alert,
            'week_ending' => $week_ending,
            'shows' => $shows,
            'subject' => __('alerts.show_subject')." / ".$week_ending->format("m-d-y"),
        ];

        if ($shows->count() > 0) {
            //Send the email...
            Mail::to($user->email, $user->first_name.' '.$user->last_name)
                ->queue(new AlertShow($user, $data));
        }
    }

    public function spots($user, $alert, $week_ending)
    {
        $spots = SpotVersion::select([
                'id',
                'title',
                'version',
                'service',
                'non_dr',
            ])
            ->where('air_date', $week_ending->format("Y-m-d"))
            ->where('language_id', 1)
            ->orderBy('title')
            ->get();

        $data = [
            'user' => $user,
            'alert' => $alert,
            'week_ending' => $week_ending,
            'spots' => $spots,
            'subject' => __('alerts.spots_subject')." / ".$week_ending->format("m-d-y"),
        ];

        if ($spots->count() > 0) {
            //Send the email...
            Mail::to($user->email, $user->first_name.' '.$user->last_name)
                ->queue(new AlertSpot($user, $data));
        }
    }
}
