<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class QueueFailedMassRetry extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'queue:retry-range';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Supply start and end ID and it will add them all back to the queue.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = $this->option('startId');
        $end = $this->option('endId');

        while ($start <= $end) {
            $this->call('queue:retry', ['id' => $start]);

            $start++;
        }
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['startId', null, InputOption::VALUE_REQUIRED, 'ID to start with.', null],
            ['endId', null, InputOption::VALUE_REQUIRED, 'ID to end with.', null],
        ];
    }
}
