<?php

namespace App\Console\Commands;

use App\Models\Company;
use App\Models\CompanyMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncCompaniesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:synccompanies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync companies between legacy_im2, legacy_spot to ims.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $company_abr = [
            'C' => 1,
            'F' => 2,
            'M' => 3,
            'P' => 4,
            'O' => 5,
            'MP' => 'woot'
        ];

        //Get all legacy_im2 companies:
        $lim2_co = DB::connection('legacy_im2')->select('select * FROM `Companies` WHERE DATE_SUB(CURDATE(),INTERVAL 2 WEEK) <= updated_at');

        foreach ($lim2_co as $co) {
            $company = Company::where('name', $co->Name)->first();

            if (! $company) {
                $company = new Company;
                $company->name = $co->Name;
                $company->address = $co->Addr1;
                $company->address_extra = $co->Addr2;
                $company->city = $co->City;
                $company->state = $co->ST;
                $company->zip = $co->ZIP;
                $company->phone = $co->Phone;
                $company->fax = $co->Fax;
                $company->contact = $co->Contact;
                $company->email = $co->Email;
                $company->website = $co->WebSite;
                $company->remarks = $co->Remarks;
                $company->save();

                if (array_key_exists($co->Type, $company_abr)) {
                    if ($company_abr[$co->Type] == 'MP') {
                        DB::insert('insert into company_type (company_id, type_id) values (?, ?)', [$company->id, 3]);
                        DB::insert('insert into company_type (company_id, type_id) values (?, ?)', [$company->id, 4]);
                    } else {
                        DB::insert('insert into company_type (company_id, type_id) values (?, ?)', [$company->id, $company_abr[$co->Type]]);
                    }
                } else {
                    echo 'Type Not Found: '.$co->Type.' ID: '.$co->Co_ID;
                }

                CompanyMapping::create([
                    'company_id' => $company->id,
                    'im2_company_id' => $co->Co_ID
                ]);
            }

            unset($company);
        }

        // // Get Fulfillment Copmanies from legacy_spot database
        // $lspot_fco = DB::connection('legacy_spot')->select('select * FROM `Fulfillment Companies`');

        // foreach ($lspot_fco as $fco) {

        //     $company = Company::where('name','=',$fco->{'Fulfillment Company'})->first();

        //     if ($company) {
        //         $this->comment('Company already exists: '. $fco->{'Fulfillment Company'});
        //         //Let's do some logic to update the tables:
        //         $company->address = $company->address == '' ? $fco->{'Street Address'} : $company->address;
        //         $company->address_extra = $company->address_extra == '' ? $fco->{'Address'} : $company->address_extra;
        //         $company->city = $company->city == '' ? $fco->City : $company->city;
        //         $company->state = $company->state == '' ? $fco->State : $company->state;
        //         $company->zip = $company->zip == '' ? $fco->Zip : $company->zip;
        //         $company->phone = $company->phone == '' ? $fco->Phone : $company->phone;
        //         $company->fax = $company->fax == '' ? $fco->Fax : $company->fax;
        //         $company->contact = $company->contact == '' ? $fco->Contact : $company->contact;
        //         $company->email = $company->email == '' ? $fco->Email : $company->email;
        //         $company->website = $company->website == '' ? $fco->Website : $company->website;
        //         $company->save();
        //     } else {
        //         $company = new Company;
        //         $company->name = $fco->{'Fulfillment Company'};
        //         $company->address = $fco->{'Street Address'};
        //         $company->address_extra = $fco->{'Address'};
        //         $company->city = $fco->City;
        //         $company->state = $fco->State;
        //         $company->zip = $fco->Zip;
        //         $company->phone = $fco->Phone;
        //         $company->fax = $fco->Fax;
        //         $company->contact = $fco->Contact;
        //         $company->email = $fco->Email;
        //         $company->website = $fco->Website;
        //         $company->save();

        //         DB::insert('insert into company_type (company_id, type_id) values (?, ?)', array($company->id, 2));

        //     }

        //     unset($company);

        // }

        // // Get Marketing Copmanies from legacy_spot database
        // $lspot_mco = DB::connection('legacy_spot')->select('select * FROM `Marketing Companies`');

        // foreach ($lspot_mco as $mco) {

        //     $company = Company::where('name',$mco->{'Marketing Company'})->first();

        //     if ($company) {
        //         $this->comment('Company already exists: '. $mco->{'Marketing Company'});
        //         //Logic to update stuff
        //         $company->address = $company->address == '' ? $mco->{'Address'} : $company->address;
        //         $company->address_extra = $company->address_extra == '' ? $mco->{'Address 2'} : $company->address_extra;
        //         $company->city = $company->city == '' ? $mco->City : $company->city;
        //         $company->state = $company->state == '' ? $mco->State : $company->state;
        //         $company->zip = $company->zip == '' ? $mco->Zip : $company->zip;
        //         $company->phone = $company->phone == '' ? $mco->Phone : $company->phone;
        //         $company->fax = $company->fax == '' ? $mco->Fax : $company->fax;
        //         $company->contact = $company->contact == '' ? $mco->Contact : $company->contact;
        //         $company->title = $company->title == '' ? $mco->Title : $company->title;
        //         $company->email = $company->email == '' ? $mco->email : $company->email;
        //         $company->website = $company->website == '' ? $mco->{'Web Address'} : $company->website;
        //         $company->save();
        //     } else {
        //         $company = new Company;
        //         $company->name = $mco->{'Marketing Company'};
        //         $company->address = $mco->{'Address'};
        //         $company->address_extra = $mco->{'Address 2'};
        //         $company->city = $mco->City;
        //         $company->state = $mco->State;
        //         $company->zip = $mco->Zip;
        //         $company->phone = $mco->Phone;
        //         $company->fax = $mco->Fax;
        //         $company->contact = $mco->Contact;
        //         $company->title = $mco->Title;
        //         $company->email = $mco->email;
        //         $company->website = $mco->{'Web Address'};
        //         $company->save();

        //         DB::insert('insert into company_type (company_id, type_id) values (?, ?)', array($company->id, 3));

        //     }

        //     unset($company);

        // }
    }
}
