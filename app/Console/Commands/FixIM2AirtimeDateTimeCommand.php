<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixIM2AirtimeDateTimeCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:airtimedatetime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Consolidates the date and the time field in the airtime table to one datetime field.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        DB::connection('legacy_im2')->disableQueryLog();
        DB::connection('legacy_im2')->update('update `Airtime` SET `fullDatetime` = CONCAT(`dDate`,\' \',`dTime`) where `fullDatetime` = ?', ['0000-00-00 00:00:00']);
    }
}
