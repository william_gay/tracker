<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncLegacyCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:synclegacy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync all the tables BITCH.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Syncing Companies...');
        $this->call('ims:synccompanies');

        $this->info('Syncing Channels...');
        $this->call('ims:syncchannels');

        $this->info('Syncing Costs...');
        $this->call('ims:synccosts');

        $this->info('Syncing Programs...');
        $this->call('ims:syncprograms');

        $this->info('Syncing Airtime...');
        $this->call('ims:syncairtime');

        $this->info('Syncing Program Weekly...');
        $this->call('ims:syncpgweekly');

        // $this->info('Syncing Spots...');
        // $this->call('ims:syncspotlog');

        // $this->info('Syncing Spot Detections...');
        // $this->call('ims:syncspotdetections');

        // $this->info('Syncing Spot Rankings...');
        // $this->call('ims:syncspotranking');
    }
}
