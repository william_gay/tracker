<?php

namespace App\Console\Commands;

use App\Models\Airtime;
use App\Models\Cost;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class FixMissingAirtimeCostsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:fixmissingairtimecosts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Some Airtimes were entered without cost data, this loops through and fixes this issue.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $end = Carbon::createFromFormat('Y-m-d', $this->argument('end'));
        $end->setTimezone('America/New_York')->endOfDay();

        $start = Carbon::createFromFormat('Y-m-d', $this->argument('start'));
        $start->setTimezone('America/New_York')->startOfDay();

        $airtimes = Airtime::whereBetween('air_date', [$start->toDateTimeString(), $end->toDateTimeString()])
            ->where('cost', 0)
            ->orderBy('air_date')
            ->get();

        $this->info('Updating '.$airtimes->count().' Airtimes!');

        foreach ($airtimes as $airtime) {
            $this->info('Old Cost: '.$airtime->cost);

            $airtime->cost = $this->getCost($airtime->channel_id, $airtime->air_date);
            $airtime->save();

            $this->info('New Cost: '.$airtime->cost);
        }

        $this->info('Done!');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['start', InputArgument::REQUIRED, 'Enter Ending Date. Format: YYYY-MM-DD'],
            ['end', InputArgument::REQUIRED, 'Enter Start Date. Format:: YYYY-MM-DD'],
        ];
    }

    private function getCost($channel_id, $air_date)
    {
        $air_date = Carbon::createFromFormat('Y-m-d H:i:s', $air_date);

        if ($air_date->dayOfWeek == 0) {
            $dayOfWeek = 7;
        } else {
            $dayOfWeek = $air_date->dayOfWeek;
        }

        $cost = Cost::where('archive', 0)
            ->where('channel_id', $channel_id)
            ->where('day', '<=', $dayOfWeek)
            ->where('time_slot', '<=', $air_date->format('H:i:s'))
            ->orderBy('time_slot', 'desc')
            ->orderBy('day', 'desc')
            ->first();

        if ($cost) {
            return $cost->amount;
        } else {
            return 0;
        }
    }
}
