<?php

namespace App\Console\Commands;

use App\Models\LiveShoppingTracker\LiveShoppingAirtimeProduct;
use App\Models\LiveShoppingTracker\LiveShoppingProduct;
use App\Models\LiveShoppingTracker\LiveShoppingProductVersion;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class FixLSTProducts extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'lst:fix-products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove duplicates with spaces.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->trimeWhitespaces();
        $this->cleanupMissingStartTime();

        $products = LiveShoppingProduct::select([
                'live_shopping_products.*',
                DB::raw('COUNT(id) as dups')
            ])
            ->groupBy('name')
            ->having('dups', '>', 1)
            ->get();

        $this->info("Total Products: ".$products->count());

        foreach ($products as $product) {
            $this->info("'".$product->name."'");
            $trimmed = trim($product->name);
            $this->info("   -> ID: ".$product->id);
            $this->info("   -> SKU: ".$product->sku);
            $this->info("   -> Trimmed: `".$trimmed."`");
            $this->info("   -> Versions: ".$product->versions->count());

            $this->figuredItOut($trimmed);

            $this->error("----------------------------");
        }

        // Some products have a first_airdate after the versions
        $this->correctIncorrectStartTime();
    }

    protected function trimeWhitespaces()
    {
        DB::update('update live_shopping_products set name = TRIM(name)');
        DB::update('update live_shopping_product_versions set name = TRIM(name)');
    }

    protected function cleanupMissingStartTime()
    {
        LiveShoppingAirtimeProduct::whereNull('start_time')->delete();
    }

    protected function correctIncorrectStartTime()
    {
        $products = LiveShoppingProduct::leftJoin('live_shopping_product_versions', 'live_shopping_product_versions.live_shopping_product_id', '=', 'live_shopping_products.id')
            ->where('live_shopping_product_versions.first_airdate', '<', 'live_shopping_products.first_airdate')
            ->get();

        foreach ($products as $product) {
            $versions = $product->versions()->pluck('id');
            if ($versions) {
                $first_airtime = LiveShoppingAirtimeProduct::whereIn('live_shopping_product_version_id', $versions)
                    ->orderBy('start_time', 'asc')
                    ->first();

                if ($first_airtime) {
                    $this->info('Product: '.$product->id.' -- '.$product->name);
                    $this->info('Product Airtime: '.$product->first_airdate);
                    $this->info('First Airtime: '.$first_airtime->start_time);

                    if ($product->first_airdate->ne($first_airtime->start_time)) {
                        $this->info('Product airing does not match actual, fixing...');
                        $product->first_airdate = $first_airtime->start_time;
                        $product->save();

                        $version = LiveShoppingProductVersion::where('live_shopping_product_id', $product->id)
                            ->orderBy('created_at', 'asc')
                            ->first();

                        $this->info('Version Airtime: '.$version->first_airdate);
                        if ($version->first_airdate->ne($product->first_airdate)) {
                            $this->info('Version airing does not match actual, fixing...');
                            $version->first_airdate = $product->first_airdate;
                            $version->save();
                        }
                    }

                    $this->info('_________________');
                }
            }
        }
    }

    protected function figuredItOut($name)
    {
        $this->info('Look Up the products');
        $products = LiveShoppingProduct::where('name', $name)->orderBy('first_airdate')->pluck('id');
        $mainProduct = $products[0];
        $removeProduct = $products;
        // Remove the 1st one, since we're using it
        unset($removeProduct[0]);

        $this->info('Total Products: '.count($products));
        $versions = LiveShoppingProductVersion::whereIn('live_shopping_product_id', $products)->pluck('id');
        $this->info('Total Version: '.count($versions));
        $airtimes = LiveShoppingAirtimeProduct::whereIn('live_shopping_product_version_id', $versions)
            ->orderBy('start_time', 'asc')
            ->get();

        $this->info('Total Airtimes: '. $airtimes->count());
        if ($airtimes->count() == 0) {
            // No Airtimes, delete it, doesn't matter
            LiveShoppingProduct::whereIn('id', $products)->delete();
            LiveShoppingProductVersion::whereIn('id', $versions)->delete();
            return;
        }

        foreach ($airtimes as $airtime) {
            $firstAirdate = $airtime->start_time;
            $this->info('First Airtime: '.$firstAirdate);
            break;
        }

        // Update the products first_airdate to match the actual airdate
        LiveShoppingProduct::where('id', $mainProduct)
            ->update(['first_airdate' => $firstAirdate]);
        // Update all the versions to point to the new main version
        LiveShoppingProductVersion::whereIn('id', $versions)
            ->update(['live_shopping_product_id' => $mainProduct]);

        // Remove other duplicate products
        LiveShoppingProduct::whereIn('id', $removeProduct)
            ->delete();
    }

    // No Longer Used
    protected function figureItOut($name)
    {
        $products = LiveShoppingProduct::where('name', $name)->get();
        foreach ($products as $product) {
            $this->info(json_encode($product));
            $this->info("Verisons: ".$product->versions->count());
            $this->info("============");
        }

        if ($this->confirm('Do you wish to delete this product? [yes|no]', false)) {
            foreach ($products as $product) {
                $product->delete();
                $this->info("Deleted!");
            }

            // Go back to the next one
            return;
        }

        $primaryProduct = $this->ask('What should we use as primary product (ID)?', $products->first()->id);
        $primaryDate = $this->ask('What should we use as primary first aired (date)?', $products->first()->first_airdate);

        // Update the first_airdate
        $product = LiveShoppingProduct::find($primaryProduct);
        if ($product) {
            $product->name = $name;
            $product->first_airdate = $primaryDate;
            $product->save();

            $productsToMerge = LiveShoppingProduct::where('name', $name)
                ->where('id', '!=', $product->id)
                ->pluck('id');

            $versionToMerge = LiveShoppingProductVersion::whereIn('live_shopping_product_id', $productsToMerge)->get();

            $this->info('Versions: '. $versionToMerge->count());
            foreach ($versionToMerge as $version) {
                $this->info(json_encode($version));
                $this->info("Updated =====");
                $version->name = $name;
                $version->live_shopping_product_id = $product->id;
                $version->save();
            }

            // Delete the other products:
            LiveShoppingProduct::where('name', $name)
                ->where('id', '!=', $product->id)
                ->delete();
        } else {
            $this->error('Couldn\'t find product! SOS');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
