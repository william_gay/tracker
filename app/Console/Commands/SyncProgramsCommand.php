<?php

namespace App\Console\Commands;

use App\Models\CategoryMapping;
use App\Models\ChannelMapping;
use App\Models\CompanyMapping;
use App\Models\Program;
use App\Models\ProgramMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncProgramsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:syncprograms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Programs Table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $lim2_programs = DB::connection('legacy_im2')->select('SELECT legacy_im2.Program.Prog_ID, legacy_im2.Program.Master_Prog_ID, legacy_im2.Program.GridTitle, legacy_im2.Program_Detail.* FROM legacy_im2.Program JOIN legacy_im2.Program_Detail ON legacy_im2.Program_Detail.Prog_ID = legacy_im2.Program.Prog_ID WHERE DATE_SUB(CURDATE(),INTERVAL 2 WEEK) <= legacy_im2.Program.updated_at');

        foreach ($lim2_programs as $lprogram) {
            $master_id = null;
            $version = 1;
            //Look up master ID in mapping table
            if ($lprogram->Master_Prog_ID > 0) {
                $id_map = ProgramMapping::where('im2_program_id', $lprogram->Master_Prog_ID)->first();
                if ($id_map) {
                    $master_id = $id_map->program_id;
                    //$version = substr($lprogram->GridTitle, -1);
                    $findversion = Program::where('master_id', $master_id)->get();
                    $versioncount = $findversion->count();
                    $this->info('Grid Title: '.$lprogram->GridTitle.' Title:'.$lprogram->Title);
                    $this->info('Version Count'.$versioncount);

                    $version = $findversion->count() + 2;
                }
            }

            $marketing_company_id = 0;
            if ($lprogram->Mkt_Co_ID != 0) {
                $mco_map = CompanyMapping::where('im2_company_id', $lprogram->Mkt_Co_ID)->first();
                if ($mco_map) {
                    $marketing_company_id = $mco_map->company_id;
                }
            }

            $production_company_id = 0;
            if ($lprogram->Prod_Co_ID != 0) {
                $pco_map = CompanyMapping::where('im2_company_id', $lprogram->Prod_Co_ID)->first();
                if ($pco_map) {
                    $production_company_id = $pco_map->company_id;
                }
            }

            $category_id = null;
            $sub_category_id = null;
            if ($lprogram->SubCat_ID != 0) {
                $subcat_map = CategoryMapping::where('im2_category_id', $lprogram->SubCat_ID)->first();
                if ($subcat_map) {
                    $category_id = $subcat_map->parent_id;
                    $sub_category_id = $subcat_map->category_id;
                }
            }

            $channel_id = null;
            if ($lprogram->Origin_Chan_ID != 0) {
                $chan_map = ChannelMapping::where('im2_channel_id', $lprogram->Origin_Chan_ID)->first();
                if ($chan_map) {
                    $channel_id = $chan_map->channel_id;
                }
            }

            $old_origin_date = new DateTime($lprogram->Origin_Date);
            $just_date = $old_origin_date->format('Y-m-d');
            $old_origin_time = new DateTime($lprogram->Origin_Time);
            $just_time = $old_origin_time->format('H:i:s');
            $new_datetime = $just_date.' '.$just_time;

            //Check to see if it exists already:
            $exists = Program::where('master_id', $master_id)
                            ->where('title', $lprogram->Title)
                            ->where('initial_date', $new_datetime)
                            ->where('product_name', $lprogram->Product_Name)->first();

            if (! $exists) {
                $program = new Program;
                $program->title = $lprogram->Title;
                $program->grid_title = $lprogram->GridTitle;
                $program->version = $version;
                $program->master_id = $master_id;
                $program->category_id = $category_id;
                $program->sub_category_id = $sub_category_id;
                $program->channel_id = $channel_id;
                $program->marketing_company_id = $marketing_company_id;
                $program->production_company_id = $production_company_id;
                $program->initial_date = $new_datetime;
                $program->monitor_date = $lprogram->Rpt_Date;
                $program->product_name = $lprogram->Product_Name;
                $program->description = $lprogram->Product_Desc;
                $program->price = $lprogram->Product_Cost;
                $program->host = $lprogram->Host;
                $program->host_extra = $lprogram->Host2;
                $program->summary = $lprogram->Show_Summary;
                $program->remarks = $lprogram->Remarks;
                $program->upsell = $lprogram->Ord_Upsell;
                $program->keywords = $lprogram->Keywords;
                $program->order_name = $lprogram->Ord_Name;
                $program->order_address = $lprogram->Ord_Addr1;
                $program->order_address_extra = $lprogram->Ord_Addr2;
                $program->order_city = $lprogram->Ord_City;
                $program->order_state = $lprogram->Ord_ST;
                $program->order_zip = $lprogram->Ord_ZIP;
                $program->order_phone = $lprogram->Ord_Phone;
                $program->website = $lprogram->Ord_Website;
                $program->customer_svc_phone = $lprogram->Cust_Svc;
                $program->language_id = 1; //English
                $program->save();

                ProgramMapping::create([
                    'program_id' => $program->id,
                    'im2_program_id' => $lprogram->Prog_ID
                ]);
            }

            unset($program);
            unset($version);
            unset($exists);
            unset($$chan_map);
            unset($id_map);
            unset($mco_map);
            unset($pco_map);
            unset($subcat_map);
            unset($new_datetime);
        }
    }
}
