<?php

namespace App\Console\Commands;

use App\Models\Airtime;
use App\Models\ChannelMapping;
use App\Models\ProgramMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixAirtimeCostCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:fixairtimecost';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix the costs of the airtimes table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Disable query logging on this big db
        DB::connection()->disableQueryLog();
        DB::connection('legacy_im2')->disableQueryLog();

        $query = 0;
        $at_a_time = 300000;
        $limit = 0;

        //$total_rows = DB::connection('legacy_im2')->select('select count(Air_ID) as count FROM `Airtime` WHERE cost_updated IS NULL or cost_updated = 0 ORDER BY Air_ID asc ');
        //$airtimes = Airtime::whereNull('cost')->whereNull('user_id')->whereNotNull('program_id');
        $airtimes = Airtime::whereNotNull('program_id')->orderBy('air_date');
        $this->info('Total Rows: '.$airtimes->count());
        $number_queries = ceil($airtimes->count() / $at_a_time);
        $this->info('This script will run '.$number_queries.' queries!');

        while ($query < $number_queries) {
            //$lim2_airtime = DB::connection('legacy_im2')->select('select * FROM `Airtime` WHERE cost_updated  IS NULL or cost_updated = 0 ORDER BY Air_ID asc LIMIT '.$limit.', '.$at_a_time);
            $airtimes = Airtime::whereNotNull('program_id')->orderBy('air_date')->skip($limit)->take($at_a_time)->get();

            $this->comment('Query: '.$query.' Limit: '.$limit);

            foreach ($airtimes as $air) {
                $legacy_channel_id = null;
                $chan_map = ChannelMapping::where('channel_id', $air->channel_id)->first();
                if ($chan_map) {
                    $legacy_channel_id = $chan_map->im2_channel_id;
                }

                $legacy_program_id = null;
                $pro_map = ProgramMapping::where('program_id', $air->program_id)->first();
                if ($pro_map) {
                    $legacy_program_id = $pro_map->im2_program_id;
                }

                $exists = DB::connection('legacy_im2')->select('select Air_ID, Cost FROM `Airtime` WHERE Chan_ID = ? AND Prog_ID = ? AND fullDateTime = ?', [$legacy_channel_id, $legacy_program_id, $air->air_date]);

                if ($exists) {
                    if ($exists[0]->Cost != $air->cost) {
                        $this->info('New Cost: '.$exists[0]->Cost. ' Old Cost: '.$air->cost.' Old Airtime ID: '.$exists[0]->Air_ID.' New Airtime ID: '.$air->id);
                        $air->cost = $exists[0]->Cost;
                        $air->save();
                    }
                } else {
                    //$this->info('Airtime does not exist! Airtime ID: '. $air->id);
                }
                unset($exists);
            }
            $query++;
            $limit = $limit + $at_a_time;
        }
    }
}
