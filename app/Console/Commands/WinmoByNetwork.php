<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Channel;
use App\Jobs\WinmoNetworkTopProducts;

class WinmoByNetwork extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'winmo:networks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate Top Networks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $channels = Channel::active()->get();
        $channels->each(function ($channel) {
            WinmoNetworkTopProducts::dispatch($channel)->onQueue('winmo');
        });
    }
}
