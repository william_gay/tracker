<?php

namespace App\Console\Commands;

use App\Models\Spot;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class InsertSpotMasterIdCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:spotmasterid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Enter Spot Master ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $spots = Spot::orderBy('title')->get();

        foreach ($spots as $spot) {
            $this->info("Updating Spot: ".$spot->id.' - '.$spot->title);
            if ($spot->version == 1) {
                $master_id = $spot->id;

                $this->info('Master ID: '.$master_id);

                $updateSpots = Spot::where('version', '!=', 1)
                    ->where('title', $spot->title)
                    ->update(['master_id' => $master_id]);
            }
        }
    }
}
