<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use App\Models\OAuthClient;
use App\Models\OAuthClientEndpoint;
use App\Models\Server;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CreateOAuthClientForAllServersCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'oauth:createClientsForAllCaptureServers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates an individual OAuth Client for every capture server that is currently designated as Active.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Get all servers
        $servers = Server::where('active', '=', 1)->get();

        foreach ($servers as $server) {
        //Instantiate new objects
            $client = new OAuthClient();
            $clientEndpoint = new OAuthClientEndpoint();

            //create a reusable random string
            $clientIDRandom = Str::random(5);
            $clientSecretRandom = Str::random(8);

            //Start assingin' some variables!
            $client->name                  = $server->name;
            $client->id                    = $clientIDRandom;
            $client->secret                = $clientSecretRandom;
            $clientEndpoint->client_id     = $clientIDRandom;
            $clientEndpoint->redirect_uri  = $server->name.'.monitor.imsreport.com/oauth/authorize/mediaanalytics';

            //Save 'em!
            $client->save();
            $clientEndpoint->save();
        }
    }
}
