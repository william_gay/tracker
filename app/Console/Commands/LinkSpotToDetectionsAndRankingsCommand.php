<?php

namespace App\Console\Commands;

use App\Models\Spot;
use App\Models\SpotRanking;
use App\Models\SpotVersion;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class LinkSpotToDetectionsAndRankingsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'spots:fix_detections_rankings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to fix the rankings and detections to point to the spot table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Updating Rankings...');
        $rankings = SpotRanking::orderBy('id')->get();

        foreach ($rankings as $ranking) {
            $spot = Spot::find($ranking->spot_id);

            if (! $spot) {
                $spot = SpotVersion::where('id', $ranking->spot_id)->first();

                //$this->info('Whoa, parent version not linked... updating...');

                if ($spot) {
                    $ranking->spot_id = $spot->spot_id;
                    $ranking->spot_version_id = $ranking->spot_id;
                }
            }

            $ranking->spot_version_id = $ranking->spot_id;
            $ranking->save();
        }
    }
}
