<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Channel;
use App\Models\Company;
use App\Models\Spot;
use App\Models\SpotMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class FixSpotMappingCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:fixspotmapping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finish Spot Mappings table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        DB::connection()->disableQueryLog();
        DB::connection('legacy_spot')->disableQueryLog();

        $at_a_time = 10000;

        $total_rows = DB::connection('legacy_spot')->select('select count(ID) as count FROM legacy_spot.`Ranking` WHERE ID > 15011 ORDER BY `ID` asc');
        $number_queries = ceil($total_rows[0]->count / $at_a_time);
        $this->info('This script will run '.$number_queries.' queries!');
        $query = 0;
        $limit = 0;

        while ($query < $number_queries) {
            $lspots = DB::connection('legacy_spot')->select('SELECT * FROM legacy_spot.`Spot Log` WHERE ID > 15011 ORDER BY `ID` asc LIMIT '.$limit.', '.$at_a_time);

            $this->comment('Query: '.$query.' Limit: '.$limit);

            foreach ($lspots as $lspot) {
                //Look up possible match for new Spot
                $exists = Spot::where('title', $lspot->{'Product Name'})
                    ->where('version', $lspot->Version)
                    //->where('length', $this->getLength($lspot->{'Length'}))
                    ->where('air_date', $lspot->{'Date'})->first();

                if (! $exists) {
                    $this->comment('SOS We don\'t have this in the DB! '.$lspot->ID.' '.$lspot->{'Product Name'});
                } else {
                    $mapExists = SpotMapping::where('spot_id', $exists->id)
                        ->where('legacy_spot_id', $lspot->ID)
                        ->first();

                    if (! $mapExists) {
                        SpotMapping::create([
                            'spot_id' => $exists->id,
                            'legacy_spot_id' => $lspot->ID
                        ]);
                    }
                }

                unset($exists);
            }

            $query++;
            $limit = $limit + $at_a_time;
        }
    }

    private function getLength($legacyLength)
    {
        $lengthmap = [
            '120' => 120,
            '5 min' => 300,
            '5 min.' => 300,
            ':10' => 10,
            ':120' => 120,
            ':1200' => 1200,
            ':15' => 15,
            ':150' => 150,
            ':180' => 180,
            ':20' => 20,
            ':220' => 220,
            ':240' => 240,
            ':30' => 30,
            ':300' => 300,
            ':40' => 40,
            ':45' => 45,
            ':50' => 50,
            ':60' => 60,
            ':75' => 75,
            ':80' => 80,
            ':90' => 90
        ];

        if (array_key_exists($legacyLength, $lengthmap)) {
            $length = $lengthmap[$legacyLength];
        } else {
            $length = null;
        }

        return $length;
    }
}
