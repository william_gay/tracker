<?php

namespace App\Console\Commands;

use App\Models\SpotMapping;
use App\Models\SpotRanking;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncSpotRankingCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:syncspotranking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Spot Ranking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Disable query logging on this big db
        DB::connection()->disableQueryLog();
        DB::connection('legacy_spot')->disableQueryLog();

        $at_a_time = 10000;

        $total_rows = DB::connection('legacy_spot')->select('select count(ID) as count FROM legacy_spot.`Ranking` WHERE DATE_SUB(CURDATE(),INTERVAL 2 WEEK) <= `Rank Date`');
        $number_queries = ceil($total_rows[0]->count / $at_a_time);
        $this->info('This script will run '.$number_queries.' queries!');
        $query = 0;
        $limit = 0;

        while ($query < $number_queries) {
            $lspot_ranking = DB::connection('legacy_spot')->select('SELECT  * FROM legacy_spot.`Ranking` WHERE DATE_SUB(CURDATE(),INTERVAL 2 WEEK) <= `Rank Date` ORDER BY `Rank Date` asc LIMIT '.$limit.', '.$at_a_time);

            $this->comment('Query: '.$query.' Limit: '.$limit);

            foreach ($lspot_ranking as $ranking) {
                //Look up spot_id
                $spotmap = SpotMapping::where('legacy_spot_id', $ranking->ID)->first();

                if (!$spotmap) {
                    $this->error('Spot Map Not Found! '.$ranking->ID);
                } else {
                    //Check to see if it exists already:
                    $exists = SpotRanking::where('rank_date', $ranking->{'Rank Date'})
                                    ->where('rank', $ranking->{'Rank'})
                                    ->where('new_for_week', $ranking->{'New for Week'})
                                    ->where('spot_id', $spotmap->spot_id)->first();

                    if (! $exists) {
                        $newranking = new SpotRanking;
                        $newranking->spot_id = $spotmap->spot_id;
                        $newranking->rank_date = $ranking->{'Rank Date'};
                        $newranking->rank = $ranking->{'Rank'};
                        $newranking->new_for_week = $ranking->{'New for Week'};
                        $newranking->save();
                    }
                }
            }

            $query++;
            $limit = $limit + $at_a_time;
        }
    }
}
