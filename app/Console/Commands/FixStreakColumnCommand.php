<?php

namespace App\Console\Commands;

use App\Models\SpotRanking;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class FixStreakColumnCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fire:fixstreak';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fire caused skip in reports, this retallies the streak column as it should.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startDate = Carbon::createFromFormat('Y-m-d', '2015-07-03')->startOfDay();

        while ($startDate->lte(Carbon::now())) {
            $this->info('Updating Streak for '. $startDate);
            $rankings = SpotRanking::where('rank_date', $startDate->format('Y-m-d'))->get();
            foreach ($rankings as $ranking) {
                $previous = $this->previousWeek($startDate->copy(), $ranking->spot_id);
                if ($previous) {
                    $this->info('Updating Spot ID: '.$ranking->spot_id);
                    $this->info('Previous: '.$previous->rank);
                    $this->info('Streak: '.($previous->streak + 1));
                    $ranking->prev_rank = $previous->rank;
                    $ranking->streak = ($previous->streak + 1);
                    $ranking->save();
                } else {
                    $this->info('new spot');
                }
                $this->error('-----------------------------');
            }
            $startDate->addDays(7);
        }
    }

    public function previousWeek($rankDate, $spotId)
    {
        if ($rankDate->format('Y-m-d') == '2015-07-03') {
            $checkDate = '2015-06-12';
        } else {
            $checkDate = $rankDate->subDays(7)->format('Y-m-d');
        }

        $this->info('Checking for streak in: '.$checkDate);

        return SpotRanking::where('rank_date', $checkDate)
            ->where('spot_id', $spotId)
            ->first();
    }
}
