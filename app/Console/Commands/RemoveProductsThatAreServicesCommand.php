<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RemoveProductsThatAreServicesCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'products:remove-services';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove Products that are services.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::with('spots', 'spots.spotVersions')->orderBy('name')->get();

        $deleteArr = [];
        foreach ($products as $product) {
            $this->info($product->name);
            if ($product->spots->count() > 0) {
                $this->info(' Product has '.$product->spots->count(). ' spots!');
                foreach ($product->spots as $spot) {
                    $this->info('  Inside spot loop');
                    foreach ($spot->spotVersions as $version) {
                        if ($version->service == 1) {
                            $this->error('Spot Version is a service!');
                            if (!in_array($product->id, $deleteArr)) {
                                $deleteArr[] = $product->id;
                            }
                        }
                    }
                }
            }
        }

        foreach ($deleteArr as $delete) {
            $product = Product::find($delete);
            $product->programs()->detach();
            $product->spots()->detach();
            $product->retailerProducts()->delete();

            $product->delete();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
