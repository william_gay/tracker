<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateLegacyCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:updatelegacy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command that kicks off commands to update legacy databases (IM2,Spot).';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Let's download dbs!
        $this->info('Download the DBs first!');
        $this->call('ims:dldbs');

        //Let's call Spot First
        //$this->info('Importing Spots Database');
        //$this->call('ims:updatedbspot');

        //Let's call IM2
        $this->info('Importing IM2 Database');
        $this->call('ims:updatedbim2');

        //Let's make a datetime field in the airtime table for Explore Analytics
        $this->info('Airtime Make Datetime');
        $this->call('ims:airtimedatetime');

        //Let's make a directory on S3 for next week
        //$this->info('Make directory on AWS for next week.');
        //$this->call('ims:createawsdirectory');

        //Let's kick off the sync script:
        $this->info('Syncing database');
        $this->call('ims:synclegacy');
    }
}
