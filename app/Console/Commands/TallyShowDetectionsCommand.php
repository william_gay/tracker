<?php

namespace App\Console\Commands;

use App\Models\Airtime;
use App\Models\Language;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\ProgramWeekly;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class TallyShowDetectionsCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:tallyshowdetections';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tally Show Detections as the end of the week to create rankings.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $weekEnding = Carbon::createFromFormat('Y-m-d H:i:s', $this->argument('weekEnding'));
        $weekBeginning = Carbon::createFromFormat('Y-m-d H:i:s', $this->argument('weekBeginning'));

        if ($this->option('language')) {
            $language = Language::find($this->option('language'));
            $languages = [$language];
        } else {
            $languages = Language::orderBy('name')->get();
        }

        foreach ($languages as $language) {
            if ($language->id == 1) {
                $this->info('Ranking Detections for '.$language->name);
                $this->tallyShows($language->id, $weekBeginning, $weekEnding);
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['weekEnding', InputArgument::REQUIRED, 'Enter Week Ending Date. Format: YYYY-MM-DD'],
            ['weekBeginning', InputArgument::REQUIRED, 'Enter Week Beginning Date. Format:: YYYY-MM-DD'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['language', null, InputOption::VALUE_REQUIRED, 'Specific the language to rank.', null],
        ];
    }

    private function tallyShows($languageId, $weekBeginning, $weekEnding)
    {
        $showsAired = Airtime::select('program_version_id')
            ->leftJoin('program_versions', 'program_versions.id', '=', 'airtimes.program_version_id')
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->verified()
            ->whereBetween('air_date', [$weekBeginning->format('Y-m-d H:i:s'), $weekEnding->format('Y-m-d H:i:s')])
            ->where('channels.language_id', $languageId)
            ->where('channels.pi', '=', '0') // Make sure paid programming channels aren't in here
            ->groupBy('program_version_id')
            ->get();

        foreach ($showsAired as $show) {
            $firstShow = $this->firstShow($show->program_version_id);
            $previousWeek = $this->previousWeek($firstShow, $weekEnding);

            $exists = ProgramWeekly::where('program_id', $firstShow)
                ->where('week_ending', $weekEnding->format('Y-m-d'))
                ->first();

            if ($exists) {
                continue;
            }

            $stats = new ProgramWeekly;
            $stats->week_ending = $weekEnding->format('Y-m-d');
            $stats->program_id = $firstShow;
            $stats->airs = $this->getShowAirs($firstShow, $weekBeginning, $weekEnding);
            $stats->media_index = $this->getShowIndex($firstShow, $weekBeginning, $weekEnding);
            $stats->stations = $this->getStations($firstShow, $weekBeginning, $weekEnding);

            if ($previousWeek) {
                $stats->prev_freq_rank = $previousWeek->freq_rank;
                $stats->prev_media_rank = $previousWeek->media_rank;
                $stats->streak = $previousWeek->streak + 1;
            } else {
                $stats->streak = 1;
            }

            $stats->save();
        }

        $this->rankFreq($weekEnding);
        $this->rankMedia($weekEnding);
    }

    private function rankFreq($weekEnding)
    {
        $shows = ProgramWeekly::where('week_ending', $weekEnding->format('Y-m-d'))
            ->orderBy('airs', 'desc')
            ->get();

        //Initialize rank
        $rank = 1;
        foreach ($shows as $show) {
            $show->freq_rank = $rank;
            $show->save();
            $rank++;
        }
    }

    private function rankMedia($weekEnding)
    {
        $shows = ProgramWeekly::where('week_ending', $weekEnding->format('Y-m-d'))
            ->orderBy('media_index', 'desc')
            ->get();

        //Initialize rank
        $rank = 1;
        foreach ($shows as $show) {
            $show->media_rank = $rank;
            $show->save();
            $rank++;
        }
    }

    private function getShowAirs($showId, $weekBeginning, $weekEnding)
    {
        $shows = $this->getShowIds($showId);

        $airs = Airtime::whereBetween('air_date', [$weekBeginning->format('Y-m-d H:i:s'), $weekEnding->format('Y-m-d H:i:s')])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->verified()
            ->where('channels.pi', '=', '0') // Make sure paid programming channels aren't in here
            ->whereIn('program_version_id', explode(',', $shows->ids))
            ->get();

        return $airs->count();
    }

    private function getShowIndex($showId, $weekBeginning, $weekEnding)
    {
        $shows = $this->getShowIds($showId);

        $airs = Airtime::select(DB::raw('sum(airtimes.cost)/2000 as media_index'))
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->verified()
            ->whereBetween('air_date', [$weekBeginning->format('Y-m-d H:i:s'), $weekEnding->format('Y-m-d H:i:s')])
            ->where('channels.pi', '=', '0') // Make sure paid programming channels aren't in here
            ->whereIn('program_version_id', explode(',', $shows->ids))
            ->first();

        return $airs->media_index;
    }

    private function getStations($showId, $weekBeginning, $weekEnding)
    {
        $shows = $this->getShowIds($showId);

        $airs = Airtime::whereBetween('air_date', [$weekBeginning->format('Y-m-d H:i:s'), $weekEnding->format('Y-m-d H:i:s')])
            ->leftJoin('channels', 'channels.id', '=', 'airtimes.channel_id')
            ->verified()
            ->whereIn('program_version_id', explode(',', $shows->ids))
            ->where('channels.pi', '=', '0') // Make sure paid programming channels aren't in here
            ->groupBy('channel_id')
            ->get();

        return $airs->count();
    }

    private function previousWeek($showId, $week)
    {
        $prevWeek = Carbon::createFromFormat('Y-m-d', $week->format('Y-m-d'));
        $prevWeek->subDays(7);

        return ProgramWeekly::where('week_ending', $prevWeek->format('Y-m-d'))
            ->where('program_id', $showId)
            ->first();
    }

    private function getShowIds($showId)
    {
        $shows = ProgramVersion::select(
            DB::raw('group_concat(id) as ids')
        )
            ->where('program_id', $showId)
            ->first();

        return $shows;
    }

    private function firstShow($showId)
    {
        $show = ProgramVersion::find($showId);

        return $show->program_id;
    }
}
