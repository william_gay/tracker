<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateDBSpotCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:updatedbspot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to update the Spot DB.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $first_date = strtotime("2013-09-13");
        $accessFile = storage_path().'/ms-access/Spot.mdb';

        //Get that tables of Spot DB
        $tables_data = [];
        exec('mdb-tables -d, "'.$accessFile.'"', $tables_data);
        $tables = explode(',', $tables_data[0]);
        print_r($tables);

        //Remove `Paste Errors` table from import array
        if (array_key_exists(5, $tables) and $tables[5] == 'Paste Errors') {
            unset($tables[5]);
        }
        //Remove `Sheet1$_ImportErrors` table from import array
        if (array_key_exists(10, $tables) and $tables[10] == 'Sheet1$_ImportErrors') {
            unset($tables[10]);
        }
        //Remove Compact Errors
        if (array_key_exists(12, $tables) and $tables[12] == 'MSysCompactError') {
            unset($tables[12]);
        }
        //Remove last item in array because that shit is empty
        if (end($tables) == '') {
            array_pop($tables);
        }

        print_r($tables);

        $rowSep = "GROWlSEP";
        $colSep = ",";

        foreach ($tables as $table) {
            if (empty($table)) {
                continue;
            }
            $this->info('Processing '.$table.'...');
            $data = [];

            exec('mdb-export -d"'.$colSep.'" -R"'.$rowSep.'" -D "%F %H:%M:%S" '.$accessFile.' "'.$table.'"', $data);

            if (array_key_exists(0, $data)) {
                //First row is column names
                $col_titles = str_getcsv($data[0], ',', '"');
                //Remove the column names from the import
                unset($data[0]);
            } else {
                $this->error('SUM TING WONG with table: '.$table.' in file: '.$accessFile);
                print_r($data);
            }

            $alldata = join("\n", $data); //Join
            $alldata = str_replace("\\", "\\\\", $alldata);

            $data = explode($rowSep, $alldata);
            //Remove last item in array since it's empty
            array_pop($data);

            $i=0;

            DB::connection('legacy_spot')->disableQueryLog();

            while ($i<count($data)) {
                //Print CSV Line
                //print_r($data[$i]);
                $columns = str_getcsv($data[$i], ',', '"');
                $col_count = count($columns);

                //Print Columns after loaded into Array
                //print_r($columns);

                if ($i % 1000 == 0) {
                    $this->info("Rows Parsed: ".$i);
                }

                if ($table == 'Detections') {
                    if (strtotime($columns[1]) < $first_date) {
                        //Only try and insert new data
                        //$this->info($columns[1].' is old..');
                        $i++;
                        continue;
                    } else {
                        $this->info($columns[1].' is new..');
                    }
                } elseif ($table == 'Spot Log') {
                    if (strtotime($columns[14]) < $first_date) {
                        //Only try and insert new data
                        //$this->info($columns[1].' is old..');
                        $i++;
                        continue;
                    } else {
                        $this->info($columns[14].' is new..');
                    }
                } elseif ($table == 'Ranking') {
                    if (strtotime($columns[1]) < $first_date) {
                        //Only try and insert new data
                        //$this->info($columns[1].' is old..');
                        $i++;
                        continue;
                    } else {
                        $this->info($columns[1].' is new..');
                    }
                }

                $comma_separated_titles = implode("`,`", $col_titles);
                $comma_separated_titles = "`".$comma_separated_titles."`";

                DB::connection('legacy_spot')->insert('insert ignore into `'.$table.'` ('.$comma_separated_titles.') values ('.join(',', array_fill(0, $col_count, '?')).')', $columns);
                $i++;
            }
        }
    }
}
