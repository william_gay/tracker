<?php

namespace App\Console\Commands;

use App\Models\Language;
use App\Models\Spot;
use App\Models\SpotAirtime;
use App\Models\SpotDetection;
use App\Models\SpotRanking;
use App\Models\SpotVersion;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class TallySpotDetectionsCommand extends Command
{

    protected $weekBeginning;
    protected $weekEnding;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:tallyspotdetections';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Talley the spot detections at the end of the week.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->weekEnding = Carbon::createFromFormat('Y-m-d H:i:s', $this->argument('weekEnding'));
        $this->weekBeginning = Carbon::createFromFormat('Y-m-d H:i:s', $this->argument('weekBeginning'));

        if ($this->option('language')) {
            $language = Language::find($this->option('language'));
            $languages = [$language];
        } else {
            $languages = Language::orderBy('name')->get();
        }

        foreach ($languages as $language) {
            $this->info('Tallying Detections for '.$language->name);
            $this->tallyDetections($language->id, $this->weekBeginning, $this->weekEnding);
            if ($language->id == 1) {
                $this->info('Ranking Detections for '.$language->name);
                $this->rankSpots($language->id, $this->weekEnding);
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['weekEnding', InputArgument::REQUIRED, 'Enter Week Ending Date. Format: YYYY-MM-DD'],
            ['weekBeginning', InputArgument::REQUIRED, 'Enter Week Beginning Date. Format:: YYYY-MM-DD'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['language', null, InputOption::VALUE_REQUIRED, 'Specific the language to rank.', null],
        ];
    }

    private function tallyDetections($languageId, $weekBeginning, $weekEnding)
    {
        $this->info('In the tallyDetections function. Language ID: '.$languageId. ' Week Beginning: '.$weekBeginning.' Week Ending: '.$weekEnding);
        $detections = SpotAirtime::select('spot_airtimes.id', 'spot_versions.spot_id', 'spot_version_id')
            ->whereBetween('spot_airtimes.air_date', [$weekBeginning->format('Y-m-d H:i:s'), $weekEnding->format('Y-m-d H:i:s')])
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_airtimes.spot_version_id')
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->where('channels.language_id', $languageId)
            ->where('channels.pi', '=', '0') // Make sure paid programming channels aren't in here
            ->groupBy('spot_airtimes.spot_version_id')
            ->get();

        $this->info('Count: '.$detections->count());

        foreach ($detections as $detection) {
            $this->info("Adding Detection: ".$detection->spot_id. " Airtime ID: ".$detection->id);

            if (!$detection->spotVersion->spot) {
                $this->error("This ID sucks... ".$detection->spot_id);
            }

            $exists = SpotDetection::where('spot_version_id', $detection->spot_version_id)
                ->where('language_id', $languageId)
                ->where('rank_date', $weekEnding->format('Y-m-d'))
                ->first();

            if ($exists) {
                continue;
            }

            $insert = new SpotDetection;
            $insert->spot_version_id = $detection->spot_version_id;
            $insert->rank_date = $weekEnding->format('Y-m-d');
            $insert->new_for_week = $this->isNewForWeek($detection->spot_version_id, $weekEnding);
            $insert->detections = $this->getDetections($detection->spot_version_id, $weekBeginning, $weekEnding);
            $insert->media_index = $this->getMediaIndex($detection->spot_version_id);
            $insert->language_id = $languageId;
            $insert->save();
        }
    }

    private function rankSpots($languageId, $weekEnding)
    {
        $spotDetections = SpotDetection::where('spot_detections.language_id', $languageId)
            ->leftJoin('spot_versions', 'spot_versions.id', '=', 'spot_detections.spot_version_id')
            ->where('rank_date', $weekEnding->format('Y-m-d'))
            ->where('spot_versions.service', '=', 0)
            ->where('spot_versions.non_dr', '=', 0)
            ->where('spot_versions.brand_advert', '=', 0)
            ->orderBy('detections', 'desc')
            ->get();

        foreach ($spotDetections as $detection) {
            $previousWeek = $this->previousWeek($detection->spot_id, $weekEnding, $languageId);

            $ids = $this->spotIds($detection->spot_id, $languageId);
            $high = SpotDetection::where('rank_date', $weekEnding->format('Y-m-d'))
                ->whereIn('spot_version_id', $ids)
                ->orderBy('detections', 'desc')
                ->first();

            $frequency = SpotDetection::where('rank_date', $weekEnding->format('Y-m-d'))
                ->whereIn('spot_version_id', $ids);

            $exists = SpotRanking::where('spot_id', $detection->spot_id)
                ->where('rank_date', $weekEnding->format('Y-m-d'))
                ->first();

            if ($exists) {
                continue;
            }

            $ranking = new SpotRanking;
            $ranking->rank_date = $weekEnding->format('Y-m-d');
            $ranking->spot_id = $this->getSpotId($detection->spot_version_id);
            $ranking->spot_version_id = $high->spot_version_id;
            $ranking->frequency = $frequency->sum('detections');
            $ranking->media_index = $this->getMediaIndex($high->spot_version_id, true);

            if ($previousWeek) {
                $ranking->prev_rank = $previousWeek->rank;
                $ranking->streak = $previousWeek->streak + 1;
            } else {
                $ranking->streak = 1;
            }

            $ranking->save();
        }

        $this->rankEm($weekEnding);
    }

    private function rankEm($weekEnding)
    {
        $spots = SpotRanking::where('rank_date', $weekEnding->format('Y-m-d'))
            ->orderBy('frequency', 'desc')
            ->get();

        //Initialize rank
        $rank = 1;
        foreach ($spots as $spot) {
            $spot->rank = $rank;
            $spot->save();
            $rank++;
        }
    }

    private function previousWeek($spotId, $week, $languageId)
    {
        $prevWeek = Carbon::createFromFormat('Y-m-d', $week->format('Y-m-d'));
        $prevWeek->subDays(7);

        return SpotRanking::where('rank_date', $prevWeek->format('Y-m-d'))
            ->where('spot_id', $spotId)
            ->first();
    }

    private function getDetections($spotId, $weekBeginning, $weekEnding)
    {
        $spotDetections = SpotAirtime::where('spot_version_id', $spotId)
            ->whereBetween('air_date', [$weekBeginning->format('Y-m-d H:i:s'), $weekEnding->format('Y-m-d H:i:s')])
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->where('channels.pi', '=', '0') // Make sure paid programming channels aren't in here
            ->get();

        return $spotDetections->count();
    }

    private function getMediaIndex($spot_version_id, $all = false)
    {
        if ($all) {
            $ids = $this->getSpotIds($spot_version_id);
        } else {
            $ids = [$spot_version_id];
        }

        $airs = SpotAirtime::select(DB::raw('sum(spot_airtimes.cost)/2000 as media_index'))
            ->leftJoin('channels', 'channels.id', '=', 'spot_airtimes.channel_id')
            ->whereBetween('air_date', [$this->weekBeginning->format('Y-m-d H:i:s'), $this->weekEnding->format('Y-m-d H:i:s')])
            ->where('channels.pi', '=', '0') // Make sure paid programming channels aren't in here
            ->whereIn('spot_version_id', $ids)
            ->first();

        return $airs->media_index;
    }

    private function getSpotIds($spot_version_id)
    {
        $spotVersion = SpotVersion::find($spot_version_id);
        $spot_ids = SpotVersion::where('spot_id', $spotVersion->spot_id)->pluck('id')->toArray();

        return $spot_ids;
    }

    private function isNewForWeek($spotId, $weekEnding)
    {
        $spot = SpotVersion::find($spotId);

        if (! $spot) {
            dd($spotId);
        }

        if ($spot->air_date == $weekEnding->format('Y-m-d')) {
            return 1;
        } else {
            return 0;
        }
    }

    private function getSpotId($spotId)
    {
        $spot = SpotVersion::find($spotId);

        return $spot->spot_id;
    }

    private function spotIds($spotId, $languageId)
    {
        $ids = SpotVersion::select(DB::raw('group_concat(id) as ids'))
            ->where('spot_id', $spotId)
            ->where('language_id', $languageId)
            ->where('service', '=', 0)
            ->where('non_dr', '=', 0)
            ->first();

        return explode(',', $ids->ids);
    }
}
