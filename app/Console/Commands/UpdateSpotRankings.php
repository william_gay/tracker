<?php

namespace App\Console\Commands;

use App\Models\SpotDetection;
use App\Models\SpotRanking;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class UpdateSpotRankings extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:updatespotrankings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates spot rankings from all the way back when.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //$rankWeek = Carbon::createFromDate(1994, 12, 2, 'EST');
        //$now = new Carbon;
        $rankWeek = Carbon::createFromDate(2006, 07, 21, 'EST');
        $now = Carbon::createFromDate(2013, 06, 28, 'EST');

        while ($rankWeek->lte($now)) {
            $this->info('Processing Week: '. $rankWeek->format('Y-m-d'));
            $this->processWeek($rankWeek);
            $this->incrementWeek($rankWeek);
        }
    }

    private function incrementWeek($week)
    {
        $week->addDays(7);

        return $week;
    }

    private function processWeek($week)
    {
        $spotRankings = SpotRanking::where('rank_date', $week->format('Y-m-d'))->get();

        if ($spotRankings->count() > 0) {
            foreach ($spotRankings as $spot) {
                $previousWeek = $this->previousWeek($spot->spot_id, $week);

                if ($previousWeek) {
                    $spot->prev_rank = $previousWeek->rank;
                    $spot->streak = $previousWeek->streak + 1;
                } else {
                    $spot->streak = 1;
                }

                $this->info('Spot ID: '.$spot->spot_id. ' Week: '.$week->format('Y-m-d'));

                $detections = $this->getDetections($spot->spot_id, $week);
                if ($detections) {
                    $spot->frequency = $detections->detections;
                }

                $spot->save();
            }
        }

        return;
    }

    private function previousWeek($spotId, $week)
    {
        $prevWeek = Carbon::createFromFormat('Y-m-d', $week->format('Y-m-d'));
        $prevWeek->subDays(7);

        return SpotRanking::where('spot_id', $spotId)
            ->where('rank_date', $prevWeek->format('Y-m-d'))
            ->first();
    }

    private function getDetections($spotId, $week)
    {
        return SpotDetection::select('detections')
            ->where('spot_id', $spotId)
            ->where('rank_date', $week->format('Y-m-d'))
            ->first();
    }
}
