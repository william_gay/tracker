<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;

class MakeS3DirectoryCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:createawsdirectory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create AWS Directory for weekly Access databases.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $friday = date('Y-m-d', strtotime("next friday"));
        $bucket = 'ims-access';

        $s3 = App::make('aws')->get('s3')->registerStreamWrapper();

        if (! file_exists('s3://'.$bucket.'/'.$friday.'/')) {
            $result = $s3->putObject([
                'Bucket' => $bucket,
                'Key'    => $friday.'/'
            ]);

            $this->info('s3://'.$bucket.'/'.$friday.'/'.' Directory Created!');
        } else {
            $this->info('s3://'.$bucket.'/'.$friday.'/'.' Directory already exists.');
        }
    }
}
