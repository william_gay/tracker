<?php

namespace App\Console\Commands;

use App\Models\Airtime;
use App\Models\ChannelMapping;
use App\Models\ProgramMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncAirtimeCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:syncairtime';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Airtime Table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Disable query logging on this big db
        DB::connection()->disableQueryLog();
        DB::connection('legacy_im2')->disableQueryLog();

        $query = 0;
        $at_a_time = 250000;
        $limit = 0;

        $total_rows = DB::connection('legacy_im2')->select('select count(Air_ID) as count FROM `Airtime` WHERE imported IS NULL or imported = 0 ORDER BY Air_ID asc ');
        $this->info('Total Rows: '.$total_rows[0]->count);
        $number_queries = ceil($total_rows[0]->count / $at_a_time);
        $this->info('This script will run '.$number_queries.' queries!');

        while ($query < $number_queries) {
            $lim2_airtime = DB::connection('legacy_im2')->select('select * FROM `Airtime` WHERE imported  IS NULL or imported = 0 ORDER BY Air_ID asc LIMIT '.$limit.', '.$at_a_time);

            $this->comment('Query: '.$query.' Limit: '.$limit);

            foreach ($lim2_airtime as $lair) {
                $channel_id = null;
                if ($lair->Chan_ID > 0) {
                    $chan_map = ChannelMapping::where('im2_channel_id', $lair->Chan_ID)->first();
                    if ($chan_map) {
                        $channel_id = $chan_map->channel_id;
                    }
                }

                $program_id = null;

                $pro_map = ProgramMapping::where('im2_program_id', $lair->Prog_ID)->first();

                if ($pro_map) {
                    $program_id = $pro_map->program_id;
                }

                //Check to see if it exists already:
                $exists = Airtime::where('channel_id', $channel_id)
                                ->where('program_id', $program_id)
                                ->where('air_date', $lair->fullDatetime)->first();

                if (! $exists) {
                    $airtime = new Airtime;
                    $airtime->channel_id = $channel_id;
                    $airtime->program_id = $program_id;
                    $airtime->air_date = $lair->fullDatetime;
                    $airtime->type = $lair->Type;
                    $airtime->cost = $lair->Cost;
                    $airtime->save();

                    $this->comment('Inserted! New ID: '.$airtime->id.' Old ID: '.$lair->Air_ID);

                    //Update old DB to mark as complete
                    DB::connection('legacy_im2')->update('UPDATE `Airtime` SET imported = 1 WHERE Air_ID = ?', [$lair->Air_ID]);
                } else {
                    //Update old DB to mark as complete
                    DB::connection('legacy_im2')->update('UPDATE `Airtime` SET imported = 1 WHERE Air_ID = ?', [$lair->Air_ID]);
                }

                unset($exists);
                unset($airtime);
                unset($chan_map);
                unset($pro_map);
                unset($cost_map);
            }

            $query++;
            $limit = $limit + $at_a_time;
        }
    }
}
