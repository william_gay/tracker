<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Artisan;
use App\Models\User;
use App\Notifications\RankingsCompleted;
use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\Messages\SlackMessage;

class RunRankings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rankings:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the rankins on a weekly basis.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (Carbon::now()->isWeekend()) {
            $lastFriday = Carbon::now()->previous(Carbon::FRIDAY)
                ->hour(16)
                ->minute(0)
                ->second(0);

            $firstFriday = $lastFriday->copy()->previous(Carbon::FRIDAY)
                ->hour(16)
                ->minute(0)
                ->second(0);

            // Spot rankings
            Artisan::call(
                'ims:tallyspotdetections',
                [
                    'weekEnding' => $lastFriday,
                    'weekBeginning' => $firstFriday,
                    '--language' => 1,
                ]
            );

            // Spot Spanish rankings
            Artisan::call(
                'ims:tallyspotdetections',
                [
                    'weekEnding' => $lastFriday,
                    'weekBeginning' => $firstFriday,
                    '--language' => 2,
                ]
            );

            // Program rankings
            Artisan::call(
                'ims:tallyshowdetections',
                [
                    'weekEnding' => $lastFriday,
                    'weekBeginning' => $firstFriday,
                ]
            );

            $users = User::whereIn('email', ['nick@imsreport.com', 'steve@imsreport.com'])->get();
            Notification::send($users, new RankingsCompleted($lastFriday->toDateString(), $firstFriday->toDateString()));
        }
    }
}
