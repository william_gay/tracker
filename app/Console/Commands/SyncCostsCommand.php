<?php

namespace App\Console\Commands;

use App\Models\ChannelMapping;
use App\Models\Cost;
use App\Models\CostMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncCostsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:synccosts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync the costs.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $lim2_costs = DB::connection('legacy_im2')->select('select * FROM `Costs` WHERE DATE_SUB(CURDATE(),INTERVAL 2 WEEK) <= updated_at');

        foreach ($lim2_costs as $lcost) {
            $old_date = new DateTime($lcost->dTime);
            $just_time = $old_date->format('H:i:s');
            $new_datetime = $lcost->PostedOn.' '.$just_time; //Consolidated datetime PostedOn and dTime from legacy_im2

            //Get Channel ID Mapping
            $channel = ChannelMapping::where('im2_channel_id', '=', $lcost->Chan_ID)->first();
            $channel_id = isset($channel) ? $channel->channel_id : null;

            //Check to see if it exists already:
            $exists = Cost::where('channel_id', $channel_id)
                            ->where('day_of_week', $lcost->DayOfWeek)
                            ->where('cost_date', $new_datetime)
                            ->where('amount', $lcost->Amount)->first();

            //Only insert if it doesn't exist
            if (! $exists) {
                $cost = new Cost;
                $cost->channel_id = $channel_id;
                $cost->day_of_week = $lcost->DayOfWeek;
                $cost->cost_date = $new_datetime;
                $cost->amount = $lcost->Amount;
                $cost->type = $lcost->Type;
                $cost->archive = $lcost->Archive;
                $cost->save();

                CostMapping::create([
                    'cost_id' => $cost->id,
                    'im2_cost_id' => $lcost->Cost_ID
                ]);
            }

            unset($channel);
            unset($channel_id);
            unset($exists);
            unset($old_date);
            unset($just_time);
            unset($new_datetime);
        }
    }
}
