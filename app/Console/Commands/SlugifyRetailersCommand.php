<?php

namespace App\Console\Commands;

use App\Models\Retail\Retailer;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class SlugifyRetailersCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'slugify:retailers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Give Retailers Slugs.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $retailers = Retailer::where('slug', '')->get();

        foreach ($retailers as $retailer) {
            $this->info('Updating Slug for: '.$retailer->name);
            $retailer->slug = slugify($retailer->name);
            $this->info('Slug: '.$retailer->slug);
            $retailer->save();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
