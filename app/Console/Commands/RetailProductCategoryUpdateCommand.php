<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\Program;
use App\Models\ProgramVersion;
use App\Models\Spot;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RetailProductCategoryUpdateCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'retail:productupdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Try and find show or spots that relate to product to update category.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $products = Product::where('category_id', 0)
            ->orWhere('sub_category_id', 0)
            ->get();

        foreach ($products as $product) {
            $programs = Program::where('title', $product->name);
            $spots = Spot::where('title', $product->name);
            $programVersions = ProgramVersion::where('title', $product->name)->orWhere('grid_title', $product->name);

            $this->info('Searching.... '.$product->name);
            $this->info($programs->count(). ' Programs...');
            $this->info($spots->count(). ' Spots...');

            if ($programs->count() > 0) {
                $this->info('Matching Program!');
                $this->error('Category ID: '. $programs->first()->category_id);
                $this->error('Sub Category ID: '. $programs->first()->sub_category_id);

                $product->category_id = $programs->first()->category_id;
                $product->sub_category_id = $programs->first()->sub_category_id;

                $product->programs()->sync($programs->pluck('id'));
            }

            if ($spots->count() > 0) {
                $this->info('Matching Spot!');

                $this->error('Category ID: '. $spots->first()->category_id);
                $this->error('Sub Category ID: '. $spots->first()->sub_category_id);

                $product->category_id = $spots->first()->category_id;
                $product->sub_category_id = $spots->first()->sub_category_id;

                $product->spots()->sync($spots->pluck('id'));
            }

            if ($programVersions->count() > 0) {
                $this->info('Matching Program Version!');
                $this->error('Category ID: '. $programVersions->first()->category_id);
                $this->error('Sub Category ID: '. $programVersions->first()->sub_category_id);

                $product->category_id = $programVersions->first()->category_id;
                $product->sub_category_id = $programVersions->first()->sub_category_id;

                $product->programs()->sync($programVersions->pluck('program_id'));
            }

            $product->save();
        }
    }
}
