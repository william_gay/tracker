<?php

namespace App\Console\Commands;

use App\Models\ProgramVersion;
use App\Models\SpotVersion;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FixWeekEndingDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ims:fix-weekending';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loops through shows and spots and sets week ending times';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('Do you wish to check Spots?')) {
            SpotVersion::orderBy('id', 'desc')->chunk('100', function ($spots) {
                foreach ($spots as $spot) {
                    dump($spot->initial_date, $spot->getOriginal('air_date'));
                    if (($spot->initial_date == '0000-00-00 00:00:00' && $spot->getOriginal('air_date') == '0000-00-00') || ($spot->getOriginal('air_date') == '0000-00-00' && is_null($spot->initial_date))) {
                        $spot->air_date = null;
                        $spot->initial_date = null;
                        $spot->save();

                        continue;
                    } elseif ($spot->initial_date == '0000-00-00 00:00:00' || is_null($spot->initial_date)) {
                        $spot->initial_date = $spot->air_date;
                        $spot->save();
                    }

                    if (is_null($spot->initial_date)) {
                        // Skip it!
                        continue;
                    }

                    $initial_date = Carbon::parse($spot->initial_date);
                    $cutOff = $initial_date->copy()->setTime(16, 0, 0);
                    if ($initial_date->dayOfWeek != Carbon::FRIDAY ||
                        $initial_date->gt($cutOff)) {
                        $friday = $initial_date->copy()->next(Carbon::FRIDAY);
                    } else {
                        $friday = $initial_date->copy();
                    }

                    // Set the time to 4PM
                    $friday->setTime(16, 0, 0);
                    $air_date = $spot->air_date->copy();
                    $air_date->setTime(16, 0, 0);

                    if ($friday->ne($air_date)) {
                        $this->error('Spot air_date after week ending!');
                        $this->error('Spot ID: '.$spot->id);
                        $this->error('Air Date: '. $spot->air_date.' Initial Date: '.$spot->initial_date);
                        $this->info('Proposed Air Date: '.$friday->toDateString());
                        $spot->air_date = $friday->toDateString();
                        $spot->save();
                        $this->error('------------');
                    }
                }
            });
        }

        if ($this->confirm('Do you wish to check Shows?')) {
            ProgramVersion::orderBy('id', 'desc')->chunk('100', function ($shows) {
                foreach ($shows as $show) {
                    if ($show->getOriginal('initial_date') == '0000-00-00 00:00:00' && $show->getOriginal('monitor_date') != '0000-00-00') {
                        $show->initial_date = $show->monitor_date;
                        $show->save();
                    } elseif ($show->getOriginal('initial_date') == '0000-00-00 00:00:00' && $show->getOriginal('monitor_date') == '0000-00-00') {
                        $show->initial_date = null;
                        $show->monitor_date = null;
                        $show->save();

                        continue;
                    }

                    if (is_null($show->initial_date)) {
                        // Skip it!
                        continue;
                    }

                    $cutOff = $show->initial_date->copy()->setTime(16, 0, 0);

                    if ($show->initial_date->dayOfWeek != Carbon::FRIDAY ||
                        $show->initial_date->gt($cutOff)) {
                        $friday = $show->initial_date->copy()->next(Carbon::FRIDAY);
                    } else {
                        $friday = $show->initial_date->copy();
                    }

                    // Set the time to 4PM
                    $friday->setTime(16, 30, 0);
                    $monitor_date = $show->monitor_date->copy();
                    $monitor_date->setTime(16, 30, 0);

                    if ($friday->ne($monitor_date)) {
                        $this->error('Show air_date after week ending!');
                        $this->error('Show ID: '.$show->id);
                        $this->error('Monitor Date: '. $show->monitor_date.' Initial Date: '.$show->initial_date);
                        $this->info('Proposed Air Date: '.$friday->toDateString());
                        $this->error('------------');
                    }
                }
            });
        }
    }
}
