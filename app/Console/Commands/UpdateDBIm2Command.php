<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateDBIm2Command extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:updatedbim2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update DB IM2.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $first_date = strtotime("2013-11-01");
        $accessFile = storage_path().'/ms-access/IM2.mdb';

        //Get that tables of Spot DB
        $tables_data = [];
        exec('mdb-tables -d, "'.$accessFile.'"', $tables_data);
        $tables = explode(',', $tables_data[0]);
        print_r($tables);

        //Remove last item in array because that shit is empty
        if (end($tables) == '') {
            array_pop($tables);
        }

        print_r($tables);

        $rowSep = "GROWlSEP";
        $colSep = ",";

        foreach ($tables as $table) {
            if (empty($table)) {
                continue;
            }
            $this->info('Processing '.$table.'...');
            $data = [];

            exec('mdb-export -d"'.$colSep.'" -R"'.$rowSep.'" -D "%F %H:%M:%S" '.$accessFile.' "'.$table.'"', $data, $status);

            $this->info('mdb-export status: '.$status);

            //print_r($data);

            if (array_key_exists(0, $data)) {
                //First row is column names
                $col_titles = str_getcsv($data[0], ',', '"');
                print_r($col_titles);
                //Remove the column names from the import
                unset($data[0]);
            } else {
                $this->error('SUM TING WONG with table: '.$table.' in file: '.$accessFile);
                print_r($data);
            }
            $alldata = join("\n", $data); //Join
            $alldata = str_replace("\\", "\\\\", $alldata);
            $data = explode($rowSep, $alldata);
            //Remove last item in array since it's empty
            array_pop($data);
            $i=0;

            DB::connection('legacy_im2')->disableQueryLog();

            while ($i<count($data)) {
                //Print CSV Line
                //print_r($data[$i]);
                $columns = str_getcsv($data[$i], ',', '"');
                $col_count = count($columns);

                //Print Columns after loaded into Array
                //print_r($columns);

                if ($i % 1000 == 0) {
                    $this->info("Rows Parsed: ".$i);
                }

                if ($table == 'Airtime') {
                    if (strtotime($columns[3]) < $first_date) {
                        //Only try and insert new data
                        //$this->info($columns[1].' is old..');
                        $i++;
                        continue;
                    } else {
                        $this->info($columns[3].' is new..');
                    }
                } elseif ($table == 'Program_History') {
                    if (strtotime($columns[1]) < $first_date) {
                        //Only try and insert new data
                        //$this->info($columns[1].' is old..');
                        $i++;
                        continue;
                    } else {
                        $this->info($columns[1].' is new..');
                    }
                }

                $comma_separated_titles = implode("`,`", $col_titles);
                $comma_separated_titles = "`".$comma_separated_titles."`";

                DB::connection('legacy_im2')->insert('insert ignore into `'.$table.'` ('.$comma_separated_titles.') values ('.join(',', array_fill(0, $col_count, '?')).')', $columns);
                $i++;
            }
        }
    }
}
