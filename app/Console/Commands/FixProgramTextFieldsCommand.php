<?php

namespace App\Console\Commands;

use App\Models\Program;
use App\Models\ProgramMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixProgramTextFieldsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:fixprogramtext';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to update text in programs table because they were pulled from tinytext.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Disable query logging on this big db
        DB::connection()->disableQueryLog();
        DB::connection('legacy_im2')->disableQueryLog();

        $at_a_time = 10000;

        $number_queries = ceil(Program::orderBy('id')->count() / $at_a_time);
        $this->info('This script will run '.$number_queries.' queries!');
        $query = 0;
        $limit = 0;

        while ($query < $number_queries) {
            //Select the first batch
            $programs = Program::orderBy('id')->skip($limit)->take($at_a_time)->get();

            $this->comment('Query: '.$query.' Limit: '.$limit);

            foreach ($programs as $program) {
                //Get Program Mapping:
                $programmap = ProgramMapping::where('program_id', $program->id)->first();
                if (!$programmap) {
                    $this->error('Program Map Not Found! ID: '.$program->id.' Title:'.$program->title);
                } else {
                    $legacy_program = DB::connection('legacy_im2')->table('Program')
                                        ->leftJoin('Program_Detail', 'Program.Prog_ID', '=', 'Program_Detail.Prog_ID')
                                        ->where('Program.Prog_ID', $programmap->im2_program_id)
                                        ->first();

                    $this->info('Title: '. $legacy_program->Title);
                    $this->info('Grid Title: '.$legacy_program->GridTitle);
                    $this->info('Description: '.$legacy_program->Product_Desc);
                    $this->info('Show Summary: '.$legacy_program->Show_Summary);
                    $this->info('Order Upsell: '.$legacy_program->Ord_Upsell);

                    $update = Program::findOrFail($program->id);
                    $update->title = $legacy_program->Title;
                    $update->grid_title = $legacy_program->GridTitle;
                    $update->description = $legacy_program->Product_Desc;
                    $update->summary = $legacy_program->Show_Summary;
                    $update->remarks = $legacy_program->Remarks;
                    $update->upsell = $legacy_program->Ord_Upsell;
                    $update->save();
                }
            }
            $query++;
            $limit = $limit + $at_a_time;
        }
    }
}
