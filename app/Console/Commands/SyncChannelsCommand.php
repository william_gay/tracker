<?php

namespace App\Console\Commands;

use App\Models\Channel;
use App\Models\ChannelMapping;
use App\Models\CompanyMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncChannelsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:syncchannels';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync the Channels.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $lim2_channels = DB::connection('legacy_im2')->select('select * FROM `Channels` WHERE DATE_SUB(CURDATE(),INTERVAL 2 WEEK) <= updated_at');

        foreach ($lim2_channels as $lchannel) {
            $company_id = null;
            if ($lchannel->Chan_Co_ID != 0) {
                // Lookup company mapping
                $company = CompanyMapping::where('im2_company_id', '=', $lchannel->Chan_Co_ID)->first();
                $company_id = $company->id;
            }

            //Check to see if it exists already:
            $exists = Channel::where('abbr', $lchannel->Abbr)
                            ->where('name', $lchannel->FullName)
                            ->where('language_id', 1)->first();

            //Only insert if it doesn't exist
            if (! $exists) {
                $channel = new Channel;
                $channel->abbr = $lchannel->Abbr;
                $channel->name = $lchannel->FullName;
                $channel->info_active = (int) $lchannel->Active;
                $channel->company_id = $company_id;
                $channel->language_id = 1; //English
                $channel->save();

                ChannelMapping::create(
                    ['channel_id'=>$channel->id, 'im2_channel_id' => $lchannel->Chan_ID]
                );
            }

            unset($company);
            unset($exists);
        }
    }
}
