<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use App\Models\Category;
use App\Models\Channel;
use App\Models\Company;
use App\Models\Spot;
use App\Models\SpotMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SyncSpotLogCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:syncspotlog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Spot Logs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Disable query logging on this big db
        DB::connection()->disableQueryLog();
        DB::connection('legacy_spot')->disableQueryLog();

        $lspot_logs = DB::connection('legacy_spot')->select('SELECT  * FROM legacy_spot.`Spot Log` WHERE DATE_SUB(CURDATE(),INTERVAL 2 WEEK) <= `Date` ORDER BY ID asc');

        foreach ($lspot_logs as $spot) {
            //Lookup category and sub category
            $category = Category::where('name', $spot->Category)->whereNull('parent_id')->first();
            if (!$category) {
                //$this->error('Category Not Found: '.$spot->ID.' '.$spot->{'Product Name'}.' Category:'.$spot->Category);

                //Category is probably named differently create an array to get the ID.
                $catmap = [
                    'Comp. & Electronics' => 5,
                    'Educ. & Self Help' => 7,
                    'Educ. & self Help' => 7
                ];

                $category_id = $catmap[$spot->Category];

                $this->info('Category Found! '.$spot->ID.' '.$spot->{'Product Name'}.' Category:'.$spot->Category);
            } else {
                $category_id = $category->id;
                $this->info('Category Found! '. $spot->ID.' '.$spot->{'Product Name'}.' Category:'.$spot->Category);
            }

            //Figure out if it is Other, or Accessories
            if (substr($spot->{'Sub-Category'}, 0, 5) == 'Other') {
                $sub_category = 'Other';
            } else {
                $pos = strpos($spot->{'Sub-Category'}, ' - ');
                if ($pos !== false) {
                    $getcat = explode(' - ', $spot->{'Sub-Category'});
                    $sub_category = $getcat[0];
                    $this->info('Sub Cat Found by exploding: '.$spot->ID.' '.$spot->{'Product Name'}.' Category:'.$spot->{'Sub-Category'});
                } else {
                    $sub_category = $spot->{'Sub-Category'};
                }
            }

            $sub_category = Category::where('name', $sub_category)->where('parent_id', $category_id)->first();
            if (!$sub_category) {
                //$this->error('Sub Category Not Found: '.$spot->ID.' '.$spot->{'Product Name'}.' Category:'.$spot->{'Sub-Category'});

                //Category is probably named differently create an array to get the ID.
                $sub_category = Category::where('name', Str::plural($spot->{'Sub-Category'}))->first();
                if (!$sub_category) {
                    $this->error('Plural Sub Category Not Found: '.$spot->ID.' '.$spot->{'Product Name'}.' Category:'.$spot->{'Sub-Category'});

                    //Search for 'Accessories - Auto', 'Other - Kitchen'
                    $subcatmap = [
                        "Children's Ed." => 53,
                        "Adult Ed." => 52,
                        "Leisure" => 115,
                        "Instructional" => 92,
                        "Travel" => 116,
                        "Storage" => 87,
                        "Masculine" => 106,
                        "Computer" => 41,
                        "Insurance" => 38,
                        "Diet & Weightloss" => 73,
                        "Cookware" => 90
                    ];

                    $sub_category_id = $subcatmap[$spot->{'Sub-Category'}];
                    $this->info('Sub Cat Found by map: '.$spot->ID.' '.$spot->{'Product Name'}.' Category:'.$spot->{'Sub-Category'});

                    if ($sub_category_id == 115 or $sub_category_id == 116) {
                        //New Parent Category Travel & Leisure
                        $category_id = 19;
                    } elseif ($sub_category_id == 106) {
                        $category_id = 16;
                    } elseif ($sub_category_id == 41) {
                        $category_id = 5;
                    } elseif ($sub_category_id == 38) {
                        $category_id = 4;
                    } elseif ($sub_category_id == 73) {
                        $category_id = 11;
                    } elseif ($sub_category_id == 90) {
                        $category_id = 13;
                    }
                } else {
                    $this->info('Plural Sub Category Found! '.$spot->ID.' '.$spot->{'Product Name'}.' Category:'.Str::plural($spot->{'Sub-Category'}));
                    $sub_category_id = $sub_category->id;
                }
            } else {
                $sub_category_id = $sub_category->id;
            }

            $channel_id = null;
            if ($spot->Channel != '') {
                $spot_chan = trim($spot->Channel);
                $spot_chan = explode(',', $spot_chan);
                $spot_chan = $spot_chan[0];
                //Lookup Channel
                $channel = Channel::where('abbr', $spot_chan)->first();
                if (!$channel) {
                    //$this->error('Channel Not Found: '.$spot->ID.' '.$spot->{'Product Name'}.' Channel: '.$spot->Channel.' Fixed:'.$spot_chan);

                    $chanmap = [
                        'E' => 24, ' E' => 24,
                        'LIF' => 6,
                        'NICK' => 8,'NICk' => 8, 'NIC' => 8,
                        'TVLD' => 86,'TVVL' => 86,
                        'STYL' => 69,
                        'HIST' => 65,'HIST-I' => 65,
                        'AE' => 68,
                        'G4' => 74,'G$' => 74,'G4TV' => 74,'G Channel' => 74,'G channel' => 74,
                        'DSCH' => 10,'DSCT' => 10,'DISX' => 10, 'DST' => 10, 'DSCJ' => 10,
                        'FITTV' => 133,
                        'DSCK' => 134,'DSK' => 134,
                        'DHOM' => 73,'DH' => 73,
                        'VH-1' => 18,'VH1 CLA' => 18, 'VHI' => 18,
                        'FLN' => 79,
                        'CN' => 160,
                        'TOON' => 132,'TOON NICK' => 132,'CAR' => 132, 'TDV' => 132,
                        'TVFOOD' => 30,
                        'Broadcast' => 112,
                        'SCIFI' => 14,
                        'GAMEGOOD' => 127,
                        'ION' => 58,
                        'INSP GOOD' => 34,'INSP NICK' => 34, 'ISNP' => 34,
                        'CNBC GOOD' => 23,'CNBCW' => 23,'CNBC-W' => 23, 'CNCBW' => 23,
                        'DIS. KIDS' => 134,
                        ' FNC' => 128,
                        'OLN' => 123,
                        'TDIS' => 10,'DSC' => 10,
                        'FXNW' => 28,
                        'HDLN' => 120,
                        'STY' => 69, 'STLY' => 69,
                        'HISTI' => 65,'HISTi' => 65,'HISIT' => 65, 'HSIT' => 65,
                        'HI' => 65,
                        'MSN' => 114,'MSBNC' => 114,
                        'HN' => 120,
                        'SCI-CH' => 124, ' SC' => 124, 'SCICH' => 124,
                        'TVLAND' => 86, 'TLVD' => 86,
                        'VS`' => 131,
                        'Nick' => 8,
                        'SPIK' => 70,
                        'ESPNC' => 4, 'ESPNN' => 4,
                        'ES2' => 5, 'EESPN2' => 5,
                        'DHF' => 73, 'HEALTH' => 73,
                        'TGC' => 135, ' TGC' => 135, 'TCG' => 135,
                        'BRT' => 25,
                        'PINTBS' => 9,
                        'TV1' => 152, 'TVOEN' => 152,
                        'CNNH' => 118,
                        'TWC`' => 138,
                        'FUEK' => 85,
                        'SNBC' => 161,
                        'CBC' => 142,
                        'FDIY' => 63
                    ];

                    if (array_key_exists($channel, $chanmap)) {
                        $channel_id = $chanmap[$spot_chan];
                    } else {
                        $this->error('Channel Not found! '. $channel);
                        $channel_id = null;
                    }

                    $this->info('Channel Found! '.$spot->ID.' '.$spot->{'Product Name'}.' Channel: '.$spot->Channel.' Fixed:'.$spot_chan);
                } else {
                    $channel_id = $channel->id;
                }
            } else {
                $channel_id = null;
            }

            if ($spot->{'Marketing Company'} != 'N/A') {
                //Lookup Marketing Company
                $mco = Company::where('name', $spot->{'Marketing Company'})->first();
                if (!$mco) {
                    $this->error('Marketing Company Not Found: '.$spot->ID.' '.$spot->{'Product Name'});
                    //Create Company since it doesn't exist
                    $company = new Company;
                    $company->name = $spot->{'Marketing Company'};
                    $company->save();

                    $company_id = $company->id;
                } else {
                    $company_id = $mco->id;
                }
            } else {
                $company_id = null;
            }

            //Length
            $lengthmap = [
                '120' => 120,
                '5 min' => 300,
                '5 min.' => 300,
                ':10' => 10,
                ':120' => 120,
                ':1200' => 1200,
                ':15' => 15,
                ':150' => 150,
                ':180' => 180,
                ':20' => 20,
                ':220' => 220,
                ':240' => 240,
                ':30' => 30,
                ':300' => 300,
                ':40' => 40,
                ':45' => 45,
                ':50' => 50,
                ':60' => 60,
                ':75' => 75,
                ':80' => 80,
                ':90' => 90
            ];

            if (array_key_exists($spot->{'Length'}, $lengthmap)) {
                $length = $lengthmap[$spot->{'Length'}];
            } else {
                $length = null;
            }

            //Check to see if it exists already:
            $exists = Spot::where('title', $spot->{'Product Name'})
                            ->where('version', $spot->Version)
                            ->where('length', $length)
                            ->where('air_date', $spot->{'Date'})->first();

            if (! $exists) {
                // Spot
                $newspot = new Spot;
                $newspot->title = $spot->{'Product Name'};
                $newspot->version = $spot->Version;
                $newspot->description = $spot->{'Product Description'};
                $newspot->category_id = $category_id;
                $newspot->sub_category_id = $sub_category_id;
                $newspot->channel_id = $channel_id;
                $newspot->marketing_company_id = $company_id;
                $newspot->price = $spot->{'Price'};
                $newspot->price_extra = $spot->{'Price 2'};
                $newspot->shipping_cost = $spot->{'Shipping Cost'};
                $newspot->tape_number = $spot->{'Tape Number'};
                $newspot->time_in = $spot->{'Time In'};
                $newspot->length = $length;
                $newspot->record_speed = $spot->{'Record Speed'};
                $newspot->air_date = $spot->{'Date'};
                $newspot->new_version = $spot->{'New Version'};
                $newspot->notes = $spot->{'Notes'};
                $newspot->page_number = $spot->{'Page Number'};
                $newspot->oddball = $spot->{'Oddball'};
                $newspot->corporate = $spot->{'Corporate'};
                $newspot->website = $spot->{'Web Address'};
                $newspot->phone = $spot->{'Phone'};
                $newspot->cs_phone = $spot->{'Customer Service Phone'};
                $newspot->remarks = $spot->{'Remarks'};
                $newspot->tags = $spot->{'Sub-Category 2'};
                $newspot->save();

                SpotMapping::create([
                    'spot_id' => $newspot->id,
                    'legacy_spot_id' => $spot->ID
                ]);
            }

            unset($exists);
            unset($category);
            unset($category_id);
            unset($sub_category);
            unset($channel);
            unset($channel_id);
            unset($mco);
            unset($company_id);
            unset($newspot);
        }
    }
}
