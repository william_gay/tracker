<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Product;
use App\Jobs\WinmoAggregateMediaSpend;

class WinmoMediaSpend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'winmo:media-spend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate the media spend';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $winmoProducts = Product::orderBy('id')->get();

        $winmoProducts->each(function ($product) {
            WinmoAggregateMediaSpend::dispatch($product)->onQueue('winmo');
        });
    }
}
