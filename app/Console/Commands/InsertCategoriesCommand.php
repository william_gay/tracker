<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\CategoryMapping;
use Illuminate\Console\Command;

class InsertCategoriesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:insertcats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update categories from Spot and IM2.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // Insert the Parent Categories
        $automotive = Category::create(
            ['name' => 'Automotive']
        );

        $beauty = Category::create(
            ['name' => 'Beauty']
        );

        $brand = Category::create(
            ['name' => 'Brand / Image Aware, Lead Gen.']
        );

        $business = Category::create(
            ['name' => 'Business & Finance']
        );

        $computers = Category::create(
            ['name' => 'Computers & Electronics']
        );

        $crafts = Category::create(
            ['name' => 'Crafts & Hobbies']
        );

        $education = Category::create(
            ['name' => 'Education & Self Help']
        );

        $entertainment = Category::create(
            ['name' => 'Entertainment']
        );

        $fashion = Category::create(
            ['name' => 'Fashion']
        );

        $food = Category::create(
            ['name' => 'Food & Beverage']
        );

        $health = Category::create(
            ['name' => 'Health & Fitness']
        );

        $household = Category::create(
            ['name' => 'Household']
        );

        $kitchen = Category::create(
            ['name' => 'Kitchen']
        );

        $misc = Category::create(
            ['name' => 'Miscellaneous']
        );

        $novelties = Category::create(
            ['name' => 'Novelties']
        );

        $personal = Category::create(
            ['name' => 'Personal']
        );

        $pets = Category::create(
            ['name' => 'Pet Products']
        );

        $sports = Category::create(
            ['name' => 'Sports']
        );

        $travel = Category::create(
            ['name' => 'Travel & Leisure']
        );

        // Insert the Automotive Sub Cats
        if ($automotive->count() > 0) {
            $cleaner = Category::create(
                ['parent_id' => $automotive->id,'name' => 'Cleaners, Polish']
            );

            CategoryMapping::create(
                ['parent_id' => $automotive->id,'category_id'=>$cleaner->id, 'im2_category_id' => 1, 'im2_category_name' => 'Cleaners, Polish', 'spot_category_name' => 'Cleaners, Polish']
            );

            $engine = Category::create(
                ['parent_id' => $automotive->id,'name' => 'Engine/Oil Treatments']
            );

            CategoryMapping::create(
                ['parent_id' => $automotive->id,'category_id'=>$engine->id, 'im2_category_id' => 2, 'im2_category_name' => 'Engine/Oil Treatments', 'spot_category_name' => 'Engine/Oil Treatments']
            );

            $accessories = Category::create(
                ['parent_id' => $automotive->id,'name' => 'Accessories']
            );

            CategoryMapping::create(
                ['parent_id' => $automotive->id,'category_id'=>$accessories->id, 'spot_category_name' => 'Accessories - Auto']
            );

            $safety = Category::create(
                ['parent_id' => $automotive->id,'name' => 'Safety & Security']
            );

            CategoryMapping::create(
                ['parent_id' => $automotive->id,'category_id'=>$safety->id, 'spot_category_name' => 'Safety & Security - Auto']
            );

            $brand = Category::create(
                ['parent_id' => $automotive->id,'name' => 'Brand Awareness']
            );

            CategoryMapping::create(
                ['parent_id' => $automotive->id,'category_id'=>$brand->id, 'im2_category_id' => 66]
            );

            $other = Category::create(
                ['parent_id' => $automotive->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $automotive->id,'category_id'=>$other->id, 'im2_category_id' => 3, 'im2_category_name' => 'Other - Automotive', 'spot_category_name' => 'Other - Auto']
            );
        }

        if ($beauty->count() > 0) {
            $cosmetics = Category::create(
                ['parent_id' => $beauty->id,'name' => 'Cosmetics']
            );

            CategoryMapping::create(
                ['parent_id' => $beauty->id,'category_id'=>$cosmetics->id, 'im2_category_id' => 4, 'im2_category_name' => 'Cosmetics', 'spot_category_name' => 'Cosmetics']
            );

            $dental = Category::create(
                ['parent_id' => $beauty->id,'name' => 'Dental']
            );

            CategoryMapping::create(
                ['parent_id' => $beauty->id,'category_id'=>$dental->id, 'im2_category_id' => 5, 'im2_category_name' => 'Dental', 'spot_category_name' => 'Dental']
            );

            $hair = Category::create(
                ['parent_id' => $beauty->id,'name' => 'Hair Care']
            );

            CategoryMapping::create(
                ['parent_id' => $beauty->id,'category_id'=>$hair->id, 'im2_category_id' => 6, 'im2_category_name' => 'Hair Care']
            );

            $skin = Category::create(
                ['parent_id' => $beauty->id,'name' => 'Skin Care']
            );

            CategoryMapping::create(
                ['parent_id' => $beauty->id,'category_id'=>$skin->id, 'im2_category_id' => 8, 'im2_category_name' => 'Skin Care', 'spot_category_name' => 'Skin Care']
            );

            $other = Category::create(
                ['parent_id' => $beauty->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $beauty->id,'category_id'=>$other->id, 'im2_category_id' => 7, 'im2_category_name' => 'Other - Beauty', 'spot_category_name' => 'Other - Beauty']
            );
        }

        if ($brand->count() > 0) {
            $branda = Category::create(
                ['parent_id' => $brand->id,'name' => 'Brand Awareness']
            );

            CategoryMapping::create(
                ['parent_id' => $brand->id,'category_id'=>$branda->id, 'im2_category_id' => 9, 'im2_category_name' => 'Brand Awareness']
            );

            $lead = Category::create(
                ['parent_id' => $brand->id,'name' => 'Lead Generation']
            );

            CategoryMapping::create(
                ['parent_id' => $brand->id,'category_id'=>$lead->id, 'im2_category_id' => 10, 'im2_category_name' => 'Lead Generation']
            );

            $pharma = Category::create(
                ['parent_id' => $brand->id,'name' => 'Pharmaceutical']
            );

            CategoryMapping::create(
                ['parent_id' => $brand->id,'category_id'=>$pharma->id, 'im2_category_id' => 11, 'im2_category_name' => 'Pharmaceutical']
            );

            //No Mapping Here
            Category::create(
                ['parent_id' => $brand->id,'name' => 'Other']
            );
        }

        if ($business->count() > 0) {
            $businesso = Category::create(
                ['parent_id' => $business->id,'name' => 'Business Opportunity']
            );

            CategoryMapping::create(
                ['parent_id' => $business->id,'category_id'=>$businesso->id, 'im2_category_id' => 12, 'im2_category_name' => 'Business Opportunity', 'spot_category_name' => 'Business Opportunity']
            );

            $credit = Category::create(
                ['parent_id' => $business->id,'name' => 'Credit']
            );

            CategoryMapping::create(
                ['parent_id' => $business->id,'category_id'=>$credit->id, 'im2_category_id' => 13, 'im2_category_name' => 'Credit', 'spot_category_name' => 'Credit']
            );

            $finance = Category::create(
                ['parent_id' => $business->id,'name' => 'Finance']
            );

            CategoryMapping::create(
                ['parent_id' => $business->id,'category_id'=>$finance->id, 'im2_category_id' => 14, 'im2_category_name' => 'Finance', 'spot_category_name' => 'Finance']
            );

            $insurance = Category::create(
                ['parent_id' => $business->id,'name' => 'Insurance']
            );

            CategoryMapping::create(
                ['parent_id' => $business->id,'category_id'=>$insurance->id, 'spot_category_name' => 'Insurance']
            );

            $loan = Category::create(
                ['parent_id' => $business->id,'name' => 'Loan & Mortgage']
            );

            CategoryMapping::create(
                ['parent_id' => $business->id,'category_id'=>$loan->id, 'spot_category_name' => 'Loan & Mortgage']
            );

            $other = Category::create(
                ['parent_id' => $business->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $business->id,'category_id'=>$other->id, 'spot_category_name' => 'Other - Business & Finance']
            );
        }

        if ($computers->count() > 0) {
            $computer = Category::create(
                ['parent_id' => $computers->id,'name' => 'Computer']
            );

            CategoryMapping::create(
                ['parent_id' => $computers->id,'category_id'=>$computer->id, 'im2_category_id' => 15, 'im2_category_name' => 'Computer', 'spot_category_name' => 'Computer']
            );

            $electronics = Category::create(
                ['parent_id' => $computers->id,'name' => 'Electronics']
            );

            CategoryMapping::create(
                ['parent_id' => $computers->id,'category_id'=>$electronics->id, 'im2_category_id' => 16, 'im2_category_name' => 'Electronics', 'spot_category_name' => 'Electronics']
            );

            $internet = Category::create(
                ['parent_id' => $computers->id,'name' => 'Internet']
            );

            CategoryMapping::create(
                ['parent_id' => $computers->id,'category_id'=>$internet->id, 'im2_category_id' => 17, 'im2_category_name' => 'Internet']
            );

            $software = Category::create(
                ['parent_id' => $computers->id,'name' => 'Software']
            );

            CategoryMapping::create(
                ['parent_id' => $computers->id,'category_id'=>$software->id, 'im2_category_id' => 18, 'im2_category_name' => 'Software']
            );

            $telephony = Category::create(
                ['parent_id' => $computers->id,'name' => 'Telephony']
            );

            CategoryMapping::create(
                ['parent_id' => $computers->id,'category_id'=>$telephony->id, 'spot_category_name' => 'Telephony']
            );

            $hardware = Category::create(
                ['parent_id' => $computers->id,'name' => 'Hardware']
            );

            CategoryMapping::create(
                ['parent_id' => $computers->id,'category_id'=>$hardware->id, 'im2_category_id' => 65, 'im2_category_name' => 'Hardware']
            );

            //No Mapping
            Category::create(
                ['parent_id' => $computers->id,'name' => 'Other']
            );
        }

        if ($crafts->count() > 0) {
            $collectible = Category::create(
                ['parent_id' => $crafts->id,'name' => 'Collectibles']
            );

            CategoryMapping::create(
                ['parent_id' => $crafts->id,'category_id'=>$collectible->id, 'im2_category_id' => 19, 'im2_category_name' => 'Collectibles', 'spot_category_name' => 'Collectibles']
            );

            $craftso = Category::create(
                ['parent_id' => $crafts->id,'name' => 'Crafts']
            );

            CategoryMapping::create(
                ['parent_id' => $crafts->id,'category_id'=>$craftso->id, 'im2_category_id' => 20, 'im2_category_name' => 'Crafts', 'spot_category_name' => 'Crafts']
            );

            $hobbies = Category::create(
                ['parent_id' => $crafts->id,'name' => 'Hobbies']
            );

            CategoryMapping::create(
                ['parent_id' => $crafts->id,'category_id'=>$hobbies->id, 'im2_category_id' => 21, 'im2_category_name' => 'Hobbies', 'spot_category_name' => 'Hobbies']
            );

            // No Mapping here
            Category::create(
                ['parent_id' => $crafts->id,'name' => 'Other']
            );
        }

        if ($education->count() > 0) {
            $adult = Category::create(
                ['parent_id' => $education->id,'name' => 'Adult Education']
            );

            CategoryMapping::create(
                ['parent_id' => $education->id,'category_id'=>$adult->id, 'im2_category_id' => 22, 'im2_category_name' => 'Adult Education', 'spot_category_name' => 'Adult Ed.']
            );

            $children = Category::create(
                ['parent_id' => $education->id,'name' => 'Children Education']
            );

            CategoryMapping::create(
                ['parent_id' => $education->id,'category_id'=>$children->id, 'im2_category_id' => 23, 'im2_category_name' => 'Children Education', 'spot_category_name' => 'Children\'s Ed.']
            );

            $relationship = Category::create(
                ['parent_id' => $education->id,'name' => 'Relationships']
            );

            CategoryMapping::create(
                ['parent_id' => $education->id,'category_id'=>$relationship->id, 'im2_category_id' => 24, 'im2_category_name' => 'Relationships']
            );

            $selfhelp = Category::create(
                ['parent_id' => $education->id,'name' => 'Self Help']
            );

            CategoryMapping::create(
                ['parent_id' => $education->id,'category_id'=>$selfhelp->id, 'im2_category_id' => 25, 'im2_category_name' => 'Self Help', 'spot_category_name' => 'Self Help']
            );

            //No mapping here...
            Category::create(
                ['parent_id' => $education->id,'name' => 'Other']
            );
        }

        if ($entertainment->count() > 0) {
            $betting = Category::create(
                ['parent_id' => $entertainment->id,'name' => 'Betting']
            );

            CategoryMapping::create(
                ['parent_id' => $entertainment->id,'category_id'=>$betting->id, 'im2_category_id' => 26, 'im2_category_name' => 'Betting']
            );

            $dating = Category::create(
                ['parent_id' => $entertainment->id,'name' => 'Dating']
            );

            CategoryMapping::create(
                ['parent_id' => $entertainment->id,'category_id'=>$dating->id, 'im2_category_id' => 27, 'im2_category_name' => 'Betting']
            );

            $music = Category::create(
                ['parent_id' => $entertainment->id,'name' => 'Music']
            );

            CategoryMapping::create(
                ['parent_id' => $entertainment->id,'category_id'=>$music->id, 'im2_category_id' => 29, 'im2_category_name' => 'Music', 'spot_category_name' => 'Music']
            );

            $psychic = Category::create(
                ['parent_id' => $entertainment->id,'name' => 'Psychic']
            );

            CategoryMapping::create(
                ['parent_id' => $entertainment->id,'category_id'=>$psychic->id, 'im2_category_id' => 31, 'im2_category_name' => 'Psychic', 'spot_category_name' => 'Psychic']
            );

            $video = Category::create(
                ['parent_id' => $entertainment->id,'name' => 'Video']
            );

            CategoryMapping::create(
                ['parent_id' => $entertainment->id,'category_id'=>$video->id, 'im2_category_id' => 33, 'im2_category_name' => 'Music', 'spot_category_name' => 'Music']
            );

            $books = Category::create(
                ['parent_id' => $entertainment->id,'name' => 'Books']
            );

            CategoryMapping::create(
                ['parent_id' => $entertainment->id,'category_id'=>$books->id, 'spot_category_name' => 'Books']
            );

            $games = Category::create(
                ['parent_id' => $entertainment->id,'name' => 'Games & Toys']
            );

            CategoryMapping::create(
                ['parent_id' => $entertainment->id,'category_id'=>$games->id, 'spot_category_name' => 'Games & Toys']
            );

            $magazine = Category::create(
                ['parent_id' => $entertainment->id,'name' => 'Magazines']
            );

            CategoryMapping::create(
                ['parent_id' => $entertainment->id,'category_id'=>$magazine->id, 'spot_category_name' => 'Magazine']
            );

            $other = Category::create(
                ['parent_id' => $entertainment->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $entertainment->id,'category_id'=>$other->id, 'im2_category_id' => 30, 'im2_category_name' => 'Other - Entertainment', 'spot_category_name' => 'Other - Entertainment']
            );
        }

        if ($fashion->count() > 0) {
            $accessories = Category::create(
                ['parent_id' => $fashion->id,'name' => 'Accessories']
            );

            CategoryMapping::create(
                ['parent_id' => $fashion->id,'category_id'=>$accessories->id, 'im2_category_id' => 34, 'im2_category_name' => 'Accessories', 'spot_category_name' => 'Accessories - Fashion']
            );

            $apparel = Category::create(
                ['parent_id' => $fashion->id,'name' => 'Apparel']
            );

            CategoryMapping::create(
                ['parent_id' => $fashion->id,'category_id'=>$apparel->id, 'im2_category_id' => 35, 'im2_category_name' => 'Apparel', 'spot_category_name' => 'Apparel']
            );

            $jewelry = Category::create(
                ['parent_id' => $fashion->id,'name' => 'Jewelry']
            );

            CategoryMapping::create(
                ['parent_id' => $fashion->id,'category_id'=>$jewelry->id, 'im2_category_id' => 36, 'im2_category_name' => 'Jewelry', 'spot_category_name' => 'Jewelry']
            );

            $sunglasses = Category::create(
                ['parent_id' => $fashion->id,'name' => 'Sunglasses']
            );

            CategoryMapping::create(
                ['parent_id' => $fashion->id,'category_id'=>$sunglasses->id, 'im2_category_id' => 37, 'im2_category_name' => 'Sunglasses']
            );

            $other = Category::create(
                ['parent_id' => $fashion->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $fashion->id,'category_id'=>$other->id, 'spot_category_name' => 'Other - Fashion']
            );
        }

        if ($food->count() > 0) {
            $foodo = Category::create(
                ['parent_id' => $food->id,'name' => 'Food & Beverage']
            );

            CategoryMapping::create(
                ['parent_id' => $food->id,'category_id'=>$foodo->id, 'spot_category_name' => 'Food & Beverage']
            );

            Category::create(
                ['parent_id' => $food->id,'name' => 'Other']
            );
        }

        if ($health->count() > 0) {
            $diet = Category::create(
                ['parent_id' => $health->id,'name' => 'Diet & Weightloss']
            );

            CategoryMapping::create(
                ['parent_id' => $health->id,'category_id'=>$diet->id, 'im2_category_id' => 38, 'im2_category_name' => 'Diet & Weightloss', 'spot_category_name' => 'Diet & Weightloss']
            );

            $supp = Category::create(
                ['parent_id' => $health->id,'name' => 'Diet Supplements & Vitamins']
            );

            CategoryMapping::create(
                ['parent_id' => $health->id,'category_id'=>$supp->id, 'im2_category_id' => 39, 'im2_category_name' => 'Diet Supplements & Vitamins', 'spot_category_name' => 'Diet Supplements & Vitamins']
            );

            $exercise = Category::create(
                ['parent_id' => $health->id,'name' => 'Exercise Equipment']
            );

            CategoryMapping::create(
                ['parent_id' => $health->id,'category_id'=>$exercise->id, 'im2_category_id' => 40, 'im2_category_name' => 'Exercise Equipment', 'spot_category_name' => 'Exercise Equipment']
            );

            $expro = Category::create(
                ['parent_id' => $health->id,'name' => 'Exercise Programs']
            );

            CategoryMapping::create(
                ['parent_id' => $health->id,'category_id'=>$expro->id, 'im2_category_id' => 41, 'im2_category_name' => 'Exercise Programs', 'spot_category_name' => 'Exercise Programs']
            );

            $pharm = Category::create(
                ['parent_id' => $health->id,'name' => 'Pharmaceuticals']
            );

            CategoryMapping::create(
                ['parent_id' => $health->id,'category_id'=>$pharm->id, 'spot_category_name' => 'Pharmaceuticals']
            );

            $other = Category::create(
                ['parent_id' => $health->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $health->id,'category_id'=>$other->id, 'im2_category_id' => 42, 'im2_category_name' => 'Other - Health & Fitness', 'spot_category_name' => 'Other - Health & Fitness']
            );
        }

        if ($household->count() > 0) {
            $appliance = Category::create(
                ['parent_id' => $household->id,'name' => 'Appliance']
            );

            CategoryMapping::create(
                ['parent_id' => $household->id,'category_id'=>$appliance->id, 'im2_category_id' => 43, 'im2_category_name' => 'Appliance - Household', 'spot_category_name' => 'Appliance - Household']
            );

            $cleaners = Category::create(
                ['parent_id' => $household->id,'name' => 'Cleaners']
            );

            CategoryMapping::create(
                ['parent_id' => $household->id,'category_id'=>$cleaners->id, 'im2_category_id' => 44, 'im2_category_name' => 'Cleaners', 'spot_category_name' => 'Cleaners']
            );

            $homeimprovement = Category::create(
                ['parent_id' => $household->id,'name' => 'Home Improvement']
            );

            CategoryMapping::create(
                ['parent_id' => $household->id,'category_id'=>$homeimprovement->id, 'im2_category_id' => 45, 'im2_category_name' => 'Home Improvement', 'spot_category_name' => 'Home Improvement']
            );

            $housewares = Category::create(
                ['parent_id' => $household->id,'name' => 'Housewares']
            );

            CategoryMapping::create(
                ['parent_id' => $household->id,'category_id'=>$housewares->id, 'im2_category_id' => 46, 'im2_category_name' => 'Housewares']
            );

            $outdoor = Category::create(
                ['parent_id' => $household->id,'name' => 'Outdoor/Garden']
            );

            CategoryMapping::create(
                ['parent_id' => $household->id,'category_id'=>$outdoor->id, 'im2_category_id' => 47, 'im2_category_name' => 'Outdoor/Garden', 'spot_category_name' => 'Outdoor/Garden']
            );

            $safetysecure = Category::create(
                ['parent_id' => $household->id,'name' => 'Safety & Security']
            );

            CategoryMapping::create(
                ['parent_id' => $household->id,'category_id'=>$safetysecure->id, 'im2_category_id' => 48, 'im2_category_name' => 'Safety & Security', 'spot_category_name' => 'Safety & Security - Household']
            );

            $tools = Category::create(
                ['parent_id' => $household->id,'name' => 'Tools']
            );

            CategoryMapping::create(
                ['parent_id' => $household->id,'category_id'=>$tools->id, 'im2_category_id' => 49, 'im2_category_name' => 'Tools', 'spot_category_name' => 'Tools']
            );

            $furnishings = Category::create(
                ['parent_id' => $household->id,'name' => 'Furnishings']
            );

            CategoryMapping::create(
                ['parent_id' => $household->id,'category_id'=>$furnishings->id, 'spot_category_name' => 'Furnishings']
            );

            $storage = Category::create(
                ['parent_id' => $household->id,'name' => 'Storage']
            );

            CategoryMapping::create(
                ['parent_id' => $household->id,'category_id'=>$storage->id,'spot_category_name' => 'Storage']
            );

            $other = Category::create(
                ['parent_id' => $household->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $household->id,'category_id'=>$other->id, 'spot_category_name' => 'Other - Household']
            );
        }

        if ($kitchen->count() > 0) {
            $appliance = Category::create(
                ['parent_id' => $kitchen->id,'name' => 'Appliance']
            );

            CategoryMapping::create(
                ['parent_id' => $kitchen->id,'category_id'=>$appliance->id, 'im2_category_id' => 50, 'im2_category_name' => 'Appliance - Kitchen', 'spot_category_name' => 'Appliance - Kitchen']
            );

            $cookware = Category::create(
                ['parent_id' => $kitchen->id,'name' => 'Cookware']
            );

            CategoryMapping::create(
                ['parent_id' => $kitchen->id,'category_id'=>$cookware->id, 'im2_category_id' => 51, 'im2_category_name' => 'Cookware', 'spot_category_name' => 'Cookware']
            );

            $slicer = Category::create(
                ['parent_id' => $kitchen->id,'name' => 'Slicer-Dicers']
            );

            CategoryMapping::create(
                ['parent_id' => $kitchen->id,'category_id'=>$slicer->id, 'im2_category_id' => 53, 'im2_category_name' => 'Slicer-Dicers', 'spot_category_name' => 'Slicer-Dicers']
            );

            $instruct = Category::create(
                ['parent_id' => $kitchen->id,'name' => 'Instructional']
            );

            CategoryMapping::create(
                ['parent_id' => $kitchen->id,'category_id'=>$instruct->id, 'spot_category_name' => 'Instructional']
            );

            $storing = Category::create(
                ['parent_id' => $kitchen->id,'name' => 'Storing']
            );

            CategoryMapping::create(
                ['parent_id' => $kitchen->id,'category_id'=>$storing->id, 'spot_category_name' => 'Storing']
            );

            $other = Category::create(
                ['parent_id' => $kitchen->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $kitchen->id,'category_id'=>$storing->id, 'im2_category_id' => 52, 'im2_category_name' => 'Other - Kitchen', 'spot_category_name' => 'Other - Kitchen']
            );
        }

        if ($misc->count() > 0) {
            $notfor = Category::create(
                ['parent_id' => $misc->id,'name' => 'Not for Profit']
            );

            CategoryMapping::create(
                ['parent_id' => $misc->id,'category_id'=>$notfor->id, 'im2_category_id' => 54, 'im2_category_name' => 'Not for Profit', 'spot_category_name' => 'Not for Profit']
            );

            $religious = Category::create(
                ['parent_id' => $misc->id,'name' => 'Religious']
            );

            CategoryMapping::create(
                ['parent_id' => $misc->id,'category_id'=>$religious->id, 'im2_category_id' => 57, 'im2_category_name' => 'Religious', 'spot_category_name' => 'Religious']
            );

            $membership = Category::create(
                ['parent_id' => $misc->id,'name' => 'Memberships, Services']
            );

            CategoryMapping::create(
                ['parent_id' => $misc->id,'category_id'=>$membership->id, 'spot_category_name' => 'Memberships, Services']
            );

            $other = Category::create(
                ['parent_id' => $misc->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $misc->id,'category_id'=>$other->id, 'im2_category_id' => 55, 'im2_category_name' => 'Other - Miscellaneous', 'spot_category_name' => 'Other - Miscellaneous']
            );
        }

        if ($novelties->count() > 0) {
            $noveltieso = Category::create(
                ['parent_id' => $novelties->id,'name' => 'Novelties']
            );

            CategoryMapping::create(
                ['parent_id' => $novelties->id,'category_id'=>$noveltieso->id, 'spot_category_name' => 'Novelties']
            );

            // No mapping
            Category::create(
                ['parent_id' => $novelties->id,'name' => 'Other']
            );
        }

        if ($personal->count() > 0) {
            $bath = Category::create(
                ['parent_id' => $personal->id,'name' => 'Bath & Massage']
            );

            CategoryMapping::create(
                ['parent_id' => $personal->id,'category_id'=>$bath->id,'im2_category_id' => 58, 'im2_category_name' => 'Bath & Massage', 'spot_category_name' => 'Bath & Massage']
            );

            $pain = Category::create(
                ['parent_id' => $personal->id,'name' => 'Pain Relief']
            );

            CategoryMapping::create(
                ['parent_id' => $personal->id,'category_id'=>$pain->id,'im2_category_id' => 60, 'im2_category_name' => 'Pain Relief', 'spot_category_name' => 'Pain Relief']
            );

            $self = Category::create(
                ['parent_id' => $personal->id,'name' => 'Self Defense']
            );

            CategoryMapping::create(
                ['parent_id' => $personal->id,'category_id'=>$self->id,'im2_category_id' => 61]
            );

            $appliance = Category::create(
                ['parent_id' => $personal->id,'name' => 'Appliance']
            );

            CategoryMapping::create(
                ['parent_id' => $personal->id,'category_id'=>$appliance->id,'spot_category_name' => 'Appliance - Personal']
            );

            $feminine = Category::create(
                ['parent_id' => $personal->id,'name' => 'Feminine']
            );

            CategoryMapping::create(
                ['parent_id' => $personal->id,'category_id'=>$feminine->id,'spot_category_name' => 'Feminine']
            );

            $masculine = Category::create(
                ['parent_id' => $personal->id,'name' => 'Masculine']
            );

            CategoryMapping::create(
                ['parent_id' => $personal->id,'category_id'=>$masculine->id,'spot_category_name' => 'Masculine']
            );

            $other = Category::create(
                ['parent_id' => $personal->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $personal->id,'category_id'=>$other->id, 'im2_category_id' => 59, 'im2_category_name' => 'Other - Personal', 'spot_category_name' => 'Other - Personal']
            );
        }

        if ($pets->count() > 0) {
            $accessories = Category::create(
                ['parent_id' => $pets->id,'name' => 'Accessories']
            );

            CategoryMapping::create(
                ['parent_id' => $pets->id,'category_id'=>$accessories->id,'im2_category_id' => 56, 'im2_category_name' => 'Pet', 'spot_category_name' => 'Accessories - Pet Products']
            );

            $training = Category::create(
                ['parent_id' => $pets->id,'name' => 'Training']
            );

            CategoryMapping::create(
                ['parent_id' => $pets->id,'category_id'=>$training->id, 'spot_category_name' => 'Training']
            );

            $other = Category::create(
                ['parent_id' => $pets->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $pets->id,'category_id'=>$other->id, 'spot_category_name' => 'Other - Pet Products']
            );
        }

        if ($sports->count() > 0) {
            $fishing = Category::create(
                ['parent_id' => $sports->id,'name' => 'Fishing']
            );

            CategoryMapping::create(
                ['parent_id' => $sports->id,'category_id'=>$fishing->id,'im2_category_id' => 62, 'im2_category_name' => 'Fishing', 'spot_category_name' => 'Fishing']
            );

            $golf = Category::create(
                ['parent_id' => $sports->id,'name' => 'Golf']
            );

            CategoryMapping::create(
                ['parent_id' => $sports->id,'category_id'=>$golf->id,'im2_category_id' => 63, 'im2_category_name' => 'Golf', 'spot_category_name' => 'Golf']
            );

            $collect = Category::create(
                ['parent_id' => $sports->id,'name' => 'Collectibles']
            );

            CategoryMapping::create(
                ['parent_id' => $sports->id,'category_id'=>$collect->id,'spot_category_name' => 'Collectibles - Sports']
            );

            $other = Category::create(
                ['parent_id' => $sports->id,'name' => 'Other']
            );

            CategoryMapping::create(
                ['parent_id' => $sports->id,'category_id'=>$other->id,'im2_category_id' => 64, 'im2_category_name' => 'Other - Sports', 'spot_category_name' => 'Other - Sports']
            );
        }

        if ($travel->count() > 0) {
            $leisure = Category::create(
                ['parent_id' => $travel->id,'name' => 'Leisure']
            );

            CategoryMapping::create(
                ['parent_id' => $travel->id,'category_id'=>$leisure->id,'im2_category_id' => 28, 'im2_category_name' => 'Leisure', 'spot_category_name' => 'Leisure']
            );

            $travelo = Category::create(
                ['parent_id' => $travel->id,'name' => 'Travel']
            );

            CategoryMapping::create(
                ['parent_id' => $travel->id,'category_id'=>$travelo->id,'im2_category_id' => 32, 'im2_category_name' => 'Travel', 'spot_category_name' => 'Travel']
            );

            Category::create(
                ['parent_id' => $travel->id,'name' => 'Other']
            );
        }
    }
}
