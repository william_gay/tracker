<?php

namespace App\Console\Commands;

use App\Models\Spot;
use App\Models\SpotMapping;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class FixSpotTextFieldsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ims:fixspottext';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to fix the text fields in ims database since they were pulled from a tinytext.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        //Disable query logging on this big db
        DB::connection()->disableQueryLog();
        DB::connection('legacy_spot')->disableQueryLog();

        $at_a_time = 10000;

        $number_queries = ceil(Spot::orderBy('id')->count() / $at_a_time);
        $this->info('This script will run '.$number_queries.' queries!');
        $query = 0;
        $limit = 0;

        while ($query < $number_queries) {
            //Select the first batch
            $spots = Spot::orderBy('id')->skip($limit)->take($at_a_time)->get();

            $this->comment('Query: '.$query.' Limit: '.$limit);

            foreach ($spots as $spot) {
                //Get Program Mapping:
                $spotmap = SpotMapping::where('spot_id', $spot->id)->first();
                if (!$spotmap) {
                    $this->error('Spot Map Not Found! ID: '.$spot->id.' Title:'.$spot->title);
                } else {
                    $legacy_spot = DB::connection('legacy_spot')->table('Spot Log')
                                        ->where('ID', $spotmap->legacy_spot_id)
                                        ->first();

                    $this->info('Title: '. $legacy_spot->{'Product Name'});
                    $this->info('Description: '.$legacy_spot->{'Product Description'});
                    $this->info('Remarks: '.$legacy_spot->{'Remarks'});

                    $update = Spot::findOrFail($spot->id);
                    $update->description = $legacy_spot->{'Product Description'};
                    $update->remarks = $legacy_spot->{'Remarks'};
                    $update->save();
                }
            }
            $query++;
            $limit = $limit + $at_a_time;
        }
    }
}
