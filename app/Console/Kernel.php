<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\LinkSpotToDetectionsAndRankingsCommand::class,
        \App\Console\Commands\TalleyQuarterlyRankingsCommand::class,
        \App\Console\Commands\UpdateSpotRankings::class,
        \App\Console\Commands\AddDescriptionsToSpanish::class,
        \App\Console\Commands\SetMonitorDateCommand::class,
        \App\Console\Commands\UpdateDBIm2Command::class,
        \App\Console\Commands\FixProgramTextFieldsCommand::class,
        \App\Console\Commands\SyncPGWeeklyCommand::class,
        \App\Console\Commands\ClearBusterCacheCommand::class,
        \App\Console\Commands\SyncChannelsCommand::class,
        \App\Console\Commands\SendOutAlertsCommand::class,
        \App\Console\Commands\FixAirtimeCostCommand::class,
        \App\Console\Commands\UpdateLegacyCommand::class,
        \App\Console\Commands\SlugifyRetailersCommand::class,
        \App\Console\Commands\SyncSpotLogCommand::class,
        \App\Console\Commands\InsertCategoriesCommand::class,
        \App\Console\Commands\FixSpotMappingCommand::class,
        \App\Console\Commands\SyncSpotRankingCommand::class,
        \App\Console\Commands\AddUserCommand::class,
        \App\Console\Commands\UpdateDBSpotCommand::class,
        \App\Console\Commands\RemoveProductsThatAreServicesCommand::class,
        \App\Console\Commands\SyncSpotDetectionsCommand::class,
        \App\Console\Commands\FixProgramCategoriesCommand::class,
        \App\Console\Commands\FixStreakColumnCommand::class,
        \App\Console\Commands\SyncProgramsCommand::class,
        \App\Console\Commands\InsertSpotMasterIdCommand::class,
        \App\Console\Commands\SyncAirtimeCommand::class,
        \App\Console\Commands\CreateOAuthClientForAllServersCommand::class,
        \App\Console\Commands\DeleteAllOAuthClientsCommand::class,
        \App\Console\Commands\DownloadDatabasesCommand::class,
        \App\Console\Commands\FixProgramWebsiteAndChannel::class,
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\FixSpotNewVersionCommand::class,
        \App\Console\Commands\TallyShowDetectionsCommand::class,
        \App\Console\Commands\RetailProductCategoryUpdateCommand::class,
        \App\Console\Commands\GenerateEmptySlugsCommand::class,
        \App\Console\Commands\FixMissingAirtimeCostsCommand::class,
        \App\Console\Commands\FixSpotRankingsCommand::class,
        \App\Console\Commands\SyncCostsCommand::class,
        \App\Console\Commands\AssociateFirstChannelCommand::class,
        \App\Console\Commands\SyncCompaniesCommand::class,
        \App\Console\Commands\QueueFailedMassRetry::class,
        \App\Console\Commands\MakeS3DirectoryCommand::class,
        \App\Console\Commands\FixSpotTextFieldsCommand::class,
        \App\Console\Commands\SyncLegacyCommand::class,
        \App\Console\Commands\FixLSTProducts::class,
        \App\Console\Commands\FixIM2AirtimeDateTimeCommand::class,
        \App\Console\Commands\TallySpotDetectionsCommand::class,
        \App\Console\Commands\FixWeekEndingDates::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('horizon:snapshot')->everyFiveMinutes();
        $schedule->command('telescope:prune --hours=48')->daily();

        $schedule->command('rankings:run')
            ->weekly()
            ->sundays()
            ->timezone('America/New_York')
            ->at('08:15')
            ->thenPing('http://beats.envoyer.io/heartbeat/5ZCL2a9xZv0WCY8');

        $schedule->command('winmo:predictor')->weeklyOn(7, '21:30');
        $schedule->command('winmo:media-spend')->weeklyOn(7, '22:30');
        $schedule->command('products:presence')->weeklyOn(7, '23:00');
        $schedule->command('winmo:networks')->weeklyOn(7, '23:50');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
