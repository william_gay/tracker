require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import MediaSpend from './components/MediaSpend.vue';
Vue.component('media-spend', MediaSpend);
import Coverage from './components/Coverage.vue';
Vue.component('coverage', Coverage);
import Predictor from './components/Predictor.vue';
Vue.component('predictor', Predictor);

import { VTooltip, VPopover, VClosePopover } from 'v-tooltip'

Vue.directive('tooltip', VTooltip)
Vue.directive('close-popover', VClosePopover)
Vue.component('v-popover', VPopover)

const app = new Vue({
    el: '#app'
});
