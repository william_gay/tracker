<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| Products Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used for Spots
	|
	*/
	'description' => 'Product Description',
	'price' => 'Price'

);