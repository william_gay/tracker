<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Spots Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Spots
    |
    */

    'spots' => 'Spot|Spots',
    'title' => 'Title',
    'price' => 'Price',
    'price_extra' => 'Original Price',
    'shipping' => 'Shipping and Handling',
    'offer' => 'Offer',
    'length' => 'Length',
    'category' => 'Category',
    'marketing_company' => 'Marketing Company',
    'phone' => 'Phone',
    'website' => 'Website',
    'channels' => 'Channels',
    'air_date' => 'Date Aired',
    'intial_date' => 'Initial Date',
    'week_ending' => 'Week Ending',
    'review' => 'Review',
    'detections' => 'Detection|Detections'
);
