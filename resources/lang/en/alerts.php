<?php

return array(

    'alerts' => 'Alert|Alerts',
    'category_subject' => 'IMS Alert: Category Alert',
    'show_subject' => 'IMS Alert: New Shows Alert',
    'spots_subject' => 'IMS Alert: New Spots Alert',
    'show_intro' => 'The following shows were detected',
    'spot_intro' => 'The following spots were detected',
    'category_intro' => 'The following shows and spots were detected in :category',
    'footer' => 'You are receiving this message because you signed up for alerts on our website. To discontinue alerts, you can remove any configured alerts <a href="https://my.imsreport.com/user/alerts">here</a>.',
);
