<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Manufacturers Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	"manufacturers" => 'Manufacturer|Manufacturers',
	"create" => "New Manufacturer",
	"edit" => "Edit Manufacturer"

);
