<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| Retailers Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used for Spots
	|
	*/

	'retailers' => 'Retailers',
	'sqft' => 'Average Square Feet',
	'store_count' => 'Store Count',
	'website' => 'Website',
	'name' => 'Retailer',
	'no_product_data' => 'No product data available yet!',
	'stores' => 'Stores',
	'products' => 'Products',
	'categories' => 'Categories',
	'product_distribution_category' => 'Product Distribution by Category',
	'total_products' => 'Products',
	'lowest_price' => 'Lowest Price',
	'highest_price' => 'Highest Price',
	'drtv_price' => 'DRTV Price',
	'total_airs' => 'Airs',
	'total_media_spend' => 'Media Spend',
	'retail_distribution' => 'Retail Distribution',
	'product_breakdown' => 'Product Breakdown',
	'high_ims_rank' => 'Max IMS Rank',
	'ims_streak' => 'Weeks Ranked',
	'distribution' => 'Retail Distribution'

);