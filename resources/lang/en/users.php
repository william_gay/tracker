<?php

return array(

    'logout'                     => 'Logout successful.',
    'register_success'           => 'Thank you for registering. Please check your email to activate your account!',
    'create_success'             => 'User created.',
    'update_success'             => 'User updated.',
    'login_success'              => 'You are now logged in.',
    'delete_denied'              => 'Sorry you can\'t delete yourself.',
    'delete_success'             => 'User deleted.',
    'permissions_update_success' => 'User permissions updated.',
    'activation_success'         => 'User activated successfully.',
    'activation_fail'            => 'User activation failed.',
    'deactivation_success'       => 'User deactivated successfully.',
    'not_found'                  => 'User Not Found!',
    'password_reset'             => 'Password Reset!',
    'password_reset_failed'      => 'Reset failed.',
    'password_reset_code_invalid'=> 'Reset code invalid.',
    'password_reset_success'     => 'You will receive a password reset email shortly.',
    'welcome_subject'            => 'Welcome to the Media Analytics\' DRTV Tracker',
    'subscription_create_success' => 'Congratulations! You have successfully started your subscription.',
    'subscription_cancel_success' => 'You have successfully canceled your subscription.'
);
