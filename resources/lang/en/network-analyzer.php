<?php

return [
    'no-products' => 'No results returned with the given filters. A show or spot is required to show insights.',
];
