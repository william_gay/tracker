<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Global Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used globally
	|
	*/

	'actions' => 'Actions',
	'edit' => 'Edit',
	'delete' => 'Delete',
	'created_at' => 'Created At',
	'updated_at' => 'Updated At',
	'slug' => 'Slug',
	'name' => 'Name',

);