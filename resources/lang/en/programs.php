<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Programs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for Programs
    |
    */

    'programs' => 'Program|Programs',
    'title' => 'Title',
    'product_name' => 'Product Name',
    'grid_title' => 'Grid Title',
    'host' => 'Host',
    'host_extra' => '2nd Host',
    'cost' => 'Cost',
    'price' => 'Price',
    'category' => 'Category',
    'marketing_company' => 'Marketing Company',
    'production_company' => 'Production Company',
    'address' => 'Address',
    'phone' => 'Phone',
    'website' => 'Website',
    'channel' => '1st Channel Aired',
    'air_date' => 'Air Date',
    'week_ending' => 'Week Ending',
    'upsell' => 'Upsell',
);