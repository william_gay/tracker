<?php

return array(
	/*
	|--------------------------------------------------------------------------
	| Retailer Products Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used for Spots
	|
	*/
	'attributes' => 'Attributes',
	'general_configuration' => 'General Configuration',
	'date_recorded' => 'Record Date'
);