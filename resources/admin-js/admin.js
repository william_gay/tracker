$(function() {

    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });

    $('.dropdown-toggle').dropdown();

    $.extend(true, $.fn.dataTable.defaults, {
        "dom": 'Bfltip',
        "pageLength": 25,
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "buttons": [
            'copy', 'excel', 'pdf', 'csv'
        ]
    });

    $('a[rel=tooltip]').tooltip();

    $('.select2').select2({
        'width':"element",
        'placeholder' : "Select"
    });

    $('#keywords').select2({
        tags: true,
        tokenSeparators: [","],
        placeholder: "Enter some keywords...",
        minimumInputLength: 2,
        multiple: true,
        createSearchChoice: function (term) {
            return {
                id: $.trim(term),
                text: $.trim(term) + ' (new tag)'
            };
        },
        ajax: {
            url: '/api/keywords/search',
            dataType: "json",
            data: function(term, page) {
                return {
                    q: term
                };
            },
            results: function(data, page) {
                return {
                    results: data.keywords
                };
            }
        },
        initSelection: function (element, callback) {
            var data = [];

            function splitVal(string, separator) {
                var val, i, l;
                if (string === null || string.length < 1) return [];
                val = string.split(separator);
                for (i = 0, l = val.length; i < l; i = i + 1) val[i] = $.trim(val[i]);
                return val;
            }

            $(splitVal(element.val(), ",")).each(function () {
                data.push({
                    id: this,
                    text: this
                });
            });

            callback(data);
        }
    });

    $('#channel_id').select2();
    $('#file_name').select2();

    $('.slug').slugify('#name');

    $('.datetimepicker').datetimepicker({
        format: "MM/DD/YYYY hh:mm:ss A"
    });

    $('.dtpicker').datetimepicker({
        format: "YYYY-MM-DD hh:mm A"
    });

    $( "#startDate" ).datetimepicker({
        format: "YYYY-MM-DD"
    });
    $( "#endDate" ).datetimepicker({
        format: "YYYY-MM-DD"
    });

    $('.date-picker').datetimepicker({
        format: "YYYY-MM-DD"
    });
    $('#initial_date').datetimepicker({
        format: "YYYY-MM-DD HH:mm:ss"
    });
    $( ".monitor-date" ).datetimepicker({
        useCurrent: false,
        format: "YYYY-MM-DD"
    });
    $('#monitor_date').datetimepicker({
        useCurrent: false,
        format: "YYYY-MM-DD",
        daysOfWeekDisabled: [
          0,1,2,3,4,6
        ]
    });
    $( "#air_date_frm" ).datetimepicker({
        useCurrent: false,
        format: "YYYY-MM-DD",
        daysOfWeekDisabled: [
          0,1,2,3,4,6
        ]
    });

    $('.time-picker').datetimepicker({
        format: "hh:mm"
    });

    $('.first_airdate').datetimepicker({
        format: "YYYY-MM-DD hh:mm:ss A"
    });

    $('#category_id').change(function(){
        $.get("/api/categories/dropdown",
            { parent_id: $(this).val() },
            function(data) {
                var sub_category_id = $('#sub_category_id');
                sub_category_id.empty();
                $.each(data.categories, function(index, element) {
                    sub_category_id.append("<option value='"+ index +"'>" + element + "</option>");
                });
            }
        );
    });

    $('#live_shopping_category_id').change(function(){
        $.get("/api/lst/categories/dropdown",
            { parent_id: $(this).val() },
            function(data) {
                var sub_category_id = $('#live_shopping_sub_category_id');
                sub_category_id.empty();
                $.each(data.categories, function(index, element) {
                    sub_category_id.append("<option value='"+ index +"'>" + element + "</option>");
                });
                sub_category_id.val($('#live_shopping_sub_category_id_hidden').val());
            }
        );
    });

    $('#company_id').select2({
        placeholder: "Search for a Company",
        minimumInputLength: 3,
        ajax: {
            url: "/api/companies/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 30,
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.companies};
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/companies/one", {
                    data: {
                        company_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: formatName,
        formatSelection: formatName,
        escapeMarkup: function (m) { return m; }
    });

    $('#marketing_company_id').select2({
        placeholder: "Search for a Company",
        minimumInputLength: 3,
        ajax: {
            url: "/api/companies/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 30,
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.companies};
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/companies/one", {
                    data: {
                        company_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: formatName,
        formatSelection: formatName,
        escapeMarkup: function (m) { return m; }
    });

    $('#production_company_id').select2({
        placeholder: "Search for a Company",
        minimumInputLength: 3,
        ajax: {
            url: "/api/companies/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 30,
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.companies};
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/companies/one", {
                    data: {
                        company_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: formatName,
        formatSelection: formatName,
        escapeMarkup: function (m) { return m; }
    });

    $('#spot_id').select2({
        placeholder: "Search for a Spot",
        minimumInputLength: 2,
        ajax: {
            url: "/api/spots/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 25,
                    language_id: $('#language_id').val()
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.spots};
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/spots/one", {
                    data: {
                        spot_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: formatTitle,
        formatSelection: formatTitle,
        escapeMarkup: function (m) { return m; }
    });

    $('#spot_ids').select2({
        multiple: true,
        placeholder: "Search for Spots",
        minimumInputLength: 2,
        ajax: {
            url: "/api/spots/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 25,
                    language_id: $('#language_id').val()
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.spots};
            }
        },
        initSelection: function(element, callback) {
            var returndata = [];
            $(element.val().split(",")).each(function () {
                $.ajax("/api/spots/one", {
                    data: {
                        spot_id: this
                    },
                    dataType: "jsonp"
                }).done(function(data) {
                    returndata.push({id: data.id, title: data.title});
                    callback(returndata);
                });
            });
        },
        formatResult: formatTitle,
        formatSelection: formatTitle,
        escapeMarkup: function (m) { return m; }
    });

    $('#program_id').select2({
        placeholder: "Search for a Program",
        minimumInputLength: 2,
        ajax: {
            url: "/api/programs/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 25,
                    language_id: $('#language_id').val()
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.programs};
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/programs/one", {
                    data: {
                        program_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: formatTitle,
        formatSelection: formatTitle,
        escapeMarkup: function (m) { return m; }
    });

    $('#program_ids').select2({
        multiple: true,
        placeholder: "Search for Programs",
        minimumInputLength: 2,
        ajax: {
            url: "/api/programs/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 25,
                    language_id: $('#language_id').val()
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.programs};
            }
        },
        initSelection: function(element, callback) {
            var returndata = [];
            $(element.val().split(",")).each(function () {
                $.ajax("/api/programs/one", {
                    data: {
                        program_id: this
                    },
                    dataType: "jsonp"
                }).done(function(data) {
                    returndata.push({id: data.id, title: data.title});
                    callback(returndata);
                });
            });
        },
        formatResult: formatTitle,
        formatSelection: formatTitle,
        escapeMarkup: function (m) { return m; }
    });

    $('#retailer_id').select2({
        placeholder: "Search for a Retailer",
        minimumInputLength: 2,
        ajax: {
            url: "/api/retailers/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 25
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.retailers};
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/retailers/one", {
                    data: {
                        retailer_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: formatTitle,
        formatSelection: formatTitle,
        escapeMarkup: function (m) { return m; }
    });

    $('#retailer_product_id').select2({
        placeholder: "Search for a Retailer Product",
        minimumInputLength: 2,
        ajax: {
            url: "/api/retailer-products/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 25
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.products};
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/retailer-products/one", {
                    data: {
                        retailer_product_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: formatTitle,
        formatSelection: formatTitle,
        escapeMarkup: function (m) { return m; }
    });

    $('#retailer_product_ids').select2({
        multiple: true,
        placeholder: "Search for Retailer Products",
        minimumInputLength: 2,
        ajax: {
            url: "/api/retailer-products/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 25
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.products};
            }
        },
        initSelection: function(element, callback) {
            var returndata = [];
            $(element.val().split(",")).each(function () {
                $.ajax("/api/retailer-products/one", {
                    data: {
                        retailer_product_id: this
                    },
                    dataType: "jsonp"
                }).done(function(data) {
                    returndata.push({id: data.id, title: data.title});
                    callback(returndata);
                });
            });
        },
        formatResult: formatTitle,
        formatSelection: formatTitle,
        escapeMarkup: function (m) { return m; }
    });

    $('#category_ids').select2({
        multiple: true,
        placeholder: "Search for Categories",
        minimumInputLength: 2,
        ajax: {
            url: "/api/categories/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 25
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.categories};
            }
        },
        initSelection: function(element, callback) {
            var returndata = [];
            $(element.val().split(",")).each(function () {
                $.ajax("/api/categories/one", {
                    data: {
                        category_id: this
                    },
                    dataType: "jsonp"
                }).done(function(data) {
                    returndata.push({id: data.id, title: data.name});
                    callback(returndata);
                });
            });
        },
        formatResult: formatName,
        formatSelection: formatName,
        escapeMarkup: function (m) { return m; }
    });

    $('#product_id').select2({
        multiple: false,
        placeholder: "Search for Product",
        minimumInputLength: 2,
        ajax: {
            url: "/api/products/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 25
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.products};
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/products/one", {
                    data: {
                        product_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: formatName,
        formatSelection: formatName,
        escapeMarkup: function (m) { return m; }
    });

    $('#product_ids').select2({
        multiple: true,
        placeholder: "Search for Products",
        minimumInputLength: 2,
        ajax: {
            url: "/api/products/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 25
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.products};
            }
        },
        initSelection: function(element, callback) {
            var returndata = [];
            $(element.val().split(",")).each(function () {
                $.ajax("/api/products/one", {
                    data: {
                        product_id: this
                    },
                    dataType: "jsonp"
                }).done(function(data) {
                    returndata.push({id: data.id, title: data.title});
                    callback(returndata);
                });
            });
        },
        formatResult: formatTitle,
        formatSelection: formatTitle,
        escapeMarkup: function (m) { return m; }
    });

});

function formatTitle(format){
    return format.title;
}

function formatName(format){
    return format.name;
}

/**
 * Create a confirm modal
 * We want to send an HTTP DELETE request
 *
 * @usage  <a href="posts/2" data-method="delete"
 *          data-modal-text="Are you sure you want to delete"
 *         >
 *
 */
(function() {

    laravel =
    {
        initialize: function()
        {
            this.methodLinks = $('a[data-method]');
            this.registerEvents();
        },

        registerEvents: function()
        {
            this.methodLinks.on('click', this.handleMethod);
        },

        handleMethod: function(e)
        {
            e.preventDefault();
            var link = $(this);

            var httpMethod = link.data('method').toUpperCase();
            var allowedMethods = ['PUT', 'DELETE'];
            var extraMsg = link.data('modal-text');
            var msg  = '<i class="icon-warning-sign modal-icon"></i>&nbsp;Are you sure you want to&nbsp;' + extraMsg;

            // If the data-method attribute is not PUT or DELETE,
            // then we don't know what to do. Just ignore.
            if ( $.inArray(httpMethod, allowedMethods) === - 1 )
            {
                return;
            }

            bootbox.dialog({
                "message": msg,
                "title": "Please Confirm",
                "buttons": {
                    danger: {
                        label: "Confirm Delete",
                        className: "btn-danger",
                        callback: function() {
                            var form =
                                $('<form>', {
                                    'method': 'POST',
                                    'action': link.attr('href')
                                });
                            var hiddenInput =
                                $('<input>', {
                                    'name': '_method',
                                    'type': 'hidden',
                                    'value': link.data('method')
                                });
                            var csrfToken =
                                $('<input>', {
                                    'name': '_token',
                                    'type': 'hidden',
                                    'value': $('meta[name="csrf-token"]').attr('content')
                                });
                            form.append(hiddenInput).append(csrfToken).appendTo('body').submit();
                        }
                    },
                    close: {
                        label: "Cancel",
                        className: "btn"
                    }
                }
            });
        }
    };

    laravel.initialize();

})();
