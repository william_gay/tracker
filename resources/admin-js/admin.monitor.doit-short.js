var newTable;
$(function () {
  $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
  });

  $("#air_date_changer").datetimepicker({
    format: "YYYY-MM-DD"
  });

  $("#air_date_changer").on("dp.change", function(e) {
      var newdate = $("#air_date_changer").val();
      $('#air_date').val(newdate);
      //Update Dropdown with Dayparts
      var daypartdd = $.ajax({
        url: "/api/dayparts",
        data: { day: newdate},
        animation: "spinner",
        async: true
      });

      daypartdd.done(function (json) {
        var i;
        $('#daypart_id').empty().select2("destroy");
        for (i = 0; i < json.length; i++) {
          $("<option/>").attr("value", json[i].id).text(json[i].name + ' ' + json[i].abbr + ' Time: ' + json[i].time_start + ' - ' + json[i].time_end).appendTo($('#daypart_id'));
        }
        $('#daypart_id').select2();
      });

      //Update Date in dropdown for Long form
      var datedd = $.ajax({
        url: "/api/times/" + newdate,
        animation: "spinner",
        async: true
      });

      datedd.done(function (json) {
        var key;
        $('#air_time').empty();
        for (key in json) {
          if (json.hasOwnProperty(key)) {
            $("<option/>").attr("value", key).text(json[key]).appendTo($('#air_time'));
          }
        }
        //Reset Longform Form
        $('#program_id').select2("val", "");
        $('#airtime_id').val("");
        $('#new_show').val("");
        updateTimes();
        getLog();
      });
    });

    $('#channel_id').change(function(){
        getLog();
    });

    $('#refresh-log').click(function(){
        getLog();
    });

    $('#refresh-new').click(function(){
        newTable.draw();
    });

    $('#daypart-tgl').click(function(){
        $('#daypart-tbl').toggle();
    });

    $('#longform-tgl').click(function(){
        $('#longform-frm').toggle();
    });

    /* Removes Spot Link on the right side from click and list */
    $('#spotLink').on('click', '.removeItem', function(e){
        e.preventDefault();
        $(this).parent().parent('li').remove();
    });
    /* Hide All Previews */
    $('#hidePreview').click(function(e){
        e.preventDefault();
        $('#previewList').empty();
    });

    $('#newCk').click(function(){
        if($(this).is(':checked')) {
            $('#spot_id').select2("val","");
            $("#newFld").show();
            $('input[name=new_spot]').focus();  // checked
        } else {
            $("#newFld").hide();
            $('input[name=new_spot]').val("");
        }
    });

    $('#flgCk').click(function(){
        if($(this).is(':checked')) {
            $("#flagFld").show();  // checked
        } else {
            $("#flagFld").hide();
            $('input[name=flag_spot]').val("");
        }
    });

    $('#channel_id').select2({
        formatResult: channelFormat,
        formatSelection: channelFormat,
        escapeMarkup: function(m) { return m; }
    });

    $('#daypart_id').select2();

    /* AJAX Request to get Spots in Auto-Complete */
    $('#spot_version_id').select2({
        placeholder: "Search for a Spot Version",
        minimumInputLength: 2,
        ajax: {
            url: "/api/spot-versions/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 50,
                    language_id: $('#language_id').val()
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.spots};
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/spot-versions/one", {
                    data: {
                        spot_version_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: spotFormatResult,
        formatSelection: spotFormatSelection,
        escapeMarkup: function (m) { return m; }
    });

    /* Submit Handler */
    $('.spot-airtime-submit').click(function(e){
        e.preventDefault();

        var $submit = $(this);
        var $form = $(this).parent().parent('.spot-airtime-form');
        var $progress = $form.find('.progress');
        var $spot = $form.find('#spot_version_id');
        var $versionCheck = $('#verCk');
        var $newSpotCheck = $('#newCk');
        var $flagSpotCheck = $('#flgCk');
        var $newSpot = $('input[name=new_spot]');
        var $flagSpot = $('input[name=flag_spot]');

        if ($spot.val() === '' && !$versionCheck.is(':checked') && !$newSpotCheck.is(':checked') && !$flagSpotCheck.is(':checked')) {
            return;
        }

        $submit.attr("disabled", "disabled");
        $progress.show();

        var request = $.ajax({
            url: "/admin/spotairtime/store",
            type: "post",
            data: $form.serialize(),
            async: true
        });
        request.done(function (response, textStatus, jqXHR){
            //console.log(response);
            $progress.hide();
            //Select next time in dropdown
            $spot.select2("val","").focus();
            $newSpot.val('');
            $flagSpot.val('');

            if($versionCheck.is(':checked')){
                $versionCheck.attr('checked',false);
            }

            if($newSpotCheck.is(':checked')){
                $newSpotCheck.attr('checked',false);
                $("#newFld").hide();
            }

            if($flagSpotCheck.is(':checked')){
                $flagSpotCheck.attr('checked',false);
                $("#flagFld").hide();
            }

            $submit.removeAttr("disabled");
            getLog();
            newTable.draw();
        });
        // callback handler that will be called on failure
        request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console
            console.error(
                "The following error occured: "+
                textStatus, errorThrown
            );
            $submit.removeAttr("disabled");
            $progress.hide();
        });
    });

    /* Long Form Start */
    $('#program_version_id').select2({
        placeholder: "Search for a Program Version",
        minimumInputLength: 2,
        ajax: {
            url: "/api/program-versions/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page_limit: 50,
                    language_id: $('#language_id').val()
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to alter remote JSON data
                return {results: data.programs};
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/program-versions/one", {
                    data: {
                        program_version_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: programFormatResult,
        formatSelection: programFormatSelection,
        escapeMarkup: function (m) { return m; }
    });

    $('.airtime-submit').click(function(e){
        e.preventDefault();

        var $submit = $(this);
        var $form = $('.airtime-form');
        var $airdate = $form.find('.air-date-drop > option:selected');
        var $progress = $form.find('.progress');
        var $program_version_id = $('#program_version_id');
        var $language_id = $('#language_id');
        var $channel_id = $('#channel_id');
        var $airtime_id = $('#airtime_id');
        var $regular = $('#regular_programming');
        var $newshowCk = $('#newShowCk');
        var $newshow = $('#new_show');
        $submit.attr("disabled", "disabled");
        $progress.show();

        if ($program_version_id.val() == '' && $newshow.val() == '') {
            console.log('Program empty.');
            $progress.hide();
            $submit.removeAttr("disabled");
            return;
        }

        // console.log($program_id.val());
        // console.log($newShow.val());
        // console.log($airdate.val());
        // console.log($language_id.val());
        // console.log($channel_id.val());
        // console.log($regular.val());

        var airtime_request = $.ajax({
            url: "/admin/airtime/store",
            type: "POST",
            data: {
                "airtime_id": $airtime_id.val(),
                "program_version_id": $program_version_id.val(),
                "custom_program": $newshow.val(),
                "air_date": $airdate.val(),
                "language_id": $language_id.val(),
                "channel_id": $channel_id.val(),
                "regular_programming": $regular.val()
            }
        });
        airtime_request.done(function (response, textStatus, jqXHR){
            //console.log(response);
            $progress.hide();
            //Select next time in dropdown
            $airdate
                .prop("selected", false)
                .next()
                .prop("selected", true);
            $regular.prop("checked",false);
            $program_version_id.select2("val","").focus();
            $newshow.val('').focus();
            updateTimes();
            $('#newShowFld').hide();
            $submit.removeAttr("disabled");
        });
        // callback handler that will be called on failure
        airtime_request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console
            console.error(
                "The following error occured: "+
                textStatus, errorThrown
            );
            $submit.removeAttr("disabled");
            $progress.hide();
        });
    });

    $( '.air-date-drop' ).change(function(e){
        updateTimes();
    });

    $('#newShowCk').click(function(){
        if($(this).is(':checked')) {
            $('#program_id').select2("val","");
            $("#newShowFld").show();
            $('input[name=new_show]').focus();  // checked
        } else {
            $("#newFld").hide();
            $('input[name=new_show]').val("");
        }
    });

    //Trigger Change on First Load
    $( '.air-date-drop' ).trigger('change');

    newTable = $('#new-spots-tbl').dataTable({
        "pageLength": 10,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "/admin/spots",
            "data": {
                "language_id": $('#language_id').val()
            }
        },
        "columns": [
            {"data": "title", "name": "title", "title": "title"},
            {"data": "version", "name": "version", "title": "v"},
            {"data": "length", "name": "length", "title": "length"},
            {"data": "mp4", "name": "mp4", "title": "mp4"},
            {"data": "language.name", "name": "language.name", "title": "Language"},
            {"data": "channel", "name": "channel", "title": "Channel"},
            {"data": "air_date", "name": "air_date", "title": "air_date"},
            {"data": "user", "name": "user", "title": "User"}
        ],
        responsive: true
    });

    $('#inputLog').on('click', '.quickAdd', function(){
        $('#spot_version_id').select2("val",$(this).data('spot')).trigger("change");
    });

    /* Add a click handler for the delete row */
    $('#inputLog').on('click', '.deleteLog', function(e) {
        e.preventDefault();

        var airtime_id = $(this).data('delete');
        var msg  = '<i class="icon-warning-sign modal-icon"></i>&nbsp;Are you sure you want to&nbsp;';
        bootbox.dialog({
            "message": msg,
            "title": "Please Confirm",
            "buttons": {
                danger: {
                    label: "Confirm Delete",
                    className: "btn-danger",
                    callback: function() {
                        //Send AJAX Request to Server to Delete Row:
                        var request = $.ajax({
                        url: "/admin/spotairtime/delete",
                        type: "post",
                        data: {"airtime_id": airtime_id},
                        async: true

                        });
                        request.done(function (response, textStatus, jqXHR){
                            //console.log(response);
                            getLog();
                        });
                    }
                },
                close: {
                    label: "Cancel",
                    className: "btn"
                }
            }
        });
    });

    getLog();
});

function getLog() {
    //console.log('getLog called!');
    var $airdate = $('#air_date_changer');
    var $language_id = $('#language_id');
    var $channel_id = $('#channel_id');

    var input_log = $.ajax({
        url: "/admin/spotairtime/table",
        type: "GET",
        data: {
            "air_date": $airdate.val(),
            "language_id": $language_id.val(),
            "channel_id": $channel_id.val()
        }
    });
    input_log.done(function (response, textStatus, jqXHR){
        var $input = $('#inputLog');
        $input.empty();
        $input.fadeIn().html(response);
    });
    // callback handler that will be called on failure
    input_log.fail(function (jqXHR, textStatus, errorThrown){
        // log the error to the console
        console.error(
            "The following error occured: "+
            textStatus, errorThrown
        );
    });
}

function spotFormatResult(spot) {
    var markup = "<table class='company-result'><tr>";
    markup += "<td class='company-info'><div class='company-title'>" + spot.title + "</div>";
    markup += "</td></tr></table>";
    return markup;
}

function spotFormatSelection(spot) {
    $('#spotLink').show();
    $('#previewList').append('<li class="item"><a href="/spots/show/'+spot.id+'" target="_blank">'+spot.title+'<button class="close removeItem" data-dismiss="item">x</button></a></li>');
    return spot.title;
}

function programFormatResult(program) {
    var markup = "<table class='company-result'><tr>";
    markup += "<td class='company-info'><div class='company-title'>" + program.title + "</div>";
    markup += "</td></tr></table>";
    return markup;
}

function programFormatSelection(program) {
    return program.title;
}

function channelFormat(channel) {
    //console.log(channel);
    //return "<img class='channel' src='https://ims-logos.s3.amazonaws.com/channels/" + channel.id + "/logo.png' />" + channel.text;
    return channel.text;
}

function updateTimes() {
    //console.log('Update Times');
    var $form = $('.airtime-form');
    var $progress = $form.find('.progress');

    var $airtime_id = $('#airtime_id');
    var $program_version_id = $('#program_version_id');
    var $newShow = $('input[name=new_show]');
    var $channel_id = $('#channel_id');
    var $air_date = $('#air_time');
    var $regular = $('#regular_programming');

    var drop_request = $.ajax({
        url: "/admin/airtime/show",
        type: "get",
        data: {
            "_method": "get",
            "channel_id": $channel_id.val(),
            "air_date": $air_date.val()
        },
        async: true
    });
    drop_request.done(function (response, textStatus, jqXHR){
        //console.log(response);

        if(response.airtime !== null) {
            //console.log($program_id);
            $program_version_id.select2("val",response.program.id).trigger("change");
            $airtime_id.val(response.airtime.id);
            if(response.program.id == 11560){
                $regular.prop('checked',true);
            } else {
                $regular.prop('checked',false);
            }
        } else {
            $newShow.val('');
            $program_version_id.select2("val","").trigger("change");
            $airtime_id.val('');
            $regular.prop('checked',false);
        }

    });
    // callback handler that will be called on failure
    drop_request.fail(function (jqXHR, textStatus, errorThrown){
        // log the error to the console
        console.error(
            "The following error occured: "+
            textStatus, errorThrown
        );
    });
}
