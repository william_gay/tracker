var newTable;
var tempProgramSearchTerm = '';
var $customProgramInput = $('#custom_program_name');
$(function() {
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });

    $('#inputLog').on('click', '#confirmative', function (e){
        e.preventDefault();
        var $monitor_date = $('#air_date_changer');
        var $channel_id = $('#channel_id');

        var verify_log = $.ajax({
            url: "/admin/airtimes/verify",
            type: "POST",
            data: {
                "air_date": $monitor_date.val(),
                "channel_id": $channel_id.val()
            }
        });
        verify_log.done(function (response, textStatus, jqXHR){
            getLog();
        });
        // callback handler that will be called on failure
        verify_log.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console
            console.error(
                "Verify Airdate triggered error occured: "+
                textStatus, errorThrown
            );
        });

    });

    $('#set-date').click(function(e){
        e.preventDefault();
        var language_id = $('#language_id').val();
        var monitor_date = $('#air_date_changer').val();
        var type = 'long';

        var url = '/admin/monitor?rows=1';
        if (url.indexOf('?') > -1){
           url += '&language_id='+language_id+'&monitor_date='+monitor_date+'&type='+type;
        }else{
           url += '&language_id='+language_id+'&monitor_date='+monitor_date+'&type='+type;
        }
        window.location.href = url;
    });

    $('#channel_id').change(function(){
        getLog();
    });

    $('#refresh-log').click(function(e){
        e.preventDefault();
        getLog();
    });

    $('#refresh-new').click(function(){
        newTable.draw();
    });

    $( "#air_date_changer" ).datetimepicker({
        format: "YYYY-MM-DD"
    });

    /* Removes Spot Link on the right side from click and list */
    $('#programLink').on('click', '.removeItem', function(e){
        e.preventDefault();
        $(this).parent().parent('li').remove();
    });
    $('#hidePreview').click(function(e){
        e.preventDefault();
        $('#previewList').empty();
    });

    $('#newCk').click(function(){
        if($(this).is(':checked')) {
            $('#program_id').select2("val","");
            $("#newFld").show();
            $('input[name=new_show]').focus();  // checked
        } else {
            $("#newFld").hide();
            $('input[name=new_show]').val("");
        }
    });

    /* Long Form Start */
    $('#program_version_id').select2({
        placeholder: "Search for a Program Version",
        minimumInputLength: 2,
        quietMillis: 150,
        ajax: {
            url: "/api/program-versions/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                tempProgramSearchTerm = term;
                return {
                    q: term, // search term
                    page_limit: 50,
                    language_id: $('#language_id').val()
                };
            },
            results: function (data, page) {
                if (data.total === 0) {
                    $customProgramInput.val(tempProgramSearchTerm);
                    return {
                        results: [
                            {id: null, title: 'New Program: ' +tempProgramSearchTerm}
                        ]
                    };
                }
                return {
                    results: data.programs
                };
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/program-versions/one", {
                    data: {
                        program_version_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: function(program) {
            return program.title;
        },
        formatSelection: function(program) {
            if (program.id !== null) {
                $('#programLink').show();
                $('#previewList').append('<li class="item"><a href="/programs/show/'+program.id+'" target="_blank">'+program.title+'<button class="close removeItem" data-dismiss="item">x</button></a></li>');
            }
            return program.title;
        },
        escapeMarkup: function (m) { return m; }
    });

    $('.airtime-submit').click(function(e){
        e.preventDefault();

        var $submit = $(this);
        var $form = $('.program-airtime-form');
        var $airdate = $('#air_time > option:selected');
        var $progress = $form.find('.progress');
        var $program_version_id = $('#program_version_id');
        var $language_id = $('#language_id');
        var $channel_id = $('#channel_id');
        var $versionCheck = '';
        $submit.attr("disabled", "disabled");
        $progress.show();

        if ($('#verCk').is(':checked')) {
            $versionCheck = 'on';
        }

        if ($customProgramInput.val() === '' && $('#program_version_id').val() === ''){
            console.log('Program empty.');
            $progress.hide();
            return;
        }

        var airtime_request = $.ajax({
            url: "/admin/airtime/store",
            type: "POST",
            data: {
                "program_version_id": $program_version_id.val(),
                "new_version": $versionCheck,
                "custom_program": $customProgramInput.val(),
                "air_date": $airdate.val(),
                "language_id": $language_id.val(),
                "channel_id": $channel_id.val()
            }
        });
        airtime_request.done(function (response, textStatus, jqXHR){
            //console.log(response);
            $progress.hide();
            //Select next time in dropdown
            $airdate
                .prop("selected", false)
                .next()
                .prop("selected", true);
            $program_version_id.select2("val","").focus();

            if($('#verCk').is(':checked')){
                $('#verCk').attr('checked',false);
            }

            $submit.removeAttr("disabled");

            updateTimes();
            getLog();
            newTable.draw();
        });
        // callback handler that will be called on failure
        airtime_request.fail(function (jqXHR, textStatus, errorThrown){
            // log the error to the console
            console.error(
                "The following error occured: "+
                textStatus, errorThrown
            );
            $submit.removeAttr("disabled");

            $progress.hide();
        });
    });


  newTable = $('#new-shows-tbl').dataTable( {
    "pageLength": 10,
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": "/admin/programs",
        "data": {
            "language_id": $('#language_id').val(),
            "air_date": $('#air_date_changer').val()
        }
    },
    "columns": [
        {"data": "title", "name": "title", "title": "title"},
        {"data": "grid_title", "name": "grid_title", "title": "Grid"},
        {"data": "version", "name": "version", "title": "v"},
        {"data": "channel", "name": "channel", "title": "Channel"},
        {"data": "mp4", "name": "mp4", "title": "mp4"},
        {"data": "initial_date", "name": "initial_date", "title": "Air Date"},
        {"data": "user", "name": "user", "title": "User"},
        {"data": "id", "name": "id", "title": "Actions"}
    ],
    responsive: true,
    "drawCallback": function( settings ) {
        laravel.initialize();
    }
  });

    $('#channel_id').select2({
        formatResult: channelFormat,
        formatSelection: channelFormat,
        escapeMarkup: function(m) { return m; }
    });

    $('#inputLog').on('click', '.quickAdd', function(){
        $('#program_id').select2("val",$(this).data('program')).trigger("change");
    });

    /* Add a click handler for the delete row */
    $('#inputLog').on('click', '.deleteLog', function(e) {
        e.preventDefault();

        var airtime_id = $(this).data('delete');
        var msg  = '<i class="icon-warning-sign modal-icon"></i>&nbsp;Are you sure you want to&nbsp;';
        bootbox.dialog({
            "message": msg,
            "title": "Please Confirm",
            "buttons": {
                danger: {
                    label: "Confirm Delete",
                    className: "btn-danger",
                    callback: function() {
                        //Send AJAX Request to Server to Delete Row:
                        var request = $.ajax({
                        url: "/admin/airtime/delete",
                        type: "post",
                        data: {"airtime_id": airtime_id},
                        async: true

                        });
                        request.done(function (response, textStatus, jqXHR){
                            //console.log(response);
                            getLog();
                        });
                    }
                },
                close: {
                    label: "Cancel",
                    className: "btn"
                }
            }
        });
    });

    getLog();

});

function getLog() {
    //console.log('getLog called!');
    var $airdate = $('#air_time > option:selected');
    var $language_id = $('#language_id');
    var $channel_id = $('#channel_id');

    var input_log = $.ajax({
        url: "/admin/airtimes/table",
        type: "GET",
        data: {
            "air_date": $airdate.val(),
            "language_id": $language_id.val(),
            "channel_id": $channel_id.val()
        }
    });
    input_log.done(function (response, textStatus, jqXHR){
        var $input = $('#inputLog');
        $input.empty();
        $input.fadeIn().html(response);
    });
    // callback handler that will be called on failure
    input_log.fail(function (jqXHR, textStatus, errorThrown){
        // log the error to the console
        console.error(
            "The following error occured: "+
            textStatus, errorThrown
        );
    });
}

function channelFormat(channel) {
    //console.log(channel);
    //return "<img class='channel' src='https://ims-logos.s3.amazonaws.com/channels/" + channel.id + "/logo.png' />" + channel.text;
    return channel.text;
}

function updateTimes() {
    //console.log('Update Times');
    var $form = $('.airtime-form');
    var $progress = $form.find('.progress');

    var $airtime_id = $('#airtime_id');
    var $program_version_id = $('#program_version_id');
    var $newShow = $('input[name=new_show]');
    var $channel_id = $('#channel_id');
    var $air_date = $('#air_time');
    var $regular = $('#regular_programming');

    var drop_request = $.ajax({
        url: "/admin/airtime/show",
        type: "get",
        data: {
            "_method": "get",
            "channel_id": $channel_id.val(),
            "air_date": $air_date.val()
        },
        async: true
    });
    drop_request.done(function (response, textStatus, jqXHR){
        //console.log(response);

        if(response.airtime !== null) {
            //console.log($program_id);
            $program_version_id.select2("val",response.program.id).trigger("change");
            $airtime_id.val(response.airtime.id);
            if(response.program.id == 11560){
                $regular.prop('checked',true);
            } else {
                $regular.prop('checked',false);
            }
        } else {
            $newShow.val('');
            $program_version_id.select2("val","").trigger("change");
            $airtime_id.val('');
            $regular.prop('checked',false);
        }

    });
    // callback handler that will be called on failure
    drop_request.fail(function (jqXHR, textStatus, errorThrown){
        // log the error to the console
        console.error(
            "The following error occured: "+
            textStatus, errorThrown
        );
    });
}
