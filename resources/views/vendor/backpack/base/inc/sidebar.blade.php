@if (Sentry::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Sentry::getUser()->first_name, 0, 1) }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Sentry::getUser()->first_name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>

          <li><a href="{{ route('admin.monitor.index') }}"><i class="fa fa-rocket"></i> <span>DRTV Monitoring</span></a></li>
          <li><a href="{{ route('admin.lst.monitor.index') }}"><i class="fa fa-tv"></i> <span>LST Monitoring</span></a></li>

          <li class="header">Datas</li>
          <li><a href="{{ route('admin.categories.index') }}"><span>Categories</span></a></li>
          <li><a href="{{ route('admin.category-reports.index') }}"><span>Category Reports</span></a></li>
          <li><a href="{{ route('admin.channels.index') }}"><span>Channels</span></a></li>
          <li><a href="{{ route('admin.companies.index') }}"><span>Companies</span></a></li>
          <li><a href="{{ route('admin.dayparts.index') }}"><span>Day Parts</span></a></li>
          <li><a href="{{ route('admin.languages.index') }}"><span>Languages</span></a></li>
          <li><a href="{{ route('admin.products.index') }}"><span>Products</span></a></li>
          <li><a href="{{ route('admin.programs.index') }}"><span>Programs</span></a></li>
          <li><a href="{{ route('admin.reports.index') }}"><span>Reports</span></a></li>
          <li><a href="{{ route('admin.retailers.index') }}"><span>Retailers</span></a></li>
          <li><a href="{{ route('admin.retailer-products.index') }}"><span>Retailer Products</span></a></li>
          <li><a href="{{ route('admin.servers.index') }}"><span>Servers</span></a></li>
          <li><a href="{{ route('admin.spots.index') }}"><span>Spots</span></a></li>

          <li class="header">Live Shopping</li>
          <li><a href="{{ route('admin.lst.categories.index') }}"><span>Categories</span></a></li>
          <li><a href="{{ route('admin.lst.guides.index') }}"><span>Guides</span></a></li>
          <li><a href="{{ route('admin.lst.shows.index') }}"><span>Shows</span></a></li>
          <li><a href="{{ route('admin.lst.products.index') }}"><span>Products</span></a></li>
          <li><a href="{{ route('admin.lst.monitor.index') }}"><span>Monitor</span></a></li>
          <li><a href="{{ route('admin.lst.shows.reconcile') }}"><span>Reconsile</span></a></li>

          <li class="header">Reporting</li>
          <li><a href="{{ route('admin.email-campaigns.index') }}"><span>Email Campaigns</span></a></li>
          <li><a href="{{ route('admin.spots.new') }}"><span>New Spots</span></a></li>
          <li><a href="{{ route('admin.spots.unique') }}"><span>Unique Spots</span></a></li>
          <li><a href="{{ route('admin.spots.rankings') }}"><span>Spot Ranking</span></a></li>
          <li><a href="{{ route('admin.spotairtime.stats') }}"><span>Spot Detections</span></a></li>
          <li><a href="{{ route('admin.spots.pdf') }}"><span>Print Spots</span></a></li>
          <li><a href="{{ route('admin.reporting.alerts') }}"><span>Configured User Alerts</span></a></li>

          <li class="header">Auto-Detection</li>
          <li><a href="{{ route('admin.autodetection.network') }}"><span>Networks</span></a></li>
          <li><a href="{{ route('admin.autodetection.details') }}"><span>Details</span></a></li>
          <li><a href="{{ route('admin.airtimes.autodetection') }}"><span>Long-Form</span></a></li>
          <li><a href="{{ route('admin.airtimes.autodetection-breakdown') }}"><span>Long-From Breakdown</span></a></li>
          <li><a href="{{ route('admin.spotairtime.autodetection') }}"><span>Short-Form</span></a></li>
          <li><a href="{{ route('admin.spotairtime.autodetection-breakdown') }}"><span>Short-From Breakdown</span></a></li>

          <!-- ======================================= -->
          <li class="header">{{ trans('backpack::base.user') }}</li>
          <li><a href="{{ url('admin/users') }}"><i class="fa fa-user"></i> <span>Manage Users</span></a></li>
          <li><a href="{{ route('admin.groups.index') }}"><i class="fa fa-users"></i> <span>Groups</span></a></li>
          <li><a href="{{ route('admin.permissions.index') }}"><span>Permissions</span></a></li>

          <li><a href="{{ url('admin/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
