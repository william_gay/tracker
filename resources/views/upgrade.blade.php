@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-rocket"></i>
        Upgrade
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('upgrade') !!}
    <div class="row">
        <div class="col-xs-12">
            <h4>Upgrade Your Account</h4>
            <p>Need more data? Want more reports? Contact Sales to upgrade your account. <a href="mailto:sales@imsreport.com">sales@imsreport.com</a></p>  
        </div>
    </div>
@stop

@section('script')

@stop