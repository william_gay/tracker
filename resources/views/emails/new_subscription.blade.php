<h4>New Subscription Alert!</h4>

<ul>
    <li><strong>Name:</strong> {!! $user['first_name'].' '.$user['last_name'] !!}</li>
    <li><strong>E-mail:</strong> {!! $user['email'] !!}</li>
    <li><strong>Subscription:</strong> {!! $input['plan'] !!} </li>
</ul>
