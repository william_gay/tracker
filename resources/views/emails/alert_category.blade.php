<p>Hello {!! $user->first_name.' '.$user->last_name !!},</p>

<h4>Week Ending: {!! $week_ending->format("F j, Y") !!}</h4>

<p>{!! __('alerts.category_intro', array('category'=>$category->name)) !!}:</p>

@if($shows)
    <h5>Shows</h5>
    <ul>
    @foreach($shows as $show)
        @if($show->version == 1)
        <li>{!! Html::linkRoute('programs.show',$show->title, $show->id) !!} <small style="color:red;">New Product</small></li>
        @else
        <li>{!! Html::linkRoute('programs.show',$show->title, $show->id) !!}</li>
        @endif
    @endforeach
    </ul>
@else
    <p>No detected shows with {!! $category->name !!}</p>
@endif

@if($spots)
    <h5>Spots</h5>
    <ul>
    @foreach($spots as $spot)
        @if($spot->version == 1)
        <li>{!! Html::linkRoute('spots.show',$spot->title, $spot->id) !!} <small style="color:red;">New Product</small></li>
        @else
        <li>{!! Html::linkRoute('spots.show',$spot->title, $spot->id) !!}</li>
        @endif
    @endforeach
    </ul>
@else
    <p>No detected spots with {!! $category->name !!}</p>
@endif

<p style="font-size:10px;">{!! __('alerts.footer') !!}</p>
