Hello {!! $user->first_name.' '.$user->last_name !!},

Week Ending: {!! $week_ending->format("F j, Y") !!}

{!! __('alerts.category_intro', array('category'=>$category->name)) !!}:

@if($shows)
    Shows

    @foreach($shows as $show)
        @if($show->version == 1)
        {!! $show->title !!} *New Product*
        @else
        {!! $show->title !!}
        @endif
    @endforeach
@else
    No detected shows with {!! $category->name !!}
@endif

@if($spots)
    Spots

    @foreach($spots as $spot)
        @if($spot->version == 1)
        {!! $spot->title !!} *New Product*
        @else
        {!! $spot->title !!}
        @endif
    @endforeach
@else
    No detected spots with {!! $category->name !!}
@endif

{!! __('alerts.footer') !!}
