Hello {!! $user->first_name.' '.$user->last_name !!},

It seems you have requested a password reset. Please click this link to complete the reset: {!! link_to_route('user.reset.finish','Click here to reset.', array('userEmail' => $user->email, 'resetCode' =>$resetCode)) !!}

If you did not request a password reset please disregard this message. No further action is necessary.