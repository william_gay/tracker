Hello {!! $user->first_name.' '.$user->last_name !!},

Week Ending: {!! $week_ending->format("F j, Y") !!}

{!! __('alerts.spot_intro') !!}:


@foreach($spots as $spot)
    @if($spot->version == 1)
    	{!! $spot->title !!}
    	@if($spot->service == 0 and $spot->non_dr == 0)
    		*New DR Product Spot*
    	@elseif($spot->service == 1 and $spot->non_dr == 0)
    		*New DR Spot*
    	@else
    		*New Spot*
    	@endif
    @else
    	{!! $spot->title !!}
    @endif
@endforeach

{!! __('alerts.footer') !!}
