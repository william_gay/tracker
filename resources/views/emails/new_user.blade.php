<h4>New User Alert!</h4>

<ul>
    <li><strong>Name:</strong> {!! $user->first_name.' '.$user->last_name !!}</li>
    <li><strong>E-mail:</strong> {!! $user->email !!}</li>
    <li><strong>Phone:</strong> {!! $request['phone'] ?? 'N/A' !!}</li>
    <li><strong>Company:</strong> {!! $request['company'] ?? 'N/A' !!} </li>
    <li><strong>Position:</strong> {!! $request['position'] ?? 'N/A' !!} </li>
    <li><strong>How did you find out about us?:</strong> {!! $request['find_us'] ?? 'N/A' !!}</li>
    @if($product)
    <li><strong>Product: </strong> {!! $product->name !!}</li>
    @endif
</ul>
