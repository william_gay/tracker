@extends('emails.email-campaigns.layout')

@section('intro')
<div style="color: #222222;font-family: arial, sans-serif;line-height: normal;">
	<div style="color: #222222;font-family: arial, sans-serif;line-height: normal;text-align: center;">
		<span style="font-size:24px">Hello {!! $user['first_name'] ?? 'Friend' !!},</span>
	</div>
	<div style="color: #222222;font-family: arial, sans-serif;line-height: normal;">&nbsp;</div>
	<div style="color: #222222;font-family: arial, sans-serif;line-height: normal;text-align: center;">
		<span style="font-size:24px">See what shows and spots hit the airwaves last month!</span>
	</div>
</div>
@stop

@section('intro-text')
<div style="color: #222222;font-family: arial, sans-serif;line-height: normal;text-align: left;">
	<span style="color: #222222;font-family: arial,sans-serif;font-size: 18px;line-height: normal;text-align: center;">Media Analytics Rankings are based on total aggregate national network airings and media index, detected weekly for long form and short form in direct response.&nbsp;</span>
</div>
@stop

@section('content')
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">

                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                            <div style="text-align: left; background: #1b1b1b; padding: 10px; ">
                            	<span style="color:white; font-size:32px">
                            		<a href="{!! route('email-campaign.show', ['monthly-100-media-index-lf', 'hash' => $hash]) !!}" style="color:white;">
                            			Top 100 Long Form - Media Index
                            		</a>
                            	</span>
                            </div>

                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">

                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                            <div style="text-align: left; background: #1b1b1b; padding: 10px; ">
                            	<span style="color:white; font-size:32px">
                            		<a href="{!! route('email-campaign.show', ['monthly-100-frequency-lf', 'hash' => $hash]) !!}" style="color:white;">
                            			Top 100 Long Form - Frequency
                            		</a>
                            	</span>
                            </div>

                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock">
    <tbody class="mcnDividerBlockOuter">
        <tr>
            <td class="mcnDividerBlockInner" style="padding: 18px;">
                <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-top-width: 1px;border-top-style: solid;border-top-color: #FFFFFF;">
                    <tbody><tr>
                        <td>
                            <span></span>
                        </td>
                    </tr>
                </tbody></table>
            </td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">

                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                            <div style="text-align: left; background: #1b1b1b; padding: 10px; ">
                            	<span style="color:white; font-size:32px">
                            		<a href="{!! route('email-campaign.show', ['monthly-50-product-ranking-sf', 'hash' => $hash]) !!}" style="color:white;">
                            			Top 50 Short Form - Product
                            		</a>
                            	</span>
                            </div>

                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">

                <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                    <tbody><tr>

                        <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                            <div style="text-align: left; background: #1b1b1b; padding: 10px; ">
                            	<span style="color:white; font-size:32px">
                            		<a href="{!! route('email-campaign.show', ['monthly-50-direct-response-sf', 'hash' => $hash]) !!}" style="color:white;">
                            			Top 50 Short Form - Direct Response
                            		</a>
                            	</span>
                            </div>

                        </td>
                    </tr>
                </tbody></table>

            </td>
        </tr>
    </tbody>
</table>
@stop
