<p>Hello {!! $user->first_name.' '.$user->last_name !!},</p>

<h4>Week Ending: {!! $week_ending->format("F j, Y") !!}</h4>

<p>{!! __('alerts.spot_intro') !!}:</p>
<ul>
@foreach($spots as $spot)
    @if($spot->version == 1)
    <li>
    	{!! Html::linkRoute('spots.show',$spot->title, $spot->id) !!}
    	@if($spot->service == 0 and $spot->non_dr == 0)
    		<small style="color:red;">New DR Product Spot</small>
    	@elseif($spot->service == 1 and $spot->non_dr == 0)
    		<small style="color:red;">New DR Spot</small>
    	@else
    		<small style="color:red;">New Spot</small>
    	@endif
    </li>
    @else
    <li>
    	{!! Html::linkRoute('spots.show',$spot->title, $spot->id) !!}
    </li>
    @endif
@endforeach
</ul>

<p style="font-size:10px;">{!! __('alerts.footer') !!}</p>
