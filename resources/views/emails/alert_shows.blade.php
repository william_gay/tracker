<p>Hello {!! $user->first_name.' '.$user->last_name !!},</p>

<h4>Week Ending: {!! $week_ending->format("F j, Y") !!}</h4>

<p>{!! __('alerts.show_intro') !!}:</p>
<ul>
@foreach($shows as $show)
    @if($show->version == 1)
    <li>{!! Html::linkRoute('programs.show',$show->title, $show->id) !!} <small style="color:red;">New Show</small></li>
    @else
    <li>{!! Html::linkRoute('programs.show',$show->title, $show->id) !!}</li>
    @endif
@endforeach
</ul>

<p style="font-size:10px;">{!! __('alerts.footer') !!}</p>
