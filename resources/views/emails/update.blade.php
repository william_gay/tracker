@php
//get the first name
$first_name = $user->first_name;
$last_name = $user->last_name;
$email = $user->email;
$subject = 'Update Content Form';
$date_time = date("F j, Y, g:i a");
@endphp

<h1><?php echo ($first_name); ?> <?php echo ($last_name); ?> Is requesting an update to the information on this page  </h1>

<p>
Contact form submitted:

User: <?php echo ($first_name); ?> <?php echo ($last_name); ?> <br>
Email address: <?php echo ($email);?> <br>
Subject: <?php echo ($subject); ?><br>
Message: <?php echo ($body);?><br>
Page: <?php echo ($url); ?><br>

Date: <?php echo($date_time);?><br>
User IP address: <?php echo($userIpAddress);?><br>
</p>
