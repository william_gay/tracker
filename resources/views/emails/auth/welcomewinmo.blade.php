<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- NAME: SIMPLE TEXT -->
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Media Analytics &dash; Welcome to the Media Analytics' DRTV Tracker</title>

    <style type="text/css">
        p{
            margin:1em 0;
        }
        table{
            border-collapse:collapse;
        }
        img,a img{
            border:0;
            outline:none;
            text-decoration:none;
        }
        h1,h2,h3,h4,h5,h6{
            display:block;
            margin:0;
            padding:0;
        }
        body,#bodyTable,#bodyCell{
            height:100% !important;
            margin:0;
            padding:0;
            width:100% !important;
        }
        #outlook a{
            padding:0;
        }
        img{
            -ms-interpolation-mode:bicubic;
        }
        .ReadMsgBody{
            width:100%;
        }
        .ExternalClass{
            width:100%;
        }
        td,table{
            mso-table-lspace:0pt;
            mso-table-rspace:0pt;
        }
        p,a,li,td,body,table,blockquote{
            -ms-text-size-adjust:100%;
            -webkit-text-size-adjust:100%;
        }
        .ExternalClass,.ExternalClass p,.ExternalClass td,.ExternalClass div,.ExternalClass span,.ExternalClass font{
            line-height:100%;
        }
        a.mcnButton{
            display:block;
        }
        .mcnImage{
            vertical-align:bottom;
        }
        .mcnTextContent img{
            height:auto !important;
        }

        body,#bodyTable{
            background-color:#FFFFFF;
        }

        #bodyCell{
            border-top:0;
        }

        #templateContainer{
            border:0;
        }

        h1{
            color:#202020 !important;
            font-family:Helvetica;
            font-size:24px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:left;
        }

        h2{
            color:#202020 !important;
            font-family:Helvetica;
            font-size:20px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:-.75px;
            text-align:left;
        }

        h3{
            color:#202020 !important;
            font-family:Helvetica;
            font-size:18px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:left;
        }

        h4{
            color:#202020 !important;
            font-family:Helvetica;
            font-size:16px;
            font-style:normal;
            font-weight:bold;
            line-height:125%;
            letter-spacing:normal;
            text-align:left;
        }

        #templateHeader{
            border-bottom:0;
        }

        .headerContainer .mcnTextContent,.headerContainer .mcnTextContent p{
            color:#202020;
            font-family:Helvetica;
            font-size:15px;
            line-height:150%;
            text-align:left;
        }

        .headerContainer .mcnTextContent a{
            color:#202020;
            font-weight:normal;
            text-decoration:underline;
        }

        #templateBody{
            border-top:0;
            border-bottom:0;
        }

        .bodyContainer .mcnTextContent,.bodyContainer .mcnTextContent p{
            color:#202020;
            font-family:Helvetica;
            font-size:15px;
            line-height:150%;
            text-align:left;
        }

        .bodyContainer .mcnTextContent a{
            color:#202020;
            font-weight:normal;
            text-decoration:underline;
        }

        #templateFooter{
            border-top:0;
        }

        .footerContainer .mcnTextContent,.footerContainer .mcnTextContent p{
            color:#202020;
            font-family:Helvetica;
            font-size:11px;
            line-height:125%;
            text-align:left;
        }

        .footerContainer .mcnTextContent a{
            color:#202020;
            font-weight:normal;
            text-decoration:underline;
        }
        @media only screen and (max-width:480px){
            body{
                width:100% !important;
                min-width:100% !important;
            }

        }   @media only screen and (max-width:480px){
            td[class=footerContainer] a[class=utilityLink]{
                display:block !important;
            }

        }   @media only screen and (max-width:480px){
            td[class=mcnImageGroupContent]{
                padding:9px !important;
            }

        }   @media only screen and (max-width:480px){
            td[class=mcnImageCardBottomImageContent]{
                padding-bottom:9px !important;
            }

        }   @media only screen and (max-width:480px){
            table[class=mcpreview-image-uploader]{
                display:none !important;
                width:100% !important;
            }

        }   @media only screen and (max-width:480px){
            td[class=mcnImageGroupBlockInner]{
                padding-top:0 !important;
                padding-bottom:0 !important;
            }

        }   @media only screen and (max-width:480px){
            tbody[class=mcnImageGroupBlockOuter]{
                padding-top:9px !important;
                padding-bottom:9px !important;
            }

        }   @media only screen and (max-width:480px){
            td[class=mcnTextContent],td[class=mcnBoxedTextContentColumn]{
                padding-right:18px !important;
                padding-left:18px !important;
            }

        }   @media only screen and (max-width:480px){
            table[class=mcnCaptionLeftContentOuter] td[class=mcnTextContent],table[class=mcnCaptionRightContentOuter] td[class=mcnTextContent]{
                padding-top:9px !important;
            }

        }   @media only screen and (max-width:480px){
            td[class=mcnImageCardLeftImageContent],td[class=mcnImageCardRightImageContent]{
                padding-right:18px !important;
                padding-bottom:0 !important;
                padding-left:18px !important;
            }

        }   @media only screen and (max-width:480px){
            td[class=mcnImageCardTopImageContent],td[class=mcnCaptionBlockInner] table[class=mcnCaptionTopContent]:last-child td[class=mcnTextContent]{
                padding-top:18px !important;
            }

        }   @media only screen and (max-width:480px){
            img[class=mcnImage],table[class=mcnCaptionTopContent],table[class=mcnCaptionBottomContent],table[class=mcnTextContentContainer],table[class=mcnBoxedTextContentContainer],table[class=mcnImageGroupContentContainer],table[class=mcnCaptionLeftTextContentContainer],table[class=mcnCaptionRightTextContentContainer],table[class=mcnCaptionLeftImageContentContainer],table[class=mcnCaptionRightImageContentContainer],table[class=mcnImageCardLeftTextContentContainer],table[class=mcnImageCardRightTextContentContainer],table[class=socialContainer]{
                width:100% !important;
            }

        }   @media only screen and (max-width:480px){

            table[id=templateContainer],table[id=templatePreheader],table[id=templateHeader],table[id=templateBody],table[id=templateFooter]{
                max-width:600px !important;
                width:100% !important;
            }

        }   @media only screen and (max-width:480px){

            h1{
                font-size:24px !important;
                line-height:125% !important;
            }

        }   @media only screen and (max-width:480px){

            h2{
                font-size:20px !important;
                line-height:125% !important;
            }

        }   @media only screen and (max-width:480px){

            h3{
                font-size:18px !important;
                line-height:125% !important;
            }

        }   @media only screen and (max-width:480px){

            h4{
                font-size:16px !important;
                line-height:125% !important;
            }

        }   @media only screen and (max-width:480px){

            td[class=headerContainer] td[class=mcnTextContent],td[class=headerContainer] td[class=mcnTextContent] p{
                font-size:18px !important;
                line-height:125% !important;
            }

        }   @media only screen and (max-width:480px){

            td[class=bodyContainer] td[class=mcnTextContent],td[class=bodyContainer] td[class=mcnTextContent] p{
                font-size:18px !important;
                line-height:125% !important;
            }

        }   @media only screen and (max-width:480px){

            td[class=footerContainer] td[class=mcnTextContent],td[class=footerContainer] td[class=mcnTextContent] p{
                font-size:14px !important;
                line-height:115% !important;
            }

        }   @media only screen and (max-width:480px){

            table[class=mcnBoxedTextContentContainer] td[class=mcnTextContent],td[class=mcnBoxedTextContentContainer] td[class=mcnTextContent] p{
                font-size:18px !important;
                line-height:125% !important;
            }

        }</style></head>
<body>
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
        <tr>
            <td align="left" valign="top" id="bodyCell">
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                    <tr>
                        <td align="left" valign="top">
                            <!-- BEGIN HEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                <tr>
                                    <td valign="top" class="headerContainer" style="padding-top:9px; padding-bottom:9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                                        <tbody><tr>

                                                            <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                                                                <h1 style="text-align: right;"><img align="none" height="39" src="{{ config('app.url') }}/assets/img/logo.png" style="width: 150px; height: 39px; margin: 0px;" width="150"></h1>

                                                            </td>
                                                        </tr>
                                                        </tbody></table>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                            </table>
                            <!-- // END HEADER -->
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <!-- BEGIN BODY // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                <tr>
                                    <td valign="top" class="bodyContainer" style="padding-top:9px; padding-bottom:9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                                        <tbody><tr>

                                                            <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                                                                <h3>Hello {{ $user->first_name }}</h3>
                                                                <p>Thanks for your interest in our T.V. advertising Brand Tracker. We've received your free brand report request and will have it for you shortly.</p>
                                                                <p>If you would like to see a free demo of our full platform and capabilities please email us <a href="mailto:sales@imsreport.com">sales@imsreport.com</a> or call <a href="tel:1-610-200-6041">610-200-6041</a> to schedule. Media Analytics tracks TV ad campaigns for over 2000 brands everyday, in real time, across the TV landscape.</p>
                                                                <p>As a Thank You we have created a Free Subscription to our Monthly Top 25 listing.</p>
                                                                <p>To activate your account, <a href="{!! URL::to('user') !!}/{!! $user->id !!}/activate/{!! urlencode($activationCode) !!}">click here.</a></p>
                                                                <p>Or point your browser to this address: <br /> {!! URL::to('user') !!}/{!! $user->id !!}/activate/{!! urlencode($activationCode) !!}</p>
                                                                <br>
                                                                <br>
                                                                <strong><em>Don't wait! Stay in the know with the latest news and insights in Direct Response and Retail.</em></strong>&nbsp;<a href="http://www.imsreport.com/blog" target="_blank">Learn what's new at Media Analytics!</a>&nbsp;
                                                            </td>
                                                        </tr>
                                                        </tbody></table>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock">
                                            <tbody class="mcnFollowBlockOuter">
                                            <tr>
                                                <td align="center" valign="top" style="padding:9px" class="mcnFollowBlockInner">
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer">
                                                        <tbody><tr>
                                                            <td align="center" style="padding-left:9px;padding-right:9px;">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContent" style="border: 1px solid #EEEEEE;background-color: #FAFAFA;">
                                                                    <tbody><tr>
                                                                        <td align="center" valign="top" style="padding-top:9px; padding-right:9px; padding-left:9px;">
                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                <tbody><tr>
                                                                                    <td valign="top">


                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked">

                                                                                            <tbody><tr>
                                                                                                <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:5px;">
                                                                                                    <a href="https://www.linkedin.com/company/3059380" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/dark-linkedin-96.png" alt="LinkedIn" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                                                                </td>
                                                                                            </tr>


                                                                                            <tr>
                                                                                                <td align="center" valign="top" class="mcnFollowTextContent" style="padding-right:10px; padding-bottom:9px;">
                                                                                                    <a href="https://www.linkedin.com/company/3059380" target="_blank" style="color: #606060;font-family: Arial;font-size: 11px;font-weight: normal;line-height: 100%;text-align: center;text-decoration: none;">LinkedIn</a>
                                                                                                </td>
                                                                                            </tr>

                                                                                            </tbody></table>


                                                                                        <!--[if gte mso 6]>
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                        <![endif]-->


                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked">

                                                                                            <tbody><tr>
                                                                                                <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:5px;">
                                                                                                    <a href="https://twitter.com/IMSReport" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/dark-twitter-96.png" alt="Twitter" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                                                                </td>
                                                                                            </tr>


                                                                                            <tr>
                                                                                                <td align="center" valign="top" class="mcnFollowTextContent" style="padding-right:10px; padding-bottom:9px;">
                                                                                                    <a href="https://twitter.com/IMSReport" target="_blank" style="color: #606060;font-family: Arial;font-size: 11px;font-weight: normal;line-height: 100%;text-align: center;text-decoration: none;">Twitter</a>
                                                                                                </td>
                                                                                            </tr>

                                                                                            </tbody></table>


                                                                                        <!--[if gte mso 6]>
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                        <![endif]-->


                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked">

                                                                                            <tbody><tr>
                                                                                                <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:10px; padding-bottom:5px;">
                                                                                                    <a href="www.imsreport.com" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/dark-link-96.png" alt="Media Analytics" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                                                                </td>
                                                                                            </tr>


                                                                                            <tr>
                                                                                                <td align="center" valign="top" class="mcnFollowTextContent" style="padding-right:10px; padding-bottom:9px;">
                                                                                                    <a href="www.imsreport.com" target="_blank" style="color: #606060;font-family: Arial;font-size: 11px;font-weight: normal;line-height: 100%;text-align: center;text-decoration: none;">Media Analytics</a>
                                                                                                </td>
                                                                                            </tr>

                                                                                            </tbody></table>


                                                                                        <!--[if gte mso 6]>
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                        <![endif]-->


                                                                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="mcnFollowStacked">

                                                                                            <tbody><tr>
                                                                                                <td align="center" valign="top" class="mcnFollowIconContent" style="padding-right:0; padding-bottom:5px;">
                                                                                                    <a href="https://plus.google.com/u/1/101228582080841333508/" target="_blank"><img src="http://cdn-images.mailchimp.com/icons/social-block-v2/dark-googleplus-96.png" alt="Google Plus" class="mcnFollowBlockIcon" width="48" style="width:48px; max-width:48px; display:block;"></a>
                                                                                                </td>
                                                                                            </tr>


                                                                                            <tr>
                                                                                                <td align="center" valign="top" class="mcnFollowTextContent" style="padding-right:0; padding-bottom:9px;">
                                                                                                    <a href="https://plus.google.com/u/1/101228582080841333508/" target="_blank" style="color: #606060;font-family: Arial;font-size: 11px;font-weight: normal;line-height: 100%;text-align: center;text-decoration: none;">Google Plus</a>
                                                                                                </td>
                                                                                            </tr>

                                                                                            </tbody></table>


                                                                                        <!--[if gte mso 6]>
                                                                                        </td>
                                                                                        <td align="left" valign="top">
                                                                                        <![endif]-->

                                                                                    </td>
                                                                                </tr>
                                                                                </tbody></table>
                                                                        </td>
                                                                    </tr>
                                                                    </tbody></table>
                                                            </td>
                                                        </tr>
                                                        </tbody></table>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                            </table>
                            <!-- // END BODY -->
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <!-- BEGIN FOOTER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateFooter">
                                <tr>
                                    <td valign="top" class="footerContainer" style="padding-top:9px; padding-bottom:9px;"><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock">
                                            <tbody class="mcnTextBlockOuter">
                                            <tr>
                                                <td valign="top" class="mcnTextBlockInner">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="600" class="mcnTextContentContainer">
                                                        <tbody><tr>

                                                            <td valign="top" class="mcnTextContent" style="padding-top:9px; padding-right: 18px; padding-bottom: 9px; padding-left: 18px;">

                                                                <em>Copyright © {!! date('Y') !!} Media Analytics, All rights reserved.</em>
                                                                <br>
                                                                <br>
                                                                <br>
                                                                <strong>Our mailing address is:</strong>
                                                                <br>
                                                                16 West Chestnut Street<br>
                                                                West Chester, PA 19380
                                                                <br>
                                                                <br>
                                                                <a href="https://my.imsreport.com/user/alerts" class="utilityLink">unsubscribe from this list</a>
                                                                <br>
                                                                <br>
                                                            </td>
                                                        </tr>
                                                        </tbody></table>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                            </table>
                            <!-- // END FOOTER -->
                        </td>
                    </tr>
                </table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </table>
</center>
</body>
</html>
