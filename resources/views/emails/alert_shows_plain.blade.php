Hello {!! $user->first_name.' '.$user->last_name !!},

Week Ending: {!! $week_ending->format("F j, Y") !!}

{!! __('alerts.show_intro') !!}:


@foreach($shows as $show)
    @if($show->version == 1)
    {!! $show->title !!} *New Show*
    @else
    {!! $show->title !!}
    @endif
@endforeach

{!! __('alerts.footer') !!}
