@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-exclamation-triangle"></i>
        Error
    </h3>
@stop

@section('content')
    <div class="row">
    	<p>Something went wrong. {!! Html::linkRoute('dashboard','Click here') !!} to start over.</p>
    </div>
@stop

