@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-exclamation-triangle"></i>
        404 Error
    </h3>
@stop

@section('content')
    <div class="row">
    	<p>The page you are looking for cannot be found. {!! Html::linkRoute('dashboard','Click here') !!} to start over.</p>
    </div>
@stop

