@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-exclamation-triangle"></i>
        401 Error
    </h3>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
        	<p>Access denied. {!! Html::linkRoute('dashboard','Click here') !!} to start over.</p>
        	<p>Contact <a href="mailto:sales@imsreport.com">sales@imsreport.com</a> or fill in the form below to upgrade your account.</p>
        </div>
        <div class="col-sm-6">
            {!! Form:: open(array('action' => 'ContactController@postContact')) !!}

                <ul class="errors">
                    @foreach($errors->all('<li>:message</li>') as $message)
                    {!! $message !!}
                    @endforeach
                </ul>
                <div class="form-group col-sm-12">
                    {!! Form:: label ('message', 'Message*' )!!}
                    {!! Form:: textarea ('message', '', array('class' => 'form-control'))!!}
                </div>
                <div class="form-group col-sm-12 pull-right">
                    {!! Form::submit('Send', array('class' => 'btn btn-blue')) !!}
                </div>

            {!! Form:: close() !!}


        </div>
    </div>
@stop
