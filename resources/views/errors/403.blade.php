@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-exclamation-triangle"></i>
        403 Error
    </h3>
@stop

@section('content')
    <div class="row">
    	<p>You do not have permissions to the page you are trying to access. {!! Html::linkRoute('dashboard','Click here') !!} to start over.</p>
    	<p>Contact sales@imsreport.com to upgrade your account.</p>
    </div>
@stop

