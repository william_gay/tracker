<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-bookmark"></i>
        <a href="/admin/retailer-products">Retailer Products</a> @if(isset($retailer_products))({!! $retailer_products->count() !!})@endif
        <a href="{!! URL::route('admin.retailer-products.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Retailer Product">
            <i class="fa fa-plus"></i>
            New Retailer Product
        </a>
    </h3>
  </div>
</div>
