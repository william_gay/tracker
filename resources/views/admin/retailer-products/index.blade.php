@extends('backpack::layout')

@section('header')
    @include('admin.retailer-products.header')
@stop
@section('style')
<style>
.container {
    width: 99%;
}
</style>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12" style="padding-top:10px;">
            <table class="table table-striped table-bordered" id="retailer-products">
                <thead>
                    <tr>
                        <th class="col-md-1">Name</th>
                        <th class="col-md-1">Retailer</th>
                        <th>URL</th>
                        <th>Category</th>
                        <th class="col-md-1">Min Price</th>
                        <th class="col-md-1">Max Price</th>
                        <th class="col-md-1"><i class="icon-cog"></i></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($retailer_products as $product)
                    <tr>
                        <td>
                            <a href="{!! route("products.show", array($product->product_slug)) !!}" target="_blank">
                                {!! $product->product_name !!}
                            </a>
                        </td>
                        <td>{!! $product->retailer_name !!}</td>
                        <td>
                            <a href="{!! $product->retailer_website !!}" target="_blank">
                                {!! $product->retailer_website !!}
                            </a>
                        </td>
                        <td>{!! $product->category_name !!}</td>
                        <td>{!! number_format($product->min_price, 2) !!}</td>
                        <td>{!! number_format($product->max_price, 2) !!}</td>
                        <td>
                            <a href="{!! route("admin.retailer-products.edit", array($product->id)) !!}"
                                class="btn btn-default" target="_blank" rel="tooltip" title="Edit Retailer Product">
                                <i class="fa fa-pencil-square-o"></i>
                            </a>
                            <a href="{!! route("admin.retailer-products.delete", array($product->id)) !!}"
                                class="btn btn-danger deleteMe" target="_blank">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7">
                            {{ $retailer_products->links() }}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</div>
@stop

@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('.deleteMe').click(function(){
        $(this).closest('tr').remove();
    });
});

    var oTable;
    // $(document).ready(function() {

    //     oTable = $('#retailer-products').dataTable( {
    //         "pageLength": -1,
    //         "sDom": "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
    //
    //         "oLanguage": {
    //             "sLengthMenu": "_MENU_ records per page"
    //         },
    //         //"processing": true,
    //         //"serverSide": true,
    //         //"ajax": "{!! URL::to('admin/retailer-products/data') !!}",
    //         "oLanguage": {
    //             "sSearch": "Search all columns:"
    //         }
    //         // "fnServerData": function ( sSource, aoData, fnCallback ) {
    //         //     $.getJSON( sSource, aoData, function (json) {
    //         //         fnCallback(json)
    //         //         laravel.initialize();
    //         //     });
    //         // }
    //     });
    // });
</script>
@stop
