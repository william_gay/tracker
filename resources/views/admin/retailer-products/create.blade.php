@extends('backpack::layout')

@section('header')
    @include('admin.retailer-products.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Retailer Product</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.retailer-products.store' )) !!}
                    {!! Former::xlarge_text('product_id', 'Product')->class('col-md-6')->id('product_id')->required() !!}
                    {!! Former::xlarge_text('retailer_id', 'Retailer')->class('col-md-6')->id('retailer_id')->required() !!}
                    {!! Former::xlarge_text('min_price', 'Min Price')->required() !!}
                    {!! Former::xlarge_text('max_price', 'Max Price') !!}
                    {!! Former::xlarge_text('website','Website')->class('form-control website') !!}
                    {!! Former::textarea('general_configuration', __('retailer-products.general_configuration'))->rows(8)->class('col-md-10') !!}
                    {!! Former::textarea('attributes', __('retailer-products.attributes'))->rows(8)->class('col-md-10') !!}
                    <div class="form-group">
                        <label for="date_recorded" class="control-label col-lg-2 col-sm-4">
                            {!! __('retailer-products.date_recorded') !!}
                            <sup>*</sup>
                        </label>

                        <div class='input-group datetimepicker col-lg-4' data-date-format="YYYY-MM-DD HH:mm:ss">
                            <input type='text' class="form-control" name="date_recorded" />
                            <span class="input-group-addon"><span class="fa fa-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.retailer-products.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@stop
