@extends('backpack::layout')

@section('header')
    <h3>
        <i class="fa fa-bookmark"></i>
        <a href="/admin/retailer-products">Retailer Products Importer</a>
        <a href="{!! URL::route('admin.retailer-products.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Retailer Product">
            <i class="fa fa-plus"></i>
            New Retailer Product
        </a>
    </h3>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12" style="padding-top:10px;">
            {!! Former::horizontal_open( route('admin.retailer-products.processImport' ))->enctype('multipart/form-data') !!}
                <div class="form-group">
                        <label for="date_recorded" class="control-label col-lg-2 col-sm-4">
                            {!! __('retailer-products.date_recorded') !!}
                            <sup>*</sup>
                        </label>

                        <div class='input-group datetimepicker col-lg-4' data-date-format="YYYY-MM-DD HH:mm:ss">
                            <input type='text' class="form-control" name="date_recorded" />
                            <span class="input-group-addon"><span class="fa fa-calendar"></span>
                            </span>
                        </div>
                    </div>
                {!! Former::file('csv','CSV')->accept('file') !!}
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Import</button>
                    </div>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>
</div>
@stop

@section('script')

@stop
