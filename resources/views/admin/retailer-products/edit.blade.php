@extends('backpack::layout')

@section('header')
    @include('admin.retailer-products.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Edit Retailer Product {!! $retailer_product->name !!}</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.retailer-products.update', array($retailer_product->id)), 'PUT') !!}
                    {!! Former::hidden('product_id', $retailer_product->product_id) !!}
                    {!! Former::xlarge_text('product', 'Product', $retailer_product->product->name )->disabled()->class('col-md-6')->help('<a href="'. route("admin.products.edit", array($retailer_product->product_id)).'" target="_blank">Edit Product</a>') !!}
                    {!! Former::select('select_retailer_id', 'Retailer', $retailers, $retailer_product->retailer_id ) !!}
                    {!! Former::xlarge_text('min_price', 'Min Price', $retailer_product->min_price)->required() !!}
                    {!! Former::xlarge_text('max_price', 'Max Price', $retailer_product->max_price) !!}
                    {!! Former::xlarge_text('website','Website', $retailer_product->website)->class('form-control website') !!}
                    {!! Former::textarea('general_configuration', __('retailer-products.general_configuration'), $retailer_product->general_configuration)->rows(8)->class('col-md-10') !!}
                    {!! Former::textarea('attributes', __('retailer-products.attributes'), $retailer_product->attributes)->rows(8)->class('col-md-10') !!}
                    <div class="form-group">
                        <label for="date_recorded" class="control-label col-lg-2 col-sm-4">
                            {!! __('retailer-products.date_recorded') !!}
                            <sup>*</sup>
                        </label>
                        <div class='input-group datetimepicker col-lg-4' data-date-format="YYYY-MM-DD HH:mm:ss">
                            <input type='text' class="form-control" name="date_recorded" value="{!! $retailer_product->date_recorded !!}" />
                            <span class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.retailer-products.index') !!}" class="btn btn-large">Cancel</a>
                        <a href="{!! route("admin.retailer-products.destroy", array($retailer_product->id)) !!}"
                            data-method="delete" data-modal-text="Delete this Retailer Product?" class="btn btn-danger">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@stop
