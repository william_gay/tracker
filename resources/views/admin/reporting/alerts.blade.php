@extends('backpack::layout')

@section('header')
<h3>
	<i class="fa fa-rocket"></i>
	Configured Alerts
</h3>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
<div class="row">
	<div class="col-md-12">
		<h3>Users with configured alerts: </h3>
		<table class="table table-bordered table-hover alerts">
			<thead>
				<th>Name</th>
				<th>E-mail</th>
				<th>Alert</th>
				<th>Category</th>
				<th>Created At</th>
			</thead>
			<tbody>
			@foreach($alerts as $alert)
				<tr>
					<td>{!! $alert->user->first_name ?? 'N/A' !!} {!! $alert->user->last_name ?? '' !!}</td>
					<td>{!! $alert->user->email ?? 'N/A' !!}</td>
					<td>{!! $alert->type ?? 'N/A' !!}</td>
					<td>{!! $alert->category->name ?? 'N/A' !!}</td>
					<td>{!! $alert->created_at->toDateTimeString() !!}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
@stop

@section('script')
<script>
var oTable;
$(function(){
    oTable = $('.alerts').dataTable( {
        "pageLength": 100,
        "order": [[ 1, "desc" ]]
    });

});
</script>
@stop
