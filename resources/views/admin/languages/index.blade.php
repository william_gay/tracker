@extends('backpack::layout')

@section('header')
    @include('admin.languages.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Languages ({!! $languages->count() !!})</p>
                <div class="block-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Abbr</th>
                                <th>Modified</th>
                                <th><i class="icon-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($languages as $language)
                                <tr>
                                    <td>{!! $language->id !!}</td>
                                    <td>{!! $language->name !!}</td>
                                    <td>{!! $language->abbr !!}</td>
                                    <td>{!! date("m/d/Y  h:i a",strtotime($language->updated_at)) !!}</td>
                                    <td>
                                        <a href="{!! route('admin.languages.edit', array($language->id)) !!}" class="btn btn-default btn-sm">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <a href="{!! route('admin.languages.destroy', array($language->id)) !!}"
                                            data-method="delete"
                                            data-modal-text="Delete this Language?"
                                            class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>&nbsp;Delete Language
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5">{!! $languages->links() !!}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop
