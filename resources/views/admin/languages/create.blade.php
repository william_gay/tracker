@extends('backpack::layout')

@section('header')
    @include('admin.languages.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Language</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.languages.store' )) !!}
                    {!! Former::xlarge_text('name','Name')->required() !!}
                    {!! Former::xlarge_text('abbr','Abbr')->required() !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.languages.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
