<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-bookmark"></i>
        <a href="/admin/languages">Languages (Markets)</a>
        <a href="{!! URL::route('admin.languages.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Group">
            <i class="fa fa-plus"></i>
            New Language
        </a>
    </h3>
  </div>
</div>
