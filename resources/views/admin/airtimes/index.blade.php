@extends('backpack::layout')

@section('header')
<div class="box box-default">
    <div class="box-head box-border">
    @include('admin.airtimes.header')
    </div>
</div>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Airtimes ({!! $airtimes->count() !!})</p>
                <div class="block-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Air Date</th>
                                <th>Grid Title</th>
                                <th>Title</th>
                                <th>Created</th>
                                <th><i class="icon-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($airtimes as $airtime)
                                <tr>
                                    <td>{!! $airtime->id !!}</td>
                                    <td>{!! date("m/d/Y  h:i a",strtotime($airtime->air_date)) !!}</td>
                                    <td>{!! $airtime->program->grid_title !!}</td>
                                    <td>{!! $airtime->program->title !!}</td>
                                    <td>{!! date("m/d/Y  h:i a",strtotime($airtime->created_at)) !!}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
                                                Action
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{!! route('admin.airtimes.edit', array($airtime->id)) !!}">
                                                       <i class="fa fa-pencil-square-o"></i>&nbsp;Edit Airtime
                                                   </a>
                                                </li>
                                                <li>
                                                    <a href="{!! route('admin.airtimes.destroy', array($airtime->id)) !!}"
                                                       data-method="delete"
                                                       data-modal-text="Delete this Airtime?">
                                                       <i class="icon-trash"></i>&nbsp;Delete Airtime
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="8">{!! $airtimes->links() !!}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
