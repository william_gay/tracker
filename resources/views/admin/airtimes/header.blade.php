<h3>
    <i class="fa fa-bookmark"></i>
    Airtimes
    <a href="{!! URL::route('admin.airtimes.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Airtime">
        <i class="fa fa-plus"></i>
        New Airtime
    </a>
</h3>