@extends('admin.cpanel.layouts')

@section('header')
<div class="box box-default">
    <div class="box-head box-border">
    @include('admin.airtimes.header')
    </div>
</div>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Edit Airtime</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.airtimes.update', array($airtime->id)), 'PUT' ) !!}
                    {!! Former::select('channel_id','Channel')->options($channels,$airtime->channel_id) !!}
                    {!! Former::select('program_id','Program')->options($programs,$airtime->program_id) !!}
                    <div class="control-group">
                        <label for="rows" class="control-label">
                            Date
                        </label>
                        <div class="controls">
                            <input id="air-date" name="air_date" type="date=" value="{!! date("Y-m-d",strtotime($airtime->air_date)) !!}"> {!! Form::select('air_time',$times, date("H:i:s",strtotime($airtime->air_date))) !!}
                        </div>
                    </div>
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.airtimes.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
