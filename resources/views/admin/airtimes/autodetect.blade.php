@extends('backpack::layout')

@section('header')
<div class="box box-default">
    <div class="box-header with-border">
    <h3>
        <i class="fa fa-magic"></i>
        Long-Form Automagic
    </h3>
    </div>
</div>
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
</style>
@stop

@section('content')
<div class="box box-default">
    <div class="box-body">
<div class="row-fluid room">
    <div class="col-md-12" style="margin-top: 5px;">
    {!! Former::horizontal_open()->id('dateRange')->method('GET') !!}
         Start Date: <input class="input-xlarge" required="true" id="startDate" type="text" name="startDate" value="{!! $startDate->format('Y-m-d') !!}">
         End Date: <input class="input-xlarge" required="true" id="endDate" type="text" name="endDate" value="{!! $endDate->format('Y-m-d') !!}">
         <input class="btn-sm btn-primary btn" type="submit" value="Submit">
    {!! Former::close() !!}
    </div>
</div>
<div class="row-fluid room">
    <div class="col-md-12">
        <table class="table table-bordered table-striped">
            <thead>
                <th>Title</th>
                <th>Grid Title</th>
                <th>Date</th>
                <th>Channel</th>
                <th>Server</th>
            </thead>
            <tbody>
            @foreach($airtimes as $airtime)
            <tr>
                <td>{!! link_to_route('programs.show', $airtime->title, $airtime->program_version_id) !!}</td>
                <td>{!! $airtime->grid_title !!}</td>
                <th>{!! $airtime->air_date !!}</th>
                <td>{!! $airtime->channel_name !!}</td>
                <td>
                    <?php $airdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $airtime->air_date) ?>
                    <a href="{!! 'http://'.$airtime->server_name.'.monitor.imsreport.com/capture/'.$airtime->channel_abbr.'/'.$airdate->format('Y').'/'.$airdate->format('m').'/'.$airdate->format('d').'/'.$airtime->channel_abbr.'_'.$airdate->format('Y-m-d').'_'.$airdate->format('H.i').'_304p.mp4' !!}" target="_blank">{!! $airtime->server_name !!}</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
@stop

@section('script')
<script>
$(function(){
    //
});
</script>
@stop
