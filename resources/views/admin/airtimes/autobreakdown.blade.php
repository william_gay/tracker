@extends('backpack::layout')

@section('header')
<div class="box box-default">
    <div class="box-header with-border">
    <h3>
        <i class="fa fa-magic"></i>
        Long-Form Automagic Breakdown
    </h3>
    </div>
</div>
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
</style>
@stop

@section('content')
@section('header')
<div class="box with-border">
    <div class="box-body">
    <div class="row-fluid room">
    <div class="col-md-12" style="margin-top: 5px;">
    {!! Former::horizontal_open()->id('dateRange')->method('GET') !!}
         Start Date: <input class="input-xlarge" required="true" id="startDate" type="text" name="startDate" value="{!! $startDate->format('Y-m-d') !!}">
         End Date: <input class="input-xlarge" required="true" id="endDate" type="text" name="endDate" value="{!! $endDate->format('Y-m-d') !!}">
         <input class="btn-sm btn-primary btn" type="submit" value="Submit">
    {!! Former::close() !!}
    </div>
</div>
<div class="row-fluid room">
    <div class="col-md-12">
        <table class="table table-bordered table-striped">
            <thead>
                <th>Title</th>
                <th>Grid Title</th>
                <th>Version</th>
                <th>Market</th>
                <th>AutoDetect Filename</th>
                <th>Auto Count</th>
                <th>Total Count</th>
                <th>Auto Networks</th>
                <th>Total Networks</th>
            </thead>
            <tbody>
            @foreach($airtimes as $airtime)
            @if($airtime->auto_count == 0)
            <tr class="danger">
            @elseif($airtime->auto_count/$airtime->all_count < .75)
            <tr class="warning">
            @else
            <tr>
            @endif
                <td>{!! link_to_route('spots.show', $airtime->title, $airtime->id) !!}</td>
                <td>{!! $airtime->grid_title !!}</td>
                <td>{!! $airtime->version !!}</td>
                <td>
                @if($airtime->language)
                    {!! $airtime->language->name !!}
                @endif
                </td>
                <td>
                    <a href="/admin/airtimes/autodetection-program-breakdown/{!! $airtime->id !!}">Explore</a> ({!! $airtime->clipsters->count()!!})
                    <ul>
                    @foreach($airtime->clipsters as $clip)
                        <li>{!! $clip->filename !!}</li>
                    @endforeach
                    </ul>
                </td>
                <td>{!! $airtime->auto_count !!}</td>
                <td>{!! $airtime->all_count !!}</td>
                <td>{!! $airtime->auto_networks !!}</td>
                <td>{!! $airtime->total_networks !!}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
@stop

@section('script')
<script>
$(function(){
    //
});
</script>
@stop
