<div class="fluid">
    @if ($airtimes->count() - $airtimes->sum('verified') != 0)
        <h5 class="text-center"><i class="fa fa-times"></i> {!! $airtimes->count() - $airtimes->sum('verified') !!} un-confirmed detections. <a href="#" id="confirmative">Sign-Off</a></h5>
    @else
        <h5 class="text-center"><i class="fa fa-check"></i> Confirmative!</h5>
    @endif
</div>
<div class="fluid" style="height:25px">
    <span class="pull-left">{!! $airtimes->count() !!} Total {!! Lang::choice('spots.detections', $airtimes->count()) !!}.</span>
    @if($server)
    <span class="pull-right">
        {!! $channel->name !!} &bull;
        <a href="http://{!! $server->ip !!}" target="_blank">{!! $server->name !!}</a> &bull; <a href="http://{!! $server->ip !!}{{ $path }}" target="_blank">Local</a> &bull; <a href="http://{!! strtolower($server->name) !!}.monitor.imsreport.com{!! $path !!}" target="_blank">Remote</a>
    </span>
    @endif
</div>
@if($airtimes->count() > 0)
<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <th></th>
            <th>Title</th>
            <th>Grid Title</th>
            <th style="width: 25px;">v</th>
            <th>Notes</th>
            <th style="width: 40px;"></th>
            <th>User</th>
            <th class="col-md-1">Air Date</th>
            <th><i class="icon-cog"></i></th>
        </tr>
    </thead>
    <tbody>
    @foreach ($airtimes as $airtime)
        <tr>
            <td><a href="#" data-program="{!! $airtime->programVersion->id !!}" title="Quick Add" text="Quick Add Program?" class="btn btn-sm btn-default quickAdd"><i class="fa fa-plus"></i></a></td>
            <td><a href="{!! route("programs.show",array($airtime->programVersion->id)) !!}" target="_blank">{!! $airtime->programVersion->title !!}</a> - <a href="{!! route("admin.programs.edit",array($airtime->programVersion->id)) !!}" target="_blank">Edit</a></td>
            <td>{!! $airtime->programVersion->grid_title !!}</td>
            <td>{!! $airtime->programVersion->version !!}</td>
            <td>{!! $airtime->programVersion->remarks !!}</td>
            <td>
                @if($airtime->programVersion->clipsters->count() > 0)
                    <i class="fa fa-bolt" title="Auto Detection Enabled"></i> {!! link_to_route('admin.autodetection.details', $airtime->programVersion->clipsters->count(), ['filename' => $airtime->programVersion->clipsters->first()->filename], ['target' => '_blank']) !!}
                @endif
                @if($airtime->programVersion->program and $airtime->programVersion->program->product->count() > 0)
                    <br>
                        <i class="fa fa-money" title="Product Associated"></i>
                        {!! link_to_route('admin.products.edit', $airtime->programVersion->program->product->count(), ['id' => $airtime->programVersion->program->product->first()->id], ['target' => '_blank']) !!}
                    </a>
                @endif
            </td>
            <td>
            @if($airtime->user)
                {!! str_replace('@imsreport.com', "", $airtime->user->email) !!}
            @else
                @if($airtime->url)
                    <a href="http://{!! $airtime->url!!}" target="_blank">Log</a>
                @endif
            @endif
            </td>
            <td>{!! str_replace($air_date->format('Y-m-d'), '', $airtime->air_date) !!}</td>
            <td><a href="#" data-delete="{!! $airtime->id !!}" text="Delete this Airtime?" class="btn btn-sm btn-danger deleteLog"><i class="fa fa-trash-o"></i></a></td>
        </tr>
    @endforeach
    </tbody>
</table>
@else
    <div class="alert alert-info">No airings entered yet!</div>
@endif
