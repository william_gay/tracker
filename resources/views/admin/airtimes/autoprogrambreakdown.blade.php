@extends('backpack::layout')

@section('header')
<div class="box box-default">
    <div class="box-head box-border">
    <h3>
        <i class="fa fa-magic"></i>
        Long-Form Automagic Breakdown &bull; {!! $programVersion->title !!}
    </h3>
    </div>
</div>
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
</style>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
<div class="row-fluid room">
    <div class="col-md-12" style="margin-top: 5px;">
    {!! Former::horizontal_open()->id('dateRange')->method('GET') !!}
         Start Date: <input class="input-xlarge" required="true" id="startDate" type="text" name="startDate" value="{!! $startDate->format('Y-m-d') !!}">
         End Date: <input class="input-xlarge" required="true" id="endDate" type="text" name="endDate" value="{!! $endDate->format('Y-m-d') !!}">
         <input class="btn-sm btn-primary btn" type="submit" value="Submit">
    {!! Former::close() !!}
    </div>
</div>
<div class="row-fluid room">
    <div class="col-md-12">
        <table class="table table-bordered table-striped">
            <thead>
                <th>Network</th>
                <th>Air Date</th>
                <th>User</th>
                <th>Log</th>
            </thead>
            <tbody>
            @foreach($airtimes as $airtime)
                @if(!is_null($airtime->email))
                    <tr class="danger">
                @else
                    <tr>
                @endif
                <td>{!! $airtime->channel->name ?? 'N/A' !!}</td>
                <td>{!! $airtime->air_date !!}</td>
                <td>{!! $airtime->user->email ?? 'Auto-Detected' !!}</td>
                <td>
                @if( $airtime->url)
                    <a href="http://{!! $airtime->url !!}" target="_blank">{!! $airtime->channel->server->name !!}</a> - <a href='http://{!! $airtime->channel->server->name !!}.monitor.imsreport.com/?directory={!!urlencode('/capture/'.$airtime->channel->abbr.'/'.$airtime->air_dateable->format('Y').'/'.$airtime->air_dateable->format('m').'/'.$airtime->air_dateable->format('d')) !!}' target="_blank">Check</a>
                @elseif($airtime->channel and $airtime->channel->server)
                    {!! $airtime->channel->server->name !!} - <a href="http://{!! $airtime->channel->server->name !!}.monitor.imsreport.com/capture/{!! $airtime->channel->abbr !!}/{!! $airtime->air_dateable->format('Y') !!}/{!! $airtime->air_dateable->format('m') !!}/{!! $airtime->air_dateable->format('d') !!}/{!! $airtime->channel_abbr !!}_{!! $airtime->air_dateable->format('Y-m-d_H.i') !!}.txt" target="_blank">Check</a>
                @else
                    N/A
                @endif
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
@stop

@section('script')
<script>
$(function(){
    //
});
</script>
@stop
