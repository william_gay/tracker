@extends('backpack::layout')

@section('header')
    @include('admin.spots.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Printed Spot Reports</p>
                <div class="block-body">
                    {!! Former::horizontal_open( route('admin.spots.pdf'), 'GET' ) !!}
                        {!! Former::select('report_type', 'Report')->options(array(0=>'-- Select One --', 'top-50' => 'Top 50', 'top-100' => 'Top 100', 'new-spots' => 'New Spots', 'top-5-category' => 'Top 5 Category')) !!}
                        {!! Former::xlarge_text('month', 'Month')->help('This displays the month on the top 100 report') !!}
                        {!! Former::xlarge_text('startDay', 'Start Day')->id('startDay')->help('Only use this for top 100 report') !!}
                        {!! Former::xlarge_text('weekEnding', 'Week Ending')->id('weekEnding') !!}
                        {!! Former::xlarge_text('page_number', 'Page Number') !!}
                        {!! Former::actions()
                            ->large_primary_submit('Submit')
                            ->large_inverse_reset('Reset') !!}
                    {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('script')
<script>
$(function() {
    //
});
</script>
@stop
