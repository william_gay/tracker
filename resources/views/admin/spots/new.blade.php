@extends('backpack::layout')

@section('header')
    @include('admin.spots.header-reporting')
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
</style>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
<div class="row room">
    <div class="col-md-12" style="margin-top: 5px;">
    {!! Former::horizontal_open()->id('dateRange')->method('GET') !!}
         Start Date: <input class="input-xlarge" required="true" id="startDate" type="text" name="startDate" value="{!! $startDate->format('Y-m-d') !!}">
         End Date: <input class="input-xlarge" required="true" id="endDate" type="text" name="endDate" value="{!! $endDate->format('Y-m-d') !!}">
         {!! Form::select('language_id', $languages, $language_id) !!}
         <input class="btn-xs btn-primary btn" type="submit" value="Submit">
    {!! Former::close() !!}
    </div>
</div>
<div class="row-fluid room">
    <div class="col-md-12">
        <h4>Total Spots: {!! $spots->count() !!}</h4>
        <table class="table table-bordered table-striped">
            <thead>
                <th>ID</th>
                <th>Title</th>
                <th>V</th>
                <th>Service</th>
                <th>DR</th>
                <th>Brand</th>
                <th>Date</th>
                <th></th>
                <th>Description</th>
                <th>Category</th>
                <th>Channel</th>
                <th>Co.</th>
                <th>Price</th>
                <th>S/H</th>
                <th>Website</th>
                <th>Phone</th>
                <th>Notes</th>
                <th>Remarks</th>
                <th>Video</th>
            </thead>
            <tbody>
            @foreach($spots as $spot)
            <tr>
                <td>{!! link_to_route('spots.show', $spot->id ,$spot->id) !!}</td>
                <td>
                    {!! $spot->title !!}
                     <small><a href="{!! route("admin.spots.edit",array($spot->id)) !!}" target="_blank">Edit</a></small>
                </td>
                <td>{!! $spot->version !!}</td>
                <td>{!! $spot->service == 1 ? 'Service' : 'Not Service' !!}</td>
                <td>{!! $spot->nondr == 1 ? 'Non DR' : 'DR' !!}</td>
                <td>{!! $spot->brand_advert == 1 ? 'Brand' : 'Not Brand' !!}</td>
                <th>{!! $spot->initial_date !!}</th>
                <td>:{!! $spot->length !!}</td>
                <td>{!! $spot->description !!}</td>
                <td>
                @if($spot->category and $spot->subCategory)
                    {!! $spot->category->name !!}, {!! $spot->subCategory->name !!}
                @endif
                </td>
                <td>
                @if($spot->channel)
                    {!! $spot->channel->name !!}
                @endif
                </td>
                <td>
                @if($spot->marketingCompany)
                    {!! $spot->marketingCompany->name !!}
                @endif
                </td>
                <td>${!! $spot->price !!}</td>
                <td>${!! $spot->shipping_cost !!}</td>
                <td>{!! $spot->website !!}</td>
                <td>{!! $spot->phone !!}</td>
                <td>{!! $spot->notes !!}</td>
                <td>{!! $spot->remarks !!}</td>
                <td>{!! $spot->mp4 == 1 ? 'Video' : 'No Video'  !!}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
@stop

@section('script')
<script>
$(function(){
    //
});
</script>
@stop
