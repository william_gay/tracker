@extends('backpack::layout')

@section('header')
    @include('admin.spots.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Spot</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.spots.store'), 'POST') !!}
                    {!! Former::xlarge_text('title','Title')->required() !!}
                    {!! Former::xlarge_text('version','Version')->required() !!}
                    {!! Former::select('language_id','Language')->options($languages)->id('language_id')->help('Change Language for Auto-complete Master Spot')->required() !!}
                    {!! Former::xlarge_text('spot_id','Master Spot')->class('col-md-6')->help('Leave blank for new spot.') !!}
                    {!! Former::select('category_id','Category')->options(array(0=>'-- Select One --') + $categories)->id('category_id')->required() !!}
                    {!! Former::select('sub_category_id','Sub Category')->options(array(0=>'-- Select One --'))->id('sub_category_id')->required() !!}
                    {!! Former::checkbox('service','Service')->text('Check this box to exclude this spot from the product retail ranking.') !!}
                    {!! Former::checkbox('non_dr','Non DR')->text('Check this box to exclude this spot from the product retail ranking and the DR Ranking. This is a per-version setting since some products originate in DR and switch to Non-DR.') !!}
                    {!! Former::checkbox('brand_advert','Brand Advert')->text('Check this box if for Brand Advertising, excludes from ranking and new Spots.') !!}
                    {!! Former::select('channel_id', 'Initial Channel')->options(array(0=>'-- Select One --') + $channels)->id('channel_id')->class('col-md-3') !!}
                    <div class="form-group">
                        <label for="marketing_company_id" class="control-label col-lg-2 col-sm-4">Marketing Company</label>
                        <div class="col-sm-10">
                            <input id="marketing_company_id" type="hidden" name="marketing_company_id" style="width: 250px;">
                            <label class="checkbox" style="margin-left: 10px;">
                                <input type="checkbox" name="new_company_ck" id="newCo"> New Company
                            </label>
                            <input type="text" name="new_company" class="col-md-5" placeholder="Enter New Company Name Here" />
                        </div>
                    </div>
                    {!! Former::xlarge_text('length', 'Length')->help('Time in seconds. IE: 60') !!}
                    {!! Former::textarea('description', 'Description')->rows(5)->class('col-md-10') !!}
                    {!! Former::xlarge_text('currency', 'Currency', '$')->required() !!}
                    {!! Former::xlarge_text('price', __('spots.price'))->help('Total Price on website') !!}
                    {!! Former::xlarge_text('price_extra', __('spots.price_extra'))->help('Original price when spot detected.') !!}
                    {!! Former::xlarge_text('shipping_cost', __('spots.shipping'))->help('Shipping price') !!}
                    {!! Former::textarea('offer', __('spots.offer'))->rows(5)->class('col-md-8')->help('Offer described, IE: 4 Payments of $24.99') !!}
                    {!! Former::xlarge_text('website', 'Website') !!}
                    {!! Former::xlarge_text('phone', 'Phone') !!}
                    {!! Former::textarea('notes', 'Notes')->rows(5)->class('col-md-10') !!}
                    {!! Former::xlarge_text('air_date', 'Air Date')->id('air_date_frm') !!}
                    {!! Former::xlarge_text('initial_date', 'Initial Date')->id('initial_date') !!}
                    {!! Former::textarea('remarks', 'Remarks')->rows(5)->class('col-md-10') !!}
                    {{-- Former::xlarge_text('file_name', 'File Name') --}}
                    {!! Former::checkbox('flv','FLV')->text('FLV Exists?') !!}
                    {!! Former::checkbox('mp4','MP4')->text('MP4 Exists?') !!}
                    {!! Former::checkbox('poster','Poster')->text('Poster Exists?') !!}
                    {!! Former::textarea('keywords', 'Keywords') !!}
                    {!! Former::xlarge_text('cs_phone', 'Customer Service Phone') !!}
                    {!! Former::xlarge_text('page_number', 'Page Number') !!}
                    {!! Former::xlarge_text('odd_ball', 'Odd Ball') !!}
                    {!! Former::xlarge_text('corporate', 'Corporate') !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.spots.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop
