<div class="box box-default">
  <div class="box-header with-border">
<h3>
    <i class="fa fa-bookmark"></i>
    <a href="/admin/spots">Spots</a>
    <a href="{!! URL::route('admin.spots.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Spot">
        <i class="fa fa-plus"></i>
        New Spot
    </a>
</h3>
</div>
</div>
