@extends('backpack::layout')

@section('header')
    @include('admin.spots.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Edit Spot
                    <span class="pull-right" style="margin-right: 10px;">Last Updated By: {!! $spot->user->email ?? 'N/A' !!}</span>
                </p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.spots.update', array($spot->id)), 'PUT' ) !!}
                    {!! Former::xlarge_text('title','Title',$spot->title)->required() !!}
                    {!! Former::xlarge_text('version','Version',$spot->version)->required() !!}
                    {!! Former::select('language_id','Language')->options($languages, $spot->language_id)->id('language_id')->help('Change Language for Auto-complete Master Spot')->required() !!}
                    {!! Former::xlarge_text('spot_id','Master Spot',$spot->spot_id)->class('col-md-6') !!}
                    {!! Former::select('category_id','Category')->options(array(0=>'-- Select One --') + $categories,$spot->category_id)->id('category_id')->required() !!}
                    {!! Former::select('sub_category_id','Sub Category')->options(array(0=>'-- Select One --') + $subcats,$spot->sub_category_id)->id('sub_category_id')->required() !!}
                    {!! Former::select('channel_id', 'Initial Channel')->options(array(0=>'-- Select One --') + $channels, $spot->channel_id)->id('channel_id')->class('col-md-3') !!}
                    {!! Former::checkbox('service','Service')->check(array('service' => $spot->service))->text('Check this box to exclude this spot from the Retail Product ranking.') !!}
                    {!! Former::checkbox('non_dr','Non DR')->check(array('non_dr' => $spot->non_dr))->text('Check this box to exclude this spot from the product retail ranking and the DR Ranking. This is a per-version setting since some products originate in DR and switch to Non-DR.') !!}
                    {!! Former::checkbox('brand_advert','Brand Advert')->check(array('brand_advert' => $spot->brand_advert))->text('Check this box if for Brand Advertising, excludes from ranking and new Spots.') !!}
                    <div class="form-group">
                        <label for="marketing_company_id" class="control-label col-lg-2 col-sm-4">{!! __('programs.marketing_company') !!}</label>
                        <div class="col-sm-10">
                            <input id="marketing_company_id" type="hidden" name="marketing_company_id" style="width: 250px;" value="{!! $spot->marketing_company_id !!}">
                            <label class="checkbox" style="margin-left: 10px;">
                                <input type="checkbox" name="new_company_ck" id="new_company_ck" id="newCo"> New Company
                            </label>
                            <input type="text" name="new_company" id="new_company" disabled="disabled" required="required" class="col-md-5" placeholder="Enter New Company Name Here" />
                        </div>
                    </div>
                    {!! Former::xlarge_text('length', 'Length',$spot->length) !!}
                    {!! Former::textarea('description', 'Description',$spot->description)->rows(5)->class('col-md-10') !!}
                    {!! Former::xlarge_text('currency', 'Currency', $spot->currency)->required() !!}
                    {!! Former::xlarge_text('price', __('spots.price'),$spot->price)->help('Total Price on website') !!}
                    {!! Former::xlarge_text('price_extra', __('spots.price_extra'),$spot->price_extra)->help('Original price when spot detected.') !!}
                    {!! Former::xlarge_text('shipping_cost', __('spots.shipping'),$spot->shipping_cost)->help('Shipping price') !!}
                    {!! Former::textarea('offer', __('spots.offer'), $spot->offer)->rows(5)->class('col-md-8')->help('Offer described, IE: 4 Payments of $24.99') !!}
                    {!! Former::xlarge_text('website', 'Website',$spot->website) !!}
                    {!! Former::xlarge_text('phone', 'Phone',$spot->phone) !!}
                    {!! Former::textarea('notes', 'Notes',$spot->notes)->rows(5)->class('col-md-10') !!}
                    {!! Former::xlarge_text('air_date', 'Air Date',$spot->air_date)->id('air_date_frm') !!}
                    {!! Former::xlarge_text('initial_date', 'Initial Date', $spot->initial_date)->id('initial_date') !!}
                    {!! Former::textarea('remarks', 'Remarks',$spot->remarks)->rows(5)->class('col-md-10') !!}
                    {!! Former::select('file_name','File Name')->options(array(0=>'-- Select One --') + $videos,$spot->file_name)->id('file_name')->class('col-md-6') !!}
                    {!! Former::checkbox('flv','FLV')->check(array('flv'=>$spot->flv))->text('FLV Exists?') !!}
                    {!! Former::checkbox('mp4','MP4')->check(array('mp4'=>$spot->mp4))->text('MP4 Exists?') !!}
                    {!! Former::checkbox('poster','Poster')->check(array('poster'=>$spot->poster))->text('Poster Exists?') !!}
                    {!! Former::textarea('keywords', 'Keywords',$spot->keywords) !!}
                    {!! Former::xlarge_text('cs_phone', 'Customer Service Phone',$spot->cs_phone) !!}
                    {!! Former::xlarge_text('page_number', 'Page Number',$spot->page_number) !!}
                    {!! Former::xlarge_text('odd_ball', 'Odd Ball',$spot->odd_ball) !!}
                    {!! Former::xlarge_text('corporate', 'Corporate',$spot->corporate) !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.spots.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
            </div>
            </div>
        </div>
    </div>
    </div>
</div>
@stop

@section('after_scripts')
<script>
$(function() {
    $('#new_company_ck').click(function () {
        if ($(this).is(':checked')) {
            $('#new_company').removeAttr('disabled');
        } else {
            $('#new_company').attr('disabled', 'disabled');
        }
    });
});
</script>
@endsection
