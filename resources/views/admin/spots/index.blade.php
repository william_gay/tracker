@extends('backpack::layout')

@section('header')
    @include('admin.spots.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row-fluid">
        <div class="col-md-12">
            <div class="block">
                <div class="block-body">
                    {!! $html->table() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('script')
{!! $html->scripts() !!}
@stop
