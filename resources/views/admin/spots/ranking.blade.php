@extends('backpack::layout')

@section('header')
<h3>
    @include('admin.spots.header-reporting')
</h3>
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
input.RowCount {
    width: 30px;
}
input.Detections {
    width: 30px;
}
input.V {
    width: 30px;
}
input.Service {
    width: 40px;
}
input.DR {
    width: 30px;
}
input.Brand {
    width: 30px;
}
input.Length {
    width: 30px;
}
</style>
@stop

@section('content')
<div class="box box-default">
    <div class="box-body">
        <div class="row room">
            <div class="col-md-12" style="margin-top: 5px;">
            {!! Former::horizontal_open()->id('dateRange')->method('GET') !!}
                 Start Date: <input class="input-xlarge" required="true" id="startDate" type="text" name="startDate" value="{!! $startDate->format('Y-m-d') !!}">
                 End Date: <input class="input-xlarge" required="true" id="endDate" type="text" name="endDate" value="{!! $endDate->format('Y-m-d') !!}">
                 {!! Form::select('language_id', $languages, $language_id) !!}
                 <input class="btn-xs btn-primary btn" type="submit" value="Submit">
            {!! Former::close() !!}
            </div>
        </div>
        <div class="row-fluid room">
            <div class="col-md-12">
                <h4>Total Spots: {!! $spots->count() !!}</h4>
                <table class="table table-bordered table-striped" id="rankings">
                    <thead>
                        <tr>
                            <th>Row Count</th>
                            <th>Detections</th>
                            <th>Title</th>
                            <th>V</th>
                            <th>Service</th>
                            <th>DR</th>
                            <th>Brand</th>
                            <th>Length</th>
                            <th>Category</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Row Count</th>
                            <th>Detections</th>
                            <th>Title</th>
                            <th>V</th>
                            <th>Service</th>
                            <th>DR</th>
                            <th>Brand</th>
                            <th>Length</th>
                            <th>Category</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach($spots as $spot)
                        <tr>
                            <td></td>
                            <td>{!! $spot->detections !!}</td>
                            <td>
                                {!! $spot->title !!}
                                 <small><a href="{!! route("admin.spots.edit",array($spot->id)) !!}" target="_blank">Edit</a></small>
                            </td>
                            <td>{!! $spot->version !!}</td>
                            <td>{!! $spot->service == 1 ? 'Service' : 'Not' !!}</td>
                            <td>{!! $spot->nondr == 1 ? 'Not' : 'DR' !!}</td>
                            <td>{!! $spot->brand_advert ? 'Brand': 'Not' !!}</td>
                            <td>:{!! $spot->length !!}</td>
                            <td>
                                {!! $spot->category !!}, {!! $spot->sub_category !!}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('script')
<script>
var oTable;
$(document).ready(function() {

  $('#rankings tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" class="'+title.replace(/\s/g,'')+'" />' );
    });


  oTable = $('#rankings').DataTable({
        "columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": [[ 1, 'asc' ]]
    });
  oTable.columns().every( function () {
        var that = this;

        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
  oTable.on( 'order.dt search.dt', function () {
        oTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

  //Refresh the table
  // setInterval(function () {
  //   $("input[name='search_service'").val('Not').keyup();
  // }, 1000);
});
</script>
@stop
