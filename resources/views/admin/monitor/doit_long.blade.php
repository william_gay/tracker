@extends('backpack::layout')

@section('style')
<style>
.container {
    width: 99%;
}
form.airtime-form {
    margin-bottom: 0px;
}
img.channel {
    height: 15px;
    padding-right: 10px;
}
tr.row_selected {
    background-color: #ddd !important;
}
.air-date-drop {
    margin-left: 10px !important;
}
input.program-name {
    width: 100% !important;
}
</style>
@stop

@section('header')
    @include('admin.monitor.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <h3>{!! $languages[$language_id] !!} Long-form Monitoring <form class="form-inline" style="display:inline;"><input type="text" id="air_date_changer" name="air_date_changer" value="{!! $monitor_date->format('Y-m-d') !!}" /> {!! Former::select('language_id','')->options($languages, $language_id)->id('language_id') !!} <a class='btn btn-sm btn-default' id="set-date">Set Date</a></form></h3>
        </div>
    </div>
    <div class="row-fluid">
        <div class="col-md-6">
            <h4>Long Form</h4>
            <form class="program-airtime-form form-inline">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="airtime_id" value="">
                {!! Former::select('channel_id','')->options($channels_array)->class('col-md-9')->id('channel_id') !!}
                {!! Former::select('air_time','')->options($times)->class('air-date-drop col-md-3 form-control')->id('air_time') !!}
                {!! Form::hidden('custom_program_name',null, array('id'=>'custom_program_name')) !!}
                {!! Former::text('program_version_id','')->class('col-md-12 program-name')->placeholder('Start Typing to Find Show')->required() !!}

                <div class="col-md-12" style="margin-top:5px;">
                    <a href="#" class="btn btn-primary airtime-submit">Save</a>
                    <label class="checkbox" style="margin-left: 10px;">
                        <input type="checkbox" name="new_version" id="verCk"> New Version
                    </label>
                </div>
                <div class="col-md-11">
                    <div class="progress progress-striped" style="display:none;">
                      <div class="progress-bar progress-bar-info" style="width: 100%;"></div>
                    </div>
                </div>
            </form>
            <div class="col-md-12">
                <h4>Input Log <small><a href="#" id="refresh-log"><i class="icon-refresh"></i> Refresh</a></small></h4>
                <div id="inputLog"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div id="programLink" style="display:none;">
                <h4>Preview Shows <small><a href="#" id="hidePreview">Hide All</a></small></h4>
                <ul id="previewList" class="nav nav-list">
                </ul>
            </div>
            <div class="col-md-11">
            <h4>New Shows This Week <small><a href="#" id="refresh-new"><i class="icon-refresh"></i> Refresh</a></small></h4>
            <table class="table table-striped table-bordered" id="new-shows-tbl">
                <thead>
                    <th>Title</th>
                    <th>Grid Title</th>
                    <th style="width:25px;">v</th>
                    <th style="width:35px;">Network</th>
                    <th></th>
                    <th>Detected</th>
                    <th>User</th>
                    <th style="width:70px;"></th>
                </thead>
            </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('script')
<script src="{{ asset("assets/js/admin.monitor.doit-long.js") }}"></script>
@stop
