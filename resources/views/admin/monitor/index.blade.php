@extends('backpack::layout')

@section('header')
    @include('admin.monitor.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
	<div class="row">
        <div class="col-md-12">
        	<div class="block">
                <p class="block-heading">Monitor Wizard</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.monitor.index' ),'GET') !!}
                    {!! Former::text('monitor_date','Date',date('Y-m-d'),array('class'=>'monitor-date','id'=>'monitor-date'))!!}
                    {!! Former::select('language_id','Language')->options($languages,1) !!}
                    {!! Former::select('type', 'Type')->options(array('long'=>'Long Form', 'short'=>'Short & Long Form'),'short') !!}
                    {!! Former::actions()
                    ->large_primary_submit('Submit')
                    ->large_inverse_reset('Reset') !!}
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop
