@extends('backpack::layout')

@section('style')
<style>
.container {
    width: 99%;
}
form.spot-airtime-form {
    margin-bottom: 0px;
}
img.channel {
    height: 15px;
    padding-right: 10px;
}
tr.row_selected {
    background-color: #ddd !important;
}
.air-date-drop {
    margin-left: 10px !important;
}
</style>
@stop

@section('header')
    @include('admin.monitor.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <h3>{!! $languages[$language_id] !!} Spot and Longform Monitoring <input type="text" id="air_date_changer" name="air_date_changer" value="{!! $monitor_date->format('Y-m-d') !!}" /> <small><a href="{!! URL::route('admin.monitor.index') !!}">Click here</a> to change Date, Language or Rows.</small></h3>
        </div>
    </div>
    <div class="row-fluid">
        <div class="col-md-7">
            <h4>Short Form</h4>
            <form class="spot-airtime-form form-inline">
                <input type="hidden" name="_method" value="POST">
                <input type="hidden" name="language_id" id="language_id" value="{!! $language_id !!}">
                <input type="hidden" name="air_date" id="air_date" value="{!! $monitor_date->format('Y-m-d') !!}">
                <div class="col-md-12" style="margin-bottom:5px;">
                {!! Former::select('channel_id','')->options($channels_array)->class('col-md-8')->id('channel_id') !!}
                    <div class="input-group">
                        <input id="time-picker" name="air_time" type="text" class="form-control" value="00:00:00">
                        <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-bottom:5px;">
                {!! Former::text('spot_version_id','')->class('col-md-12 spot-name')->placeholder('Start Typing to Find Spot')->required() !!}
                </div>
                <div class="col-md-12" style="margin-bottom:5px;">
                    <a href="#" class="btn btn-primary spot-airtime-submit">Save</a>
                    <label class="checkbox" style="margin-left: 10px;">
                        <input type="checkbox" name="new_version" id="verCk"> New Version
                    </label>
                     <label class="checkbox" style="margin-left: 10px;">
                        <input type="checkbox" name="new_spot_ck" id="newCk"> New Spot
                    </label>
                    <label class="checkbox" style="margin-left: 10px; display:none;">
                        <input type="checkbox" name="flag_spot_ck" id="flgCk"> Flag Spot
                    </label>
                </div>
                <div class="col-md-12" style="margin-bottom:5px;display:none;" id="newFld">
                    <input type="text" name="new_spot" class="col-md-12" placeholder="Enter New Spot Name Here" />
                </div>
                <div class="col-md-12" style="margin-bottom:5px;display:none;" id="flagFld">
                    <input type="text" name="flag_spot" class="col-md-12" placeholder="Enter Thumbnail Name to Flag Here" />
                </div>
                <div class="col-md-11">
                    <div class="progress progress-striped" style="display:none;">
                      <div class="progress-bar progress-bar-info" style="width: 100%;"></div>
                    </div>
                </div>
            </form>
            <div class="col-md-12">
                <h4>Input Log <small><a href="#" id="refresh-log"><i class="icon-refresh"></i> Refresh</a></h4>
                <div id="inputLog"></div>
            </div>
        </div>
        <div class="col-md-5">
            <div id="longForm" class="col-md-11">
                <h4>Long Form <a href="#" id="longform-tgl"><small>Show/Hide</small></a></h4>
                <div id="longform-frm">
                <form class="airtime-form form-inline">
                    <input type="hidden" name="airtime_id" id="airtime_id" value="" />
                    <div class="col-md-12" style="margin-bottom:5px;">
                        {!! Former::text('program_version_id','')->class('col-md-9 program-name')->placeholder('Start Typing to Find Program')->required() !!}
                        {!! Former::select('air_time','')->options($times)->class('col-md-2 air-date-drop')->id('air_time') !!}
                    </div>
                    <div class="col-md-12" style="margin-bottom:5px;">
                        <a href="#" class="btn btn-primary airtime-submit">Save</a>
                        <label class="checkbox" style="margin-left: 10px;">
                            <input type="checkbox" name="new_show_ck" id="newShowCk"> New Show
                        </label>
                        <label class="checkbox" style="margin-left: 10px;">
                            <input type="checkbox" name="regular_programming" id="regular_programming" class="regular"> Regular Programming
                        </label>
                    </div>
                    <div class="col-md-12" style="margin-bottom:5px;display:none;" id="newShowFld">
                        <input type="text" name="custom_show" id="new_show" class="col-md-11" placeholder="Enter New Show Name Here" />
                    </div>
                    <div class="col-md-11">
                        <div class="progress progress-striped" style="display:none;">
                          <div class="progress-bar progress-bar-info" style="width: 100%;"></div>
                        </div>
                    </div>
                </form>
                </div>
            </div>
            <div id="spotLink" style="display:none;">
                <h4>Preview Spots <small><a href="#" id="hidePreview">Hide All</a></small></h4>
                <ul id="previewList" class="nav nav-list">
                </ul>
            </div>
            <div class="col-md-11">
            <h4>Day Parts <a href="#" id="daypart-tgl"><small>Show/Hide</small></a></h4>
            <table class="table table-striped" id="daypart-tbl" style="display:none;">
                <thead>
                    <th>Name</th>
                    <th>Abbr.</th>
                    <th>Start Day</th>
                    <th>End Day</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                </thead>
                <tbody>
            @foreach($dayparts as $part)
                <tr>
                    <td>{!! $part->name !!}</td>
                    <td>{!!$part->abbr !!}</td>
                    <td>{!! $days[$part->day_start] !!}</td>
                    <td>{!! $days[$part->day_end] !!}</td>
                    <td>{!! substr($part->time_start, 0, -3) !!}</td>
                    <td>{!! substr($part->time_end, 0, -3) !!}</td>
                </tr>
            @endforeach
                </tbody>
            </table>
            </div>
            <div class="col-md-11">
            <h4>New Spots This Week <small><a href="#" id="refresh-new"><i class="icon-refresh"></i> Refresh</a></small></h4>
            <table class="table table-striped table-bordered" id="new-spots-tbl">
                <thead>
                    <th>Title</th>
                    <th style="width:25px;">v</th>
                    <th style="width:30px;">Len.</th>
                    <th></th>
                    <th class="col-md-1">Mkt.</th>
                    <th>Network</th>
                    <th>Detected</th>
                    <th>User</th>
                </thead>
            </table>
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('script')
<script src="{{ asset("assets/js/admin.monitor.doit-short.js") }}"></script>

<script>
$(function() {
    $('#time-picker').datetimepicker({
        format: 'HH:mm:ss',
        useCurrent: false
    });
});
</script>
@stop
