@extends('backpack::layout')

@section('header')
    @include('admin.groups.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            {!! Former::horizontal_open(route('admin.groups.profile', array($group->id)))->method('PUT') !!}
            <div class="block">
                <p class="block-heading">{!!$group->name!!} Group Permissions</p>
                <div class="block-body">

                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active"><a href="#ims" data-toggle="tab">IMS Profile</a></li>
                        <li><a href="#generic" data-toggle="tab">Generic Permissions</a></li>
                        <li><a href="#module" data-toggle="tab">Modules Permissions</a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="ims">
                        <legend>IMS Profile</legend>
                            {!! Former::text('data_since','Data Since', $group_profile->data_since->format('Y-m-d'),array('class'=>'date-picker'))->help('Select the date which they can see back to.') !!}
                            {!! Former::text('data_to','Data To', $group_profile->data_to->format('Y-m-d'),array('class'=>'date-picker'))->help('Select date after today that matches subscription length.') !!}
                            {!! Former::select('subscription_id','Subscription')->options(array(''=> 'N/A')+$subscriptions,$group_profile->subscription_id)->help('Note, this does not give the group subscription Access, make sure you add the Subscription group to the user. ') !!}
                            {!! Former::text('company_id','Company', $group_profile->company_id) !!}
                        </div>
                        <div class="tab-pane" id="generic">
                            @foreach( $genericPerm as $perm)
                                <legend>Generic Permissions</legend>
                                @foreach( $perm['permissions'] as $input )
                                    {!! Former::select($input['name'],$input['text'])
                                        ->options(array('0' => 'Deny', '1' => 'Allow'))
                                        ->value($input['value'])
                                        ->id($input['id'])
                                    !!}
                                @endforeach
                            @endforeach
                        </div>
                        <div class="tab-pane" id="module">
                            @if (count($modulePerm) < 1)
                                <div class="alert alert-info">
                                    {!! __('cpanel::permissions.no_found') !!}
                                </div>
                            @else
                                @foreach( $modulePerm as $perm)
                                    <legend>{!! $perm['name'] !!} Module</legend>
                                    @foreach( $perm['permissions'] as $input )
                                        {!! Former::select($input['name'],$input['text'])
                                            ->options(array('0' => 'Deny', '1' => 'Allow'))
                                            ->value($input['value'])
                                            ->id($input['id'])
                                        !!}
                                    @endforeach
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <a href="{!!route('admin.groups.index')!!}" class="btn">Cancel</a>
                    </div>
                </div>
            </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>
</div>
@stop
