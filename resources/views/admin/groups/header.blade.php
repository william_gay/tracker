<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-group"></i>
        Groups
        <a href="{!! route('admin.groups.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Group">
            <i class="fa fa-plus"></i>
            New Group
        </a>
    </h3>
</div>
</div>
