@extends('backpack::layout')

@section('header')
    @include('admin.groups.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            {!! Former::horizontal_open(route('admin.groups.update', array($group->id)))->method('PUT') !!}
            <div class="block">
                <p class="block-heading">Edit "{!! $group->name !!}" Group</p>
                <div class="block-body">


                    {!! Former::xlarge_text('name','Name')->value($group->name)->required() !!}

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <a href="{!!route('admin.groups.index')!!}" class="btn">Cancel</a>
                    </div>
                </div>
            </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>
</div>
@stop
