@extends('backpack::layout')

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">

            <div class="margin-top-20">
                @if ( Session::has('login_error') )
                    <div class="alert-login alert-error">
                        <strong>{!! Session::get('login_error') !!}</strong>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
        <h3 class="text-center">Sign-In</h3>
            <form action="{!! URL::route('admin.login') !!}" class="form-horizontal" role="form" method="POST">
                <div class="form-group">
                    <label for="{!! $login_attribute !!}" class="col-sm-2 control-label">{!! ucfirst($login_attribute) !!}</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="login_attribute" id="login_attribute" value="{!! Request::old('login_attribute') !!}" placeholder="E-Mail">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember_me" value="true">  Remember me on this computer
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-primary" type="submit">Sign in</button>
                    </div>
                </div>
            </form>

            <div class="login-extra">
                <!--
                    TODO: make link to forget password
                 -->
            </div>

        </div>
    </div>
</div>
</div>
@stop
