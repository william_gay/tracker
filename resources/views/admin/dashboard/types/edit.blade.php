@extends('backpack::layout')

@section('header')
    @include('admin.dashboard.types.header')
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Dashboard Type</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.dashboard-types.update', array($type->id)), 'PUT') !!}
                    {!! Former::xlarge_text('name', 'Name', $type->name)->required() !!}
                    {!! Former::xlarge_text('slug', 'Slug', $type->slug)->required() !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.dashboard-types.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
