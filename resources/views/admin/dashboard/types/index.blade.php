@extends('backpack::layout')

@section('header')
    @include('admin.dashboard.types.header')
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Dashboard Types ({!! $types->count() !!})</p>
                <div class="block-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Updated At</th>
                                <th>Created At</th>
                                <th><i class="icon-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($types as $type)
                                <tr>
                                    <td>{!! $type->id !!}</td>
                                    <td>{!! $type->name !!}</td>
                                    <td>{!! $type->slug !!}</td>
                                    <td>{!! $type->updated_at !!}</td>
                                    <td>{!! $type->created_at !!}</td>
                                    <td>
                                    <a href="{!! route('admin.dashboard-types.edit', array($type->id)) !!}" class="btn btn-default btn-sm">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    <a href="{!! route('admin.dashboard-types.destroy', array($type->id)) !!}"
                                       data-method="delete"
                                       data-modal-text="Delete this Dashboard Type?"
                                       class="btn btn-sm btn-danger">
                                       <i class="fa fa-trash-o"></i>
                                    </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="9">
                                    {!! $types->links() !!}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
