<h3>
    <i class="fa fa-dashboard"></i>
    Dashboard Types
    <a href="{!! URL::route('admin.dashboard-types.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Dashboard Type">
        <i class="fa fa-plus"></i>
        New Dashboard Type
    </a>
</h3>