@extends('backpack::layout')

@section('header')
<div class="box box-default">
    <div class="box-header with-border">
        <h3>
            <i class="fa fa-dashboard"></i>
            Dashboard
        </h3>
    </div>
</div>
@stop

@section('style')
    <style>
    .container {
        width: 100%;
    }
    </style>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
	<div class="row">
		<div class="col-md-12">
			<p></p>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12">
        	<h4>
                Welcome back, {!! Sentry::getUser()->first_name !!}
                <small>
                    Last Login: {!! date("m/d/Y  h:i a",strtotime(Sentry::getUser()->last_login)) !!}
                    &bull;
                    Member Since: {!! date("m/d/Y  h:i a",strtotime(Sentry::getUser()->created_at)) !!}
                </small>
            </h4>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h4 class="text-center">Popular Reports of All Time</h4>
            <table class="table">
                <thead>
                    <th>Report</th>
                    <th>Users</th>
                    <th>Views</th>
                </thead>
                <tbody>
                @foreach($reports as $report)
                    <tr>
                        <td>
                            {!! link_to_route('reports.show', $report->name, $report->report_id) !!}
                        </td>
                        <td>
                            {!! $report->users !!}
                        </td>
                        <td>
                            {!! $report->views !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <h4 class="text-center">Popular Reports Last Week</h4>
            <table class="table">
                <thead>
                    <th>Report</th>
                    <th>Users</th>
                    <th>Views</th>
                </thead>
                <tbody>
                @foreach($weekReports as $report)
                    <tr>
                        <td>
                            {!! link_to_route('reports.show', $report->name, $report->report_id) !!}
                        </td>
                        <td>
                            {!! $report->users !!}
                        </td>
                        <td>
                            {!! $report->views !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <h4 class="text-center">Popular Shows</h4>
            <table class="table">
                <thead>
                    <th>Title</th>
                    <th>Users</th>
                    <th>Views</th>
                </thead>
                <tbody>
                @foreach($programs as $report)
                    <tr>
                        <td>
                            {!! link_to_route('programs.show', $report->title, $report->program_version_id) !!}
                        </td>
                        <td>
                            {!! $report->users !!}
                        </td>
                        <td>
                            {!! $report->views !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h4 class="text-center">Most Active Users</h4>
            <table class="table">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Views</th>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                            {!! $user->first_name.' '.$user->last_name !!}
                        </td>
                        <td>
                            {!! $user->email !!}
                        </td>
                        <td>
                            {!! $user->views !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <h4 class="text-center">Active Users Last Week</h4>
            <table class="table">
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Views</th>
                </thead>
                <tbody>
                @foreach($weekUsers as $user)
                    <tr>
                        <td>
                            {!! $user->first_name.' '.$user->last_name !!}
                        </td>
                        <td>
                            {!! $user->email !!}
                        </td>
                        <td>
                            {!! $user->views !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <h4 class="text-center">Popular Spots</h4>
            <table class="table">
                <thead>
                    <th>Title</th>
                    <th>Users</th>
                    <th>Views</th>
                </thead>
                <tbody>
                @foreach($spots as $report)
                    <tr>
                        <td>
                            {!! link_to_route('spots.show', $report->title, $report->spot_version_id) !!}
                        </td>
                        <td>
                            {!! $report->users !!}
                        </td>
                        <td>
                            {!! $report->views !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>
</div>
@stop
