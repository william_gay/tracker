@extends('backpack::layout')

@section('header')
<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-pencil-square-o"></i>
        Register
    </h3>
    </div>
</div>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">

            <div class="block">
                <p class="block-heading">Registration</p>
                <div class="block-body">
                    {!! Former::horizontal_open(route('admin.register')) !!}
                        <fieldset>
                            <legend>Personal Information</legend>
                            {!! Former::xlarge_text('first_name', 'First Name') !!}
                            {!! Former::xlarge_text('last_name', 'Last Name') !!}
                        </fieldset>
                        <fieldset>
                            <legend>Email</legend>
                            {!! Former::xlarge_text('email','Email') !!}
                        </fieldset>
                        <fieldset>
                            <legend>Password</legend>
                            {!! Former::xlarge_password('password', 'Password') !!}
                            {!! Former::xlarge_password('password_confirmation', 'Confirm Password') !!}
                        </fieldset>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                    {!! Former::close() !!}
                </div>
            </div>

        </div>
    </div>
    </div>
    </div>
@stop
