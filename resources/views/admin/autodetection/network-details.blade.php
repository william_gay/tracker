@extends('backpack::layout')

@section('header')
<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-magic"></i>
        Detections Details by Network &bull; {!! $network->name !!}
    </h3>
    </div>
</div>
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
</style>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
<div class="row">
    <div class="col-md-12" style="margin-top: 5px;">
    {!! Former::inline_open()->id('dateRange')->method('GET') !!}
        {!! Former::text('startDate', '', $startDate->format('Y-m-d')) !!}
        {!! Former::text('endDate', '', $endDate->format('Y-m-d')) !!}
        {!! Form::select('language_id',$languages, $language_id) !!}
        {!! Former::submit() !!}
    {!! Former::close() !!}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <h3>Long-form By Network</h3>
        @if($longform)
        <table class="table table-hover table-bordered lf-by-network">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Detections</th>
                    <th>Auto</th>
                    <th>Manual</th>
                </tr>
            </thead>
            <tbody>
            @foreach($longform as $lf)
                <tr>
                    <td>{!!  $lf->air_dateable->format('Y-m-d') !!}</td>
                    <td>{!!  $lf->total_count ?? 'N/A' !!}</td>
                    <td>{!!  $lf->auto_count ?? 'N/A' !!}</td>
                    <td>{!!  $lf->manual_count ?? 'N/A' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p class="bg-warning">No Data!</p>
        @endif
    </div>
    <div class="col-md-6">
        <h3>Short-form By Network</h3>
        @if($shortform)
        <table class="table table-hover table-bordered sf-by-network">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Detections</th>
                    <th>Auto</th>
                    <th>Manual</th>
                </tr>
            </thead>
            <tbody>
            @foreach($shortform as $sf)
                <tr>
                    <td>{!!  $sf->air_dateable->format('Y-m-d') !!}</td>
                    <td>{!!  $sf->total_count ?? 'N/A' !!}</td>
                    <td>{!!  $sf->auto_count ?? 'N/A' !!}</td>
                    <td>{!!  $sf->manual_count ?? 'N/A' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p class="bg-warning">No Data!</p>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6">
    @if(!$shows->isEmpty())
        <table class="table table-hover table-bordered">
            <thead>
                <th>Show</th>
                <th>Initial Date</th>
            </thead>
            <tbody>
            @foreach ($shows as $show)
                <tr>
                    <td>{!! link_to_route('programs.show', $show->title, $show->id, array('target' => '_blank')) !!}</td>
                    <td>{!! $show->initial_date !!}</td>
                </tr>
            </tbody>
            @endforeach
        </table>
    @else
        <p class="bg-warning">No new spots!</p>
    @endif
    </div>
    <div class="col-md-6">
    @if(!$spots->isEmpty())
        <table class="table table-hover table-bordered">
            <thead>
                <th>Spot</th>
                <th>Initial Date</th>
            </thead>
            <tbody>
            @foreach ($spots as $spot)
                <tr>
                    <td>{!! link_to_route('spots.show', $spot->title, $spot->id, array('target' => '_blank')) !!}</td>
                    <td>{!! $spot->initial_date !!}</td>
                </tr>
            </tbody>
            @endforeach
        </table>
    @else
        <p class="bg-warning">No new spots!</p>
    @endif
    </div>
</div>
</div>
</div>
@stop

@section('script')
<script>
$(function(){

    oTable = $('.lf-by-network,.sf-by-network').dataTable( {
        "pageLength": 100
    });
});
</script>
@stop
