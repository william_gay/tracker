@extends('backpack::layout')

@section('header')
<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-magic"></i>
        Detections Details by Network
    </h3>
</div>
</div>
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
</style>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
<div class="row">
    <div class="col-md-12" style="margin-top: 5px;">
    {!! Former::inline_open()->id('dateRange')->method('GET') !!}
        {!! Former::text('startDate', '', $startDate->format('Y-m-d')) !!}
        {!! Former::text('endDate', '', $endDate->format('Y-m-d')) !!}
        {!! Form::select('language_id',$languages, $language_id) !!}
        {!! Former::submit() !!}
    {!! Former::close() !!}
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <h3>Long-form By Network</h3>
        @if(!$longform->isEmpty())
        <table class="table table-hover table-bordered lf-by-network">
            <thead>
                <tr>
                    <th>Network</th>
                    <th>Detections</th>
                    <th>Auto</th>
                    <th>Manual</th>
                    <th>New</th>
                </tr>
            </thead>
            <tbody>
            @foreach($longform as $lf)
                <tr>
                    <td>{!!  link_to_route('admin.autodetection.network.details', $lf->channel->name, array('id' => $lf->channel->id, 'startDate' => $startDate->format('Y-m-d'), 'endDate' => $endDate->format('Y-m-d'))) !!}</td>
                    <td>{!!  $lf->total_count ?? 'N/A' !!}</td>
                    <td>{!!  $lf->auto_count ?? 'N/A' !!}</td>
                    <td>{!!  $lf->manual_count ?? 'N/A' !!}</td>
                    <td> @if($lf->new_count > 0) YES @else NO @endif </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p class="bg-warning">No Data!</p>
        @endif
    </div>
    <div class="col-md-6">
        <h3>Short-form By Network</h3>
        @if(!$shortform->isEmpty())
        <table class="table table-hover table-bordered sf-by-network">
            <thead>
                <tr>
                    <th>Network</th>
                    <th>Detections</th>
                    <th>Auto</th>
                    <th>Manual</th>
                    <th>New</th>
                </tr>
            </thead>
            <tbody>
            @foreach($shortform as $sf)
                <tr>
                    <td>{!!  link_to_route('admin.autodetection.network.details', $sf->channel->name, array('id' => $sf->channel->id, 'startDate' => $startDate->format('Y-m-d'), 'endDate' => $endDate->format('Y-m-d'))) !!}</td>
                    <td>{!!  $sf->total_count ?? 'N/A' !!}</td>
                    <td>{!!  $sf->auto_count ?? 'N/A' !!}</td>
                    <td>{!!  $sf->manual_count ?? 'N/A' !!}</td>
                    <td> @if($sf->new_count > 0) YES @else NO @endif </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <p class="bg-warning">No Data!</p>
        @endif
    </div>
    </div>
</div>
</div>
</div>
@endsection

@section('script')
<script>
$(function(){

    oTable = $('.lf-by-network,.sf-by-network').dataTable( {
        "pageLength": 100
    });
});
</script>
@stop
