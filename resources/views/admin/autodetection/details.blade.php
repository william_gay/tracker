@extends('backpack::layout')

@section('header')
<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-magic"></i>
        Detections Details by Filename
    </h3>
    </div>
</div>
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
</style>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
<div class="row">
    <div class="col-md-12" style="margin-top: 5px;">
    {!! Former::inline_open()->id('dateRange')->method('GET') !!}
        {!! Former::text('startDate', '', $startDate->format('Y-m-d')) !!}
        {!! Former::text('endDate', '', $endDate->format('Y-m-d')) !!}
        {!! Former::text_xlarge('filename', '', $filename)->placeholder('SF_the-rack-3_26165_120.avi') !!}
        {!! Former::submit() !!}
    {!! Former::close() !!}
    </div>
</div>
@if(Request::filled('filename') and isset($details))
<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">{!! $filename ?? 'No file entered!' !!}</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Auto-Detected</th>
                    <th>Manual-Detected</th>
                    <th>Deleted</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{!! $details['autodetected'] !!}</td>
                    <td>{!! $details['manualdetected'] !!}</td>
                    <td>{!! $details['deleted'] !!}</td>
                    <td>{!! $details['total'] !!}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        @if($ad)
            <h4 class="text-center">Clips Tracked ({!! $ad->clipsters->count() !!})</h4>
            <ul>
            @foreach($ad->clipsters as $clip)
                <li>{!! $clip->filename !!}</li>
            @endforeach
            </ul>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table id="airtimes" class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>Channel</th>
                    <th>Air Date</th>
                    <th>Cost</th>
                    <th>User</th>
                    <th>Created At</th>
                    <th>Deleted At</th>
                </tr>
            </thead>
            <tbody>
                @foreach($raw as $a)
                <tr>
                    <td>{!! $a->channel->name !!}</td>
                    <td>{!! $a->air_date !!}</td>
                    <td>{!! $a->cost ?? 'N/A' !!}</td>
                    <td>
                    @if($a->user)
                        {!! str_replace('@imsreport.com', "", $a->user->email) !!}
                    @else
                        @if($a->url)
                            <a href="http://{!! $a->url!!}" target="_blank">Log</a>
                        @endif
                    @endif
                    </td>
                    <td>{!! $a->created_at !!}</td>
                    <td>{!! $a->deleted_at !!}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@else
    <h4>Enter a filename to start!</h4>
    <p>Filename must be formatted like this: [LF/SF]_[title-version]_[duration].avi</p>
    <p>Examples: </p>
    <ul>
        <li>SF_the-rack-3_26165_120.avi</li>
        <li>LF_rydis-robo-vacmop_12745_1710_letterboxed</li>
    </ul>
@endif
</div>
</div>
@endsection

@section('script')
<script>
$(function(){

    oTable = $('#airtimes').dataTable( {
        "pageLength": 100
    });
});
</script>
@stop
