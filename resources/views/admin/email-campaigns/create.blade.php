@extends('backpack::layout')

@section('header')
<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-dashboard"></i>
        Email Campaigns
    </h3>
    </div>
</div>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs" role="tablist">
            @unless (Request::get('market_id') == 4)
                <li class="active"><a id="weeklyTab"href="#weekly" role="tab" data-toggle="tab">Weekly</a></li>
            @endunless
                <li><a id="monthlyTab" href="#monthly" role="tab" data-toggle="tab">Monthly</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="weekly">
                    <h2>Weekly Email Campaign</h2>
                    <div class="row">
                        <div class="col-md-12 column" style="padding: 30px;">
                            {!! Html::ul($errors->all()) !!}

                            {!! Form::open(array('url' => 'admin/email-campaign/create')) !!}
                                {!! Form::hidden('period_type', 'week') !!}
                                <div class="form-group">
                                    {!! Form::label('notes', 'Notes') !!}
                                    {!! Form::textarea('notes', null, ['class' => 'form-control']) !!}
                                </div>
                                {!! Form::submit('Create Weekly Campaign', array('class' => 'btn btn-success')) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="monthly">
                    <h2>Monthly Email Campaign</h2>
                    <div class="row">
                        <div class="col-md-12 column" style="padding: 30px;">
                            {!! Html::ul($errors->all()) !!}

                            {!! Form::open(array('url' => 'admin/email-campaign/create')) !!}
                                {!! Form::hidden('period_type', 'month') !!}
                                <div class="form-group">
                                    {!! Form::label('notes', 'Notes') !!}
                                    {!! Form::textarea('notes', null,['class' => 'form-control']) !!}
                                </div>
                                {!! Form::submit('Create Monthly Campaign', array('class' => 'btn btn-success')) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop


@section('script')
<script type="text/javascript">
$(document).ready(function(){

    //=============================================================================
    // Bootstrap Longform/Shortform Tab Selector & Tab-Chart Fix
    //============================================================================
    $('#weeklyTab').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    })
    $('#monthlyTab').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    })
});
</script>
@stop
