@extends('backpack::layout')

@section('header')
<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-dashboard"></i>
        Email Campaigns
    </h3>
    </div>
</div>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="{!! URL::to('admin/email-campaign') !!}">Email Campaigns Listing</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="">
                                <a href="{!! URL::to('admin/email-campaign/create') !!}">Create an Email Campaign</a>
                            </li>
                     </div>
                </nav>
                <div class="row">
                    <div class="col-md-12 column" style="padding: 30px;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>Creted Date</td>
                                    <td>Week/Month Ending</td>
                                    <td>Period Type</td>
                                    <td>Notes</td>
                                    <td>Launch</td>
                                    <td>Modify</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($emailCampaigns as $emailCampaign)
                                    <tr>
                                        <td>{!! $emailCampaign->created_at !!}</td>
                                        <td>
                                            @if ($emailCampaign->period_type == 'week')
                                            {!! $emailCampaign->created_at->copy()->previous(Carbon\Carbon::FRIDAY)->format('Y-m-d') !!}
                                            @else
                                            {!! $emailCampaign->created_at->copy()->subMonth()->startOfMonth()->format('Y-m-d') !!}
                                            @endif
                                        </td>
                                        <td>{!! $emailCampaign->period_type !!}</td>
                                        <td>{!! $emailCampaign->notes !!}</td>
                                        @if ($emailCampaign->approved != 1)
                                            <td>
                                            @if ($emailCampaign->period_type == 'month')
                                                {!! Form::open(array('url' => 'admin/email-campaign/create/month')) !!}
                                                    {!! Form::hidden('emailCampaignId', $emailCampaign->id )!!}
                                                    {!! Form::submit('Send Monthly Campaign &#x00A;(CLICKING THIS WILL EMAIL SUBSCRIBERS)', array('class' => 'btn btn-success')) !!}
                                                {!! Form::close() !!}
                                            @else
                                                {!! Form::open(array('url' => 'admin/email-campaign/create/week')) !!}
                                                    {!! Form::hidden('emailCampaignId', $emailCampaign->id )!!}
                                                    {!! Form::submit('Send Weekly Campaign &#x00A;(CLICKING THIS WILL EMAIL SUBSCRIBERS)', array('class' => 'btn btn-success')) !!}
                                                {!! Form::close() !!}
                                            @endif
                                            </td>
                                             <td>
                                            {!! Form::open(array('url' => 'admin/email-campaign/' . $emailCampaign->id, 'class' => 'pull-right')) !!}
                                                {!! Form::hidden('_method', 'DELETE') !!}
                                                {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                                            {!! Form::close() !!}
                                            <a class="btn btn-small btn-info" href=" {!!URL::to('admin/email-campaign/'. $emailCampaign->id. '/edit') !!}">Edit</a>
                                            </td>
                                        @else
                                            <td>Sent: {!! $emailCampaign->send_count !!} emails</td>
                                            <td>Nope!</td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                {{$emailCampaigns->links()}}
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@stop
