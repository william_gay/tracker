@extends('backpack::layout')

@section('header')
<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-dashboard"></i>
        Email Campaigns
    </h3>
   </div>
</div>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-sm-12">
            <h2>Weekly Email Campaign</h2>
            <div class="row">
                <div class="col-md-12 column" style="padding: 30px;">
                    {!! Html::ul($errors->all()) !!}

                    {!! Form::open(array('url' => 'admin/email-campaign/edit')) !!}
                        {!! Form::hidden('emailCampaignId', $emailCampaign->id) !!}
                        <div class="form-group">
                            {!! Form::label('notes', 'Notes') !!}
                            {!! Form::textarea('notes', $emailCampaign->notes, ['class' => 'form-control']) !!}
                        </div>
                        {!! Form::submit('Update Weekly Campaign', array('class' => 'btn btn-success')) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop
