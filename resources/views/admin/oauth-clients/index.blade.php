@extends('backpack::layout')

@section('header')
    @include('admin.oauth-clients.header')
@stop

@section('content')
	{{-- <div class="row">
        <div class="col-md-12">
        	<div class="block">
                <p class="block-heading">OAuth Client Listing</p>
                <div class="block-body">
                {!! Form::open(array('url' => 'admin/oauth-clients')) !!}
                    <div>
                        {!! Form::label('client_name', 'Client Name') !!}
                        {!! Form::text('client_name', '', array('placeholder' => 'Capture Server 79ers', 'class' => 'form-control input-md')) !!}
                    </div>
                    <br />
                    <div>
                        {!! Form::label('redirect_uri', 'Redirect URI') !!}
                        {!! Form::text('redirect_uri', '', array('placeholder' => 'http://', 'class' => 'form-control input-md')) !!}
                    </div>
                    <br />
                    <div>
                        {!! Form::submit('Register', array('class' => 'btn btn-success')) !!}
                    </div>
                </div>
                <div class="row">
                @foreach($clients as $client)
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                      <div class="caption">
                        <h3>{!! $client->name !!}</h3>
                        <p style="display: inline;">Client ID: </p><p>{!! $client->id !!}</p>
                        <p style="display: inline;">Client Secret: </p><p>{!! $client->secret !!}</p>
                        <p style="display: inline;">Redirect URI: </p><p>{!! $client->endpoint->redirect_uri !!}</p>
                        <p><a href="#" class="btn btn-primary" role="button">Edit</a> <a href="#" class="btn btn-danger" role="button">Delete</a></p>
                      </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div> --}}
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="{!! URL::to('admin/oauth-clients') !!}">OAuth Client Listing</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="">
                                <a href="{!! URL::to('admin/oauth-clients/create') !!}">Create a Client</a>
                            </li>
                     </div>
                </nav>
                <div class="row">
                    <div class="col-md-12 column" style="padding: 30px;">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Client ID</td>
                                    <td>Client Secret</td>
                                    <td>Redirect URI</td>
                                    <td style="width: 140px;">Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($clients as $client)
                                    <tr>
                                        <td>{!! $client->name !!}</td>
                                        <td>{!! $client->id !!}</td>
                                        <td>{!! $client->secret !!}</td>
                                        <td>{!! $client->clientEndpoint->redirect_uri !!}</td>

                                        <td>
                                            {!! Form::open(array('url' => 'admin/oauth-clients/' . $client->id, 'class' => 'pull-right')) !!}
                                                {!! Form::hidden('_method', 'DELETE') !!}
                                                {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                                            {!! Form::close() !!}
                                            <a class="btn btn-small btn-info" href=" {!!URL::to('admin/oauth-clients/'. $client->id. '/edit') !!}">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
