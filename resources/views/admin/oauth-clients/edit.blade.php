@extends('backpack::layout')

@section('header')
    @include('admin.oauth-clients.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="{!! URL::to('admin/oauth-clients') !!}">OAuth Client Listing</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="">
                                <a href="{!! URL::to('admin/oauth-clients/create') !!}">Create a Client</a>
                            </li>
                     </div>
                </nav>

                <div class="row">
                	<div class="col-md-12 column" style="padding: 30px;">
		                {!! Html::ul($errors->all()) !!}

		                {!! Form::model($client, array(
		                	'route' => array('admin.oauth-clients.update', $client->id),
		                	'method' => 'PUT'
		                )) !!}

		                	<div class="form-group">
		                		{!! Form::label('client_name', 'Client Name') !!}
		                		{!! Form::text('client_name', $client->name, array(
			                		'class' => 'form-control',
			                		'placeholder' => 'Capture Server 79ers'
			                	)) !!}
		                	</div>

		                	<div class="form-group">
		                		{!! Form::label('redirect_uri', 'Redirect URI') !!}
		                		{!! Form::text('redirect_uri', $client->clientEndpoint->redirect_uri, array(
			                		'class' => 'form-control',
			                		'placeholder' => 'https://'
			                	)) !!}
		                	</div>

		                	{!! Form::submit('Edit Client', array('class' => 'btn btn-success')) !!}

		                {!! Form::close() !!}
		            </div>
		        </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

