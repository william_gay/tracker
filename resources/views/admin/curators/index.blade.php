@extends('backpack::layout')

@section('header')
    @include('admin.curators.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
	<div class="row">
        <div class="col-md-12">
        	<div class="block">
                <p class="block-heading">Curator Listing</p>
                <div class="block-body">
                {!! Form::open(array('url' => 'admin/curators')) !!}
                    <div>
                        {!! Form::label('client_name', 'Capture Server Name') !!}
                        {!! Form::text('client_name', '', array('placeholder' => 'Capture Server Name')) !!}
                    </div>
                    <div>
                        {!! Form::label('redirect_uri', 'Redirect URI') !!}
                        {!! Form::text('redirect_uri', '', array('placeholder' => 'http://')) !!}
                    </div>
                    <div>
                        {!! Form::submit('Register') !!}
                    </div>
                </div>
                <div class="block-body">
                @foreach($curators as $curator)
                <div style="border: 1px solid black; margin: 10px; padding: 5px; display: inline-block;">
                    <h2>{!! $curator->name !!}</h2>
                    <h4 style="display: inline;">Client ID: </h4><p>{!! $curator->id !!}</p>
                    <h4 style="display: inline;">Client Secret: </h4><p>{!! $curator->secret !!}</p>
                    <h4 style="display: inline;">Redirect URI: </h4><p>{!! $curator->endpoint->redirect_uri !!}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
</div>
@stop
