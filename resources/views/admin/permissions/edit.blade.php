@extends('backpack::layout')

@section('header')
    @include('admin.permissions.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Create new permissions for a module</p>
                <div class="block-body">
                    {!! Former::horizontal_open(route('admin.permissions.update', array($permission->id)))->method('PUT') !!}
                    <input name="id" value="{!! $permission->id !!}" type="hidden" />
                    {!! Former::xlarge_text('name', 'Module Name',$permission->name) !!}

                    <label class="control-label" for="permissions">
                        Permissions
                    </label>
                    <div class="controls">
                        @foreach ($roles['inputs'] as $key => $value)
                            <label class="checkbox">
                                <input type="checkbox" name="permissions[{!!$key!!}]"
                                    value="{!!$key!!}" {!! in_array($key, $permission->rules) ? 'checked="checked"' : '' !!}>
                                {!! ucfirst($key) !!}
                            </label>
                        @endforeach
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <a href="{!! route('admin.permissions.index') !!}" class="btn">Cancel</a>
                    </div>
                    {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
