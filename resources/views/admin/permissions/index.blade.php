@extends('backpack::layout')

@section('header')
    @include('admin.permissions.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">

            <div class="block">
                <p class="block-heading">
                    Permissions |
                    <em>Generic permissions</em>
                </p>
                <div class="block-body">
                    <ul>
                        @foreach ($roles['inputs'] as $role => $value)
                             <li>{!! ucfirst($role) !!}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="block">
                <p class="block-heading">
                    Permissions |
                    <em>Modules Permissions</em>
                </p>
                <div class="block-body">

                    @if($permissions->isEmpty())
                        <div class="alert alert-info">
                            {!! __('permissions.no_found') !!}
                        </div>
                    @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="col-md-2">Module</th>
                                    <th class="col-md-8">Roles</th>
                                    <th class="col-md-2"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($permissions->all() as $permission)
                                    <tr>
                                        <td>{!! $permission->name !!}</td>
                                        <td>
                                            <ul class="inline">
                                                @foreach ($permission->permissions as $role)
                                                    <li>{!! $role !!}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>
                                        @if($permission->name != 'Reports')
                                            <a href="{!! route('admin.permissions.edit', array($permission->id)) !!}"
                                                class="btn" rel="tooltip" title="Edit Permission">
                                                <i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <a href="{!! route('admin.permissions.destroy', array($permission->id)) !!}"
                                                class="btn btn-danger" rel="tooltip" title="Delete Permission" data-method="delete"
                                                data-modal-text="delete this Permission?">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
