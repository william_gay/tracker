<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-ban"></i>
        Permissions
        <a href="{!! route('admin.permissions.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Permissions">
            <i class="fa fa-plus"></i>
            New Permissions
        </a>
    </h3>
</div>
</div>
