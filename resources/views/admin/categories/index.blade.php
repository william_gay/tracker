@extends('backpack::layout')

@section('header')
    @include('admin.categories.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Categories ({!! $categories->count() !!})</p>
                <div class="block-body">
                    <table class="table table-striped" id="categories">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Parent Category</th>
                                <th>Category</th>
                                <th>Created</th>
                                <th><i class="icon-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                                <tr>
                                    <td>{!! $category->id !!}</td>
                                    <td>
                                    @if($category->parent_id != null)
                                        {!! $pcats[$category->parent_id] !!}
                                    @endif
                                    </td>
                                    <td>{!! $category->name !!}</td>
                                    <td>{!! date("m/d/Y  h:i a",strtotime($category->created_at)) !!}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
                                                Action
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{!! route('admin.categories.edit', array($category->id)) !!}">
                                                       <i class="fa fa-pencil-square-o"></i>&nbsp;Edit Category
                                                   </a>
                                                </li>
                                                <li>
                                                    <a href="{!! route('admin.categories.destroy', array($category->id)) !!}"
                                                       data-method="delete"
                                                       data-modal-text="Delete this Category?">
                                                       <i class="icon-trash"></i>&nbsp;Delete Category
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@stop

@section('script')
<script type="text/javascript">
    var oTable;
    $(document).ready(function() {
        oTable = $('#categories').dataTable();
    });
</script>
@endsection
