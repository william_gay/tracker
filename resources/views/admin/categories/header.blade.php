<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-sitemap"></i>
        <a href="/admin/categories">Categories</a>
        <a href="{!! URL::route('admin.categories.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Category">
            <i class="fa fa-plus"></i>
            New Category
        </a>
    </h3>
    </div>
</div>
