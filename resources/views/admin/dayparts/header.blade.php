<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-sun-o"></i>
        <a href="/admin/dayparts">Day Parts</a>
        <a href="{!! URL::route('admin.dayparts.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Day Part">
            <i class="fa fa-plus"></i>
            New Day Part
        </a>
    </h3>
    </div>
</div>
