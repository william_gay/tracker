@extends('backpack::layout')

@section('header')
    @include('admin.dayparts.header')
@stop

@section('content')
<div class="box box-default">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <p class="block-heading">Day Parts ({!! $dayparts->count() !!})</p>
                    <div class="block-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Abbr</th>
                                    <th>Day Start</th>
                                    <th>Day End</th>
                                    <th>Time Start</th>
                                    <th>Time End</th>
                                    <th>Week End</th>
                                    <th><i class="icon-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dayparts as $part)
                                    <tr>
                                        <td>{!! $part->id !!}</td>
                                        <td>{!! $part->name !!}</td>
                                        <td>{!! $part->abbr !!}</td>
                                        <td>{!! $days[$part->day_start] !!}</td>
                                        <td>{!! $days[$part->day_end] !!}</td>
                                        <td>{!! $part->time_start !!}</td>
                                        <td>{!! $part->time_end !!}</td>
                                        <td>{!! $part->week_end == 0 ? 'No' : 'Yes' !!}</td>
                                        <td>
                                        <a href="{!! route('admin.dayparts.edit', array($part->id)) !!}" class="btn btn-default btn-sm">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <a href="{!! route('admin.dayparts.destroy', array($part->id)) !!}"
                                           data-method="delete"
                                           data-modal-text="Delete this Day Part?"
                                           class="btn btn-sm btn-danger">
                                           <i class="fa fa-trash-o"></i>
                                        </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="9">
                                        {!! $dayparts->links() !!}
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
