@extends('backpack::layout')

@section('header')
    @include('admin.dayparts.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Day Part</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.dayparts.update', array($daypart->id)), 'PUT') !!}
                    {!! Former::xlarge_text('name', 'Name', $daypart->name)->required() !!}
                    {!! Former::xlarge_text('abbr', 'Abbr', $daypart->abbr)->required() !!}
                    {!! Former::select('day_start', 'Day Start')->options($days, $daypart->day_start) !!}
                    {!! Former::select('day_end', 'Day End')->options($days, $daypart->day_end) !!}
                    {!! Former::xlarge_text('time_start', 'Time Start', $daypart->time_start)->class('timepicker') !!}
                    {!! Former::xlarge_text('time_end', 'Time End', $daypart->time_end)->class('timepicker') !!}
                    {!! Former::checkbox('week_end', 'Week End?')->check(array('week_end' => $daypart->week_end ))->text('Is on weekend?') !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.dayparts.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('script')
<script>
$(function() {
    $('.timepicker').pickatime({
        'format': 'H:i',
        'interval': 5
    });
});
</script>
@stop
