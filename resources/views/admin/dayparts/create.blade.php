@extends('backpack::layout')

@section('header')
    @include('admin.dayparts.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Day Part</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.dayparts.store' )) !!}
                    {!! Former::xlarge_text('name','Name')->required() !!}
                    {!! Former::xlarge_text('abbr','Abbr')->required() !!}
                    {!! Former::select('day_start', 'Day Start')->options($days) !!}
                    {!! Former::select('day_end', 'Day End')->options($days) !!}
                    {!! Former::xlarge_text('time_start', 'Time Start')->class('timepicker') !!}
                    {!! Former::xlarge_text('time_end', 'Time End')->class('timepicker') !!}
                    {!! Former::checkbox('week_end', 'Week End?')->text('Is on weekend?') !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.dayparts.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('script')
<script>
$(function() {
    $('.timepicker').pickatime({
        'format': 'H:i',
        'interval': 5
    });
});
</script>
@stop
