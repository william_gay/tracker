<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-bookmark"></i>
        <a href="{!! URL::route('admin.manufacturers.index') !!}">{!! Lang::choice('manufacturers.manufacturers', 2) !!}</a>
        <a href="{!! URL::route('admin.manufacturers.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="{!! __('manufacturers.create') !!}">
            <i class="fa fa-plus"></i>
            {!! __('manufacturers.create') !!}
        </a>
    </h3>
    </div>
</div>
