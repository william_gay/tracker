@extends('backpack::layout')

@section('header')
    @include('admin.manufacturers.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12" style="padding-top:10px;">
            <table class="table table-striped table-bordered" id="category_reports">
                <thead>
                    <tr>
                        <th class="col-md-4">Name</th>
                        <th class="col-md-2">Slug</th>
                        <th class="col-md-2">Created At</th>
                        <th class="col-md-2">Updated At</th>
                        <th class="col-md-2"><i class="icon-cog"></i></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    </div>
    </div>
@stop

@section('script')
<script type="text/javascript">
    var oTable;
    $(document).ready(function() {

        oTable = $('#category_reports').dataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": "{!! URL::to('admin/manufacturers/data') !!}",
            "fnServerData": function ( sSource, aoData, fnCallback ) {
                $.getJSON( sSource, aoData, function (json) {
                    fnCallback(json)
                    laravel.initialize();
                });
            }
        });
    });
</script>
@stop
