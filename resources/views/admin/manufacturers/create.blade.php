@extends('backpack::layout')

@section('header')
    @include('admin.manufacturers.header')
@stop
@section('help')
    <p class="lead">{!! __('manufacturers.manufacturers') !!}</p>
    <p>
        Create manufacturers bro...
    </p>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">{!! __('manufacturers.create') !!}</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.manufacturers.store' ))->enctype('multipart/form-data') !!}
                    {!! Former::xlarge_text('name','Name')->required()->id('name') !!}
                    {!! Former::xlarge_text('slug','Slug')->required()->class('form-control slug') !!}
                    {!! Former::xlarge_text('gln', 'GLN') !!}
                    {!! Former::xlarge_text('upc_prefix', 'UPC Prefix') !!}
                    {!! Former::xlarge_text('gs1_prefix', 'GS1 Prefix') !!}
                    {!! Former::xlarge_text('website','Website') !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.products.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
