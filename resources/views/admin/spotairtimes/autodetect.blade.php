@extends('backpack::layout')

@section('header')
    <h3>
        <i class="fa fa-magic"></i>
        Short-Form Automagic
    </h3>
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
</style>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
<div class="row-fluid room">
    <div class="col-md-12" style="margin-top: 5px;">
    Limited 10,000 Results
    {!! Former::horizontal_open()->id('dateRange')->method('GET') !!}
         Start Date: <input class="input-xlarge" required="true" id="startDate" type="text" name="startDate" value="{!! $startDate->format('Y-m-d') !!}">
         End Date: <input class="input-xlarge" required="true" id="endDate" type="text" name="endDate" value="{!! $endDate->format('Y-m-d') !!}">
         <input class="btn-sm btn-primary btn" type="submit" value="Submit">
    {!! Former::close() !!}
    </div>
</div>
<div class="row-fluid room">
    <div class="col-md-12">
        <table class="table table-bordered table-striped">
            <thead>
                <th>Title</th>
                <th>Date</th>
                <th>Channel</th>
                <th>Server</th>
            </thead>
            <tbody>
            @foreach($airtimes as $airtime)
            <tr>
                <td>{!! link_to_route('spots.show', $airtime->spotVersion->title, $airtime->spot_version_id) !!}</td>
                <th>{!! $airtime->air_date !!}</th>
                <td>{!! $airtime->channel->name !!}</td>
                <td>
                    <?php $airdate = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $airtime->air_date) ?>
                    @if($airtime->channel and $airtime->channel->server)
                    <a href="{!! 'http://'.$airtime->channel->server->name.'.monitor.imsreport.com/capture/'.$airtime->channel->abbr.'/'.$airdate->format('Y').'/'.$airdate->format('m').'/'.$airdate->format('d').'/'.$airtime->channel->abbr.'_'.$airdate->format('Y-m-d').'_'.$airdate->format('H.i').'_318p.mp4' !!}" target="_blank">{!! $airtime->channel->server->name !!}</a>
                    @else
                    No Server Configured
                    @endif
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
@stop

@section('script')
<script>
$(function(){
    //
});
</script>
@stop
