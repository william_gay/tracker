@extends('backpack::layout')

@section('header')
    @include('admin.monitor.header')
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
</style>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
<div class="row-fluid room">
    <div class="col-md-12" style="margin-top: 5px;">
    {!! Former::horizontal_open()->id('dateRange')->method('GET')
!!}
         Start Date: <input class="input-xlarge" required="true" id="startDate" type="text" name="startDate" value="{!! $startDate->format('Y-m-d') !!}">
         End Date: <input class="input-xlarge" required="true" id="endDate" type="text" name="endDate" value="{!! $endDate->format('Y-m-d') !!}">
         Market: {!! Form::select('language_id',$languages, $languageId) !!}
         <input class="btn-xs btn-primary btn" type="submit" value="Submit">
    {!! Former::close() !!}
    </div>
</div>
<div class="row-fluid room">
    <div class="col-md-12">
        <h4>Curator Stats</h4>
        <table class="table table-bordered table-striped">
            <thead>
                <th>User</th>
                <th>New Spots</th>
                <th>Channels</th>
                <th>Detections</th>
            </thead>
            <tbody>
            @foreach($stats as $stat)
            <tr>
                <td>{!! $stat['email'] !!}</td>
                <td>{!! $stat['new_spots'] !!}</td>
                <td>{!! $stat['channels'] !!}</td>
                <td>{!! $stat['detection_count'] !!}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row-fluid room">
    <div class="col-md-6">
        <h4>Channel Stats</h4>
        <table class="table table-bordered table-striped localDT">
            <thead>
                <th>Channel</th>
                <th>Detections</th>
            </thead>
            <tbody>
            @foreach($channel_stats as $channel)
            <tr>
                <td>{!! link_to_route('admin.spotairtime.stats.channel', $channel->channel_name, array('channelId'=> $channel->channel_id, 'startDate' => $startDate->format('Y-m-d'), 'endDate' => $endDate->format('Y-m-d'), 'language_id' => $languageId) ) !!}</td>
                <td>{!! $channel->detection_count !!}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <h4>Spot Stats</h4>
        <table class="table table-bordered table-striped localDT">
            <thead>
                <th>Spot</th>
                <th>Detections</th>
            </thead>
            <tbody>
            @foreach($spot_stats as $spot)
            <tr>
                <td>{!! $spot->spot_name !!}</td>
                <td>{!! $spot->detection_count !!}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="row-fluid room">
    <div class="col-md-6">
    <h4>Day Part Stats</h4>
        <table class="table table-bordered table-striped localDT">
            <thead>
                <th>Day Part</th>
                <th>Detections</th>
            </thead>
            <tbody>
            @foreach($daypart_stats as $daypart)
            <tr>
                <td>{!! $daypart->daypart_name !!}</td>
                <td>{!! $daypart->detection_count !!}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
    <h4>Day Stats</h4>
        <table class="table table-bordered table-striped localDT">
            <thead>
                <th>Day</th>
                <th>Detections</th>
            </thead>
            <tbody>
            @foreach($day_stats as $day)
            <tr>
                <td>{!! $days[$day->day] !!}</td>
                <td>{!! $day->detection_count !!}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
@stop

@section('script')
<script>
var oTable;
$(function(){

    oTable = $('.localDT').dataTable( {
        "pageLength": 10,
        "sDom": "<'row-fluid'<'col-md-6'l><'col-md-6'f>r>t<'row-fluid'<'col-md-6'i><'col-md-6'p>>",

        "order": [[ 1, "desc" ]],
        "oLanguage": {
          "sLengthMenu": "_MENU_ records per page"
        }
    });

});
</script>
@stop
