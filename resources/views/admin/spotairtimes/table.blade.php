<h4 class="text-center">Hey {!! Sentry::getUser()->first_name !!}, you are inserting {!! $channel->name !!} on {!! $air_date->format('m/d/Y') !!}</h4>
<div class="fluid" style="height:25px">
    <span class="pull-left">{!! $airtimes->count() !!} Total {!! Lang::choice('spots.detections', $airtimes->count()) !!}.</span>
    @if ($server)
    <span class="pull-right">
        {!! $channel->name !!} &bull;
        <a href="http://{!! $server->ip !!}" target="_blank">{!! $server->name !!}</a> &bull; <a href="http://{!! $server->ip !!}{{ $path }}" target="_blank">Local</a> &bull; <a href="http://{!! strtolower($server->name) !!}.monitor.imsreport.com{!! $path !!}" target="_blank">Remote</a>
    </span>
    @endif
</div>
@if($airtimes->count() != 0)
<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <th></th>
            <th>Title</th>
            <th style="width: 25px;">v</th>
            <th>Price</th>
            <th style="width: 35px;"></th>
            <th></th>
            <th>Notes</th>
            <th>Day Part</th>
            <th>Time</th>
            <th>User</th>
            <th class="col-md-1">Input Time</th>
            <th><i class="icon-cog"></i></th>
        </tr>
    </thead>
    <tbody>
    @foreach ($airtimes as $airtime)
        <tr>
            <td><a href="#" data-spot="{!! $airtime->spot_version_id !!}" title="Quick Add" text="Quick Add Program?" class="btn btn-sm btn-default quickAdd"><i class="fa fa-plus"></i></a></td>
            <td><a href="{!! route("spots.show",array($airtime->spot_version_id)) !!}">{!! $airtime->spotVersion->title ?? 'N/A' !!}</a> - <a href="{!! route("admin.spots.edit",array($airtime->spot_version_id)) !!}">Edit</a></td>
            <td>{!! $airtime->spotVersion->version !!}</td>
            <td>{!! $airtime->spotVersion->price !!}</td>
            <td>
                @if($airtime->spotVersion->clipsters->count() > 0)
                    <i class="fa fa-bolt" title="Auto Detection Enabled"></i> {!! link_to_route('admin.autodetection.details', $airtime->spotVersion->clipsters->count(), ['filename' => $airtime->spotVersion->clipsters->first()->filename], ['target' => '_blank']) !!}
                @endif
                @if($airtime->spotVersion->spot and $airtime->spotVersion->spot->product and $airtime->spotVersion->spot->product->count() > 0)
                    <br>
                    <i class="fa fa-money" title="Auto Detection Enabled"></i>
                @endif
            </td>
            <td>:{!! $airtime->spotVersion->length !!}</td>
            <td>{!! $airtime->spotVersion->notes !!}</td>
            <td>{!! $airtime->dayPart->name ?? 'N/A' !!}</td>
            <td>{!! str_replace($air_date->format('Y-m-d'), '', $airtime->air_date) !!}</td>
            <td>
            @if($airtime->user and $airtime->user->email)
                {!! str_replace('@imsreport.com', "", $airtime->user->email) !!}
            @else
                @if($airtime->url)
                    <a href="http://{!! $airtime->url!!}" target="_blank">Log</a>
                @endif
            @endif
            </td>
            <td>{!! $airtime->created_at->format('m/d H:i:s') !!}</td>
            <td><a href="#" data-delete="{!! $airtime->id !!}" text="Delete this Airtime?" class="btn btn-sm btn-danger deleteLog"><i class="fa fa-trash-o"></i></a></td>
        </tr>
    @endforeach
    </tbody>
</table>
@else
    <div class="alert alert-info">No airings entered yet!</div>
@endif
