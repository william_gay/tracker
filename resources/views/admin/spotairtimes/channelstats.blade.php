@extends('backpack::layout')

@section('header')
    @include('admin.monitor.header')
@stop

@section('style')
<style>
.container {
    width: 99%;
}
.room {
    margin-left: 5px;
    margin-right: 5px;
}
</style>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
<div class="row-fluid room">
    <div class="col-md-12" style="margin-top: 5px;">
    {!! Former::horizontal_open()->id('dateRange')->method('GET')
!!}
         Start Date: <input class="input-xlarge" required="true" id="startDate" type="text" name="startDate" value="{!! $startDate->format('Y-m-d') !!}">
         End Date: <input class="input-xlarge" required="true" id="endDate" type="text" name="endDate" value="{!! $endDate->format('Y-m-d') !!}">
         Market: {!! Form::select('language_id',$languages, $languageId) !!}
         <input class="btn-sm btn-primary btn" type="submit" value="Submit">
    {!! Former::close() !!}
    </div>
</div>
<div class="row-fluid room">
    <div class="col-md-12">
    <h4><img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $channel->logo_location !!}" style="height: 50px;" /> {!! $channel->name .' - '. $channel->abbr !!} <small>Day Part Stats</small></h4>
        <table class="table table-bordered table-striped localDT">
            <thead>
                <th>Day Part</th>
                <th>Hours Per Day</th>
                <th>Days Per Week</th>
                <th>Hours Monitored</th>
                <th>Detections Per Hour/Per Day</th>
                <th>Detections</th>
            </thead>
            <tbody>
            @foreach($daypart_stats as $daypart)
            <tr>
                <td>{!! $daypart->daypart_name !!}</td>
                <td>{!! number_format($daypart->minutes/60,2) !!}</td>
                <td>{!! $daypart->days !!}</td>
                <td>{!! number_format($daypart->minutes/60,2) !!}</td>
                <td>
                    {!! number_format(($daypart->detection_count/($daypart->minutes/60)/(($totalDays/7)*$daypart->days)), 2) !!}
                </td>
                <td>{!! $daypart->detection_count !!}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
</div>
@stop

@section('script')
<script>
var oTable;
$(function(){
    //

    oTable = $('.localDT').dataTable( {
        "pageLength": 10,
        "sDom": "<'row-fluid'<'col-md-6'l><'col-md-6'f>r>t<'row-fluid'<'col-md-6'i><'col-md-6'p>>",

        "order": [[ 5, "desc" ]],
        "oLanguage": {
          "sLengthMenu": "_MENU_ records per page"
        }
    });

});
</script>
@stop
