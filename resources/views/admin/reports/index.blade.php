@extends('backpack::layout')

@section('header')
    @include('admin.reports.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-body">
                    {!! $html->table() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('script')
{!! $html->scripts() !!}
@stop
