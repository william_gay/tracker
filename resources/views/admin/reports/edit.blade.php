@extends('backpack::layout')

@section('header')
    @include('admin.reports.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Edit Report</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.reports.update', array($report->id)), 'PUT' ) !!}
                    {!! Former::xlarge_text('name','Name',$report->name)->required()->id('name') !!}
                    {!! Former::xlarge_text('slug','Slug',$report->slug)->required()->class('form-control slug') !!}
                    {!! Former::select('template','Template')->options($templates,$report->template) !!}
                    {!! Former::select('type','Type')->options($types,$report->type) !!}
                    {!! Former::xlarge_text('limit','Limit',$report->limit)->required() !!}
                    {!! Former::select('format','Format')->options($format, $report->format) !!}
                    {!! Former::select('interval','Interval')->options($intervals,$report->interval) !!}
                    {!! Former::select('language_id','Language')->options($languages,$report->language_id) !!}
                    {!! Former::textarea('js','JS', $report->js)->id('js') !!}
                    {!! Former::select('active','Action')->options($active,$report->active) !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.reports.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('script')
<script>
  var editor_js = CodeMirror.fromTextArea(document.getElementById("js"), {
    lineNumbers: true,
    matchBrackets: true,
    continueComments: "Enter",
    extraKeys: {"Ctrl-Q": "toggleComment"}
  });
</script>
@stop
