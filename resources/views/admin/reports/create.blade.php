@extends('backpack::layout')

@section('header')
    @include('admin.reports.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Report</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.reports.store' )) !!}
                    {!! Former::xlarge_text('name','Name')->required()->id('name') !!}
                    {!! Former::xlarge_text('slug','Slug')->required()->class('form-control slug') !!}
                    {!! Former::select('template','Template')->options($templates) !!}
                    {!! Former::select('type','Type')->options($types) !!}
                    {!! Former::select('format','Format')->options($format) !!}
                    {!! Former::xlarge_text('limit','Limit')->required() !!}
                    {!! Former::select('interval','Interval')->options($intervals) !!}
                    {!! Former::select('language_id','Language')->options($languages) !!}
                    {!! Former::textarea('js','JS')->id('js') !!}
                    {!! Former::select('active','Action')->options($active) !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.reports.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('script')
<script>
  var editor_js = CodeMirror.fromTextArea(document.getElementById("js"), {
    lineNumbers: true,
    matchBrackets: true,
    continueComments: "Enter",
    extraKeys: {"Ctrl-Q": "toggleComment"}
  });
</script>
@stop
