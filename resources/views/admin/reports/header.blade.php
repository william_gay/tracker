<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-bookmark"></i>
        <a href="/admin/reports">Reports</a>
        <a href="{!! URL::route('admin.reports.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Report">
            <i class="fa fa-plus"></i>
            New Report
        </a>
    </h3>
  </div>
</div>
