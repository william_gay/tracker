@extends('backpack::layout')

@section('header')
    @include('admin.category-reports.header')
@stop
@section('help')
    <p class="lead">Category Reports</p>
    <p>
        Create category reports bro...
    </p>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Category Report</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.category-reports.store' )) !!}
                    {!! Former::xlarge_text('name','Name')->required()->id('name') !!}
                    {!! Former::xlarge_text('slug','Slug')->required()->class('slug') !!}
                    {!! Former::textarea('overview', 'Overview')->rows(7)->class('col-md-9') !!}
                    {!! Former::select('category_id','Category')->options([0 => '-- Select One --'] + $categories)->id('category_id') !!}
                    {!! Former::xlarge_text('start_date', 'Start Date')->class('monitor-date') !!}
                    {!! Former::xlarge_text('end_date', 'End Date')->class('monitor-date') !!}
                    {!! Former::xlarge_text('program_ids', 'Programs')->class('col-md-6')->id('program_ids') !!}
                    {!! Former::xlarge_text('spot_ids', 'Spots')->class('col-md-6')->id('spot_ids') !!}
                    {!! Former::select('language_id','Language')->options($languages) !!}
                    {!! Former::select('enabled', 'Publicly Viewable')->options($status)->help('Disabled will only show to administrators.') !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.category-reports.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
