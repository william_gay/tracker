<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-bookmark"></i>
        <a href="/admin/category-reports">Category Reports</a>
        <a href="{!! URL::route('admin.category-reports.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Category Report">
            <i class="fa fa-plus"></i>
            New Category Report
        </a>
    </h3>
    </div>
</div>
