@extends('backpack::layout')

@section('header')
    <h3>
    	Not Found
    </h3>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
	<div class="row">
        <div class="col-md-12">
        <h1>404</h1>
        </div>
    </div>
   </div>
</div>
@stop
