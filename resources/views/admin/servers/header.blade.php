<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-life-ring"></i>
        <a href="/admin/servers">Servers</a>
        <a href="{!! URL::route('admin.servers.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Day Part">
            <i class="fa fa-plus"></i>
            New Server
        </a>
    </h3>
  </div>
</div>
