@extends('backpack::layout')

@section('header')
    @include('admin.servers.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Edit Server</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.servers.update', array($server->id)), 'PUT') !!}
                    {!! Former::xlarge_text('name','Name', $server->name)->required() !!}
                    {!! Former::xlarge_text('ip','IP', $server->ip)->required() !!}
                    {!! Former::xlarge_text('mac','MAC', $server->mac)->required() !!}
                    {!! Former::xlarge_text('mobo','MOBO', $server->mobo)->required() !!}
                    {!! Former::xlarge_text('processor','Processor', $server->processor)->required() !!}
                    {!! Former::xlarge_text('watts','Watts', $server->watts)->required() !!}
                    {!! Former::xlarge_text('ram','RAM', $server->ram)->required() !!}
                    {!! Former::xlarge_text('location','Location', $server->location)->required() !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.servers.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
