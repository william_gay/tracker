@extends('backpack::layout')

@section('header')
    @include('admin.servers.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Server</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.servers.store' )) !!}
                    {!! Former::xlarge_text('name','Name')->required() !!}
                    {!! Former::xlarge_text('ip','IP')->required() !!}
                    {!! Former::xlarge_text('mac','MAC')->required() !!}
                    {!! Former::xlarge_text('mobo','MOBO')->required() !!}
                    {!! Former::xlarge_text('processor','Processor')->required() !!}
                    {!! Former::xlarge_text('watts','Watts')->required() !!}
                    {!! Former::xlarge_text('ram','RAM')->required() !!}
                    {!! Former::xlarge_text('location','Location')->required() !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.servers.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
