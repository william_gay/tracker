@extends('backpack::layout')

@section('header')
    @include('admin.servers.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Servers ({!! $servers->count() !!})</p>
                <div class="block-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>IP</th>
                                <th>MOBO</th>
                                <th>PROC</th>
                                <th>WATTS</th>
                                <th>RAM</th>
                                <th>LOCO</th>
                                <th><i class="icon-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($servers as $server)
                                <tr>
                                    <td>{!! $server->id !!}</td>
                                    <td>{!! $server->name !!}</td>
                                    <td>{!! $server->ip !!}</td>
                                    <td>{!! $server->mobo !!}</td>
                                    <td>{!! $server->processor !!}</td>
                                    <td>{!! $server->watts !!}</td>
                                    <td>{!! $server->ram !!}</td>
                                    <td>{!! $server->location !!}</td>
                                    <td>
                                    <a href="{!! route('admin.servers.edit', array($server->id)) !!}" class="btn btn-default btn-sm">
                                        <i class="fa fa-pencil-square-o"></i>
                                    </a>
                                    <a href="{!! route('admin.servers.destroy', array($server->id)) !!}"
                                       data-method="delete"
                                       data-modal-text="Delete this Server?"
                                       class="btn btn-sm btn-danger">
                                       <i class="fa fa-trash-o"></i>
                                    </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="9">
                                    {!! $servers->links() !!}
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
