@extends('backpack::layout')

@section('header')
    @include('admin.channels.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Edit Channel</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.channels.update', array($channel->id)), 'PUT' )->enctype('multipart/form-data') !!}
                    {!! Former::xlarge_text('abbr','Abbr',$channel->abbr)->required() !!}
                    {!! Former::xlarge_text('name','Name',$channel->name)->required() !!}
                    {!! Former::xlarge_text('currency','Currency', $channel->currency)->required() !!}
                    {!! Former::xlarge_text('hhs','HHS', $channel->hhs) !!}
                    {!! Former::xlarge_text('receiver', 'Receiver', $channel->receiver) !!}
                    {!! Former::xlarge_text('receiver_serial', 'Receiver Serial', $channel->receiver_serial) !!}
                    {!! Former::select('server_id', 'Server')->options(array('' => 'Select One') + $servers, $channel->server_id) !!}
                    {!! Former::xlarge_text('server_device', 'Server Device', $channel->server_device) !!}
                    {!! Former::xlarge_text('carrier', 'Carrier', $channel->carrier) !!}
                    {!! Former::xlarge_text('station', 'Station', $channel->station) !!}
                    {!! Former::select('info_active','Infomercial Status')->options($status,$channel->info_active) !!}
                    {!! Former::select('spot_active','Spot Status')->options($status, $channel->spot_active) !!}
                    {!! Former::select('capturing', 'Capturing')->options($status, $channel->capturing) !!}
                    {!! Former::select('autodetect', 'Auto Detection')->options($status, $channel->autodetect) !!}
                    {!! Former::select('pi', 'PI')->options($status, $channel->pi) !!}
                    {!! Former::select('language_id','Language')->options($languages, $channel->language_id) !!}
                    @if ($channel->logo_location != null)
                        <img src="{!! "https://s3-us-west-2.amazonaws.com/ims-logos/".$channel->logo_location !!}" />
                    @endif
                    {!! Former::file('channel_logo','Logo')->accept('image') !!}
                    {!! Former::xlarge_text('logical_dish','Logical Dish',$channel->logical_dish) !!}
                    {!! Former::xlarge_text('logical_direct','Logical Direct TV',$channel->logical_direct) !!}
                    {!! Former::xlarge_text('logical_fios','Logical Fios',$channel->logical_fios) !!}
                    {!! Former::xlarge_text('logical_comcast','Logical Comcast',$channel->logical_comcast) !!}
                    {!! Former::xlarge_text('broadcast_channel','Broadcast Channel',$channel->broadcast_channel) !!}
                    {!! Former::xlarge_text('xmlid','XMLID',$channel->xmlid) !!}
                    {!! Former::xlarge_text('call_sign','Call Sign',$channel->call_sign) !!}
                    {!! Former::xlarge_text('paid_programming_start','Paid Programming Start *',$channel->paid_programming_start)->class('timepicker') !!}
                    {!! Former::xlarge_text('paid_programming_end','Paid Programming End *',$channel->paid_programming_end)->class('timepicker') !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.channels.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('script')
<script>
$(function() {
    $('.timepicker').datetimepicker({
        format: 'HH:mm:ss'
    });
});
</script>
@stop
