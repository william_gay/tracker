@extends('backpack::layout')

@section('header')
    @include('admin.channels.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Channel</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.channels.store' )) !!}
                    {!! Former::xlarge_text('abbr','Abbr')->required() !!}
                    {!! Former::xlarge_text('name','Name')->required() !!}
                    {!! Former::xlarge_text('currency','Currency')->required() !!}
                    {!! Former::xlarge_text('hhs','HHS') !!}
                    {!! Former::xlarge_text('receiver', 'Receiver') !!}
                    {!! Former::xlarge_text('reciever_serial', 'Receiver Serial') !!}
                    {!! Former::select('server_id', 'Server')->options(array('' => 'Select One') + $servers, 0) !!}
                    {!! Former::xlarge_text('server_device', 'Server Device') !!}
                    {!! Former::xlarge_text('carrier', 'Carrier') !!}
                    {!! Former::xlarge_text('station', 'Station') !!}
                    {!! Former::select('info_active','Infomercial Status')->options($status,0) !!}
                    {!! Former::select('spot_active','Spot Status')->options($status,0) !!}
                    {!! Former::select('capturing', 'Capturing')->options($status, 0) !!}
                    {!! Former::select('autodetect', 'Auto Detection')->options($status, 0) !!}
                    {!! Former::select('pi', 'PI')->options($status, 0) !!}
                    {!! Former::select('language_id','Language')->options($languages,1) !!}
                    {!! Former::file('channel_logo','Logo')->accept('image') !!}
                    {!! Former::xlarge_text('logical_dish','Logical Dish') !!}
                    {!! Former::xlarge_text('logical_direct','Logical Direct TV') !!}
                    {!! Former::xlarge_text('logical_fios','Logical Fios') !!}
                    {!! Former::xlarge_text('logical_comcast','Logical Comcast') !!}
                    {!! Former::xlarge_text('broadcast_channel','Broadcast Channel') !!}
                    {!! Former::xlarge_text('xmlid','XMLID') !!}
                    {!! Former::xlarge_text('call_sign','Call Sign') !!}
                    {!! Former::xlarge_text('paid_programming_start','Paid Programming Start *')->class('timepicker') !!}
                    {!! Former::xlarge_text('paid_programming_end','Paid Programming End *')->class('timepicker') !!}

                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.companies.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@section('script')
<script>
$(function() {
    $('.timepicker').datetimepicker({
        format: 'HH:mm:ss'
    });
});
</script>
@stop
