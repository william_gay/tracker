<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-video-camera"></i>
        <a href="{!! URL::route('admin.channels.index') !!}">Channels</a>
        <a href="{!! URL::route('admin.channels.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Group">
            <i class="fa fa-plus"></i>
            New Channel
        </a>
    </h3>
</div>
