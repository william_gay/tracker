@extends('backpack::layout')

@section('header')
    @include('admin.companies.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Company</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.companies.store' ))->enctype('multipart/form-data') !!}
                    {!! Former::file('company_logo','Logo')->accept('image') !!}
                    {!! Former::xlarge_text('name','Name')->required() !!}
                    {!! Former::xlarge_text('address','Address') !!}
                    {!! Former::xlarge_text('address_extra','Address Extra') !!}
                    {!! Former::xlarge_text('city', 'City') !!}
                    {!! Former::xlarge_text('state', 'State')->help('Ex: PA') !!}
                    {!! Former::xlarge_text('zip', 'Zip') !!}
                    {!! Former::xlarge_text('phone', 'Phone') !!}
                    {!! Former::xlarge_text('fax','Fax') !!}
                    {!! Former::xlarge_text('agency','Agency') !!}
                    {!! Former::xlarge_text('Contact','Contact') !!}
                    {!! Former::xlarge_text('title','Title') !!}
                    {!! Former::xlarge_text('email','E-Mail') !!}
                    {!! Former::xlarge_text('website','Website') !!}
                    {!! Former::textarea('remarks','Remarks') !!}

                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.companies.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
