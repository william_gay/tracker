<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-globe"></i>
        Companies
        <a href="{!! URL::route('admin.companies.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Company">
            <i class="fa fa-plus"></i>
            New Company
        </a>
    </h3>
    </div>
</div>
