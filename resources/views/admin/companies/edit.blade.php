@extends('backpack::layout')

@section('header')
    @include('admin.companies.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Edit Company</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.companies.update', array($company->id)), 'PUT' )->enctype('multipart/form-data') !!}
                    <input type="hidden" name="id" value="{!! $company->id !!}" />
                    @if ($company->logo_location != null)
                        <img src="{!! "https://s3-us-west-2.amazonaws.com/ims-logos/".$company->logo_location !!}" />
                    @endif
                    {!! Former::file('company_logo','Logo')->accept('image') !!}
                    {!! Former::xlarge_text('name','Name', $company->name)->required() !!}
                    {!! Former::xlarge_text('address','Address', $company->address) !!}
                    {!! Former::xlarge_text('address_extra','Address Extra', $company->address_extra) !!}
                    {!! Former::xlarge_text('city', 'City', $company->city) !!}
                    {!! Former::xlarge_text('state', 'State', $company->state)->help('Ex: PA') !!}
                    {!! Former::xlarge_text('zip', 'Zip', $company->zip) !!}
                    {!! Former::xlarge_text('phone', 'Phone', $company->phone) !!}
                    {!! Former::xlarge_text('fax','Fax', $company->fax) !!}
                    {!! Former::xlarge_text('agency','Agency', $company->agency) !!}
                    {!! Former::xlarge_text('contact','Contact', $company->contact) !!}
                    {!! Former::xlarge_text('title','Title', $company->title) !!}
                    {!! Former::xlarge_text('email','E-Mail', $company->email) !!}
                    {!! Former::xlarge_text('website','Website', $company->website) !!}
                    {!! Former::textarea('remarks','Remarks', $company->remarks) !!}

                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.channels.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
            </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
