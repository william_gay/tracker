@extends('backpack::layout')

@section('header')
    <h3>
        <i class="fa fa-bookmark"></i>
        <a href="/admin/products">Products &bull; Retailer Products &bull; {!! $product->name !!}</a>
    </h3>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12" style="padding-top:10px;">
        @foreach($retailer_products as $rp)
            <table class="table table-striped retail-product">
                <tr>
                    <td class="col-md-4">{!! Former::xlarge_text('retailer', '', $rp->retailer->name)->disabled() !!}</td>
                    <td class="col-md-2">{!! Former::xlarge_text('max_price', '', $rp->min_price)->placeholder('Min Price') !!}</td>
                    <td class="col-md-2">{!! Former::xlarge_text('max_price', '', $rp->max_price)->placeholder('Max Price') !!}</td>
                    <td class="col-md-4">{!! Former::xlarge_text('website', '', $rp->website)->placeholder('Website') !!}</td>
                </tr>
                <tr>
                    <td class="col-md-12" colspan="4">
                        {!! Former::textarea('general_configuration', '', $rp->general_configuration)->rows(3)->placeholder('Configuration') !!}
                    </td>
                </tr>
                <tr>
                    <td class="col-md-12" colspan="4">
                        {!! Former::textarea('attributes', '', $rp->attributes)->rows(3)->placeholder('Attributes') !!}
                    </td>
                </tr>
            </table>
            <p class="text-center">&bull;</p>
        @endforeach

            <table class="table table-striped retail-product">
                <tr>
                    <td class="col-md-4">{!! Former::xlarge_text('retailer_id', '') !!}</td>
                    <td class="col-md-2">{!! Former::xlarge_text('min_price', '')->placeholder('Min Price') !!}</td>
                    <td class="col-md-2">{!! Former::xlarge_text('max_price', '')->placeholder('Max Price') !!}</td>
                    <td class="col-md-4">{!! Former::xlarge_text('website', '')->placeholder('Website') !!}</td>
                </tr>
                <tr>
                    <td class="col-md-12" colspan="4">
                        {!! Former::textarea('general_configuration', '')->rows(3)->placeholder('Configuration') !!}
                    </td>
                </tr>
                <tr>
                    <td class="col-md-12" colspan="4">
                        {!! Former::textarea('attributes', '')->rows(3)->placeholder('Attributes') !!}
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
</div>
@stop

@section('script')

@stop
