@extends('backpack::layout')

@section('header')
    @include('admin.products.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Edit Product {!! $product->name !!}</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.products.update', array($product->id)), 'PUT')->enctype('multipart/form-data') !!}
                    {!! Former::xlarge_text('name','Name', $product->name)->required()->id('name') !!}
                    {!! Former::xlarge_text('slug','Slug', $product->slug)->required()->class('form-control slug') !!}
                    {!! Former::select('category_id','Category')->options(array(0 => '-- Select One --') + $categories, $product->category_id)->id('category_id')->required() !!}
                    {!! Former::select('sub_category_id','Sub Category')->options(array(0=>'-- Select One --') + $subcats, $product->sub_category_id)->id('sub_category_id')->required() !!}
                    {!! Former::xlarge_text('winmo_company_id', 'Winmo Company ID', $product->winmo_company_id) !!}
                    {!! Former::xlarge_text('winmo_brand_id', 'Winmo Brand ID', $product->winmo_brand_id) !!}
                    {!! Former::checkbox('spyd', 'Spyd')->check(array('spyd'=>$product->spyd))->help('Added to Price2Spy') !!}
                    {!! Former::xlarge_text('asin', 'ASIN', $product->asin) !!}
                    {!! Former::xlarge_text('gtin', 'GTIN', $product->gtin) !!}
                    {!! Former::xlarge_text('sku', 'SKU', $product->sku) !!}
                    {!! Former::xlarge_text('model_number', 'Model Number', $product->model_number) !!}
                    {!! Former::xlarge_text('manufacturer_id', 'Manufacturer', $product->manufacturer_number) !!}
                    {!! Former::xlarge_text('website','Website', $product->website) !!}
                    {!! Former::textarea('description', 'Description', $product->description)->rows(12)->class('col-md-10') !!}
                    {!! Former::xlarge_text('program_ids', 'Programs', implode(',',$product->programs->pluck('id')->toArray()))->class('col-md-6')->id('program_ids') !!}
                    {!! Former::xlarge_text('spot_ids', 'Spots', implode(',',$product->spots->pluck('id')->toArray()))->class('col-md-6')->id('spot_ids') !!}
                    <div class="form-group">
                        <label for="retailer_products" class="control-label col-lg-2 col-sm-4">Retailer Products</label>
                        <div class="col-lg-10 col-sm-8">
                            <a href="{!! route('admin.products.retailer', $product->id) !!}" class="btn btn-primary btn-sm">Manage Retailer Products</a><br/>
                            Total Count: {!! $product->retailerProducts->count() !!}
                        </div>
                    </div>
                    @if ($product->logo_location != null)
                        <img src="https://{!! $product->logo_location !!}" />
                    @endif
                    {!! Former::file('product_logo','Logo')->accept('image') !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.products.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
