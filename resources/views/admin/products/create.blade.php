@extends('backpack::layout')

@section('header')
    @include('admin.products.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Product</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.products.store' ))->enctype('multipart/form-data') !!}
                    {!! Former::xlarge_text('name','Name')->required()->id('name') !!}
                    {!! Former::xlarge_text('slug','Slug')->required()->class('form-control slug') !!}
                    {!! Former::select('category_id','Category')->options(array(0 => '-- Select One --') + $categories)->id('category_id')->required() !!}
                    {!! Former::select('sub_category_id','Sub Category')->options(array(0=>'-- Select One --'))->id('sub_category_id')->required() !!}
                    {!! Former::xlarge_text('winmo_company_id', 'Winmo Company ID') !!}
                    {!! Former::xlarge_text('winmo_brand_id', 'Winmo Brand ID') !!}
                    {!! Former::checkbox('spyd', 'Spyd')->help('Added to Price2Spy') !!}
                    {!! Former::xlarge_text('asin', 'ASIN') !!}
                    {!! Former::xlarge_text('gtin', 'GTIN') !!}
                    {!! Former::xlarge_text('sku', 'SKU') !!}
                    {!! Former::xlarge_text('model_number', 'Model Number') !!}
                    {!! Former::xlarge_text('manufacturer_id', 'Manufacturer') !!}
                    {!! Former::xlarge_text('website','Website') !!}
                    {!! Former::textarea('description', 'Description')->rows(12)->class('col-md-10') !!}
                    {!! Former::xlarge_text('program_ids', 'Programs')->class('col-md-6')->id('program_ids') !!}
                    {!! Former::xlarge_text('spot_ids', 'Spots')->class('col-md-6')->id('spot_ids') !!}
                    <div class="form-group">
                        <label for="retailer_products">Retailer Products</label>
                        <small>Add after product is created.</small>
                    </div>
                    {!! Former::file('product_logo','Logo')->accept('image') !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.products.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
