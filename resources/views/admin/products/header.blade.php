<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-bookmark"></i>
        <a href="/admin/products">Products</a>
        <a href="{!! URL::route('admin.products.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Product">
            <i class="fa fa-plus"></i>
            New Product
        </a>
    </h3>
</div>
</div>
