@extends('backpack::layout')

@section('header')
    @include('admin.spotcosts.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <h4><img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $channel->logo_location !!}" style="height: 50px;" /> {!! $channel->name .' - '. $channel->abbr !!} <small>Spot Costs</small></h4>
        </div>
        <div class="col-md-12">
            {!! Former::horizontal_open(route('admin.spotcosts.add'), 'POST')->id('addCost') !!}
                {!! Former::hidden('channel_id', $channel->id)->id('hidden_channel_id') !!}
                <input name="channel_id" type="hidden" value="{!! $channel->id !!}" />
                {!! Former::xlarge_text('cost_date', 'Cost Date', $cost_date)->id('cost_date')->help('The most recent cost data will be applied to detection.') !!}
                {!! Former::select('daypart_id','Daypart')->options($daypart_select)->id('daypart_id')->class('col-md-4') !!}
                {!! Former::xlarge_text('amount','Cost')->required()->id('amount')->class('numbersOnly')->help('IE: 2500') !!}
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save', 'id' => 'saveCostBtn')) !!}
                    </div>
                </div>
            {!! Former::close() !!}
        </div>
        <div class="col-md-12">
            <table class="table table-striped" id="costTbl">
                <thead>
                    <th class="col-md-1"></th>
                    <th>Channel</th>
                    <th>Daypart</th>
                    <th>Time Slot</th>
                    <th>Cost</th>
                    <th>Created Date</th>
                </thead>
            </table>
        </div>
    </div>
    </div>
</div>
@stop

@section('script')
<script src="{{ asset("assets/js/admin.spotcosts.js") }}"></script>

@endsection
