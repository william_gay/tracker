@extends('backpack::layout')

@section('header')
    <h3>
        <i class="fa fa-bookmark"></i>
        <a href="/admin/retailers">Retailer Importer</a>
        <a href="{!! URL::route('admin.retailers.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Retailer">
            <i class="fa fa-plus"></i>
            New Retailers
        </a>
    </h3>
@stop
@section('help')
    <p class="lead">Retailers</p>
    <p>
        Create Retailers bro...
    </p>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12" style="padding-top:10px;">
            {!! Former::horizontal_open( route('admin.retailers.processImport' ))->enctype('multipart/form-data') !!}
                {!! Former::file('csv','CSV')->accept('file') !!}
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Import</button>
                    </div>
                </div>
            {!! Former::close() !!}
        </div>
    </div>
</div>
</div>
@stop

@section('script')

@stop
