@extends('backpack::layout')

@section('header')
    @include('admin.retailers.header')
@stop
@section('help')
    <p class="lead">Retailers</p>
    <p>
        Create Retailers bro...
    </p>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12" style="padding-top:10px;">
            {!! $html->table() !!}
        </div>
    </div>
</div>
</div>
@stop

@section('script')
{!! $html->scripts() !!}
@stop
