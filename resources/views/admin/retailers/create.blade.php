@extends('backpack::layout')

@section('header')
    @include('admin.retailers.header')
@stop
@section('help')
    <p class="lead">Retailers</p>
    <p>
        Create retailers bro...
    </p>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Retailer</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.retailers.store' ))->enctype('multipart/form-data') !!}
                    {!! Former::xlarge_text('name','Name')->required()->id('name') !!}
                    {!! Former::xlarge_text('slug','Slug')->required()->class('form-control slug') !!}
                    {!! Former::xlarge_text('website','Website')->required()->class('website') !!}
                    {!! Former::xlarge_text('sqft', __('retailers.sqft'))->required()->class('reach') !!}
                    {!! Former::xlarge_text('store_count', __('retailers.store_count'))->required() !!}
                    {!! Former::textarea('notes', 'Notes')->rows(5)->class('col-md-10') !!}
                    {!! Former::checkbox('visible','Visible')->text('Show this retailer on the frontend.') !!}
                    {!! Former::file('retailer_logo','Logo')->accept('image') !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.retailers.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
