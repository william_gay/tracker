<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-bookmark"></i>
        <a href="/admin/retailers">Retailers</a>
        <a href="{!! URL::route('admin.retailers.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New Retailer">
            <i class="fa fa-plus"></i>
            New Retailer
        </a>
    </h3>
  </div>
</div>
