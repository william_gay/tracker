@extends('backpack::layout')

@section('header')
    @include('admin.programs.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Program</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.programs.store' )) !!}
                    {!! Former::xlarge_text('grid_title','Grid Title')->required() !!}
                    {!! Former::xlarge_text('title','Title')->required() !!}
                    {!! Former::xlarge_text('version','Version')->required() !!}
                    {!! Former::select('language_id','Language')->options($languages)->required() !!}
                    {!! Former::xlarge_text('program_id','Master Program')->class('col-md-6')->help('Leave empty for New Program.') !!}
                    {!! Former::select('category_id','Category')->options(array(0 => '-- Select One --') + $categories)->id('category_id')->required() !!}
                    {!! Former::select('sub_category_id','Sub Category')->options(array(0=>'-- Select One --'))->id('sub_category_id')->required() !!}
                    {!! Former::select('channel_id','Initial Channel')->options($channels)->class('select2') !!}
                    <div class="form-group">
                        <label for="marketing_company_id" class="control-label col-lg-2 col-sm-4">{!! __('programs.marketing_company') !!}</label>
                        <div class="col-sm-10">
                            <input id="marketing_company_id" type="hidden" name="marketing_company_id" style="width: 250px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="production_company_id" class="control-label col-lg-2 col-sm-4">{!! __('programs.production_company') !!}</label>
                        <div class="col-sm-10">
                            <input id="production_company_id" type="hidden" name="production_company_id" style="width: 250px;">
                        </div>
                    </div>
                    {!! Former::xlarge_text('initial_date', 'Initial Date') !!}
                    {!! Former::xlarge_text('monitor_date', 'Monitor Date')->id('monitor_date') !!}
                    {!! Former::xlarge_text('product_name', 'Product Name') !!}
                    {!! Former::xlarge_text('price', 'Price')->help('DR Offer') !!}
                    {!! Former::xlarge_text('currency', 'Currency', '$')->required() !!}
                    {!! Former::xlarge_text('cost', 'Cost')->help('Actual price, one time price or payments added up. Do not include shipping use lowest price.') !!}
                    {!! Former::xlarge_text('shipping_cost', 'Shipping Cost')->help('Shipping price number only') !!}
                    {!! Former::xlarge_text('host', 'Host') !!}
                    {!! Former::xlarge_text('host_extra', 'Host Extra') !!}
                    {!! Former::textarea('description', 'Description')->rows(12)->class('col-md-10') !!}
                    {!! Former::textarea('summary', 'Summary')->rows(12)->class('col-md-10') !!}
                    {!! Former::textarea('remarks', 'Remarks')->rows(7)->class('col-md-10') !!}
                    {!! Former::textarea('upsell', 'Upsell')->rows(7)->class('col-md-10') !!}
                    {!! Former::checkbox('flv','FLV')->text('FLV Exists?') !!}
                    {!! Former::checkbox('mp4','MP4')->text('MP4 Exists?') !!}
                    {!! Former::checkbox('poster','Poster')->check(array('poster'=>0))->text('Poster Exists?') !!}
                    {!! Former::textarea('keywords', 'Keywords') !!}
                    {!! Former::xlarge_text('order_name', 'Order Name') !!}
                    {!! Former::xlarge_text('order_address', 'Order Address') !!}
                    {!! Former::xlarge_text('order_address_extra', 'Order Address Extra') !!}
                    {!! Former::xlarge_text('order_city', 'Order City') !!}
                    {!! Former::xlarge_text('order_state', 'Order State') !!}
                    {!! Former::xlarge_text('order_zip', 'Order Zip') !!}
                    {!! Former::xlarge_text('order_phone', 'Order Phone') !!}
                    {!! Former::xlarge_text('website', 'Website') !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.programs.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
