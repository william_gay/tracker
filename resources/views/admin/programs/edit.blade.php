@extends('backpack::layout')

@section('header')
    @include('admin.programs.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Edit Program
                    <span class="pull-right" style="margin-right: 10px;">Last Updated By: {!! $program->user->email ?? 'N/A' !!}</span>
                </p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.programs.update', array($program->id)), 'PUT' ) !!}
                    {!! Former::xlarge_text('grid_title','Grid Title',$program->grid_title)->required() !!}
                    {!! Former::xlarge_text('title','Title',$program->title)->required() !!}
                    {!! Former::xlarge_text('version','Version',$program->version)->required() !!}
                    {!! Former::select('language_id','Language')->options($languages,$program->language_id)->required() !!}
                    {!! Former::xlarge_text('program_id','Master Program',$program->program_id)->class('col-md-6') !!}
                    {!! Former::select('category_id','Category')->options(array(0=>'-- Select One --') + $categories,$program->category_id)->id('category_id')->required() !!}
                    {!! Former::select('sub_category_id','Sub Category')->options(array(0=>'-- Select One --') + $subcats,$program->sub_category_id)->id('sub_category_id')->required() !!}
                    {!! Former::select('channel_id','Initial Channel')->options($channels,$program->channel_id)->class('select2') !!}
                    <div class="form-group">
                        <label for="marketing_company_id" class="control-label col-lg-2 col-sm-4">{!! __('programs.marketing_company') !!}</label>
                        <div class="col-sm-10">
                            <input id="marketing_company_id" type="hidden" name="marketing_company_id" style="width: 250px;" value="{!! $program->marketing_company_id !!}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="production_company_id" class="control-label col-lg-2 col-sm-4">{!! __('programs.production_company') !!}</label>
                        <div class="col-sm-10">
                            <input id="production_company_id" type="hidden" name="production_company_id" style="width: 250px;" value="{!! $program->production_company_id !!}">
                        </div>
                    </div>
                    {!! Former::xlarge_text('initial_date', 'Initial Date',$program->initial_date) !!}
                    {!! Former::xlarge_text('monitor_date', 'Monitor Date',$program->monitor_date->format('Y-m-d'))->id('monitor_date') !!}
                    {!! Former::xlarge_text('product_name', 'Product Name',$program->product_name) !!}
                    {!! Former::xlarge_text('price', 'Price',$program->price)->help('DR Offer') !!}
                    {!! Former::xlarge_text('currency', 'Currency', $program->currency)->required() !!}
                    {!! Former::xlarge_text('cost', 'Cost',$program->cost)->help('Actual price, one time price or payments added up. Do not include shipping use lowest price.') !!}
                    {!! Former::xlarge_text('shipping_cost', 'Shipping Cost',$program->shipping_cost)->help('Shipping price number only') !!}
                    {!! Former::xlarge_text('host', 'Host',$program->host) !!}
                    {!! Former::xlarge_text('host_extra', 'Host Extra',$program->host_extra) !!}
                    {!! Former::textarea('description', 'Description',$program->description)->rows(12)->class('col-md-10') !!}
                    {!! Former::textarea('summary', 'Summary',$program->summary)->rows(12)->class('col-md-10') !!}
                    {!! Former::textarea('remarks', 'Remarks',$program->remarks)->rows(7)->class('col-md-10') !!}
                    {!! Former::textarea('upsell', 'Upsell',$program->upsell)->rows(7)->class('col-md-10') !!}
                    {!! Former::textarea('keywords', 'Keywords',$program->keywords) !!}
                    {!! Former::select('file_name','File Name')->options(array(0=>'-- Select One --') + $videos,$program->file_name)->id('file_name')->class('col-md-6') !!}
                    {!! Former::checkbox('flv','FLV')->check(array('flv'=>$program->flv))->text('FLV Exists?') !!}
                    {!! Former::checkbox('mp4','MP4')->check(array('mp4'=>$program->mp4))->text('MP4 Exists?') !!}
                    {!! Former::checkbox('poster','Poster')->check(array('poster'=>$program->poster))->text('Poster Exists?') !!}
                    {!! Former::xlarge_text('order_name', 'Order Name',$program->order_name) !!}
                    {!! Former::xlarge_text('order_address', 'Order Address',$program->address) !!}
                    {!! Former::xlarge_text('order_address_extra', 'Order Address Extra',$program->order_address_extra) !!}
                    {!! Former::xlarge_text('order_city', 'Order City',$program->order_city) !!}
                    {!! Former::xlarge_text('order_state', 'Order State',$program->order_state) !!}
                    {!! Former::xlarge_text('order_zip', 'Order Zip',$program->order_zip) !!}
                    {!! Former::xlarge_text('order_phone', 'Order Phone',$program->order_phone) !!}
                    {!! Former::xlarge_text('website', 'Website',$program->website) !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.programs.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
