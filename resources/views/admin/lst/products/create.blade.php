@extends('backpack::layout')

@section('header')
    @include('admin.lst.products.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Product</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.lst.products.store'))->enctype('multipart/form-data') !!}
                    {!! Former::xlarge_text('name','Name')->required() !!}
                    {!! Former::xlarge_text('msrp','MSRP')->required() !!}
                    {!! Former::xlarge_text('sku', 'SKU')->required() !!}
                    {!! Former::xlarge_text('website','Website') !!}
                    {!! Former::select('live_shopping_category_id','Category')->options(['' => 'Select One'] + $categories)->required() !!}
                    {!! Former::select('live_shopping_sub_category_id','Sub-Category')->options(['' => 'Select One']) !!}
                    {!! Former::file('product_img','Image')->accept('image') !!}
                    {!! Former::xlarge_text('first_airdate','First Aired', null, ['class' => 'first_airdate'])->required() !!}
                    {!! Former::textarea('notes','Notes')->rows(3) !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.lst.products.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
