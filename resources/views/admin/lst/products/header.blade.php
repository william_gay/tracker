<div class="box box-default">
  <div class="box-header with-border">
    <h3>
        <i class="fa fa-sitemap"></i>
        <a href="/admin/lst/products">Live Shopping Products</a>
        <a href="{!! URL::route('admin.lst.products.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Add New Product">
            <i class="fa fa-plus"></i>
            New Product
        </a>
    </h3>
</div>
</div>
