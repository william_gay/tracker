@extends('backpack::layout')

@section('header')
    @include('admin.lst.products.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Edit Product</p>
                <div class="block-body">
                    <!-- {!! Former::horizontal_open( route('admin.lst.products.update', array($product->id)), 'PUT' ) !!} -->
                    {!! Former::horizontal_open( route('admin.lst.products.update', array($product->id)), 'PUT' )->enctype('multipart/form-data') !!}
                        {!! Former::xlarge_text('name','Name', $product->name)->required() !!}
                        {!! Former::xlarge_text('msrp','MSRP', $product->msrp)->required() !!}
                        {!! Former::xlarge_text('sku','SKU', $product->sku)->required() !!}
                        {!! Former::xlarge_text('website','Website', $product->website) !!}
                        {!! Former::select('live_shopping_category_id','Category')->options(['' => 'Select One'] + $categories, $product->live_shopping_category_id) !!}
                        {!! Former::select('live_shopping_sub_category_id','Sub-Category')->options(['' => 'Select One'] + $subCategories, $product->live_shopping_sub_category_id) !!}
                        @if ($product->img_location != null)
                            <img src="https://s3-us-west-2.amazonaws.com/ims-lst-products/{!! $product->img_location !!}" style="max-width:100%;" />
                        @endif
                        {!! Former::file('product_img','Image')->accept('image') !!}
                        {!! Former::xlarge_text('first_airdate','First Aired', $product->first_airdate->format("Y-m-d h:i:s A"), ['class' => 'first_airdate']) !!}
                        {!! Former::textarea('notes','Notes', $product->notes)->rows(3) !!}
                        <div class="form-group">
                            <div class="form-actions col-lg-4 col-lg-offset-2 col-sm-4">
                                {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                                <a href="{!! URL::route('admin.lst.shows.index') !!}" class="btn btn-large">Cancel</a>
                                <a href="{!! route("admin.lst.products.destroy", array($product->id)) !!}" data-method="delete" data-modal-text="Delete this Product? All product versions will be deleted as well!" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> Delete Product and Versions</a>
                            </div>
                        </div>
                    {!! Former::close() !!}
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="text-center">Available Versions ({!! $product->versions->count() !!}): {!! Form::select('version', ['add' => 'New Version'] + $product->versions()->pluck('name', 'id')->toArray(), $version_id, ['id' => 'version']) !!} {!! Form::button('Go', ['class' => 'btn btn-sm', 'id' => 'btn-version']) !!}</h4>
                            <div id="alert-message"></div>
                            <div id="product-version"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@stop

@section('script')
<script>
$(function() {
    @if ($version_id != 0)
        setTimeout(function(){
            $('#btn-version').trigger('click');
        }, 500);
    @endif

    $('#btn-version').on('click', function(e){
        e.preventDefault();
        $('#alert-message').empty();
        $version = $('#version').val();

        getForm($version);
    });

    $('#product-version').on('click', '#save-version', function(e){
        e.preventDefault();

        $formAction = $('#frm-product-version').attr('action');
        $formData = $('#frm-product-version').serialize();

        $.post($formAction, $formData, function(data) {
            html = '<div class="alert alert-'+data.result+' alert-dismissible" role="alert"> \
                    <button type="button" class="close" data-dismiss="alert"> \
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> \
                    '+data.message+' \
                    </div>';

            $('#alert-message').html(html);

            if ($formAction == '{!! route('admin.lst.product-versions.store') !!}') {
                //window.location.reload();

                //Update Dropdown list.
                $('#version').append("<option value='"+ data.version_id +"'>" + $('#frm-product-version input[name="name"]').val() + "</option>");
                $('#version').val(data.version_id);

                getForm(data.version_id);
            }
        });

    });
});

function getForm(version)
{
    if (version == 'add') {
        $.get('/admin/lst/product-versions/add', {'productId': {!! $product->id !!}}, function(data){
            //console.log(data);
            $('#product-version').empty();
            $('#product-version').html(data);
            $('.first_airdate').datetimepicker({
                sideBySide: true,
                format: "YYYY-MM-DD hh:mm A"
            });
        });
    } else {
        $.get('/admin/lst/product-versions/edit/'+version, {'productId': {!! $product->id !!}}, function(data){
            //console.log(data);
            $('#product-version').empty();
            $('#product-version').html(data);
            $('.first_airdate').datetimepicker({
                sideBySide: true,
                format: "YYYY-MM-DD hh:mm A"
            });
            laravel.initialize();
        });
    }
}
</script>
@stop
