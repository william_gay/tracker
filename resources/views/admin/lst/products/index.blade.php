@extends('backpack::layout')

@section('header')
    @include('admin.lst.products.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Products ({!! $products->count() !!})</p>
                <div class="block-body">
                    <table id="report" class="table table-hover table-bordered results datatable dataTable">
                        <thead>
                          <tr role="row">
                            <th class="sorting">Name</th>
                            <th>MSRP</th>
                            <th>SKU</th>
                            <th>Category</th>
                            <th>Sub-Category</th>
                            <th>Initial Date</th>
                            <th></th>
                          </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('script')
<script>
var oTable;
$(function(){
  oTable = $('#report').dataTable({
    "pageLength": 50,
    "order": [[ 5, "asc" ]],
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": "{!! route('admin.lst.products.datatables') !!}"
    },
    "columns": [
        {"data": "name", "name": "name", "title": "Name"},
        {"data": "msrp", "name": "msrp", "title": "msrp"},
        {"data": "sku", "name": "sku", "title": "sku"},
        {"data": "category_name", "name": "category_name", "title": "category"},
        {"data": "subcategory_name", "name": "subcategory_name", "title": "subcategory"},
        {"data": "first_airdate", "name": "first_airdate", "title": "first_airdate"},
        {"data": "id", "name": "id", "title": "actions"}
    ],
    "drawCallback": function( settings ) {
      laravel.initialize();
    }
  });
});
</script>
@stop
