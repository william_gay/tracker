<h3>
    <i class="fa fa-sitemap"></i>
    <a href="/admin/lst/shows">Live Shopping Shows</a>
    <a href="{!! URL::route('admin.lst.shows.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Add New Show">
        <i class="fa fa-plus"></i>
        New Show
    </a>
</h3>