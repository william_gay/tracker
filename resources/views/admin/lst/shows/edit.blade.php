@extends('backpack::layout')

@section('header')
    @include('admin.lst.shows.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Edit Show</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.lst.shows.update', array($show->id)), 'PUT' ) !!}
                    {!! Former::xlarge_text('title','Title', $show->title)->required() !!}
                    {!! Former::xlarge_text('first_airdate','Firs Aired', $show->first_airdate->format('m/d/Y g:i:s A'))->class('form-control datetimepicker')->required() !!}
                    {!! Former::xlarge_text('duration','Duration (Minutes)', $show->duration)->required() !!}
                    {!! Former::select('channel_id','Channel')->options($channels, $show->channel_id) !!}
                    {!! Former::textarea('notes','Notes', $show->notes)->rows(3) !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.lst.shows.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
            </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop
