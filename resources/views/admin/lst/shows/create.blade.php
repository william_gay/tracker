@extends('backpack::layout')

@section('header')
    @include('admin.lst.shows.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
         <div class="block">
                <p class="block-heading">Create Show</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.lst.shows.store' )) !!}
                    {!! Former::xlarge_text('title','Title')->required() !!}
                    {!! Former::xlarge_text('first_airdate','First Aired')->class('form-control datetimepicker')->required() !!}
                    {!! Former::xlarge_text('duration','Duration (Minutes)', 60)->required() !!}
                    {!! Former::select('channel_id','Channel')->options($channels) !!}
                    {!! Former::textarea('notes','Notes')->rows(3) !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.lst.shows.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
