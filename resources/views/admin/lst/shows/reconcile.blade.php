@extends('backpack::layout')

@section('header')
    @include('admin.lst.shows.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Show Reconsile</p>
                <div class="block-body">
                {!! Former::inline_open()->method('GET') !!}
                {!! Former::select('network')->options($channels)->select($channel->id) !!}
                {!! Former::text('day')->class('monitor-date')->value($day->toDateString()) !!}
                {!! Former::checkbox('moreData')->text('Extra Data')->value($moreData) !!}
                {!! Former::submit() !!}
                {!! Former::close() !!}
                @if ($shows->isEmpty())
                    No Shows today...
                @else

                    @foreach ($shows as $show)
                        @if (isset($endTime) and $endTime != $show->start_time)
                        <div class="alert alert-danger">
                            Show Missing @ {!! $endTime !!}!
                        </div>
                        @endif
                        <?php $endTime = $show->end_time; ?>
                        <h5>"{!! $show->show->title ?? 'Missing Show ID: '.$show->live_shopping_show_id !!}" ({!! link_to_route('lst.show', $show->live_shopping_show_id, [$show->live_shopping_show_id]) !!}) &bull; {!! $show->start_time !!} &#8594; {!! $show->end_time !!}
                            <br>Products: {!! $show->products->count() !!}
                            <br>Entered By:
                            @if ($show->user)
                                {!! $show->user->email !!}
                            @else
                                Auto
                            @endif

                            <span class="pull-right">
                                <a href="{!! route("admin.lst.airtime.destroy", array($show->id)) !!}" data-method="delete" data-modal-text="Delete this Show and Airings?" class="btn btn-sm btn-default">
                                    <i class="fa fa-trash-o"> Delete Show and Airings</i>
                                </a>
                            </span>
                        </h5>
                        <table class="table table-bordered table-striped datatable">
                            <thead>
                                <tr>
                                    <th>PID</th>
                                    <th>Product</th>
                                    <th>VID</th>
                                    <th>Version</th>
                                    <th>Price</th>
                                    <th>DSV</th>
                                    <th>User</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($show->products as $product)
                                    @if (! $product->productVersion or ! $product->productVersion->product)
                                        Error! <br>
                                        {!! dd($product, $product->productVersion) !!}
                                    @endif
                                <tr>
                                    <td>
                                        {!! link_to_route('lst.product', $product->productVersion->product->id, [$product->productVersion->product->id]) !!}
                                    </td>
                                    <td>
                                        {!! $product->productVersion->product->name !!}
                                        @if($moreData)
                                        <ul>
                                            <li>
                                                <b>First Aired:</b> {!! $product->productVersion->product->first_airdate !!}
                                            </li>
                                            <li>
                                                <b>SKU: </b> {!! $product->productVersion->product->sku !!}
                                            </li>
                                            <li>
                                                <b>MSRP: </b> {!! $product->productVersion->product->msrp !!}
                                            </li>
                                            <li>
                                                <a href="{!! $product->productVersion->product->website !!}" target="_blank">website</a>
                                            </li>
                                        </ul>
                                        @endif
                                    </td>
                                    <td>
                                        {!! $product->productVersion->id !!}
                                    </td>
                                    <td>
                                        {!! $product->productVersion->name !!}
                                        @if($moreData)
                                        <ul>
                                            <li>
                                                <b>First Aired:</b> {!! $product->productVersion->first_airdate !!}
                                            </li>
                                            <li>
                                                <b>Configuration:</b> {!! $product->productVersion->product_configuration !!}
                                            </li>
                                            <li>
                                                <b>Payment Plan:</b> {!! $product->productVersion->payment_plan_notes !!}
                                            </li>
                                        </ul>
                                        @endif
                                    </td>
                                    <td>{!! $product->productVersion->price !!}</td>
                                    <td>
                                        @if($product->productVersion->daily_special_value)
                                        <i class="fa fa-check"></i>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($product->productVersion->user)
                                            {!! $product->productVersion->user->email !!}
                                        @else
                                            Auto
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <hr>
                    @endforeach

                    @if ($shows->last()->end_time != $day->copy()->endOfDay()->addSecond()->toDateTimeString())
                        <div class="alert alert-danger">
                            Show Missing @ {!! $shows->last()->end_time !!}!
                        </div>
                    @endif
                @endif
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop

@section('script')
<script>
$(function(){
    $('.datatable').dataTable({
        "pageLength": 50,
        "order": [[ 1, "asc" ]]
    });
});
</script>
@endsection
