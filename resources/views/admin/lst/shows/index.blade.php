@extends('backpack::layout')

@section('header')
    @include('admin.lst.shows.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Shows</p>
                <div class="block-body">
                    <table class="table table-striped" id="report">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Channel</th>
                                <th>Title</th>
                                <th>First Aired</th>
                                <th>Duration (Mins)</th>
                                <th>Created</th>
                                <th><i class="icon-cog"></i></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop

@section('script')
<script>
var oTable;
$(function(){
  oTable = $('#report').dataTable( {
    "pageLength": 50,
    "order": [[ 5, "asc" ]],
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": "{!! route('admin.lst.shows.datatables') !!}"
    },
    "columns": [
        {"data": "id", "name": "id", "title": "ID"},
        {"data": "channel.name", "name": "channel.name", "title": "Channel"},
        {"data": "title", "name": "title", "title": "Title"},
        {"data": "first_airdate", "name": "first_airdate", "title": "first_airdate"},
        {"data": "duration", "name": "duration", "title": "duration"},
        {"data": "created_at", "name": "created_at", "title": "created_at"},
        {"data": "actions", "name": "actions", "title": "actions", "orderable": false}
    ],
    "drawCallback": function( settings ) {
      laravel.initialize();
    }
  });
});
</script>
@stop
