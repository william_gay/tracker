<div class="box box-default">
  <div class="box-header with-border">
<h3>
    <i class="fa fa-sitemap"></i>
    <a href="/admin/lst/guides">Live Shopping Guides</a>
    <a href="{!! URL::route('admin.lst.guides.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Add New Guide">
        <i class="fa fa-plus"></i>
        New Guide
    </a>
</h3>
</div>
</div>
