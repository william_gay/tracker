@extends('backpack::layout')

@section('header')
    @include('admin.lst.guides.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Guides ({!! $guides->count() !!})</p>
                <div class="block-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Channel</th>
                                <th>Name</th>
                                <th>URL</th>
                                <th>Created</th>
                                <th><i class="icon-cog"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($guides as $guide)
                                <tr>
                                    <td>{!! $guide->id !!}</td>
                                    <td>{!! $guide->channel->name !!}</td>
                                    <td>{!! $guide->name !!}</td>
                                    <td><a href="{!! $guide->url !!}" target="_blank">{!! $guide->url !!}</a></td>
                                    <td>{!! $guide->created_at->setTimezone('US/Eastern')->format('Y-m-d H:i:s') !!}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
                                                Action
                                                <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{!! route('admin.lst.guides.edit', array($guide->id)) !!}">
                                                       <i class="fa fa-pencil-square-o"></i>&nbsp;Edit Guide
                                                   </a>
                                                </li>
                                                <li>
                                                    <a href="{!! route('admin.lst.guides.destroy', array($guide->id)) !!}"
                                                       data-method="delete"
                                                       data-modal-text="Delete this Category?">
                                                       <i class="icon-trash"></i>&nbsp;Delete Guide
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="8">{!! $guides->links() !!}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop
