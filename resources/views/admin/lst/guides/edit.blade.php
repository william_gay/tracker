@extends('backpack::layout')

@section('header')
    @include('admin.lst.guides.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Edit Guide</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.lst.guides.update', array($guide->id)), 'PUT' ) !!}
                    {!! Former::select('channel_id','Channel')->options($channels,$guide->channel_id) !!}
                    {!! Former::xlarge_text('name','Name',$guide->name)->required() !!}
                    {!! Former::xlarge_text('url','URL',$guide->url) !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.lst.guides.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
            </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop
