@extends('backpack::layout')

@section('header')
    @include('admin.lst.monitor.header')
@stop

@section('style')
    <style>
    .container {
        width: 99%;
    }
    label.error {
        color: red;
    }
    .select2-container--default .select2-search--inline .select2-search__field {
        width: auto !important;
    }
    </style>
@endsection

@section('content')
<div class="box box-default">
  <div class="box-body">
<div class="row">
    <div class="col-md-12">
        <ul class="list-inline" style="margin-top:5px;">
                <li><a href="http://capture21.monitor.imsreport.com/?directory=%2Fcapture%2FQVC" target="_blank">QVC</a></li>
                <li><a href="http://capture22.monitor.imsreport.com/?directory=%2Fcapture%2FHSN%2F" target="_blank">HSN</a></li>
                <li><a href="http://capture17.monitor.imsreport.com/?directory=%2Fcapture%2FEL" target="_blank">EL</a></li>
        </ul>
        <h5>Show Details</h5>
        {!! Form::open(['method' => 'get', 'class' => 'form-inline', 'id' => 'lst-tracker']) !!}
        <div class="form-group">
            {!!  Form::label('Channel') !!}
            {!! Form::select('lst_channel_id', $channels, $channel_id, ['class' => 'form-control', 'id' => 'lst_channel_id', 'required']) !!}
        </div>
        <div class="form-group" style="width: 320px;">
            {!! Former::text('lst_show_id', '')->required()->class('show-name form-control')->style('width: 100%')->placeholder('Start Typing to Find Show') !!}
            <span id="edit-show"></span>
            {!! Form::hidden('custom_show_name', null, ['id' => 'custom_show_name']) !!}
            {!! Form::hidden('show_airtime_id', null, ['id' => 'show_airtime_id']) !!}
        </div>
        <div class="form-group">
            {!! Former::text('date')->placeholder('Start Time')->id('air_date')->required()->value($air_date->format('Y-m-d h:i A')) !!}
        </div>
        <div class="form-group">
            {!! Former::text('end_time')->placeholder('End Time')->id('end_time')->required()->value($air_date->copy()->addHour()->format('Y-m-d h:i A')) !!}
        </div>
        <br><br>
        <div class="form-group">
            {!! Former::textarea('show_notes')->cols(50)->id('show_notes')->placeholder('Host name, Name of Spokesperson') !!}
        </div>
        <div class="form-group">
                {!! Form::button('<i class="fa fa-save"></i> Save Show', ['class' => 'btn btn-sm', 'id' => 'save-airtime']) !!}
        </div>
        {!! Form::close() !!}
        {!! Form::open([ 'class' => 'form-inline', 'id' => 'product-section', 'style' => 'display:none;', 'enctype' => 'multipart/form-data']) !!}
            <hr>
            <h5>Product Details</h5>
            <div class="form-group" style="width: 400px;">
            {!! Former::text('lst_product_version_id','')->required()->style('width: 100%;')->class('col-md-4 product-name')->placeholder('Start Typing to Find Product')->id('lst_product_version_id') !!}
            {!! Form::hidden('custom_product_name', null,['id' => 'custom_product_name']) !!}
            {!! Form::hidden('lst_product_id', null,['id' => 'lst_product_id']) !!}
            </div>
            <div class="form-group">
                {!! Former::text('sku', 'SKU')->id('sku')->required() !!}
            </div>
            <div class="form-group">
                {!! Former::text('msrp', 'MSRP')->id('msrp')->required() !!}
            </div>
            <div class="form-group">
                {!! Former::select('live_shopping_category_id', 'Category')->options(['' => 'Select One'] + $categories)->required() !!}
            </div>
            <div class="form-group">
                {!! Form::hidden('live_shopping_sub_category_id_hidden', null, ['id' => 'live_shopping_sub_category_id_hidden']) !!}
                {!! Former::select('live_shopping_sub_category_id', 'Sub-Cat')->options(['' => 'Select One'])->required() !!}
            </div>
            <br><br>
            <div class="form-group">
                {!! Former::file('product_img','Image')->accept('image') !!}
            </div>
            <br><br>
            <div class="form-group">
                {!! Former::text('website', 'Website')->id('website')->required() !!}
            </div>
            <div class="form-group">
                {!! Former::textarea('product_notes')->rows(1)->cols(100)->id('product_notes') !!}
            </div>
        <hr>
        <h5>Product Version Details</h5>
            <div class="form-group">
                {!! Former::text('price', 'Price')->id('price')->required() !!}
            </div>
            <div class="form-group">
            {!! Former::text('price_max', 'Max Price')->id('price_max') !!}
            </div>
            <div class="form-group">
                {!! Former::text('shipping', 'Shipping')->id('shipping') !!}
            </div>
            <div class="form-group">
                {!! Former::checkbox('payment_plan')->id('payment_plan') !!}
            </div>
            <div class="form-group">
                {!! Former::text('payment_plan_notes', 'PP Notes')->id('payment_plan_notes') !!}
            </div>
            <br><br>
            <div class="form-group">
                {!! Former::checkbox('daily_special_value')->id('daily_special_value') !!}
            </div>
            <div class="form-group">
                {!! Former::text('units_sold')->id('units_sold') !!}
            </div>
            <br><br>
            <div class="form-group">
                {!! Former::textarea('config')->rows(3)->cols(70)->id('config') !!}
            </div>
            <div class="form-group">
                {!! Former::textarea('product_version_notes')->rows(3)->cols(70)->id('product_version_notes') !!}
            </div>
            <br><br>
            <div class="form-group">
                <div class="input-group">
                    {!! Form::button('<i class="fa fa-plus"></i> Add Product Airtime', ['class' => 'btn btn-sm', 'id' => 'add-product-airtime']) !!}
                </div>
            </div>
        {!! Form::close() !!}
        <hr>
            <h5 class="text-center">Product Airings</h5>

                <div id="product-airings"><p class="alert alert-info">No product airings entered yet!</p></div>
        </div>
        <hr>
    </div>
</div>
</div>
</div>
@stop

@section('script')
<script>
$(function() {
    $("#air_date").datetimepicker({
        sideBySide: true,
        format: "YYYY-MM-DD hh:mm A"
    });
    $("#end_time").datetimepicker({
        sideBySide: true,
        format: "YYYY-MM-DD hh:mm A"
    });

    $("body").on("dp.change", "#air_date",function (e) {
        // Don't allow you to end the show before it starts
        $('#end_time').data("DateTimePicker").minDate(e.date);
        // Set the min date for Product start time
    });

    $customShowInput = $('#custom_show_name');
    $('#lst_show_id').select2({
        placeholder: "Search for a LST Show",
        minimumInputLength: 2,
        ajax: {
            url: "/api/lst/shows/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                tempShowSearchTerm = term;
                return {
                    q: term, // search term
                    page_limit: 25
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                if (data.total === 0) {
                    $customShowInput.val(tempShowSearchTerm);
                    $('#lst_show_id').removeAttr('required');
                    $('#edit-show').empty();

                    return {
                        results: [
                            {id: null, title: 'New Show: ' +tempShowSearchTerm}
                        ]
                    };
                }

                $customShowInput.val("");

                return {
                    results: data.shows
                };
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/lst/shows/one", {
                    data: {
                        lst_show_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: function formatTitle(format){
            return format.title;
        },
        formatSelection: function formatTitle(format){
            $('#show_notes').val(format.notes);

            if (format.id != undefined) {
                $('#edit-show').html('<a href="/admin/lst/shows/'+format.id+'/edit" target="_blank">Edit Show</a>')
            }

            return format.title;
        },
        escapeMarkup: function (m) { return m; }
    });

    $customProductInput = $('#custom_product_name');
    $('#lst_product_version_id').select2({
        placeholder: "Search for a LST Product",
        minimumInputLength: 2,
        ajax: {
            url: "/api/lst/product-versions/search",
            dataType: 'jsonp',
            quietMillis: 100,
            data: function (term, page) {
                tempProductSearchTerm = term;
                return {
                    q: term, // search term
                    page_limit: 25
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                if (data.total === 0) {
                    $('#custom_product_name').val(tempProductSearchTerm);
                    $('#lst_product_version_id').removeAttr('required');
                    return {
                        results: [
                            {id: null, name: 'New Product: ' +tempProductSearchTerm}
                        ]
                    };
                }

                $('#custom_product_name').val("");

                return {
                    results: data.products
                };
            }
        },
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                $.ajax("/api/lst/product-versions/one", {
                    data: {
                        product_version_id: id
                    },
                    dataType: "jsonp"
                }).done(function(data) {
                    callback(data);
                });
            }
        },
        formatResult: function formatTitle(format){
            if (format.price == undefined) {
                return format.name;
            }

            return format.name +' $'+format.price;
        },
        formatSelection: function formatTitle(format){
            if (format.product == undefined) {
                return format.name;
            }

            $('#lst_product_id').val(format.product.id);
            $('#product_notes').val(format.product.notes);
            $('#msrp').val(format.product.msrp);
            $('#sku').val(format.product.sku);
            $('#website').val(format.product.website);
            $('#live_shopping_category_id').val(format.product.live_shopping_category_id).trigger('change');
            $('#live_shopping_sub_category_id_hidden').val(format.product.live_shopping_sub_category_id);

            if (format.payment_plan == 1) {
                $('#payment_plan').attr('checked', true);
            } else {
                $('#payment_plan').attr('checked', false);
            }

            if (format.daily_special_value == 1) {
                $('#daily_special_value').attr('checked', true);
            } else {
                $('#daily_special_value').attr('checked', false);
            }
            $('#units_sold').val(format.units_sold);

            $('#shipping').val(format.shipping_and_handling);
            $('#price').val(format.price);
            $('#price_max').val(format.max_price);
            $('#config').val(format.product_configuration);
            $('#payment_plan_notes').val(format.payment_plan_notes);
            $('#product_version_notes').val(format.notes);

            return format.name +' $'+format.price;
        },
        escapeMarkup: function (m) { return m; }
    });

    // When page first loads, check to see if we have an airtime
    setTimeout(function(){
        loadShow();
    }
    ,100);

    $('body').on('dp.change', '#air_date',function(e) {
        e.preventDefault();
        // Load Show if there is an airtime
        loadShow();
    });

    $('#lst_channel_id').change(function (){
        loadShow();
    });

    $('#save-airtime').click(function(e) {
        e.preventDefault();

        if (! $('#lst-tracker').valid()){
            return;
        }

        $channel_id = $('#lst_channel_id').val();
        $air_date = $('#air_date').val();
        $start_time = $('#start_time').val();
        $end_time = $('#end_time').val();
        $lst_show_id = $('#lst_show_id').val();
        $lst_show_notes = $('#show_notes').val();

        //Post a new Show if it is one
        if ($customShowInput.val() !== '') {
            $.post("/api/lst/shows/add",
                {
                    channel_id: $channel_id,
                    air_date: $air_date,
                    start_time: $start_time,
                    end_time: $end_time,
                    custom_show_name: $customShowInput.val(),
                    show_notes: $lst_show_notes
                },
                function(data) {
                    // Load airtime ID into hidden field
                    $('#lst_show_id').select2("val",data.id);
                    $customShowInput.val('');
                    $('#product-section').fadeIn();

                    addAirtime();
                    //Reload Product Airtime Table
                    loadProductAirtimes();
                }
            );
        } else {
            $.post("/api/lst/shows/update/"+$lst_show_id,
                {
                    show_notes: $lst_show_notes
                },
                function(data) {
                    $('#product-section').fadeIn();

                    addAirtime();
                    //Reload Product Airtime Table
                    loadProductAirtimes();
                }
            );
        }
    });

    $('#lst-tracker').validate({
        ignore: " "
    });

    $('#product-section').validate({
        ignore: " ",
        rules: {
            price: {
              required: true,
              number: true
            },
            msrp: {
              required: true,
              number: true
            }
        }
    });

    $('#add-product-airtime').click(function(e) {
        e.preventDefault();

        if (! $('#product-section').valid()) {
            return;
        }

        //Do the Product Thing
        $product_id = $('#lst_product_id').val();
        $custom_product = $('#custom_product_name').val();

        var data = new FormData();
        data.append('product_id', $('#lst_product_id').val());
        data.append('name', $('#custom_product_name').val());
        data.append('air_date', $('#air_date').val());
        data.append('msrp', $('#msrp').val());
        data.append('sku', $('#sku').val());
        data.append('website', $('#website').val());
        data.append('notes', $('#product_notes').val());
        data.append('category_id', $('#live_shopping_category_id').val());
        data.append('sub_category_id', $('#live_shopping_sub_category_id').val());
        //get image file
        var productImage = document.getElementById('product_img');
        data.append('product_img', productImage.files[0]);

        if ($custom_product !== '') {
            // Insert
            $.ajax({
              url: "/api/lst/products/add",
              type: "POST",
              data: data,
              mimeType:"multipart/form-data",
              contentType: false,
              cache: false,
              processData:false,
              success: function(data){
                  var returnData = JSON.parse(data);
                  $('#lst_product_id').val(returnData.id);
                  $product_id = returnData.id;
                  addVersion();
              }
            });

        } else {
            // Update
            $.ajax({
              url: "/api/lst/products/update/"+$product_id,
              type: "POST",
              data: data,
              mimeType:"multipart/form-data",
              contentType: false,
              cache: false,
              processData:false,
              success: function(data){
                  addVersion();
              }
            });
        }
    });

    $('#product-airings').on('click', '.quickAdd', function(){
        $product_id = $(this).data('product');
        $product_version_id = $(this).data('productversion');

        $('#lst_product_version_id').select2("val",$product_version_id).trigger("change");
    });

    $('#product-airings').on('click', '.deleteEntry', function(e){
        e.preventDefault();

        $delete_id = $(this).data('delete');

        var msg  = '<i class="icon-warning-sign modal-icon"></i>&nbsp;Are you sure you want to&nbsp;';
        bootbox.dialog({
            "message": msg,
            "title": "Please Confirm",
            "buttons": {
                danger: {
                    label: "Confirm Delete",
                    className: "btn-danger",
                    callback: function() {
                        //Send AJAX Request to Server to Delete Row:
                        var request = $.ajax({
                            url: "/api/lst/airtime-products/delete/"+$delete_id,
                            type: "post",
                            async: true
                        });
                        request.done(function (response, textStatus, jqXHR){
                            //console.log(response);
                            loadProductAirtimes();
                        });
                    }
                },
                close: {
                    label: "Cancel",
                    className: "btn"
                }
            }
        });
    });

    function loadShow() {
        $channel_id = $('#lst_channel_id').val();
        $air_date = $('#air_date').val();
        $start_time = $('#start_time').val();

        $.get("/api/lst/airtime/get",
            {
                channel_id: $channel_id,
                air_date: $air_date,
                start_time: $start_time
            },
            function(data) {
                //console.log(data);
                if (data.id === undefined) {
                    clearInputs(true);
                } else {
                    $('#lst_show_id').select2("val",data.live_shopping_show_id);
                    $('#show_airtime_id').val(data.id);
                    $('#end_time').val(data.end_time);
                    $('#product-section').fadeIn();
                    loadProductAirtimes();
                }
            }
        );
    }

    function clearInputs(showsToo)
    {
        showsToo = typeof showsToo !== 'undefined' ? showsToo : false;

        if (showsToo) {
            $('#lst_show_id').select2("val","");
            $('#show_airtime_id').val('');
            $('#show_notes').val('');
            $('#product-section').hide();
            $('#product-airings').empty();
            $('#product-airings').html('<p class="alert alert-info">No product airings entered yet!</p>');
        }

        // Product
        $('#lst_product_id').val('');
        $('#lst_product_version_id').select2("val", "").trigger("change");
        $('#custom_product_name').val('');
        $('#product_notes').val('');
        $('#msrp').val('');
        $('#sku').val('');
        $('#website').val('');
        $('#live_shopping_category_id').val('');
        $('#live_shopping_sub_category_id').val('');
        $('#product_img').val('');

        // Product Version
        $('#payment_plan').attr('checked', false);
        $('#payment_plan_notes').val('');
        $('#daily_special_value').attr('checked', false);
        $('#units_sold').val('');
        $('#product_version_notes').val('');
        $('#config').val('');
        $('#shipping').val('');
        $('#price').val('');
        $('#price_max').val('');
    }

    function addVersion() {
        // Do the Product Version Thing
        $product_id = $('#lst_product_id').val();
        $version = $('#lst_product_version_id').val();
        $customProductInput = $('#custom_product_name').val();
        $product_configuration = $('#config').val();
        $price = $('#price').val();
        $price_max = $('#price_max').val();
        $shipping = $('#shipping').val();
        $payment_plan = $('#payment_plan').is(':checked') ? 1 : 0;
        $payment_plan_notes = $('#payment_plan_notes').val();
        $daily_special_value = $('#daily_special_value').is(':checked') ? 1 : 0;
        $units_sold = $('#units_sold').val();
        $product_version_notes = $('#product_version_notes').val();

        if ($customProductInput != '' && $version == '') {
            // Insert

            $.post("/api/lst/product-versions/add",
                {
                    name: $customProductInput,
                    air_date: $air_date,
                    live_shopping_product_id: $product_id,
                    product_configuration: $product_configuration,
                    price: $price,
                    max_price: $price_max,
                    shipping_and_handling: $shipping,
                    payment_plan: $payment_plan,
                    payment_plan_notes: $payment_plan_notes,
                    daily_special_value: $daily_special_value,
                    units_sold: $units_sold,
                    notes: $product_version_notes
                },
                function(data) {
                    // Show a status message
                    $('#lst_product_version_id').append('<option value="'+data.id+'">'+data.name+' '+data.price+'</option>');
                    $('#lst_product_version_id').val(data.id);

                    addProductAirtime();
                }
            );

        } else {
            // Update
            $.post("/api/lst/product-versions/update/"+$version,
                {
                    air_date: $air_date,
                    live_shopping_product_id: $product_id,
                    product_configuration: $product_configuration,
                    price: $price,
                    max_price: $price_max,
                    shipping_and_handling: $shipping,
                    payment_plan: $payment_plan,
                    payment_plan_notes: $payment_plan_notes,
                    daily_special_value: $daily_special_value,
                    units_sold: $units_sold,
                    notes: $product_version_notes
                },
                function(data) {
                    // Show a status message

                    addProductAirtime();
                }
            );
        }
    }

    function addAirtime() {
        $channel_id = $('#lst_channel_id').val();
        $air_date = $('#air_date').val();
        $start_time = $('#start_time').val();
        $end_time = $('#end_time').val();
        $lst_show_id = $('#lst_show_id').val();
        $airtime_id = $('#show_airtime_id').val();

        if ($airtime_id > 0) {
            //Post Airtime
            $.post("/api/lst/airtime/update/"+$airtime_id,
                {
                    channel_id: $channel_id,
                    air_date: $air_date,
                    start_time: $start_time,
                    end_time: $end_time,
                    live_shopping_show_id: $lst_show_id
                },
                function(data) {
                    // Load airtime ID into hidden field
                    $('#show_airtime_id').val(data.id);

                    //Reload Product Airtime Table
                    loadProductAirtimes();
                }
            );
        } else {
            //Post Airtime
            $.post("/api/lst/airtime/add",
                {
                    channel_id: $channel_id,
                    air_date: $air_date,
                    start_time: $start_time,
                    end_time: $end_time,
                    live_shopping_show_id: $lst_show_id
                },
                function(data) {
                    // Load airtime ID into hidden field
                    $('#show_airtime_id').val(data.id);

                    //Reload Product Airtime Table
                    loadProductAirtimes();
                }
            );
        }
    }

    function addProductAirtime() {
        // Add product, then reload the product airtimes table
        $show_airtime_id = $('#show_airtime_id').val();
        $air_date = $('#air_date').val();
        $lst_product_version_id = $('#lst_product_version_id').val();

        $.post("/api/lst/airtime-products/add",
            {
                show_airtime_id: $show_airtime_id,
                air_date: $air_date,
                product_start_time: $air_date,
                lst_product_version_id: $lst_product_version_id
            },
            function(data) {
                loadProductAirtimes();
            }
        );
    }

    function loadProductAirtimes() {
        $airtime_id = $('#show_airtime_id').val();

        //Get Products by Airtime
        $.get("/api/lst/airtime-products/get",
            {
                show_airtime_id: $airtime_id
            },
            function(data) {
                // Out with the old
                $('#product-airings').empty();
                // In with the new
                $('#product-airings').html(data);

                clearInputs();
            }
        );
    }
});
</script>
@stop
