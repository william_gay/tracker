{!! Former::horizontal_open( route('admin.lst.product-versions.update'), null, ['id' => 'frm-product-version']) !!}
	{!! Form::hidden('id', $version->id) !!}
	{!! Form::hidden('live_shopping_product_id', $version->product->id) !!}
    {!! Former::xlarge_text('name','Name', $version->name)->required() !!}
    {!! Former::textarea('product_configuration','Product Configuration', $version->product_configuration)->rows(3) !!}
    {!! Former::xlarge_text('price','Price', $version->price) !!}
    {!! Former::xlarge_text('shipping_and_handling','Shipping', $version->shipping_and_handling) !!}
    {!! Former::checkbox('payment_plan','Payment Plan')->check(['payment_plan' => $version->payment_plan]) !!}
    {!! Former::textarea('payment_plan_notes', 'Payment Plan Details', $version->payment_plan_notes) !!}
    {!! Former::checkbox('daily_special_value', 'DSV')->check(['daily_special_value' => $version->daily_special_value]) !!}
    {!! Former::xlarge_text('units_sold', 'Units Sold', $version->units_sold) !!}
    {!! Former::xlarge_text('first_airdate','First Aired', $version->first_airdate->format("Y-m-d h:i:s A"))->class('form-control first_airdate') !!}
    {!! Former::textarea('notes','Notes', $version->notes)->rows(3) !!}
    <div class="form-group">
        <div class="form-actions col-lg-4 col-lg-offset-2 col-sm-4">
            {!! Form::button('Save Version', array('class' => 'btn btn-success btn-save btn-large', 'id' => 'save-version')) !!} <a href="{!! route('admin.lst.product-versions.destroy', [$version->id]) !!}" class="btn btn-danger" data-method="delete" data-modal-text="Delete this Product Version? All airing data will be deleted as well!">Delete Version</a>
        </div>
    </div>
{!! Former::close() !!}
