{!! Former::horizontal_open( route('admin.lst.product-versions.store'), null, ['id' => 'frm-product-version']) !!}
	{!! Form::hidden('live_shopping_product_id', $product->id) !!}
    {!! Former::xlarge_text('name','Name', $product->name)->required() !!}
    {!! Former::textarea('product_configuration','Product Configuration')->rows(3) !!}
    {!! Former::xlarge_text('price','Price') !!}
    {!! Former::xlarge_text('shipping_and_handling','Shipping') !!}
    {!! Former::checkbox('payment_plan','Payment Plan') !!}
    {!! Former::textarea('payment_plan_notes', 'Payment Plan Details') !!}
    {!! Former::checkbox('daily_special_value', 'DSV') !!}
    {!! Former::xlarge_text('units_sold', 'Units Sold') !!}
    {!! Former::xlarge_text('first_airdate','First Aired', $product->first_airdate->format("Y-m-d h:i:s A"))->class('form-control first_airdate') !!}
    {!! Former::textarea('notes','Notes')->rows(3) !!}
    <div class="form-group">
        <div class="form-actions col-lg-2 col-lg-offset-2 col-sm-4">
            {!! Form::button('Save Version', array('class' => 'btn btn-success btn-save btn-large', 'id' => 'save-version')) !!}
        </div>
    </div>
{!! Former::close() !!}
