@if(! $products->isEmpty())
<p>Total: {!! $products->count() !!}</p>
<table class="table table-bordered">
    <thead>
        <th width="20"></th>
        <th>Product</th>
        <th>DSV</th>
        <th>Config</th>
        <th>Pricing</th>
        <th>Air Date/Show Time</th>
        <!-- <th>End Time</th> -->
        <th width="20"></th>
    </thead>
    <tbody>
        @foreach($products as $product)
        <tr>
            <td>
                @if ($product->productVersion)
                <a href="#" data-product="{!! $product->productVersion->product->id !!}" data-productversion="{!! $product->productVersion->id !!}" title="Quick Add" text="Quick Add Product?" class="btn btn-sm btn-default quickAdd">
                    <i class="fa fa-plus"></i>
                </a>
                @endif

            </td>
            <td>
                <ul class="list-unstyled">
                @if ($product->productVersion)
                <li>
                {!! $product->productVersion->name !!}
                    <small><a href="{!! route('admin.lst.products.edit', [$product->productVersion->product->id, 'version_id' => $product->productVersion->id]) !!}" target="_blank">Edit</a></small>
                </li>
                @endif
                @if ($product->productVersion and $product->productVersion->product->msrp)
                <li><strong>SKU:</strong> {!! $product->productVersion->product->sku ?? 'N/A' !!}</li>
                <li><strong>MSRP: </strong> ${!! number_format($product->productVersion->product->msrp, 2) !!}</li>
                @endif
                @if ($product->productVersion and $product->productVersion->product->category)
                <li><strong>Category: </strong> {!! $product->productVersion->product->category->name ?? 'N/A' !!}</li>
                @endif
                @if ($product->productVersion and $product->productVersion->product->subCategory)
                <li><strong>Sub-Category: </strong> {!! $product->productVersion->product->subCategory->name ?? 'N/A' !!}</li>
                @endif
                </ul>
            </td>
            <td>
            @if($product->productVersion and $product->productVersion->daily_special_value)
                <i class="fa fa-trophy"></i>
            @endif
            {!! $product->productVersion->units_sold ?? '' !!}
            </td>
            <td>
            @if($product->productVersion)
                {!! $product->productVersion->product_configuration !!}
            @endif
            </td>
            <td>
            @if($product->productVersion)
                <ul class="list-unstyled">
                    @if ($product->productVersion->price)
                    <li>
                        <strong>Price:</strong> ${!! number_format($product->productVersion->price, 2) !!}
                    </li>
                    @endif
                    @if ($product->productVersion->max_price)
                    <li>
                        <strong>Max Price:</strong> ${!! number_format($product->productVersion->max_price, 2) !!}
                    </li>
                    @endif
                    @if ($product->productVersion->shipping_and_handling)
                    <li>
                        <strong>Shipping:</strong>
                        @if (is_numeric($product->productVersion->shipping_and_handling))
                            {!! $channel->currency ?? '' !!}{{ number_format($product->productVersion->shipping_and_handling, 2) }}
                        @else
                            {!! $product->productVersion->shipping_and_handling ?? 'N/A' !!}
                        @endif
                    </li>
                    @endif
                    @if($product->productVersion->payment_plan)
                    <li>
                        <strong>Payment Plan:</strong> {!! $product->productVersion->payment_plan_notes !!}
                    </li>
                    @endif
                </ul>
            @endif
            </td>
            <td>
            @if($product->start_time)
                {!! $product->start_time->format('g:i A m/d/Y') !!}
            @endif
            </td>
            <!-- <td>
            @if($product->end_time)
                <span class="lastAirtime" data-date="{!! $product->end_time->format('Y-m-d g:i A') !!}">
                    {!! $product->end_time->format('g:i A m/d/Y') !!}
                </span>
            @endif
            </td> -->
            <td>
                <a href="#" data-delete="{!! $product->id !!}" text="Delete this Airtime?" class="btn btn-sm btn-danger deleteEntry">
                    <i class="fa fa-trash-o"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@else
    <p class="alert alert-info">No product airings entered yet!</p>
@endif
