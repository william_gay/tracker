@extends('backpack::layout')

@section('header')
    @include('admin.lst.categories.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Categories</p>
                <div class="block-body">
                    <table class="table table-striped" id="report">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Parent Category</th>
                                <th>Category</th>
                                <th>Created</th>
                                <th><i class="icon-cog"></i></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@stop

@section('script')
<script>
var oTable;
$(function(){
  oTable = $('#report').dataTable( {
    "pageLength": 50,
    "order": [[ 4, "asc" ]],
    "processing": true,
    "serverSide": true,
    "ajax": {
        "url": "{!! route('admin.lst.categories.datatables') !!}"
    },
    "columns": [
        {"data": "category_id", "name": "category_id", "title": "ID"},
        {"data": "parent_category_name", "name": "parent_category_name", "title": "Parent Category"},
        {"data": "category_name", "name": "category_name", "title": "Category"},
        {"data": "created_at", "name": "created_at", "title": "created_at"},
        {"data": "id", "name": "id", "title": "actions"}
    ],
    "drawCallback": function( settings ) {
      laravel.initialize();
    }
  });
});
</script>
@stop
