@extends('backpack::layout')

@section('header')
    @include('admin.lst.categories.header')
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <p class="block-heading">Edit Category</p>
                <div class="block-body">
                {!! Former::horizontal_open( route('admin.lst.categories.update', array($category->id)), 'PUT' ) !!}
                    {!! Former::select('parent_id','Parent Category')->options($pcats,$category->parent_id) !!}
                    {!! Former::xlarge_text('name','Name',$category->name)->required() !!}
                    {!! Former::textarea('description','Description',$category->description) !!}
                    <div class="form-actions">
                        {!! Form::submit('Save', array('class' => 'btn btn-success btn-save btn-large')) !!}
                        <a href="{!! URL::route('admin.lst.categories.index') !!}" class="btn btn-large">Cancel</a>
                    </div>
                {!! Former::close() !!}
            </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop
