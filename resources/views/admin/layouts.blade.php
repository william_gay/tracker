<!DOCTYPE html>
<!--[if lt IE 7]>       <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>          <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>          <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{!! Config::get('app.site_config.title') !!}</title>
    <meta name="description" content="{!! Config::get('app.site_config.description') !!}">
    <meta name="viewport" content="width=device-width">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    @include('cxp.partials.favicon')

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="{{ mix("assets/css/shared.css") }}" rel="stylesheet" type='text/css'>
    <link href="{{ mix("assets/css/admin.css") }}" rel="stylesheet" type='text/css'>

    @yield('style')
    <script>
    var laravel = {};
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    @include('cxp.partials.bugsnag')
</head>
<body>
    <!-- #wrap -->
    <div id="wrap">
        <!-- #top -->
        <div id="top">
            <!-- .navbar -->
            <nav class="navbar navbar navbar-top" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="{!!URL::to('/')!!}">
                        <img src="{!! asset('assets/img/logo.png') !!}" style="width: 150px;" alt="{!! Config::get('app.site_config.company_short') !!} Dashboard"/>
                      </a>
                    </div>
                    @if (Sentry::check())
                        <a class="btn btn-default navbar-btn btn-inverse navbar-right" data-placement="bottom" data-original-title="Logout" rel="tooltip"
                           href="{!! route('admin.logout') !!}"><i class="fa fa-power-off"></i>
                        </a>
                        <!-- /.topnav -->
                        <div class="collapse navbar-collapse">
                            <!-- .nav -->
                            <ul class="nav navbar-nav">
                                @foreach (Config::get('app.menu') as $title => $args)
                                    @if ($args['type'] === 'single')
                                        <li>{!! Html::linkRoute($args['route'], $title) !!}</li>
                                    @else
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="blank.html#">
                                               {!! $title !!}
                                            </a>
                                            <ul class="dropdown-menu">
                                                @foreach ($args['links'] as $title => $value)
                                                    <li>{!! Html::linkRoute($value['route'], $title) !!}</li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                            <!-- /.nav -->
                        </div>
                    @endif
                </div>
            </nav>
            <!-- /.navbar -->
        </div>
        <!-- /#top -->
        <!-- .head -->
        <header class="head">
            <!-- ."main-bar -->
            <div class="main-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            @yield('header')
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.main-bar -->
        </header>
        <!-- /.head -->

        <!-- #content -->
        <div id="content">

            <div class="container">
                @include('admin.partials.alert')
                @yield('content')
            </div>
            <!-- /.outer -->
        </div>
        <!-- /#content -->
        <!-- #push do not remove -->
        <div id="push"></div>
        <!-- /#push -->
    </div>
    <!-- /#wrap -->

    <div class="clearfix"></div>
    <div id="footer">
        <p>{!! date('Y') !!} © {!! Config::get('app.site_config.site_name') !!} </p>
    </div>

    <!-- #helpModal -->
    <div id="helpModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="helpModalLabel"><i class="icon-external-link"></i> Help</h3>
        </div>
        <div class="modal-body">
            @section('help')
                <p>No Help for this section.</p>
            @show
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
    <!-- /#helpModal -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script>
        if (typeof jQuery == 'undefined') {
            document.write(unescape("%3Cscript src='/assets/js/jquery-1.11.1.min.js' type='text/javascript'%3E%3C/script%3E"));
        }
        </script>

        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>

        <script src="{{ mix("assets/js/shared.js") }}"></script>
        <script src="{{ mix("assets/js/admin.js") }}"></script>

        @yield('script')

</body>
</html>
