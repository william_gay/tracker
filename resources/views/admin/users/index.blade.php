@extends('backpack::layout')

@section('header')
<div class="box box-default">
  <div class="box-header with-border">
    @include('admin.users.header')
  </div>
</div>
@stop

@section('content')
<div class="box box-default">
  <div class="box-body">
    <div class="row">
        <div class="col-md-12" style="padding-top:10px;">
            <table class="table table-striped table-bordered" id="users">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Groups</th>
                        <th>Active</th>
                        <th>Joined</th>
                        <th>Last Visit</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{!! Html::linkRoute('admin.users.show',$user->first_name.' '.$user->last_name, array($user->id)) !!}</td>
                            <td>{!! $user->email !!}</td>
                            <td>
                                @foreach($user->groups as $group)
                                    <span class="group-list">{!! $group->getName() !!}</span>
                                @endforeach
                            </td>
                            <td>{!! ($user->activated) ? 'yes' : 'no' !!}</td>
                            <td>{!! $user->activated_at !!}</td>
                            <td>{!! is_null($user->last_login) ? 'Never Visited' : $user->last_login !!}</td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#">
                                        Action
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                       <li>
                                           <a href="{!! route('admin.users.edit', array($user->id)) !!}">
                                               <i class="fa fa-pencil-square-o"></i>&nbsp;Edit User
                                           </a>
                                       </li>
                                        <li>
                                            <a href="{!! route('admin.users.permissions', array($user->id)) !!}">
                                                <i class="icon-ban-circle"></i>&nbsp;Permissions
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{!! route('admin.users.destroy', array($user->id)) !!}"
                                               data-method="delete"
                                               data-modal-text="delete this User?">
                                               <i class="icon-trash"></i>&nbsp;Delete User
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            @if ($user->isActivated())
                                                <a href="{!! route('admin.users.deactivate', array($user->id)) !!}"
                                                   data-method="put"
                                                   data-modal-text="Deactivate this User?">
                                                    <i class="fa fa-trash-o"></i>&nbsp;Deactivate
                                                </a>
                                            @else
                                                <a href="{!! route('admin.users.activate', array($user->id)) !!}"
                                                   data-method="put"
                                                   data-modal-text="Activate this User?">
                                                    <i class="icon-check"></i>&nbsp;Activate
                                                </a>
                                            @endif
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="{!! route('admin.users.throttling', array($user->id)) !!}">
                                                <i class="icon-key"></i>&nbsp;Throttling
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>
</div>
@stop

@section('script')
<script>
var oTable;
$(function(){
    oTable = $('#users').dataTable({
    "pageLength": 50
  });
});
</script>
@stop
