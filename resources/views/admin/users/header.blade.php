<h3>
    <i class="fa fa-users"></i>
    Users
    <a href="{!! route('admin.users.create') !!}" class="btn btn-primary pull-right" rel="tooltip" title="Create New User">
        <i class="fa fa-plus"></i>
        New User
    </a>
</h3>