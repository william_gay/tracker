@extends('cxp.layout')

@section('header')
    <h3>
        <i class="icon-desktop"></i>
        Networks
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('network', $network) !!}
    <div class="row-fluid">
        <h3 class="text-center">{!! $network->name !!} Scheduler</h3>
        <div class="date-selection text-center">
            <form class="form-inline" style="display:inline-block;">
            {!! Former::xlarge_text('start-date','', $start_date->format("F j, Y"))->class('span4 form-control start-date')->placeholder($start_date->format("F j, Y"))->required() !!} {!! Former::xlarge_text('end-date','', $end_date->format("F j, Y"))->class('span4 form-control end-date')->placeholder($end_date->format("F j, Y"))->required() !!}
            <button class="select btn btn-blue btn-icon">Select Date Range <i class="fa fa-calendar"></i></button>
            </form>
        </div>
    </div>
    @if($airtimes)
    <div class="row">
        <div class="col-xs-12">

            <ul class="list-inline text-center stats-overview">
                <li>
                    <div class="well well-sm">
                        <h5>Total Show Airs</h5>
                        <h3> {!! $airtimes->airs !!} </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Total Show Media Index</h5>
                        <h3> {!! number_format($airtimes->media_index,2) !!} </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Average Show Cost Per Air</h5>
                        <h3> @if($airtimes->airs > 0)
                                $ {!! number_format($airtimes->total_cost/$airtimes->airs,2) !!}
                            @else
                                N/A
                            @endif
                        </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Total Spot Airs</h5>
                        <h3> {!! $spotAirtimes->airs !!} </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Total Unique Spot Airs</h5>
                        <h3> {!! $spotAirtimes->unique_spots !!} </h3>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4 class="text-center">Top 10 Shows</h4>
            <div id="show-distribution" style="width:100%;height: 300px;"></div>
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        Weekly Show Breakdown
                    </div>
                </div>
                <div class="panel-body scrollable" data-height="300" style="overflow: hidden; width: auto; height: 300px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Week Ending</th>
                                <th>Frequency</th>
                                <th>Media Index</th>
                                <th>Unique Shows</th>
                            </tr>
                        </thead>
                        <?php $count = 0; ?>
                        @foreach($weeklyShowBreakdown as $breakdown)
                        <tr>
                            <td>{!! date("m/d/Y",strtotime($breakdown['end_date'])) !!}</td>
                            <td>{!! $breakdown['airs'] !!}</td>
                            <td>{!! number_format($breakdown['media_index'],2) !!}</td>
                            <td>{!! $breakdown['unique_shows'] !!}</td>
                        </tr>

                            <?php $count ++;?>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <h4 class="text-center">Top 10 Spots</h4>
            <div id="spot-distribution" style="width:100%;height: 300px;"></div>
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        Weekly Spot Breakdown
                    </div>
                </div>
                <div class="panel-body scrollable" data-height="300" style="overflow: hidden; width: auto; height: 300px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Week Ending</th>
                                <th>Frequency</th>
                                <th>Unique Spots</th>
                            </tr>
                        </thead>
                        <?php $count = 0; ?>
                        @foreach($weeklySpotBreakdown as $breakdown)
                        <tr>
                            <td>{!! date("m/d/Y",strtotime($breakdown['end_date'])) !!}</td>
                            <td>{!! $breakdown['airs'] !!}</td>
                            <td>{!! $breakdown['unique_spots'] !!}</td>
                        </tr>
                            <?php $count ++;?>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4 class="text-center">Raw Show Airings</h4>
            <table id="show-airings" class="table table-hover table-bordered results">
                <thead>
                    <th>Show</th>
                    <th>Grid Title</th>
                    <th>Day of Week</th>
                    <th>Air Date</th>
                    <th>Air Time</th>
                </thead>
                <tbody>
                    @foreach($showAirings as $airtime)
                    <tr>
                        <td>
                            {!! $airtime->title !!}
                        </td>
                        <td>
                            {!! $airtime->grid_title !!}
                        </td>
                        <td>{!! date("l",strtotime($airtime->air_date)) !!}</td>
                        <td>{!! date("m/d/Y",strtotime($airtime->air_date)) !!}</td>
                        <td>{!! date("H:i a",strtotime($airtime->air_date)) !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h4 class="text-center">Raw Spot Airings</h4>
            <table id="spot-airings" class="table table-hover table-bordered results">
                <thead>
                    <th>Spot</th>
                    <th>Day of Week</th>
                    <th>Air Date</th>
                    <th>Air Time</th>
                    <th>Day Part</th>
                </thead>
                <tbody>
                    @foreach($spotAirings as $airtime)
                    <tr>
                        <td>
                            {!! $airtime->title !!}
                        </td>
                        <td>{!! date("l",strtotime($airtime->air_date)) !!}</td>
                        <td>{!! date("m/d/Y",strtotime($airtime->air_date)) !!}</td>
                        <td>{!! date("h:i:s A",strtotime($airtime->air_date)) !!}</td>
                        <td>{!! $airtime->daypart_name !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif
@stop

@section('script')
<script>
$(function(){
    @if($showPie)
    var spotPie = AmCharts.makeChart("show-distribution", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $showPie->toJson() !!},
        "valueField": "freq",
        "titleField": "grid_title",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "amExport": {
            top : 0,
            right : 50,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
    @endif
    @if($spotPie)
    var spotPie = AmCharts.makeChart("spot-distribution", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $spotPie->toJson() !!},
        "valueField": "freq",
        "titleField": "title",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "amExport": {
            top : 0,
            right : 50,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
    @endif

    var showTable, spotTable;
    spotTable = $('#spot-airings').dataTable( {
        "pageLength": 10,


        "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        },
        "columns": [
            null,
            null,
            null,
            null,
            null
        ],
        "oTableTools": {
            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    "sPdfMessage": "IMS Report - Networks Monitored"
                },
                "print"
            ]
        }
    });
    showTable = $('#show-airings').dataTable( {
        "pageLength": 10,


        "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        },
        "columns": [
            null,
            null,
            null,
            null,
            null
        ],
        "oTableTools": {
            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    "sPdfMessage": "IMS Report - Networks Monitored"
                },
                "print"
            ]
        }
    });
});
</script>
@stop
