@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-desktop"></i>
        Networks &bull; {!! $language->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('networks') !!}

    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center">
                <i class="fa fa-desktop"></i> Network Search
            </h4>
            <input type="hidden" name="language_id" id="language_id" value="{!! $language_id !!}" />
        @if($networks)
            <table id="report" class="table table-hover table-bordered results">
                <thead>
                    <th class="col-xs-2 hidden-xs"></th>
                    <th class="col-xs-3">Abbreviation</th>
                    <th class="col-xs-7">Name</th>
                </thead>
                <tbody>
                @foreach($networks as $network)
                    <tr>
                        <td class="hidden-xs">
                        @if($network->logo_location)
                            <img src="{!! "https://s3-us-west-2.amazonaws.com/ims-logos/".$network->logo_location !!}" class="gridlogo" data-order="{!! $network->abbr !!}" />
                        @endif
                        </td>
                        <td>
                            {!! Html::linkRoute('networks.show',$network->abbr, $network->id) !!}
                        </td>
                        <td>{!! $network->name !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
        </div>
    </div>
@stop

@section('script')
<script>
var oTable;
$(function(){
  oTable = $('#report').dataTable( {
    "pageLength": 100
  });
});
</script>
@stop
