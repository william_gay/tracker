@extends('cxp.layout')

@section('header')
    <h3>
        <i class="icon-desktop"></i>
        Network Breakdown
    </h3>
@stop

@section('content')

    @include('cxp.partials.filters.network-breakdown')
    <div class="row">
        <div class="col-xs-12">
            <h6 class="text-center">
                {{ $channelsSelected }}
            </h6>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h4 class="text-center">Show Category Breakdown</h4>
            <div id="show-distribution" style="width:100%;height: 350px;"></div>
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Weekly Show Breakdown
                    </div>
                </div>
                <div class="panel-body">
                    <div id="show-breakdown" style="width:100%;height: 300px;"></div>
                </div>
                <div class="panel-body scrollable" data-height="300" style="overflow: hidden; width: auto; height: 300px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Week Ending</th>
                                <th>Frequency</th>
                                <th>Media Index</th>
                                <th>Unique Shows</th>
                            </tr>
                        </thead>
                        @foreach($weeklyShowBreakdown as $breakdown)
                        <tr>
                            <td>{{ date("m/d/Y",strtotime($breakdown['end_date'])) }}</td>
                            <td>{{ $breakdown['airs'] }}</td>
                            <td>{{ number_format($breakdown['media_index'],2) }}</td>
                            <td>{{ $breakdown['unique_shows'] }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <h4 class="text-center">Spot Category Breakdown</h4>
            <div id="spot-distribution" style="width:100%;height: 350px;"></div>
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Weekly Spot Breakdown
                    </div>
                </div>
                <div class="panel-body">
                    <div id="spot-breakdown" style="width:100%;height: 300px;"></div>
                </div>
                <div class="panel-body scrollable" data-height="300" style="overflow: hidden; width: auto; height: 300px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Week Ending</th>
                                <th>Frequency</th>
                                <th>Media Index</th>
                                <th>Unique Spots</th>
                            </tr>
                        </thead>
                        @foreach($weeklySpotBreakdown as $breakdown)
                        <tr>
                            <td>{{ date("m/d/Y",strtotime($breakdown['end_date'])) }}</td>
                            <td>{{ $breakdown['airs'] }}</td>
                            <td>{{ number_format($breakdown['media_index'],2) }}</td>
                            <td>{{ $breakdown['unique_spots'] }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Hourly Show Breakdown
                    </div>
                </div>
                <div class="panel-body">
                    <div id="show-hour" style="width:100%;height: 350px;"></div>
                </div>
                <div class="panel-body scrollable" data-height="300" style="overflow: hidden; width: auto; height: 300px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Hour</th>
                                <th>Frequency</th>
                                <th>Media Index</th>
                                <th>Categories</th>
                            </tr>
                        </thead>
                        @foreach($showHourBar as $breakdown)
                        <tr>
                            <td>{{ $breakdown->hour }}</td>
                            <td>{{ $breakdown->freq }}</td>
                            <td>{{ number_format($breakdown->media, 2) }}</td>
                            <td>{{ $breakdown->category_count }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Hourly Spot Breakdown
                    </div>
                </div>
                <div class="panel-body">
                    <div id="spot-hour" style="width:100%;height: 350px;"></div>
                </div>
                <div class="panel-body scrollable" data-height="300" style="overflow: hidden; width: auto; height: 300px;">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Hour</th>
                                <th>Frequency</th>
                                <th>Media Index</th>
                                <th>Categories</th>
                            </tr>
                        </thead>
                        @foreach($spotHourBar as $breakdown)
                        <tr>
                            <td>{{ $breakdown->hour }}</td>
                            <td>{{ $breakdown->freq }}</td>
                            <td>{{ number_format($breakdown->media, 2) }}</td>
                            <td>{{ $breakdown->category_count }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
<script>
$(function(){
    @include('cxp.partials.network-breakdown.common-scripts')
    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);

    @if($showPie)
    var spotPie = AmCharts.makeChart("show-distribution", {
        "pathToImages": "{{URL::to('/')}}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {{ $showPie->toJson() }},
        "valueField": "freq",
        "titleField": "category",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "amExport": {
            top : 0,
            right : 50,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{{URL::to('/')}}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
    @endif
    @if($spotPie)
    var spotPie = AmCharts.makeChart("spot-distribution", {
        "pathToImages": "{{URL::to('/')}}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {{ $spotPie->toJson() }},
        "valueField": "freq",
        "titleField": "category",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "amExport": {
            top : 0,
            right : 50,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{{URL::to('/')}}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
    @endif

    var showHourBar = AmCharts.makeChart("show-hour", {
        "type": "serial",
        "theme": "light",
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 1,
            "position": "right",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "dataProvider": {{ $showHourBar }},
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.3,
            "gridAlpha": 0
        }],
        "graphs": [{
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Category Count",
            "type": "column",
            "color": "#000000",
            "valueField": "category_count"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Frequency",
            "type": "column",
            "color": "#000000",
            "valueField": "freq"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Media Index",
            "type": "column",
            "color": "#000000",
            "valueField": "media"
        }],
        "categoryField": "hour",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "export": {
            "enabled": true
         }
    });

    var spotHourBar = AmCharts.makeChart("spot-hour", {
        "type": "serial",
        "theme": "light",
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 1,
            "position": "right",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "dataProvider": {{ $spotHourBar }},
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.3,
            "gridAlpha": 0
        }],
        "graphs": [{
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Category Count",
            "type": "column",
            "color": "#000000",
            "valueField": "category_count"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Frequency",
            "type": "column",
            "color": "#000000",
            "valueField": "freq"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Media Index",
            "type": "column",
            "color": "#000000",
            "valueField": "media"
        }],
        "categoryField": "hour",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "export": {
            "enabled": true
         }
    });

    var showBreakdown = AmCharts.makeChart("show-breakdown", {
        "type": "serial",
        "theme": "none",
        "pathToImages": "{{URL::to('/')}}/assets/img/",
        "dataProvider": {{ json_encode($weeklyShowBreakdown) }},
        "legend": {
            "useGraphSettings": true
        },
        "valueAxes": [{
            "id":"v1",
            "axisColor": "#FF6600",
            "axisThickness": 2,
            "gridAlpha": 0,
            "offset": 50,
            "axisAlpha": 1,
            "position": "left"
        }, {
            "id":"v2",
            "axisColor": "#FCD202",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "left"
        }, {
            "id":"v3",
            "axisColor": "#B0DE09",
            "axisThickness": 2,
            "gridAlpha": 0,
            "offset": 50,
            "axisAlpha": 1,
            "maximum": 250,
            "minimum": 1,
            "reversed": true,
            "position": "right"
        }],
        "graphs": [{
            "valueAxis": "v1",
            "axisColor": "#FF6600",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Frequency",
            "valueField": "airs"
        }, {
            "valueAxis": "v2",
            "axisColor": "#FCD202",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Media Index",
            "valueField": "media_index"
        }, {
            "valueAxis": "v3",
            "axisColor": "#B0DE09",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Unique Shows",
            "valueField": "unique_shows"
        }],
        "zoomOutButtonRollOverAlpha": 0.15,
        "chartCursor": {
            "categoryBalloonDateFormat": "MMM DD JJ:NN",
            "cursorPosition": "mouse",
            "showNextAvailable":true
        },
        "autoMarginOffset": 5,
        "columnWidth": 1,
        "categoryField": "date",
        "categoryAxis": {
            "minPeriod": "hh",
            "parseDates": true
        },
        "amExport": {
            top : 0,
            right : 150,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{{URL::to('/')}}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });

    var spotBreakdown = AmCharts.makeChart("spot-breakdown", {
        "type": "serial",
        "theme": "none",
        "pathToImages": "{{URL::to('/')}}/assets/img/",
        "dataProvider": {{ json_encode($weeklySpotBreakdown) }},
        "legend": {
            "useGraphSettings": true
        },
        "valueAxes": [{
            "id":"v1",
            "axisColor": "#FF6600",
            "axisThickness": 2,
            "gridAlpha": 0,
            "offset": 50,
            "axisAlpha": 1,
            "position": "left"
        }, {
            "id":"v2",
            "axisColor": "#FCD202",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "left"
        }, {
            "id":"v3",
            "axisColor": "#B0DE09",
            "axisThickness": 2,
            "gridAlpha": 0,
            "offset": 50,
            "axisAlpha": 1,
            "maximum": 250,
            "minimum": 1,
            "reversed": true,
            "position": "right"
        }],
        "graphs": [{
            "valueAxis": "v1",
            "axisColor": "#FF6600",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Frequency",
            "valueField": "airs"
        }, {
            "valueAxis": "v2",
            "axisColor": "#FCD202",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Media Index",
            "valueField": "media_index"
        }, {
            "valueAxis": "v3",
            "axisColor": "#B0DE09",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Unique Spots",
            "valueField": "unique_spots"
        }],
        "zoomOutButtonRollOverAlpha": 0.15,
        "chartCursor": {
            "categoryBalloonDateFormat": "MMM DD JJ:NN",
            "cursorPosition": "mouse",
            "showNextAvailable":true
        },
        "autoMarginOffset": 5,
        "columnWidth": 1,
        "categoryField": "date",
        "categoryAxis": {
            "minPeriod": "hh",
            "parseDates": true
        },
        "amExport": {
            top : 0,
            right : 150,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{{URL::to('/')}}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
});
</script>
@stop
