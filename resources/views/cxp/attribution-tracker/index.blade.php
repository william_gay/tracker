@extends('cxp.layout')

@section('style')
<style>
.gauges{
    height: 300px;
}
.performance{
    height: 100px;
}
.centerCharts {
    height: 250px;
}
.scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
}
</style>
@endsection

@section('header')
    <h3>
        <i class="fa fa-heartbeat"></i>
        Attribution Tracker
    </h3>
@endsection

@section('content')

@include('cxp.partials.attribution-tracker.filters.dashboard')

<div class="row">
    <div class="col-md-5">
        <div class="panel panel-dark" data-collapsed="0"> <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">Web Attribution</div>
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                </div>
            </div> <!-- panel body -->
             <div class="panel-body">
                <div id="chartdiv" class="gauges"></div>
                <div id="gaugeChart0" class="gauges"></div>
                <div id="gaugeChart1" class="gauges"></div>
            </div>
        </div>
        <div class="panel panel-dark" data-collapsed="0"> <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">Overall Performance</div>
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                </div>
            </div> <!-- panel body -->
             <div class="panel-body">
                <div id="performanceChart0" class="performance"></div>
                <div id="performanceChart1" class="performance"></div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="panel panel-dark" data-collapsed="0"> <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">TV Media</div>
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                </div>
            </div> <!-- panel body -->
             <div class="panel-body">
                <div id="bubbleChart" class="centerCharts"></div>
            </div>
        </div>
        <div class="panel panel-dark" data-collapsed="0"> <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">Call Attribution</div>
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                </div>
            </div> <!-- panel body -->
             <div class="panel-body">
                <div id="stackedBar" class="centerCharts"></div>
            </div>
        </div>
        <div class="panel panel-dark" data-collapsed="0"> <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">Amazon Attribution</div>
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                </div>
            </div> <!-- panel body -->
             <div class="panel-body">
                <div id="verticalBar" class="centerCharts"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){

    @include('cxp.partials.attribution-tracker.common-scripts')

    $('#startTime').datetimepicker({
    format: 'hh:mm A',
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-arrow-up",
      down: "fa fa-arrow-down",
      next: "fa fa-arrow-right",
      previous: "fa fa-arrow-left"
    }
  });
    $('#endTime').datetimepicker({
    format: 'hh:mm A',
    icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-arrow-up",
      down: "fa fa-arrow-down",
      next: "fa fa-arrow-right",
      previous: "fa fa-arrow-left"
    }
  });

    $('#product_picker').multiselect(productConfigurationSet);
    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);
    $('#daypart_picker').multiselect(daypartConfigurationSet);

});

var gaugeChart = AmCharts.makeChart( "chartdiv", {
  "type": "gauge",
  "theme": "light",
  "path": "http://www.amcharts.com/lib/3/",
  "pathToImages": "{!!URL::to('/')!!}/assets/img/",
  "axes": [ {
    "axisThickness": 1,
    "axisAlpha": 0.2,
    "tickAlpha": 0.2,
    "valueInterval": 20,
    "bands": [ {
      "color": "#cc4748",
      "endValue": 90,
      "startValue": 0
    }, {
      "color": "#fdd400",
      "endValue": 130,
      "startValue": 90
    }, {
      "color": "#84b761",
      "endValue": 220,
      "innerRadius": "95%",
      "startValue": 130
    } ],
    "bottomText": "160 Page Views",
    "bottomTextYOffset": -20,
    "endValue": 220
  } ],
  "arrows": [ {
             "color": "#84b761",
              "value": 160
  } ],
  "export": {
    "enabled": true
  }
} );

var gaugeChart = AmCharts.makeChart( "gaugeChart0", {
  "theme": "light",
  "type": "gauge",
  "pathToImages": "{!!URL::to('/')!!}/assets/img/",
  "axes": [ {
    "axisColor": "#67b7dc",
    "axisThickness": 3,
    "endValue": 100 ,
    "gridInside": false,
    "inside": false,
    "radius": "100%",
    "valueInterval": 100,
    "tickColor": "#67b7dc",
    "bottomText": "% New Users",
    "bottomTextColor": "#67b7dc"
  }, {
    "axisColor": "#fdd400",
    "axisThickness": 3,
    "endValue": 400,
    "radius": "80%",
    "valueInterval": 50,
    "tickColor": "#fdd400",
    "topText": "Users",
    "topTextColor": "#fdd400"
  } ],
  "arrows": [ {
    "color": "#67b7dc",
    "innerRadius": "20%",
    "nailRadius": 0,
    "radius": "85%",
    "value":  20
  },{
    "color": "#fdd400",
    "innerRadius": "20%",
    "nailRadius": 0,
    "radius": "85%",
    "value":  50
  } ],
  "export": {
    "enabled": true
  }
} );

var gaugeChart1 = AmCharts.makeChart( "gaugeChart1", {
  "theme": "light",
  "type": "gauge",
  "pathToImages": "{!!URL::to('/')!!}/assets/img/",
  "axes": [ {
    "axisColor": "#67b7dc",
    "axisThickness": 3,
    "endValue": 100 ,
    "gridInside": false,
    "inside": false,
    "radius": "100%",
    "valueInterval": 100,
    "tickColor": "#67b7dc",
    "bottomText": "Page Views Per Session",
    "bottomTextColor": "#67b7dc"
  }, {
    "axisColor": "#fdd400",
    "axisThickness": 3,
    "endValue": 400,
    "radius": "80%",
    "valueInterval": 50,
    "tickColor": "#fdd400",
    "topText": "Page Views",
    "topTextColor": "#fdd400"
  } ],
  "arrows": [ {
    "color": "#67b7dc",
    "innerRadius": "20%",
    "nailRadius": 0,
    "radius": "85%",
    "value":  50
  },{
    "color": "#fdd400",
    "innerRadius": "20%",
    "nailRadius": 0,
    "radius": "85%",
    "value":  90
  } ],
  "export": {
    "enabled": true
  }
} );

var chart = AmCharts.makeChart("chartdiv1", {
    "type": "serial",
    "theme": "light",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "autoMargins": false,
    "marginTop": 30,
    "marginLeft": 80,
    "marginBottom": 30,
    "marginRight": 50,
    "dataProvider": [{
        "category": "Evaluation",
        "excelent": 20,
        "good": 20,
        "average": 20,
        "poor": 20,
        "bad": 20,
        "limit": 78,
        "full": 100,
        "bullet": 65
    }],
    "valueAxes": [{
        "maximum": 100,
        "stackType": "regular",
        "gridAlpha": 0
    }],
    "startDuration": 1,
    "graphs": [{
        "columnWidth": 0.5,
        "lineColor": "#000000",
        "lineThickness": 3,
        "noStepRisers": true,
        "stackable": false,
        "type": "step",
        "valueField": "limit"
    }, {
        "fillAlphas": 0.8,
        "lineColor": "#19d228",
        "showBalloon": false,
        "type": "column",
        "valueField": "excelent"
    }, {
        "fillAlphas": 0.8,
        "lineColor": "#b4dd1e",
        "showBalloon": false,
        "type": "column",
        "valueField": "good"
    }, {
        "fillAlphas": 0.8,
        "lineColor": "#f4fb16",
        "showBalloon": false,
        "type": "column",
        "valueField": "average"
    }, {
        "fillAlphas": 0.8,
        "lineColor": "#f6d32b",
        "showBalloon": false,
        "type": "column",
        "valueField": "poor"
    }, {
        "fillAlphas": 0.8,
        "lineColor": "#fb7116",
        "showBalloon": false,
        "type": "column",
        "valueField": "bad"
    }, {
        "clustered": false,
        "columnWidth": 0.3,
        "fillAlphas": 1,
        "lineColor": "#000000",
        "stackable": false,
        "type": "column",
        "valueField": "bullet"
    }],
    "rotate": true,
    "columnWidth": 1,
    "categoryField": "category",
    "categoryAxis": {
        "gridAlpha": 0,
        "position": "left"
    }
});


var performanceChart0 = AmCharts.makeChart("performanceChart0", {
    "type": "serial",
    "rotate": true,
    "theme": "light",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "autoMargins": false,
    "marginTop": 30,
    "marginLeft": 80,
    "marginBottom": 30,
    "marginRight": 50,
    "dataProvider": [{
        "category": "CPI",
        "excelent": 20,
        "good": 20,
        "average": 20,
        "poor": 20,
        "bad": 20,
        "limit": 78,
        "full": 100,
        "bullet": 55
    }],
    "valueAxes": [{
        "maximum": 100,
        "stackType": "regular",
        "gridAlpha": 0
    }],
    "startDuration": 1,
    "graphs": [{
        "columnWidth": 0.5,
        "lineColor": "#000000",
        "lineThickness": 3,
        "noStepRisers": true,
        "stackable": false,
        "type": "step",
        "valueField": "limit"
    }, {
        "valueField": "full",
        "showBalloon": false,
        "type": "column",
        "lineAlpha": 0,
        "fillAlphas": 0.8,
        "fillColors": ["#19d228", "#f6d32b", "#fb2316"],
        "gradientOrientation": "horizontal",
    }, {
        "clustered": false,
        "columnWidth": 0.3,
        "fillAlphas": 1,
        "lineColor": "#000000",
        "stackable": false,
        "type": "column",
        "valueField": "bullet"
    }],
    "columnWidth": 1,
    "categoryField": "category",
    "categoryAxis": {
        "gridAlpha": 0,
        "position": "left"
    }
});

var performanceChart1 = AmCharts.makeChart("performanceChart1", {
    "type": "serial",
    "rotate": true,
    "theme": "light",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "autoMargins": false,
    "marginTop": 30,
    "marginLeft": 80,
    "marginBottom": 30,
    "marginRight": 50,
    "dataProvider": [{
        "category": "CPO",
        "excelent": 20,
        "good": 20,
        "average": 20,
        "poor": 20,
        "bad": 20,
        "limit": 70,
        "full": 100,
        "bullet": 65
    }],
    "valueAxes": [{
        "maximum": 100,
        "stackType": "regular",
        "gridAlpha": 0
    }],
    "startDuration": 1,
    "graphs": [{
        "columnWidth": 0.5,
        "lineColor": "#000000",
        "lineThickness": 3,
        "noStepRisers": true,
        "stackable": false,
        "type": "step",
        "valueField": "limit"
    }, {
        "valueField": "full",
        "showBalloon": false,
        "type": "column",
        "lineAlpha": 0,
        "fillAlphas": 0.8,
        "fillColors": ["#19d228", "#f6d32b", "#fb2316"],
        "gradientOrientation": "horizontal",
    }, {
        "clustered": false,
        "columnWidth": 0.3,
        "fillAlphas": 1,
        "lineColor": "#000000",
        "stackable": false,
        "type": "column",
        "valueField": "bullet"
    }],
    "columnWidth": 1,
    "categoryField": "category",
    "categoryAxis": {
        "gridAlpha": 0,
        "position": "left"
    }
});

var verticalBar = AmCharts.makeChart("verticalBar", {
        "type": "serial",
        "theme": "none",
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "dataProvider": {!! $amazonAtt->toJson() !!},
        "valueAxes": [{
            "id":"v1",
            "axisColor": "#FF6600",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "left",
            "title": "Page Views"
        }, {
            "id":"v2",
            "axisColor": "#FCD202",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "right",
            "title": "Orders"
        }],
        "graphs": [{
            "valueAxis": "v1",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Page Views",
            "valueField": "airs"
        }, {
            "valueAxis": "v2",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Orders",
            "valueField": "media"
        }],
        "zoomOutButtonRollOverAlpha": 0.15,
        "chartCursor": {
            "categoryBalloonDateFormat": "MMM DD JJ:NN",
            "cursorPosition": "mouse",
            "showNextAvailable":true
        },
        "autoMarginOffset": 5,
        "columnWidth": 1,
        "categoryField": "date",
        "categoryAxis": {
            "minPeriod": "hh",
            "parseDates": true
        },
        "amExport": {
            top : 0,
            right : 150,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });

 var bubbleChart = AmCharts.makeChart("bubbleChart", {
     "type": "xy",
     "theme": "light",
     "pathToImages": "{!!URL::to('/')!!}/assets/img/",
     "marginRight": 80,
     "marginTop": 17,
     "dataProvider": [{
         "y": 10,
         "x": 14,
         "value": 59,
         "y2": -5,
         "x2": 0,
         "value2": 44
     }, {
         "y": 5,
         "x": 3,
         "value": 50,
         "y2": -15,
         "x2": -8,
         "value2": 12
     }, {
         "y": -10,
         "x": -3,
         "value": 19,
         "y2": -4,
         "x2": 6,
         "value2": 35
     }, {
         "y": -6,
         "x": 5,
         "value": 65,
         "y2": -5,
         "x2": -6,
         "value2": 168
     }, {
         "y": 15,
         "x": -4,
         "value": 92,
         "y2": -10,
         "x2": -8,
         "value2": 102
     }, {
         "y": 13,
         "x": 1,
         "value": 8,
         "y2": -2,
         "x2": -3,
         "value2": 41
     }, {
         "y": 1,
         "x": 6,
         "value": 35,
         "y2": 0,
         "x2": 1,
         "value2": 16
     }],
     "valueAxes": [{
         "position": "bottom",
         "axisAlpha": 0
     }, {
         "minMaxMultiplier": 1.2,
         "axisAlpha": 0,
         "position": "left"
     }],
     "startDuration": 1.5,
     "graphs": [{
         "balloonText": "x:<b>[[x]]</b> y:<b>[[y]]</b><br>value:<b>[[value]]</b>",
         "bullet": "bubble",
         "lineAlpha": 0,
         "valueField": "value",
         "xField": "x",
         "yField": "y",
         "fillAlphas": 0,
         "bulletBorderAlpha": 0.2,
         "maxBulletSize": 80

     }, {
         "balloonText": "x:<b>[[x]]</b> y:<b>[[y]]</b><br>value:<b>[[value]]</b>",
         "bullet": "bubble",
         "lineAlpha": 0,
         "valueField": "value2",
         "xField": "x2",
         "yField": "y2",
         "fillAlphas": 0,
         "bulletBorderAlpha": 0.2,
         "maxBulletSize": 80

     }],
     "marginLeft": 46,
     "marginBottom": 35,
     "chartScrollbar": {},
     "chartCursor": {},
     "export": {
         "enabled": true
     }
 });

var stackedBar = AmCharts.makeChart("stackedBar", {
        "type": "serial",
        "theme": "none",
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "dataProvider": {!! $callAtt->toJson() !!},
        "valueAxes": [{
            "id":"v1",
            "axisColor": "#FF6600",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "left",
            "title": "Calls"
        }, {
            "id":"v2",
            "axisColor": "#FCD202",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "right",
            "title": "Sales"
        }],
        "graphs": [{
            "valueAxis": "v1",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Calls",
            "valueField": "airs"
        }, {
            "valueAxis": "v2",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Sales",
            "valueField": "media"
        }],
        "zoomOutButtonRollOverAlpha": 0.15,
        "chartCursor": {
            "categoryBalloonDateFormat": "MMM DD JJ:NN",
            "cursorPosition": "mouse",
            "showNextAvailable":true
        },
        "autoMarginOffset": 5,
        "columnWidth": 1,
        "categoryField": "date",
        "categoryAxis": {
            "minPeriod": "hh",
            "parseDates": true
        },
        "amExport": {
            top : 0,
            right : 150,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
</script>
@endsection
