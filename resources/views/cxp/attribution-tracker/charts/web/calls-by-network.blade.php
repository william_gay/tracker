var chart = AmCharts.makeChart("calls-by-network", {
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "theme": "light",
    "type": "serial",
    "dataProvider": [{
        "network": "CMT",
        "calls": 425,
        "color": "#7F8DC9"
    }, {
        "network": "ABC",
        "calls": 350,
        "color": "#7F8DB9"
    }, {
        "network": "BRAVO",
        "calls": 279,
        "color": "#7F8DA3"
    }, {
        "network": "SyFy",
        "calls": 255,
        "color": "#7F8DA1"
    }, {
        "network": "PIVOT",
        "calls": 246,
        "color": "#7F8DA2"
    }, {
        "network": "GSN",
        "calls": 156,
        "color": "#7F8DA4"
    }],
    "valueAxes": [{
        "title": "Number of Page Views"
    }],
    "graphs": [{
        "balloonText": "Page Views for [[category]]: [[value]]",
        "fillAlphas": 1,
        "lineAlpha": 0.2,
        "title": "Page Views",
        "type": "column",
        "valueField": "calls"
    }],
    "rotate": true,
    "categoryField": "network",
    "categoryAxis": {
        "gridPosition": "start",
        "fillAlpha": 0.05,
        "position": "left"
    },
    "amExport": {
        top : 30,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

