var chart = AmCharts.makeChart("drilldown-by-daypart", {
    "type": "serial",
    "theme": "light",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "dataDateFormat": "YYYY-MM-DD JJ:NN",
    "valueAxes": [{
        "integersOnly": true,
        "reversed": false,
        "axisAlpha": 0,
        "dashLength": 5,
        "gridCount": 10,
        "position": "left",
        "title": "Number of Page Views"
    }],
    "legend": {
        "useGraphSettings": true
    },
    "startDuration": 0.5,
    "graphs": [{
        "valueAxis": "lifetime",
        "bullet": "round",
        "title": "Lifetime",
        "connect": true,
        "valueField": "lifetime",
        "fillAlphas": 0
    }, {
        "valueAxis": "bravo",
        "bullet": "round",
        "title": "BRAVO",
        "valueField": "bravo",
        "fillAlphas": 0
    }, {
        "valueAxis": "pivot",
        "bullet": "round",
        "title": "PIVOT",
        "valueField": "pivot",
        "fillAlphas": 0
    }, {
        "valueAxis": "syfy",
        "bullet": "round",
        "title": "SYFY",
        "valueField": "syfy",
        "fillAlphas": 0
    }, {
        "valueAxis": "amc",
        "bullet": "round",
        "title": "AMC",
        "valueField": "amc",
        "fillAlphas": 0
    }],
    "chartCursor": {
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "date",
    "categoryAxis": {
        "gridPosition": "start",
        "parseDates": true,
        "dateFormats": [{period:'fff',format:'JJ:NN:SS'},
            {period:'ss',format:'JJ:NN:SS'},
            {period:'mm', format:'L:NN A'},
            {period:'hh',format:'L:NN A'},
            {period:'DD',format:'MMM DD'},
            {period:'WW',format:'MMM DD'},
            {period:'MM',format:'MMM'},
            {period:'YYYY',format:'YYYY'}
        ],
        "minPeriod": "mm",
        "axisAlpha": 0,
        "fillAlpha": 0.05,
        "fillColor": "#000000",
        "gridAlpha": 0,
        "position": "top"
    },
    "dataProvider": {!! $drillDown->toJson() !!}
});
