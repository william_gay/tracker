var chart = AmCharts.makeChart("cost-per-inquiry", {
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "type": "serial",
    "theme": "light",
    "titles": [
        {
            "text": "Cost Per Inquiry",
            "size": 15
        }
    ],
    "legend": {
        "equalWidths": false,
        "useGraphSettings": true,
        "valueAlign": "left",
        "valueWidth": 120
    },
    "dataProvider": [{
        "daypart": "Early Fringe",
        "cost": 1,
        "airings": 3,
        "cpi": 1
    }, {
        "daypart": "News",
        "cost": 1,
        "airings": 2,
        "cpi": 1
    }, {
        "daypart": "Late Night",
        "cost": 5,
        "airings": 10,
        "cpi": 1
    }, {
        "daypart": "Daytime",
        "cost": 1,
        "airings": 5,
        "cpi": 1
    }, {
        "daypart": "Primetime",
        "cost": 4,
        "airings": 8,
        "cpi": 1
    },
    {
        "daypart": "Overnight",
        "cost": 2,
        "airings": 4,
        "cpi": 1
    }],
    "valueAxes": [{
        "id": "daypart",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "bottom",
        "title": "Cost"
    }, {
        "id": "airings",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "right",
        "title": "Airings"
    }, {
        "id": "cpi",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "bottom"
    }],
    "graphs": [{
        "balloonText": "[[value]]",
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "useLineColorForBulletBorder": true,
        "labelPosition": "right",
        "labelText": "[[value]]",
        "fillAlphas": 0,
        "valueField": "airings",
        "valueAxis": "airings"
    },
    {
        "balloonText": "[[cpi]]",
        "type": "column",
        "labelPosition": "bottom",
        "fillAlphas": 0.7,
        "legendValueText": "[[daypart]]",
        "valueField": "cost",
        "valueAxis": "daypart"
    }],
    "chartCursor": {
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
         "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
    },
    "categoryField": "daypart",
    "categoryAxis": {
        "autoGridCount": false,
        "axisColor": "#555555",
        "gridAlpha": 0.1,
        "gridColor": "#FFFFFF",
        "gridCount": 50
    },
    "amExport": {
        top : 30,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

