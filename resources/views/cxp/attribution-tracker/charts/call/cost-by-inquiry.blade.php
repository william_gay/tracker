var chart = AmCharts.makeChart("cost-by-inquiry", {
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "theme": "light",
    "type": "serial",
    "dataProvider": [{
        "network": "SyFy",
        "cost": 23.5,
        "color": "#397D02"
    }, {
        "network": "GSN",
        "cost": 35,
        "color": "#397D02"
    }, {
        "network": "CMT",
        "cost": 40,
        "color": "#397D02"
    }, {
        "network": "BRAVO",
        "cost": 42,
        "color": "#397D02"
    }, {
        "network": "ABC",
        "cost": 46,
        "color": "#ffff00"
    }, {
        "network": "PIVOT",
        "cost": 50,
        "color": "#ff0000"
    }],
    "valueAxes": [{
        "title": "Cost Per Inquiry"
    }],
    "graphs": [{
        "balloonText": "Cost per Inquiry $[[value]]",
        "fillAlphas": 1,
        "lineAlpha": 0.2,
        "colorField": "color",
        "title": "Cost",
        "type": "column",
        "valueField": "cost"
    }],
    "rotate": true,
    "categoryField": "network",
    "categoryAxis": {
        "gridPosition": "start",
        "fillAlpha": 0.05,
        "position": "left",
        "axisColor": "#DADADA",
        "gridAlpha": 0.07
    },
    "guides": [{
        value: 47,
        labelRotation: 90,
        lineColor: "#CC0000",
        postion: "top",
        lineAlpha: 1,
        fillAlpha: 0.2,
        fillColor: "#CC0000",
        label: "Target"
    }],
    "amExport": {
        top : 30,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

