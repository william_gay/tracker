@extends('cxp.layout')

@section('style')
<style>
.bar-chart {
    width: 100%;
    height: 350px;
}
.legend {
    height: 100px;
    overflow: auto;
}
.scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
}
.graph {
    width: 100%;
    height: 450px;
}
</style>
@endsection

@section('header')
    <h3>
        <i class="fa fa-heartbeat"></i>
        Attribution Tracker <small>Web</small>
    </h3>
@endsection

@section('content')
    @include('cxp.partials.attribution-tracker.filters.web')

    <div class="row">
        <div class="col-md-6">
            <div class="bar-chart" id="calls-by-network"></div>
        </div>
        <div class="col-md-6">
            <div class="bar-chart" id="cost-by-inquiry"></div>
            <table class="table table-bordered">
                <tr>
                    <td style="background-color: #397D02;" width="25"></td>
                    <td>Below Limit</td>
                    <td style="background-color: #ffff00;" width="25"></td>
                    <td>Approaching Limit</td>
                    <td style="background-color: #ff0000;" width="25"></td>
                    <td>Above Limit</td>
                </tr>
            </table>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="bar-chart" id="cost-per-inquiry"></div>
        </div>
    </div>
    <hr>
    <div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Honest.com</h3>
        <h4><span class="pull-left">April 7th, 2015</span><span class="pull-right">Late Fringe &dash; Overnight</h4>
        <div id="drilldown-by-daypart" class="graph"></div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){

    @include('cxp.partials.attribution-tracker.common-scripts')

    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);
    $('#daypart_picker').multiselect(daypartConfigurationSet);

    @include('cxp.attribution-tracker.charts.web.calls-by-network')
    @include('cxp.attribution-tracker.charts.web.cost-by-inquiry')
    @include('cxp.attribution-tracker.charts.web.cost-per-inquiry')
    @include('cxp.attribution-tracker.charts.web.drilldown-by-daypart')

});
</script>
@endsection
