<!-- Top Header Navbar -->
<header class="navbar navbar-fixed-top" id="header"><!-- set fixed position by adding class "navbar-fixed-top" -->
        <div class="navbar-inner">
            <!-- logo -->
            <div class="navbar-brand" id="navbar-brand">
                <a class="brand" href="{!! URL::to('dashboard') !!}">
                    @if(App::environment() == 'drtvlab')
                    <img src="{!! asset('assets/img/drtvlab/logo-small.gif') !!}" alt="{!! Config::get('app.site_config.company_short') !!} Dashboard" style="max-height: 34px;" />
                    @else
                    <img src="{!! asset('assets/img/logo.png') !!}" alt="{!! Config::get('app.site_config.company_short') !!} Dashboard"/>
                    @endif
                </a>
            </div>

@if (Sentry::check())
            <ul class="navbar-nav">

                <li class="visible-xs root-level {!! set_active(['dashboard']) !!}">
                    <a href="{!! URL::to('dashboard') !!}">
                        <i class="fa fa-tachometer"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="visible-xs root-level {!! set_active(['reports', 'reports/*']) !!}">
                    <a href="{!! URL::to('reports') !!}">
                        <i class="fa fa-signal"></i>
                        <span>Reports</span>
                    </a>
                </li>

                @if(Sentry::getUser()->hasAnyAccess(array('category-reports')))

                <li class="visible-xs root-level {!! set_active(['category-reports','category-reports/*']) !!}">
                    <a href="{!! URL::to('category-reports') !!}">
                        <i class="fa fa-sitemap"></i>
                        <span>Category Reports</span>
                    </a>
                </li>
                @endif

                @if (Sentry::getuser()->hasAccess('admin.view'))
                <li class="visible-xs root-level">
                    <a href="{!! URL::to('retail') !!}">
                        <i class="fa fa-shopping-cart"></i>
                        <span>Retail Report</span>
                        <span class="badge badge-success badge-roundless">New</span>
                    </a>
                </li>
                @endif

                <li class="visible-xs root-level  {!! set_active(['programs','programs/*']) !!}">
                    <a href="{!! URL::to('programs') !!}">
                        <i class="fa fa-video-camera"></i>
                        <span>Shows</span>
                    </a>
                </li>

                <li class="visible-xs root-level {!! set_active(['spots','spots/*']) !!}">
                    <a href="{!! URL::to('spots') !!}">
                        <i class="fa fa-tint"></i>
                        <span> Spots</span>
                    </a>
                </li>

                @if(Sentry::getUser()->hasAccess('reports.national-networks'))
                <li class="visible-xs root-level {!! set_active(['networks','networks/*']) !!}">
                    <a href="{!! URL::to('networks') !!}">
                        <i class="fa fa-desktop"></i>
                        <span>Networks</span>
                    </a>
                </li>
                @endif

                <li class="visible-xs root-level {!! set_active(['support']) !!}">
                    <a href="{!! URL::to('support') !!}">
                        <i class="fa fa-question-circle"></i>
                        <span>Support</span>
                    </a>
                </li>
                @if (Sentry::getuser()->hasAccess('admin.view'))

                <li class="visible-xs root-level">
                    <a href="/admin" target="_blank">
                        <i class="fa fa-cog left"></i>
                        <span>Admin Panel</span>
                    </a>
                </li>
                @endif
                <li class="visible-xs root-level">
                    <a href="{!! route('user.logout') !!}">
                        <i class="fa fa-sign-out right"></i>
                        <span>Log Out</span>
                    </a>
                </li>

            </ul>


            <!-- notifications and other links -->
            <ul class="nav navbar-right pull-right">


                {{-- <!-- Search Bar -->
                <li id="search" class="root-level search-input-collapsed">
                    <!-- add class "search-input-collapsed" to auto collapse search input -->
                    <form method="get" action="">
                        <input type="text" name="q" class="search-input" placeholder="Search something...">
                        <button type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </form>
                </li> --}}

                <li class="sep"></li>

                <!-- raw links -->

                @if (Sentry::getuser()->hasAccess('admin.view'))

                    <li>
                        <a href="/admin" target="_blank">
                            <i class="fa fa-cog left"></i>
                            Admin Panel
                        </a>
                    </li>
                @endif

                <li class="sep"></li>


                <!-- dropdowns -->

                <li class="dropdown hidden-xs">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        Settings
                        <i class="fa fa-chevron-down right"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>{!! link_to_route('user.settings','Profile') !!}</li>
                        <li>{!! link_to_route('user.alerts','Alerts') !!}</li>
                    </ul>
                </li>

                <li class="sep"></li>

                <li class="dropdown">

                    <!-- dropdown menu (messages) -->
                    <ul class="dropdown-menu">
                        <li class="external">
                            <a href="{!! route('user.alerts') !!}">You have <strong>{!! alerts_configured() !!}</strong> {!! Lang::choice('alerts.alerts',alerts_configured()) !!}  Configured.</a>
                        </li>
                    </ul>

                </li>



                <li class="sep"></li>

                <li>
                    <a href="{!! route('user.logout') !!}">
                        Log Out<i class="fa fa-sign-out right"></i>
                    </a>
                </li>


                <!-- mobile only -->
                <li class="visible-xs">

                    <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                    <div class="horizontal-mobile-menu visible-xs">
                        <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>

                </li>

            </ul>
@else
            <ul class="nav navbar-right pull-right">
                <li>
                <a href="{!! route('user.login') !!}">
                    Log In<i class="fa fa-sign-in right"></i>
                </a>
                </li>

                <!-- mobile only -->
                <li class="visible-xs">

                    <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                    <div class="horizontal-mobile-menu visible-xs">
                        <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>

                </li>

            </ul>
@endif

        </div>

</header>

    <!-- END Top Header Navbar -->
