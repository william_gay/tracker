<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-shadow">
            <div class="panel-heading">
                <div class="panel-title">
                    Network Airing Comparison
                </div>
            </div>
            <div class="panel-body">
                <div id="longForm" style="width:100%; height:675px; font-size:11px;"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
document.addEventListener('DOMContentLoaded',function(){
//=============================================================================
// Longform Network Airing Comparison Chart
//=============================================================================
var longFormChart = AmCharts.makeChart("longForm", {
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "channel_name",
    "rotate": true,
    "placeholder": "Select a Channel",
    "allowClear": true,
    "startDuration": 1,
    "categoryAxis": {
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[title]] of [[category]]:[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Total Program Airings for Current Period",
            "type": "column",
            "valueField": "air_count"
        },
        {
            "balloonText": "[[title]] of [[category]]:[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-2",
            "title": "Total Program Airings for Previous Period",
            "type": "column",
            "valueAxis": "ValueAxis-2",
            "valueField": "prev_air_count"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Total Program Airings for Current Period"
        },
        {
            "id": "ValueAxis-2",
            "position": "right",
            "gridAlpha": 0,
            "title": "Total Program Airings for Previous Period"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {},
    "dataProvider": {!! $longForms->toJson() !!}
});
});
</script>