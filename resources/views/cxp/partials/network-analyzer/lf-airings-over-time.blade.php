<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-shadow">
            <div class="panel-heading">
                <div class="panel-title">
                    Network Airings Over Time
                </div>
            </div>
            <div class="panel-body">
                <div id="longAirings" style="width:100%; height:400px; font-size:11px;"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
//=============================================================================
// Longform Total Airings Line Graph
//=============================================================================
var chart = AmCharts.makeChart("longAirings", {
    "type": "serial",
    // "theme": "light",
     "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "dataProvider": {!! $longFormAiringsOverTime->toJson() !!},
    "valueAxes": [{
        "logarithmic": true,
        "dashLength": 1,
        "guides": [{
            "dashLength": 6,
            "inside": true,
            "label": "average",
            "lineAlpha": 1,
            "value": 90.4,
            "guides": [{
                "fillAlphas": 1,
                "fillColor": "#000000",
                "inside": true,
                "lineAlpha": 1,
                "toValue": 20,
                "value": 10
            }]
        }],
        "position": "left"
    }],
    "graphs": [{
        "bullet": "round",
        "id": "g1",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 7,
        "lineThickness": 2,
        "title": "Price",
        "type": "smoothedLine",
        "useLineColorForBulletBorder": true,
        "valueField": "air_count",
        "fillAlphas": .15
    }],
    "chartScrollbar": {},
    "chartCursor": {
        "cursorPosition": "mouse"
    },
    "dataDateFormat": "YYYY-MM-DD",
    "categoryField": "air_date",
    "categoryAxis": {
        "parseDates": true
    }
});
</script>