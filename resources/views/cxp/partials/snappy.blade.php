@if( App::environment() == 'production')
<script
    src="//d2s6cp23z9c3gz.cloudfront.net/js/embed.widget.min.js"
    data-position="top right"
    data-domain="imsreport.besnappy.com"
    data-contact="1"
    data-title="Support"
    data-background="#ffa812">
</script>
@endif
