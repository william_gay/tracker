
@if (Sentry::check())

<!-- Side Navbar -->
<div class="sidebar-menu">

<!--       <div class="text-center">
                <a href="javascript: toggle_sidebar_menu(false);" class="btn btn-default"><i class="fa fa-bars"></i></a>
            </div> -->

        <ul id="main-menu">

            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            {{-- <li id="search" class="root-level">
                <form method="get" action="">
                    <input type="text" name="q" class="search-input" placeholder="Search something..." style="">
                    <button type="submit" style="">
                        <i class="fa fa-search"></i>
                    </button>
                </form>
            </li> --}}

            <li class="root-level {!! set_active(['dashboard']) !!}">
                <a href="{!! URL::to('dashboard') !!}">
                    <i class="fa fa-tachometer"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="root-level has-sub {!! set_active(['reports', 'reports/*']) !!}">
                <a href="{!! URL::to('reports') !!}">
                    <i class="fa fa-signal"></i>
                    <span>Media Reports</span>
                </a>
                <ul>
                    <li>
                        <a href="{!! URL::to('reports') !!}">
                            <span>All Reports</span>
                        </a>
                    </li>
<!-- National  -->
                    <li class="has-sub">
                        <a href="#">
                            <span>National Long Form</span>
                        </a>
                        <ul>
                            <li>
                                    @if(Sentry::getUser()->hasAccess('reports.national-shows-new'))
                                        <a href="/programs/new">
                                            New Shows Last Week
                                            <span class="badge badge-secondary badge-roundless">{!! getNewPrograms() !!}</span>
                                        </a>
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">
                                            New Shows Last Week
                                            <span class="badge badge-secondary badge-roundless">{!! getNewPrograms() !!}</span>
                                        </a>
                                    @endif
                            </li>
                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Weekly Reports
                                </a>
                            <ul>
                                @if(Sentry::getUser()->hasAccess('reports.category-report'))
                                <li>
                                    <a href="/categories/long-form">Category Report</a>
                                </li>
                                @endif

                            @foreach($reportsMenu->long_form as $report)
                                @if($report['interval'] == 'weekly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">
                                            {!! $report['name'] !!}
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Monthly Reports
                                </a>
                            <ul>

                            @foreach($reportsMenu->long_form as $report)
                                @if($report['interval'] == 'monthly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">
                                            {!! $report['name'] !!}
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>
                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Annual Reports
                                </a>
                            <ul>

                            @foreach($reportsMenu->long_form as $report)
                                @if($report['interval'] == 'yearly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">
                                            {!! $report['name'] !!}
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li>
                                   @if(Sentry::getUser()->hasAccess('reports.national-shows'))
                                        {!! link_to_route('programs.all', 'Show Search', array('language_id' => 1)) !!}
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">Show Search</a>
                                    @endif
                            </li>
                        </ul>
                    </li>

                    <li class="has-sub">
                        <a href="#">
                            <span>National Short Form</span>
                        </a>
                        <ul>
                            <li>
                                    @if(Sentry::getUser()->hasAccess('reports.national-spots-new'))
                                        <a href="/spots/new">
                                            New Spots Last Week
                                            <span class="badge badge-secondary badge-roundless">{!! getNewSpots() !!}</span>
                                        </a>
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">
                                            New Spots Last Week
                                            <span class="badge badge-secondary badge-roundless">{!! getNewSpots() !!}</span>
                                        </a>
                                    @endif
                            </li>
                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Weekly Reports
                                </a>
                            <ul>
                                @if(Sentry::getUser()->hasAccess('reports.category-report'))
                                <li>
                                    <a href="/categories/short-form">Category Report</a>
                                </li>
                                @endif
                            @foreach($reportsMenu->short_form as $report)
                                @if($report['interval'] == 'weekly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">
                                        {!! $report['name'] !!}
                                        @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                        <span class="badge badge-secondary badge-roundless">New</span>
                                        @endif
                                        @if($report['id'] == 72)
                                            <span class="badge badge-secondary badge-roundless">Admin Only</span>
                                        @endif
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Monthly Reports
                                </a>
                            <ul>

                            @foreach($reportsMenu->short_form as $report)
                                @if($report['interval'] == 'monthly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">
                                            {!! $report['name'] !!}
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Annual Reports
                                </a>
                            <ul>

                            @foreach($reportsMenu->short_form as $report)
                                @if($report['interval'] == 'yearly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">
                                            {!! $report['name'] !!}
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li>

                                @if(Sentry::getUser()->hasAccess('reports.national-spots'))
                                    <a href="/spots">Spot Search</a>
                                @else
                                    <a data-toggle="modal" data-target="#upgradeModal">Spot Search</a>
                                @endif

                            </li>
                        </ul>
                    </li>
<!-- /END National -->


<!-- Spanish -->
                    <li class="has-sub">
                        <a href="#">
                            <span>National Spanish Long Form</span>
                        </a>
                        <ul>
                            <li>
                                    @if(Sentry::getUser()->hasAccess('reports.spanish-shows-new'))
                                        <a href="/programs/new?language_id=2">
                                            New Shows Last Week
                                        <span class="badge badge-secondary badge-roundless">{!! getNewPrograms(2) !!}</span>
                                        </a>
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">
                                            New Shows Last Week
                                            <span class="badge badge-secondary badge-roundless">{!! getNewPrograms(2) !!}</span>
                                        </a>
                                    @endif
                            </li>
                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Weekly Reports
                                </a>
                            <ul>

                            @foreach($reportsMenu->spanish_long_form as $report)
                                @if($report['interval'] == 'weekly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                        <li>
                                            <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                        </li>
                                        @else
                                        <li>
                                            <a href="/reports/show/{!! $report['id'] !!}">
                                            {!! $report['name'] !!}
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                            </a>
                                        </li>
                                        @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Monthly Reports
                                </a>
                            <ul>

                            @foreach($reportsMenu->spanish_long_form as $report)
                                @if($report['interval'] == 'monthly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">
                                            {!! $report['name'] !!}
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li>
                            @if(Sentry::getUser()->hasAccess('reports.spanish-shows'))
                                {!! link_to_route('programs.all', 'Show Search', array('language_id' => 2)) !!}
                            @else
                                <a data-toggle="modal" data-target="#upgradeModal">Show Search</a>
                            @endif
                            </li>
                        </ul>
                    </li>

                    <li class="has-sub">
                        <a href="#">
                            <span>National Spanish Short Form</span>
                        </a>
                        <ul>
                            <li>
                                    @if(Sentry::getUser()->hasAccess('reports.spanish-spots-new'))
                                        <a href="/spots/new?language_id=2">
                                            New Spots Last Week
                                            <span class="badge badge-secondary badge-roundless">{!! getNewSpots(2) !!}</span>
                                        </a>
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">
                                            New Spots Last Week
                                            <span class="badge badge-secondary badge-roundless">{!! getNewSpots(2) !!}</span>
                                        </a>
                                    @endif
                            </li>
                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Weekly Reports
                                </a>
                            <ul>

                            @foreach($reportsMenu->spanish_short_form as $report)
                                @if($report['interval'] == 'weekly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">
                                            {!! $report['name'] !!}
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Monthly Reports
                                </a>
                            <ul>

                            @foreach($reportsMenu->spanish_short_form as $report)
                                @if($report['interval'] == 'monthly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">
                                            {!! $report['name'] !!}
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li>

                                @if(Sentry::getUser()->hasAccess('reports.spanish-spots'))
                                    <a href="/spots?language_id=2">Spot Search</a>
                                @else
                                    <a data-toggle="modal" data-target="#upgradeModal">Spot Search</a>
                                @endif

                            </li>
                        </ul>
                    </li>
<!-- /END Spanish -->

@if(Sentry::getUser()->hasAccess('reports.australia'))
<!-- Australia -->

                    <li class="has-sub">
                        <a href="#">
                            <span>Australia Short Form</span>
                        </a>
                        <ul>
                            <li>
                                    @if(Sentry::getUser()->hasAccess('reports.australian-spot-search-new'))

                                        <a href="/spots/new?language_id=4">
                                            New Spots Last Week
                                            <span class="badge badge-secondary badge-roundless">{!! getNewSpots(4) !!}</span>
                                        </a>
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">
                                            New Spots Last Week
                                            <span class="badge badge-secondary badge-roundless">{!! getNewSpots(4) !!}</span>
                                        </a>
                                    @endif
                            </li>
                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Weekly Reports
                                </a>
                            <ul>

                            @foreach($reportsMenu->aus_short_form as $report)
                                @if($report['interval'] == 'weekly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">{!! $report['name'] !!}</a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li class="has-sub">
                                <a href="{!! URL::to('reports') !!}">
                                    Monthly Reports
                                </a>
                            <ul>

                            @foreach($reportsMenu->aus_short_form as $report)
                                @if($report['interval'] == 'monthly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <li>
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! $report['name'] !!}</a>
                                    </li>
                                    @else
                                    <li>
                                        <a href="/reports/show/{!! $report['id'] !!}">{!! $report['name'] !!}</a>
                                    </li>
                                    @endif
                                @endif
                            @endforeach
                            </ul>
                            </li>

                            <li>

                                @if(Sentry::getUser()->hasAccess('reports.australian-spot-search-new'))
                                    <a href="/spots?language_id=4">Spot Search</a>
                                @else
                                    <a data-toggle="modal" data-target="#upgradeModal">Spot Search</a>
                                @endif

                            </li>
                        </ul>
                    </li>

<!-- /END Australia -->
@endif

                </ul>
            </li>
            @if(Sentry::getUser()->hasAnyAccess(array('reports.network-report')))
            <li class="root-level has-sub {!! set_active(['network-analyzer','network-analyzer/*']) !!}">
                <a href="{!! URL::to('network-analyzer') !!}">
                    <i class="fa fa-desktop"></i>
                    <span>Network Explorer</span>
                </a>
                <ul>
                    <li>
                        <a href="{!! URL::to('network-analyzer') !!}">
                            <span>By Airings</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! URL::to('network-analyzer/categories') !!}">
                            <span>By Categories</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! URL::to('network-analyzer/products') !!}">
                            <span>By Products</span>
                        </a>
                    </li>
                    @if (Sentry::getUser()->hasAccess('reports.sales-optimizer'))
                    <li>
                        <a href="{!! URL::route('sales-optimizer.home') !!}">
                            <span>Sales Optimizer</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! URL::route('sales-optimizer.comparison') !!}">
                            <span>Sales Optimizer Comparison</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! URL::route('sales-optimizer.products') !!}">
                            <span>Sales Optimizer Products</span>
                        </a>
                    </li>
                    @endif
                </ul>

            </li>
            @endif
            @if(Sentry::getUser()->hasAccess('reports.attribution-tracker'))
            <li class="root-level {!! set_active(['attribution-tracker','attribution-tracker/*']) !!}">
                <a href="{!! URL::route('attribution-tracker.index') !!}">
                    <i class="fa fa-heartbeat"></i>
                    <span>Attribution Tracker</span>
                </a>
                <ul>
                    <!--
                    <li>
                        <a href="{!! URL::route('attribution-tracker.index') !!}">
                            <span>Dashboard</span>
                        </a>
                    </li>
                    -->
                    <li>
                        <a href="{!! URL::route('attribution-tracker.index') !!}">
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! URL::route('attribution-tracker.call') !!}">
                            <span>Call Attribution</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! URL::route('attribution-tracker.web') !!}">
                            <span>Web Attribution</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            @if(Sentry::getUser()->hasAnyAccess(array('category-reports.*')))
            <li class="root-level {!! set_active(['category-reports','category-reports/*']) !!}">
                <a href="{!! URL::to('category-reports') !!}">
                    <i class="fa fa-sitemap"></i>
                    <span>Custom Reports</span>
                </a>
            </li>
            @endif
            @if (Sentry::getuser()->hasAccess('reports.retail'))
            <li class="root-level {!! set_active(['retail-report', 'retail-report/*']) !!}">
                <a href="{!! URL::route('retail-report.monthly') !!}">
                    <i class="fa fa-shopping-cart"></i>
                    <span>Retail Analyzer</span>
                </a>
            </li>
            @endif
            @if (Sentry::getuser()->hasAccess('reports.lst'))
            <li class="root-level {!! set_active(['lst', 'lst/*']) !!}">
                <a href="{!! URL::route('lst.index') !!}">
                    <i class="fa fa-gift"></i>
                    <span>Live Shopping Tracker</span>
                </a>
                <ul>
                    <li>
                        <a href="{!! URL::route('lst.index') !!}">
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! URL::route('lst.categories') !!}">
                            <span>Categories</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! URL::route('lst.products') !!}">
                            <span>New Products</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! URL::route('lst.search') !!}">
                            <span>Product Search</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            <li class="root-level  {!! set_active(['programs','programs/*']) !!}">
                <a href="{!! URL::to('programs') !!}">
                    <i class="fa fa-video-camera"></i>
                    <span>Show Search</span>
                </a>
            </li>

            <li class="root-level {!! set_active(['spots','spots/*']) !!}">
                <a href="{!! URL::to('spots') !!}">
                    <i class="fa fa-tint"></i>
                    <span>&nbsp; Spot Search</span>
                </a>
            </li>

            @if(Sentry::getUser()->hasAccess('reports.national-networks'))
            <li class="root-level {!! set_active(['networks','networks/*']) !!}">
                <a href="{!! URL::to('networks') !!}">
                    <i class="fa fa-desktop"></i>
                    <span>Networks</span>
                </a>
            </li>
            @endif

            @if(Sentry::getUser()->hasAccess('reports.companies'))
            <li class="root-level {!! set_active(['companies','companies/*']) !!}">
                <a href="{!! URL::to('companies') !!}">
                    <i class="fa fa-building-o"></i>
                    <span>Companies</span>
                </a>
            </li>
            @endif

            @if(Sentry::getUser()->hasAccess('reports.agencies'))
            <li class="root-level {!! set_active(['agencies','agencies/*']) !!}">
                <a href="{!! URL::to('agencies') !!}">
                    <i class="fa fa-building"></i>
                    <span>Agencies</span>
                </a>
            </li>
            @endif

            <li class="root-level">
                <a href="#" class="sidebar-collapse-icon with-animation">
                    <i class="fa fa-bars"></i>
                    <span>Toggle Menu</span>
                </a>
            </li>
        </ul>
    </div>

    <!-- END Side Navbar -->

@endif
