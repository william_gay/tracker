<div class="row" id="filter-bar">
  <div class="date-selection text-center">
     <div class="filter-sticky" style="display: inline-block;">
      @if($monthSelect)
          {!! Form::open(array(
              'url'    => URL::current(),
              'method' => 'get',
              'id'     => 'month-select',
              'class'  => 'form-inline',
              'style'  => 'display: inline-block')) !!}
            {!! Form::dateRangePickerMonth('report', $month) !!}

            {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
          {!! Form::close() !!}
      @else
          <div class="alert alert-info">No monthly reports available yet!</div>
      @endif
    </div>
  </div>
</div>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        @if($report->active == 0)
            <div class="alert alert-info text-center">This report is not active yet and is not publically viewable!</div>
        @endif
    </div>
</div>

