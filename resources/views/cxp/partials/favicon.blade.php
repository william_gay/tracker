@if (App::environment() == 'drtvlab')
<link rel="icon" href="/drtvlab/favicon.ico">
<link rel="shortcut icon" href="/drtvlab/favicon.ico">
<link rel="apple-touch-icon" href="{!! asset('assets/img/drtvlab/apple-touch-icon.png') !!}">
<link rel="apple-touch-icon" sizes="72x72" href="{!! asset('assets/img/drtvlab/apple-touch-icon-72x72-precomposed.png') !!}">
<link rel="apple-touch-icon" sizes="114x114" href="{!! asset('assets/img/drtvlab/apple-touch-icon-114x114-precomposed.png') !!}">
@else
<link rel="shortcut icon" href="/favicon.png">
<link rel="apple-touch-icon" href="{!! asset('assets/img/apple-touch-icon.png') !!}">
<link rel="apple-touch-icon" sizes="72x72" href="{!! asset('assets/img/apple-touch-icon-72x72-precomposed.png') !!}">
<link rel="apple-touch-icon" sizes="114x114" href="{!! asset('assets/img/apple-touch-icon-114x114-precomposed.png') !!}">
@endif
