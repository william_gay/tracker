<div class="row" id="filter-bar">
    <div class="col-sm-12">
        <div class="panel panel-default panel-shadow">
            <div class="panel-heading" id="filter-standard">
              <div class="filter-sticky">
                 <div class="panel-title">
                    {!! Form::open(array(
                        'url'    => URL::current(),
                        'method' => 'get',
                        'id'     => 'rangeForm',
                        'style'  => 'display: inline-block')) !!}
                        {!! Form::label('CPI', 'CPI') !!} {!! Form::radio('cpi_cpo', 'CPI') !!} {!! Form::label('CPO') !!} {!! Form::radio('cpi_cpo', 'CPO') !!}
                        {!! Form::select('analytics[]', ['Web Analytics', 'Amazon Analytics'], [], ['id' => 'analytic_picker', 'class' => 'matchingSize']) !!}
                        {!! Form::select('channels[]', $channelFormPopulator, $channelsToQuery, ['multiple' => true, 'id' => 'channel_picker', 'class' => 'matchingSize']) !!}
                        {!! Form::select('dayparts[]', $daypartFormPopulator, $daypartToQuery, ['multiple' => true, 'id' => 'daypart_picker', 'class' => 'matchingSize']) !!}
                        <div id="reportrange" style="display: inline-block;">
                          <i class="fa fa-calendar fa-lg"></i>
                          <span>{!!  $start_date->format("F j, Y") !!} - {!!  $end_date->format("F j, Y") !!}</span>
                        </div>
                      {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
