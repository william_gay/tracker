<div class="row" id="filter-bar">
    <div class="col-sm-12">
        <div class="panel panel-default panel-shadow">
            <div class="panel-heading" id="filter-standard">
              <div class="filter-sticky">
                 <div class="panel-title">
                    {!! Form::open(array(
                        'url'    => URL::current(),
                        'method' => 'get',
                        'id'     => 'rangeForm',
                        'style'  => 'display: inline-block')) !!}
                        {!! Form::select('product', [0 => 'Grandma\'s Helper'], [0], ['id' => 'product_picker', 'class' => 'matchingSize']) !!}
                        {!! Form::label('CPI', 'CPI') !!} {!! Form::checkbox('cpi_cpo', 'CPI', true) !!} {!! Form::label('CPO') !!} {!! Form::checkbox('cpi_cpo', 'CPO', true) !!}
                        {!! Form::select('channels[]', $channelFormPopulator, $channelsToQuery, ['multiple' => true, 'id' => 'channel_picker', 'class' => 'matchingSize']) !!}
                        <div id="reportrange" style="display: inline-block;">
                          <i class="fa fa-calendar fa-lg"></i>
                          <span>{!!  $start_date->format("F j, Y") !!} - {!!  $end_date->format("F j, Y") !!}</span>
                        </div>
                        {!! Form::select('dayparts[]', $daypartFormPopulator, $daypartToQuery, ['multiple' => true, 'id' => 'daypart_picker', 'class' => 'matchingSize']) !!}
                        <div class="form-inline btn-group" style="display:inline-block;">
                          <input type="text" name="startTime" id="startTime" value="12:00 AM" class="form-control col-md-1">
                          <input type="text" name="endTime" id="endTime" value="12:00 PM" class="form-control col-md-1">
                        </div>
                      {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
