<h3>
    @if (Config::get('app.site_config.twitter') != '')
    <a href="{!! Config::get('app.site_config.twitter') !!}" target="_blank"><i class="fa fa-twitter-square"></i></a>&nbsp;&nbsp;
    @endif
    @if (Config::get('app.site_config.linkedin') != '')
    <a href="{!! Config::get('app.site_config.linkedin') !!}" target="_blank"><i class="fa fa-linkedin-square"></i></a>&nbsp;&nbsp;
    @endif
    @if (Config::get('app.site_config.facebook') != '')
    <a href="{!! Config::get('app.site_config.facebook') !!}" target="_blank"><i class="fa fa-facebook-square"></i></a>
    @endif
</h3>
