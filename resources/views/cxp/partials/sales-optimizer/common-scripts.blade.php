$(window).scroll(function () {
  if ( $(this).scrollTop() > 100 && !$('.filter-sticky').hasClass('open') ) {
    $('.filter-sticky').addClass('open filter-sticky-stuck');
    $('.filter-sticky').slideUp("fast", function() {
        $('.filter-sticky').insertAfter('#navbar-brand').slideDown();
    });
   } else if ( $(this).scrollTop() < 100 && $('.filter-sticky').hasClass('open')) {
    $('.filter-sticky').slideUp("fast", function(){
        $('.filter-sticky-stuck').removeClass('open filter-sticky-stuck');
        $('#filter-standard').append($('.filter-sticky'));
        $('.filter-sticky').slideDown("fast");
    });
  }
});

//=============================================================================
// Selector for Categories
//=============================================================================
var categoryConfigurationSet = {
    templates: {
        ul: '<ul class="multiselect-container dropdown-menu scrollable-menu"></ul>',
    },
    includeSelectAllOption: true,
    buttonText: function(options, select) {
        if (options.length == 0) {
            return 'Categories';
        } else {
            if (options.length > this.numberDisplayed) {
                return 'Categories';
            } else {
                return 'Categories';
            }
        }
    }
};

//=============================================================================
// Selector for Channels
//=============================================================================
var channelConfigurationSet = {
    templates: {
        ul: '<ul class="multiselect-container dropdown-menu scrollable-menu"></ul>',
    },
    includeSelectAllOption: true,
    buttonText: function(options, select) {
        if (options.length == 0) {
            return 'Channels';
        } else {
            if (options.length > this.numberDisplayed) {
                return 'Channels';
            } else {
                return 'Channels';
            }
        }
    }
};

//=============================================================================
// Selector for Channels Comparison
//=============================================================================
var channelComparisonConfigurationSet = {
    templates: {
        ul: '<ul class="multiselect-container dropdown-menu scrollable-menu"></ul>',
    },
    includeSelectAllOption: true,
    buttonText: function(options, select) {
        if (options.length == 0) {
            return 'Channels to Compare';
        } else {
            if (options.length > this.numberDisplayed) {
                return 'Channels to Compare';
            } else {
                return 'Channels to Compare';
            }
        }
    }
};

//=============================================================================
// Selector for Demographics
//=============================================================================
$('#language_id').multiselect({
    buttonText: function(options, select) {
        if (options.length == 0) {
            return 'Demographic';
        }
        else {
            if (options.length > this.numberDisplayed) {
                return 'Demographic';
            }
            else {
                return 'Demographic';
            }
        }
    },
    onChange: function(option, checked){
        var values = [];
        $.get("/api/channels/dropdown",
            { language_id: option.val() },
            function(data) {
                var channel_id = $('#channel_picker');
                var channel_compare_id = $('#channel_compare_picker');
                channel_id.empty();
                channel_compare_id.empty();
                $.each(data.channels, function(index, element) {
                    values.push(index);
                    channel_id.append("<option value='"+ index +"'>" + element + "</option>");
                    channel_compare_id.append("<option value='"+ index +"'>" + element + "</option>");
                });
                channel_id.multiselect('rebuild');
                channel_id.multiselect('deselect', option.val());
                channel_id.next('div').find('button').click();

                channel_compare_id.multiselect('rebuild');
                channel_compare_id.multiselect('deselect', option.val());
            }
        );
    }
});

//=====================================
// DATE
//=====================================
$('#reportrange').daterangepicker(
    {
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
         'Last 7 Days': [moment().subtract('days', 6), moment()],
         'Last 30 Days': [moment().subtract('days', 29), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')],
      },
      startDate: moment().subtract('days', 29),
      endDate: moment()
    },
    function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#rangeStart').val(start.format('MMMM D, YYYY'));
        $('#rangeEnd').val(end.format('MMMM D, YYYY'));
    }
);
