<div class="row" id="filter-bar">
    <div class="col-sm-12">
        <div class="panel panel-default panel-shadow">
            <div class="panel-heading" id="filter-standard">
              <div class="filter-sticky">
                 <div class="panel-title">
                    {!! Form::open(array(
                        'url'    => URL::current(),
                        'method' => 'get',
                        'id'     => 'rangeForm',
                        'style'  => 'display: inline-block')) !!}
                      {!! Form::select('market_id', $marketFormPopulator, Request::filled('market_id')? Request::get('market_id') : 1, ['id' => 'language_id', 'class' => 'matchingSize']) !!}
                      {!! Form::select('channels_compare[]', $channelFormPopulator, $channelsCompareToQuery, ['id' => 'channel_compare_picker', 'multiple' => 'multiple', 'class' => 'matchingSize']) !!}
                      {!! Form::dateRangePickerStart('rangeStart', $start_date) !!}
                      {!! Form::dateRangePickerEnd('rangeEnd', $end_date) !!}
                      <div id="reportrange" style="display: inline-block;">
                          <i class="fa fa-calendar fa-lg"></i>
                          <span>{!!  $start_date->format("F j, Y") !!} - {!!  $end_date->format("F j, Y") !!}</span>
                      </div>
                      <br>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          Products:
                        </div>
                        <div class="col-md-10">
                          {!! Former::xlarge_text('spot_ids', '', $products)->class('col-md-12')->id('spot_ids') !!}
                        </div>
                      </div>
                      <br>
                      {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
