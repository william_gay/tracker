<div class="row" id="filter-bar">
  <div class="col-sm-12">
    <div class="panel panel-default panel-shadow">
      <div class="panel-heading" id="filter-standard">
        <div class="filter-sticky">
          <div class="panel-title">
            {!! Form::open(array(
                'url'    => URL::current(),
                'method' => 'get',
                'id'     => 'rangeForm',
                'style'  => 'display: inline-block')) !!}
            @if(isset($category))
              <div id="e2" class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                  {!! $category->name !!} <span class="caret"></span>
                </button>
                <ul class="dropdown-menu  scrollable-menu" role="menu">
                  @foreach ($allCategories as $singleCategory)
                    @unless($singleCategory->id == $category->id)
                      <li>{!! link_to_route('network-anaylizer.categories.show', $singleCategory->name,  ['id' => $singleCategory->id]) !!}</li>
                    @endunless
                  @endforeach
                </ul>
              </div>
            @endif
            {!! Form::select('market_id', $marketFormPopulator, $marketToQuery, ['id' => 'language_id', 'class' => 'matchingSize']) !!}
            {!! Form::select('ct[]', $categoryFormPopulator, $categoriesToQuery, ['multiple' => 'multiple', 'id' => 'category_picker', 'class' => 'matchingSize']) !!}
            {!! Form::select('sct[]', $subcategoryFormPopulator, $subcategoriesToQuery, ['multiple' => 'multiple', 'id' => 'subcategory_picker', 'class' => 'matchingSize']) !!}
            {!! Form::select('ch[]', $channelFormPopulator, $channelsToQuery, ['multiple' => 'multiple', 'id' => 'channel_picker', 'class' => 'matchingSize']) !!}
            @if (isset($showLengthSelect) && $showLengthSelect)
              {!! Form::select('spot_length[]', [15=>15,30=>30,60=>60,120=>120,300=>300], $spotLengthsToQuery, ['multiple' => 'multiple', 'id' => 'spot_length_picker', 'class' => 'matchingSize']) !!}
            @endif
            {!! Form::dateRangePickerStart('rangeStart', $start_date) !!}
            {!! Form::dateRangePickerEnd('rangeEnd', $end_date) !!}
            <div id="reportrange" style="display: inline-block;">
              <i class="fa fa-calendar fa-lg"></i>
              <span>{!!  $start_date->format("F j, Y") !!} - {!!  $end_date->format("F j, Y") !!}</span>
            </div>
            {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
