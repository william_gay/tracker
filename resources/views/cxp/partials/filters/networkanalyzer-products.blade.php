<div class="row" id="filter-bar">
  <div class="col-sm-12">
    <div class="panel panel-default panel-shadow">
      <div class="panel-heading" id="filter-standard">
        <div class="filter-sticky">
          <div class="panel-title">
            {!! Form::open(array(
                'url'    => URL::current(),
                'method' => 'get',
                'id'     => 'rangeForm',
                'style'  => 'display: inline-block')) !!}
            {!! Form::select('market_id', $marketFormPopulator, $marketToQuery, ['id' => 'language_id', 'class' => 'matchingSize']) !!}
            {!! Form::select('ch[]', $channelFormPopulator, $channelsToQuery, ['multiple' => true, 'id' => 'channel_picker', 'class' => 'matchingSize']) !!}
            @if (isset($showLengthSelect) && $showLengthSelect)
              {!! Form::select('spot_length[]', [15=>15,30=>30,60=>60,120=>120,300=>300], $spotLengthsToQuery, ['multiple' => 'multiple', 'id' => 'spot_length_picker', 'class' => 'matchingSize']) !!}
            @endif
            {!! Form::dateRangePickerStart('rangeStart', $start_date) !!}
            {!! Form::dateRangePickerEnd('rangeEnd', $end_date) !!}
            <div id="reportrange" style="display: inline-block;">
              <i class="fa fa-calendar fa-lg"></i>
              <span>{!!  $start_date->format("F j, Y") !!} - {!!  $end_date->format("F j, Y") !!}</span>
            </div>
            <br>
            <div class="row">
              <div class="col-md-10">
                Spots: {!! Former::xlarge_text('spt', '', $spotsToQuery)->id('spot_ids') !!}
              </div>
            </div>
            <div class="row">
              <div class="col-md-10">
                Shows: {!! Former::xlarge_text('prgm', '', $programsToQuery)->id('program_ids') !!}
              </div>
            </div>
            <br>
            {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
