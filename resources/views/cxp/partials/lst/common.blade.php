$(window).scroll(function () {
  if ( $(this).scrollTop() > 100 && !$('.filter-sticky').hasClass('open') ) {
    $('.filter-sticky').addClass('open filter-sticky-stuck');
    $('.filter-sticky').slideUp("fast", function() {
        $('.filter-sticky').insertAfter('#navbar-brand').slideDown();
    });
   } else if ( $(this).scrollTop() < 100 && $('.filter-sticky').hasClass('open')) {
    $('.filter-sticky').slideUp("fast", function(){
        $('.filter-sticky-stuck').removeClass('open filter-sticky-stuck');
        $('#filter-standard').append($('.filter-sticky'));
        $('.filter-sticky').slideDown("fast");
    });
  }
});

//=============================================================================
// Selector for Categories
//=============================================================================
var categoryConfigurationSet = {
    templates: {
        ul: '<ul class="multiselect-container dropdown-menu scrollable-menu"></ul>',
    },
    includeSelectAllOption: true,
    buttonText: function(options, select) {
        if (options.length == 0) {
            return 'Categories';
        } else {
            if (options.length > this.numberDisplayed) {
                return 'Categories';
            } else {
                return 'Categories';
            }
        }
    }
};

//=============================================================================
// Selector for Channels
//=============================================================================
var channelConfigurationSet = {
    templates: {
        ul: '<ul class="multiselect-container dropdown-menu scrollable-menu"></ul>',
    },
    includeSelectAllOption: true,
    buttonText: function(options, select) {
        if (options.length == 0) {
            return 'Channels';
        } else {
            if (options.length > this.numberDisplayed) {
                return 'Channels';
            } else {
                return 'Channels';
            }
        }
    }
};

//=====================================
// DATE
//=====================================
$('#reportrange').daterangepicker(
    {
      ranges: {
         'Today': [moment(), moment()],
         'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
         'Last 7 Days': [moment().subtract('days', 6), moment()],
         'Last 30 Days': [moment().subtract('days', 29), moment()],
         'This Month': [moment().startOf('month'), moment().endOf('month')],
         'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')],
      },
      startDate: moment().subtract('days', 29),
      endDate: moment()
    },
    function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        $('#rangeStart').val(start.format('MMMM D, YYYY'));
        $('#rangeEnd').val(end.format('MMMM D, YYYY'));
    }
);
