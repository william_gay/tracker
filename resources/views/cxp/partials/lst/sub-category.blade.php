<div class="row" id="filter-bar">
    <div class="col-sm-12">
        <div class="panel panel-default panel-shadow">
            <div class="panel-heading" id="filter-standard">
              <div class="filter-sticky">
                 <div class="panel-title">
                    {!! Form::open(array(
                        'url'    => URL::current(),
                        'method' => 'get',
                        'id'     => 'rangeForm',
                        'style'  => 'display: inline-block')) !!}
                        @if(isset($category))
                        <div id="e2" class="btn-group">
                          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            {!! $category->name !!} <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu  scrollable-menu" role="menu">
                            @foreach ($allCategories as $singleCategory)
                                    <li>{!! link_to_route('lst.category', $singleCategory->name,  ['categoryId' => $singleCategory->id,
                                      'rangeStart' => $range['start']->format('F j, Y'), 'rangeEnd' => $range['end']->format('F j, Y')]) !!}</li>
                            @endforeach
                          </ul>
                        </div>
                        @endif
                        @if(isset($subCategory))
                        <div id="e2" class="btn-group">
                          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            {!! $subCategory->name !!} <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu  scrollable-menu" role="menu">
                            @foreach ($allSubCategories as $singleCategory)
                                @unless($singleCategory->id == $subCategory->id)
                                    <li>{!! link_to_route('lst.sub-category', $singleCategory->name,  ['categoryId' => $singleCategory->id,
                                      'rangeStart' => $range['start']->format('F j, Y'), 'rangeEnd' => $range['end']->format('F j, Y')]) !!}</li>
                                @endunless
                            @endforeach
                          </ul>
                        </div>
                        @endif
                      {!! Form::select('channels[]', $channels, $channelsToQuery, ['multiple' => true, 'id' => 'channel_picker', 'class' => 'matchingSize']) !!}
                      {!! Form::checkbox('DSV', '1', \Request::get('DSV')) !!}
                      {!! Form::label('Daily Special Value Only') !!}
                      {!! Form::dateRangePickerStart('rangeStart', $range['start']) !!}
                      {!! Form::dateRangePickerEnd('rangeEnd', $range['end']) !!}
                      <div id="reportrange" style="display: inline-block;">
                          <i class="fa fa-calendar fa-lg"></i>
                          <span>{!!  $range['start']->format("F j, Y") !!} - {!!  $range['end']->format("F j, Y") !!}</span>
                      </div>
                      {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@include('cxp.partials.lst.channels-selected')
