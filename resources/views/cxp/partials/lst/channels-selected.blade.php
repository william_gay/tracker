<div class="row">
  <div class="col-md-12">
    @if (isset($subCategory))
    <h2 class="text-center">{!! $subCategory->name !!}</h2>
    @elseif (isset($category))
    <h2 class="text-center">{!! $category->name !!}</h2>
    @endif
    <h3 class="text-center">Selected Channels: {!! $channelsSelected->implode(', ') !!}</h3>
  </div>
</div>
