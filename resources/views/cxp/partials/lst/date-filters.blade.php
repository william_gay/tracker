<div class="row" id="filter-bar">
    <div class="col-sm-12">
        <div class="panel panel-default panel-shadow">
            <div class="panel-heading" id="filter-standard">
              <div class="filter-sticky">
                 <div class="panel-title">
                    {!! Form::open(array(
                        'url'    => URL::current(),
                        'method' => 'get',
                        'id'     => 'rangeForm',
                        'style'  => 'display: inline-block')) !!}
                      {!! Form::dateRangePickerStart('rangeStart', $range['start']) !!}
                      {!! Form::dateRangePickerEnd('rangeEnd', $range['end']) !!}
                      <div id="reportrange" style="display: inline-block;">
                          <i class="fa fa-calendar fa-lg"></i>
                          <span>{!!  $range['start']->format("F j, Y") !!} - {!!  $range['end']->format("F j, Y") !!}</span>
                      </div>
                      {!! Form::submit('Submit', ['class' => 'btn btn-info']) !!}
                    {!! Form::close() !!}
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
