<!-- #helpModal -->
<div class="modal fade" id="upgradeModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Please Upgrade for Access</h4>
            </div>

            {!! Form:: open(array('action' => 'ContactController@postContact')) !!}
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <p>Please contact <a href="mailto:sales@imsreport.com">sales@imsreport.com</a> or fill in the form below to upgrade your account.</p>
                    </div>
                    <div class="col-sm-12">

                            @if (isset($errors))
                            <ul class="errors">
                                @foreach($errors->all('<li>:message</li>') as $message)
                                {!! $message !!}
                                @endforeach
                            </ul>
                            @endif

                            <div class="form-group col-sm-12">
                                {!! Form:: label ('message', 'Message*' )!!}
                                {!! Form:: textarea ('message', '', array('class' => 'form-control'))!!}
                            </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit('Send', array('class' => 'btn btn-blue')) !!}
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form:: close() !!}
        </div>
    </div>
</div>
<!-- /#helpModal -->
