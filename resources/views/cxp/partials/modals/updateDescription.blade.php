<div class="modal fade" id="updatePageDescriptionModal" aria-hidden="false" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Detail your request: </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-left">
                        </div>
                        <div class="col-sm-12">
                            {!! Form:: open(array('action' => 'ContactController@updatePageDescription')) !!}
                                <ul class="errors">
                                    @foreach($errors->all('<li>:message</li>') as $message)
                                    {!! $message !!}
                                    @endforeach
                                </ul>

                                <div class="form-group col-sm-12">
                                    {!! Form:: label ('message', 'Message*' )!!}
                                    {!! Form:: textarea ('body', '', array('class' => 'form-control'))!!}
                                </div>

                        </div>
                        <div class="pull-right">
                            {!! Form::submit('Send', array('class' => 'btn btn-blue')) !!}
                            {!! Form:: close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
