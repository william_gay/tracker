<!-- #welcomeModal -->
<div class="modal fade" id="welcomeModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Welcome to the NEW Media Analytics Dashboard</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                            <div class="list-group">

                                <span class="list-group-item active">
                                    <h4 class="orange list-group-item-heading">Instant Access</h4>
                                    <p class="list-group-item-text">The new Media Analytics Dashboard offers instant access to the insight you need, formatted to your specifications, and accessible on any web-enabled device.</p>
                                </span>

                                <span class="list-group-item active">
                                    <h4 class="orange list-group-item-heading">Customizable</h4>
                                    <p class="list-group-item-text">Your customizable* dashboard provides a concise snapshot of your selected areas of interest while serving as a gateway to the entire Media Analytics Platform, including the Industry Leading IMS Report, full of big picture data and insight.</p>
                                </span>

                                <span class="list-group-item active">
                                    <h4 class="orange list-group-item-heading">Mobile Optimized</h4>
                                    <p class="list-group-item-text">Mobile optimized design puts Media Analytics just a click away on any web-enabled mobile device.  You can even save this as an application to your iOs or Android Home Screen.</p>
                                </span>

                                <span class="list-group-item">
                                    <h4 class="orange list-group-item-heading">Updated Menu</h4>
                                    <p class="list-group-item-text">Access any <a href="/reports">reports</a> from any page by selecting from our new Menu.</p>
                                </span>

                                <span class="list-group-item">
                                    <h4 class="orange list-group-item-heading">Manage Alerts</h4>
                                    <p class="list-group-item-text">Update your <a href="/user/alerts">alerts</a> to be notified of new products or changes in your category.</p>
                                </span>

                                <span class="list-group-item">
                                    <h4 class="orange list-group-item-heading">Enhanced Support</h4>
                                    <p class="list-group-item-text">If you have any support questions, feel free to use our support tab in the top right of every page, or contact our support team <a href="mailto:support@imsreport.com">support@imsreport.com</a></p>
                                </span>

                                <span class="list-group-item">
                                    <h4 class="orange list-group-item-heading">Want to Upgrade?</h4>
                                    <p class="list-group-item-text">If wish to upgrade your access, please contact <a href="mailto:sales@imsreport.com">sales@imsreport.com</a> or fill in our <a data-dismiss="modal" data-toggle="modal" data-target="#upgradeModal">upgrade form.</a></p>
                                </span>
                            </div>

                    </div>
                    <div class="col-xs-12">
                        <div class="pull-left">
                        <small>* Customizable features coming soon</small>
                        </div>
                        <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Continue</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#welcomeModal -->
