<!-- #IEModal -->
<div class="modal fade" id="IEModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Welcome to the NEW Media Analytics Dashboard</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <p style="color:#000000;">
                            The Media Analytics Dashboard is optimized for Modern Browsers. If you are using a version of Internet Explorer earlier than IE10, please upgrade now or download one of the supported browsers below.
                        </p>
                        <p style="color:#000000;">
                            If you have any questions, please contact <a href="mailto:support@imsreport.com">support</a> or fill in the form by selecting the "Support" tab in the top right corner.
                        </p>
                            <ul>
                                <li><a href="http://getfirefox.com" target="_blank">Mozilla Firefox</a></li>
                                <li><a href="http://google.com/chrome" target="_blank">Google Chrome</a></li>
                                <li><a href="http://www.opera.com/" target="_blank">Opera</a></li>
                            </ul>
                    </div>
                    <div class="col-xs-12">
                        <div class="pull-right">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Continue</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#IEModal -->
