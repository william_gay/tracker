@if( App::environment() != 'local' )
<script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="b3959fd0454692f587b9546eedf396bd"></script>
    @if (Sentry::check())
    <script>
    Bugsnag.releaseStage = "{!! App::environment() !!}";
    Bugsnag.user = {
        id: {!! Sentry::getUser()->id !!},
        name: "{!! Sentry::getUser()->first_name.' '.Sentry::getUser()->last_name !!}",
        email: "{!! Sentry::getUser()->email !!}"
    };
    </script>
    @endif
@endif
