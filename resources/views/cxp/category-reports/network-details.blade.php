@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-sitemap"></i>
        Category Report {!! $report->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('category-report-network-detail', $report) !!}
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">Network Breakdown</h3>

            @if($report->id == 1)
            <div style="margin:0px;padding:0px;overflow:hidden">
                <iframe src="https://my.exploreanalytics.com/pub/view/f1db22fa90fd43beb5e938ad03434806" frameborder="0" style="overflow:hidden;height:450px;width:100%" height="450px" width="100%"></iframe>
            </div>
            @endif

        </div>
    </div>
@stop