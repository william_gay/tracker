@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-sitemap"></i>
        Category Report : {!! $report->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('category-report-program-detail', $report) !!}
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">Program Breakdown</h3>

            @if($report->id == 1)
                <iframe src="https://my.exploreanalytics.com/pub/view/019ec6e7a7384e6dac5be2f5fce1dd06" frameborder="0" style="overflow:hidden;height:450px;width:100%" height="450px" width="100%"></iframe>
                <hr>
                <a href="{!! URL::to('category-reports/program-detail-full', array($report->id)) !!}" target="_blank">Full Screen</a>
                <iframe src="https://my.exploreanalytics.com/pub/view/ffc580d19eae4ce9882818c2dfc9c289" frameborder="0" style="overflow:hidden;height:7000px;width:100%" height="7000px" width="100%"></iframe>
            @endif

        </div>
    </div>
@stop