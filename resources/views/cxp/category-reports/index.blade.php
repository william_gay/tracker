@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-sitemap"></i>
        Category Reports
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('category-reports') !!}

    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center"><i class="fa fa-sitemap"></i>
                Available Reports</h3>
            <table class="table table-hover table-bordered">
                <thead>
                    <th>Name</th>
                    <th>Shows</th>
                    <th>Spots</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th class="col-xs-2"></th>
                </thead>
                <tbody>
            @foreach($category_reports as $report)
                @if($report->enabled or Sentry::getUser()->isSuperUser())
                    <tr>
                        <td>{!! $report->name !!}</td>
                        <td>
                        @if(Sentry::getUser()->hasAccess('category-reports.'.$report->slug))
                            @if ($report->programs->count() == 0)
                                All
                            @else
                                {!! $report->programs->count() !!}
                            @endif
                        @endif
                        </td>
                        <td>
                        @if(Sentry::getUser()->hasAccess('category-reports.'.$report->slug))
                            @if ($report->spots->count() == 0)
                                All
                            @else
                                {!! $report->spots->count() !!}
                            @endif
                        @endif
                        </td>
                        <td>
                        @if(Sentry::getUser()->hasAccess('category-reports.'.$report->slug))
                            {!! $report->start_date->format('m/d/Y') !!}
                        @endif
                        </td>
                        <td>
                        @if(Sentry::getUser()->hasAccess('category-reports.'.$report->slug))
                            {!! $report->end_date->format('m/d/Y') !!}
                        @endif
                        </td>
                        <td>
                        @if(Sentry::getUser()->hasAccess('category-reports.'.$report->slug))
                            <a class="btn btn-green btn-icon icon-left" href="/category-reports/show/{!! $report->id !!}"><i class="fa fa-eye"></i> View</a>
                        @else
                            <a class="btn btn-green btn-icon icon-left" href="/category-reports/purchase/{!! $report->id !!}"><i class="fa fa-usd"></i> Purchase</a>
                        @endif
                        </td>
                    </tr>
                @endif
            @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
