@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-sitemap"></i>
        Category Report {!! $report->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('category-report-spot-detail', $report) !!}
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">Spot Breakdown</h3>

            @if($report->id == 1)
            <div style="margin:0px;padding:0px;overflow:hidden">
                <iframe src="https://my.exploreanalytics.com/pub/view/a4674cdc4b9c4e399c89becf4e43ca73" frameborder="0" style="overflow:hidden;height:400px;width:100%" height="400px" width="100%"></iframe>
            </div>
                <hr>
            <a href="{!! URL::to('category-reports/spot-detail-full', array($report->id)) !!}" target="_blank">Full Screen</a>
            <div style="margin:0px;padding:0px;overflow:hidden">
                <iframe src="https://my.exploreanalytics.com/pub/view/6c4e5b560af14e84ae20479855d16261" frameborder="0" style="overflow:hidden;height:3100px;width:100%" height="3100px" width="100%"></iframe>
            </div>
            @endif

        </div>
    </div>
@stop