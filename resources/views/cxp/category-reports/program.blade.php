@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-sitemap"></i>
        Category Report {!! $report->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('category-report-program', $report, $program) !!}
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center"> {!! $program->title !!} - Frequency and Media Spend Over Time</h3>
            <div class="date-selection text-center">
                <form class="form-inline" style="display:inline-block;">
                {!! Former::xlarge_text('start-date','')
                    ->class('span4 start-date form-control picka')
                    ->placeholder($start_date->format("F j, Y"))
                    ->required() !!}

                {!! Former::xlarge_text('end-date','')
                    ->class('span4 end-date form-control picka')
                    ->placeholder($end_date->format("F j, Y"))
                    ->required() !!}
                <button class="select btn btn-blue btn-icon">Select Date Range <i class="fa fa-calendar"></i></button>
                </form>
            </div>
            <div id="media-over-time" style="width:100%;height:350px;"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center">Raw Airings</h4>
            <table id="airings" class="table table-hover table-bordered results">
                <thead>
                    <th>Title</th>
                    <th>Network</th>
                    <th>Day of Week</th>
                    <th>Air Date</th>
                    <th>Air Time</th>
                    <th>Cost</th>
                </thead>
                <tbody>
                    @foreach($airtimes as $airtime)
                    <tr>
                        <td>
                        {!! $airtime->grid_title !!}
                        </td>
                        <td class="text-center">
                            <span data-order="{!! $airtime->name !!}" title="{!! $airtime->name !!}">
                                <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $airtime->logo_location !!}" class="network-img" />
                            </span>
                            {!! $airtime->name !!}
                        </td>
                        <td>{!! $airtime->air_dateable->format("l") !!}</td>
                        <td>{!! $airtime->air_dateable->format("m/d/Y") !!}</td>
                        <td>{!! $airtime->air_dateable->format("H:i a") !!}</td>
                        <td><span data-order="{!! $airtime->cost !!}" title="{!! $airtime->cost !!}">${!! number_format($airtime->cost,2, '.',',') !!}</span></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('script')
<script>
$(function() {
    $('.picka').datetimepicker({
        format: 'MMMM D, YYYY',
        useCurrent: false
    });

    @if($over_time)
        var overtime = AmCharts.makeChart("media-over-time", {
            "type": "serial",
            "theme": "none",
            "pathToImages": "{!!URL::to('/')!!}/assets/img/",
            "dataProvider": {!! $over_time->toJson() !!},
            "valueAxes": [{
                "id":"v1",
                "axisColor": "#FF6600",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "left",
                "title": "Frequency"
            }, {
                "id":"v2",
                "axisColor": "#FCD202",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "right",
                "title": "Media Spend"
            }],
            "graphs": [{
                "valueAxis": "v1",
                "balloonText": "[[title]]: [[value]]",
                "lineThickness": 2,
                "title": "Frequency",
                "valueField": "airs"
            }, {
                "valueAxis": "v2",
                "balloonText": "[[title]]: $[[value]]",
                "lineThickness": 2,
                "title": "Media Spend",
                "valueField": "media"
            }],
            "zoomOutButtonRollOverAlpha": 0.15,
            "chartCursor": {
                "categoryBalloonDateFormat": "MMM DD JJ:NN",
                "cursorPosition": "mouse",
                "showNextAvailable":true
            },
            "autoMarginOffset": 5,
            "columnWidth": 1,
            "categoryField": "date",
            "categoryAxis": {
                "minPeriod": "hh",
                "parseDates": true
            },
            "amExport": {
                top : 0,
                right : 150,
                exportJPG : true,
                exportPNG : true,
                exportSVG : true,
                exportPDF : true,
                menuItems: [{
                    textAlign: 'center',
                    icon: '{!!URL::to('/')!!}/assets/img/export.png',
                    iconTitle: 'Save chart as an image',
                }]
            }
        });
    @endif
    var oTable;
    oTable = $('#airings').dataTable({
        "pageLength": 10,
        "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    });
});
</script>
@stop
