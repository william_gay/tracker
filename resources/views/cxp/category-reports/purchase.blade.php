@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-sitemap"></i>
        Category Reports : {!! $report->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('category-report', $report) !!}
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">{!! $report->name !!} Category Report</h3>
            <p class="text-center">{!! $report->start_date->format('m/d/Y') !!} - {!! $report->end_date->format('m/d/Y') !!}</p>

            <div class="alert">Buy Now!</div>

            <p class="text-center">Please contact sales@imsreport.com to purchase this report.</p>
        </div>
    </div>
@stop