@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-sitemap"></i>
        Category Reports : {!! $report->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('category-report', $report) !!}
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center"><i class="fa fa-sitemap"></i>
                {!! $report->name !!} Category Report</h3>
            <div class="date-selection text-center">
                <form class="form-inline" style="display:inline-block;">
                {!! Former::xlarge_text('start-date','', $start_date->format("F j, Y"))
                    ->class('span4 form-control start-date picka')
                    ->placeholder($start_date->format("F j, Y"))
                    ->required() !!}

                {!! Former::xlarge_text('end-date','', $end_date->format("F j, Y"))
                    ->class('span4 form-control end-date picka')
                    ->placeholder($end_date->format("F j, Y"))
                    ->required() !!}

                <button class="select btn btn-blue btn-icon">Select Date Range <i class="fa fa-calendar"></i></button>
                </form>
            </div>

            <ul class="list-inline text-center stats-overview">
                <li>
                    <div class="well well-sm">
                        <h5>Airings</h5>
                        <h3>
                            @if($spot_versions and $program_versions)
                                {!! $program_airings->count() + $spot_airings->count() !!}
                            @elseif($spot_versions)
                                {!! $spot_airings->count() !!}
                            @elseif($program_versions)
                                {!! $program_airings->count() !!}
                            @else
                                0
                            @endif
                        </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Shows</h5>
                        <h3>
                            <a href="/category-reports/products/{!! $report->id !!}#programs">{!! $program_count !!}</a>
                        </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Spots</h5>
                        <h3>
                            <a href="/category-reports/products/{!! $report->id !!}#spots">{!! $spot_count !!}</a>
                        </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Average Media Spend</h5>
                        <h3>
                            $ {!! number_format($program_airings->avg('cost'), 2 , '.', ',') !!}
                        </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Date Range</h5>
                        <h3>
                            {!! $start_date->format('m/d/Y') !!} - {!! $end_date->format('m/d/Y') !!}
                        </h3>
                    </div>
                </li>
            </ul>

        </div>
    </div>
    @if($report->id == 0)
    <hr>
    <div class="row">
        <div class="col-xs-12">
            <p class="text-center">
                <ul class="nav nav-pills nav-justified">
                    <li><a href="/category-reports/program-detail/{!! $report->id !!}">Program Breakdown</a></li>
                    <li><a href="/category-reports/spot-detail/{!! $report->id !!}">Spot Breakdown</a></li>
                    <li><a href="/category-reports/network-detail/{!! $report->id !!}">Network Breakdown</a></li>
                </ul>
            </p>
        </div>
    </div>
    @endif
    <hr>
    <div class="row">
        <div class="col-xs-8">
            <div id="media-over-time" style="width:100%;height:300px;"></div>
        </div>

        <div class="col-xs-4">
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        Top {!! $top_media_spend->count() !!} Media Spend
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <th>Program</th>
                            <th><div class="text-right">Spend</div></th>
                        </thead>
                        <tbody>
                            @foreach($top_media_spend as $program)
                            <tr>
                                <td>{!! $program->title !!}</td>
                                <td><div class="text-right">$ {!! number_format($program->cost,2, '.',',') !!}</div></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-xs-4">
            <div id="network-distribution" style="width:100%;height: 300px;"></div>
        </div>
       <div class="col-xs-4">
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        Top {!! $top_networks->count() !!} Networks
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <th>Network</th>
                            <th>Airs</th>
                            <th><div class="text-right">Spend</div></th>
                        </thead>
                        <tbody>
                            @foreach($top_networks as $network)
                            <tr>
                                <td>{!! $network->name !!}</td>
                                <td>{!! $network->airings !!}</td>
                                <td><div class="text-right">$ {!! number_format($network->cost,2, '.',',') !!}</div></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
       <div class="col-xs-4">
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        Top {!! $top_frequency->count() !!} Frequency
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <th>Program</th>
                            <th><div class="text-right">Frequency</div></th>
                        </thead>
                        <tbody>
                            @foreach($top_frequency as $program)
                            <tr>
                                <td>{!! $program->title !!}</td>
                                <td><div class="text-right">{!! $program->airings !!}</div></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if($report->overview)
    <hr>
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">Overview</h3>
            {!! nl2br($report->overview) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        </div>
    </div>
    @endif
@stop

@section('script')
<script>
$(function() {

    @if($over_time)
        var overtime = AmCharts.makeChart("media-over-time", {
            "type": "serial",
            "theme": "none",
            "pathToImages": "{!!URL::to('/')!!}/assets/img/",
            "dataProvider": {!! $over_time->toJson() !!},
            "valueAxes": [{
                "id":"v1",
                "axisColor": "#FF6600",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "left",
                "title": "Frequency"
            }, {
                "id":"v2",
                "axisColor": "#FCD202",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "right",
                "title": "Media Spend"
            }],
            "graphs": [{
                "valueAxis": "v1",
                "balloonText": "[[title]]: [[value]]",
                "lineThickness": 2,
                "title": "Frequency",
                "valueField": "airs"
            }, {
                "valueAxis": "v2",
                "balloonText": "[[title]]: $[[value]]",
                "lineThickness": 2,
                "title": "Media Spend",
                "valueField": "media"
            }],
            "zoomOutButtonRollOverAlpha": 0.15,
            "chartCursor": {
                "categoryBalloonDateFormat": "MMM DD JJ:NN",
                "cursorPosition": "mouse",
                "showNextAvailable":true
            },
            "autoMarginOffset": 5,
            "columnWidth": 1,
            "categoryField": "date",
            "categoryAxis": {
                "minPeriod": "hh",
                "parseDates": true
            },
            "amExport": {
                top : 0,
                right : 150,
                exportJPG : true,
                exportPNG : true,
                exportSVG : true,
                exportPDF : true,
                menuItems: [{
                    textAlign: 'center',
                    icon: '{!!URL::to('/')!!}/assets/img/export.png',
                    iconTitle: 'Save chart as an image',
                }]
            }
        });
    @endif
    @if($channelPie)
    var chart = AmCharts.makeChart("network-distribution", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $channelPie->toJson() !!},
        "valueField": "chanfreq",
        "titleField": "title",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "exportConfig":{
          menuItems: [{
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          format: 'png'
          }]
        }
    });
    @endif
});
</script>
@stop
