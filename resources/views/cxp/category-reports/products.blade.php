@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-sitemap"></i>
        Category Reports
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('category-report-products', $report) !!}
    <div class="row">
        <div class="col-xs-12">
          <h4 class="text-center">Programs</h4>
            <table id="programs" class="table table-hover table-bordered results">
                <thead>
                    <th class="col-xs-5">Title</th>
                    <th>Airs</th>
                    <th>Spend</th>
                </thead>
                <tbody>
                @foreach($programs as $program)
                    <tr>
                        <td><a href="{!! route("category-reports.program", array($report->id, $program->id)) !!}">{!! $program->title !!}</a></td>
                        <td>{!! $program->airs !!}</td>
                        <td>$ {!! number_format($program->media,2, '.',',') !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
          <h4 class="text-center">Spots</h4>
            <table id="spots" class="table table-hover table-bordered results">
                <thead>
                    <th class="col-xs-5">Title</th>
                    <th class="col-xs-2">Airs</th>
                </thead>
                <tbody>
                @foreach($spots as $spot)
                    <tr>
                        <td><a href="{!! route("category-reports.spot", array($report->id, $spot->id)) !!}">{!! $spot->title !!}</a></td>
                        <td>{!! $spot->airs !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('script')
<script>
var programsTable, spotsTable;
$(function(){
  programsTable = $('#programs').dataTable( {
    "pageLength": 10,



    "columns": [
      null,
      null,
      null
    ],
    "oTableTools": {
        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
        "aButtons": [
            "copy",
            "csv",
            "xls",
            {
                "sExtends": "pdf",
                "sPdfOrientation": "landscape",
                "sPdfMessage": "IMS Report - Networks Monitored"
            },
            "print"
        ]
    }
  });

  spotsTable = $('#spots').dataTable( {
    "pageLength": 10,



    "columns": [
      null,
      null
    ],
    "oTableTools": {
        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
        "aButtons": [
            "copy",
            "csv",
            "xls",
            {
                "sExtends": "pdf",
                "sPdfOrientation": "landscape",
                "sPdfMessage": "IMS Report - Networks Monitored"
            },
            "print"
        ]
    }
  });
});
</script>
@stop
