@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-calendar"></i>
        Show Scheduler &bull;
        <a href="{!! route('programs.show', [$program->id]) !!}">
            {!!$program->grid_title!!}
        </a>
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('program',$program) !!}
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-center">
            <form class="form-inline">
                <div class="form-group">
                    {!! Form::select('versions[]', $versionA, $vSelected, array('multiple', 'id' => 'versions')) !!}
                </div>
                <br><br>
                <div class="form-group">
                    {!! Former::xlarge_text('start-date','', $start_date->format("F j, Y"))->class('span4 form-control start-date')->placeholder($start_date->format("F j, Y"))->required() !!}
                </div>
                <div class="form-group">
                    {!! Former::xlarge_text('end-date','', $end_date->format("F j, Y"))->class('span4 form-control end-date')->placeholder($end_date->format("F j, Y"))->required() !!}
                </div>
                <div class="form-group">
                    <button class="select btn btn-blue btn-icon">Select Date Range and Versions <i class="fa fa-calendar"></i></button>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-sm btn-danger" data-toggle="popover" title="Help" data-content="Scheduler will load data for all versions available in the selected date range. To omit a version please click the x next to the title. If you wish to add a version click in the empty space in the version bar to select."><i class="fa fa-question"></i></button>
                </div>
            </form>
        </div>
    </div>
    @if($airtimes)
    <div class="row">
        <div class="col-sm-3">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $airtimes->airs !!}" data-postfix data-duration="1500">0</div>
                <h3>Total Number of Airs</h3>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">{!! number_format($airtimes->media_index,2) !!}</div>
                <h3>Total Media Index</h3>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                    @if($airtimes->airs > 0)
                        ${!! number_format($airtimes->total_cost/$airtimes->airs,2) !!}
                    @else
                        N/A
                    @endif
                </div>
                <h3>Average Cost Per Air</h3>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $airtimes->channels !!}" data-postfix data-duration="1500">0</div>
                <h3>Total Channels</h3>
            </div>
        </div>
    </div>
    @if($channels)
    <div class="row">
        <!-- Top 10 Networks -->
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Top 10 Networks
                    </div>
                    <div class="panel-options">
                        <a href="#" data-rel="collapse"><i class="fa fa-chevron-down"></i></a>
                        <a href="#" data-rel="close"><i class="fa fa-times-circle-o"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="network-distribution" style="width:100%;height: 400px;"></div>
                </div>
            </div>
        </div>
        <!-- END Top 10 Networks -->
        <!-- Weekly Breakdown -->
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Weekly Breakdown
                    </div>
                    <div class="panel-options">
                        <a href="#" data-rel="collapse"><i class="fa fa-chevron-down"></i></a>
                        <a href="#" data-rel="close"><i class="fa fa-times-circle-o"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="network-distribution-bar" style="width:100%;height: 400px;"></div>
                </div>
            </div>
        </div>
        <!-- END Weekly Breakdown -->
    </div>
    @endif
    @if($overTime)
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Ranking History
                    </div>
                    <div class="panel-options">
                        <a href="#" data-rel="collapse"><i class="fa fa-chevron-down"></i></a>
                        <a href="#" data-rel="close"><i class="fa fa-times-circle-o"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="col-xs-4">
                        <table id="rank" class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Week Ending</th>
                                    <th>Airings (Rank)</th>
                                    <th>Media Index (Rank)</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($overTime as $time)
                                <tr>
                                    <td>{!! $time->week_ending->format('m/d/Y') !!}</td>
                                    <td>{!! $time->airs !!} ({!! $time->freq_rank !!})</td>
                                    <td>{!! $time->media !!} ({!! $time->media_rank !!})</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-8">
                        <div id="rank-hist" style="width: 100%;height: 300px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <!-- Raw Airings -->
        <div class="col-xs-12">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Raw Airings <small>All times are in Eastern Time</small>
                    </div>
                    <div class="panel-options">
                        <a href="#" data-rel="collapse"><i class="fa fa-chevron-down"></i></a>
                        <a href="#" data-rel="close"><i class="fa fa-times-circle-o"></i></a>
                    </div>
                </div>
                <div class="panel-body">
                    <table id="airings" class="table table-hover table-bordered results">
                        <thead>
                            <th>Network</th>
                            <th>Grid Title</th>
                            <th>Version</th>
                            <th>Day of Week</th>
                            <th>Air Date</th>
                            <th>Air Time</th>
                        </thead>
                        <tbody>
                            @foreach($airtimes_raw as $airtime)
                            <tr class="text-center">
                                <td>
                                <span data-order="{!! $airtime->channel->name !!}">
                                @if($airtime->channel->logo_location)
                                    <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $airtime->channel->logo_location !!}" class="network-img" />
                                    {!! $airtime->channel->name !!}
                                @else
                                    <p class="text-center">{!! $airtime->channel->name !!}</p>
                                @endif
                                </span>
                                </td>
                                <td>{!! $airtime->programVersion->grid_title !!}</td>
                                <td>{!! $airtime->programVersion->version !!}</td>
                                <td>{!! $airtime->air_dateable->format("l") !!}</td>
                                <td>{!! $airtime->air_dateable->format("m/d/Y") !!}</td>
                                <td>{!! $airtime->air_dateable->format("H:i") !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END Raw Airings -->

    </div>
    @endif
@stop

@section('script')
<script>
$(function(){
    $('#versions').select2();

    @if($channels)
    var chart = AmCharts.makeChart("network-distribution", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $channels->slice(0, 10)->toJson() !!},
        "valueField": "airs",
        "titleField": "channel_name",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "amExport": {
            top : 0,
            right : 50,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
    var lineChart = AmCharts.makeChart("network-distribution-bar", {
        "type": "serial",
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "categoryField": "channel_name",
        "rotate": true,
        "placeholder": "Select a Channel",
        "allowClear": true,
        "startDuration": 1,
        "categoryAxis": {
            "gridPosition": "start"
        },
        "chartCursor": {},
        "chartScrollbar": {},
        "trendLines": [],
        "graphs": [
            {
                "balloonText": "[[title]] of [[category]]:[[value]]",
                "fillAlphas": 1,
                "id": "AmGraph-1",
                "title": "Airings",
                "type": "column",
                "valueField": "airs"
            }
        ],
        "guides": [],
        "valueAxes": [
            {
                "id": "ValueAxis-1",
                "title": "Frequency of Show Airings by Network"
            }
        ],
        "allLabels": [],
        "balloon": {},
        "legend": {},
        "dataProvider": {!! $channels->toJson() !!},
        "amExport": {
            top : 0,
            right : 50,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
    @endif

  $('#airings').dataTable();
  $('#rank').dataTable();

@if($overTime)
    var overtime = AmCharts.makeChart("rank-hist", {
        "type": "serial",
        "theme": "none",
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "dataProvider": {!! $overTime->toJson() !!},
        "legend": {
            "useGraphSettings": true
        },
        "valueAxes": [{
            "id":"v1",
            "axisColor": "#FF6600",
            "axisThickness": 2,
            "gridAlpha": 0,
            "offset": 50,
            "axisAlpha": 1,
            "position": "left"
        }, {
            "id":"v2",
            "axisColor": "#FCD202",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "left"
        }, {
            "id":"v3",
            "axisColor": "#B0DE09",
            "axisThickness": 2,
            "gridAlpha": 0,
            "offset": 50,
            "axisAlpha": 1,
            "maximum": 250,
            "minimum": 1,
            "reversed": true,
            "position": "right"
        }, {
            "id":"v4",
            "axisColor": "#0D4F8B",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "maximum": 250,
            "minimum": 1,
            "reversed": true,
            "position": "right"
        }],
        "graphs": [{
            "valueAxis": "v1",
            "axisColor": "#FF6600",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Frequency",
            "valueField": "airs"
        }, {
            "valueAxis": "v2",
            "axisColor": "#FCD202",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Media Index",
            "valueField": "media"
        }, {
            "valueAxis": "v3",
            "axisColor": "#B0DE09",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Media Rank",
            "valueField": "media_rank"
        }, {
            "valueAxis": "v4",
            "axisColor": "#0D4F8B",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Frequency Rank",
            "valueField": "freq_rank"
        }],
        "zoomOutButtonRollOverAlpha": 0.15,
        "chartCursor": {
            "categoryBalloonDateFormat": "MMM DD JJ:NN",
            "cursorPosition": "mouse",
            "showNextAvailable":true
        },
        "autoMarginOffset": 5,
        "columnWidth": 1,
        "categoryField": "week_ending",
        "categoryAxis": {
            "minPeriod": "hh",
            "parseDates": true
        },
        "amExport": {
            top : 0,
            right : 150,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
@endif
});
</script>
@stop
