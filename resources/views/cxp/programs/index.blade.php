@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-video-camera"></i>
        Shows &bull; {!! $language->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('programs') !!}
    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center"><i class="fa fa-video-camera"></i> New Shows - {!! $week_ending->format("F j, Y") !!}</h4>
            <div class="date-selection text-center">
            <form class="form-inline" id="week_ending_frm" style="display:inline-block;">
                <input type="hidden" name="language_id" value="{!! $language_id !!}" />
                {!! Former::xlarge_text('week_ending','')->id('pickadate')->class('span6 form-control')->placeholder($week_ending->format("F j, Y")) !!}
                <button class="select btn btn-blue btn-icon">Select Week Ending <i class="fa fa-calendar"></i></button>
            </form>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        @if($programs)
            <table id="report" class="table table-hover table-bordered results datatable dataTable">
                <thead>
                    <th>Title</th>
                    <th>Grid Title</th>
                    <th>Marketing Co.</th>
                    <th>Category</th>
                    <th>Sub-Category</th>
                    <th>First Detected</th>
                </thead>
                <tbody>
                @foreach($programs as $program)
                    <tr>
                        <td>{!! Html::linkRoute('programs.show',$program->title, $program->id) !!}</td>
                        <td>{!! $program->grid_title !!}</td>
                        <td>
                        @if($program->marketingCompany)
                            {!! $program->marketingCompany->name !!}
                        @endif
                        </td>
                        <td>
                        @if($program->category)
                            {!! $program->category->name !!}
                        @endif
                        </td>
                        <td>
                        @if($program->subCategory)
                            {!! $program->subCategory->name !!}
                        @endif
                        </td>
                        <td>{!! date("m/d/Y h:i a",strtotime($program->initial_date)) !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="no_data">No shows this week yet...</div>
        @endif
        </div>
    </div>
@stop

@section('script')
<script>
var oTable;
$(function(){
  oTable = $('#report').dataTable( {
    "pageLength": 100,
    "lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]],
    responsive: true
  });
});
</script>
@stop
