@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-video-camera"></i>
        Show Overview : {!! $program->grid_title !!}
        @if(Sentry::getUser()->isSuperUser())
            <small><a href="{!! route("admin.programs.edit",array($program->id)) !!}" target="_blank">Edit</a></small>
        @endif
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('program',$program) !!}

    @if($program->program->product->first())
    <iframe src="{{ route('product.embed', ['product' => $program->program->product->first()->id, "hideCTA" => "true"]) }}" scrolling="no" frameborder="0" width="100%" height="700" style="width:100%;height: 700px; border: 0px;"></iframe>
    @endif

    @if(Sentry::getUser()->isSuperUser())
    <div class="date-selection text-center">
        <form class="form-inline" id="week_ending_frm" style="display:inline-block;">
            <span style="color: red;">Admin Only: </span> {!! Former::xlarge_text('week_ending','', $week_ending->format("F j, Y"))->id('weekly-rankings')->placeholder($week_ending->format("F j, Y")) !!}
            <button class="select btn btn-small">Select Week Ending</button>
        </form>
    </div>
    @endif
    @if(Sentry::getUser()->hasAccess('reports.show-overview'))
    <div class="row">
        <ul class="list-inline text-center stats-overview">
                <li>
                    <div class="well well-sm">
                        <h5>Frequency</h5>
                        <h3>
                        @if($programWeekly)
                            {!! $programWeekly->airs !!} ({!! $programWeekly->freq_rank !!})
                        @else
                            N/A
                        @endif
                        </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Media Index</h5>
                        <h3>
                        @if($programWeekly)
                            {!! $programWeekly->media_index !!} ({!! $programWeekly->media_rank !!})
                        @else
                            N/A
                        @endif
                        </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Life Avg Freq</h5>
                        <h3>
                            {!! number_format($fivetwofreq, 2) !!}
                        </h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Life Freq Min</h5>
                        <h3>{!! number_format($fivetwofreqmin, 0) !!}</h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Life Freq Max</h5>
                        <h3>{!! number_format($fivetwofreqmax, 0) !!}</h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Life Avg Media Idx</h5>
                        <h3>{!! number_format($fivetwomedia,2) !!}</h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Life Media Idx Min</h5>
                        <h3>{!! number_format($fivetwomediamin,2) !!}</h3>
                    </div>
                </li>
                <li>
                    <div class="well well-sm">
                        <h5>Life Media Idx Max</h5>
                        <h3>{!! number_format($fivetwomediamax,2) !!}</h3>
                    </div>
                </li>
        </ul>
                    @if($overTime)
                            <div id="show-trend" style="width: 100%;height: 300px;"></div>
                    @endif
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Show Details
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table">
                        @if($program->product_name)
                        <tr>
                            <td class="bold">{!! __('programs.product_name') !!}</td><td>{!! $program->product_name !!} v{!! $program->version !!}</td>
                        </tr>
                        @endif
                        @if($program->host)
                        <tr>
                            <td class="bold">{!! __('programs.host') !!}</td><td>{!! $program->host !!}{{ ($program->host_extra != '') ? ', '.$program->host_extra : '' }}</td>
                        </tr>
                        @endif
                        @if($program->price)
                        <tr>
                            <td class="bold">{!! __('programs.price') !!}</td><td>{!! $program->price !!}</td>
                        </tr>
                        @endif
                        @if($program->category)
                        <tr>
                            <td class="bold">{!! __('programs.category') !!}</td><td>{!! $program->category->name.', '.$program->subCategory->name !!}</td>
                        </tr>
                        @endif
                        @if($program->marketingCompany)
                        <tr>
                            <td class="bold">{!! __('programs.marketing_company') !!}</td>
                            <td>
                                <a href="{!! route("companies.show", array($program->marketingCompany->id)) !!}">{!! $program->marketingCompany->name !!}</a>  @if(Sentry::getUser()->isSuperUser()) <small><a href="{!! route("admin.companies.edit",array($program->marketingCompany->id)) !!}" target="_blank">Edit</a></small> @endif
                            </td>
                        </tr>
                        @endif
                        @if($program->productionCompany and $program->productionCompany->name != 'N/A')
                        <tr>
                            <td class="bold">{!! __('programs.production_company') !!}</td><td>{!! $program->productionCompany->name !!}</td>
                        </tr>
                        @endif
                        @if($program->order_address != '')
                        <tr>
                            <td class="bold">{!! __('programs.address') !!}</td><td>{!! $program->order_address !!}</td>
                        </tr>
                        @endif
                        @if($program->order_phone)
                        <tr>
                            <td class="bold">{!! __('programs.phone') !!}</td><td>{!! ($program->order_phone != '') ? $program->order_phone : 'N/A' !!}</td>
                        </tr>
                        @endif
                        @if($program->website)
                        <tr>
                            <td class="bold">{!! __('programs.website') !!}</td><td>{!! makeClickable($program->website) !!}</td>
                        </tr>
                        @endif
                        @if($program->channel)
                        <tr>
                            <td class="bold">{!! __('programs.channel') !!}</td><td>{!! $program->channel->name !!}</td>
                        </tr>
                        @endif
                        @if($program->monitor_date != '0000-00-00')
                        <tr>
                            <td class="bold">{!! __('programs.week_ending') !!}</td><td>{!! date("m/d/Y",strtotime($program->monitor_date)) !!}</td>
                        </tr>
                        @endif
                    </table>
                    @if(Sentry::getUser()->hasAccess('reports.network-details'))
                    <p class="text-center">
                        @if(Sentry::getUser()->hasAccess('reports.product-scheduler')) {!! Html::linkRoute('programs.schedule','Show Scheduler',$program->id, array('class'=>'btn btn-blue')) !!}@endif
                                {{-- <a class="btn btn-warning" href="#"><i class="fa fa-plus-circle"></i>&nbsp;Add To Favorites</a> --}}
                    </p>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow" data-collapsed="0">
                <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title">Show Video</div>
                </div>
                <div class="panel-body text-center">
                @if(Sentry::getUser()->hasAccess('reports.program-video'))

                    @if($program->file_name)
                    <video id="program-video" class="video-js vjs-default-skin text-center hidden-xs" controls preload="none" width="100%" height="324"
                        @if(isset($signed_poster))
                            poster="{!! $signed_poster !!}"
                        @else
                            poster="/assets/img/video-poster.png"
                        @endif
                    data-setup="{}">
                        @if(isset($signed_flv))
                            <source src="{!! $signed_flv !!}" type='video/flv' />
                        @elseif(isset($signed_mp4))
                            <source src="{!! $signed_mp4 !!}" type='video/mp4' />
                        @endif
                    </video>
                        <p class="text-center">
                        @if(isset($signed_flv))
                            <a href="{!! $signed_flv !!}" target="_blank" class="btn btn-info btn-icon icon-left"><i class="fa fa-download"></i> Click to Download</a>
                        @elseif(isset($signed_mp4))
                            <a href="{!! $signed_mp4 !!}" target="_blank" class="click-download btn btn-info btn-icon icon-left"><i class="fa fa-download"></i> Click to Download</a>
                        @endif
                        </p>
                    @else
                        <div class="no_data">Video not available.</div>
                    @endif
                @endif
                </div>
            </div>
        </div>

    </div>
    @if($program->description)
    <div class="row">
        <div class="col-xs-12">

                <div class="panel panel-default panel-shadow" data-collapsed="0">

                <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title">Show Description</div>
                </div>

                <!-- panel body -->
                <div class="panel-body">

                        @if($program->description)
                            {!! $program->description !!}
                        @else
                            <div class="no_data">No description available.</div>
                        @endif
                </div>

            </div>
        </div>
    </div>
    @endif
    @if($program->summary)
    <div class="row">

        <div class="col-xs-12">

                <div class="panel panel-default panel-shadow" data-collapsed="0">

                <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title">Show Summary</div>
                </div>

                <!-- panel body -->
                <div class="panel-body">

                    @if($program->summary)
                        {!! $program->summary !!}
                    @else
                        <div class="no_data">No summary available.</div>
                    @endif
                </div>

            </div>
        </div>
    </div>
    @endif
    @if($program->upsell)
    <div class="row">

        <div class="col-xs-12">

                <div class="panel panel-default panel-shadow" data-collapsed="0">

                <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title">Show Upsell</div>
                </div>

                <!-- panel body -->
                <div class="panel-body">

                    @if($program->upsell)
                        {!! $program->upsell !!}
                    @else
                        <div class="no_data">No upsell available.</div>
                    @endif
                </div>

            </div>
        </div>
    </div>
    @endif
    @include('cxp.partials.modals.updateDescription')
    <div class="row">
        <div class="col-xs-offset-5">
            <div class="panel">
                <a data-toggle="modal" href="#updatePageDescriptionModal" class="btn btn-info">
                    <i class="fa fa-envelope-o"></i> Request an Update</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        Show Versions
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th class="col-xs-5">Grid Title</th>
                            <th class="col-xs-3">First Detected</th>
                            <th class="col-xs-3 hidden-xs hidden-sm">Last Detection</th>
                            <th class="col-xs-1"></th>
                        </thead>
                    @foreach($programVersions as $version)
                        <tr>
                            <td>
                                {{ $version->grid_title }}
                            </td>
                            <td>
                                {!! $version->initial_date->format("m/d/Y <\b\\r> h:i a") !!}
                            </td>
                            <td class="hidden-xs hidden-sm">
                                @if ($version->has('airtimes'))
                                {!!Carbon\Carbon::parse($version->airtimes->max('air_date'))->format("m/d/Y <\b\\r> h:i a")!!}
                                @else
                                N/A
                                @endif
                            </td>
                            <td>
                            @if($version->id == $program->id)
                                <a href="{!! route('programs.show', array($version->id)) !!}" class="btn btn-green btn-icon icon-left disabled"><i class="fa fa-eye"></i> View</a>
                            @else
                                <a href="{!! route('programs.show', array($version->id)) !!}" class="btn btn-green btn-icon icon-left"><i class="fa fa-eye"></i> View</a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Week Ending: {!! $week_ending->format("F j, Y") !!} (All Versions)
                    @if(Sentry::getUser()->hasAccess('reports.network-details'))
                        <small style="float:right;">Networks With Airings: {!! $totalChannels->channels !!}</small>
                    @endif
                    </div>
                </div>
            @if(Sentry::getUser()->hasAccess('reports.network-details'))
                <div class="panel-body">
                    @if($totalChannels->channels > 0)
                    <table class="table table-hover table-bordered networks">
                        <thead>
                            <th>Network</th>
                            <th style="text-align:center;">Frequency</th>
                            <th style="text-align:center;">Media Index</th>
                        </thead>
                        <tbody>
                    <?php $count = 0; ?>
                    @foreach($channelList as $channel)
                        <tr>
                        <td>
                        @if($channel->logo_location)
                            <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $channel->logo_location !!}" class="network-img" />
                            <p class="text-center">{!! $channel->abbr !!}</p>
                        @else
                            <p class="text-center">{!! $channel->abbr !!}</p>
                        @endif
                        </td>
                        <td class="count text-center">{!! $channel->chancount !!}</td>
                        <td class="count text-center">{!! number_format($channel->media_index,2) !!}</td>
                        </tr>
                        <?php $count ++;?>
                    @endforeach
                        </tbody>
                    </table>
                    @else
                        <div class="no_data">Network details are unavailable for this show, because it did not air in the week ending on {!! $week_ending->format("F j, Y") !!}.</div>
                    @endif
                    <p class="text-center">@if(Sentry::getUser()->hasAccess('reports.product-scheduler')) {!! Html::linkRoute('programs.schedule','Show Scheduler',$program->id, array('class'=>'btn btn-blue')) !!}@endif</p>
                </div>
            @else
                <div class="panel-body">
                    <p>Network details are unavailable with your current plan. {!! Html::linkRoute('upgrade','Upgrade your plan today.') !!}</p>
                </div>
            @endif
            </div>
        </div>
    </div>
@stop

@section('script')
<script>
$('.modal').appendTo('body');
videojs.options.flash.swf = "{!! asset('assets/swf/video-js.swf') !!}";
@if($overTime)
    var overtime = AmCharts.makeChart("show-trend", {
        "type": "serial",
        "theme": "none",
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "dataProvider": {!! $overTime->toJson() !!},
        "legend": {
            "useGraphSettings": true
        },
        "valueAxes": [{
            "id":"v1",
            "axisColor": "#FF6600",
            "axisThickness": 2,
            "gridAlpha": 0,
            "offset": 50,
            "axisAlpha": 1,
            "position": "left"
        }, {
            "id":"v2",
            "axisColor": "#FCD202",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "left"
        }, {
            "id":"v3",
            "axisColor": "#B0DE09",
            "axisThickness": 2,
            "gridAlpha": 0,
            "offset": 50,
            "axisAlpha": 1,
            "maximum": 250,
            "minimum": 1,
            "reversed": true,
            "position": "right"
        }, {
            "id":"v4",
            "axisColor": "#0D4F8B",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "maximum": 250,
            "minimum": 1,
            "reversed": true,
            "position": "right"
        }],
        "graphs": [{
            "valueAxis": "v1",
            "axisColor": "#FF6600",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Frequency",
            "valueField": "airs"
        }, {
            "valueAxis": "v2",
            "axisColor": "#FCD202",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Media Index",
            "valueField": "media"
        }, {
            "valueAxis": "v3",
            "axisColor": "#B0DE09",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Media Rank",
            "valueField": "media_rank"
        }, {
            "valueAxis": "v4",
            "axisColor": "#0D4F8B",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Frequency Rank",
            "valueField": "freq_rank"
        }],
        "zoomOutButtonRollOverAlpha": 0.15,
        "chartCursor": {
            "categoryBalloonDateFormat": "MMM DD JJ:NN",
            "cursorPosition": "mouse",
            "showNextAvailable":true
        },
        "autoMarginOffset": 5,
        "columnWidth": 1,
        "categoryField": "date",
        "categoryAxis": {
            "minPeriod": "hh",
            "parseDates": true
        },
        "amExport": {
            top : 0,
            right : 150,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
@endif

    var oTable;
    $(function(){
      oTable = $('.networks').dataTable( {
        "order": [[ 1, "desc" ]]
      });
    });
</script>
@stop
