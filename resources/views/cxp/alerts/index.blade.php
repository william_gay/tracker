@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-exclamation-triangle"></i>
        Manage Alerts
    </h3>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            {!! Former::horizontal_open( route('user.alerts.email'), 'POST' ) !!}
                <fieldset>
                <legend>Rankings Email Alerts</legend>
                <div class="row">
                    <div class="col-xs-4">
                        <label for="email_weekly" class="control-label">
                            Receive Weekly Rankings Email?
                            <input id="email_weekly" type="checkbox" name="email_weekly" @if(Sentry::getUser()->email_weekly == 1) checked @endif value="1">
                        </label>
                    </div>
                    <div class="col-xs-4">
                        <label for="email_monthly" class="control-label">
                            Receive Monthly Rankings Email?
                            <input id="email_monthly" type="checkbox" name="email_monthly" @if(Sentry::getUser()->email_monthly == 1) checked @endif value="1">
                        </label>
                    </div>
                    <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary">Save Alerts</button>
                    </div>
                </div>
            </fieldset>
            {!! Former::close() !!}
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        @if($alerts->count() > 0)
            <table class="table table-striped table-bordered">
                <thead>
                    <th width="15"></th>
                    <th>How Often?</th>
                    <th>What Kind?</th>
                    <th>What Category?</th>
                    <th width="25" style="text-align:center;"><i class="fa fa-cog"></i></th>
                </thead>
                <tbody>
                @foreach($alerts as $alert)
                <tr>
                    <td style="text-align:center;"><i class="fa fa-exclamation-triangle" style="color:red;"></i></td>
                    <td>{!! $intervals[$alert->interval] !!}</td>
                    <td>{!! $types[$alert->type] !!}</td>
                    <td>{!! isset($alert->type_extra) ? $categories[$alert->type_extra] : 'All' !!}</td>
                    <td style="text-align:center;">
                        <a href="{!! route('alerts.destroy', array($alert->id)) !!}" data-method="delete" data-modal-text="Delete this Alert?" class="btn btn-danger">
                            <i class="fa fa-trash-o"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="alert alert-warning">No alerts configured yet, use the form below to get started!</div>
        @endif
        @if($create_new_alert)
            {!! Former::horizontal_open( route('user.alerts'), 'POST' ) !!}
            <fieldset>
                <legend>Create Alert</legend>
                <div class="row">
                    <div class="col-xs-4">
                        <h2>Alert Interval</h2>
                    {!! Former::select('interval')
                        ->class('form-control')
                        ->options($intervals, 'weekly')
                        ->help('How often do you want to be notified?') !!}
                    </div>
                    <div class="col-xs-4">
                        <h2>Alert Type</h2>
                    {!! Former::select('type')
                        ->class('form-control')
                        ->options($types)
                        ->id('alert_type')
                        ->help('What type of notifications would you like to receive?') !!}
                    </div>
                    <div class="col-xs-4" id="alert_category">
                        <h2>Category</h2>
                    {!! Former::select('type_extra','Category')
                        ->class('form-control')
                        ->options($categoriesContained)
                        ->help('What category would you like to receive notifications on?')
                        ->disabled() !!}
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls"><br>
                        <button type="submit" class="btn btn-primary">Add Alert</button>
                    </div>
                </div>
            </fieldset>
            {!! Former::close() !!}
        </div>
        @else
            <fieldset>
                <legend>Create Alert</legend>
                <h4>You have reached your alert limit, please contact <a href="mailto:sales@imsreport.com">sales@imsreport.com</a> if you would like to upgrade.</h4>
            </fieldset>
        @endif
    </div>
@stop

@section('script')
<script>
$(function(){
    $('#alert_type').change(function(e){
        if($(this).val() == 'category') {
            $('#type_extra').prop('disabled', false);
            $('#alert_category').slideDown();
        } else {
            $('#type_extra').prop('disabled', true);
            $('#alert_category').slideUp();
        }
    });

    $('#alert_category').hide();
});
</script>
@stop
