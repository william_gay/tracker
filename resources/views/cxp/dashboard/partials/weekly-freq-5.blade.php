<h3 class="text-center">Frequency</h3>
<table class="table table-bordered table-striped">
	<thead>
		<th>This Week</th>
		<th>Last Week</th>
		<th>Title</th>
		<th># of Airs</th>
	</thead>
	<tbody>
	@foreach($freq5Weekly as $freq)
		<tr>
			<td>{!! $freq->freq_rank !!}</td>
			<td>{!! $freq->prev_freq_rank !!}</td>
			<td>{!! $freq->program->title !!}</td>
			<td>{!! $freq->airs !!}</td>
		</tr>
	@endforeach
	</tbody>
</table>
