<h3 class="text-center">Media Index</h3>
<table class="table table-bordered table-striped">
	<thead>
		<th>This Week</th>
		<th>Last Week</th>
		<th>Title</th>
		<th>Media Index</th>
	</thead>
	<tbody>
	@foreach($media5Weekly as $media)
		<tr>
			<td>{!! $media->media_rank !!}</td>
			<td>{!! $media->prev_media_rank !!}</td>
			<td>{!! $media->program->title !!}</td>
			<td>{!! $media->media_index !!}</td>
		</tr>
	@endforeach
	</tbody>
</table>