@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-tachometer"></i>
        My Dashboard
    </h3>
@stop

@section('content')

<!-- Main Stats -->
    <div class="row">
        <!-- New Products -->
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="icon"><i class="fa fa-video-camera"></i></div>
                <div class="num" data-start="0" data-end="{!! $ssStats->newShowCount !!}" data-postfix data-duration="1500">
                    @if(Sentry::getUser()->hasAccess('reports.national-shows-new'))
                     <a href="/programs/new">0</a>
                    @else
                        0
                    @endif
                </div>
                <h3>New Shows</h3>
                <p>Week Ending {!! date("F jS, Y",strtotime($lastFriday)) !!}</p>
            </div>

            {{--New Shows--}}
            <div class="panel panel-default panel-shadow" data-collapsed="0">
                <!-- panel body -->
                <div class="panel-body dashboard-panel">

                    <h4 class="text-center">New Shows</h4>

                    <table class="table responsive">
                        <thead>
                            <tr>
                                <th class="bold">Product</th>
                                <th class="bold">Category</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ssStats->newShows as $show)
                            <tr>
                                <td>
                                {!! Html::linkRoute('programs.show',$show->title, $show->id) !!}
                                </td>
                                <td>
                                    @if ($show->category)
                                    {!! $show->category->name !!}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <p class="text-center">
                         <a href="/programs/new" class="btn btn-blue">See All</a>
                    </p>
                </div>
                <!-- END panel body -->
            </div>
        </div>
        <!-- END New Products -->

        <!-- Creatives -->
        <div class="col-lg-3 col-md-6">

            <div class="tile-stats tile-white tile-white-primary">
                <div class="icon"><i class="fa fa-tint"></i></div>
                <div class="num" data-start="0" data-end="{!! $ssStats->newSpotCount !!}" data-postfix data-duration="1500">
                    @if(Sentry::getUser()->hasAccess('reports.national-spots-new'))
                        <a href="/spots/new">0</a>
                    @else
                        0
                    @endif
                </div>
                <h3>New Spots</h3>
                <p>Week Ending {!! date("F jS, Y",strtotime($lastFriday)) !!}</p>
            </div>
            {{--New Spots--}}
            <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                <!-- panel body -->
                <div class="panel-body dashboard-panel">

                    <h4 class="text-center">New Spots</h4>

                    <table class="table responsive">
                        <thead>
                            <tr>
                                <th class="bold">Product</th>
                                <th class="bold">Category</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ssStats->newSpots as $spot)
                            <tr>
                                <td>{!! Html::linkRoute('spots.show',$spot->title, $spot->id) !!}</td>
                                <td>
                                @if($spot->category)
                                    {!! $spot->category->name !!}
                                @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <p class="text-center">
                         <a href="/spots/new" class="btn btn-blue">See All</a>
                    </p>
                </div>
                <!-- END panel body -->
            </div>

        </div>
        <!-- END New Creatives -->

        <!-- Programs -->
        <div class="col-lg-3 col-md-6">

            <div class="tile-stats tile-white-orange">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
                <div class="num" data-start="0" data-end="{!! $ssStats->totalShowCount !!}" data-postfix data-duration="1500">0</div>

                <h3>Unique Long Form Shows Detected</h3>
                <p>
                    @if(Sentry::getUser()->hasAccess('reports.national-shows'))
                        {!! link_to_route('programs.all', 'Show Search', array('language_id' => 1)) !!}
                    @else
                        <a data-toggle="modal" data-target="#upgradeModal">Show Search</a>
                    @endif
                </p>
            </div>
            {{-- Top Shows--}}
            <div class="panel panel-default panel-shadow" data-collapsed="0">
                <!-- panel body -->
                <div class="panel-body dashboard-panel">

                    <h4 class="text-center">Top 5 Shows</h4>

                    <table class="table responsive">
                        <thead>
                            <tr>
                                <th class="bold">Product</th>
                                <th class="bold">Airs</th>
                                <th class="bold" data-toggle="tooltip" data-placement="top" title="Media Index">IDX</th>
                                <th class="bold text-center">Networks</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($showfreq5Weekly as $rank)
                            <tr>
                                <td>
                                {!! Html::linkRoute('programs.show',$rank->title, $rank->program_version_id) !!}
                                </td>
                                <td>{!! $rank->airs !!}</td>
                                <td>{!! $rank->media_index !!}</td>
                                <td class="text-center">{!! $rank->stations !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <p class="text-center">
                         <a href="/reports/show/4" class="btn btn-blue">See All</a>
                    </p>
                </div>
                <!-- END panel body -->
            </div>

        </div>
        <!-- END Programs -->

        <!-- Spots -->
        <div class="col-lg-3 col-md-6">

            <div class="tile-stats tile-white-orange">
                <div class="icon"><i class="fa fa-shopping-cart"></i></div>
                <div class="num" data-start="0" data-end="{!! $ssStats->totalSpotCount !!}" data-postfix data-duration="1500">0</div>

                <h3>Unique Short Form Spots Detected</h3>
                <p>
                    @if(Sentry::getUser()->hasAccess('reports.national-spots'))
                        <a href="/spots">Spot Search</a>
                    @else
                        <a data-toggle="modal" data-target="#upgradeModal">Spot Search</a>
                    @endif
                </p>
            </div>

            {{--Top Spots--}}
            <div class="panel panel-default panel-shadow" data-collapsed="0"><!-- to apply shadow add class "panel-shadow" -->
                <!-- panel body -->
                <div class="panel-body dashboard-panel">

                    <h4 class="text-center">Top 5 Spots</h4>

                    <table class="table responsive">
                        <thead>
                            <tr>
                                <th class="bold">Product</th>
                                <th class="bold">Streak</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($spotfreq5Weekly as $rank)
                            <tr>
                                <td>{!! Html::linkRoute('spots.show',$rank->title, $rank->spot_version_id) !!}</td>
                                <td>{!! $rank->streak !!}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <p class="text-center">
                         <a href="/reports/show/36" class="btn btn-blue">See All</a>
                    </p>
                </div>
                <!-- END panel body -->
            </div>

        </div>
        <!-- END Spots -->

    </div>

    <div class="row">
    @if($freqWeekly)
        <div class="col-sm-6 pie-container" style="float:left;">
        @if(Sentry::getUser()->hasAccess('reports.weekly-top-50-frequency-lf'))
            <h3 class="text-center">{!! Html::linkRoute('reports.show','Show Frequency',4) !!}</h3>
        @else
            <h3 class="text-center">Show Frequency</h3>
        @endif
            <div id="weekly-freq" style="width:100%;height:350px;"></div>
        </div>
    @endif
    @if($spotWeekly)
        <div class="col-sm-6 pie-container" style="float:right;">
        @if(Sentry::getUser()->hasAccess('reports.weekly-top-50-spot-ranking'))
            <h3 class="text-center">{!! Html::linkRoute('reports.show','Spot Frequency',25) !!}</h3>
        @else
            <h3 class="text-center">Spot Frequency</h3>
        @endif
            <div id="weekly-spot-freq" style="width:100%;height:350px;"></div>
        </div>
    @endif
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-center">Top 25 Long Form - Last Three Months</h3>
            <div id="media-over-time" style="width:100%;height:300px;"></div>
        </div>
    </div>

@stop


@section('script')
<script>
$(function() {

@if($freqWeekly)
    var chart = AmCharts.makeChart("weekly-freq", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $freqWeekly->toJson() !!},
        "valueField": "airs",
        "titleField": "program_name",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "exportConfig":{
          menuItems: [{
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          format: 'png'
          }]
        }
    });
@endif
@if($spotWeekly)
    var chart = AmCharts.makeChart("weekly-spot-freq", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $spotWeekly->toJson() !!},
        "valueField": "frequency",
        "titleField": "title",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "exportConfig":{
          menuItems: [{
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          format: 'png'
          }]
        }
    });
@endif
@if($overTime)
    var overtime = AmCharts.makeChart("media-over-time", {
        "type": "serial",
        "theme": "none",
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "dataProvider": {!! $overTime->toJson() !!},
        "valueAxes": [{
            "id":"v1",
            "axisColor": "#FF6600",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "left",
            "title": "Frequency"
        }, {
            "id":"v2",
            "axisColor": "#FCD202",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "right",
            "title": "Media Index"
        }],
        "graphs": [{
            "valueAxis": "v1",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Frequency",
            "valueField": "airs"
        }, {
            "valueAxis": "v2",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Media Index",
            "valueField": "media"
        }],
        "zoomOutButtonRollOverAlpha": 0.15,
        "chartCursor": {
            "categoryBalloonDateFormat": "MMM DD JJ:NN",
            "cursorPosition": "mouse",
            "showNextAvailable":true
        },
        "autoMarginOffset": 5,
        "columnWidth": 1,
        "categoryField": "date",
        "categoryAxis": {
            "minPeriod": "hh",
            "parseDates": true
        },
        "amExport": {
            top : 0,
            right : 150,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
@endif
});
</script>

@stop
