@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-gift"></i>
        {!! link_to_route('lst.index', 'Live Shopping Tracker') !!}
        <small>Product &bull; {!! $product->name !!}
        @if(Sentry::getUser()->isSuperUser())
                <a href="{!! route("admin.lst.products.edit",array($product->id)) !!}" target="_blank">Edit</a>
            </small>
        @endif
    </h3>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">{!! $product->name !!}</h4>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6">
        <table class="table table-striped">
            <tr>
                <th>SKU</th>
                <td>
                @if($product->sku)
                    {!! $product->sku !!}
                    @else
                  N/A
                @endif
                </td>
              </tr>
              <tr>
                <th>MSRP</th>
                <td>
                @if($product->msrp)
                    {!! $channel->currency !!}{{ number_format($product->msrp, 2) }}
                @else
                    N/A
                @endif
                </td>
            </tr>
            <tr>
                <th>Category</th>
                <td>{!! link_to_route('lst.category', $product->category->name, ['categoryId' => $product->category->id]) !!},
          {!! link_to_route('lst.sub-category', $product->subCategory->name, ['categoryId' => $product->subCategory->id]) !!}</td>
      </tr>
      <tr>
        <th>Offers</th>
        <td>{!! $versions->count() !!}</td>
      </tr>
      <tr>
        <th>Product URL</th>
        <td><a href="{!! $product->website !!}" target="_blank">{!! $product->name !!}</a></td>
        <tr>
            <th>Last Aired</th>
            <td>
            @if ($airings->last() and $airings->last()->start_time)
                {!! $airings->last()->start_time->format('m/d/Y h:i a') !!}
            @endif
            </td>
        </tr>
        </table>
    </div>
  <div class="col-md-6">
    @if($product->img_location)
        <img src="https://s3-us-west-2.amazonaws.com/ims-lst-products/{!! $product->img_location !!}" class="product-img" style="max-width:100%;" />
    @else
        <p class="text-center">No Product Image</p>
    @endif
  </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Offer Details</h3>

        @if($versions->count() > 0)
        <table class="table table-bordered">
            <thead>
                <th>Version ID</th>
                <th>Configuration</th>
                <th>Price</th>
                <th>Daily Special Value</th>
                <th>Initial Airdate</th>
            </thead>
            <tbody>
            @foreach($versions as $version)
                <tr>
                  <td>{!! $version->newId !!}</td>
                    <td>{!! $version->product_configuration ?? 'N/A' !!}</td>
                    <td>
                        <ul class="list-unstyled">
                            @if ($version->price)
                            <li>
                                <strong>Price:</strong> {!! $channel->currency !!}{{ number_format($version->price, 2) }}
                            </li>
                            @endif
                            @if ($version->shipping_and_handling)
                            <li>
                                <strong>Shipping:</strong>
                                @if (is_numeric($version->shipping_and_handling))
                                    {!! $channel->currency !!}{{ number_format($version->shipping_and_handling, 2) }}
                                @else
                                    {!! $version->shipping_and_handling ?? 'N/A' !!}
                                @endif
                            </li>
                            @endif
                            @if($version->payment_plan)
                            <li>
                                <strong>Payment Plan:</strong> {!! $version->payment_plan_notes !!}
                            </li>
                            @endif
                        </ul>
                    </td>
                    <td>
                        @if($version->daily_special_value)
                            <i class="fa fa-check"></i>
                        @endif
                    </td>
                    <td>
                        {!! $version->first_airdate->format('m/d/Y h:i a') !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <div class="no_data">No versions available.</div>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Show Details</h3>

        <table class="table table-bordered">
            <thead>
        <th>Show Title</th>
                <th>Air Date/Time</th>
                <th>Version ID</th>
                <th>Average Airtime</th>
        <th>Network</th>
            </thead>
            <tbody>
            @foreach($airings as $air)
                <tr>
                    <td>
                    @if ($air->airtime and $air->airtime->show)
                        {!! link_to_route('lst.show', $air->airtime->show->title, ['showId' => $air->airtime->show->id]) !!}
                    @endif
                    </td>
                    <td>
                    @if ($air->start_time and $air->airtime)
                      {!! link_to_route('lst.airtime', $air->start_time->format('m/d/Y h:i a'), ['airtimeId' => $air->airtime->id]) !!}
                    @endif
                    </td>
                    <td>
                    @if ($air->productVersion->newId)
                      {!! $air->productVersion->newId !!}
                    @endif
                    </td>
                    <td>
                    @if ($air->airtime and $air->airtime->id)
                        {!! round(($air->airtime->end_time->diffInMinutes($air->airtime->start_time)) / $air->airtime->products->count(), 2) !!} minutes
                    @endif
                    </td>
                    <td>
                    @if ($air->airtime and $air->airtime->channel)
                        {!! $air->airtime->channel->name !!}
                    @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@stop
