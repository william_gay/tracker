@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-gift"></i>
        {!! link_to_route('lst.index', 'Live Shopping Tracker') !!}
        <small>Show &bull; {!! $show->title !!}
        @if(Sentry::getUser()->isSuperUser())
                <a href="{!! route("admin.lst.shows.edit",array($show->id)) !!}" target="_blank">Edit</a>
            </small>
        @endif
    </h3>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">{!! $show->title !!}</h4>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6">
        <table class="table table-striped">
            <tr>
                <th>Channel</th>
                <td>
                @if($show->channel_id)
                    {!! $show->channel->name !!}
                @else
                  N/A
                @endif
                </td>
            </tr>
            <tr>
              <th>Show Duration</th>
                <td>
                  @if ($show->duration)
                    {!! $show->duration !!} minutes
                  @endif
                </td>
            </tr>
            <tr>
              <th>Last Aired</th>
                <td>
                  @if ($show->airtimes)
                    {!! $show->airtimes->last()->start_time->format('m/d/Y h:i a') !!}
                  @endif
                </td>
            </tr>
        </table>
    </div>
  <div class="col-md-6">
    @if($show->channel->logo_location)
        <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $show->channel->logo_location !!}" class="channel-logo" style="max-width:100%;" />
    @else
        <p class="text-center">No Channel Logo</p>
    @endif
  </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Airtimes</h3>

        @if($show->airtimes)
        <table class="table table-bordered">
            <thead>
                <th>Air Date/Time</th>
                <th>Number of Products</th>
            </thead>
            <tbody>
            @foreach($show->airtimes as $airtime)
                <tr>
                  <td>
                  {!! link_to_route('lst.airtime', $airtime->start_time->format('m/d/Y h:i a'), ['airtimeId' => $airtime->id]) !!}
                </td>
                <td> {!! $airtime->products->count() !!} </th>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <div class="no_data">No versions available.</div>
        @endif
    </div>
</div>
@stop
