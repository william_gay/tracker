@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-gift"></i>
        {!! link_to_route('lst.index', 'Live Shopping Tracker', ['rangeStart' => $range['start']->format('F j, Y'), 'rangeEnd' => $range['end']->format('F j, Y')]) !!} <small>Products</small>
    </h3>
@stop

@section('style')
<style>
.scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
}
.category-chart {
    width: 100%;
    height: 500px;
}
</style>
@stop

@section('content')
    @include('cxp.partials.lst.categories')

    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">New Products by Category</h4>
            <div id="new-products" class="category-chart"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('cxp.lst.partials.products')
        </div>
    </div>

@stop

@section('script')
<script type="text/javascript">
$(document).ready(function(){

    @include('cxp.partials.lst.common')

    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);

    //=============================================================================
    // Data Tables
    //=============================================================================

    var newproducts = AmCharts.makeChart("new-products", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $newProducts->toJson() !!},
        "valueField": "products",
        "titleField": "name",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "urlField": "url",
        "amExport": {
            top : 0,
            right : 50,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });

});
</script>
@stop
