@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-gift"></i>
        {!! link_to_route('lst.index', 'Live Shopping Tracker', ['rangeStart' => $range['start']->format('F j, Y'), 'rangeEnd' => $range['end']->format('F j, Y')]) !!} <small>Categories</small>
    </h3>
@stop

@section('style')
<style>
.scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
}
.category-chart {
    width: 100%;
    height: 400px;
}
</style>
@stop

@section('content')
    @include('cxp.partials.lst.categories')

    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">Average Sale Price by Category</h4>
            <div id="avg-sale" class="category-chart"></div>
        </div>
    </div>

    <hr>

    <div class="row">
        <table class="table table-bordered table-striped" id="lstcategories">
            <thead>
                <th>Name</th>
                <th>Airings</th>
                <th>Mininum Price</th>
                <th>Maximum Price</th>
                <th>Average Sale Price</th>
            </thead>
            <tbody>
            @foreach($topCategories as $cat)
                    <tr>
                        <td>
                            {!! link_to_route('lst.category', $cat->category_name, ['categoryId' => $cat->category_id, 'channels' => $channelsToQuery,
                            'rangeStart' => $range['start']->format('F j, Y'), 'rangeEnd' => $range['end']->format('F j, Y')]) !!}
                        </td>
                        <td>{!! $cat->airs !!}</td>
                        <td><span title="{!! $cat->min_price !!}">{!! $cat->channel_currency !!}{{ $cat->min_price }}</span></td>
                        <td><span title="{!! $cat->max_price !!}">{!! $cat->channel_currency !!}{{ $cat->max_price }}</span></td>
                        <td><span title="{!! $cat->avg_price !!}">{!! $cat->channel_currency !!}{{ $cat->avg_price }}</span></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    @stop

@section('script')
<script type="text/javascript">
$(document).ready(function(){

    @include('cxp.partials.lst.common')

    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);
    //==========================================================================
    // Data Tables
    //==========================================================================

    $('#lstcategories').dataTable({
        searching: false,
        lengthChange: false,
        paging: false,
        "columns": [
          null,
          null,
          null,
          null,
          null,
        ],
        "order": [[ 1, "desc" ]]
    });

    var avgSale = AmCharts.makeChart("avg-sale", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "serial",
        "theme": "none",
        "rotate": true,
        "dataProvider": {!! $topCategories->toJson() !!},
        "valueAxes": [{
            "gridColor":"#FFFFFF",
            "gridAlpha": 0.2,
            "dashLength": 0,
            "unit": "$",
            "unitPosition": "left",
            "precision": 2
        }],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [{
            "balloonText": "[[category]]: <b>$[[value]]</b>",
            "fillAlphas": 0.8,
            "lineAlpha": 0.2,
            "type": "column",
            "valueField": "avg_price",
            "precision": 2,
            "urlField": "url"
        }],
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "category_name",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0,
             "tickPosition":"start",
             "tickLength":20
        },
        "amExport": {
            top : 0,
            right : 50,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
});
</script>
@stop
