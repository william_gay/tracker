@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-gift"></i>
        {!! link_to_route('lst.index', 'Live Shopping Tracker') !!}
        <small>Show &bull; {!! $airtime->show->title !!}
        @if(Sentry::getUser()->isSuperUser())
                <a href="{!! route("admin.lst.shows.edit",array($airtime->show->id)) !!}" target="_blank">Edit</a>
            </small>
        @endif
    </h3>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">{!! link_to_route('lst.show', $airtime->show->title, ['showId' => $airtime->show->id]) !!}</h3>
        <h3 class="text-center">{!! $airtime->start_time->format('m/d/Y h:i a') !!}</h3>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-6">
        <table class="table table-striped">
            <tr>
                <th>Show Name</th>
                <td>
                @if($airtime->show->title)
                    {!! link_to_route('lst.show', $airtime->show->title, ['showId' => $airtime->show->id]) !!}
                @else
                  N/A
                @endif
                </td>
            </tr>
            <tr>
                <th>Channel</th>
                <td>
                @if($airtime->channel_id)
                    {!! $airtime->channel->name !!}
                @else
                  N/A
                @endif
                </td>
            </tr>
            <tr>
              <th>Show Duration</th>
                <td>
                  @if ($airtime->show->duration)
                    {!! $airtime->show->duration !!} minutes
                  @endif
                </td>
            </tr>
            <tr>
              <th>Air Date/Time</th>
                <td>
                  @if ($airtime->start_time)
                    {!! $airtime->start_time->format('m/d/Y h:i a') !!}
                  @endif
                </td>
            </tr>
        </table>
    </div>
  <div class="col-md-6">
    @if($airtime->channel->logo_location)
        <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $airtime->channel->logo_location !!}" class="channel-logo" style="max-width:100%;" />
    @else
        <p class="text-center">No Channel Logo</p>
    @endif
  </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Products</h3>

        @if($airtime->products)
        <table class="table table-bordered">
            <thead>
                <th>Product Name</th>
                <th>Price</th>
                <th>Category</th>
                <th>Sub-Category</th>
            </thead>
            <tbody>
            @foreach($airtime->products as $product)
                <tr>
                  <td> {!! link_to_route('lst.product', $product->productVersion->name, ['productId' => $product->productVersion->product->id]) !!}</td>
                  <td> {!! $product->airtime->channel->currency !!}{{ $product->productVersion->price }} </td>
                  <td> {!! link_to_route('lst.category', $product->productVersion->product->category->name, ['categoryId' => $product->productVersion->product->category->id]) !!}</td>
                  <td> {!! link_to_route('lst.sub-category', $product->productVersion->product->subCategory->name, ['categoryId' => $product->productVersion->product->subCategory->id]) !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            <div class="no_data">No versions available.</div>
        @endif
    </div>
</div>
@stop
