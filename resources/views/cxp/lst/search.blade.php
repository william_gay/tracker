@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-gift"></i>
        {!! link_to_route('lst.index', 'Live Shopping Tracker', ['rangeStart' => $range['start']->format('F j, Y'), 'rangeEnd' => $range['end']->format('F j, Y')]) !!} <small>Search</small>
    </h3>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12">
		  	<h4 class="text-center">Product Search</h4>
            <table id="report" class="table table-hover table-bordered results datatable dataTable">
            {!! $html->table() !!}
        </div>
</div>
@stop

@section('script')
{!! $html->scripts() !!}
@stop
