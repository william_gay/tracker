@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-gift"></i>
        {!! link_to_route('lst.index', 'Live Shopping Tracker', ['rangeStart' => $range['start']->format('F j, Y'), 'rangeEnd' => $range['end']->format('F j, Y')]) !!}
        <small> Category &bull; {!! $category->name !!} &bull; {!! $subCategory->name !!} </small>
    </h3>
@endsection

@section('style')
    <style>
        .scrollable-menu {
            height: auto;
            max-height: 400px;
            overflow-x: hidden;
        }
        .category-chart {
            width: 100%;
            height: 400px;
        }
    </style>
@endsection

@section('content')
    @include('cxp.partials.lst.sub-category')

    <div class="row">
        <div class="col-md-12">
            <h4 class="text-center">Frequency by Airdate</h4>
            <div id="by-airtime" class="category-chart"></div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-12">
            @if ($byChannel->isEmpty())
                <div class="alert alert-info">No Products aired in this category.</div>
            @else
                <table class="table table-bordered table-striped" id="productTable">
                    <thead>
                        <th>Network</th>
                        <th>Average Price</th>
                        <th>Min Price</th>
                        <th>Max Price</th>
                        <th>Frequency</th>
                    </thead>
                    <tbody>
                        @foreach($byChannel as $channel)
                            <tr>
                                <td>{!! $channel->channel_name !!}</td>
                                <td> {!! $channel->channel_currency !!} {!! $channel->avg !!} </td>
                                <td> {!! $channel->channel_currency !!} {!! $channel->min_price !!} </td>
                                <td> {!! $channel->channel_currency !!} {!! $channel->max_price !!} </td>
                                <td>{!! $channel->airs !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            @include('cxp.lst.partials.products')
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){

            @include('cxp.partials.lst.common')

            $('#channel-tab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            $('#channel_picker').multiselect(channelConfigurationSet);

            //Datatables

            $('#productTable').dataTable({
                searching: false,
                lengthChange: false,
                paging: false,
                info: false,
                "columns": [
                    null,
                    { "type": "title-numeric" },
                    { "type": "title-numeric" },
                    { "type": "title-numeric" },
                    null,
                ],
                "order": [[ 0, "desc" ]]
            });


            //Charts

            var byAirtime = AmCharts.makeChart("by-airtime", {
                "pathToImages": "{!!URL::to('/')!!}/assets/img/",
                "type": "serial",
                "theme": "none",
                "legend": {
                    "horizontalGap": 10,
                    "maxColumns": 1,
                    "position": "right",
                    "useGraphSettings": true,
                    "markerSize": 10
                },
                "dataDateFormat": "YYYYMMDD",
                "dataProvider": {!! json_encode($byDate) !!},
                "valueAxes": [{
                    "gridColor":"#FFFFFF",
                    "gridAlpha": 0.2,
                    "dashLength": 0,
                    "unitPosition": "right"
                }],
                "gridAboveGraphs": true,
                "startDuration": 1,
                "graphs": [
                    @foreach($channelsSelected as $channel)
                        {
                            "balloonText": "{!!$channel!!} Airs: <b>[[value]]</b>",
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletColor": "#FFFFFF",
                            "bulletSize": 5,
                            "hideBulletsCount": 50,
                            "useLineColorForBulletBorder": true,
                            "lineThickness": 2,
                            "type": "smoothedLine",
                            // "connect": false,
                            "valueField": "{!!$channel!!}",
                            "title": "{!!$channel!!}",
                        },
                    @endforeach
                ],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": true
                },
                "categoryField": "date",
                "categoryAxis": {
                    "parseDates": true,
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition":"middle",
                    "tickLength":20
                },
                "amExport": {
                    top : 0,
                    right : 50,
                    exportJPG : true,
                    exportPNG : true,
                    exportSVG : true,
                    exportPDF : true,
                    menuItems: [{
                        textAlign: 'center',
                        icon: '{!!URL::to('/')!!}/assets/img/export.png',
                        iconTitle: 'Save chart as an image',
                    }]
                }
            });

        });
    </script>
@endsection
