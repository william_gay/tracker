<h3 class="text-center">Products</h3>
@if ($products->isEmpty())
    <div class="alert alert-info">No Products aired in this category.</div>
@else
    <table class="table table-bordered table-striped" id="lstProducts">
        <thead>
            <th>Product</th>
            <th>Network</th>
            <th>Category</th>
            <th>Sub-Category</th>
            <th>Versions</th>
            <th>Price</th>
            <th>Airs</th>
            <th>First Airing</th>
        </thead>
        <tbody>
            @foreach($products as $air)
                <tr>
                    <td>
                        <a href="{!! route("lst.product", array($air->product_id)) !!}">{!! $air->product_version_name !!}</a>
                    </td>
                    <td>{!! $air->channel_name !!}</td>
                    <td>{!! $air->category_name !!}</td>
                    <td>{!! $air->sub_category_name !!}</td>
                    <td>{!! $air->productVersion->product->versions->count() !!}</td>
                    <td><span title="{!! $air->product_price !!}">{!! $air->airtime->channel->currency !!}{{ number_format($air->product_price, 2) }}</span></td>
                    <td>{!! $air->airs !!}</td>
                    <td>{!! $air->productVersion->product->first_airdate->format('m/d/Y H:i a') !!}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif

@section('script')
    <script>
        $('#lstProducts').DataTable({
            pageLength: 50,
            "columns": [
                null,
                null,
                null,
                null,
                { "type": "title-numeric" },
                null,
                null,
                null
            ],
            "order": [[ 5, "desc" ]],
            "dom": 'Bfrtip',
            "buttons": [
                'copy', 'excel', 'pdf', 'csv'
            ]
        });
    </script>
    @parent
@endsection
