@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-gift"></i>
        {!! link_to_route('lst.index', 'Live Shopping Tracker') !!} <small>Dashboard</small>
    </h3>
@stop

@section('style')
<style>
.scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
}
.category-chart {
    width: 100%;
    height: 300px;
}
/* Added for detail icon */
td.details-control {
    background: url('/assets/img/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('/assets/img/details_close.png') no-repeat center center;
}

</style>
@stop

@section('content')
   @include('cxp.partials.lst.dashboard-filter')
    <div class="row">
        <h3 class="text-center">Category Airs By Channel</h3>
        <div id="categories-channels-chart" class="category-chart"></div>
    </div>
    <hr>
    <div class="row">
        <h3 class="text-center">Category Airs By Channel</h3>
        <div class="alert alert-info hidden">No airings found.</div>
        <table class="display table table-bordered table-striped" id="categories-channels-table">
            <thead>
                <th></th>
                <th>Channel</th>
                <th>
                    <a data-toggle="tooltip" title="Number of Products Airing">Frequency</a>
                </th>
            </thead>
        </table>
    </div>
@stop

@section('script')

<script type="text/javascript">
$(document).ready(function(){

    @include('cxp.partials.lst.common')

    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);

    //=============================================================================
    // Charts
    //=============================================================================
    var chart = AmCharts.makeChart("categories-channels-chart", {
        "type": "serial",
        "theme": "light",
        "legend": {
            "enabled": false,
            "horizontalGap": 10,
            "maxColumns": 1,
            "position": "right",
            "useGraphSettings": true,
            "markerSize": 10
        },
        "responsive": {
            "enabled": true
        },
        "dataProvider": {!! json_encode($topCategoriesByChannel) !!},
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.5,
            "gridAlpha": 0
        }],
        "graphs": [
            @foreach($topCategories->keys() as $category)
                {
                    "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                    "fillAlphas": 0.8,
                    "labelText": "[[airs]]",
                    "lineAlpha": 0.3,
                    "title": "{!!$category!!}",
                    "type": "column",
                    "color": "#000000",
                    "valueField": "{!!$category!!}"
                },
            @endforeach
        ],
        "rotate": true,
        "categoryField": "channelName",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "export": {
            "enabled": true
         }
    });
    //=============================================================================
    // Data Tables
    //=============================================================================
    /* Formatting function for row details - modify as you need */
    function format ( d ) {
        // `d` is the original data object for the row
        var format =
        // '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<table class="table table-bordered table-striped">'+
            '<thead>'+
                '<th>Category</th>'+
                '<th>Frequency</th>'+
            '</thead>'+
            '<tbody>';
        var topCategories = {!!json_encode($topCategories)!!};
        //get ?params from url
        var params = window.location.search;
        params += "&channels[]=" + d.channelId;
        for (var genre in d){
            // add a <tr> and link for each category
            if (topCategories[genre]){
                format += '<tr> <td><a href="/lst/category/' + topCategories[genre];
                format += params + '">'
                format += genre + '</a>' + '</td>';
                format += '<td>' + d[genre] + '</td> </tr>';
            }
        }
        format +=
            '</tbody>'+
        '</table>';
        return format;
    }

    var table = $('#categories-channels-table').DataTable( {
        data: {!! json_encode($topCategoriesByChannel) !!},
        searching: false,
        lengthChange: false,
        paging: false,
        info: false,
        columns: [
            {
                className:      'details-control',
                orderable:      false,
                data:           null,
                defaultContent: ''
            },
            { data: "channelName" },
            { data: "channelAirs" },
        ],
        "order": [[2, 'desc']]
    } );

    // Add event listener for opening and closing details
    $('#categories-channels-table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            //console.log(row.data());
            var childRowData = format(row.data());
            row.child(childRowData).show();
            tr.addClass('shown');
        }
    } );
    // End Datatables
});
</script>
@stop
