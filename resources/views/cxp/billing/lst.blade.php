@extends('cxp.layout')

@section('style')
    <style>
    #example-image {
            position: relative;
            margin: 2em 10px 4em;
            background: #fff;
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
    }
    .form-group {
        margin-left: auto;
        margin-right: auto;
    }
    .form-group.shrink {
        width: 320px;
    }
    .login-bottom-links {
        width: 320px;
        margin-left: auto;
        margin-right: auto;
        text-align: center;
    }
    .current {
        font-weight: bold;
        font-size: 2em;
    }
    #form-progress .not-current a {
        color: #999;
    }
    #form-progress .not-current a:hover {
        background-color: initial;
        cursor: inherit;
        border: 1px solid transparent;
        color: #999;
    }
    .month-input {
        padding-right: 0;
    }
    .margin-fix {
        margin: 15px;
    }

    .plan-card {
        border: 1px solid transparent;
        border-radius: 3px 3px 0 0;
        color: #999;
    }
    .plan-card:hover {
        cursor: pointer;
    }
    .plan-card {
        color: #555;
        background-color: #fff;
        border: 1px solid #ddd;
        height: 214px;
    }
    .plan-card.active {
        color: #555;
        background-color: #B6FFB5;
        border: 1px solid #FFF;
    }
    .plan-card.active hr {
        border-top: 1px solid #FFF;
    }
    .plan-card h4 {
        margin-top: 17px;
        text-align: center;
    }
    .plan-card p {
        display: block;
        text-align: center;
        font-size: 18px;
        margin-bottom: 17px;
    }
    .plan-card p small {
        font-size: 20px;
    }
    .plan-card.active p {
        font-weight: bold;
        font-size: 37px;
    }
    .plan-card.active p small {
        font-size: 20px;
    }
    .plan-card span {
        display: block;
        text-align: center;
        margin-bottom: 17px;
    }
    .zero-height {
        max-height: 0 !important;
        height: 0 !important;
        padding: 0 !important;
        margin: 0 !important;
    }
    #confirm li, #confirm span {
        color: #000;
    }
    #toc {
        text-align: center;
    }
    #toc .checkbox {
        display: inline-block;
    }
    label {
        color: #373e4a;
    }
    .modal-backdrop {
      z-index: -1;
    }
    .modal-body {
        max-height: 50vh;
        color: black;
        overflow: auto;
    }
    .loader,
    .loader:before,
    .loader:after {
      border-radius: 50%;
    }
    .loader:before,
    .loader:after {
      position: absolute;
      content: '';
    }
    .loader:before {
      width: 5.2em;
      height: 10.2em;
      /*background: #0dc5c1;*/
      background: #FFF;
      border-radius: 10.2em 0 0 10.2em;
      top: -0.1em;
      left: -0.1em;
      -webkit-transform-origin: 5.2em 5.1em;
      transform-origin: 5.2em 5.1em;
      -webkit-animation: load2 2s infinite ease 1.5s;
      animation: load2 2s infinite ease 1.5s;
    }
    .loader {
      font-size: 11px;
      text-indent: -99999em;
      margin: 55px auto;
      position: relative;
      width: 10em;
      height: 10em;
      box-shadow: inset 0 0 0 1em #0dc5c1;
      -webkit-transform: translateZ(0);
      -ms-transform: translateZ(0);
      transform: translateZ(0);
    }
    .loader:after {
      width: 5.2em;
      height: 10.2em;
      /*background: #0dc5c1;*/
      background: #FFF;
      border-radius: 0 10.2em 10.2em 0;
      top: -0.1em;
      left: 5.1em;
      -webkit-transform-origin: 0px 5.1em;
      transform-origin: 0px 5.1em;
      -webkit-animation: load2 2s infinite ease;
      animation: load2 2s infinite ease;
    }
    @-webkit-keyframes load2 {
      0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
    @keyframes load2 {
      0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
      }
      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    </style>
@stop
@section('header')
    <h3>
        <i class="fa fa-exclamation-triangle"></i>
        Subscribe to Live Shopping Tracker
    </h3>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
                @if (App::environment() != 'drtvlab')
                    <h2 class="text-center">Subscribe to Live Shopping Tracker. Gain Insight, Create Your Own Opportunities.</h2>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                                <img src="{!! asset('assets/img/lst.jpg') !!}" class="img-responsive" id="example-image" alt="Sample IMS Report">
                            </div>
                    </div>
                    <hr id="before-error">
            @endif

            <!-- Nav tabs -->
            <div class="row text-center">
              <ul class="nav nav-tabs" id="form-progress">
                <li class="current active" id="payment-tab-header"><a>Payment Details</a></li>
                <li class="not-current" id="confirm-tab-header"><a>Confirm Details</a></li>
              </ul>
            </div>

            <!-- Tab panes -->
            <div role="tabpanel" class="tab-pane fade " id="payment">
                <form action="" method="POST" id="payment-form" class="">
                    <span class="payment-errors"></span>

                    <div class=" row col-md-12 form-group">
                        <div class="input-group input-group-lg">
                            <div class="input-group-addon">
                                <i class="entypo-credit-card"></i>
                            </div>
                            <input class="form-control col-sm-11" type="text" size="20" maxlength="16" data-stripe="number" placeholder="Card Number">
                        </div>
                    </div>

                    <div class=" row col-md-7 form-group">
                        <div class="input-group input-group-lg">
                            <div class="input-group-addon">
                                <i class="entypo-chat"></i>
                            </div>
                            <input class="form-control col-sm-11" type="text" size="4" maxlength="4" data-stripe="cvc" placeholder="CVC">
                        </div>
                    </div>

                    <div class=" row col-md-2 month-input form-group">
                        <div class="input-group input-group-lg">
                            <div class="input-group-addon">
                                <i class="entypo-controller-volume"></i>
                            </div>
                            <input class="form-control col-sm-11" type="text" size="2" maxlength="2" data-stripe="exp-month" placeholder="MM">
                        </div>
                    </div>

                    <div class=" row col-md-3 form-group">
                        <div class="input-group input-group-lg">
                            <div class="input-group-addon">
                                <i class="entypo-dots-three-horizonal"></i>
                            </div>
                            <input class="form-control col-sm-11" type="text" size="4" maxlength="4" data-stripe="exp-year" placeholder="YYYY">
                        </div>
                    </div>

                    <div class="form-group row plans-row ">
                        <div class=" plan-card col-md-4 col-md-offset-2 active">
                            <h4 class="">Monthly</h4>
                            <hr>
                            <strike><p><small>$299/mo</small></p></strike>
                            <p class="">$249<small>/mo</small></p>
                        </div>
                        <div class=" plan-card col-md-4">
                            <h4 class="">Annually</h4>
                            <hr>
                            <strike><p class=""><small>$249/mo</small></p></strike>
                            <p class="">$199<small>/mo</small></p>
                            <span class="">$2,388<small>/yr</small></span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div id="toc" class="col-md-8 col-md-offset-2">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> By clicking here, I agree to the <a href="#" data-toggle="modal" data-target="#tocModal">Terms and Conditions</a>.
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="tocModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Terms and Conditions</h4>
                                </div>
                                <div class="modal-body">
                                    <h1>Terms of Use ("Terms")</h1>
                                    <p>Last updated: October 05, 2015</p>
                                    <p>Please read these Terms of Use ("Terms", "Terms of Use") carefully before using the http://liveshoppingtracker.com website (the "Service") operated by Media Analytics, LLC ("us", "we", or "our").</p>
                                    <p>Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who wish to access or use the Service.</p>
                                    <p><strong>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Service.</strong></p>
                                    <p><strong>Purchases</strong></p>
                                    <p>If you wish to purchase any product or service made available through the Service ("Purchase"), you may be asked to supply certain information relevant to your Purchase including, without limitation, your credit card number, the expiration date of your credit card, your billing address, and your shipping information.</p>
                                    <p>You represent and warrant that: (i) you have the legal right to use any credit card(s) or other payment method(s) in connection with any Purchase; and that (ii) the information you supply to us is true, correct and complete.</p>
                                    <p>The service may employ the use of third party services for the purpose of facilitating payment and the completion of Purchases. By submitting your information, you grant us the right to provide the information to these third parties subject to our Privacy Policy.</p>
                                    <p>We reserve the right to refuse or cancel your order at any time for reasons including but not limited to: product or service availability, errors in the description or price of the product or service, error in your order or other reasons.</p>
                                    <p>We reserve the right to refuse or cancel your order if fraud or an unauthorized or illegal transaction is suspected.</p>
                                    <p><strong>Availability, Errors and Inaccuracies</strong></p>
                                    <p>We are constantly updating product and service offerings on the Service. We may experience delays in updating information on the Service and in our advertising on other web sites. The information found on the Service may contain errors or inaccuracies and may not be complete or current. Products or services may be mispriced, described inaccurately, or unavailable on the Service and we cannot guarantee the accuracy or completeness of any information found on the Service.</p>

                                    <p>We therefore reserve the right to change or update information and to correct errors, inaccuracies, or omissions at any time without prior notice.</p>
                                    <p><strong>Promotions</strong></p>
                                    <p>Any promotions (collectively, "Promotions") made available through the Service may be governed by rules that are separate from these Terms & Conditions. If you participate in any Promotions, please review the applicable rules as well as our Privacy Policy. If the rules for a Promotion conflict with these Terms and Conditions, the Promotion rules will apply.</p>
                                    <p><strong>Accounts</strong></p>
                                    <p>When you create an account with us, you guarantee that you are above the age of 18, and that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on the Service.</p>
                                    <p>You are responsible for maintaining the confidentiality of your account and password, including but not limited to the restriction of access to your computer and/or account. You agree to accept responsibility for any and all activities or actions that occur under your account and/or password, whether your password is with our Service or a third-party service. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.</p>
                                    <p>You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trademark that is subject to any rights of another person or entity other than you, without appropriate authorization. You may not use as a username any name that is offensive, vulgar or obscene.</p>
                                    <p>We reserve the right to refuse service, terminate accounts, remove or edit content, or cancel orders in our sole discretion.</p>
                                    <p><strong>Intellectual Property</strong></p>
                                    <p>The Service and its original content, features and functionality are and will remain the exclusive property of Media Analytics, LLC and its licensors. The Service is protected by copyright, trademark, and other laws of both the United States and foreign countries. Our trademarks and trade dress may not be used in connection with any product or service without the prior written consent of Media Analytics, LLC.</p>
                                    <p><strong>Links To Other Web Sites</strong></p>
                                    <p>Our Service may contain links to third party web sites or services that are not owned or controlled by Media Analytics, LLC.</p>
                                    <p>Media Analytics, LLC has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites.</p>
                                    <p>You acknowledge and agree that Media Analytics, LLC shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party web sites or services.</p>
                                    <p>We strongly advise you to read the terms and conditions and privacy policies of any third party web sites or services that you visit.</p>
                                    <p><strong>Termination</strong></p>
                                    <p>We may terminate or suspend your account and bar access to the Service immediately, without prior notice or liability, under our sole discretion, for any reason whatsoever and without limitation, including but not limited to a breach of the Terms.</p>
                                    <p>If you wish to terminate your account, you may simply discontinue using the Service.</p>
                                    <p>All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>
                                    <p><strong>Indemnification</strong></p>
                                    <p>You agree to defend, indemnify and hold harmless Media Analytics, LLC and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Service, by you or any person using your account and password, or b) a breach of these Terms.</p>
                                    <p><strong>Limitation Of Liability</strong></p>
                                    <p>In no event shall Media Analytics, LLC, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.</p>
                                    <p><strong>Disclaimer</strong></p>
                                    <p>Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.</p>
                                    <p>Media Analytics, LLC its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements.</p>
                                    <p><strong>Exclusions</strong></p>
                                    <p>Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.</p>
                                    <p><strong>Governing Law</strong></p>
                                    <p>These Terms shall be governed and construed in accordance with the laws of Pennsylvania, United States, without regard to its conflict of law provisions.</p>
                                    <p>Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have had between us regarding the Service.</p>
                                    <p><strong>Changes</strong></p>
                                    <p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>
                                    <p>By continuing to access or use our Service after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Service.</p>
                                    <p><strong>Contact Us</strong></p>
                                    <p>If you have any questions about these Terms, <a href="//www.imsreport.com/contact">please contact us</a>.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=" form-group shrink payment-button-container">
                        <div class=" row">
                            <div class=" col-sm-12">
                                <button type="submit" class=" btn btn-block btn-orange btn-login" style="background:#ff9600;"> <i class="entypo-right-open-mini"></i> <strong>Confirm Payment</strong>
                                    </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div role="tabpanel" class="tab-pane fade " id="confirm">
                <div class="row alert alert-warning confirm-email margin-fix">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    Please confirm your account details and proceed to pay.
                </div>

                <div class="row form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <ul>
                            <li><strong>Plan: </strong><span id="plan-confirm"></span>
                            <li><strong>Credit Card - Last 4: </strong><span id="last-4-confirm"></span></li>
                            <li><strong>Credit Card - Expiration Month: </strong><span id="exp-month-confirm"></span></li>
                            <li><strong>Credit Card - Expiration Year: </strong><span id="exp-year-confirm"></span></li>
                        </ul>
                    </div>
                </div>


                <div class="row form-group shrink payment-button-container">
                    <div class="col-sm-12">
                        <button type="submit" id="final-confirm-battle" class="btn btn-block btn-orange btn-login" style="background:#ff9600;"> <i class="entypo-right-open-mini"></i> <strong>Submit Payment</strong>
                            </button>
                    </div>
                </div>
            </div>
            </form>

        </div>
    </div>
@stop

@section('script')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
$(function(){
    var userId = {!! $user->id !!};
    var monthlyPlanId = 'Live shopping Tracker Monthly';
    var yearlyPlanId = 'Live Shopping Tracker Annually';
    var plan = monthlyPlanId;

    var last4 = '';
    var expMonth = '';
    var expYear = '';
    var firstName = '';
    var lastName = '';
    var email = '';
    var companyName = '';
    var stripeResponse = '';

    $('#confirm').find('*').addClass('zero-height');
    $('#tocModal').appendTo("body");

    $('#payment button[type=submit]').attr('disabled', 'disabled');

    // switchToPaymentForm();
    function switchToPaymentForm() {
        $('#form-progress a').each(function() {
            $(this).parent()
                .removeClass('current')
                .removeClass('active')
                .removeClass('not-current')
                .addClass('not-current');
        });

        $('#payment-tab-header').removeClass('not-current').addClass('current').addClass('active');

        $('#account').toggleClass('in');
        setTimeout(function() {
            $('#account').toggleClass('active');

            setTimeout(function() {
                $('#payment').toggleClass('active').removeClass('zero-height');
                $('#payment .zero-height').each(function() {
                    $(this).removeClass('zero-height');
                });

                setTimeout(function() {
                    $('#payment').toggleClass('in');
                }, 100);
            }, 100)
        }, 500);
    }

    function switchToConfirm() {
        $('#payment').find('*').addClass('zero-height');
        $('#')
        $('#form-progress a').each(function() {
            $(this).parent()
                .removeClass('current')
                .removeClass('active')
                .removeClass('not-current')
                .addClass('not-current');
        });

        $('#confirm-tab-header').removeClass('not-current').addClass('current').addClass('active');

        $('#payment').toggleClass('in');
        setTimeout(function() {
            // $('#account').toggleClass('active');

            setTimeout(function() {
                $('#payment').toggleClass('active').removeClass('zero-height');
                $('#confirm')
                    .removeClass('zero-height')
                    .find('*').removeClass('zero-height');

                setTimeout(function() {
                    $('#confirm').toggleClass('in');
                    $('#email-confirm').text(email);
                    $('#first-name-confirm').text(firstName);
                    $('#last-name-confirm').text(lastName);
                    $('#company-name-confirm').text(companyName);
                    $('#plan-confirm').text(plan);
                    $('#last-4-confirm').text(last4);
                    $('#exp-month-confirm').text(expMonth);
                    $('#exp-year-confirm').text(expYear);
                }, 100);
            }, 100)
        }, 500);
    }

    $('.plan-card').click(function() {
        if ($(this).hasClass('active')) {
            return;
        }
        $('.plan-card').each(function() {
            $(this).toggleClass('active');
        });
        if (plan == monthlyPlanId) {
            plan = yearlyPlanId;
        } else {
            plan = monthlyPlanId;
        }
    });

    var stripePublic = '{!! getenv("STRIPE_PUBLIC") !!}';
    Stripe.setPublishableKey(stripePublic);

    $('.checkbox input').click(function() {
        $('#payment button[type=submit]').prop('disabled', function(i, v) {
            return !v;
        });
    });

    $('#confirm button[type=submit]').click(function() {
        $('#confirm').prepend('<div class="loader">Loading...</div>');
    });

    switchToPaymentForm();

    $('#payment-form').submit(function(event) {
        var $form = $(this);

        // Disable the submit button to prevent repeated clicks
        $form.find('button').prop('disabled', true);

        Stripe.card.createToken($form, function(status, response) {
            $('.remember-to-delete-these').each(function() {
                $(this).remove();
            });
            if (status == 402) {
                $('.confirm-email').after(
                '<div class="remember-to-delete-these alert alert-danger margin-fix">'+
                '<button type="button" class="close" data-dismiss="alert">×</button>'+
                response.error.message+
                '</div>'
                );

                $('.payment-button-container').find('button').removeAttr('disabled');
            } else {
                expMonth = response.card.exp_month;
                expYear = response.card.exp_year;
                last4 = response.card.last4;
                stripeResponse = response;
                switchToConfirm();
            return false;
            }
        });

        // Prevent the form from submitting with the default action
        return false;
    });

    $('#final-confirm-battle').click(function(e) {
        e.preventDefault();

        if (plan == monthlyPlanId) {
            var coupon = '50off';
        } else {
            var coupon = '600off';
        }

        $.ajax({
            type: "POST",
            url: "/billing/plans",
            data: JSON.stringify({
                stripe: stripeResponse,
                'user-id': userId,
                plan: plan,
                coupon: coupon,
                'sentry-plan-group-name': 'Live Shopping Tracker Subscription',
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                if (data.status == 400) {
                    $('.remember-to-delete-these').each(function() {
                        $(this).remove();
                    });
                    var $errorDiv = $('.payment-button-container');

                    $errorDiv.append(
                        '<div class="remember-to-delete-these alert alert-danger ">'+
                            '<button type="button" class="close" data-dismiss="alert">×</button>'+
                            data.message+
                        '</div>'
                    );

                    $errorDiv.find('button').removeAttr('disabled');
                    return;
                }

                window.location.href = '/user/login';
            },
            failure: function(errMsg) {
                console.log(errMsg);
            }
        });
    });

});
</script>
@stop
