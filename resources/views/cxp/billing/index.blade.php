@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-exclamation-triangle"></i>
        Manage Billing
    </h3>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
                @if ($user->subscriptions()->where('stripe_active', 1)->count() > 0)
                    <legend>Active Subscriptions</legend>
                    @foreach ($user->subscriptions()->where('stripe_active', 1)->get() as $subscription)
                    <fieldset id="subscription-{!!$subscription->id!!}">
                        <div class="row">
                            <div class="col-xs-12">
                            </div>
                            <div class="col-xs-4">
                                <label for="email_weekly" class="control-label">
                                    <strong>{!! $subscription->stripe_plan !!}</strong>
                                </label>
                            </div>
                            <div class="col-xs-4">
                                <label for="email_monthly" class="control-label">
                                    @if ($subscription->getTrialEndDate())
                                        14 Day Trial Ends on {!! $subscription->getTrialEndDate() !!}
                                    @endif
                                </label>
                            </div>
                            <div class="col-xs-4">
                                {!! Former::horizontal_open( route('billing.unsubscribe', $subscription->id), 'DELETE' ) !!}
                                    <button type="submit" class="btn btn-danger">Unsubscribe</button>
                                {!! Former::close() !!}
                            </div>
                        </div>
                    </fieldset>
                    @endforeach
                @endif

                @if ($user->subscriptionsGracePeriod()->count() > 0)
                    <legend>Grace Period Subscriptions</legend>
                    @foreach ($user->subscriptionsGracePeriod() as $subscription)
                    <fieldset id="subscription-{!!$subscription->id!!}">
                        <div class="row">
                            <div class="col-xs-12">
                            </div>
                            <div class="col-xs-4">
                                <label for="email_weekly" class="control-label">
                                    <strong>{!! $subscription->stripe_plan !!}</strong>
                                </label>
                            </div>
                            <div class="col-xs-4">
                                <label for="email_monthly" class="control-label">
                                    Ends on {!! $subscription->getSubscriptionEndDate() !!}
                                </label>
                            </div>
                            <div class="col-xs-4">
                                <a href="/user/billing/lst"><button type="submit" class="btn btn-success">Renew</button></a>
                            </div>
                        </div>
                    </fieldset>
                    @endforeach
                @endif

                @if ($user->subscriptionsGracePeriod()->count() == 0 && $user->subscriptions()->count() == 0)
                    <legend>You have no active Subscriptions.</legend>
                    <p><a href="/user/billing/lst"><button type="submit" class="btn btn-success">Subscribe to the Live Shopping Tracker</button></a></p>
                @endif
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        </div>
    </div>
@stop

@section('script')
<script>
$(function(){
    $('#alert_type').change(function(e){
        if($(this).val() == 'category') {
            $('#type_extra').prop('disabled', false);
            $('#alert_category').slideDown();
        } else {
            $('#type_extra').prop('disabled', true);
            $('#alert_category').slideUp();
        }
    });

    $('#alert_category').hide();
});
</script>
@stop
