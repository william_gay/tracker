@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-table"></i>
        Infomercial Grids &bull; {!! $language->name !!}
    </h3>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12 report-title">
            <h4 class="text-center">Infomercial Grids <small>All times are in {!! Carbon\Carbon::now()->tzName !!}</small></h4>
            <div class="date-selection text-center">
                <form class="form-inline">
                {!! Former::xlarge_text('report-date','')->class('span2 report-date form-control')->placeholder($grid_date->format("F j, Y"))->required() !!}
                <button class="select btn btn-blue btn-icon">Select Date<i class="fa fa-calendar"></i></button>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table cellpadding="0" cellspacing="0" border="0" id="network-grid" class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Network</th>
                    @foreach($times as $time)
                        <th>{!! $time !!}</th>
                    @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($gridSet as $grid)
                        <tr>
                        @foreach($grid as $air)
                            <td>{!! $air !!}</td>
                        @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('script')
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/380cb78f450/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script>
$(document).ready(function () {

    var table = $('#network-grid').DataTable({
        responsive: true,
        "pageLength": 50,
        "dom": 'T<"clear">lfrtip',
        "order": [[0, 'asc']],
        "columns": [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true }
        ]
    });
    new $.fn.dataTable.FixedHeader( table, {
        left:   true
    } );
    var tableTools = new $.fn.dataTable.TableTools( table, {
        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
        "buttons": [
            "copy",
            "csv",
            "xls",
            "pdf",
            { "type": "print", "buttonText": "Print me!" }
        ]
    } );

    $( tableTools.fnContainer() ).insertAfter('div.info');
});

var calcDataTableHeight = function() {
    return $(window).height() - 60;
};
</script>
@stop
