@extends('cxp.layout')

@section('header')
  <h3>
    <i class="fa fa-signal"></i>
    Short-Form Category Report
  </h3>
@stop

@section('content')
  {!! Breadcrumbs::render('report',$report) !!}

  <div class="row">
    <div class="col-xs-12">
      @include('cxp.categories.partials.filters')

      <h4 class="text-center">Airings Per Week</h4>
      @if(!$countPerCategory->isEmpty())
	<div id="short-airs" style="width:100%;height:600px;"></div>
      @else
	<div class="text-center alert">
	  No Results
	</div>
      @endif

      <h4 class="text-center">Cost Per Week</h4>
      @if(!$costPerCategory->isEmpty())
	<div id="short-cost" style="width:100%;height:600px;"></div>
      @else
	<div class="text-center alert">
	  No Results
	</div>
      @endif

    </div>
  </div>
@endsection

@section('script')
  <script>
  $(document).ready(function(){

    @include('cxp.partials.network-analyzer.common-scripts')
    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);

    function getCategoryGraphs (data, value = 'count') {
      return data.reduce(function (acc, row) {
	var categories = Object.keys(row).filter(function (key) {
	  return key != 'date'
	})

	var existing = acc.map(function (graph) {
	  return graph.title
	})

	var newCategories = categories.filter(function (category) {
	  return existing.indexOf(category) < 0
	})

	var newGraphs = newCategories.map(function (category){
	  return {
	    "valueAxis": "v1",
	    "balloonText": "[[title]]: [[value]]",
	    "lineThickness": 2,
	    "title": category,
	    "valueField": category
	  }
	})

	return acc.concat(newGraphs)
      }, [])
    }

    @if(!$countPerCategory->isEmpty())

    var data = {!! $countPerCategory->toJson() !!}
      var airs = AmCharts.makeChart("short-airs", {
	"type": "serial",
	"legend": {
	  "useGraphSettings": true
	},
	"theme": "none",
	"pathToImages": "{!!URL::to('/')!!}/assets/img/",
	"dataDateFormat": "YYYY-MM-DD",
	"valueAxes": [{
	  "id":"v1",
	  "axisAlpha": 0,
	  "position": "left"
	}],
	"graphs": getCategoryGraphs(data, 'count'),
	"chartScrollbar": {
	  "graph": "g1",
	  "scrollbarHeight": 30
	},
	"chartCursor": {
	  "cursorPosition": "mouse",
	  "pan": true,
	  "valueLineEnabled":true,
	  "valueLineBalloonEnabled":true
	},
	"categoryField": "date",
	"categoryAxis": {
	  "parseDates": true,
	  "dashLength": 1,
	  "minorGridEnabled": true,
	  "position": "top"
	},
	exportConfig:{
	  menuRight: '20px',
	  menuBottom: '50px',
	  menuItems: [{
	    icon: '{!!URL::to('/')!!}/assets/img/export.png',
	    format: 'png'
	  }]
	},
	"dataProvider": data
      }
      );
    @endif

    @if(!$costPerCategory->isEmpty())
    var data = {!! $costPerCategory->toJson() !!}
      var cost = AmCharts.makeChart("short-cost", {
	"type": "serial",
	"legend": {
	  "useGraphSettings": true
	},
	"theme": "none",
	"pathToImages": "{!!URL::to('/')!!}/assets/img/",
	"dataDateFormat": "YYYY-MM-DD",
	"valueAxes": [{
	  "id":"v1",
	  "axisAlpha": 0,
	  "position": "left"
	}],
	"graphs": getCategoryGraphs(data, 'cost'),
	"chartScrollbar": {
	  "graph": "g1",
	  "scrollbarHeight": 30
	},
	"chartCursor": {
	  "cursorPosition": "mouse",
	  "pan": true,
	  "valueLineEnabled":true,
	  "valueLineBalloonEnabled":true
	},
	"categoryField": "date",
	"categoryAxis": {
	  "parseDates": true,
	  "dashLength": 1,
	  "minorGridEnabled": true,
	  "position": "top"
	},
	exportConfig:{
	  menuRight: '20px',
	  menuBottom: '50px',
	  menuItems: [{
	    icon: '{!!URL::to('/')!!}/assets/img/export.png',
	    format: 'png'
	  }]
	},
	"dataProvider": data
      }
      );
    @endif

  });
  </script>
@endsection
