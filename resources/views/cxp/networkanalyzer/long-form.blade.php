@extends('cxp.layout')

@section('header')
  <h3>
    <i class="fa fa-tachometer"></i>
    {!! link_to_route('network-analyzer.home', 'Network Explorer') !!} <small>Airings</small>
  </h3>
@stop

@section('style')
  <style>
  .scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
  }
  </style>
@stop

@section('content')

  @include('cxp.partials.filters.networkanalyzer')
  <!-- Main Stats -->
  <!-- Bar graph  -->
  <div class="row">
    <div class="col-sm-12">
      <ul class="nav nav-tabs" role="tablist">
        <li><a href="{{route('network-analyzer.short-form')}}">Shortform</a></li>
        <li class="active"><a href="{{route('network-analyzer.long-form')}}">Longform</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="longform">
          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Network Airing Comparison
                  </div>
                </div>
                <div class="panel-body">
                  @if($longForms->isEmpty())
                    <p class="no-results">No results returned with the given filters.</p>
                  @else
                    <div id="longForm" style="width:100%; height:675px; font-size:11px; padding:15px;"></div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Network Airings Over Time
                  </div>
                </div>
                <div class="panel-body">
                  @if($longFormAiringsOverTime->isEmpty())
                    <p class="no-results">No results returned with the given filters.</p>
                  @else
                    <div id="longAirings" style="width:100%; height:400px; font-size:11px;"></div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    New Shows
                  </div>
                </div>
                <div class="panel-body">
                  @if($newLongFormProgramsBynetwork->isEmpty())
                    <p>No new shows detected.</p>
                  @else
                    <div id="newProgramsChartL" style="width:100%; height:583px; font-size:11px;"></div>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    New Shows
                  </div>
                </div>
                <div class="panel-body">
                  @if($newLongFormProgramsBynetwork->isEmpty())
                    <p>No new shows detected.</p>
                  @else
                    <table id="newLongShows" class="table table-hover table-bordered results">
                      <thead>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Airings</th>
                        @if($marketToQuery == 1)
                          <th>Media Spend</th>
                        @endif
                      </thead>
                      <tbody>
                        @foreach($newLongFormProgramsBynetwork as $longForm)
                          <tr>
                            <td>{!! link_to_route('programs.schedule', $longForm->program_title, ['program' => $longForm->program_id]) !!}</td>
                            <td>{!! link_to_route('network-anaylizer.categories.show', $longForm->category_name,  ['id' => $longForm->category_id]) !!}</td>
                            <td>{!! $longForm->air_count !!}</td>
                            @if($marketToQuery == 1)
                              <td>
                                <span data-order="{!! $longForm->air_cost !!}">
                                  ${!! number_format($longForm->air_cost) !!}
                                </span>
                              </td>
                            @endif
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Top Ranking Networks
                  </div>
                </div>
                <div class="panel-body">
                  @if($longForms->isEmpty())
                    <p class="no-results">No results returned with the given filters.</p>
                  @else
                    <table id="longChannels" class="table table-hover table-bordered results">
                      <thead>
                        <th>Name</th>
                        <th>Airings</th>
                        @if($marketToQuery == 1)
                          <th>Media Spend</th>
                        @endif
                      </thead>
                      <tbody>
                        @foreach($longForms as $longForm)
                          <tr>
                            <td>{!! link_to_route('network-analyzer.show', $longForm->channel_name, ['id' => $longForm->channel_id]) !!}</td>
                            <td>{!! $longForm->air_count !!}</td>
                            @if($marketToQuery == 1)
                              <td>
                                <span data-order="{!! $longForm->air_cost !!}">
                                  ${!! number_format($longForm->air_cost) !!}
                                </span>
                              </td>
                            @endif
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Top Ranking Shows
                  </div>
                </div>
                <div class="panel-body">
                  @if($longFormProgramsByAirings->isEmpty())
                    <p class="no-results">No results returned with the given filters.</p>
                  @else
                    <table id="longPrograms" class="table table-hover table-bordered results">
                      <thead>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Versions</th>
                        <th>Channels</th>
                        <th>Airings</th>
                        @if($marketToQuery == 1)
                          <th>Media Spend</th>
                        @endif
                      </thead>
                      <tbody>
                        @foreach($longFormProgramsByAirings as $longForm)
                          <tr>
                            <td>{!! link_to_route('programs.schedule', $longForm->program_title, ['program' => $longForm->program_id]) !!}</td>
                            <td>{!! link_to_route('network-anaylizer.categories.show', $longForm->category_name,  ['id' => $longForm->category_id]) !!}</td>
                            <td>{!! $longForm->program_versions ?? 'N/A' !!}</td>
                            <td>{!! $longForm->channel_count ?? 'N/A' !!}</td>
                            <td>{!! $longForm->air_count !!}</td>
                            @if($marketToQuery == 1)
                              <td>
                                <span data-order="{!! $longForm->air_cost !!}">
                                  ${!! number_format($longForm->air_cost) !!}
                                </span>
                              </td>
                            @endif
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop


@section('script')
  <script type="text/javascript">
  $(document).ready(function(){

    @include('cxp.partials.network-analyzer.common-scripts')

    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);
    $('#subcategory_picker').multiselect(subcategoryConfigurationSet);

    //=============================================================================
    // Data Tables
    //=============================================================================
    $('#longChannels').dataTable({
      "order": [[ 1, "desc" ]]
    });

    $('#longPrograms').dataTable({
      "order": [[ 2, "desc" ]]
    });

    $('#newLongShows').dataTable({
      "order": [[ 2, "desc" ]]
    });

    //=============================================================================
    //=============================================================================
    // LONGFORM CHARTS
    //=============================================================================
    //=============================================================================

    //=============================================================================
    // Longform Network Airing Comparison Chart
    //=============================================================================
    @if(! $longForms->isEmpty())
    var longFormChart = AmCharts.makeChart("longForm", {
      "type": "serial",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "categoryField": "channel_name",
      "rotate": true,
      "placeholder": "Select a Channel",
      "allowClear": true,
      "startDuration": 1,
      "categoryAxis": {
        "gridPosition": "start"
      },
      "chartCursor": {},
      "chartScrollbar": {},
      "trendLines": [],
      "graphs": [
        {
          "balloonText": "[[title]] of [[category]]:[[value]]",
          "fillAlphas": 1,
          "id": "AmGraph-1",
          "title": "Show Airings",
          "type": "column",
          "valueField": "air_count"
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "title": "Frequency of Show Airings by Network"
        }
      ],
      "allLabels": [],
      "balloon": {},
      "legend": {},
      "dataProvider": {!! $longForms->toJson() !!},
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
          textAlign: 'center',
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          iconTitle: 'Save chart as an image',
        }]
      }
    });
    @endif

    //=============================================================================
    // Longform Total Airings Line Graph
    //=============================================================================
    @if(! $longFormAiringsOverTime->isEmpty())
    var chart = AmCharts.makeChart("longAirings", {
      "type": "serial",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "dataProvider": {!! $longFormAiringsOverTime->toJson() !!},
      "valueAxes": [{
        "logarithmic": true,
        "dashLength": 1,
        "guides": [{
          "dashLength": 6,
          "inside": true,
          "label": "average",
          "lineAlpha": 1,
          "value": 90.4,
          "guides": [{
            "fillAlphas": 1,
            "fillColor": "#000000",
            "inside": true,
            "lineAlpha": 1,
            "toValue": 20,
            "value": 10
          }]
        }],
        "position": "left"
      }],
      "graphs": [{
        "bullet": "round",
        "id": "g1",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 7,
        "lineThickness": 2,
        "title": "Price",
        "type": "smoothedLine",
        "useLineColorForBulletBorder": true,
        "valueField": "air_count",
        "fillAlphas": .15
      }],
      "chartScrollbar": {},
      "chartCursor": {
        "cursorPosition": "mouse"
      },
      "dataDateFormat": "YYYY-MM-DD",
      "categoryField": "air_date",
      "categoryAxis": {
        "parseDates": true
      },
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
          textAlign: 'center',
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          iconTitle: 'Save chart as an image',
        }]
      }
    });
    @endif

    //=============================================================================
    // New Longform Programs Chart
    //=============================================================================
    @if(! $newLongFormProgramsBynetwork->isEmpty())
    var newProgramsChartL = AmCharts.makeChart("newProgramsChartL", {
      "type": "serial",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "categoryField": "program_title",
      "rotate": true,
      "startDuration": 1,
      "networkAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
      },
      "chartCursor": {},
      "chartScrollbar": {},
      "trendLines": [],
      "graphs": [
        {
          "balloonText": "[[value]]",
          "fillAlphas": 1,
          "id": "AmGraph-1",
          "title": "Airings",
          "type": "column",
          "valueField": "air_count"
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "title": "Frequency"
        }
      ],
      "allLabels": [],
      "balloon": {},
      "legend": {},
      "dataProvider": {!! $newLongFormProgramsBynetwork->toJson() !!},
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
          textAlign: 'center',
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          iconTitle: 'Save chart as an image',
        }]
      }
    });
    @endif
  });
  </script>
@stop
