@extends('cxp.layout')

@section('header')
  <h3>
    <i class="fa fa-sitemap"></i>
    {!! link_to_route('network-analyzer.categories', 'Network Explorer') !!} <small>Categories</small>
  </h3>
@stop

@section('style')
  <style>
  .scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
  }
  </style>
@stop

@section('content')

  <h6>{!! subscription_welcome() !!}</h6>

  @include('cxp.partials.filters.networkanalyzer', ['showLengthSelect' => true])

  <div class="row">
    <div class="col-sm-12">
      <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="{{route('network-analyzer.categories.short-form')}}">Shortform</a></li>
        @unless (Request::get('market_id') == 4)
          <li><a href="{{route('network-analyzer.categories.long-form')}}">Longform</a></li>
        @endunless
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="shortform">
          <div class="panel panel-default panel-shadow">
            <div class="row">
              <div class="col-sm-6">
                <div class="panel panel-default panel-shadow">
                  <div class="panel-heading">
                    <div class="panel-title">
                      Airings by Category
                    </div>
                  </div>
                  <div class="panel-body">
                    @if($shortForms->isEmpty())
                      <p class="no-results">No results returned with the given filters.</p>
                    @else
                      <div id="airingsDivS" style="width:100%; height:600px; font-size:11px;"></div>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="panel panel-default panel-shadow">
                  <div class="panel-heading">
                    <div class="panel-title">
                      Frequency
                    </div>
                  </div>
                  <div class="panel-body">
                    @if($shortForms->isEmpty())
                      <p class="no-results">No results returned with the given filters.</p>
                    @else
                      <div id="spendingDivS" style="width:100%; height:675px; font-size:11px;"></div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="panel panel-default panel-shadow">
                  <div class="panel-heading">
                    <div class="panel-title">
                      Total Airings Over Time
                    </div>
                  </div>
                  <div class="panel-body">
                    @if($shortFormAiringsOverTime->isEmpty())
                      <p class="no-results">No results returned with the given filters.</p>
                    @else
                      <div id="shortForm" style="width:100%; height:400px; font-size:11px;"></div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="panel panel-default panel-shadow">
                  <div class="panel-heading">
                    <div class="panel-title">
                      Category Overview
                    </div>
                  </div>
                  <div class="panel-body">
                    @if($shortForms->isEmpty())
                      <p class="no-results">No results returned with the given filters.</p>
                    @else
                      <table id="shortCat" class="table table-hover table-bordered results">
                        <thead>
                          <th>Category</th>
                          <th>Airings</th>
                          @if($marketToQuery == 1)
                            <th>Media Spend</th>
                          @endif
                        </thead>
                        <tbody>
                          @foreach($shortForms as $shortForm)
                            <tr>
                              <td>{!! link_to_route('network-anaylizer.categories.show', $shortForm->category_name,  ['id' => $shortForm->category_id]) !!}</td>
                              <td>{!! $shortForm->air_count !!}</td>
                              @if($marketToQuery == 1)
                                <td data-order="{!! $shortForm->air_cost !!}">
                                  ${!! number_format($shortForm->air_cost) !!}
                                </td>
                              @endif
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="panel panel-default panel-shadow">
                  <div class="panel-heading">
                    <div class="panel-title">
                      Top Ranking Networks
                    </div>
                  </div>
                  <div class="panel-body">
                    @if($shortFormNetworksByCategory->isEmpty())
                      <p class="no-results">No results returned with the given filters.</p>
                    @else
                      <table id="shortNet" class="table table-hover table-bordered results">
                        <thead>
                          <th>Name</th>
                          <th>Airings</th>
                          @if($marketToQuery == 1)
                            <th>Media Spend</th>
                          @endif
                        </thead>
                        <tbody>
                          @foreach($shortFormNetworksByCategory as $shortForm)
                            <tr>
                              <td>{!! link_to_route('network-analyzer.show', $shortForm->channel_name, ['id' => $shortForm->channel_id]) !!}</td>
                              <td>{!! $shortForm->air_count !!}</td>
                              @if($marketToQuery == 1)
                                <td data-order="{!! $shortForm->air_cost !!}">
                                  ${!! number_format($shortForm->air_cost) !!}
                                </td>
                              @endif
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="panel panel-default panel-shadow">
                  <div class="panel-heading">
                    <div class="panel-title">
                      New Spots
                    </div>
                  </div>
                  <div class="panel-body">
                    @if($newShortFormSpotsByNetwork->isEmpty())
                      <p>No new spots detected.</p>
                    @else
                      <div id="newSpotsChartS" style="width:100%; height:583px; font-size:11px;"></div>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="panel panel-default panel-shadow">
                  <div class="panel-heading">
                    <div class="panel-title">
                      New Spots
                    </div>
                  </div>
                  <div class="panel-body">
                    @if($newShortFormSpotsByNetwork->isEmpty())
                      <p>No new spots detected.</p>
                    @else
                      <table id="newSmallSpots" class="table table-hover table-bordered results">
                        <thead>
                          <th>Name</th>
                          <th>TRT</th>
                          <th>Category</th>
                          <th>Airings</th>
                          @if($marketToQuery == 1)
                            <th>Media Spend</th>
                          @endif
                        </thead>
                        <tbody>
                          @foreach($newShortFormSpotsByNetwork as $shortForm)
                            <tr>
                              <td>{!! link_to_route('spots.schedule', $shortForm->spot_title, ['spot' => $shortForm->spot_id]) !!}</td>
                              <td data-order="{{ $shortForm->length }}">:{{ $shortForm->length }}</td>
                              <td>{!! link_to_route('network-anaylizer.categories.show', $shortForm->category_name,  ['id' => $shortForm->category_id]) !!}</td>
                              <td>{!! $shortForm->air_count !!}</td>
                              @if($marketToQuery == 1)
                                <td data-order="{!! $shortForm->air_cost !!}">
                                  ${!! number_format($shortForm->air_cost) !!}
                                </td>
                              @endif
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@stop

@section('script')
  <script>
  $(document).ready(function(){

    @include('cxp.partials.network-analyzer.common-scripts')

    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);
    $('#subcategory_picker').multiselect(subcategoryConfigurationSet);
    $('#spot_length_picker').multiselect(spotLengthConfigurationSet);

    //=============================================================================
    // Data Tables
    //=============================================================================
    $('#shortNet').DataTable({
      "order": [[ 1, "desc" ]]
    });
    $('#shortCat').DataTable({
      "order": [[ 1, "desc" ]]
    });
    $('#newSmallSpots').DataTable({
      "order": [[ 1, "desc" ]]
    });

    //=============================================================================
    //=============================================================================
    // SHORTFORM CHARTS
    //=============================================================================
    //=============================================================================

    @if(! $shortForms->isEmpty())
    //=============================================================================
    // Shortform Airings by Category Pie Graph
    //=============================================================================
    var airingsChartS = AmCharts.makeChart("airingsDivS", {
      "type": "pie",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      "labelRadius": 5,
      "minRadius": 120,
      "outlineThickness": 10,
      "titleField": "category_name",
      "valueField": "air_count",
      "theme": "default",
      "balloon": {},
      "titles": [],
      "dataProvider": {!! $shortForms->toJson() !!},
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true
      }
    });
    @endif

    @if(! $shortForms->isEmpty())
    //=============================================================================
    // Shortform Media Spend by Category Bar Graph
    //=============================================================================
    var spendingChartS = AmCharts.makeChart("spendingDivS", {
      "type": "serial",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "categoryField": "category_name",
      "rotate": true,
      "startDuration": 1,
      "categoryAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
      },
      "chartCursor": {},
      "chartScrollbar": {},
      "trendLines": [],
      "graphs": [
        {
          "balloonText": "[[value]]",
          "fillAlphas": 1,
          "id": "AmGraph-1",
          "title": "Frequency",
          "type": "column",
          "valueField": "air_count"
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "title": "Frequency"
        }
      ],
      "allLabels": [],
      "balloon": {},
      "legend": {},
      "dataProvider": {!! $shortForms->toJson() !!},
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
          textAlign: 'center',
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          iconTitle: 'Save chart as an image',
        }]
      }
    });
    @endif

    @if(! $shortFormAiringsOverTime->isEmpty())
    //=============================================================================
    // Shortform Total Airings Line Graph
    //=============================================================================
    var airingsLineChartS = AmCharts.makeChart("shortForm", {
      "type": "serial",
      // "theme": "light",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "dataProvider": {!! $shortFormAiringsOverTime->toJson() !!},
      "valueAxes": [{
        "logarithmic": true,
        "dashLength": 1,
        "position": "left"
      }],
      "graphs": [{
        "bullet": "round",
        "id": "g1",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 7,
        "lineThickness": 2,
        "title": "Price",
        "type": "smoothedLine",
        "useLineColorForBulletBorder": true,
        "valueField": "air_count",
        "fillAlphas": .15
      }],
      "chartScrollbar": {},
      "chartCursor": {
        "cursorPosition": "mouse"
      },
      "dataDateFormat": "YYYY-MM-DD",
      "categoryField": "air_date",
      "categoryAxis": {
        "parseDates": true
      },
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
          textAlign: 'center',
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          iconTitle: 'Save chart as an image',
        }]
      }
    });
    @endif

    @if(! $newShortFormSpotsByNetwork->isEmpty())
    //=============================================================================
    // New Shortform Spots Chart
    //=============================================================================
    var newSpotsChartS = AmCharts.makeChart("newSpotsChartS", {
      "type": "serial",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "categoryField": "spot_title",
      "rotate": true,
      "startDuration": 1,
      "networkAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
      },
      "chartCursor": {},
      "chartScrollbar": {},
      "trendLines": [],
      "graphs": [
        {
          "balloonText": "[[value]]",
          "fillAlphas": 1,
          "id": "AmGraph-1",
          "title": "Airings",
          "type": "column",
          "valueField": "air_count"
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "title": "Frequency"
        }
      ],
      "allLabels": [],
      "balloon": {},
      "legend": {},
      "dataProvider": {!! $newShortFormSpotsByNetwork->toJson() !!},
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
          textAlign: 'center',
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          iconTitle: 'Save chart as an image',
        }]
      }
    });
    @endif
  });
  </script>
@stop
