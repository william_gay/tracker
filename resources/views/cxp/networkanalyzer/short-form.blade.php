@extends('cxp.layout')

@section('header')
  <h3>
    <i class="fa fa-tachometer"></i>
    {!! link_to_route('network-analyzer.home', 'Network Explorer') !!} <small>Airings</small>
  </h3>
@stop

@section('style')
  <style>
  .scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
  }
  </style>
@stop

@section('content')

  @include('cxp.partials.filters.networkanalyzer', ['showLengthSelect' => true])

  <!-- Main Stats -->
  <!-- Bar graph  -->
  <div class="row">
    <div class="col-sm-12">
      <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="{{route('network-analyzer.short-form')}}">Shortform</a></li>
        @unless (Request::get('market_id') == 4)
          <li><a href="{{route('network-analyzer.long-form')}}">Longform</a></li>
        @endunless
      </ul>
      <div class="tab-content">
        <div class="tab-pane active"  id="shortform">
          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Network Airing Comparison
                  </div>
                </div>
                <div class="panel-body">
                  @if($shortForms->isEmpty())
                    <p class="no-results">No results returned with the given filters.</p>
                  @else
                    <div id="shortForm" style="width:100%; height:675px; font-size:11px;"></div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Network Airings Over Time
                  </div>
                </div>
                <div class="panel-body">
                  @if($ShortFormAiringsOverTime->isEmpty())
                    <p class="no-results">No results returned with the given filters.</p>
                  @else
                    <div id="shortAirings" style="width:100%; height:400px; font-size:11px;"></div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    New Spots
                  </div>
                </div>
                <div class="panel-body">
                  @if($newShortFormSpotsByNetwork->isEmpty())
                    <p>No new spots detected.</p>
                  @else
                    <div id="newSpotsChartS" style="width:100%; height:583px; font-size:11px;"></div>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    New Spots
                  </div>
                </div>
                <div class="panel-body">
                  @if($newShortFormSpotsByNetwork->isEmpty())
                    <p>No new spots detected.</p>
                  @else
                    <table id="newSmallSpots" class="table table-hover table-bordered results">
                      <thead>
                        <th>Name</th>
                        <th>TRT</th>
                        <th>Category</th>
                        <th>Airings</th>
                        @if($marketToQuery == 1)
                          <th>Media Spend</th>
                        @endif
                      </thead>
                      <tbody>
                        @foreach($newShortFormSpotsByNetwork as $shortForm)
                          <tr>
                            <td>{!! link_to_route('spots.schedule', $shortForm->spot_title, ['program' => $shortForm->spot_id]) !!}</td>
                            <td><span data-order="{{ $shortForm->length }}">:{{ $shortForm->length }}</span></td>
                            <td>{!! link_to_route('network-anaylizer.categories.show', $shortForm->category_name,  ['id' => $shortForm->category_id]) !!}</td>
                            <td>{!! $shortForm->air_count !!}</td>
                            @if($marketToQuery == 1)
                              <td>
                                <span data-order="{!! $shortForm->air_cost !!}">
                                  ${!! number_format($shortForm->air_cost) !!}
                                </span>
                              </td>
                            @endif
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Top Ranking Networks
                  </div>
                </div>
                <div class="panel-body">
                  @if($shortForms->isEmpty())
                    <p class="no-results">No results returned with the given filters.</p>
                  @else
                    <table id="shortChannels" class="table table-hover table-bordered results">
                      <thead>
                        <th>Name</th>
                        <th>Airings</th>
                        @if($marketToQuery == 1)
                          <th>Media Spend</th>
                        @endif
                      </thead>
                      <tbody>
                        @foreach($shortForms as $shortForm)
                          <tr>
                            <td>{!! link_to_route('network-analyzer.show', $shortForm->channel_name,  ['id' => $shortForm->channel_id]) !!}</td>
                            <td>{!! $shortForm->air_count !!}</td>
                            @if($marketToQuery == 1)
                              <td>
                                <span data-order="{!! $shortForm->air_cost !!}">
                                  ${!! number_format($shortForm->air_cost) !!}
                                </span>
                              </td>
                            @endif
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Top Ranking Spots
                  </div>
                </div>
                <div class="panel-body">
                  @if($shortFormProgramsByAirings->isEmpty())
                    <p class="no-results">No results returned with the given filters.</p>
                  @else
                    <table id="shortSpots" class="table table-hover table-bordered results">
                      <thead>
                        <th>Name</th>
                        <th>TRT</th>
                        <th>Category</th>
                        <th>Airings</th>
                        @if($marketToQuery == 1)
                          <th>Media Spend</th>
                        @endif
                      </thead>
                      <tbody>
                        @foreach($shortFormProgramsByAirings as $shortForm)
                          <tr>
                            <td>
                              {!! link_to_route('spots.schedule', $shortForm->spot_title, ['program' => $shortForm->spot_id]) !!}
                            </td>
                            <td><span data-order="{{ $shortForm->length }}">:{{ $shortForm->length }}</span></td>
                            <td>{!! link_to_route('network-anaylizer.categories.show', $shortForm->category_name,  ['id' => $shortForm->category_id]) !!}</td>
                            <td>{!! $shortForm->air_count !!}</td>
                            @if($marketToQuery == 1)
                              <td>
                                <span data-order="{!! $shortForm->air_cost !!}">
                                  ${!! number_format($shortForm->air_cost) !!}
                                </span>
                              </td>
                            @endif
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop


@section('script')
  <script type="text/javascript">
  $(document).ready(function(){

    @include('cxp.partials.network-analyzer.common-scripts')

    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);
    $('#subcategory_picker').multiselect(subcategoryConfigurationSet);
    $('#spot_length_picker').multiselect(spotLengthConfigurationSet);

    $('#shortChannels').DataTable({
      "order": [[ 1, "desc" ]]
    });

    $('#shortSpots').DataTable({
      "order": [[ 2, "desc" ]]
    });

    $('#newSmallSpots').DataTable({
      "order": [[ 2, "desc" ]]
    });

    //=============================================================================
    //=============================================================================
    // SHORTFORM CHARTS
    //=============================================================================
    //=============================================================================

    //=============================================================================
    // Shortform Network Airing Comparison Chart
    //=============================================================================
    @if(! $shortForms->isEmpty())
    var shortFormChart = AmCharts.makeChart("shortForm", {
      "type": "serial",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "categoryField": "channel_name",
      "rotate": true,
      "startDuration": 1,
      "categoryAxis": {
        "gridPosition": "start"
      },
      "chartCursor": {},
      "chartScrollbar": {},
      "trendLines": [],
      "graphs": [
        {
          "balloonText": "[[title]] of [[category]]:[[value]]",
          "fillAlphas": 1,
          "id": "AmGraph-1",
          "title": "Spot Airings",
          "type": "column",
          "valueField": "air_count"
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "title": "Frequency of Spot Airings by Network"
        }
      ],
      "allLabels": [],
      "balloon": {},
      "legend": {},
      "dataProvider": {!! $shortForms->toJson() !!},
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
          textAlign: 'center',
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          iconTitle: 'Save chart as an image',
        }]
      }
    });
    @endif

    //=============================================================================
    // Shortform Total Airings Line Graph
    //=============================================================================
    @if(! $ShortFormAiringsOverTime->isEmpty())
    var airingsChartS = AmCharts.makeChart("shortAirings", {
      "type": "serial",
      // "theme": "light",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "dataProvider": {!! $ShortFormAiringsOverTime->toJson() !!},
      "valueAxes": [{
        "logarithmic": true,
        "dashLength": 1,
        "guides": [{
          "dashLength": 6,
          "inside": true,
          "label": "average",
          "lineAlpha": 1,
          "value": 90.4
        }],
        "position": "left"
      }],
      "graphs": [{
        "bullet": "round",
        "id": "g1",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 7,
        "lineThickness": 2,
        "title": "Price",
        "type": "smoothedLine",
        "useLineColorForBulletBorder": true,
        "valueField": "air_count",
        "fillAlphas": .15
      }],
      "chartScrollbar": {},
      "chartCursor": {
        "cursorPosition": "mouse"
      },
      "dataDateFormat": "YYYY-MM-DD",
      "categoryField": "air_date",
      "categoryAxis": {
        "parseDates": true
      },
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
          textAlign: 'center',
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          iconTitle: 'Save chart as an image',
        }]
      }
    });
    @endif

    //=============================================================================
    // New Shortform Spots Chart
    //=============================================================================
    @if(! $newShortFormSpotsByNetwork->isEmpty())
    var newSpotsChartS = AmCharts.makeChart("newSpotsChartS", {
      "type": "serial",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "categoryField": "spot_title",
      "rotate": true,
      "startDuration": 1,
      "networkAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
      },
      "chartCursor": {},
      "chartScrollbar": {},
      "trendLines": [],
      "graphs": [
        {
          "balloonText": "[[value]]",
          "fillAlphas": 1,
          "id": "AmGraph-1",
          "title": "Frequency",
          "type": "column",
          "valueField": "air_count"
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "title": "Frequency"
        }
      ],
      "allLabels": [],
      "balloon": {},
      "legend": {},
      "dataProvider": {!! $newShortFormSpotsByNetwork->toJson() !!},
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
          textAlign: 'center',
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          iconTitle: 'Save chart as an image',
        }]
      }
    });
    @endif
  });
  </script>
@stop
