@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-shopping-cart"></i>
        {!! link_to_route('network-analyzer.home', 'Network Explorer') !!} <small>Network Explorer &bull; {!! $network->name !!}</small>
     </h3>
@stop

@section('style')
<style>
.scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
}
</style>
@stop

@section('content')

    @include('cxp.partials.filters.networkanalyzer-show')

<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a id="shortFormTab" href="#shortform" role="tab" data-toggle="tab">Shortform</a></li>
        @unless ($network->language_id == 4)
            <li><a id="longFormTab" href="#longform" role="tab" data-toggle="tab">Longform</a></li>
        @endunless
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="longform">
            @unless ($network->language_id == 4)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default panel-shadow">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    Airings Over Time
                                </div>
                            </div>
                            <div class="panel-body">
                            @if($longFormAiringsOverTime->isEmpty())
                                <p class="no-results">No results returned with the given filters.</p>
                            @else
                                <div id="longAirings" style="width:100%; height:400px; font-size:11px;"></div>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default panel-shadow">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    Top Ranking Categories
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($longFormCategoriesByNetwork->isEmpty())
                                    <p class="no-results">No results returned with the given filters.</p>
                                @else
                                    <table id="longCat" class="table table-hover table-bordered results">
                                        <thead>
                                            <th>Name</th>
                                            <th>Airings</th>
                                            @if($network->language_id == 1)
                                                <th>Media Spend</th>
                                            @endif
                                        </thead>
                                        <tbody>
                                        @foreach($longFormCategoriesByNetwork as $longForm)
                                            <tr>
                                                <td>{!! link_to_route('network-anaylizer.categories.show', $longForm->category_name,  ['id' => $longForm->category_id, 'channels[]' => $network->id])!!}</td>
                                                <td>{!! $longForm->air_count !!}</td>
                                                @if($network->language_id == 1)
                                                    <td data-order="{!! $longForm->air_cost !!}">
                                                        ${!! number_format($longForm->air_cost) !!}
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default panel-shadow">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    Top Ranking Programs
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($longFormProgramsBynetwork->isEmpty())
                                    <p class="no-results">No results returned with the given filters.</p>
                                @else
                                    <table id="longPrograms" class="table table-hover table-bordered results">
                                        <thead>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Marketing Company</th>
                                            <th>Airings</th>
                                            @if($network->language_id == 1)
                                                <th>Media Spend</th>
                                            @endif
                                        </thead>
                                        <tbody>
                                        @foreach($longFormProgramsBynetwork as $longForm)
                                            <tr>
                                                <td>{!! link_to_route('programs.schedule', $longForm->program_title, ['program' => $longForm->program_id]) !!}</td>
                                                <td>{!! link_to_route('network-anaylizer.categories.show', $longForm->category_name,  ['id' => $longForm->category_id]) !!}</td>
                                                <td>
                                                    {!! $longForm->programVersion->marketingCompany->name ?? '' !!}
                                                </td>
                                                <td>{!! $longForm->air_count !!}</td>
                                                @if($network->language_id == 1)
                                                    <td data-order="{!! $longForm->air_cost !!}">
                                                        ${!! number_format($longForm->air_cost) !!}
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel panel-default panel-shadow">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    Categories
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($longFormCategoriesByNetwork->isEmpty())
                                    <p class="no-results">No results returned with the given filters.</p>
                                @else
                                    <div id="categoriesChartL" style="width:100%; height:583px; font-size:11px;"></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default panel-shadow">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    Programs
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($longFormProgramsBynetwork->isEmpty())
                                    <p class="no-results">No results returned with the given filters.</p>
                                @else
                                    <div id="programsChartL" style="width:100%; height:583px; font-size:11px;"></div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="panel panel-default panel-shadow">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    New Shows
                                </div>
                            </div>
                            <div class="panel-body">
                                @if($newLongFormProgramsBynetwork->isEmpty())
                                        <p>No new shows detected.</p>
                                @else
                                    <div id="newProgramsChartL" style="width:100%; height:583px; font-size:11px;"></div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default panel-shadow">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    New Shows
                                </div>
                            </div>
                            <div class="panel-body">
                            @if($newLongFormProgramsBynetwork->isEmpty())
                                    <p>No new shows detected.</p>
                            @else
                                <table id="newLongPrograms" class="table table-hover table-bordered results">
                                    <thead>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Airings</th>
                                        @if($network->language_id == 1)
                                            <th>Media Spend</th>
                                        @endif
                                    </thead>
                                    <tbody>
                                    @foreach($newLongFormProgramsBynetwork as $longForm)
                                        <tr>
                                            <td>{!! link_to_route('programs.schedule', $longForm->program_title, ['program' => $longForm->program_id]) !!}</td>
                                            <td>{!! link_to_route('network-anaylizer.categories.show', $longForm->category_name,  ['id' => $longForm->category_id]) !!}</td>
                                            <td>{!! $longForm->air_count !!}</td>
                                            @if($network->language_id == 1)
                                                <td data-order="{!! $longForm->air_cost !!}">
                                                    ${!! number_format($longForm->air_cost) !!}
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane active" id="shortform">
                <div class="panel panel-default panel-shadow">
                @endif
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Airings Over Time
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @if($shortFormAiringsOverTime->isEmpty())
                                        <p class="no-results">No results returned with the given filters.</p>
                                    @else
                                        <div id="shortAirings" style="width:100%; height:400px; font-size:11px;"></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Categories
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @if($shortFormCategoriesByNetwork->isEmpty())
                                        <p class="no-results">No results returned with the given filters.</p>
                                    @else
                                        <div id="categoriesChartS" style="width:100%; height:583px; font-size:11px;"></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Top Ranking Categories
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @if($shortFormCategoriesByNetwork->isEmpty())
                                        <p class="no-results">No results returned with the given filters.</p>
                                    @else
                                        <table id="smallCat" class="table table-hover table-bordered results">
                                            <thead>
                                                <th>Name</th>
                                                <th>Airings</th>
                                                @if($network->language_id == 1)
                                                <th>Media Spend</th>
                                                @endif
                                            </thead>
                                            <tbody>
                                            @foreach($shortFormCategoriesByNetwork as $shortForm)
                                                <tr>
                                                    <td>{!! link_to_route('network-anaylizer.categories.show', $shortForm->category_name,  ['id' => $shortForm->category_id, 'channels[]' => $network->id]) !!}</td>
                                                    <td>{!! $shortForm->air_count !!}</td>
                                                    @if($network->language_id == 1)
                                                    <td data-order="{!! $shortForm->air_cost !!}">
                                                        ${!! number_format($shortForm->air_cost) !!}
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Spots
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @if($shortFormSpotsBynetwork->isEmpty())
                                        <p class="no-results">No results returned with the given filters.</p>
                                    @else
                                        <div id="spotsChartS" style="width:100%; height:583px; font-size:11px;"></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        Top Ranking Spots
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @if($shortFormSpotsBynetwork->isEmpty())
                                        <p class="no-results">No results returned with the given filters.</p>
                                    @else
                                        <table id="smallSpots" class="table table-hover table-bordered results">
                                            <thead>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Airings</th>
                                                @if($network->language_id == 1)
                                                <th>Media Spend</th>
                                                @endif
                                            </thead>
                                            <tbody>
                                            @foreach($shortFormSpotsBynetwork as $shortForm)
                                                <tr>
                                                    <td>{!! link_to_route('spots.schedule', $shortForm->spot_title, ['spot' => $shortForm->spot_id]) !!}</td>
                                                    <td>{!! link_to_route('network-anaylizer.categories.show', $shortForm->category_name,  ['id' => $shortForm->category_id]) !!}</td>
                                                    <td>{!! $shortForm->air_count !!}</td>
                                                    @if($network->language_id == 1)
                                                    <td data-order="{!! $shortForm->air_cost !!}">
                                                        ${!! number_format($shortForm->air_cost) !!}
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        New Spots
                                    </div>
                                </div>
                                <div class="panel-body">
                                    @if($newShortFormSpotsByNetwork->isEmpty())
                                            <p>No new spots detected.</p>

                                    @else
                                        <div id="newSpotsChartS" style="width:100%; height:583px; font-size:11px;"></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                            <div class="col-sm-6">
                                <div class="panel panel-default panel-shadow">
                                    <div class="panel-heading">
                                        <div class="panel-title">
                                            New Spots
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                    @if($newShortFormSpotsByNetwork->isEmpty())
                                            <p>No new spots detected.</p>
                                    @else
                                        <table id="newSmallSpots" class="table table-hover table-bordered results">
                                            <thead>
                                                <th>Name</th>
                                                <th>TRT</th>
                                                <th>Category</th>
                                                <th>Airings</th>
                                                @if($network->language_id == 1)
                                                <th>Media Spend</th>
                                                @endif
                                            </thead>
                                            <tbody>
                                            @foreach($newShortFormSpotsByNetwork as $shortForm)
                                                <tr>
                                                    <td>{!! link_to_route('spots.schedule', $shortForm->spot_title, ['spot' => $shortForm->spot_id]) !!}</td>
                                                    <td data-order="{{ $shortForm->length }}">:{{ $shortForm->length }}</td>
                                                    <td>{!! link_to_route('network-anaylizer.categories.show', $shortForm->category_name,  ['id' => $shortForm->category_id]) !!}</td>
                                                    <td>{!! $shortForm->air_count !!}</td>
                                                    @if($network->language_id == 1)
                                                    <td data-order="{{ $shortForm->air_cost }}">
                                                        ${!! number_format($shortForm->air_cost) !!}
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('script')
<script>
$(document).ready(function(){

    @include('cxp.partials.network-analyzer.common-scripts')

    $('#category_picker').multiselect(categoryConfigurationSet);

//=============================================================================
// Bootstrap Longform/Shortform Tab Selector
//=============================================================================
$('#longFormTab').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
});


//=============================================================================
// Chart Fix for Bootstrap Tabs
//=============================================================================
$('#shortFormTab').click(function (e) {
  $(this).tab('show');
  categoriesChartS.invalidateSize();
  spotsChartS.invalidateSize();
  airingsChartS.invalidateSize();
  newSpotsChartS.invalidateSize();
});

@if($longFormAiringsOverTime->isEmpty())
    $('#shortFormTab').tab('show');
@endif

//=============================================================================
// Data Tables
//=============================================================================
$('#longCat').dataTable({
    "order": [[ 1, "desc" ]]
});
$('#smallCat').dataTable({
    "order": [[ 1, "desc" ]]
});
$('#longPrograms').dataTable({
    "order": [[ 2, "desc" ]]
});
$('#smallSpots').dataTable({
    "order": [[ 2, "desc" ]]
});
$('#newSmallSpots').dataTable({
    "order": [[ 2, "desc" ]]
});
$('#newLongShows').dataTable({
    "order": [[ 2, "desc" ]]
});

//=============================================================================
//=============================================================================
// LONGFORM CHARTS
//=============================================================================
//=============================================================================

//=============================================================================
// Longform Airings Line Graph
//=============================================================================
var chart = AmCharts.makeChart("longAirings", {
    "type": "serial",
    // "theme": "light",
     "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "dataProvider": {!! $longFormAiringsOverTime->toJson() !!},
    "valueAxes": [{
        "logarithmic": true,
        "dashLength": 1,
        "guides": [{
            "dashLength": 6,
            "inside": true,
            "label": "average",
            "lineAlpha": 1,
            "value": 90.4
        }],
        "position": "left"
    }],
    "graphs": [{
        "bullet": "round",
        "id": "g1",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 7,
        "lineThickness": 2,
        "title": "Price",
        "type": "smoothedLine",
        "useLineColorForBulletBorder": true,
        "valueField": "air_count",
        "fillAlphas": .15
    }],
    "chartScrollbar": {},
    "chartCursor": {
        "cursorPosition": "mouse"
    },
    "dataDateFormat": "YYYY-MM-DD",
    "categoryField": "air_date",
    "categoryAxis": {
        "parseDates": true
    },
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

//=============================================================================
// Longform Category Chart
//=============================================================================
var categoriesChartL = AmCharts.makeChart("categoriesChartL", {
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "category_name",
    "rotate": true,
    "startDuration": 1,
    "networkAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Frequency",
            "type": "column",
            "valueField": "air_count"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Frequency"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {},
    "dataProvider": {!! $longFormCategoriesByNetwork->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

//=============================================================================
// Longform Programs Chart
//=============================================================================
var programsChartL = AmCharts.makeChart("programsChartL", {
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "program_title",
    "rotate": true,
    "startDuration": 1,
    "networkAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Frequency",
            "type": "column",
            "valueField": "air_count"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Frequency"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {},
    "dataProvider": {!! $longFormProgramsBynetwork->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

//=============================================================================
// New Longform Programs Chart
//=============================================================================
var newProgramsChartL = AmCharts.makeChart("newProgramsChartL", {
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "program_title",
    "rotate": true,
    "startDuration": 1,
    "networkAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Frequency",
            "type": "column",
            "valueField": "air_count"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Frequency"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {},
    "dataProvider": {!! $newLongFormProgramsBynetwork->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

//=============================================================================
//=============================================================================
// SHORTFORM CHARTS
//=============================================================================
//=============================================================================

//=============================================================================
// Shortform Airings Line Graph
//=============================================================================
var airingsChartS = AmCharts.makeChart("shortAirings", {
    "type": "serial",
    // "theme": "light",
     "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "dataProvider": {!! $shortFormAiringsOverTime->toJson() !!},
    "valueAxes": [{
        "logarithmic": true,
        "dashLength": 1,
        "guides": [{
            "dashLength": 6,
            "inside": true,
            "label": "average",
            "lineAlpha": 1,
            "value": 90.4
        }],
        "position": "left"
    }],
    "graphs": [{
        "bullet": "round",
        "id": "g1",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 7,
        "lineThickness": 2,
        "title": "Price",
        "type": "smoothedLine",
        "useLineColorForBulletBorder": true,
        "valueField": "air_count",
        "fillAlphas": .15
    }],
    "chartScrollbar": {},
    "chartCursor": {
        "cursorPosition": "mouse"
    },
    "dataDateFormat": "YYYY-MM-DD",
    "categoryField": "air_date",
    "categoryAxis": {
        "parseDates": true
    },
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

//=============================================================================
// Shortform Category Chart
//=============================================================================
var categoriesChartS = AmCharts.makeChart("categoriesChartS", {
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "category_name",
    "rotate": true,
    "startDuration": 1,
    "networkAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Frequency",
            "type": "column",
            "valueField": "air_count"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Frequency"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {},
    "dataProvider": {!! $shortFormCategoriesByNetwork->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});


//=============================================================================
// Shortform Spots Chart
//=============================================================================
var spotsChartS = AmCharts.makeChart("spotsChartS", {
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "spot_title",
    "rotate": true,
    "startDuration": 1,
    "networkAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Frequency",
            "type": "column",
            "valueField": "air_count"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Frequency"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {},
    "dataProvider": {!! $shortFormSpotsBynetwork->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

//=============================================================================
// New Shortform Spots Chart
//=============================================================================
var newSpotsChartS = AmCharts.makeChart("newSpotsChartS", {
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "spot_title",
    "rotate": true,
    "startDuration": 1,
    "networkAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Frequency",
            "type": "column",
            "valueField": "air_count"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Frequency"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {},
    "dataProvider": {!! $newShortFormSpotsByNetwork->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});
});
</script>
@stop
