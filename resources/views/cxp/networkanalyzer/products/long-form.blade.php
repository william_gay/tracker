@extends('cxp.layout')

@section('header')
  <h3>
    <i class="fa fa-sitemap"></i>
    {!! link_to_route('network-analyzer.products', 'Network Explorer') !!} <small>Product Explorer</small>
  </h3>
@stop

@section('style')
  <style>
  .scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
  }
  </style>
@stop

@section('content')

  <h6>{!! subscription_welcome() !!}</h6>

  @include('cxp.partials.filters.networkanalyzer-products')


  <div class="row">
    <div class="col-sm-12">
      <ul class="nav nav-tabs" role="tablist">
        <li><a href="{{route('network-analyzer.products.short-form', Request::query())}}">Shortform</a></li>
        <li class="active"><a href="{{route('network-analyzer.products.long-form', Request::query())}}">Longform</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="longform">
          <div class="row">
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Airings by Show
                  </div>
                </div>
                <div class="panel-body">
                  @if($longForms->isEmpty())
                    <p class="no-results">{!! __('network-analyzer.no-products') !!}</p>
                  @else
                    <div id="airingsDiv" style="width:100%; height:675px; font-size:11px;"></div>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Media Spend by Category
                  </div>
                </div>
                <div class="panel-body">
                  @if($longForms->isEmpty())
                    <p class="no-results">{!! __('network-analyzer.no-products') !!}</p>
                  @else
                    <div id="spendingDiv" style="width:100%; height:675px; font-size:11px;"></div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Total Airings Over Time
                  </div>
                </div>
                <div class="panel-body">
                  @if($longFormAiringsOverTime->isEmpty())
                    <p class="no-results">{!! __('network-analyzer.no-products') !!}</p>
                  @else
                    <div id="longAirings" style="width:100%; height:400px; font-size:11px;"></div>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Show Overview
                  </div>
                </div>
                <div class="panel-body">
                  @if($longForms->isEmpty())
                    <p class="no-results">{!! __('network-analyzer.no-products') !!}</p>
                  @else
                    <table id="longCat" class="table table-hover table-bordered results">
                      <thead>
                        <th>Show</th>
                        <th>Airings</th>
                        @if($marketToQuery == 1)
                          <th>Media Spend</th>
                        @endif
                      </thead>
                      <tbody>
                        @foreach($longForms as $longForm)
                          <tr>
                            <td>{!! link_to_route('programs.schedule', $longForm->program_title,  ['id' => $longForm->program_id]) !!}</td>
                            <td>{!! $longForm->air_count !!}</td>
                            @if($marketToQuery == 1)
                              <td data-order="{!! $longForm->air_cost !!}">
                                ${!! number_format($longForm->air_cost) !!}
                              </td>
                            @endif
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    Top Ranking Networks
                  </div>
                </div>
                <div class="panel-body">
                  @if($longFormNetworksByCategory->isEmpty())
                    <p class="no-results">{!! __('network-analyzer.no-products') !!}</p>
                  @else
                    <table id="longNet" class="table table-hover table-bordered results">
                      <thead>
                        <th>Name</th>
                        <th>Airings</th>
                        @if($marketToQuery == 1)
                          <th>Media Spend</th>
                        @endif
                      </thead>
                      <tbody>
                        @foreach($longFormNetworksByCategory as $longForm)
                          <tr>
                            <td>{!! link_to_route('network-analyzer.show', $longForm->channel_name, ['id' => $longForm->channel_id]) !!}</td>
                            <td>{!! $longForm->air_count !!}</td>
                            @if($marketToQuery == 1)
                              <td data-order="{!! $longForm->air_cost !!}">
                                ${!! number_format($longForm->air_cost) !!}
                              </td>
                            @endif
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    New Shows
                  </div>
                </div>
                <div class="panel-body">
                  @if($newLongFormProgramsBynetwork->isEmpty())
                    <p>No new shows detected.</p>
                  @else
                    <div id="newProgramsChartL" style="width:100%; height:583px; font-size:11px;"></div>
                  @endif
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                  <div class="panel-title">
                    New Shows
                  </div>
                </div>
                <div class="panel-body">
                  @if($newLongFormProgramsBynetwork->isEmpty())
                    <p>No new shows detected.</p>
                  @else
                    <table id="newLongShows" class="table table-hover table-bordered results">
                      <thead>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Airings</th>
                        @if($marketToQuery == 1)
                          <th>Media Spend</th>
                        @endif
                      </thead>
                      <tbody>
                        @foreach($newLongFormProgramsBynetwork as $longForm)
                          <tr>
                            <td>{!! link_to_route('programs.schedule', $longForm->program_title, ['program' => $longForm->program_id]) !!}</td>
                            <td>{!! link_to_route('network-anaylizer.categories.show', $longForm->category_name,  ['id' => $longForm->category_id]) !!}</td>
                            <td>{!! $longForm->air_count !!}</td>
                            @if($marketToQuery == 1)
                              <td data-order="{!! $longForm->air_cost !!}">
                                ${!! number_format($longForm->air_cost) !!}
                              </td>
                            @endif
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@stop

@section('script')
  <script>
  $(document).ready(function(){

    @include('cxp.partials.network-analyzer.common-scripts')

    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);

    //=============================================================================
    // Data Tables
    //=============================================================================
    $('#longNet').dataTable({
      "order": [[ 1, "desc" ]]
    });
    $('#longCat').dataTable({
      "order": [[ 1, "desc" ]]
    });
    $('#newLongShows').dataTable({
      "order": [[ 2, "desc" ]]
    });
    //=============================================================================
    //=============================================================================
    // LONGFORM CHARTS
    //=============================================================================
    //=============================================================================

    @if(! $longForms->isEmpty())
    //=============================================================================
    // Longform Airings by Category Pie Graph
    //=============================================================================
    var airingsChart = AmCharts.makeChart("airingsDiv", {
      "type": "pie",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
      "labelRadius": 5,
      "minRadius": 120,
      "outlineThickness": 10,
      "titleField": "program_title",
      "valueField": "air_count",
      "theme": "default",
      "balloon": {},
      "titles": [],
      "dataProvider": {!! $longForms->toJson() !!},
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true
      }
    });
    @endif

    @if(! $longForms->isEmpty())
    //=============================================================================
    // Longform Media Spend by Category Bar Graph
    //=============================================================================
    var spendingChart = AmCharts.makeChart("spendingDiv", {
      "type": "serial",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "categoryField": "program_title",
      "rotate": true,
      "startDuration": 1,
      "categoryAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
      },
      "chartCursor": {},
      "chartScrollbar": {},
      "trendLines": [],
      "graphs": [
        {
          "balloonText": "$[[value]]",
          "fillAlphas": 1,
          "id": "AmGraph-1",
          "title": "Media Spend",
          "type": "column",
          "valueField": "air_cost"
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "title": "Media Spend"
        }
      ],
      "allLabels": [],
      "balloon": {},
      "legend": {},
      "dataProvider": {!! $longForms->toJson() !!},
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
          textAlign: 'center',
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          iconTitle: 'Save chart as an image',
        }]
      }
    });
    @endif

    @if(! $longFormAiringsOverTime->isEmpty())
    //=============================================================================
    // Longform Total Airings Line Graph
    //=============================================================================
    var chart = AmCharts.makeChart("longAirings", {
      "type": "serial",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "dataProvider": {!! $longFormAiringsOverTime->toJson() !!},
      "valueAxes": [{
        "logarithmic": true,
        "dashLength": 1,
        "position": "left"
      }],
      "graphs": [{
        "bullet": "round",
        "id": "g1",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 7,
        "lineThickness": 2,
        "title": "Price",
        "type": "smoothedLine",
        "useLineColorForBulletBorder": true,
        "valueField": "air_count",
        "fillAlphas": .15
      }],
      "chartScrollbar": {},
      "chartCursor": {
        "cursorPosition": "mouse"
      },
      "dataDateFormat": "YYYY-MM-DD",
      "categoryField": "air_date",
      "categoryAxis": {
        "parseDates": true
      },
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true
      }
    });
    @endif

    @if(! $newLongFormProgramsBynetwork->isEmpty())
    //=============================================================================
    // New Longform Programs Chart
    //=============================================================================
    var newProgramsChartL = AmCharts.makeChart("newProgramsChartL", {
      "type": "serial",
      "pathToImages": "{!!URL::to('/')!!}/assets/img/",
      "categoryField": "program_title",
      "rotate": true,
      "startDuration": 1,
      "networkAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
      },
      "chartCursor": {},
      "chartScrollbar": {},
      "trendLines": [],
      "graphs": [
        {
          "balloonText": "[[value]]",
          "fillAlphas": 1,
          "id": "AmGraph-1",
          "title": "Airings",
          "type": "column",
          "valueField": "air_count"
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "title": "Frequency"
        }
      ],
      "allLabels": [],
      "balloon": {},
      "legend": {},
      "dataProvider": {!! $newLongFormProgramsBynetwork->toJson() !!},
      "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true
      }
    });
    @endif
  });
  </script>
@stop
