@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-shopping-cart"></i>
        Retail Report
    </h3>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="date-selection text-center">
            @if($quarterSelect)
                <form class="form-inline" style="display:inline-block;">
                    {!! Former::select('quarter','')->class('span6 form-control')->options($quarterSelect,$quarter->id) !!}
                    <button class="select btn btn-blue btn-icon">Select Quarter <i class="fa fa-calendar"></i></button>
                </form>
            @else
                <div class="alert alert-info">No quarterly reports available yet!</div>
            @endif
            </div>
        </div>
    </div>

    @if($quarter)
        <div class="col-sm-3">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="icon"><i class="fa fa-video-camera"></i></div>
                <div class="num" data-start="0" data-end="{!! $quarter->long_form_cnt !!}" data-postfix data-duration="1500">
                0
                </div>
                <h3>Long Form Products</h3>
                <p> </p>
            </div>
        </div>

        <div class="col-sm-3">

            <div class="tile-stats tile-white tile-white-primary">
                <div class="icon"><i class="fa fa-tint"></i></div>
                <div class="num" data-start="0" data-end="{!! $quarter->short_form_cnt !!}" data-postfix data-duration="1500">
                0
                </div>
                <h3>Short Form Products</h3>
                <p></p>
            </div>

        </div>

        <div class="col-sm-3">

            <div class="tile-stats tile-white-orange">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
                <div class="num" data-start="0" data-end="{!! $quarter->retailer_cnt !!}" data-postfix data-duration="1500">0</div>

                <h3>Retailers Tracked</h3>
                <p></a>
            </div>

        </div>

        <div class="col-sm-3">

            <div class="tile-stats tile-white-orange">
                <div class="icon"><i class="fa fa-shopping-cart"></i></div>
                <div  class="num" data-start="0" data-end="{!! $quarter->retail_products_cnt !!}" data-postfix data-duration="1500">0</div>

                <h3>Retail Products</h3>
                <p></p>
            </div>

        </div>
    @endif

@stop
