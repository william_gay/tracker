@extends('cxp.layout')

@include('cxp.retail-report.monthly.partials.header', array('subTitle' => 'Products'))

@section('content')

    @include('cxp.partials.retail-report-datepicker')

    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center">
                <i class="fa fa-shopping-cart"></i> Product Search
            </h4>
            <table id="products" class="table table-hover table-bordered results">
                <thead>
                    <th class="col-xs-3">Name</th>
                    <th class="col-xs-3">Category</th>
                    <th class="col-xs-3">Sub-Category</th>
                    <th>Min Price</th>
                    <th>Max Price</th>
                    <th class="col-xs-1">Retail</th>
                    <th class="col-xs-1">Spots</th>
                    <th class="col-xs-1">Show</th>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr @if($product->min_price > $product->max_price)class="danger"@endif>
                        <td>
                            {!! Html::linkRoute('retail-report.monthly.products.show',$product->name, array($product->slug, 'report' => $report->month->format('F Y'))) !!}
                            @if(Sentry::getUser()->isSuperUser())
                                <small>
                                    <a href="{!! route("admin.products.edit",array($product->id)) !!}" target="_blank">Edit</a>
                                </small>
                            @endif
                        </td>
                        <td>{!! $product->category ?? '' !!}</td>
                        <td>{!! $product->subcategory ?? '' !!}</td>
                        <td>
                            ${!! number_format($product->min_price, 2) !!}
                        </td>
                        <td>
                            @if($product->min_price != $product->max_price)
                                ${!! number_format($product->max_price, 2) !!}
                            @endif
                        </td>
                        <td>{!! $product->retailers !!}</td>
                        <td>{!! $product->spots->count() !!}</td>
                        <td>{!! $product->programs->count() !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop

@section('script')
<script>
$('#report').datepicker({
    viewMode: 'months',
    minViewMode: 'months',
    startDate: '07 2014',
    endDate: {!! json_encode($endMonth->month->format('m Y')) !!},
    format: 'MM yyyy'
});
$(window).scroll(function () {
  if ( $(this).scrollTop() > 100 && !$('.filter-sticky').hasClass('open') ) {
    $('.filter-sticky').addClass('open filter-sticky-stuck');
    $('.filter-sticky').slideUp(function() {
        $('.filter-sticky').insertAfter('#navbar-brand').slideDown();
    });
   } else if ( $(this).scrollTop() < 100 && $('.filter-sticky').hasClass('open')) {
    $('.filter-sticky').slideUp(function(){
        $('.filter-sticky-stuck').removeClass('open filter-sticky-stuck');
        $('.date-selection').append($('.filter-sticky'));
        $('.filter-sticky').slideDown();
    });
  }
});
var oTable;
$(function(){
  oTable = $('#products').dataTable({});
});
</script>
@stop
