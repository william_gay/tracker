@extends('cxp.layout')

@section('style')
<style>
#categorychartdiv {
    width       : 100%;
    height      : 375px;
    font-size   : 11px;
}
</style>
@stop

@include('cxp.retail-report.monthly.partials.header', array('subTitle' => 'Retailer &bull; '.$retailer->name))

@section('content')

    @include('cxp.partials.retail-report-datepicker')

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $products->count() !!}" data-postfix data-duration="1500">
                    0
                </div>
                <h3>{!! __('retailers.products') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                @if($retailer->store_count != 0)
                    {!! $retailer->store_count !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.stores') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                    @if($retailer->sqft)
                        {!! number_format($retailer->sqft) !!}
                    @else
                        N/A
                    @endif
                </div>
                <h3>{!! __('retailers.sqft') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $categories->count() !!}" data-postfix data-duration="1500">
                    0
                </div>
                <h3>{!! __('retailers.categories') !!}</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <a href="http://{!! $retailer->website !!}" target="_blank">
            @if($retailer->logo_location)
                <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $retailer->logo_location !!}" class="retailerlogo" title="{!! $retailer->name !!}" />
            @else
            <h4 class="text-center">
                <i class="fa fa-shopping-cart"></i> {!! $retailer->name !!}
            </h4>
            @endif
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        {!! __('retailers.product_distribution_category') !!}
                    </div>
                </div>
                <div class="panel-body">
                    <div id="categorychartdiv"></div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Category Overview
                    </div>
                </div>
                <div class="panel-body">
                    <table id="category-overview" class="table table-hover table-bordered results">
                        <thead>
                            <th>Category</th>
                            <th>Products</th>
                            <th>Avg Price</th>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>
                                    {!! Html::linkRoute('retail-report.monthly.retailers.category',$category->category_name, array($category->category_id, 'report' => $report->month->format('F Y'))) !!}
                                </td>
                                <td>{!! $category->product_count !!}</td>
                                <td>
                                    <span title="{!! $category->avg_price !!}">
                                        ${!! number_format($category->avg_price, 2) !!}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            @if($products)
            <table id="retailers" class="table table-hover table-bordered results">
                <thead>
                    <th class="col-xs-3">Name</th>
                    <th class="col-xs-2">Category</th>
                    <th class="col-xs-2">Price</th>
                    <th class="col-xs-2">Spot Price</th>
                    <th class="col-xs-2">Show Price</th>
                    <th class="col-xs-1">Website</th>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>
                        {!! Html::linkRoute('retail-report.monthly.products.show',$product->name, array($product->slug, 'report' => $report->month->format('F Y'))) !!}
                            @if(Sentry::getUser()->isSuperUser())
                                <small>
                                    <a href="/admin/retailer-products/{!! $product->id !!}/edit" target="_blank">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    | <a href="{!! route("admin.retailer-products.delete", array($product->id)) !!}"
                                        target="_blank">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </small>
                            @endif
                        </td>
                        <td>{!! $product->category_name ?? '' !!}</td>
                        <td>
                            <span title="{!! $product->min_price !!}">
                            @if($product->min_price != $product->max_price)
                                ${!! number_format($product->min_price, 2) !!} &dash; ${!! number_format($product->max_price, 2) !!}
                            @else
                                ${!! number_format($product->min_price, 2) !!}
                            @endif
                            </span>
                        </td>
                        <td>
                            <span title="{!! $product->spot_price ?? 0 !!}">
                            @if($product->spot_price)
                                ${!! $product->spot_price !!}
                            @else
                                N/A
                            @endif
                            </span>
                        </td>
                        <td>
                            {!! $product->program_price ?? 'N/A' !!}
                        </td>
                        <td>
                            @if($product->website)
                            <a class="btn btn-green btn-icon icon-left" href="{!! $product->website !!}" target="_blank">
                                <i class="fa fa-eye"></i>
                                View
                            </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>

@stop

@section('script')
<script>
$('#report').datepicker({
    viewMode: 'months',
    minViewMode: 'months',
    startDate: '07 2014',
    endDate: {!! json_encode($endMonth->month->format('m Y')) !!},
    format: 'MM yyyy'
});
$(window).scroll(function () {
  if ( $(this).scrollTop() > 100 && !$('.filter-sticky').hasClass('open') ) {
    $('.filter-sticky').addClass('open filter-sticky-stuck');
    $('.filter-sticky').slideUp(function() {
        $('.filter-sticky').insertAfter('#navbar-brand').slideDown();
    });
   } else if ( $(this).scrollTop() < 100 && $('.filter-sticky').hasClass('open')) {
    $('.filter-sticky').slideUp(function(){
        $('.filter-sticky-stuck').removeClass('open filter-sticky-stuck');
        $('.date-selection').append($('.filter-sticky'));
        $('.filter-sticky').slideDown();
    });
  }
});
var chart = AmCharts.makeChart("categorychartdiv", {
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "type": "pie",
    "theme": "none",
    "dataProvider": {!! $categories->toJson() !!},
    "valueField": "product_count",
    "titleField": "category_name",
    "outlineAlpha": 0.4,
    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

var oTable;
$(function(){
  oTable = $('#retailers').dataTable( {
    "pageLength": 100,



    "columns": [
      null,
      null,
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      null,
      { "bSortable": false }
    ],
    "oTableTools": {
        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
        "aButtons": [
            "copy",
            "csv",
            "xls",
            {
                "sExtends": "pdf",
                "sPdfOrientation": "landscape",
                "sPdfMessage": "IMS Report - Networks Monitored"
            },
            "print"
        ]
    }
  });
});

var coTable;
$(function(){
  oTable = $('#category-overview').dataTable( {
    "pageLength": 100,



    "columns": [
      null,
      null,
      { "sType": "title-numeric" }
    ],
    "oTableTools": {
        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
        "aButtons": [
            "copy",
            "csv",
            "xls",
            {
                "sExtends": "pdf",
                "sPdfOrientation": "landscape",
                "sPdfMessage": "IMS Report - Networks Monitored"
            },
            "print"
        ]
    }
  });
});
</script>
@stop
