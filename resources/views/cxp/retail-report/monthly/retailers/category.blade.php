@extends('cxp.layout')

@section('style')
<style>
#categorychartdiv {
    width       : 100%;
    height      : 550px;
    font-size   : 11px;
}
</style>
@stop

@include('cxp.retail-report.monthly.partials.header', array('subTitle' => 'Category Breakdown &dash; '.$category->name ))

@section('content')

    @include('cxp.partials.retail-report-datepicker')

    <div class="row">
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $retailers->count() !!}" data-postfix data-duration="1500">
                    0
                </div>
                <h3>{!! __('retailers.retailers') !!}</h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $products->count() !!}" data-postfix data-duration="1500">
                    0
                </div>
                <h3>{!! __('retailers.total_products') !!}</h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                @if($lowestPrice->min_price > 0)
                    ${!! number_format($lowestPrice->min_price) !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.lowest_price') !!}</h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                    ${!! number_format($highestPrice->max_price) !!}
                </div>
                <h3>{!! __('retailers.highest_price') !!}</h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                    {!! number_format($totalAirtimes) !!}
                </div>
                <h3>{!! __('retailers.total_airs') !!}</h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div>
                @if($totalMediaSpend)
                    ${!! number_format($totalMediaSpend) !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.total_media_spend') !!}</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="categorychartdiv"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">Retail Category Distribution &bull; {!! $category->name !!}</h3>
            <table id="retailers" class="table table-hover table-bordered results">
                <thead>
                    <th>Retailer</th>
                    <th>Products</th>
                    <th>Avg Square Feet</th>
                    <th>Category % of Total DR Products in Retailer</th>
                    <th>Lowest Retail Price</th>
                    <th>Highest Retail Price</th>
                    <th>Website</th>
                </thead>
                <tbody>
                @foreach($retailers as $retailer)
                    <tr>
                        <td>
                            {!! Html::linkRoute('retail-report.monthly.retailers.show',$retailer->retailer_name, array($retailer->retailer_slug, 'report' => $report->month->format('F Y'))) !!}
                        </td>
                        <td>{!! $retailer->product_count !!}</td>
                        <td>
                            <span title="{!! $retailer->sqft !!}">
                            @if ($retailer->sqft > 0)
                            {!! number_format($retailer->sqft) !!}
                            @else
                                N/A
                            @endif
                            </span>
                        </td>
                        <td>
                            <span title="{!! $retailer->product_count/$retailer->total_product_count !!}">
                            @if($retailer->total_product_count > 0)
                                {!! sprintf("%.2f%%", $retailer->product_count/$retailer->total_product_count * 100) !!}
                            @else
                                N/A
                            @endif
                            </span>
                        </td>
                        <td>
                            <span title="{!! $retailer->min_price!!}">
                            ${!! number_format($retailer->min_price, 2) !!}
                            </span>
                        </td>
                        <td>
                            <span title="{!! $retailer->max_price!!}">
                            ${!! number_format($retailer->max_price, 2) !!}
                            </span>
                        </td>
                        <td>{!! $retailer->retailer_website !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">{!! __('retailers.product_breakdown') !!}</h3>

            <table id="products" class="table table-hover table-bordered results">
                <thead>
                    <th>Product</th>
                    <th>Retailer Count</th>
                    <th>Lowest Retail Price</th>
                    <th>Highest Retail Price</th>
                    <th>DR Price</th>
                    <th>DR Airs</th>
                    <th>Media Spend</th>
                </thead>
                <tbody>
                @foreach($productsWithDR as $product)
                    <tr>
                        <td>
                            {!! Html::linkRoute('retail-report.monthly.products.show', $product['product_name'], array($product['product_slug'], 'report' => $report->month->format('F Y'))) !!}
                        </td>
                        <td>{!! $product['retailer_count'] !!}</td>
                        <td>
                            <span title="{!! $product['min_price'] !!}">
                            ${!! number_format($product['min_price'], 2) !!}
                            </span>
                        </td>
                        <td>
                            <span title="{!! $product['max_price'] !!}">
                            ${!! number_format($product['max_price'], 2) !!}
                            </span>
                        </td>
                        <td>
                        @if(is_numeric($product['dr_cost']))
                            <span title="{!! $product['dr_cost'] !!}">
                            ${!! number_format($product['dr_cost'], 2) !!}
                            </span>
                        @else
                            <span title="0">
                            {!! $product['dr_cost'] !!}
                            </span>
                        @endif
                        </td>
                        <td>{!! $product['dr_airs'] !!}</td>
                        <td>
                        @if(is_numeric($product['media_spend']))
                            <span title="{!! $product['media_spend'] !!}">
                            ${!! number_format($product['media_spend']) !!}
                            </span>
                        @else
                            <span title="0">
                            {!! $product['media_spend'] !!}
                            </span>
                        @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('script')
<script>
$('#report').datepicker({
    viewMode: 'months',
    minViewMode: 'months',
    startDate: '07 2014',
    endDate: {!! json_encode($endMonth->month->format('m Y')) !!},
    format: 'MM yyyy'
});
$(window).scroll(function () {
  if ( $(this).scrollTop() > 100 && !$('.filter-sticky').hasClass('open') ) {
    $('.filter-sticky').addClass('open filter-sticky-stuck');
    $('.filter-sticky').slideUp(function() {
        $('.filter-sticky').insertAfter('#navbar-brand').slideDown();
    });
   } else if ( $(this).scrollTop() < 100 && $('.filter-sticky').hasClass('open')) {
    $('.filter-sticky').slideUp(function(){
        $('.filter-sticky-stuck').removeClass('open filter-sticky-stuck');
        $('.date-selection').append($('.filter-sticky'));
        $('.filter-sticky').slideDown();
    });
  }
});
var pTable, rTable;
$(function(){
  pTable = $('#products').dataTable( {
    "pageLength": 100,



    "columns": [
      null,
      null,
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      null,
      null
    ],
    "oTableTools": {
        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
        "aButtons": [
            "copy",
            "csv",
            "xls",
            {
                "sExtends": "pdf",
                "sPdfOrientation": "landscape",
                "sPdfMessage": "IMS Report - Networks Monitored"
            },
            "print"
        ]
    }
  });

  rTable = $('#retailers').dataTable( {
    "pageLength": 100,



    "columns": [
      null,
      null,
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      null
    ],
    "oTableTools": {
        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
        "aButtons": [
            "copy",
            "csv",
            "xls",
            {
                "sExtends": "pdf",
                "sPdfOrientation": "landscape",
                "sPdfMessage": "IMS Report - Networks Monitored"
            },
            "print"
        ]
    }
  });
});
AmCharts.makeChart("categorychartdiv",
{
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "retailer",
    "startDuration": 1,
    "categoryAxis": {
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[title]] of [[category]]:[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "labelText": "[[value]]",
            "title": "Products",
            "type": "column",
            "valueField": "product_count"
        },
        {
            "balloonText": "[[title]] of [[category]]:[[value]]",
            "bullet": "round",
            "id": "AmGraph-2",
            "labelText": "[[value]]",
            "lineThickness": 2,
            "title": "Sq Ft - ten thousands",
            "valueField": "sqft"
        }
    ],
    "guides": [],
    "valueAxes": [],
    "allLabels": [],
    "balloon": {},
    "legend": {
        "useGraphSettings": true
    },
    "titles": [
        {
            "id": "Title-1",
            "size": 15,
            "text": "{!! $category->name !!} in Retailers"
        }
    ],
    "dataProvider": {!! $retailersGraph->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
}
);
</script>
@stop
