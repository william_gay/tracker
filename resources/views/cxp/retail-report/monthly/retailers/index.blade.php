@extends('cxp.layout')

@section('style')
<style>
#retailerchartdiv {
    width       : 100%;
    height      : 700px;
    font-size   : 11px;
}
</style>
@stop

@include('cxp.retail-report.monthly.partials.header', array('subTitle' => 'Retailers'))

@section('content')

    @include('cxp.partials.retail-report-datepicker')

    <div class="row">
        <div class="col-xs-12">
            <div id="retailerchartdiv"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center">
                Retailers
            </h4>
            @if($retailers)
            <table id="retailers" class="table table-hover table-bordered results">
                <thead>
                    <th class="col-xs-3 hidden-xs"></th>
                    <th class="col-xs-3">{!! __('retailers.name') !!}</th>
                    <th class="col-xs-2">{!! __('retailers.website') !!}</th>
                    <th>Products</th>
                    <th class="col-xs-2">{!! __('retailers.sqft') !!}</th>
                    <th class="col-xs-2">{!! __('retailers.store_count') !!}</th>
                </thead>
                <tbody>
                @foreach($retailers as $retailer)
                    <tr>
                        <td class="hidden-xs">
                            <span title="{!! $retailer->name !!}">
                            @if($retailer->logo_location)
                                <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $retailer->logo_location !!}" class="gridlogo" title="{!! $retailer->name !!}" />
                            @endif
                            </span>
                        </td>
                        <td>
                            {!! Html::linkRoute('retail-report.monthly.retailers.show',$retailer->name, array($retailer->slug, 'report' => $report->month->format('F Y'))) !!}
                        </td>
                        <td>{!! $retailer->website !!}</td>
                        <td>{!! $retailer->product_count !!}</td>
                        <td>
                            <span title="{!! $retailer->sqft !!}">
                            @if($retailer->sqft == 0)
                                N/A
                            @else
                                {!! number_format($retailer->sqft) !!}
                            @endif
                            </span>
                        </td>
                        <td>
                            <span title="{!! $retailer->store_count !!}">
                            @if($retailer->store_count == 0)
                                N/A
                            @else
                                {!! number_format($retailer->store_count) !!}
                            @endif
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>

@stop

@section('script')
<script>
$('#report').datepicker({
    viewMode: 'months',
    minViewMode: 'months',
    startDate: '07 2014',
    endDate: {!! json_encode($endMonth->month->format('m Y')) !!},
    format: 'MM yyyy'
});
$(window).scroll(function () {
  if ( $(this).scrollTop() > 100 && !$('.filter-sticky').hasClass('open') ) {
    $('.filter-sticky').addClass('open filter-sticky-stuck');
    $('.filter-sticky').slideUp(function() {
        $('.filter-sticky').insertAfter('#navbar-brand').slideDown();
    });
   } else if ( $(this).scrollTop() < 100 && $('.filter-sticky').hasClass('open')) {
    $('.filter-sticky').slideUp(function(){
        $('.filter-sticky-stuck').removeClass('open filter-sticky-stuck');
        $('.date-selection').append($('.filter-sticky'));
        $('.filter-sticky').slideDown();
    });
  }
});
var oTable;
$(function(){
  oTable = $('#retailers').dataTable( {
    "pageLength": 100,



    "columns": [
      { "sType": "title-string" },
      null,
      null,
      null,
      { "sType": "title-numeric" },
      { "sType": "title-numeric" }
    ],
    "oTableTools": {
        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
        "aButtons": [
            "copy",
            "csv",
            "xls",
            {
                "sExtends": "pdf",
                "sPdfOrientation": "landscape",
                "sPdfMessage": "IMS Report - Networks Monitored"
            },
            "print"
        ]
    }
  });
});

AmCharts.makeChart("retailerchartdiv",
{
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "name",
    "rotate": true,
    "startDuration": 1,
    "categoryAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[title]] of [[category]]:[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "labelText": "[[value]]",
            "title": "Products",
            "type": "column",
            "valueField": "product_count"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Products"
        },
        {
            "id": "ValueAxis-2",
            "position": "right",
            "gridAlpha": 0,
            "title": "Distribution By Retailer"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {
        "useGraphSettings": true
    },
    "titles": [
        {
            "id": "Title-1",
            "size": 15,
            "text": "Product Distribution By Retailer"
        }
    ],
    "dataProvider": {!! $retailers->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});
</script>
@stop
