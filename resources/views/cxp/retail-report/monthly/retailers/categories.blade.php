@extends('cxp.layout');

@include('cxp.retail-report.monthly.partials.header', array('subTitle' => 'Categories'))

@section('content')

    @include('cxp.partials.retail-report-datepicker')

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Products by Category
                    </div>
                </div>
                <div class="panel-body">
                    <div id="productsDiv" style="width:100%; height:675px; font-size:11px;"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Price by Category
                    </div>
                </div>
                <div class="panel-body">
                    <div id="priceDiv" style="width:100%; height:675px; font-size:11px;"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <table id="categories" class="table table-hover table-bordered results">
                <thead>
                    <th>Category</th>
                    <th>Number of Products</th>
                    <th>Average Price</th>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>
                            {!! Html::linkRoute('retail-report.monthly.retailers.category',$category->category_name, array($category->category_id, 'report' => $report->month->format('F Y'))) !!}
                        </td>
                        <td>{!! $category->product_count !!}</td>
                        <td>
                            <span title="{!! $category->avg_price !!}">
                                ${!! number_format($category->avg_price, 2) !!}
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('script')
<script>
$('#report').datepicker({
    viewMode: 'months',
    minViewMode: 'months',
    startDate: '07 2014',
    endDate: {!! json_encode($endMonth->month->format('m Y')) !!},
    format: 'MM yyyy'
});
$(window).scroll(function () {
  if ( $(this).scrollTop() > 100 && !$('.filter-sticky').hasClass('open') ) {
    $('.filter-sticky').addClass('open filter-sticky-stuck');
    $('.filter-sticky').slideUp(function() {
        $('.filter-sticky').insertAfter('#navbar-brand').slideDown();
    });
   } else if ( $(this).scrollTop() < 100 && $('.filter-sticky').hasClass('open')) {
    $('.filter-sticky').slideUp(function(){
        $('.filter-sticky-stuck').removeClass('open filter-sticky-stuck');
        $('.date-selection').append($('.filter-sticky'));
        $('.filter-sticky').slideDown();
    });
  }
});
var pTable;
$(function(){
  pTable = $('#categories').dataTable( {
    "pageLength": 100,



    "columns": [
      null,
      null,
      { "sType": "title-numeric" }
    ],
    "oTableTools": {
        "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
        "aButtons": [
            "copy",
            "csv",
            "xls",
            {
                "sExtends": "pdf",
                "sPdfOrientation": "landscape",
                "sPdfMessage": "IMS Report - Networks Monitored"
            },
            "print"
        ]
    }
  });
});

//=============================================================================
// Products by Category Pie Chart
//=============================================================================
var airingsChart = AmCharts.makeChart("productsDiv", {
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "type": "pie",
    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "labelRadius": 5,
    "minRadius": 120,
    "outlineThickness": 10,
    "titleField": "category_name",
    "valueField": "product_count",
    "theme": "default",
    "balloon": {},
    "titles": [],
    "exportConfig":{
          menuItems: [{
          icon: '/assets/img/export',
          format: 'png'
          }]
    },
    "dataProvider": {!! $categories->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

//=============================================================================
// Price by Category Bar Graph
//=============================================================================
var spendingChart = AmCharts.makeChart("priceDiv", {
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "category_name",
    "rotate": true,
    "startDuration": 1,
    "categoryAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "$[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Average Price",
            "type": "column",
            "valueField": "avg_price"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Average Price"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {},
    "dataProvider": {!! $categories->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});
</script>
@stop
