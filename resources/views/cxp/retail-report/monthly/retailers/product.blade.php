@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-shopping-cart"></i>
        Retailer &bull; {!! $retailer->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('retailer', $retailer) !!}

    <div class="row">
        <div class="col-xs-12">
            @if($retailer->logo_location)
                <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $retailer->logo_location !!}" class="retailerlogo" title="{!! $retailer->name !!}" />
            @else
            <h4 class="text-center">
                <i class="fa fa-shopping-cart"></i> {!! $retailer->name !!}
            </h4>
            @endif
            <h4 class="text-center">{!! $retailer->website !!}</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            @if($versions)
            <table id="report" class="table table-hover table-bordered results">
                <thead>
                    <th class="col-xs-3">Name</th>
                    <th class="col-xs-3">Category</th>
                    <th class="col-xs-3">Sub-Category</th>
                </thead>
                <tbody>
                @foreach($versions as $version)
                    <tr>
                        <td>{!! $version->name ?? 'N/A' !!}</td>
                        <td>{!! $version->category->name ?? 'N/A' !!}</td>
                        <td>{!! $version->subCategory->name ?? 'N/A' !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>

@stop

@section('script')
<script>
$('#report').datepicker({
    viewMode: 'months',
    minViewMode: 'months',
    startDate: '07 2014',
    endDate: {!! json_encode($endMonth->month->format('m Y')) !!},
    format: 'MM yyyy'
});
$(window).scroll(function () {
  if ( $(this).scrollTop() > 100 && !$('.filter-sticky').hasClass('open') ) {
    $('.filter-sticky').addClass('open filter-sticky-stuck');
    $('.filter-sticky').slideUp(function() {
        $('.filter-sticky').insertAfter('#navbar-brand').slideDown();
    });
   } else if ( $(this).scrollTop() < 100 && $('.filter-sticky').hasClass('open')) {
    $('.filter-sticky').slideUp(function(){
        $('.filter-sticky-stuck').removeClass('open filter-sticky-stuck');
        $('.date-selection').append($('.filter-sticky'));
        $('.filter-sticky').slideDown();
    });
  }
});
var oTable;
$(function(){
  oTable = $('#report').dataTable( {
    "pageLength": 100,
    "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>",


    "columns": [
      null,
      null,
      null
    ]
  });
});
</script>
@stop
