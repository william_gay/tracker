@section('header')
    <h3>
        <i class="fa fa-shopping-cart"></i>
        Retail Analyzer <small>{!! $subTitle ?? '' !!}</small>
    </h3>
@stop