@extends('cxp.layout')

@include('cxp.retail-report.monthly.partials.header', array('subTitle' => 'Monthly Report'))

@section('content')

    @include('cxp.partials.retail-report-datepicker')

    @if($report)
        <div class="col-sm-4">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="icon"><i class="fa fa-gift"></i></div>
                <div class="num" data-start="0" data-end="{!! $report->product_count !!}" data-postfix data-duration="1500">0</div>
                <h3>New Products</h3>
                <p></p>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
                <div class="num" data-start="0" data-end="{!! $report->retailer_count !!}" data-postfix data-duration="1500">0</div>
                <h3>Retailers Tracked</h3>
                <p></a>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="icon"><i class="fa fa-shopping-cart"></i></div>
                <div  class="num" data-start="0" data-end="{!! $report->total_product_count !!}" data-postfix data-duration="1500">0</div>
                <h3>Retail Products</h3>
                <p></p>
            </div>

        </div>
    @endif

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Retailer Distribution by Category
                    </div>
                </div>
                <div class="panel-body">
                    <div id="categoryDiv" style="width:100%; height:675px; font-size:11px;"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Total Products by Retailer
                    </div>
                </div>
                <div class="panel-body">
                    <div id="retailerDiv" style="width:100%; height:800px; font-size:11px;"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        New Products by Category
                    </div>
                </div>
                <div class="panel-body">
                    <div id="productsDiv" style="width:100%; height:500px; font-size:11px;"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Retailers with Most New Products
                    </div>
                </div>
                <div class="panel-body">
                    <div id="newProductsRetailerDiv" style="width:100%; height:500px; font-size:11px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="category" class="col-sm-4 col-xs-12">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Categories
                    </div>
                </div>
                <div class="panel-body text-center">
                    <table class="table table-hover">
                        <thead>
                            <th>Category</th>
                            <th>Retailers</th>
                            <th>Products</th>
                            <th><div class="text-right">Avg $</div></th>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td>
                                    <div class="text-left">
                                        {!! Html::linkRoute('retail-report.monthly.retailers.category',$category->category_name, array($category->category_id, 'report' => $report->month->format('F Y'))) !!}
                                    </div>
                                </td>
                                <td>{!! $category->retailer_count !!}</td>
                                <td>{!! $category->product_count !!}</td>
                                <td><div class="text-right">${!! number_format($category->avg_price, 2) !!}</div></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <a href="{!! URL::route('retail-report.monthly.retailers.categories', array('report' => $report->month->format('F Y'))) !!}" class="btn btn-warning">All Categories</a>
                </div>
            </div>
        </div>
        <div id="retailers" class="col-sm-4 col-xs-12">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Retailers
                    </div>
                </div>
                <div class="panel-body text-center">
                    <table class="table table-hover">
                        <thead>
                            <th>Retailer</th>
                            <th>Products</th>
                            <th><div class="text-right">Avg $</div></th>
                        </thead>
                        <tbody>
                            @foreach($retailers as $retailer)
                            <tr>
                                <td>
                                    <div class="text-left">
                                        {!! Html::linkRoute('retail-report.monthly.retailers.show',$retailer->retailer_name, array($retailer->retailer_slug, 'report' => $report->month->format('F Y'))) !!}
                                    </div>
                                </td>
                                <td>{!! $retailer->product_count !!}</td>
                                <td><div class="text-right">${!! number_format($retailer->avg_price) !!}</div></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <a href="{!! URL::route('retail-report.monthly.retailers', array('report' => $report->month->format('F Y'))) !!}" class="btn btn-warning">All Retailers</a>
                </div>
            </div>
        </div>
        <div id="products" class="col-sm-4 col-xs-12">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Products
                    </div>
                </div>
                <div class="panel-body text-center">
                    <table class="table table-hover">
                        <thead>
                            <th>Product</th>
                            <th>Retailers</th>
                            <th><div class="text-right">Avg $</div></th>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>
                                    <div class="text-left">
                                        {!! Html::linkRoute('retail-report.monthly.products.show',$product->product_name, array($product->product_slug, 'report' => $report->month->format('F Y'))) !!}
                                    </div>
                                </td>
                                <td>{!! $product->retailer_count !!}</td>
                                <td><div class="text-right">${!! number_format($product->avg_price, 2) !!}</div></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <a href="{!! URL::route('retail-report.monthly.products', array('report' => $report->month->format('F Y'))) !!}" class="btn btn-warning">All Products</a>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')
<script>
$('#report').datepicker({
    viewMode: 'months',
    minViewMode: 'months',
    startDate: '07 2014',
    endDate: {!! json_encode($endMonth->month->format('m Y')) !!},
    format: 'MM yyyy'
});
$(window).scroll(function () {
  if ( $(this).scrollTop() > 100 && !$('.filter-sticky').hasClass('open') ) {
    $('.filter-sticky').addClass('open filter-sticky-stuck');
    $('.filter-sticky').slideUp(function() {
        $('.filter-sticky').insertAfter('#navbar-brand').slideDown();
    });
   } else if ( $(this).scrollTop() < 100 && $('.filter-sticky').hasClass('open')) {
    $('.filter-sticky').slideUp(function(){
        $('.filter-sticky-stuck').removeClass('open filter-sticky-stuck');
        $('.date-selection').append($('.filter-sticky'));
        $('.filter-sticky').slideDown();
    });
  }
});
var categoryChart = AmCharts.makeChart("categoryDiv", {
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "category_name",
    "rotate": true,
    "startDuration": 1,
    "categoryAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[title]] of [[category]]:[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Products",
            "type": "column",
            "valueField": "product_count"
        },
        {
            "balloonText": "[[title]] of [[category]]:[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-2",
            "title": "Retailers",
            "type": "column",
            "valueAxis": "ValueAxis-2",
            "valueField": "retailer_count"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Products"
        },
        {
            "id": "ValueAxis-2",
            "position": "right",
            "gridAlpha": 0,
            "title": "Retailers"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {},
    "dataProvider": {!! $categories->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

var retailerChart = AmCharts.makeChart("retailerDiv", {
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "retailer_name",
    "columnSpacing": 3,
    "rotate": true,
    "startDuration": 1,
    "categoryAxis": {
        "autoRotateCount": 0,
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[title]] of [[retailer]]:[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Products",
            "type": "column",
            "valueField": "product_count"
        },
        {
            "balloonText": "[[title]] of [[retailer]]:[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-2",
            "title": "Categories",
            "type": "column",
            "valueAxis": "ValueAxis-2",
            "valueField": "category_count"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Products"
        },
        {
            "id": "ValueAxis-2",
            "position": "right",
            "gridAlpha": 0,
            "title": "Categories"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {},
    "dataProvider": {!! $retailers->toJson() !!},
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    }
});

var productsChart = AmCharts.makeChart("productsDiv", {
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "type": "pie",
    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "labelRadius": 5,
    "minRadius": 120,
    "outlineThickness": 10,
    "titleField": "category_name",
    "valueField": "product_count",
    "theme": "default",
    "balloon": {},
    "titles": [],
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    },
    "dataProvider": {!! $newProductsByCategory->toJson() !!}
});

var productsRetailerChart = AmCharts.makeChart("newProductsRetailerDiv", {
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "type": "pie",
    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "labelRadius": 8,
    "minRadius": 120,
    "outlineThickness": 16,
    "titleField": "retailer_name",
    "valueField": "product_count",
    "theme": "default",
    "balloon": {},
    "titles": [],
    "amExport": {
        top : 0,
        right : 50,
        exportJPG : true,
        exportPNG : true,
        exportSVG : true,
        exportPDF : true,
        menuItems: [{
            textAlign: 'center',
            icon: '{!!URL::to('/')!!}/assets/img/export.png',
            iconTitle: 'Save chart as an image',
        }]
    },
    "dataProvider": {!! $newProductsByRetailer->toJson() !!}
});
</script>
@stop
