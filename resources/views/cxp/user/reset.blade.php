<!DOCTYPE html>
<!--[if lt IE 7]>       <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>          <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>          <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{!! Config::get('app.site_config.description') !!}">
    <meta name="viewport" content="width=device-width">

    <title>{!! Config::get('app.site_config.title') !!} &bull; Password Reset</title>

    @include('cxp.partials.favicon')
    @include('cxp.partials.assets.styles')

    @yield('style')

    <!-- Load Google JavaScript API -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!--[if lt IE 9]>
        {!! Html::script("assets/js/ie8-responsive-file-warning.js") !!}
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        {!! Html::script("assets/js/excanvas.js") !!}
    <![endif]-->

    @include('cxp.partials.bugsnag')

    <style type="text/css">
        .login-page .login-content .login-white a {
            color: #ffffff;
        }
    </style>

</head>
<body class="page-body login-page login-form-fall" data-url="{!! Config::get('app.url') !!}">


<script type="text/javascript">
var baseurl = "{!! Config::get('app.url') !!}";
</script>

<div class="login-container">

    <div class="login-header login-caret">

        <div class="login-content">

            <a class="logo" href="{!! URL::to('dashboard') !!}">
                @if(App::environment() == 'drtvlab')
                <img src="{!! asset('assets/img/drtvlab/logo.gif') !!}" class="img-responsive text-center" style="margin:auto" alt="{!! Config::get('app.site_config.title') !!}"/>
            @else
                <img src="{!! asset('assets/img/media-analytics-large.png') !!}" class="img-responsive text-center" style="margin:auto" alt="Media Analytics Dashboard"/>
            @endif
            </a>

            <!-- progress bar indicator -->
        </div>

    </div>

    <div class="login-progressbar">
        <div></div>
    </div>

    <div class="login-form">

        <div class="login-content">
            @include('cxp.partials.alert')
            <div class="form-login-error">
                @if ( Session::has('login_error') )
                    <h3>{!! Session::get('login_error') !!}</h3>
                @endif
          </div>


          {{ Form::open(['route' => 'user.reset', 'class' => 'form-signin']) }}
                <h2 class="form-signin-heading">Reset Password</h2>
                <h5>Please Enter Your Email Below</h5>

                <div class="form-group">
                    <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-envelope-o"></i>
                            </div>
                    {!! Former::xlarge_text('email','')->class('form-control') !!}
                    </div>
                </div>

                <hr>
                <button class="btn btn-large btn-primary" type="submit">Reset</button>

                <div class="form-group">
                    <br>

                    {!! Html::linkRoute('user.login', 'Back to Login') !!}

                </div>

            {{ Form::close() }}

            <div class="login-bottom-links">
                @include('cxp.partials.social')
            </div>

        </div>

    </div>

</div>
    @include('cxp.partials.modals.ieerror')

        <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="/assets/js/excanvas.min.js"></script><![endif]-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script>
        if (typeof jQuery == 'undefined') {
            document.write(unescape("%3Cscript src='/assets/js/jquery-1.11.1.min.js' type='text/javascript'%3E%3C/script%3E"));
        }
        </script>

        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="{{ elixir("assets/js/shared.js") }}"></script>
        <script src="{{ elixir("assets/js/script.js") }}"></script>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.2/b-colvis-1.5.2/b-flash-1.5.2/b-html5-1.5.2/b-print-1.5.2/cr-1.5.0/fc-3.2.5/fh-3.1.4/kt-2.4.0/r-2.2.2/rg-1.0.3/rr-1.2.4/sc-1.5.0/sl-1.2.6/datatables.min.js"></script>

        @yield('script')

        @include('cxp.partials.snappy')

        @yield('script')

        @include('analytics')

</body>
</html>
