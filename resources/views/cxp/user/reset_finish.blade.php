@extends('cxp.layout-nosidebar')

@section('header')
    <h3>
        <i class="icon-signin"></i>
        Reset Password
    </h3>
@stop

@section('content')
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
                <div class="margin-top-20">
                    @include('cxp.partials.alert')
                    @if ( Session::has('login_error') )
                        <div class="alert-login alert-error">
                            <strong>{!! Session::get('login_error') !!}</strong>
                        </div>
                    @endif
                </div>

                {{ Form::open(['route' => 'user.reset.complete', 'class' => 'form-signin']) }}
                    <h2 class="form-signin-heading">Reset</h2>
                    {!! Former::xlarge_text('login_attribute', ucfirst($login_attribute), $user->email) !!}
                    {!! Former::xlarge_text('reset_code', 'Reset Code', $resetCode) !!}
                    {!! Former::xlarge_password('password', 'Password') !!}
                    {!! Former::xlarge_password('password_confirmation', 'Confirm Password') !!}
                    <hr>
                    <button class="btn btn-large btn-primary" type="submit">Reset</button>
                     | {!! Html::linkRoute('user.login', 'Back to Login') !!}
                {{ Form::close() }}

        </div>
    </div>
@stop
