@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-user"></i>
        User Profile
    </h3>
@stop

@section('content')
	<div class="row">
		<div class="col-xs-12">
			<p></p>
		</div>
	</div>
    {!! Former::horizontal_open( route('user.settings'), 'POST' )!!}
    <div class="row">
        <div class="col-xs-4 col-xs-offset-1">
            <div class="form-group">
            <legend>User Information </legend>
            {!! Former::xlarge_text('first_name', 'First Name', $user['first_name'])
                ->class('form-control')
                ->required() !!}
            {!! Former::xlarge_text('last_name', 'Last Name', $user['last_name'])
                ->class('form-control')
                ->required() !!}
            </div>
        </div>
        <div class="col-xs-4 col-xs-offset-1">
            <div class="form-group">
            <legend>Password <small>leave blank to keep the same password</small></legend>
            {!! Former::xlarge_password('password', 'Password')
                ->class('form-control') !!}
            {!! Former::xlarge_password('password_confirmation', 'Confirm Password')
                ->class('form-control') !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col-xs-offset-1">
            <div class="form-group">
            <legend>Favorite Categories</legend>
            {!! Former::xlarge_text('category_ids', '', $user['categories'])
                ->class('form-control') !!}
            </div>
        </div>
        <div class="col-xs-4 col-xs-offset-1">
            <div class="form-group">
            <legend>Favorite Products</legend>
            {!! Former::xlarge_text('product_ids', '', $user['products'])
                ->class('form-control') !!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col-xs-offset-1">
            <div class="form-group">
            <legend>Dashboard Type</legend>
            {!! Former::select('dashboardtype_id','')
                ->options($dashboards,$user['dashboardtype_id'])
                ->id('dashboardtype_id')
                ->class('form-control') !!}
            </div>
        </div>
        {{-- <div class="col-xs-4 col-xs-offset-1">
            <div class="form-group">
            <legend>Market</legend>
            {!! Former::select('language_id','')
                ->options($languages,$user['language_id'])
                ->id('language_id')
                ->class('form-control') !!}
            </div>
        </div> --}}
    </div>
    <div class="row">
        {{-- <div class="col-xs-4 col-xs-offset-1">
            <div class="form-group">
            <legend>Advertising Type</legend>
            {!! Former::select('advert_type','')
                ->options($advertisings,$user['advert_type'])
                ->id('advert_type')
                ->class('form-control') !!}
            </div>
        </div> --}}
    </div>
    <div class="row">
        <div class="col-xs-4 col-xs-offset-4">
            <div class="form-actions">
                <button type="submit" class="btn btn-green btn-icon icon-left"> <i class="fa fa-check"></i> Save changes</button>
                <a href="{!!route('dashboard')!!}" class="btn btn-red btn-icon icon-left"> <i class="fa fa-ban"></i> Cancel</a>
            </div>
        </div>
    </div>
    {!! Former::close() !!}
@stop

