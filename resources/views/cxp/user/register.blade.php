<!DOCTYPE html>
<!--[if lt IE 7]>       <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>          <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>          <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{!! Config::get('app.site_config.description') !!}">
    <meta name="viewport" content="width=device-width">

    <title>{!! Config::get('app.site_config.title') !!} &bull; Register</title>

    @include('cxp.partials.favicon')
    @include('cxp.partials.assets.styles')

    <style>
    .form-group {
        width: 320px;
        margin-left: auto;
        margin-right: auto;
    }
    .login-bottom-links {
        width: 320px;
        margin-left: auto;
        margin-right: auto;
        text-align: center;
    }
    </style>
    @yield('style')

    <!-- Load Google JavaScript API -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!--[if lt IE 9]>
        {!! Html::script("assets/js/ie8-responsive-file-warning.js") !!}
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        {!! Html::script("assets/js/excanvas.js") !!}
    <![endif]-->

    @include('cxp.partials.bugsnag')

    <style type="text/css">
        .login-page .login-content .login-white a {
            color: #ffffff;
        }
    </style>

</head>
<body class="page-body login-page login-form-fall" data-url="https://my.imsreport.com">


<script type="text/javascript">
var baseurl = "{!! Config::get('app.url') !!}";
</script>

<div class="login-container">

    <div class="login-header login-caret">

        <div class="login-content">

            <a class="logo" href="{!! URL::to('dashboard') !!}">
                @if(App::environment() == 'drtvlab')
                    <img src="{!! asset('assets/img/drtvlab/logo.gif') !!}" class="img-responsive text-center" style="margin:auto" alt="{!! Config::get('app.site_config.title') !!}"/>
                @else
                    <img src="{!! asset('assets/img/media-analytics-large.png') !!}" class="img-responsive text-center" style="margin:auto" alt="Media Analytics Dashboard"/>
                @endif
            </a>
        </div>
    </div>

    <div class="login-progressbar">
        <div></div>
    </div>

    <div class="login-form">

        <div class="col-md-8 col-md-offset-2">
            @include('cxp.partials.alert')

            <div class="form-login-error @if ( Session::has('login_error') )visible @endif">
                    <div class="alert-login alert-error">
                        <strong>{!! Session::get('login_error') !!}</strong>
                    </div>
            </div>


            {{ Form::open(['route' => 'user.register', 'class' => 'form-signin', 'id' => 'form_register']) }}
                @if (App::environment() != 'drtvlab')
                        <h2 class="text-center">Free Report Now Available - Monthly Top 25 in Both Long and Short Form</h2>
                        <hr>
                        <div class="row">
                            <div class="col-md-5">
                                <ul class="unstyled" style="color: #000000; margin-top: 30px;">
                                    <li><i class="icon-ok"></i> &nbsp;Get Last Month's Report NOW!</li>
                                    <li><i class="icon-ok"></i> &nbsp;Access anytime from any device when you sign up</li>
                                    <li><i class="icon-ok"></i> &nbsp;View The Long and Short form DRTV rankings</li>
                                    <li><i class="icon-ok"></i> &nbsp;See what products are running each month</li>
                                </ul>
                            </div>
                            <div class="col-md-7">
                                <img src="{!! asset('assets/img/media-rankings.png') !!}" alt="Sample IMS Report">
                            </div>
                        </div>
                        <p class="text-center"><strong style="margin:20px;color: #000000;">FREE REPORT – MONTHLY TOP 25 IN BOTH LONG AND SHORT FORM - $269 Value - Free!</strong></p>
                        <hr>
                @endif

                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                    <div class="input-group-addon">
                                        <i class="entypo-user"></i>
                                    </div>
                                    <input class="form-control col-sm-6" id="first_name" type="text" name="first_name" required="required" placeholder="First Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                    <div class="input-group-addon">
                                        <i class="entypo-users"></i>
                                    </div>
                                    <input class="form-control col-sm-6" id="last_name" type="text" name="last_name" required="required" placeholder="Last Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                    <div class="input-group-addon">
                                        <i class="entypo-briefcase"></i>
                                    </div>
                                    <input class="form-control col-sm-11" id="company" type="text" name="company" placeholder="Company">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                    <div class="input-group-addon">
                                        <i class="entypo-chat"></i>
                                    </div>
                                    <input class="form-control col-sm-11" id="find_us" type="text" name="find_us" placeholder="How did you find out about us?">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                    <div class="input-group-addon">
                                        <i class="entypo-mail"></i>
                                    </div>
                                    <input class="form-control col-sm-11" id="email" type="text" name="email" required="required" placeholder="Email">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                    <span class="input-group-addon">
                                    <i class="entypo-key"></i>
                                    </span>
                                    <input class="form-control input-lg" type="password" name="password" id="password" required="required" placeholder="Password" >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <span class="input-group-addon">
                                    <i class="entypo-key"></i>
                                    </span>
                                <input class="form-control input-lg" id="password_confirmation" type="password" name="password_confirmation" required="required" placeholder="Confirm Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-block btn-orange btn-login" style="background:#ff9600;"> <i class="entypo-right-open-mini"></i> <strong>Get Your Report</strong>
                                        </button>
                                </div>
                            </div>
                        </div>
                <hr>

            {{ Form::close() }}

            <div class="login-bottom-links" style="padding-top: 0px;">

                <a href="{!! Config::get('app.site_config.company_website') !!}" class="link">
                    <i class="entypo-home"></i> Return to {!! Config::get('app.site_config.company_short') !!}
                </a>

                <br />

                @include('cxp.partials.social')
            </div>

        </div>

    </div>

</div>
        @include('cxp.partials.modals.ieerror')

        @include('cxp.partials.assets.scripts')

        <script>
            //show the welcomeModal on page load
            jQuery(document).ready(function() {

                var rv = -1; // Return value assumes failure.

                if (navigator.appName == 'Microsoft Internet Explorer')
                {
                    var ua = navigator.userAgent;
                    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");

                    if (re.exec(ua) != null) rv = parseFloat( RegExp.$1 );
                }

                    //if rv != -1 then it's an IE browser
                    if (rv != -1) {
                        //load after 5 seconds
                        setTimeout(function(){
                            jQuery('#IEModal').modal({show:true});
                        }, 5000);
                    }

              });
        </script>


        @yield('script')

        @include('analytics')

</body>
</html>
