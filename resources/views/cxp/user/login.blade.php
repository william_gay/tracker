<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{!! config('app.site_config.description') !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>{!! Config::get('app.site_config.title') !!} &bull; Login</title>

    @include('cxp.partials.favicon')
    @include('cxp.partials.assets.styles')

    @yield('style')

    <!-- Load Google JavaScript API -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    @include('cxp.partials.assets.compatability')

    @include('cxp.partials.bugsnag')

    <style type="text/css">
        .login-page .login-content .login-white a {
            color: #ffffff;
        }
    </style>

    <script type="text/javascript">
    var baseurl = "{!! config('app.url') !!}";
    </script>

</head>
<body class="page-body login-page login-form-fall" data-url="{!! config('app.url') !!}">
    <div class="login-container">
      <div class="login-header login-caret">
        <div class="login-content">
          <a class="logo" href="{!! URL::to('dashboard') !!}">
          @if(App::environment() == 'drtvlab')
              <img src="{!! asset('assets/img/drtvlab/logo.gif') !!}" class="img-responsive text-center" style="margin:auto" alt="{!! Config::get('app.site_config.title') !!}"/>
          @else
              <img src="{!! asset('assets/img/media-analytics-large.png') !!}" class="img-responsive text-center" style="margin:auto" alt="Media Analytics Dashboard"/>
          @endif
          </a>
          <!-- progress bar indicator -->
        </div>
      </div>

      <div class="login-progressbar">
        <div></div>
      </div>

        <div class="login-form">
            <div class="login-content">
              @include('cxp.partials.alert')

              <div class="form-login-error @if ( Session::has('login_error') )visible @endif">
                <div class="alert-login alert-error">
                  <strong>{!! Session::get('login_error') !!}</strong>
                </div>
              </div>

              {{ Form::open(['route' => 'user.login', 'class' => 'form-signin']) }}
                <h2 class="form-signin-heading" style="font-size: 35px;">Sign In</h2>
                <hr>
                <div class="form-group">
                  <div class="input-group input-group-lg">
                    <div class="input-group-addon">
                        <i class="entypo-user"></i>
                    </div>
                    <input class="form-control input-lg" type="text" name="email" id="email" value="{!! Request::old('email') !!}">
                  </div>
                </div>

                <div class="form-group">
                  <div class="input-group input-group-lg">
                    <span class="input-group-addon">
                    <i class="entypo-key"></i>
                    </span>
                    <input class="form-control input-lg" type="password" name="password" id="password" >
                  </div>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-7">
                      {{-- issue with the remember me creating cookie error
                      <label class="checkbox" for="remember_me" style="color:#cccccc;">
                        <input type="checkbox" name="remember_me" id="remember_me" value="true">  Remember me
                      </label> --}}
                    </div>
                    <div class="col-sm-5 text-right">
                        <button class="col-sm-12 btn btn-lg btn-blue" type="submit">Sign in <i class="fa fa-sign-in"></i></button>
                    </div>
                  </div><!-- /row -->
                </div>

                <div class="form-group">
                  <div class="row">
                      <div class="col-sm-7 text-left" style="color:#000000;">
                        <h4>Not A Customer Yet?</h4>
                      </div>
                      <div class="col-sm-5 text-right">
                        {!! link_to_route('user.register', 'Sign Up', array(), array('class' => "col-sm-12 btn btn-link")) !!}
                      </div>
                  </div><!-- /row -->
                </div>
                <hr>
              {{ Form::close() }}

              <div class="login-bottom-links" style="padding-top: 0px;">
                {!! Html::linkRoute('user.reset', 'Reset Password') !!}
                <br />
                @include('cxp.partials.social')
              </div>
            </div>
        </div>
    </div>

    @include('cxp.partials.modals.ieerror')

    @include('cxp.partials.assets.scripts')

    <script>
      //show the welcomeModal on page load
      jQuery(document).ready(function() {
        var rv = -1; // Return value assumes failure.

        if (navigator.appName == 'Microsoft Internet Explorer') {
            var ua = navigator.userAgent;
            var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");

            if (re.exec(ua) != null) rv = parseFloat( RegExp.$1 );
        }

        //if rv != -1 then it's an IE browser
        if (rv != -1) {
            //load after 5 seconds
            setTimeout(function(){
                jQuery('#IEModal').modal({show:true});
            }, 5000);
        }
      });
    </script>

    @yield('script')

    @include('cxp.partials.snappy')

    @include('analytics')
</body>
</html>
