@extends('cxp.layout')

@section('style')
<style>
#categorychartdiv {
    width       : 100%;
    height      : 375px;
    font-size   : 11px;
}
</style>
@stop

@section('header')
    <h3>
        <i class="fa fa-shopping-cart"></i>
        Retailer &bull; {!! $retailer->name !!}
        <small>
            <a href="{!! route("admin.retailers.edit", array($retailer->id)) !!}"
                target="_blank" rel="tooltip" title="Edit Retailer Product">
                <i class="fa fa-pencil-square-o"></i>
            </a>
        </small>
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('retailer', $retailer) !!}

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $products->count() !!}" data-postfix data-duration="1500">
                    0
                </div>
                <h3>{!! __('retailers.products') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                @if($retailer->store_count != 0)
                    {!! $retailer->store_count !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.stores') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                    @if($retailer->sqft)
                        {!! number_format($retailer->sqft) !!}
                    @else
                        N/A
                    @endif
                </div>
                <h3>{!! __('retailers.sqft') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $categories->count() !!}" data-postfix data-duration="1500">
                    0
                </div>
                <h3>{!! __('retailers.categories') !!}</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <a href="http://{!! $retailer->website !!}" target="_blank">
            @if($retailer->logo_location)
                <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $retailer->logo_location !!}" class="retailerlogo" title="{!! $retailer->name !!}" />
            @else
            <h4 class="text-center">
                <i class="fa fa-shopping-cart"></i> {!! $retailer->name !!}
            </h4>
            @endif
            </a>
        </div>
        <div class="col-xs-8">
            <div class="date-selection text-center" style="margin-top: 15px;">
            @if(isset($monthSelect))
                <form class="form-inline" style="display:inline-block;">
                    {!! Former::select('report','')->class('span6 form-control')->options($monthSelect,$report->id) !!}
                    <button class="select btn btn-blue btn-icon">Select Month <i class="fa fa-calendar"></i></button>
                </form>
            @else
                <div class="alert alert-info">{!! __('retailers.no_product_data') !!}</div>
            @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        {!! __('retailers.product_distribution_category') !!}
                    </div>
                </div>
                <div class="panel-body">
                    <div id="categorychartdiv"></div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Category Overview
                    </div>
                </div>
                <div class="panel-body">
                    <table id="category-overview" class="table table-hover table-bordered results">
                        <thead>
                            <th>Category</th>
                            <th>Products</th>
                            <th>Avg Price</th>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td><a href="/retailers/category/{!! $category->category_id !!}">{!! $category->category_name !!}</a></td>
                                <td>{!! $category->product_count !!}</td>
                                <td>
                                    <span title="{!! $category->avg_price !!}">
                                        ${!! number_format($category->avg_price, 2) !!}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            @if($products)
            <table id="retailers" class="table table-hover table-bordered results">
                <thead>
                    <th class="col-xs-3">Name</th>
                    <th class="col-xs-2">Category</th>
                    <th class="col-xs-2">Price</th>
                    <th class="col-xs-2">Spot Price</th>
                    <th class="col-xs-2">Show Price</th>
                    <th class="col-xs-1">Website</th>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <td>
                            {!! Html::linkRoute('products.show',$product->name, [$product->slug]) !!}
                            @if(Sentry::getUser()->isSuperUser())
                                <small>
                                    <a href="/admin/retailer-products/{!! $product->id !!}/edit" target="_blank">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    | <a href="{!! route("admin.retailer-products.delete", array($product->id)) !!}"
                                        target="_blank">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </small>
                            @endif
                        </td>
                        <td>{!! $product->category_name ?? '' !!}</td>
                        <td>
                            <span title="{!! $product->min_price !!}">
                            @if($product->min_price != $product->max_price)
                                ${!! number_format($product->min_price, 2) !!} &dash; ${!! number_format($product->max_price, 2) !!}
                            @else
                                ${!! number_format($product->min_price, 2) !!}
                            @endif
                            </span>
                        </td>
                        <td>
                            <span title="{!! $product->spot_price ?? 0 !!}">
                            @if($product->spot_price)
                                ${!! $product->spot_price !!}
                            @else
                                N/A
                            @endif
                            </span>
                        </td>
                        <td>
                            {!! $product->program_price ?? 'N/A' !!}
                        </td>
                        <td>
                            @if($product->website)
                            <a class="btn btn-green btn-icon icon-left" href="{!! $product->website !!}" target="_blank">
                                <i class="fa fa-eye"></i>
                                View
                            </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>

@stop

@section('script')
<script>
var chart = AmCharts.makeChart("categorychartdiv", {
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "type": "pie",
    "theme": "none",
    "dataProvider": {!! $categories->toJson() !!},
    "valueField": "product_count",
    "titleField": "category_name",
    "outlineAlpha": 0.4,
    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "exportConfig":{
      menuItems: [{
      icon: '{!!URL::to('/')!!}/assets/img/export.png',
      format: 'png'
      }]
    }
});

var oTable;
$(function(){
  oTable = $('#retailers').dataTable( {
    "pageLength": 100,
    "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>",


    "columns": [
      null,
      null,
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      null,
      { "bSortable": false }
    ]
  });
});

var coTable;
$(function(){
  oTable = $('#category-overview').dataTable( {
    "pageLength": 100,
    "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>",


    "columns": [
      null,
      null,
      { "sType": "title-numeric" }
    ]
  });
});
</script>
@stop
