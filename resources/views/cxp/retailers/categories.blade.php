@extends('cxp.layout');

@section('header')
    <h3>
        <i class="fa fa-shopping-cart"></i>
        Retail Report &bull; Categories
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('retailer-categories') !!}

    <div class="row">
        <div class="col-xs-12">
            <div class="date-selection text-center" style="margin-top: 15px;">
            @if(isset($monthSelect))
                <form class="form-inline" style="display:inline-block;">
                    {!! Former::select('report','')->class('span6 form-control')->options($monthSelect,$report->id) !!}
                    <button class="select btn btn-blue btn-icon">Select Month <i class="fa fa-calendar"></i></button>
                </form>
            @else
                <div class="alert alert-info">{!! __('retailers.no_product_data') !!}</div>
            @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Category Overview
                    </div>
                </div>
                <div class="panel-body">
                    <table id="airings" class="table table-hover table-bordered results">
                        <thead>
                            <th>Category</th>
                            <th>Number of Products</th>
                            <th>Average Price</th>
                            <th></th>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td><a href="/retailers/category/{!! $category->category_id !!}">{!! $category->category_name !!}</a></td>
                                <td>{!! $category->product_count !!}</td>
                                <td>${!! number_format($category->avg_price, 2) !!}</td>
                                <td>
                                    <a class="btn btn-green btn-icon icon-left" href="/retailers/category/{!! $category->category_id !!}">
                                    <i class="fa fa-eye"></i>
                                    View
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
