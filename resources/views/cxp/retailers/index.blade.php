@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-shopping-cart"></i>
        Retailers
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('retailers') !!}

    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center">
                Retailers
            </h4>
            @if($retailers)
            <table id="retailers" class="table table-hover table-bordered results">
                <thead>
                    <th class="col-xs-3 hidden-xs"></th>
                    <th class="col-xs-3">{!! __('retailers.name') !!}</th>
                    <th class="col-xs-2">{!! __('retailers.website') !!}</th>
                    <th class="col-xs-2">{!! __('retailers.sqft') !!}</th>
                    <th class="col-xs-2">{!! __('retailers.store_count') !!}</th>
                </thead>
                <tbody>
                @foreach($retailers as $retailer)
                    <tr>
                        <td class="hidden-xs">
                            <span title="{!! $retailer->name !!}">
                            @if($retailer->logo_location)
                                <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $retailer->logo_location !!}" class="gridlogo" title="{!! $retailer->name !!}" />
                            @endif
                            </span>
                        </td>
                        <td>
                            {!! Html::linkRoute('retailers.show',$retailer->name, $retailer->slug) !!}
                            @if(Sentry::getUser()->isSuperUser())
                            <small>
                                <a href="{!! route("admin.retailers.edit", array($retailer->id)) !!}"
                                    target="_blank" rel="tooltip" title="Edit Retailer Product">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>
                            </small>
                            @endif
                        </td>
                        <td>{!! $retailer->website !!}</td>
                        <td>
                            <span title="{!! $retailer->sqft !!}">
                            @if($retailer->sqft == 0)
                                N/A
                            @else
                                {!! number_format($retailer->sqft) !!}
                            @endif
                            </span>
                        </td>
                        <td>
                            <span title="{!! $retailer->store_count !!}">
                            @if($retailer->store_count == 0)
                                N/A
                            @else
                                {!! number_format($retailer->store_count) !!}
                            @endif
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>

@stop

@section('script')
<script>
var oTable;
$(function(){
  oTable = $('#retailers').dataTable( {
    "pageLength": 100,
    "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>",


    "columns": [
      { "sType": "title-string" },
      null,
      null,
      null,
      { "sType": "title-numeric" },
      { "sType": "title-numeric" }
    ]
  });
});
</script>
@stop
