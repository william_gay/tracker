@extends('cxp.layout')

@section('style')
<style>
#categorychartdiv {
    width       : 100%;
    height      : 550px;
    font-size   : 11px;
}
</style>
@stop

@section('header')
    <h3>
        <i class="fa fa-shopping-cart"></i>
        Retail Report &bull; Category Breakdown &dash; {!! $category->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('retailer-category', $category) !!}

    <div class="row">
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $retailers->count() !!}" data-postfix data-duration="1500">
                    0
                </div>
                <h3>{!! __('retailers.retailers') !!}</h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $products->count() !!}" data-postfix data-duration="1500">
                    0
                </div>
                <h3>{!! __('retailers.total_products') !!}</h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                @if($lowestPrice->min_price > 0)
                    ${!! number_format($lowestPrice->min_price) !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.lowest_price') !!}</h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                    ${!! number_format($highestPrice->max_price) !!}
                </div>
                <h3>{!! __('retailers.highest_price') !!}</h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                    {!! number_format($totalAirtimes) !!}
                </div>
                <h3>{!! __('retailers.total_airs') !!}</h3>
            </div>
        </div>
        <div class="col-lg-2 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div>
                @if($totalMediaSpend)
                    ${!! number_format($totalMediaSpend) !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.total_media_spend') !!}</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="date-selection text-center" style="margin-top: 15px;">
            @if(isset($monthSelect))
                <form class="form-inline" style="display:inline-block;">
                    {!! Former::select('report','')->class('span6 form-control')->options($monthSelect,$report->id) !!}
                    <button class="select btn btn-blue btn-icon">Select Month <i class="fa fa-calendar"></i></button>
                </form>
            @else
                <div class="alert alert-info">{!! __('retailers.no_product_data') !!}</div>
            @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="categorychartdiv"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">Retail Category Distribution &bull; {!! $category->name !!}</h3>
            <table id="retailers" class="table table-hover table-bordered results">
                <thead>
                    <th>Retailer</th>
                    <th>Products</th>
                    <th>Avg Square Feet</th>
                    <th>Category % of Total DR Products in Retailer</th>
                    <th>Lowest Retail Price</th>
                    <th>Highest Retail Price</th>
                    <th>Website</th>
                </thead>
                <tbody>
                @foreach($retailers as $retailer)
                    <tr>
                        <td>
                            {!! Html::linkRoute('retailers.show',$retailer->retailer_name, $retailer->retailer_slug) !!}
                        </td>
                        <td>{!! $retailer->product_count !!}</td>
                        <td>
                            <span title="{!! $retailer->sqft !!}">
                            @if ($retailer->sqft > 0)
                            {!! number_format($retailer->sqft) !!}
                            @else
                                N/A
                            @endif
                            </span>
                        </td>
                        <td>
                            <span title="{!! $retailer->product_count/$retailer->total_product_count !!}">
                            @if($retailer->total_product_count > 0)
                                {!! sprintf("%.2f%%", $retailer->product_count/$retailer->total_product_count * 100) !!}
                            @else
                                N/A
                            @endif
                            </span>
                        </td>
                        <td>
                            <span title="{!! $retailer->min_price!!}">
                            ${!! number_format($retailer->min_price, 2) !!}
                            </span>
                        </td>
                        <td>
                            <span title="{!! $retailer->max_price!!}">
                            ${!! number_format($retailer->max_price, 2) !!}
                            </span>
                        </td>
                        <td>{!! $retailer->retailer_website !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">{!! __('retailers.product_breakdown') !!}</h3>

            <table id="products" class="table table-hover table-bordered results">
                <thead>
                    <th>Product</th>
                    <th>Retailer Count</th>
                    <th>Lowest Retail Price</th>
                    <th>Highest Retail Price</th>
                    <th>DR Price</th>
                    <th>DR Airs</th>
                    <th>Media Spend</th>
                </thead>
                <tbody>
                @foreach($productsWithDR as $product)
                    <tr>
                        <td>
                            <a href="{!! route("products.show", array($product['product_slug'])) !!}">{!! $product['product_name'] !!}</a>
                        </td>
                        <td>{!! $product['retailer_count'] !!}</td>
                        <td>
                            <span title="{!! $product['min_price'] !!}">
                            ${!! number_format($product['min_price'], 2) !!}
                            </span>
                        </td>
                        <td>
                            <span title="{!! $product['max_price'] !!}">
                            ${!! number_format($product['max_price'], 2) !!}
                            </span>
                        </td>
                        <td>
                        @if(is_numeric($product['dr_cost']))
                            <span title="{!! $product['dr_cost'] !!}">
                            ${!! number_format($product['dr_cost'], 2) !!}
                            </span>
                        @else
                            <span title="0">
                            {!! $product['dr_cost'] !!}
                            </span>
                        @endif
                        </td>
                        <td>{!! $product['dr_airs'] !!}</td>
                        <td>
                        @if(is_numeric($product['media_spend']))
                            <span title="{!! $product['media_spend'] !!}">
                            ${!! number_format($product['media_spend']) !!}
                            </span>
                        @else
                            <span title="0">
                            {!! $product['media_spend'] !!}
                            </span>
                        @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('script')
<script>
var pTable, rTable;
$(function(){
  pTable = $('#products').dataTable( {
    "pageLength": 100,
    "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>",


    "columns": [
      null,
      null,
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      null,
      null
    ]
  });

  rTable = $('#retailers').dataTable( {
    "pageLength": 100,
    "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>",


    "columns": [
      null,
      null,
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      { "sType": "title-numeric" },
      null
    ]
  });
});
AmCharts.makeChart("categorychartdiv",
{
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "retailer",
    "startDuration": 1,
    "categoryAxis": {
        "gridPosition": "start"
    },
    "chartCursor": {},
    "chartScrollbar": {},
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[title]] of [[category]]:[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "labelText": "[[value]]",
            "title": "Products",
            "type": "column",
            "valueField": "product_count"
        },
        {
            "balloonText": "[[title]] of [[category]]:[[value]]",
            "bullet": "round",
            "id": "AmGraph-2",
            "labelText": "[[value]]",
            "lineThickness": 2,
            "title": "Sq Ft - ten thousands",
            "valueField": "sqft"
        }
    ],
    "guides": [],
    "valueAxes": [],
    "allLabels": [],
    "balloon": {},
    "legend": {
        "useGraphSettings": true
    },
    "titles": [
        {
            "id": "Title-1",
            "size": 15,
            "text": "{!! $category->name !!} in Retailers"
        }
    ],
    "dataProvider": {!! $retailersGraph->toJson() !!}
}
);
</script>
@stop
