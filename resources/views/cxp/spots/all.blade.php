@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-tint"></i>
        Spots &bull; {!! $language->name !!}
    </h3>
@stop

@section('content')
  {!! Breadcrumbs::render('spots') !!}
  <div class="row">
    <div class="col-xs-12">
      <h4 class="text-center">
        <i class="fa fa-tint"></i> Spot Search
      </h4>
      <input type="hidden" name="language_id" id="language_id" value="{!! $language_id !!}" />
      {!! $html->table() !!}
    </div>
  </div>
@stop

@section('script')
{!! $html->scripts() !!}
@stop
