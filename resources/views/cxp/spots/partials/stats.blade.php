<ul class="list-inline text-center stats-overview">
    <li>
        <div class="well well-sm">
            <h5>Life Avg Freq</h5>
            <h3>
            @if ($avgStats->count() > 0)
                {!! number_format($avgStats->sum('frequency')/$avgStats->count(), 2) !!}
            @else
                0
            @endif
            </h3>
        </div>
    </li>
    <li>
        <div class="well well-sm">
            <h5>Life Avg Media Index</h5>
            <h3>
            @if ($avgStats->count() > 0)
                {!! number_format($avgStats->sum('media_index')/$avgStats->count(), 2) !!}
            @else
                0
            @endif
            </h3>
        </div>
    </li>
</ul>
