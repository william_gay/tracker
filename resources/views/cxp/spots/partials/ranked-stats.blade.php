<ul class="list-inline text-center stats-overview">
    <li>
        <div class="well well-sm">
            <h5>Frequency</h5>
            <h3>
            @if($spotRanking)
                {!! $spotRanking->frequency !!} ({!! $spotRanking->rank !!})
            @else
                N/A
            @endif
            </h3>
        </div>
    </li>
    <li>
        <div class="well well-sm">
            <h5>Media Index</h5>
            <h3>
            @if($spotRanking)
                {!! $spotRanking->media_index !!}
            @else
                N/A
            @endif
            </h3>
        </div>
    </li>
    <li>
        <div class="well well-sm">
            <h5>Life Avg Freq</h5>
            <h3>
                {!! number_format((float) $avgStats->freq_avg, 2) !!}
            </h3>
        </div>
    </li>
    <li>
        <div class="well well-sm">
            <h5>Life Avg Media Index</h5>
            <h3>
                {!! number_format((float) $avgStats->mi_avg, 2) !!}
            </h3>
        </div>
    </li>
    <li>
        <div class="well well-sm">
            <h5>Life Freq Min</h5>
            <h3>{!! number_format((float) $avgStats->freq_min, 0) !!}</h3>
        </div>
    </li>
    <li>
        <div class="well well-sm">
            <h5>Life Freq Max</h5>
            <h3>{!! number_format((float) $avgStats->freq_max, 0) !!}</h3>
        </div>
    </li>
</ul>
