@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-tint"></i>
        Spot Overview : {!! $spot->title !!} @if(Sentry::getUser()->isSuperUser()) <small><a href="{!! route("admin.spots.edit",array($spot->id)) !!}" target="_blank">Edit</a></small> @endif
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('spot',$spot) !!}

    @if($spot->spot->product->first())
    <iframe src="{{ route('product.embed', ['product' => $spot->spot->product->first()->id, "hideCTA" => "true"]) }}" scrolling="no" frameborder="0" width="100%" height="700" style="width:100%;height: 700px; border: 0px;"></iframe>
    @endif

    @if(Sentry::getUser()->isSuperUser())
    <div class="date-selection text-center">
        <form class="form-inline" id="week_ending_frm" style="display:inline-block;">
            <span style="color: red;">Admin Only: </span> {!! Former::xlarge_text('week_ending','', $week_ending->format("F j, Y"))->id('weekly-rankings')->class('span6')->placeholder($week_ending->format("F j, Y")) !!}
            <button class="select btn btn-small">Select Week Ending</button>
        </form>
    </div>
    @endif
    @if(Sentry::getUser()->hasAccess('reports.spot-overview'))
    <div class="row">

    @if($spotRanking)
        @include('cxp.spots.partials.ranked-stats')
    @else
        @include('cxp.spots.partials.stats')
    @endif

        @if($overTime)
                <div id="show-trend" style="width: 100%;height: 300px;"></div>
        @endif
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Spot Overview
                    </div>
                </div>
                <div class="panel-body">
                <table class="table">
                    <tr>
                        <td class="bold">{!! __('spots.title') !!}</td><td>{!! $spot->title !!} v{!! $spot->version !!}</td>
                    </tr>
                    @if($spot->price != 0.00)
                    <tr>
                        <td class="bold">{!! __('spots.price') !!}</td><td>{!! $spot->currency !!}{{ $spot->price }}</td>
                    </tr>
                    @endif
                    @if($spot->price_extra != 0.00)
                    <tr>
                        <td class="bold">{!! __('spots.price_extra') !!}</td><td>{!! $spot->currency !!}{{ $spot->price_extra }}</td>
                    </tr>
                    @endif
                    @if($spot->shipping_cost != 0.00)
                    <tr>
                        <td class="bold">{!! __('spots.shipping') !!}</td><td>{!! $spot->currency !!}{{ $spot->shipping_cost }}</td>
                    </tr>
                    @endif
                    @if($spot->offer)
                    <tr>
                        <td class="bold">{!! __('spots.offer') !!}</td><td>{!! $spot->offer !!}</td>
                    </tr>
                    @endif
                    <tr>
                        <td class="bold">{!! __('spots.length') !!}</td><td>:{!! $spot->length !!}</td>
                    </tr>
                    <tr>
                        <td class="bold">{!! __('spots.category') !!}</td><td>@if($spot->category){!! $spot->category->name !!},@endif @if($spot->subCategory){!! $spot->subCategory->name !!}@endif</td>
                    </tr>
                    @if($spot->marketing_company_id > 0 and $spot->marketingCompany->name)
                    <tr>
                        <td class="bold">{!! __('spots.marketing_company') !!}</td>
                        <td>
                            <a href="{!! route("companies.show", array($spot->marketingCompany->id)) !!}">{!! $spot->marketingCompany->name !!}</a>  @if(Sentry::getUser()->isSuperUser()) <small><a href="{!! route("admin.companies.edit",array($spot->marketingCompany->id)) !!}" target="_blank">Edit</a></small> @endif
                        </td>
                    </tr>
                    @endif
                    @if($spot->phone)
                    <tr>
                        <td class="bold">{!! __('spots.phone') !!}</td><td>{!! $spot->phone !!}</td>
                    </tr>
                    @endif
                    @if($spot->website)
                    <tr>
                        <td class="bold">{!! __('spots.website') !!}</td><td>{!! makeClickable($spot->website) !!}</td>
                    </tr>
                    @endif
                    @if($spot->channel)
                        <tr>
                            <td class="bold">{!! __('programs.channel') !!}</td><td>{!! $spot->channel->name !!}</td>
                        </tr>
                        @endif
                    @if($spot->initial_date)
                    <tr>
                        <td class="bold">{!! __('spots.week_ending') !!}</td>
                        <td>
                            {!! date("m/d/Y",strtotime($spot->air_date)) !!}
                        </td>
                    </tr>
                    @elseif($spot->air_date)
                    <tr>
                        <td class="bold">{!! __('spots.air_date') !!}</td>
                        <td>
                            {!! date("m/d/Y h:i:s A",strtotime($spot->air_date)) !!}
                        </td>
                    </tr>
                    @endif
                    </table>
                @if(Sentry::getUser()->hasAccess('reports.network-details'))

                    <p class="text-center">
                        @if(Sentry::getUser()->hasAccess('reports.spot-scheduler')) {!! Html::linkRoute('spots.schedule','Spot Scheduler',$spot->id, array('class'=>'btn btn-blue')) !!}@endif

                        {{-- <a class="btn btn-warning" href="#"><i class="fa fa-plus-circle"></i>&nbsp;Add To Favorites</a> --}}

                    </p>
                @endif
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Spot Video
                    </div>
                </div>
                <div class="panel-body">
                @if(Sentry::getUser()->hasAccess('reports.program-video'))


                    @if($spot->file_name)
                    <video id="spot-video" class="video-js vjs-default-skin hidden-xs" controls preload="none" width="100%" height="324"
                    @if(isset($signed_poster))
                        poster="{!! $signed_poster !!}"
                    @else
                        poster="/assets/img/video-poster.png"
                    @endif
                    data-setup="{}">
                    @if(isset($signed_flv))
                        <source src="{!! $signed_flv !!}" type='video/flv' />
                    @endif
                    @if(isset($signed_mp4))
                        <source src="{!! $signed_mp4 !!}" type='video/mp4' />
                    @endif
                    </video>
                        <p class="text-center">
                        @if(isset($signed_flv))
                            <a href="{!! $signed_flv !!}" target="_blank" class="btn btn-info btn-icon icon-left"><i class="fa fa-download"></i> Click to Download</a>
                        @elseif(isset($signed_mp4))
                            <a href="{!! $signed_mp4 !!}" target="_blank" class="click-download btn btn-info btn-icon icon-left"><i class="fa fa-download"></i> Click to Download</a>
                        @endif
                        </p>
                    @else
                        <div class="no_data">Video not available.</div>
                    @endif
                @endif
                </div>
            </div>
        </div>
    </div>
        <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Spot Description
                    </div>
                </div>
                <div class="panel-body">
                    @if($spot->description)
                        {!! $spot->description !!}
                    @else
                        <div class="no_data">No description available.</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('cxp.partials.modals.updateDescription')
    <div class="row">
        <div class="col-xs-offset-5">
            <div class="panel">
                <a data-toggle="modal" href="#updatePageDescriptionModal" class="btn btn-info">
                    <i class="fa fa-envelope-o"></i> Request an Update</a>
            </div>
        </div>
    </div>
    @if($spot->remarks)
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Spot Summary
                    </div>
                </div>
                <div class="panel-body">
                    @if($spot->remarks)
                        {!! $spot->remarks !!}
                    @else
                        <div class="no_data">No summary available.</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Spot Versions
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th class="col-xs-5">Title</th>
                            <th class="col-xs-1 hidden-xs">Version</th>
                            <th class="col-xs-1 hidden-xs">Length</th>
                            <th class="col-xs-3">First Detected</th>
                            <th class="col-xs-3 hidden-xs hidden-sm">Last Detection</th>
                            <th class="col-xs-1"></th>
                        </thead>
                    @foreach($spotVersions as $version)
                        <tr>
                            <td>
                                {!! $version->title !!}
                            </td>
                            <td class="hidden-xs">
                                {!! $version->version !!}
                            </td>
                            <td class="hidden-xs">
                                :{!! $version->length !!}
                            </td>
                            <td>
                                {!! date("m/d/Y", strtotime($version->initial_date)) !!}
                            </td>
                            <td class="hidden-xs hidden-sm">
                            @if(is_null($version->last_detected))
                                N/A
                            @else
                                {!! date("m/d/Y", strtotime($version->last_detected)) !!} <br>
                                {!! date("h:i a", strtotime($version->last_detected)) !!}
                            @endif
                            </td>
                            <td>
                            @if($version->id == $spot->id)
                                <a href="{!! route('spots.show', array($version->id)) !!}" class="btn btn-green btn-icon icon-left disabled"><i class="fa fa-eye"></i> View</a>
                            @else
                                <a href="{!! route('spots.show', array($version->id)) !!}" class="btn btn-green btn-icon icon-left"><i class="fa fa-eye"></i> View</a>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        Week Ending: {!! $week_ending->format("F j, Y") !!} (All Versions)
                    @if(Sentry::getUser()->hasAccess('reports.network-details'))
                        <small style="float:right;">Networks With Airings: {!! $totalChannels->channels !!}</small>
                    @endif
                    </div>
                </div>
                @if(Sentry::getUser()->hasAccess('reports.network-details'))
                <div class="panel-body">
                    @if($totalChannels->channels > 0)
                    <table class="table table-hover table-bordered networks">
                        <thead>
                            <th>Network</th>
                            <th style="text-align:center;">Frequency</th>
                        </thead>
                        <tbody>
                        <?php $count = 0; ?>
                        @foreach($channelList as $channel)
                            <tr>
                            <td>
                            @if($channel->logo_location)
                                <img src="https://s3-us-west-2.amazonaws.com/ims-logos/{!! $channel->logo_location !!}" class="network-img" />
                                <p class="text-center">{!! $channel->abbr !!}</p>
                            @else
                                <p class="text-center">{!! $channel->abbr !!}</p>
                            @endif
                            </td>
                            <td class="count text-center">{!! $channel->chancount !!}</td>
                            </tr>
                            <?php $count ++;?>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                        <div class="no_data">Network details are unavailable for this spot, because it did not air in the week ending on {!! $week_ending->format("F j, Y") !!}.</div>
                    @endif
                    <p class="text-center"> @if(Sentry::getUser()->hasAccess('reports.spot-scheduler')) {!! Html::linkRoute('spots.schedule','Spot Scheduler',$spot->id, array('class'=>'btn btn-blue')) !!}@endif</p>
                </div>
            @else
                <div class="panel-body">
                    <p>Network details are unavailable with your current plan. {!! Html::linkRoute('upgrade','Upgrade your plan today.') !!}</p>
                </div>
            @endif
            </div>
        </div>
    </div>
@stop
@section('script')
<script>
@if($overTime)
    var overtime = AmCharts.makeChart("show-trend", {
        "type": "serial",
        "theme": "none",
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "dataProvider": {!! $overTime->toJson() !!},
        "legend": {
            "useGraphSettings": true
        },
        "valueAxes": [{
            "id":"v1",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "position": "left",
            "title": "Frequency"
        }, {
            "id":"v2",
            "axisThickness": 2,
            "gridAlpha": 0,
            "axisAlpha": 1,
            "maximum": 250,
            "minimum": 1,
            "reversed": true,
            "position": "right",
            "title": "Frequency Rank"
        }],
        "graphs": [{
            "valueAxis": "v1",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Frequency",
            "valueField": "airs"
        },{
            "valueAxis": "v2",
            "balloonText": "[[title]]: [[value]]",
            "lineThickness": 2,
            "title": "Frequency Rank",
            "valueField": "rank"
        }],
        "zoomOutButtonRollOverAlpha": 0.15,
        "chartCursor": {
            "categoryBalloonDateFormat": "MMM DD JJ:NN",
            "cursorPosition": "mouse",
            "showNextAvailable":true
        },
        "autoMarginOffset": 5,
        "columnWidth": 1,
        "categoryField": "week_ending",
        "categoryAxis": {
            "minPeriod": "hh",
            "parseDates": true
        },
        "amExport": {
            top : 0,
            right : 150,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
$('.modal').appendTo('body');
@endif
    videojs.options.flash.swf = "{!! asset('assets/swf/video-js.swf') !!}";

    var oTable;
    $(function(){
      oTable = $('.networks').dataTable( {
        "order": [[ 1, "desc" ]]
      });
    });
</script>
@stop
