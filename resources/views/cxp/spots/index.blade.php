@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-tint"></i>
        Spots &bull; {!! $language->name !!}
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('spots') !!}
    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center">
                <i class="fa fa-tint"></i>
                New Spots  - {!! $week_ending->format("F j, Y") !!}</h4>
            <div class="date-selection text-center">
            <form class="form-inline" id="week_ending_frm" style="display:inline-block;">
                <input type="hidden" name="language_id" value="{!! $language_id !!}" />
                {!! Former::xlarge_text('week_ending','')->id('pickadate')->class('span2 form-control')->placeholder($week_ending->format("F j, Y")) !!}
                <button class="select btn btn-blue btn-icon">Select Week Ending <i class="fa fa-calendar"></i></button>
            </form>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        @if($spots->count() > 0)
            <table id="report" class="table table-hover table-bordered results">
                <thead>
                    <th>Title</th>
                    <th class="col-xs-1">Ver.</th>
                    <th class="col-xs-1">TRT</th>
                    <th class="col-xs-1">Price</th>
                    <th>Marketing Co.</th>
                    <th>Category</th>
                    <th>Sub-Category</th>
                    <th>First Detected</th>
                </thead>
                <tbody>
                @foreach($spots as $spot)
                    <tr>
                        <td>{!! Html::linkRoute('spots.show',$spot->title, $spot->id) !!}</td>
                        <td>{!! $spot->version !!}</td>
                        <td>:{!! $spot->length !!}</td>
                        <td>${!! $spot->price !!}</td>
                        <td>
                        @if($spot->marketingCompany)
                            {!! $spot->marketingCompany->name !!}
                        @endif
                        </td>
                        <td>
                        @if($spot->category)
                            {!! $spot->category->name !!}
                        @endif
                        </td>
                        <td>
                        @if($spot->subcategory)
                            {!! $spot->subcategory->name !!}
                        @endif
                        </td>
                        <td>
                        @if(is_null($spot->initial_date))
                            {!! date("m/d/Y",strtotime($spot->air_date)) !!}
                        @else
                            {!! date("m/d/Y",strtotime($spot->initial_date)) !!}
                        @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="no_data">No spots this week yet...</div>
        @endif
        </div>
    </div>
@stop

@section('script')
<script>
var oTable;
$(function(){
  oTable = $('#report').dataTable( {
    "pageLength": 100,
    "lengthMenu": [[10, 25, 50, 100, 250, 500], [10, 25, 50, 100, 250, 500]],
    "columns": [
      null,
      null,
      null,
      null,
      @if ($language_id != 1) { "visible": false }, @else null, @endif
      null,
      null,
      null
    ],
    responsive: true
  });
});
</script>
@stop
