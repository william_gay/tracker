@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-signal"></i>
        Reports
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('reports') !!}
    <div class="row" style="margin-top:20px;">
        <div class="col-sm-6">
            <!-- National Long Form -->
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        National Cable Long Form
                    </div>
                </div>
                <div class="panel-body">
                    <div class="main-stats">
                        <div class="row stats-row">
                            <div class="col-sm-6 stat text-center">
                                <h2>
                                    <i class="fa fa-search"></i>
                                    @if(Sentry::getUser()->hasAccess('reports.national-shows'))
                                        {!! link_to_route('programs.all', 'Show Search', array('language_id' => 1)) !!}
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">Show Search</a>
                                    @endif

                                </h2>
                            </div>
                            <div class="col-sm-6 stat text-center">
                                <h2>
                                    <i class="fa fa-calendar"></i>
                                    @if(Sentry::getUser()->hasAccess('reports.national-shows-new'))
                                        <a href="/programs/new">{!! getNewPrograms() !!} New Shows</a>
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! getNewPrograms() !!} New Shows</a>
                                    @endif
                                </h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="list-group-item text-center">Annual</h3>

                            @foreach($reportsMenu->long_form as $report)
                                @if($report['interval'] == 'yearly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                        {!! $report['name'] !!}
                                    </a>
                                    @else
                                    <a href="/reports/show/{!! $report['id'] !!}" class="list-group-item">
                                        @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                        <span class="badge badge-secondary badge-roundless">New</span>
                                        @endif
                                        {!! $report['name'] !!}
                                    </a>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="list-group">
                                <h3 class="list-group-item text-center">Weekly</h3>

                                @if(Sentry::getUser()->hasAccess('reports.category-report'))
                                <a class="list-group-item" href="/categories/long-form">Category Report</a>
                                @endif
                            @foreach($reportsMenu->long_form as $report)
                                @if($report['interval'] == 'weekly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                        {!! $report['name'] !!}
                                    </a>
                                    @else
                                    <a href="/reports/show/{!! $report['id'] !!}" class="list-group-item">
                                        @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                        <span class="badge badge-secondary badge-roundless">New</span>
                                        @endif
                                        {!! $report['name'] !!}
                                    </a>
                                    @endif
                                @endif
                            @endforeach
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="list-group">
                                <h3 class="list-group-item text-center">Monthly</h3>

                            @foreach($reportsMenu->long_form as $report)
                                @if($report['interval'] == 'monthly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                        {!! $report['name'] !!}
                                    </a>
                                    @else
                                    <a class="list-group-item" href="/reports/show/{!! $report['id'] !!}">
                                        @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                        <span class="badge badge-secondary badge-roundless">New</span>
                                        @endif
                                        {!! $report['name'] !!}
                                    </a>
                                    @endif
                                @endif
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END National Long Form -->
            <!-- Spanish Long Form -->
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        National Cable Spanish Long Form
                    </div>
                </div>
                <div class="panel-body">
                    @if(Sentry::getUser()->hasAccess('reports.spanish-spot-search-new'))
                    <div class="main-stats">
                        <div class="row stats-row">
                            <div class="col-sm-6 stat text-center">
                                <h2>
                                    <i class="fa fa-search"></i>
                                    @if(Sentry::getUser()->hasAccess('reports.spanish-shows'))
                                        {!! link_to_route('programs.all', 'Show Search', array('language_id' => 2)) !!}
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">Show Search</a>
                                    @endif
                                </h2>
                            </div>
                            <div class="col-sm-6 stat text-center">
                                <h2>
                                    <i class="fa fa-calendar"></i>
                                    @if(Sentry::getUser()->hasAccess('reports.spanish-shows-new'))
                                        {!! link_to_route('programs.new', getNewPrograms(2) .' New Shows', array('language_id' => 2)) !!}
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">getNewPrograms(2) New Shows</a>
                                    @endif
                                </h2>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="list-group">
                                <h3 class="list-group-item text-center">Weekly</h3>

                                @foreach($reportsMenu->spanish_long_form as $report)
                                    @if($report['interval'] == 'weekly')
                                        @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                            <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                                {!! $report['name'] !!}
                                            </a>
                                            @else
                                            <a class="list-group-item"  href="/reports/show/{!! $report['id'] !!}">
                                                @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                                <span class="badge badge-secondary badge-roundless">New</span>
                                                @endif
                                                {!! $report['name'] !!}
                                            </a>
                                            @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="list-group">
                                <h3 class="list-group-item text-center">Monthly</h3>

                                @foreach($reportsMenu->spanish_long_form as $report)
                                    @if($report['interval'] == 'monthly')
                                        @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                        <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                            {!! $report['name'] !!}
                                        </a>
                                        @else
                                        <a class="list-group-item"  href="/reports/show/{!! $report['id'] !!}">
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                            {!! $report['name'] !!}
                                        </a>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Spanish Long Form -->
        </div>
        <!-- END Long Form -->
        <!-- Short Form -->
        <div class="col-sm-6">
            <!-- National Short -->
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        National Cable Short Form
                    </div>
                </div>
                <div class="panel-body">
                    <div class="main-stats">
                        <div class="row stats-row">
                            <div class="col-sm-6 stat text-center">
                                <h2>
                                    <i class="fa fa-search"></i>
                                    @if(Sentry::getUser()->hasAccess('reports.national-spots'))
                                        <a href="/spots">Spot Search</a>
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">Spot Search</a>
                                    @endif
                                </h2>
                            </div>
                            <div class="col-sm-6 stat text-center">
                                <h2>
                                    <i class="fa fa-calendar"></i>
                                    @if(Sentry::getUser()->hasAccess('reports.national-spots-new'))
                                        <a href="/spots/new">{!! getNewSpots() !!} New Spots</a>
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">{!! getNewSpots() !!} New Spots</a>
                                    @endif
                                </h2>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="list-group-item text-center">Annual</h3>

                            @foreach($reportsMenu->short_form as $report)
                                @if($report['interval'] == 'yearly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                        {!! $report['name'] !!}
                                    </a>
                                    @else
                                    <a href="/reports/show/{!! $report['id'] !!}" class="list-group-item">
                                        @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                        <span class="badge badge-secondary badge-roundless">New</span>
                                        @endif
                                        {!! $report['name'] !!}
                                    </a>
                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="list-group">
                                <h3 class="list-group-item text-center">Weekly</h3>

                                @if(Sentry::getUser()->hasAccess('reports.category-report'))
                                <a class="list-group-item" href="/categories/short-form">Category Report</a>
                                @endif
                            @foreach($reportsMenu->short_form as $report)
                                @if($report['interval'] == 'weekly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']) and $report['id'] != 72)
                                    <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                        {!! $report['name'] !!}
                                    </a>
                                    @else
                                        @if($report['id'] == 72 and Sentry::getUser()->isSuperUser())
                                        <a class="list-group-item" href="/reports/show/{!! $report['id'] !!}">
                                            <span class="badge badge-secondary badge-roundless">Admin Only</span>
                                            {!! $report['name'] !!}
                                        </a>
                                        @elseif($report['id'] != 72)
                                            <a class="list-group-item" href="/reports/show/{!! $report['id'] !!}">
                                                @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                                <span class="badge badge-secondary badge-roundless">New</span>
                                                @endif
                                                {!! $report['name'] !!}
                                            </a>
                                        @endif
                                    @endif
                                @endif
                            @endforeach
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="list-group">
                                <h3 class="list-group-item text-center">Monthly</h3>

                            @foreach($reportsMenu->short_form as $report)
                                @if($report['interval'] == 'monthly')
                                    @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                    <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                        {!! $report['name'] !!}
                                    </a>
                                    @else
                                    <a class="list-group-item"  href="/reports/show/{!! $report['id'] !!}">
                                        @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                        <span class="badge badge-secondary badge-roundless">New</span>
                                        @endif
                                    {!! $report['name'] !!}
                                    </a>
                                    @endif
                                @endif
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END National Short -->
            <!-- Spanish Short -->
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        National Cable Spanish Short Form
                    </div>
                </div>
                <div class="panel-body">
                    @if(Sentry::getUser()->hasAccess('reports.spanish-spot-search-new'))
                    <div class="main-stats">
                        <div class="row stats-row">
                            <div class="col-sm-6 stat text-center">
                                <h2>
                                    <i class="fa fa-search"></i>
                                    @if(Sentry::getUser()->hasAccess('reports.spanish-spots'))
                                        {!! link_to_route('spots.all', 'Spot Search', array('language_id' => 2)) !!}
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">Spot Search</a>
                                    @endif
                                <h2>
                            </div>
                            <div class="col-sm-6 stat text-center">
                                <h2>
                                    <i class="fa fa-calendar"></i>
                                    @if(Sentry::getUser()->hasAccess('reports.spanish-spots-new'))
                                        {!! link_to_route('spots.new', getNewSpots(2) .' New Spots', array('language_id' => 2)) !!}
                                    @else
                                        <a data-toggle="modal" data-target="#upgradeModal">getNewSpots(2) New Spots</a>
                                    @endif
                                </h2>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="list-group">
                                <h3 class="list-group-item text-center">Weekly</h3>

                                @foreach($reportsMenu->spanish_short_form as $report)
                                    @if($report['interval'] == 'weekly')
                                        @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                        <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                            {!! $report['name'] !!}
                                        </a>
                                        @else
                                        <a class="list-group-item"  href="/reports/show/{!! $report['id'] !!}">
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                            {!! $report['name'] !!}
                                        </a>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="list-group">
                                <h3 class="list-group-item text-center">Monthly</h3>

                                @foreach($reportsMenu->spanish_short_form as $report)
                                    @if($report['interval'] == 'monthly')
                                        @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                        <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                            {!! $report['name'] !!}
                                        </a>
                                        @else
                                        <a class="list-group-item"  href="/reports/show/{!! $report['id'] !!}">
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                            {!! $report['name'] !!}
                                        </a>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END Spanish Short -->
    @if(Sentry::getUser()->hasAccess('reports.australia'))
            <!-- AUS Short -->
            <div class="panel panel-gradient">
                <div class="panel-heading">
                    <div class="panel-title">
                        Australia Short Form
                    </div>
                </div>
                <div class="panel-body">
                    @if(Sentry::getUser()->hasAccess('reports.aus-spot-search-new'))
                    <div class="main-stats">
                        <div class="row stats-row">
                            <div class="col-sm-6 stat text-center">
                                <h2>
                                    <i class="fa fa-search"></i> {!! link_to_route('spots.all', 'Spot Search', array('language_id' => 4)) !!}
                                </h2>
                            </div>
                            <div class="col-sm-6 stat text-center">
                                <h2>
                                    <i class="fa fa-calendar"></i> {!! link_to_route('spots.new', getNewSpots(4) .' New Spots', array('language_id' => 4)) !!}
                                </h2>
                            </div>
                        </div>
                    </div>
                    @endif


                    <div class="row">
                        <div class="col-xs-6">
                            <div class="list-group">
                                <h3 class="list-group-item text-center">Weekly</h3>

                                @foreach($reportsMenu->aus_short_form as $report)
                                    @if($report['interval'] == 'weekly')
                                        @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                        <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                            {!! $report['name'] !!}
                                        </a>
                                        @else
                                        <a class="list-group-item"  href="/reports/show/{!! $report['id'] !!}">
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                            {!! $report['name'] !!}
                                        </a>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="list-group">
                               <h3 class="list-group-item text-center">Monthly</h3>

                                @foreach($reportsMenu->aus_short_form as $report)
                                    @if($report['interval'] == 'monthly')
                                        @if( ! Sentry::getUser()->hasAccess('reports.'.$report['slug']))
                                        <a data-toggle="modal" data-target="#upgradeModal" class="list-group-item">
                                            {!! $report['name'] !!}
                                        </a>
                                        @else
                                        <a class="list-group-item"  href="/reports/show/{!! $report['id'] !!}">
                                            @if($report['created_at']->gte(Carbon\Carbon::now()->subMonth(4)))
                                            <span class="badge badge-secondary badge-roundless">New</span>
                                            @endif
                                            {!! $report['name'] !!}
                                        </a>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END AUS Short -->
    @endif
        </div>
        <!-- END Short Form -->
    </div>
@stop

