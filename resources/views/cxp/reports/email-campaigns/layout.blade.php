<!DOCTYPE html>
<!--[if lt IE 7]>       <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>          <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>          <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
    <script src="//cdn.optimizely.com/js/1705360368.js"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{!! Config::get('app.site_config.description') !!}">
    <meta name="viewport" content="width=device-width">

    <title>Media Analytics | Email Campaign</title>

    @include('cxp.partials.favicon')
    @include('cxp.partials.assets.styles')

    @yield('style')

    <!-- Load Google JavaScript API -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!--[if lt IE 9]>
        {!! Html::script("assets/js/ie8-responsive-file-warning.js") !!}
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        {!! Html::script("assets/js/excanvas.js") !!}
    <![endif]-->

    @include('cxp.partials.bugsnag')
</head>
<body class="page-body {!! Route::is( 'dashboard') ? 'page-fade' : '' !!} " data-url="https://my.imsreport.com">

<!-- .page-container -->
<div class="page-container horizontal-menu sidebar-collapsed">

    <!-- We still need to update this -->
    @include('cxp.reports.email-campaigns.partials.topbar')


<!-- .main-content -->
<div class="main-content">


        <div class="row">
            <div class="col-sm-12">
                <div class="hidden-lg well">
                    @yield('header')
                </div>
            </div>
        </div>

        <br />
            @include('cxp.partials.alert')
            @yield('content')

<br>
<!-- Footer -->
<footer class="main">
    <div class="col-xs-6 text-left">
        <h4>&copy; {!! date('Y') !!} <strong>Media Analytics, LLC</strong></h4>
    </div>
    <div class="col-xs-6 text-right">
        <h3><a href="https://twitter.com/IMSReport" target="_blank"><i class="fa fa-twitter-square"></i></a>&nbsp;&nbsp;
        <a href="https://www.linkedin.com/company/3059380" target="_blank"><i class="fa fa-linkedin-square"></i></a>&nbsp;&nbsp;
        {{-- <a href="#" target="_blank"><i class="fa fa-facebook-square"></i></a></h3> --}}
    </div>
</footer>

</div>
<!-- /.main-content -->

    <!-- /#welcomeModal -->
        @include('cxp.partials.assets.scripts')

        @yield('script')

        @if( App::environment() != 'local' )
        <script
            src="//d2s6cp23z9c3gz.cloudfront.net/js/embed.widget.min.js"
            data-position="top right"
            data-domain="imsreport.besnappy.com"
            data-contact="1"
            data-title="Support"
            data-background="#ffa812">
        </script>
        @endif

    @include('analytics')

</body>
</html>
