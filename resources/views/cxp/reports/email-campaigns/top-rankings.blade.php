<table id="report" class="table table-hover table-bordered results">
    <thead>
        <tr>
            <th>Rank</th>
            <th>Title</th>
            <th>Company</th>
            <th>Category</th>
            <th>Airs</th>
            <th>Index</th>
        </tr>
    </thead>
    <tbody>
    <?php $count = 1; ?>
    @foreach($data as $dat)
        <tr>
            <td>{!! $count !!}</td>
            <td>
                {!! Html::linkRoute('programs.show',$dat->programVersion->grid_title, $dat->program_version_id) !!}
            </td>
            <td>{!! $dat->programVersion->marketingCompany->name ?? '' !!}</td>
            <td>{!! $dat->programVersion->category->name ?? '' !!}</td>
            <td>{!! $dat->airs !!}</td>
            <td>{!! number_format($dat->media_index,2, '.','') !!}</td>
        </tr>
        <?php $count++; ?>
    @endforeach
    </tbody>
</table>
