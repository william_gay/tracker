@extends('cxp.reports.email-campaigns.layout')

@section('header')
    <h3>
        <i class="fa fa-signal"></i>
        Reports
    </h3>
@stop

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
                <p>
                    <strong>Not a member?</strong> <a href="{!! route('user.register') !!}">Sign up now</a> to receive the free IMS Top 25 Monthly Report for Long and Short Form. Stay in the know with the latest DRTV and retail trends, updates on our existing software and first hand looks into new developments from Media Analytics!
                </p>
        </div>
    </div>
@stop

@section('script')
<script>
</script>
@stop
