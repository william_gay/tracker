<table id="report" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th class="span1">Rank</th>
            <th class="span3">Product Name</th>
            <th class="span2">Marketing Company</th>
            <th class="span1">Price</th>
            <th class="span1">S/H</th>
            <th class="span2">Category</th>
            <th class="span2">Sub-Category</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $dat)
        <tr>
            <td>{!! $loop->iteration !!}</td>
            <td>
                {!! Html::linkRoute('spots.show',$dat->spotVersion->title, $dat->spot_version_id) !!}
            </td>
            <td>
                {!! $dat->spotVersion->marketingCompany->name ?? 'N/A' !!}
            </td>
            <td>
            @if($dat->spotVersion->price)
                $ {!! number_format($dat->spotVersion->price, 2) !!}
            @endif
            </td>
            <td>
            @if($dat->spotVersion->shipping_cost)
                $ {!! number_format($dat->spotVersion->shipping_cost, 2) !!}
            @endif
            </td>
            <td>
                {!! $dat->spotVersion->category->name ?? '' !!}
            </td>
            <td>
                {!! $dat->spotVersion->subCategory->name ?? '' !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
