<table id="report" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>This Week</th>
            <th>Power Ranking</th>
            <th>Streak</th>
            <th class="col-xs-3">Product Name</th>
            <th class="col-xs-3">Marketing Company</th>
            <th></th>
            <th class="col-xs-1">Price</th>
            <th class="col-xs-1">S/H</th>
            <th class="col-xs-3">Category</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $dat)
        <tr>
            <td>{!! $dat->rank !!}</td>
            <td class="center">
            <span title="{!! $dat->prev_freq_rank !!}">
            @if($dat->streak != 1)
                @if($dat->rank > $dat->prev_rank)
                    <i class="fa fa-chevron-down" style="color: red;"></i> {!! $dat->rank - $dat->prev_rank  !!}
                @elseif($dat->rank < $dat->prev_rank)
                    <i class="fa fa-chevron-up" style="color: green;"></i> {!! $dat->prev_rank - $dat->rank !!}
                @endif
            @else
                -
            @endif
            </span>
            </td>
            <td>{!! $dat->streak !!}</td>
            <td>
            @if($dat->spot)
                {!! Html::linkRoute('spots.show',$dat->spot->title,$dat->spot_version_id) !!}
            @endif
            </td>
            <td>
            @if($dat->spot and $dat->spotVersion->marketingCompany)
                {!! $dat->spotVersion->marketingCompany->name !!}
            @endif
            </td>
            <td>
                <span title="{!! $dat->prev_rank !!}">
                @if($dat->prev_rank > $report->limit)
                    <i class='fa fa-trophy text-center'></i>
                @endif
                </span>
            </td>
            <td>
            @if($dat->spot and $dat->spotVersion->price)
                $ {!! number_format($dat->spotVersion->price,2) !!}
            @endif
            </td>
            <td>
            @if($dat->spot and $dat->spotVersion->shipping_cost)
                $ {!! number_format($dat->spotVersion->shipping_cost,2) !!}
            @endif
            </td>
            <td>
            @if($dat->spot and $dat->spotVersion->category)
                {!! $dat->spotVersion->category->name !!}
            @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
