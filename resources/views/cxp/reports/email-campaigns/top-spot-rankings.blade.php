<table id="report" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Rank</th>
            <th>Product Name</th>
            <th>Marketing Company</th>
            <th>Price</th>
            <th>S/H</th>
            <th>Category</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $dat)
        <tr>
            <td>{!! $loop->iteration !!}</td>
            <td>
                {!! Html::linkRoute('spots.show',$dat->spotVersion->title,$dat->spot_version_id) !!}
            </td>
            <td>
                @if ($dat->spotVersion->marketingCompany)
                {!! $dat->spotVersion->marketingCompany->name !!}
                @endif
            </td>
            <td>
            @if($dat->spotVersion->price)
                $ {!! number_format($dat->spotVersion->price,2) !!}
            @endif
            </td>
            <td>
            @if($dat->spotVersion->shipping_cost)
                $ {!! number_format($dat->spotVersion->shipping_cost,2) !!}
            @endif
            </td>
            <td>{!! $dat->spotVersion->category->name !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>
