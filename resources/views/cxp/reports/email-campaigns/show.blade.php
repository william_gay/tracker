@extends('cxp.reports.email-campaigns.layout')

@section('header')
    <h3>
        <i class="fa fa-signal"></i>
        Reports
    </h3>
@stop

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div id="loading"></div>
            <div id="grid-message"></div>
        	@include('cxp.reports.email-campaigns.'.$report->template)
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <p class="text-center"><a href="https://my.imsreport.com/user/register" target="_blank">Click here for more insight on the top products OR new shows and spots!</a></p>
        </div>
    </div>
@stop

@section('script')
<script>
var report_length = {!! $report->limit!!};
{{-- $report->js --}}
</script>
@stop
