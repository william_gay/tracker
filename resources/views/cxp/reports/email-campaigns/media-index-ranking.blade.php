<table id="report" class="table table-hover table-bordered results">
    <thead>
        <tr>
            <th>This Week</th>
            <th>Power Ranking</th>
            <th>Streak</th>
            <th>Title</th>
            <th></th>
            <th>Company</th>
            <th>Category</th>
            <th>Airs</th>
            <th>Index</th>
            <th>Stations</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $dat)
        <tr>
            <td>{!! $dat->media_rank !!}</td>
            <td class="center">
            <span title="{!! $dat->prev_media_rank !!}">
            @if($dat->streak != 1)
                @if($dat->media_rank > $dat->prev_media_rank)
                    <i class="fa fa-chevron-down" style="color: red;"></i> {!! $dat->media_rank - $dat->prev_media_rank  !!}
                @elseif($dat->media_rank < $dat->prev_media_rank)
                    <i class="fa fa-chevron-up" style="color: green;"></i> {!! $dat->prev_media_rank - $dat->media_rank !!}
                @endif
            @else
                -
            @endif
            </span>
            </td>
            <td>{!! $dat->streak !!}</td>
            <td>
            @if($dat->program)
                {!! Html::linkRoute('programs.show',$dat->program->title,$dat->program_version_id) !!}
            @endif
            </td>
            <td class="center">
            <span title="{!! $dat->prev_media_rank !!}">
            @if($dat->prev_media_rank > $report->limit)
                <i class='fa fa-trophy text-center'></i>
            @endif
            </span>
            </td>
            <td>
            @if($dat->program->programVersions()->latest('initial_date')->first())
                {!!  $dat->program->programVersions()->latest('initial_date')->first()->marketingCompany->name ?? 'N/A' !!}
            @else
                N/A
            @endif
            </td>
            <td>
            @if($dat->program)
                @if($dat->program->category)
                    {!! $dat->program->category->name !!}
                @endif
            @endif
            </td>
            <td>{!! $dat->airs !!}</td>
            <td>{!! number_format($dat->media_index,2, '.','') !!}</td>
            <td>{!! $dat->stations !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>
