<!-- Top Header Navbar -->
<header class="navbar navbar-fixed-top"><!-- set fixed position by adding class "navbar-fixed-top" -->
		<div class="navbar-inner">
			<!-- logo -->
			<div class="navbar-brand" id="navbar-brand">
				<a class="brand" href="{!! URL::to('dashboard') !!}">
					<img src="{!! asset('assets/img/logo.png') !!}" alt="Media Analytics Dashboard"/>
				</a>
			</div>

			<h3 class="text-left">
				{!! $report->name ?? '' !!}
				@if(isset($week_ending))
					&bull;
					{!! $week_ending->format("F j, Y") !!}
				@endif
				@if(isset($month))
					&bull;
					{!! $month->format("F Y") !!}
				@endif
			</h3>
		</div>
</header>

	<!-- END Top Header Navbar -->