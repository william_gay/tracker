<table id="report" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-xs-1">This Week</th>
            <th class="col-xs-1">Power Ranking</th>
            <th class="col-xs-1">Airings Streak</th>
            <th class="col-xs-3">Product Name</th>
            <th class="col-xs-2">Marketing Company</th>
            <th class="col-xs-1"></th>
            <th class="col-xs-1">Price</th>
            <th class="col-xs-1">S/H</th>
            <th class="col-xs-2">Category</th>
            @if(Sentry::getUser()->isSuperUser())
            <th>Media Index <small><i class="fa fa-lock"></i></small></th>
            <th class="span1">Airs  <small><i class="fa fa-lock"></i></small></th>
            @endif
        </tr>
    </thead>
    <tbody>
    @foreach($data as $dat)
        <tr>
            <td>{!! $dat->rank !!}</td>
            <td class="center">
            <span data-order="{!! $dat->prev_freq_rank !!}">
            @if($dat->streak != 1)
                @if($dat->rank > $dat->prev_rank)
                    <i class="fa fa-chevron-down" style="color: red;"></i> {!! $dat->rank - $dat->prev_rank  !!}
                @elseif($dat->rank < $dat->prev_rank)
                    <i class="fa fa-chevron-up" style="color: green;"></i> {!! $dat->prev_rank - $dat->rank !!}
                @endif
            @else
                -
            @endif
            </span>
            </td>
            <td>{!! $dat->streak !!}</td>
            <td>
            @if($dat->spot)
                {!! Html::linkRoute('spots.show',$dat->spot->title,$dat->spot_version_id) !!}
                @if(Sentry::getUser()->isSuperUser())
                    <small>{!! Html::linkRoute('admin.spots.edit', 'Edit',$dat->spot_version_id)!!}</small>
                @endif
            @endif
            </td>
            <td>
            @if($dat->spot and $dat->spotVersion->marketingCompany)
                {!! $dat->spotVersion->marketingCompany->name !!}
            @endif
            </td>
            <td>
                <span data-order="{!! $dat->prev_rank !!}">
                @if($dat->prev_rank > $report->limit)
                    <i class='fa fa-trophy text-center'></i>
                @endif
                </span>
            </td>
            <td>
            @if($dat->spot and $dat->spotVersion->price)
                $ {!! number_format($dat->spotVersion->price,2) !!}
            @endif
            </td>
            <td>
            @if($dat->spot and $dat->spotVersion->shipping_cost)
                $ {!! number_format($dat->spotVersion->shipping_cost,2) !!}
            @endif
            </td>
            <td>
            @if($dat->spot and $dat->spotVersion->category)
                {!! $dat->spotVersion->category->name !!}
            @endif
            </td>
            @if(Sentry::getUser()->isSuperUser())
            <td>
            @if($dat->media_index > 0)
                {!! $dat->media_index !!}
            @endif
            </td>
            <td>
                {!! $dat->frequency !!}
            </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>

@include('cxp.reports.footnotes')

@section('script')
    @parent
    <script>
        var oTable;
        $(function(){
          oTable = $('#report').dataTable( {
            "pageLength": {!! $report->limit!!}
          });
        });
    </script>
@endsection
