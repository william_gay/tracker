<table id="report" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Product Name</th>
            <th>Marketing Company</th>
            <th>Description</th>
            <th></th>
            <th>TRT</th>
            <th>Price</th>
            <th>S/H</th>
            <th>Phone</th>
            <th>Website</th>
            <th>Category</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $dat)
        <tr>
            <td>
                {!! Html::linkRoute('spots.show',$dat->title,$dat->id) !!}
                @if(Sentry::getUser()->isSuperUser())
                    <small>{!! Html::linkRoute('admin.spots.edit', 'Edit',$dat->id)!!}</small>
                @endif
            </td>
            <td>{!! $dat->marketingCompany->name ?? '' !!}</td>
            <td>{!! $dat->description !!}</td>
            <td>
                <span data-order="{!! $dat->prev_rank !!}">
                @if($dat->prev_rank > $report->limit)
                    <i class='fa fa-trophy text-center'></i>
                @endif
                </span>
            </td>
            <td>{!! ":".$dat->length !!}</td>
            <td data-order="{{ $dat->price }}">$ {!! number_format($dat->price,2) !!}</td>
            <td>{!! $dat->shipping_price !!}</td>
            <td>{!! $dat->phone !!}</td>
            <td>{!! $dat->website !!}</td>
            <td>{!! $dat->category->name !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@section('script')
    @parent
    <script>
        var oTable;
        $(function(){
          oTable = $('#report').dataTable( {
            "page_length": {!! $report->limit!!}
          });
        });
    </script>
@endsection
