@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-signal"></i>
        Category Report
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('report',$report) !!}

    <div class="row">
        <div class="col-xs-12" style="min-height: 800px">
            <iframe src="https://www.periscopedata.com/shared/3a3e571e-661f-486b-bf72-b04bb4eefd76?embed=true" frameBorder="0" width="100%" style="min-height: 1250px;height: 100vh;"></iframe>
        </div>
    </div>
@endsection
