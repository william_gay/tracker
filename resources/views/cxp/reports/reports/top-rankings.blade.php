<table id="report" class="table table-hover table-bordered results">
    <thead>
        <tr>
            <th>Rank</th>
            <th>Title</th>
            <th>Company</th>
            <th>Category</th>
            <th>Sub-Category</th>
            <th>Airs</th>
            <th>Index</th>
        </tr>
    </thead>
    <tbody>
    <?php $count = 1; ?>
    @foreach($data as $dat)
        <tr>
            <td>{!! $count !!}</td>
            <td>
                @if(Sentry::getUser()->hasAccess('reports.national-shows'))
                    {!! Html::linkRoute('programs.show',$dat->programVersion->grid_title, $dat->program_version_id) !!}
                @else
                    {!! $dat->programVersion->grid_title !!}
                @endif
                @if(Sentry::getUser()->isSuperUser())
                    <small>{!! Html::linkRoute('admin.programs.edit', 'Edit', $dat->programVersion->id) !!}</small>
                @endif
            </td>
            <td>{!! $dat->programVersion->marketingCompany->name ?? 'N/A' !!}</td>
            <td>{!! $dat->programVersion->category->name ?? 'N/A' !!}</td>
            <td>{!! $dat->programVersion->subCategory->name ?? 'N/A' !!}</td>
            <td>{!! $dat->airs !!}</td>
            <td>{!! number_format($dat->media_index,2, '.','') !!}</td>
        </tr>
        <?php $count++; ?>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th>Total:</th>
            <th></th>
            <th></th>
        </tr>
    </tfoot>
</table>

@section('script')
    @parent
    <script>
        var oTable;
        $(function(){
          oTable = $('#report').dataTable( {
            "pageLength": {!! $report->limit!!},
            "fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
              var iTotalAirs = 0;
              var iTotalIndex = 0;
              for ( var i=iStart ; i<iEnd ; i++ )
              {
                iTotalAirs += aaData[ aiDisplay[i] ][5]*1;
                iTotalIndex += aaData[ aiDisplay[i] ][6]*1;
              }

              var nCells = nRow.getElementsByTagName('th');
              nCells[5].innerHTML = parseInt(iTotalAirs);
              nCells[6].innerHTML = iTotalIndex.toFixed(2);
            }
          });
        });
    </script>
@endsection
