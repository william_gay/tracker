<table id="report" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th class="span1">Rank</th>
            <th class="span3">Product Name</th>
            <th class="span1"></th>
            <th class="span2">Marketing Company</th>
            <th class="span1">Price</th>
            <th class="span1">S/H</th>
            <th class="span2">Category</th>
            <th class="span2">Sub-Category</th>
            @if(Sentry::getUser()->isSuperUser())
            <th>Media Index <small><i class="fa fa-lock"></i></small></th>
            <th class="span1">Airs  <small><i class="fa fa-lock"></i></small></th>
            @endif
        </tr>
    </thead>
    <tbody>
    @foreach($data as $key => $dat)
        <tr>
            <td>{!! $key+1 !!}</td>
            <td>
                {!! Html::linkRoute('spots.show',$dat->title,$dat->spot_version_id) !!}
                @if(Sentry::getUser()->isSuperUser())
                    <small>{!! Html::linkRoute('admin.spots.edit', 'Edit',$dat->spot_version_id)!!}</small>
                @endif
            </td>
            <td>
                <span data-order="{!! $dat->prev_rank !!}">
                @if($dat->prev_rank > $report->limit)
                    <i class='fa fa-trophy text-center'></i>
                @endif
                </span>
            </td>
            <td>
                {!! $dat->marketingCompany !!}
            </td>
            <td data-order="{{ $dat->price }}">
            @if($dat->price)
                $ {!! number_format($dat->price,2) !!}
            @endif
            </td>
            <td data-order="{{ $dat->shipping_cost }}">
            @if($dat->shipping_cost)
                $ {!! number_format($dat->shipping_cost,2) !!}
            @endif
            </td>
            <td>
                {!! $dat->category !!}
            </td>
            <td>
                {!! $dat->subcategory !!}
            </td>
            @if(Sentry::getUser()->isSuperUser())
            <td data-order="{{ $dat->media_index }}">
            @if($dat->media_index > 0)
                {!! $dat->media_index !!}
            @endif
            </td>
            <td>
                {!! $dat->airings !!}
            </td>
            @endif
        </tr>
    @endforeach
    </tbody>
</table>

@include('cxp.reports.footnotes')

@section('script')
    @parent
    <script>
        var oTable;
        $(function(){
          oTable = $('#report').dataTable( {
            "pageLength": {!! $report->limit!!}
          });
        });
    </script>
@endsection
