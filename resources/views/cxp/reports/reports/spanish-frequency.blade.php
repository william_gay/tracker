<table id="report" class="table table-hover table-bordered results">
    <thead>
        <tr>
            <th>This Month</th>
            <th>Title</th>
            <th>Category</th>
            <th>Company</th>
            <th>Stations</th>
            <th>Airs</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $dat)
        <tr>
            <td>{!! $loop->iteration !!}</td>
            <td>
                {!! Html::linkRoute('programs.show',$dat->programVersion->title, $dat->programVersion->id) !!}
            @if(Sentry::getUser()->isSuperUser())
                <small>{!! Html::linkRoute('admin.programs.edit', 'Edit', $dat->programVersion->id)!!}</small>
            @endif
            </td>
            <td>{!! $dat->programVersion->category->name ?? '' !!}</td>
            <td>{!! $dat->programVersion->marketingCompany->name ?? '' !!}</td>
            <td>{!! $dat->stations !!}</td>
            <td>{!! $dat->airs !!}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th>Total:</th>
            <th></th>
            <th></th>
        </tr>
    </tfoot>
</table>


@section('script')
    @parent
    <script>
        var oTable;
        $(function(){
          oTable = $('#report').dataTable( {
            "pageLength": {!! $report->limit!!},
            "fnFooterCallback": function ( nRow, aaData, iStart, iEnd, aiDisplay ) {
              var iTotalAirs = 0;
              var iTotalIndex = 0;
              for ( var i=iStart ; i<iEnd ; i++ )
              {
                iTotalAirs += aaData[ aiDisplay[i] ][4]*1;
                iTotalIndex += aaData[ aiDisplay[i] ][5]*1;
              }

              var nCells = nRow.getElementsByTagName('th');
              nCells[4].innerHTML = parseInt(iTotalAirs);
              nCells[5].innerHTML = parseInt(iTotalIndex);
            }
          });
        });
    </script>
@endsection
