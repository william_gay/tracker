@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-signal"></i>
        Category Report
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('report',$report) !!}
    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center">Category Report</h4>
            <div class="date-selection text-center">
                <form class="form-inline" style="display:inline-block;">
                    {!! Former::xlarge_text('start-date','')
                    ->class('span4 form-control start-date')
                    ->id('start-date')
                    ->placeholder($start_date->format("F j, Y"))
                    ->required() !!}
                    {!! Former::xlarge_text('end-date','')
                    ->class('span4 form-control end-date')
                    ->id('end-date')
                    ->placeholder($end_date->format("F j, Y"))
                    ->required() !!}
                <button class="select btn btn-blue btn-icon">Select Date Range <i class="fa fa-calendar"></i></button>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-9">
            <div id="category-report" style="width: 100%;height: 500px;"></div>
        </div>
        <div class="col-xs-3">
            <p id="choices">
            </p>
        </div>
    </div>
@stop

@section('script')
<script>
$(document).ready(function () {
    var datasets = {
        @foreach($resultset as $key => $value)
            @if($key != 1),@endif "{!! $value['category_slug'] !!}": {
                    label: "{!! $value['category_name'] !!}",
                    data: {!! json_encode(array_values($value['data'])) !!}
            }
        @endforeach
    };

    // hard-code color indices to prevent them from shifting as
    // countries are turned on/off

    var i = 0;
    $.each(datasets, function(key, val) {
        val.color = i;
        ++i;
    });

    // insert checkboxes
    var choiceContainer = $("#choices");
    $.each(datasets, function(key, val) {
        choiceContainer.append("<div class='checkbox'><input type='checkbox' name='" + key +
            "' checked='checked' id='id" + key + "'></input>" +
            "<label for='id" + key + "' class='checkbox inline'>"
            + val.label + "</label></div>");
    });

    choiceContainer.find("input").click(plotAccordingToChoices);

    function plotAccordingToChoices() {

        var data = [];

        choiceContainer.find("input:checked").each(function () {
            var key = $(this).attr("name");
            if (key && datasets[key]) {
                data.push(datasets[key]);
            }
        });

        if (data.length > 0) {
            $.plot("#category-report", data, {
                yaxis: {
                    min: 0,
                    axisLabel: 'Value',
                    axisLabelUseCanvas: true,
                    axisLabelPadding: 5,
                    tickFormatter: formatAxis
                },
                xaxis: {
                    axisLabelUseCanvas: true,
                    mode: "time",
                    dateformat: "%mmm %d %yy",
                    minTickSize: [1, "day"],
                    lines: {"show": "true"},
                    points: {"show": "true"},
                    clickable:true,
                    hoverable: true
                }
            });
        }
    }

    plotAccordingToChoices();

    function formatAxis(v, axis) {
        return format_usd(v,"$");
    }

    function format_usd(n, currency) {
        return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
    }
});
</script>
@stop
