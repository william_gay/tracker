<table id="report" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th class="col-xs-1">Rank</th>
            <th class="col-xs-3">Product Name</th>
            <th class="col-xs-1"></th>
            <th class="col-xs-2">Category</th>
            <th class="col-xs-2">Sub-Category</th>
            <th>Company</th>
            <th class="col-xs-1">ASD</th>
            <th class="col-xs-1">Airs</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $key => $dat)
        <tr>
            <td>{!! $key + 1 !!}</td>
            <td>
                {!! Html::linkRoute('spots.show',$dat->title, $dat->spot_version_id) !!}
                @if(Sentry::getUser()->isSuperUser())
                    <small>{!! Html::linkRoute('admin.spots.edit', 'Edit', $dat->spot_version_id)!!}</small>
                @endif
            </td>
            <td>
                <span data-order="{!! $dat->new_for_week !!}">
                @if($dat->new_for_week == 1)
                    <i class='fa fa-trophy text-center'></i>
                @endif
                </span>
            </td>
            <td>
                {!! $dat->category !!}
            </td>
            <td>
                {!! $dat->subcategory !!}
            </td>
            <td>
                {!! $dat->company_name !!}
            </td>
            <td data-order="{{ $dat->average_length }}">
                {!! number_format($dat->average_length, 2) !!}
            </td>
            <td>
                {!! $dat->airings !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

@section('script')
    @parent
    <script>
        var oTable;
        $(function(){
          oTable = $('#report').dataTable( {
            "pageLength": {!! $report->limit!!}
          });
        });
    </script>
@endsection
