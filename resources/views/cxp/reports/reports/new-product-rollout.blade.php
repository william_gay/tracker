@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-signal"></i>
        New Product Rollout Report
    </h3>
@stop

@section('content')

    {!! Breadcrumbs::render('report',$report) !!}
    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center"><a href="#" class="popover-primary" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="This report details all of the new products during their testing phase, and predicts whether or not a product needs to be tweaked further or will be successful. The report is based on a number of factors such as: total number of airings, total number of channels and media expenditures." data-original-title="Product Rollout Report">New Product Rollout Report</a></h4>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
        @if($rollout)
            <table id="report" class="table table-hover table-bordered results">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Company</th>
                        <th>First Detected</th>
                        <th>Last Aired</th>
                        <th>Air</th>
                        <th>Media</th>
                        <th>Stations</th>
                        <th>Weeks</th>
                        <th>Rollout</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($rollout as $program)
                    <tr>
                        <td>
                            {!! Html::linkRoute('programs.show',$program['title'], $program['id']) !!}
                            @if(Sentry::getUser()->isSuperUser())
                                <small>{!! Html::linkRoute('admin.programs.edit', 'Edit', $program['id'])!!}</small>
                            @endif
                        </td>
                        <td>{!! $program['category_name'] !!}</td>
                        <td>{!! $program['marketingCompany'] !!}</td>
                        <td>{!! date("m/d/Y h:i a",strtotime($program['initial_date'])) !!}</td>
                        <td>{!! date("m/d/Y h:i a",strtotime($program['last_aired'])) !!}</td>
                        <td>{!! $program['airs'] !!}</td>
                        <td><span data-order="{!! number_format($program['media'], 0, '','') !!}">${!! number_format($program['media'], 2, '.',',') !!}</span></td>
                        <td>{!! $program['stations'] !!}</td>
                        <td>
                            <span data-order="{!! $program['pro'] !!}">
                                {!! $program['pro'] !!}
                            </span>
                        </td>
                        <td>
                            <span data-order="{!! $program['pro'] !!}">
                            @if($program['pro'] >= 6 OR $program['media'] > 75000)
                                <span style="color:green">YES</span>
                            @elseif($program['pro'] < 6)
                                <span style="color:red">NO</span>
                            @else
                                -
                            @endif
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div class="no_data">No shows this week yet...</div>
        @endif
        </div>
    </div>
@stop

@section('script')
<script>
var oTable;
$(function(){
  oTable = $('#report').dataTable( {
    "pageLength": 50
  });
});
</script>
@stop
