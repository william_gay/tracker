@extends('cxp.layout')

@section('style')
<style>

</style>
@stop

@section('header')
    <h3>
        <i class="fa fa-signal"></i>
        National Infomercial Grid
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('report',$report) !!}
    <div class="row">
        <div class="col-xs-12 report-title">
            <h4 class="text-center">{!! $report->name !!} <small>All times are in Eastern Time</small></h4>
            <div class="date-selection text-center">
                <form class="form-inline">
                {!! Former::xlarge_text('report-date','')->class('span2 report-date form-control')->placeholder($report_date->format("F j, Y"))->required() !!}
                <button class="select btn btn-blue btn-icon">Select Date<i class="fa fa-calendar"></i></button>
                </form>
            </div>
        </div>
    </div>
    <hr style="padding-bottom: 50px;">
    <div class="row">
        <div class="col-xs-12">
            <table cellpadding="0" cellspacing="0" border="0" id="network-grid" class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>Network</th>
                    @foreach($times as $time)
                        <th>{!! $time !!}</th>
                    @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach($gridSet as $grid)
                        <tr>
                        @foreach($grid as $air)
                            <td>{!! $air !!}</td>
                        @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('script')
<script>
$(document).ready(function () {
    var table = $('#network-grid').DataTable({
        "pageLength": 100,
        dom: 'Bfrtip',
        "order": [[0, 'asc']],
        "scrollX": true,
        "scrollY": 500,
        fixedColumns: true
    });
});
</script>
@stop
