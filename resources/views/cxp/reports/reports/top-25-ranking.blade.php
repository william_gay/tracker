<table id="" class="table table-hover table-bordered results">
    <thead>
        <tr>
            <th>Rank</th>
            <th>Title</th>
            <th>Grid Title</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $dat )
        <tr>
            <td>{!! $count++ !!}</td>
            <td>
                @if(Sentry::getUser()->hasAccess('reports.national-shows'))
                    {!! Html::linkRoute('programs.show',$dat->programVersion->title, $dat->programVersion->program_id) !!}
                @else
                    {!! $dat->programVersion->title !!}
                @endif
                @if(Sentry::getUser()->isSuperUser())
                    <small>{!! Html::linkRoute('admin.programs.edit', 'Edit', $dat->programVersion->id) !!}</small>
                @endif
            </td>
            <td>{!! $dat->programVersion->grid_title !!}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </tfoot>
</table>

@section('script')
    @parent
    <script>
        var oTable;
        $(function(){
          oTable = $('#report').dataTable( {
            "pageLength": {!! $report->limit!!}
          });
        });
    </script>
@endsection
