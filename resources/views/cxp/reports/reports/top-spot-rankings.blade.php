<table id="report" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Rank</th>
            <th>Product Name</th>
            <th>Marketing Company</th>
            <th>Price</th>
            <th>S/H</th>
            <th>Category</th>
            <th>ASD</th>
            @if(Sentry::getUser()->isSuperUser())
            <th>Media Index <small><i class="fa fa-lock"></i></small></th>
            <th class="span1">Airs  <small><i class="fa fa-lock"></i></small></th>
            @endif
        </tr>
    </thead>
    <tbody>
    <?php $count = 1; ?>
    @foreach($data as $dat)
        <tr>
            <td>{!! $count !!}</td>
            <td>
                @if(Sentry::getUser()->hasAccess('reports.national-spots'))
                    {!! Html::linkRoute('spots.show',$dat->title,$dat->spot_version_id) !!}
                @else
                    {!! $dat->title !!}
                @endif
                @if(Sentry::getUser()->isSuperUser())
                    <small>{!! Html::linkRoute('admin.spots.edit', 'Edit',$dat->spot_version_id)!!}</small>
                @endif
            </td>
            <td>
                @if ($dat->spotVersion->marketingCompany)
                {!! $dat->spotVersion->marketingCompany->name !!}
                @endif
            </td>
            <td>
            @if($dat->price)
                $ {!! number_format($dat->price,2) !!}
            @endif
            </td>
            <td>
            @if($dat->shipping_cost)
                ${!! number_format($dat->shipping_cost, 2) !!}
            @endif
            </td>
            <td>{!! $dat->category !!}</td>
            <td>
                {!! number_format($dat->average_length, 2) !!}
            </td>
            @if(Sentry::getUser()->isSuperUser())
            <td>
            @if($dat->media_index > 0)
                {!! $dat->media_index !!}
            @endif
            </td>
            <td>
                {!! $dat->airings !!}
            </td>
            @endif
        </tr>
        <?php $count++; ?>
    @endforeach
    </tbody>
</table>

@include('cxp.reports.footnotes')

@section('script')
    @parent
    <script>
        var oTable;
        $(function(){
          oTable = $('#report').dataTable( {
            "pageLength": {!! $report->limit!!}
          });
        });
    </script>
@endsection
