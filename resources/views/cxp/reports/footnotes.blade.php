@if ($report->interval == 'weekly' and $week_ending->startOfDay()->eq(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2014-06-27 00:00:00')))
    <small>* Due to technical issues in the ranking week ending 6/27/14, IMS has updated the above posted rankings as of 7/2/14. </small>
@elseif ($report->interval == 'monthly' and $month->startOfMonth()->eq(Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2014-06-01 00:00:00')))
	<small>* Due to technical issues in the ranking week ending 6/27/14, IMS has updated the above posted rankings as of 7/2/14. </small>
@endif
