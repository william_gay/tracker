@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-signal"></i>
        Reports &bull;
        @if ($report->type == 'long')
          Long Form
        @else
          Short Form
        @endif
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('report', $report) !!}
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-xs-12">
        @if($report->interval == 'weekly')
        	<h4 class="text-center">{!! $report->name !!} - {!! $week_ending->format("F jS, Y") !!}</h4>
        @elseif($report->interval == 'monthly')
          @if($report->slug == 'top-25-report')
            <h4 class="text-center">{!! $report->name !!} - {!! $month->subMonth()->format("F, Y") !!}</h4>
          @else
            <h4 class="text-center">{!! $report->name !!} - {!! $month->format("F, Y") !!}</h4>
          @endif
        @elseif($report->interval == 'yearly')
          <h4 class="text-center">{!! $report->name !!} - {!! $year->format("Y") !!}</h4>
        @endif
          <div class="date-selection text-center">
            <form class="form-inline" id="week_ending_frm" style="display:inline-block;">
            @if($report->interval == 'weekly')
              {!! Former::xlarge_text('week_ending','', $week_ending->format("F j, Y"))->id('weekly-rankings')->class('span6 form-control')->placeholder($week_ending->format("F j, Y")) !!}
              <button class="select btn btn-blue btn-icon">Select Week Ending <i class="fa fa-calendar"></i></button>
            @elseif($report->interval == 'monthly')
            @if($report->slug != 'top-25-report')
              {!! Former::select('month','')->class('span6 form-control')->options($months,$month->format("Y-m")) !!}
              <button class="select btn btn-blue btn-icon">Select Month <i class="fa fa-calendar"></i></button>
            @endif
            @elseif($report->interval == 'yearly')
              {!! Former::select('year', '')->class('span6 form-control')->options($years, $year->format('Y')) !!}
              <button class="select btn btn-blue btn-icon">Select Year <i class="fa fa-calendar"></i></button>
            @endif
            </form>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="loading"></div>
            <div id="grid-message"></div>
        	@include('cxp.reports.reports.'.$report->template)
        </div>
    </div>
@stop

@section('script')
<script>
var report_length = {!! $report->limit!!};
</script>
@endsection
