@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-gift"></i>
        Products
    </h3>
@stop

@section('content')
    {!! Breadcrumbs::render('products') !!}

    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center">
                <i class="fa fa-shopping-cart"></i> Product Search
            </h4>
            <table id="products" class="table table-hover table-bordered results">
                <thead>
                    <th class="col-xs-3">Name</th>
                    <th class="col-xs-3">Category</th>
                    <th class="col-xs-3">Sub-Category</th>
                    <th>Min Price</th>
                    <th>Max Price</th>
                    <th class="col-xs-1">Retail</th>
                    <th class="col-xs-1">Spots</th>
                    <th class="col-xs-1">Show</th>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr @if($product->min_price > $product->max_price)class="danger"@endif>
                        <td>
                            <a href="{!! route("products.show", array($product->slug)) !!}">{!! $product->name !!}</a>
                            @if(Sentry::getUser()->isSuperUser())
                                <small>
                                    <a href="{!! route("admin.products.edit",array($product->id)) !!}" target="_blank">Edit</a>
                                </small>
                            @endif
                        </td>
                        <td>{!! $product->category ?? '' !!}</td>
                        <td>{!! $product->subcategory ?? '' !!}</td>
                        <td>
                            ${!! number_format($product->min_price, 2) !!}
                        </td>
                        <td>
                            @if($product->min_price != $product->max_price)
                                ${!! number_format($product->max_price, 2) !!}
                            @endif
                        </td>
                        <td>{!! $product->retailers !!}</td>
                        <td>{!! $product->spots()->count() !!}</td>
                        <td>{!! $product->programs()->count() !!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop

@section('script')
<script>
var oTable;
$(function(){
  oTable = $('#products').dataTable( {
    "pageLength": 100,
    "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>",


    //"bSaveState": true,
    //"processing": true,
    //"serverSide": true
    //"ajax": "{!! URL::to('products/data') !!}",
  });
});
</script>
@stop
