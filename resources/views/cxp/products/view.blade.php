@extends('cxp.layout')

@section('header')
    <h3>
        <i class="fa fa-gift"></i>
        Product &bull; {!! $product->name !!}
        @if(Sentry::getUser()->isSuperUser())
            <small>
                <a href="{!! route("admin.products.edit",array($product->id)) !!}" target="_blank">Edit</a>
            </small>
        @endif
    </h3>
@stop

@section('style')
<style>
#retailDistChart {
    width: 100%;
    height: 500px;
}

#retailRelatedChart {
    width: 100%;
    height: 500px;
}

</style>
@stop

@section('content')
    {!! Breadcrumbs::render('product', $product) !!}

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num" data-start="0" data-end="{!! $retailers->count() !!}" data-postfix data-duration="1500">
                    0
                </div>
                <h3>{!! __('retailers.retailers') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                @if($lowestPrice->min_price > 0)
                    ${!! number_format($lowestPrice->min_price, 2) !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.lowest_price') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                    ${!! number_format($highestPrice->max_price, 2) !!}
                </div>
                <h3>{!! __('retailers.highest_price') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                @if(is_numeric($drPrice))
                    ${!! number_format($drPrice, 2) !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.drtv_price') !!}</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                @if(isset($maxRank))
                    {!! $maxRank !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.high_ims_rank') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                @if(isset($streak))
                    {!! $streak !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.ims_streak') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-12">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                @if(isset($airings))
                    {!! $airings !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.total_airs') !!}</h3>
            </div>
        </div>
        <div class="col-lg-3 col-md-12">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="num">
                @if(isset($mediaSpend))
                    {!! $mediaSpend !!}
                @else
                    N/A
                @endif
                </div>
                <h3>{!! __('retailers.total_media_spend') !!}</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="date-selection text-center" style="margin-top: 15px;">
            @if(isset($monthSelect))
                <form class="form-inline" style="display:inline-block;">
                    {!! Former::select('report','')->class('span6 form-control')->options($monthSelect,$report->id) !!}
                    <button class="select btn btn-blue btn-icon">Select Month <i class="fa fa-calendar"></i></button>
                </form>
            @else
                <div class="alert alert-info">{!! __('retailers.no_product_data') !!}</div>
            @endif
            </div>
        </div>
    </div>
    @if($product->description)
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default panel-shadow">
                <!-- panel head -->
                <div class="panel-heading">
                    <div class="panel-title">{!! __('products.description') !!}</div>
                </div>

                <!-- panel body -->
                <div class="panel-body">
                    {!! $product->description !!}
                </div>

            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div id="retailDistChart"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div id="retailRelatedChart"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <h3 class="text-center">{!! __('retailers.distribution') !!}</h3>
            <table id="retailers" class="table table-hover table-bordered results">
                <thead>
                    <th>{!! __('retailers.name') !!}</th>
                    <th>{!! __('products.price') !!}</th>
                    <th>{!! __('retailers.store_count') !!}</th>
                    <th>Related DR Products</th>
                    <th>Date Detected</th>
                    <th>Store Offer</th>
                </thead>
                <tbody>
                @foreach($retailers as $retailer)
                    <tr>
                        <td>
                            {!! Html::linkRoute('retailers.show',$retailer->retailer_name, $retailer->retailer_slug) !!}
                        </td>
                        <td>
                            @if($retailer->min_price == $retailer->max_price)
                            <span title="{!! $retailer->min_price !!}">
                                @if($retailer->min_price > 0)
                                    ${!! number_format($retailer->min_price, 2) !!}
                                @else
                                    N/A
                                @endif
                            </span>
                            @else
                            <span title="{!! $retailer->min_price !!}">
                                ${!! number_format($retailer->min_price, 2) !!} &dash; ${!! number_format($retailer->max_price, 2) !!}
                            </span>
                            @endif
                        </td>
                        <td>
                        @if($retailer->store_count != 0)
                            {!! $retailer->store_count !!}
                        @else
                            N/A
                        @endif
                        </td>
                        <td>{!! $retailer->related_count !!}</td>
                        <td>{!! $retailer->date_recorded->format('m/d/Y') !!}</td>
                        <td>
                            <a class="btn btn-green btn-icon icon-left" href="{!! $retailer->retailer_product_website !!}" target="_blank">
                                <i class="fa fa-eye"></i>
                                View
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <h5 class="text-center">Retail</h5>
            @if ($product->retailerProducts->count() > 0)
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>Title</td>
                    </tr>
                </thead>
                <tbody>
                @foreach($product->retailerProducts as $rp)
                    <tr>
                        <td>
                            @if($rp->retailer)
                                {!! $rp->retailer->name !!}
                            @else
                                N/A
                            @endif
                            @if(Sentry::getUser()->isSuperUser())
                                <small>
                                    <a href="{!! route("admin.retailer-products.edit",array($rp->id)) !!}" target="_blank">Edit</a>
                                </small>
                            @endif
                            <span class="pull-right">
                            @if($rp->min_price == $rp->max_price)
                                ${!! number_format($rp->min_price, 2) !!}
                            @else
                                ${!! number_format($rp->min_price, 2) !!} &dash; ${!! number_format($rp->max_price, 2) !!}
                            @endif
                            </span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @else
                <div class="alert alert-info">No products</div>
            @endif
        </div>
        <div class="col-xs-4">
            <h5 class="text-center">Shows</h5>
            @if ($product->programs->count() > 0)
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>Title</td>
                        <td>Market</td>
                    </tr>
                </thead>
                <tbody>
                @foreach($product->programs as $p)
                    <tr>
                        <td>
                            <a href="{!! route("programs.parent", array($p->id)) !!}">{!! $p->title !!}</a>
                                @if($p->programVersions()->orderBy('monitor_date', 'desc')->first()->price)
                                <span class="pull-right">
                                    @if($p->programVersions()->orderBy('monitor_date', 'desc')->first()->cost > 0)
                                        ${!! $p->programVersions()->orderBy('monitor_date', 'desc')->first()->cost !!}
                                    @else
                                        {!! $p->programVersions()->orderBy('monitor_date', 'desc')->first()->price !!}
                                    @endif
                                </span>
                                @endif
                        </td>
                        <td>{!! $p->language->name !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @else
                <div class="alert alert-info">No programs</div>
            @endif
        </div>
        <div class="col-xs-4">
            <h5 class="text-center">Spots</h5>
            @if ($product->spots->count() > 0)
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>Title</td>
                        <td>Market</td>
                    </tr>
                </thead>
                <tbody>
                @foreach($product->spots as $s)
                    <tr>
                        <td>
                            <a href="{!! route("spots.parent", array($s->id)) !!}">{!! $s->title !!}</a>
                                @if($s->spotVersions()->orderBy('air_date', 'desc')->first()->price)
                                <span class="pull-right">
                                    ${!! number_format($s->spotVersions()->orderBy('air_date', 'desc')->first()->price, 2) !!}
                                </span>
                                @endif
                        </td>
                        <td>{!! $s->language->name !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @else
                <div class="alert alert-info">No Spots</div>
            @endif
        </div>
    </div>

@stop

@section('script')
<script>
AmCharts.makeChart("retailDistChart",
{
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "store",
    "startDuration": 1,
    "categoryAxis": {
        "gridPosition": "start"
    },
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[title]] of [[category]]:$[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-2",
            "title": "Price",
            "type": "column",
            "valueAxis": "ValueAxis-2",
            "valueField": "price"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-2",
            "position": "left",
            "gridAlpha": 0,
            "title": "Price in Retailer",
            "precision": 2,
            "unit": "$",
            "unitPosition": "left"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {
        "useGraphSettings": true
    },
    "titles": [
        {
            "id": "Title-1",
            "size": 15,
            "text": "Distribution in Retailers"
        }
    ],
    "dataProvider": {!! $retailersGraph->toJson() !!}
});

AmCharts.makeChart("retailRelatedChart",
{
    "type": "serial",
    "pathToImages": "{!!URL::to('/')!!}/assets/img/",
    "categoryField": "store",
    "startDuration": 1,
    "categoryAxis": {
        "gridPosition": "start"
    },
    "trendLines": [],
    "graphs": [
        {
            "balloonText": "[[title]] of [[category]]:[[value]]",
            "fillAlphas": 1,
            "id": "AmGraph-1",
            "title": "Related Products",
            "type": "column",
            "valueField": "related"
        }
    ],
    "guides": [],
    "valueAxes": [
        {
            "id": "ValueAxis-1",
            "title": "Related Category Products"
        }
    ],
    "allLabels": [],
    "balloon": {},
    "legend": {
        "useGraphSettings": true
    },
    "titles": [
        {
            "id": "Title-1",
            "size": 15,
            "text": "Number of Products in DR Category - {!! $product->category->name !!}"
        }
    ],
    "dataProvider": {!! $retailersGraph->toJson() !!}
}
);
var rTable;
$(function(){
  rTable = $('#retailers').dataTable( {
    "pageLength": 10,
    "sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>",


    "columns": [
      null,
      { "sType": "title-numeric" },
      null,
      null,
      null,
      null
    ]
  });
});
</script>
@stop
