<table class="table table-bordered table-striped programming">
    <thead>
        <th>Title</th>
        <th>Grid Title</th>
        <th>Category</th>
        <th>Sub Category</th>
        <th>Initial Date</th>
    </thead>
    <tbody>
    @foreach ($programs as $program)
        <tr>
            <td>
                <a href="{!! route("programs.show", array($program->id)) !!}">{!! $program->title !!}</a> @if(Sentry::getUser()->isSuperUser()) <small><a href="{!! route("admin.programs.edit",array($program->id)) !!}" target="_blank">Edit</a></small> @endif
            </td>
            <td>
                {!! $program->grid_title !!}
            </td>
            <td>
                {!! $program->category->name ?? '' !!}
            </td>
            <td>
                {!! $program->subCategory->name ?? '' !!}
            </td>
            <td>
                {!! $program->monitor_date !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
