<table class="table table-bordered table-striped programming">
    <thead>
        <th>Title</th>
        <th>Version</th>
        <th>Length</th>
        <th>Category</th>
        <th>Sub Category</th>
        <th>Initial Date</th>
    </thead>
    <tbody>
    @foreach ($spots as $spot)
        <tr>
            <td>
                <a href="{!! route("spots.show", array($spot->id)) !!}">{!! $spot->title !!}</a>  @if(Sentry::getUser()->isSuperUser()) <small><a href="{!! route("admin.spots.edit",array($spot->id)) !!}" target="_blank">Edit</a></small> @endif
            </td>
            <td>
                {!! $spot->version !!}
            </td>
            <td>
                :{!! $spot->length !!}
            </td>
            <td>
                {!! $spot->category->name  ?? '' !!}
            </td>
            <td>
                {!! $spot->subCategory->name ?? '' !!}
            </td>
            <td>
                {!! \Carbon\Carbon::parse($spot->initial_date)->format('m-d-Y h:i a') !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
