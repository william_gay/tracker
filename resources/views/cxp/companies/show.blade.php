@extends('cxp.layout')

@section('header')
    @if (Request::segments()[0] == 'agencies')
    <h3>
        <i class="fa fa-building"></i>
        Agencies &bull; {!! $company->name !!}
    </h3>
    @else
    <h3>
        <i class="fa fa-building-o"></i>
        Companies &bull; {!! $company->name !!}
    </h3>
    @endif
@stop

@section('content')
    @if (Request::segments()[0] == 'agencies')
    {!! Breadcrumbs::render('agencies') !!}
    @else
    {!! Breadcrumbs::render('companies') !!}
    @endif

    <div class="row">
        <div class="col-xs-6 col-xs-offset-3">
            <h3 class="text-center">{!! $company->name !!}
                <small>
                    <a href="{!! route("admin.companies.edit",array($company->id)) !!}" target="_blank">Edit</a>
                </small>
            </h3>

            @if ($company->logo_location)
            <img src="{!! "https://s3-us-west-2.amazonaws.com/ims-logos/".$company->logo_location !!}" style="max-width: 100%; margin-left:auto;margin-right:auto;">
            @endif

            <div class="panel panel-default panel-shadow">
                <div class="panel-heading">
                    <div class="panel-title">
                        @if (Request::segments()[0] == 'agencies')
                        Agency Overview
                        @else
                        Company Overview
                        @endif
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td class="bold">Address</td>
                            <td>
                                {!! $company->address !!}
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">City</td>
                            <td>
                                {!! $company->city !!}
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">State</td>
                            <td>
                                {!! $company->state !!}
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">Zip</td>
                            <td>
                                {!! $company->zip !!}
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">Website</td>
                            <td>
                                <a href="{!! $company->website !!}">{!! $company->website !!}</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">Contact</td>
                            <td>
                                {!! $company->contact !!}
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">E-mail</td>
                            <td>
                                {!! $company->email !!}
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">Phone</td>
                            <td>
                                {!! $company->phone !!}
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">Fax</td>
                            <td>
                                {!! $company->fax !!}
                            </td>
                        </tr>
                        <tr>
                            <td class="bold">Agency</td>
                            <td>
                                {!! $company->agency !!}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @if ($productionProgramVersions->count() > 0 or $marketingProgramVersions->count() > 0)
    <div class="row">
        <div class="col-xs-12">
            <h2 class="text-center">Long-Form</h2>
        </div>
    </div>
    <div class="row">
        @if ($productionProgramVersions->count() > 0)
        <div class="col-xs-12">
            <h4 class="text-center">Production</h4>

                @include('cxp.companies.partials.program-table', ['programs' => $productionProgramVersions])
        </div>
        @endif
        <div class="col-xs-12">
            <h4 class="text-center">Marketer</h4>
            @if ($marketingProgramVersions->count() > 0)
                @include('cxp.companies.partials.program-table', ['programs' => $marketingProgramVersions])
            @else
                <div class="no_data">No data available.</div>
            @endif
        </div>
    </div>
    @endif

    @if ($marketingSpotVersions->count() > 0)
    <div class="row">
        <div class="col-xs-12">
            <h2 class="text-center">Short-Form</h2>

            <h4 class="text-center">Marketer</h4>
                @include('cxp.companies.partials.spot-table', ['spots' => $marketingSpotVersions])
        </div>
    </div>
    @endif
@stop

@section('script')
<script>
$(function(){
    var dTable = $('.programming').dataTable( {
        "pageLength": 10,


        "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        },
        "oTableTools": {
            "sSwfPath": "/assets/swf/copy_csv_xls_pdf.swf",
            "aButtons": [
                "copy",
                "csv",
                "xls",
                {
                    "sExtends": "pdf",
                    "sPdfOrientation": "landscape",
                    "sPdfMessage": "IMS Report - Networks Monitored"
                },
                "print"
            ]
        }
    });
});
</script>
@stop
