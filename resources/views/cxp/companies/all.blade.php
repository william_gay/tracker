@extends('cxp.layout')

@section('header')
    @if (Request::segments()[0] == 'agencies')
    <h3>
        <i class="fa fa-building"></i>
        Agencies
    </h3>
    @else
    <h3>
        <i class="fa fa-building-o"></i>
        Companies
    </h3>
    @endif
@stop

@section('content')
    @if (Request::segments()[0] == 'agencies')
    {!! Breadcrumbs::render('agencies') !!}
    @else
    {!! Breadcrumbs::render('companies') !!}
    @endif

    <div class="row">
        <div class="col-xs-12">
            <h4 class="text-center">
                @if (Request::segments()[0] == 'agencies')
                <i class="fa fa-building"></i> Agency Search
                @else
                <i class="fa fa-building-o"></i> Company Search
                @endif
            </h4>
            {!! $html->table() !!}
        </div>
    </div>
@stop

@section('script')
{!! $html->scripts() !!}
@endsection
