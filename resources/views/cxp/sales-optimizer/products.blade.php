@extends('cxp.layout')

@section('style')
<style>
#quad {
    width: 100%;
    height: 350px;
}
.stacked-bar {
    width: 100%;
    height: 350px;
}
.legend {
    height: 100px;
    overflow: auto;
}
.scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
}
</style>
@endsection

@section('header')
    <h3>
        <i class="fa fa-money"></i>
        Sales Optimizer <small>Product Comparison</small>
    </h3>
@endsection

@section('content')

@include('cxp.partials.sales-optimizer.filters.products')

<div class="row">
    <div class="col-xs-12">
        <div id="network-evaluator-days" class="stacked-bar"></div>
        <div id="network-evaluator-days-legend" class="legend"></div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-xs-12">
        <div id="network-evaluator-dayparts" class="stacked-bar"></div>
        <div id="network-evaluator-dayparts-legend" class="legend"></div>
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){

    @include('cxp.partials.sales-optimizer.common-scripts')

    $('#channel_compare_picker').multiselect(channelComparisonConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);

    var daysChart = AmCharts.makeChart("network-evaluator-days", {
        "type": "serial",
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "theme": "none",
        "titles": [
            {
                "text": "Weekly Brand Frequency for {!! $networkCompare->implode(',') !!} by Days",
                "size": 15
            }
        ],
        "legend": {
            "divId": "network-evaluator-days-legend",
            "horizontalGap": 0,
            "maxColumns": 8,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 8,
            "fontSize": 8,
            "marginLeft": 5,
            "marginRight": 5,
            "valueWidth": 5,
            "verticalGap": 5
        },
        "dataProvider": {!! $newDays->toJson(JSON_NUMERIC_CHECK) !!},
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.5,
            "gridAlpha": 0
        }],
        "graphs": {!!  $byDaysGraph->toJson() !!},
        "rotate": true,
        "categoryField": "dayofweek",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "amExport": {
            top : 0,
            right : 50,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });

    var dayPartsChart = AmCharts.makeChart("network-evaluator-dayparts", {
        "type": "serial",
        "titles": [
            {
                "text": "Weekly Brand Frequency for {!! $networkCompare->implode(',') !!} by Day Parts",
                "size": 15
            }
        ],
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "theme": "none",
        "legend": {
            "divId": "network-evaluator-dayparts-legend",
            "horizontalGap": 0,
            "maxColumns": 8,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 8,
            "fontSize": 8,
            "marginLeft": 5,
            "marginRight": 5,
            "valueWidth": 5,
            "verticalGap": 5
        },
        "dataProvider": {!! $newDayParts->toJson(JSON_NUMERIC_CHECK) !!},
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.5,
            "gridAlpha": 0
        }],
        "graphs": {!!  $byDayPartsGraph->toJson() !!},
        "rotate": true,
        "categoryField": "daypart",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "amExport": {
            top : 0,
            right : 50,
            exportJPG : true,
            exportPNG : true,
            exportSVG : true,
            exportPDF : true,
            menuItems: [{
                textAlign: 'center',
                icon: '{!!URL::to('/')!!}/assets/img/export.png',
                iconTitle: 'Save chart as an image',
            }]
        }
    });
});
</script>
@endsection
