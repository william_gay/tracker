@extends('cxp.layout')

@section('style')
<style>
#quad {
    width: 100%;
    height: 350px;
}
.stacked-bar {
    width: 100%;
    height: 400px;
}
.legend {
    height: 100px;
    overflow: auto;
}
.scrollable-menu {
    height: auto;
    max-height: 400px;
    overflow-x: hidden;
}
</style>
@endsection

@section('header')
    <h3>
        <i class="fa fa-money"></i>
        Sales Optimizer <small>Comparison</small>
    </h3>
@endsection

@section('content')

@include('cxp.partials.sales-optimizer.filters.comparison')

<div class="row">
    <div class="col-xs-6">
        <h3 class="text-center">{!! $network->abbr ?? 'N/A' !!}</h3>
        @if($channelByCategory->isEmpty())
            <div class="no_data">No data available.</div>
            <div id="channel-categories" class="stacked-bar" style="display:none;"></div>
        @else
            <div id="channel-categories" class="stacked-bar"></div>
        @endif
    </div>
    <div class="col-xs-6">
        <h3 class="text-center">{!!  $networkCompare->implode(',') !!}</h3>
        @if($compareByCategory->isEmpty())
            <div class="no_data">No data available.</div>
            <div id="comparison-categories" class="stacked-bar" style="display:none;"></div>
        @else
            <div id="comparison-categories" class="stacked-bar"></div>
        @endif
    </div>
</div>
<hr>
<div class="row">
    <div class="col-xs-6">
        <h3 class="text-center">{!! $network->abbr ?? 'N/A' !!}</h3>
        @if($channelBySpot->isEmpty())
            <div class="no_data">No data available.</div>
            <div id="channel-brands" class="stacked-bar" style="display:none;"></div>
        @else
            <div id="channel-brands" class="stacked-bar"></div>
        @endif

    </div>
    <div class="col-xs-6">
        <h3 class="text-center">{!! $networkCompare->implode(',') !!}</h3>
        @if($compareByCategory->isEmpty())
            <div class="no_data">No data available.</div>
            <div id="comparison-brands" class="stacked-bar" style="display:none;"></div>
        @else
            <div id="comparison-brands" class="stacked-bar"></div>
        @endif
    </div>
</div>
@endsection

@section('script')
<script>
$(document).ready(function(){

    @include('cxp.partials.sales-optimizer.common-scripts')

    $('#channel_picker').multiselect(channelConfigurationSet);
    $('#channel_compare_picker').multiselect(channelComparisonConfigurationSet);
    $('#category_picker').multiselect(categoryConfigurationSet);

    var channelCategories = AmCharts.makeChart("channel-categories", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $channelByCategory->toJson() !!},
        "titles": [
            {
                "text": "Airings",
                "size": 15
            }
        ],
        "valueField": "airings",
        "titleField": "category_name",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "exportConfig":{
          menuItems: [{
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          format: 'png'
          }]
        }
    });

    var channelBrands = AmCharts.makeChart("channel-brands", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $channelBySpot->toJson() !!},
        "titles": [
            {
                "text": "Airings",
                "size": 15
            }
        ],
        "valueField": "airings",
        "titleField": "title",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "exportConfig":{
          menuItems: [{
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          format: 'png'
          }]
        }
    });

    var compareCategories = AmCharts.makeChart("comparison-categories", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $compareByCategory->toJson() !!},
        "titles": [
            {
                "text": "Airings",
                "size": 15
            }
        ],
        "valueField": "airings",
        "titleField": "category_name",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "exportConfig":{
          menuItems: [{
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          format: 'png'
          }]
        }
    });

    var compareBrands = AmCharts.makeChart("comparison-brands", {
        "pathToImages": "{!!URL::to('/')!!}/assets/img/",
        "type": "pie",
        "theme": "none",
        "dataProvider": {!! $compareBySpot->toJson() !!},
        "titles": [
            {
                "text": "Airings",
                "size": 15
            }
        ],
        "valueField": "airings",
        "titleField": "title",
        "outlineAlpha": 0.4,
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "exportConfig":{
          menuItems: [{
          icon: '{!!URL::to('/')!!}/assets/img/export.png',
          format: 'png'
          }]
        }
    });

});
</script>
@endsection
