<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{!! config('app.site_config.description') !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>{!! config('app.site_config.title') !!}</title>

    @include('cxp.partials.favicon')
    @include('cxp.partials.assets.styles')

    @yield('style')

    <!-- Load Google JavaScript API -->
    <script type="text/javascript" src="//www.google.com/jsapi"></script>

    @include('cxp.partials.assets.compatability')

    @include('cxp.partials.bugsnag')

</head>
<body class="page-body {!! (Route::is('dashboard') ?? Route::is('retail-report.monthly')) ? 'page-fade' : '' !!} " data-url="https://my.imsreport.com">

<!-- .page-container -->
<div class="page-container horizontal-menu with-sidebar {!! (Route::is( 'dashboard') ?? Route::is('retail-report.monthly') ?? Route::is('attribution-tracker.index')) ? 'sidebar-collapsed' : '' !!} ">

    <!-- We still need to update this -->
    @include('cxp.partials.topbar')

    @include('cxp.partials.menu')

<!-- .main-content -->
<div class="main-content">


        <div class="row">
            <div class="col-sm-12">
                    @yield('header')
            </div>
        </div>

        <br />

            @include('cxp.partials.alert')
            @yield('content')

<br>
<!-- Footer -->
<footer class="main">
    <div class="col-xs-6 text-left">
        <h4>&copy; {!! date('Y') !!} <strong>{!! Config::get('app.site_config.company') !!}</strong></h4>
    </div>
    <div class="col-xs-6 text-right">
        @include('cxp.partials.social')
    </div>
</footer>

</div>
<!-- /.main-content -->


</div>
<!-- /.page-container -->

    @include('cxp.partials.modals.help')

        @include('cxp.partials.assets.scripts')
        @yield('script')
        @stack('scripts')

        @include('cxp.partials.snappy')

    @include('analytics')

</body>
</html>
