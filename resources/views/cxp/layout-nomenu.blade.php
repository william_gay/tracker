<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{!! config('app.site_config.description') !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>{!! config('app.site_config.title') !!}</title>

    @include('cxp.partials.favicon')
    @include('cxp.partials.assets.styles')

    @yield('style')

    <!-- Load Google JavaScript API -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    @include('cxp.partials.assets.compatability')

    @include('cxp.partials.bugsnag')
</head>
<body class="page-body {!! Route::is( 'dashboard') ? 'page-fade' : '' !!} " data-url="https://my.imsreport.com">

<!-- .page-container -->
<div class="page-container horizontal-menu sidebar-collapsed">

<!-- .main-content -->
<div class="main-content">


        <div class="row">
            <div class="col-sm-12">
                <div class="hidden-lg well">
                    @yield('header')
                </div>
            </div>
        </div>

        <br />
            @include('cxp.partials.alert')
            @yield('content')

</div>
<!-- /.main-content -->


</div>
<!-- /.page-container -->

        @include('cxp.partials.assets.scripts')
        @yield('script')
</body>
</html>
