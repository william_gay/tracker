@extends('oauth.layout')

@section('content')
	<div class="row">
        <div class="col-md-12">
        	<div class="block">
                <div class="block-body">
                {!! Form::open() !!}
                    <input type="hidden" name="client_id" value="{!! $params['client_id'] !!}" />
                    <input type="hidden" name="redirect_uri" value="{!! $params['redirect_uri'] !!}" />
                    <input type="hidden" name="response_type" value="{!! $params['response_type'] !!}" />
                    <input type="hidden" name="approve" value="true" />
                    <h2>Do you grant access between {!! $clientName->first()->name !!} and the Dashboard?</h2>
                    <div>
                        {!! Form::submit('Connect', array('class' => 'btn btn-success')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection