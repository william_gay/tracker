<!DOCTYPE html>
<!--[if lt IE 7]>       <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>          <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>          <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>{!! Config::get('app.site_config.title') !!}</title>
    <meta name="description" content="{!! Config::get('app.site_config.description') !!}">
    <meta name="viewport" content="width=device-width">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">


    @yield('style')
    <script>
    var laravel = {};
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="b3959fd0454692f587b9546eedf396bd"></script>
    @if (Sentry::check())
    <script>
    Bugsnag.releaseStage = "{!! App::environment() !!}";
    Bugsnag.user = {
        id: {!! Sentry::getUser()->id !!},
        name: "{!! Sentry::getUser()->first_name.' '.Sentry::getUser()->last_name !!}",
        email: "{!! Sentry::getUser()->email !!}"
    };
    </script>
    @endif
</head>
<body>
    <!-- #wrap -->
    <div id="wrap">
        <!-- #top -->
        <div id="top">
            <!-- .navbar -->
            <nav style="text-align: center;" class="navbar navbar navbar-top" role="navigation">
                <div class="container-fluid">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a style="float: none; text-align: center; display: inline-block;" class="navbar-brand" href="{!!URL::to('/')!!}"><img src="{!! asset('assets/img/logo.png') !!}" style="float: none; width: 150px; text-align: center; display: inline-block;" alt="Media Analytics Admin Dashboard"/></a>
                    @if (Sentry::check())
                        <a class="btn btn-default navbar-btn btn-inverse navbar-right" data-placement="bottom" data-original-title="Logout" rel="tooltip"
                           href="{!! route('admin.logout') !!}"><i class="fa fa-power-off"></i>
                        </a>
                        <!-- /.topnav -->
                            <h3 style="display: inline-block;">
                                <i class="fa fa-rocket"></i>
                                <a href="/admin/oauth-clients">OAuth Secured Endpoint</a>
                            </h3>
                    @endif
                </div>
            </nav>
            <!-- /.navbar -->
        </div>
        <!-- /#top -->
        <!-- .head -->
        <header class="head">
            <!-- ."main-bar -->
            <div class="main-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            @yield('header')
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /.main-bar -->
        </header>
        <!-- /.head -->

        <!-- #content -->
        <div id="content">

            <div class="container">
                @include('admin.partials.alert')
                @yield('content')
            </div>
            <!-- /.outer -->
        </div>
        <!-- /#content -->
        <!-- #push do not remove -->
        <div id="push"></div>
        <!-- /#push -->
    </div>
    <!-- /#wrap -->

    <div class="clearfix"></div>

    <!-- #helpModal -->
    <div id="helpModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="helpModalLabel"
         aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="helpModalLabel"><i class="icon-external-link"></i> Help</h3>
        </div>
        <div class="modal-body">
            @section('help')
                <p>No Help for this section.</p>
            @show
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
    <!-- /#helpModal -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>
        if (typeof jQuery == 'undefined') {
            document.write(unescape("%3Cscript src='/assets/js/jquery-1.11.1.min.js' type='text/javascript'%3E%3C/script%3E"));
        }
        </script>

        <script src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>


        @yield('script')

        <script type="text/javascript">
        var _paq = _paq || [];
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
        var u=(("https:" == document.location.protocol) ? "https" : "http") + "://stats.imsreport.com/";
        _paq.push(['setTrackerUrl', u+'piwik.php']);
        _paq.push(['setSiteId', 1]);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
        g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
        })();
        </script>
        <noscript><p><img src="http://stats.imsreport.com/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>

</body>
</html>
