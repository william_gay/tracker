@extends('admin.layouts')

@section('header')
    @include('oauth.header')
@stop

@section('content')
	<div class="row">
        <div class="col-md-12">
        	<div class="block">
                <p class="block-heading">Authorize</p>
                <p>This is a secure route, and you are authorized.</p>
            </div>
        </div>
    </div>

@stop