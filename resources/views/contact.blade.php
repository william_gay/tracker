<html>
<body>
<!-- Blade Template engine -->
 {!! Form:: open(array('url' => 'contact')) !!} <!--contact_request is a router from Route class-->

            <ul class="errors">
                @foreach($errors->all('<li>:message</li>') as $message)
                {!! $message !!}
                @endforeach
            </ul>

            {!! Form:: label ('first_name', 'First Name*' )!!}
            {!! Form:: text ('first_name', $user->first_name )!!}

            {!! Form:: label ('last_name', 'Last Name*' )!!}
            {!! Form:: text ('last_name', $user->last_name )!!}

            {!! Form:: label ('email', 'E-mail Address*') !!}
            {!! Form:: email ('email', $user->email ) !!}

            {!! Form:: label ('subject', 'Subject') !!}
            {!! Form:: text ('subject', 'Upgrade Request') !!}

            {!! Form:: label ('message', 'Message*' )!!}
            {!! Form:: textarea ('message', '')!!}

            {!! Form::reset('Clear', array('class' => 'button')) !!}
            {!! Form::submit('Send', array('class' => 'button')) !!}

            {!! Form:: close() !!}
</body>
</html>