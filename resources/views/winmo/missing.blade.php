@extends('winmo.layout')

@section('styles')
<style>
html, #app {
    height: 100%;
}
</style>
@endsection

@section('content')
<div id="wrapper" class="h-full">
    No matching company.

    <hr class="border-b m-0" />
    <footer class="p-4 text-right">
            Access Complete Profile: <img src="{!! asset('assets/img/logo.png') !!}" class="inline-block" />
    </footer>
</div>
@endsection
