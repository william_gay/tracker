@extends('winmo.layout')

@section('styles')
<style>
.tooltip {
    display: block !important;
    z-index: 10000
}
.tooltip .tooltip-inner {
    background: #000;
    color: #fff;
    border-radius: 16px;
    padding: 5px 10px 4px
}
.tooltip .tooltip-arrow {
    width: 0;
    height: 0;
    border-style: solid;
    position: absolute;
    margin: 5px;
    border-color: #000
}

.tooltip[x-placement^=top] {
    margin-bottom: 5px
}

.tooltip[x-placement^=top] .tooltip-arrow {
    border-width: 5px 5px 0 5px;
    border-left-color: transparent !important;
    border-right-color: transparent !important;
    border-bottom-color: transparent !important;
    bottom: -5px;
    left: calc(50% - 5px);
    margin-top: 0;
    margin-bottom: 0
}

.tooltip[x-placement^=bottom] {
    margin-top: 5px
}

.tooltip[x-placement^=bottom] .tooltip-arrow {
    border-width: 0 5px 5px 5px;
    border-left-color: transparent !important;
    border-right-color: transparent !important;
    border-top-color: transparent !important;
    top: -5px;
    left: calc(50% - 5px);
    margin-top: 0;
    margin-bottom: 0
}

.tooltip[x-placement^=right] {
    margin-left: 5px
}

.tooltip[x-placement^=right] .tooltip-arrow {
    border-width: 5px 5px 5px 0;
    border-left-color: transparent !important;
    border-top-color: transparent !important;
    border-bottom-color: transparent !important;
    left: -5px;
    top: calc(50% - 5px);
    margin-left: 0;
    margin-right: 0
}

.tooltip[x-placement^=left] {
    margin-right: 5px
}

.tooltip[x-placement^=left] .tooltip-arrow {
    border-width: 5px 0 5px 5px;
    border-top-color: transparent !important;
    border-right-color: transparent !important;
    border-bottom-color: transparent !important;
    right: -5px;
    top: calc(50% - 5px);
    margin-left: 0;
    margin-right: 0
}

.tooltip[aria-hidden=true] {
    visibility: hidden;
    opacity: 0
}

.tooltip[aria-hidden=true]:not(.no-transition) {
    -webkit-transition: opacity .15s, visibility .15s;
    transition: opacity .15s, visibility .15s
}

.tooltip[aria-hidden=false] {
    visibility: visible;
    opacity: 1
}

.tooltip[aria-hidden=false]:not(.no-transition) {
    -webkit-transition: opacity .15s;
    transition: opacity .15s
}

.tooltip.info .tooltip-inner {
    background: rgba(0, 68, 153, .9);
    color: #fff;
    padding: 24px;
    border-radius: 5px;
    -webkit-box-shadow: 0 5px 30px rgba(0, 0, 0, .1);
    box-shadow: 0 5px 30px rgba(0, 0, 0, .1);
    max-width: 250px
}

.tooltip.info .tooltip-arrow {
    border-color: rgba(0, 68, 153, .9)
}

.tooltip.popover .popover-inner {
    background: #f9f9f9;
    color: #000;
    padding: 24px;
    border-radius: 5px;
    -webkit-box-shadow: 0 5px 30px rgba(0, 0, 0, .1);
    box-shadow: 0 5px 30px rgba(0, 0, 0, .1);
    width: 320px;
}

.tooltip.popover .popover-arrow {
    border-color: #f9f9f9
}

.tooltip.tooltip-loading .tooltip-inner {
    color: #7af
}

.v-popover.inline {
    display: inline-block
}

.v-popover.inline:not(:last-child) {
    margin-right: 12px


.tooltip[x-placement^="top"] {
  margin-bottom: 5px;
}
.tooltip[x-placement^="top"] .tooltip-arrow {
  border-width: 5px 5px 0 5px;
  border-left-color: transparent !important;
  border-right-color: transparent !important;
  border-bottom-color: transparent !important;
  bottom: -5px;
  left: calc(50% - 5px);
  margin-top: 0;
  margin-bottom: 0;
}
.tooltip[x-placement^="bottom"] {
  margin-top: 5px;
}
.tooltip[x-placement^="bottom"] .tooltip-arrow {
  border-width: 0 5px 5px 5px;
  border-left-color: transparent !important;
  border-right-color: transparent !important;
  border-top-color: transparent !important;
  top: -5px;
  left: calc(50% - 5px);
  margin-top: 0;
  margin-bottom: 0;
}
.tooltip[x-placement^="right"] {
  margin-left: 5px;
}
.tooltip[x-placement^="right"] .tooltip-arrow {
  border-width: 5px 5px 5px 0;
  border-left-color: transparent !important;
  border-top-color: transparent !important;
  border-bottom-color: transparent !important;
  left: -5px;
  top: calc(50% - 5px);
  margin-left: 0;
  margin-right: 0;
}
.tooltip[x-placement^="left"] {
  margin-right: 5px;
}
.tooltip[x-placement^="left"] .tooltip-arrow {
  border-width: 5px 0 5px 5px;
  border-top-color: transparent !important;
  border-right-color: transparent !important;
  border-bottom-color: transparent !important;
  right: -5px;
  top: calc(50% - 5px);
  margin-left: 0;
  margin-right: 0;
}
.tooltip.popover .popover-inner {
  background: #f9f9f9;
  color: black;
  padding: 24px;
  border-radius: 5px;
  box-shadow: 0 5px 30px rgba(black, .1);
}
.tooltip.popover .popover-arrow {
  border-color: #f9f9f9;
}
.tooltip[aria-hidden='true'] {
  visibility: hidden;
  opacity: 0;
  transition: opacity .15s, visibility .15s;
}
.tooltip[aria-hidden='false'] {
  visibility: visible;
  opacity: 1;
  transition: opacity .15s;
}
</style>
@endsection

@section('content')
<div id="wrapper" class="h-auto">
  <div class="ml-auto">
    <header class="p-4 flex flex-wrap">
      <div class="flex w-full sm:w-1/2 md:w-1/3 lg:w-1/3 xl:w-1/3">

      </div>
      <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/3 xl:w-1/3">
        <h3 class="text-center text-3xl font-bold">
          {{ $product->name }}
        </h3>
      </div>
      <div class="w-full sm:w-1/2 md:w-1/3 lg:w-1/3 xl:w-1/3">
        <p class="text-center md:text-right py-2">
          @if(! request()->has('hideCTA'))
          <a href="{{route('user.register.winmo',['product_id' => $product->id])}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">
            Access Full Report
          </a>
          @endif
        </p>
      </div>
    </header>
  </div>
  <hr class="border-b m-0" />
  <main class="flex flex-wrap m-px overflow-hidden">
    <div class="flex-1 ml-auto px-4 py-2 m-2">
      <div class="shadow p-2 my-2">
        <h3 class="font-medium text-center pt-4">
          Likelihood of a New Campaign
          <v-popover
            offset="16"
            class="inline"
          >
            <!-- This will be the popover target (for the events and position) -->
            <button class="tooltip-target inline bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-1 px-1 rounded inline-flex items-center">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="icon-help inline h-4"><path class="secondary" d="M12 19.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm1-5.5a1 1 0 0 1-2 0v-1.41a1 1 0 0 1 .55-.9L14 10.5C14.64 10.08 15 9.53 15 9c0-1.03-1.3-2-3-2-1.35 0-2.49.62-2.87 1.43a1 1 0 0 1-1.8-.86C8.05 6.01 9.92 5 12 5c2.7 0 5 1.72 5 4 0 1.3-.76 2.46-2.05 3.24L13 13.2V14z"/></svg>
            </button>

            <!-- This will be the content of the popover -->
            <template slot="popover">
              <p>
                Likelihood of a new Long Form or Short Form creative being introduced in the next 30, 60 or 90 days based on brands history. A readout of zero indicates insufficient data to predict or a brand has stalled television ad campaigns.
              </p>
            </template>
          </v-popover>
        </h3>
          <predictor :product="{{$product->toJson()}}" :days="{{$predictorDays}}"></predictor>
      </div>
      <div class="shadow p-2">
        <h3 class="font-medium text-center pt-4">
          Network TV Coverage
          <v-popover
            offset="16"
            class="inline"
          >
            <!-- This will be the popover target (for the events and position) -->
            <button class="tooltip-target inline bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-1 px-1 rounded inline-flex items-center">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="icon-help inline h-4"><path class="secondary" d="M12 19.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm1-5.5a1 1 0 0 1-2 0v-1.41a1 1 0 0 1 .55-.9L14 10.5C14.64 10.08 15 9.53 15 9c0-1.03-1.3-2-3-2-1.35 0-2.49.62-2.87 1.43a1 1 0 0 1-1.8-.86C8.05 6.01 9.92 5 12 5c2.7 0 5 1.72 5 4 0 1.3-.76 2.46-2.05 3.24L13 13.2V14z"/></svg>
            </button>

            <!-- This will be the content of the popover -->
            <template slot="popover">
              <p>
                Shows variance in the number of networks a brand is airing on compared to that brands' subcategory (as a whole), in the past 60 days.
              </p>
            </template>
          </v-popover>
        </h3>
        <coverage :coverage={{$coverage*100}}></coverage>
        <div class="flex justify-between">
          <div class="w-1/3 text-center">
            Highly Concentrated
          </div>
          <div class="w-1/3 text-center">
            Wide Dispersion
          </div>
        </div>
      </div>
    </div>
    <div class="flex-auto ml-auto px-4 py-2 m-2">
      <div class="shadow p-2 my-2 flex text-center">
        <div class="flex-1">
          <h3 class="font-medium text-lg py-2">
            DRTV Presence Format
            <v-popover
              offset="16"
              class="inline"
            >
              <!-- This will be the popover target (for the events and position) -->
              <button class="tooltip-target inline bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-1 px-1 rounded inline-flex items-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="icon-help inline h-4"><path class="secondary" d="M12 19.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm1-5.5a1 1 0 0 1-2 0v-1.41a1 1 0 0 1 .55-.9L14 10.5C14.64 10.08 15 9.53 15 9c0-1.03-1.3-2-3-2-1.35 0-2.49.62-2.87 1.43a1 1 0 0 1-1.8-.86C8.05 6.01 9.92 5 12 5c2.7 0 5 1.72 5 4 0 1.3-.76 2.46-2.05 3.24L13 13.2V14z"/></svg>
              </button>

              <!-- This will be the content of the popover -->
              <template slot="popover">
                <p>
                  Indicates if brand has ever run DRTV ads in:
                  <ul class="list-disc list-inside">
                    <li>Long Form: (half hour) length</li>
                    <li>Short Form: (15 second through 5 minute) lengths</li>
                  </ul>
                </p>
              </template>
            </v-popover>
          </h3>
        </div>
        <div class="flex-1">
          <p class="text-left text-lg py-2">
            Long Form: {!! $product->last_lf_airing ? "<span class='text-green-700'>Yes</span>" : "<span class='text-red-700'>No</span>" !!}
            &nbsp;
            Short Form: {!! $product->last_sf_airing ? "<span class='text-green-700'>Yes</span>" : "<span class='text-red-700'>No</span>" !!}
          </p>
        </div>
      </div>
        @if($airtimesByNetwork->count() > 0)
      <div class="shadow p-2 my-2">
        <h3 class="font-medium text-center py-2">
          Top TV Networks
            <v-popover
              offset="16"
              class="inline"
            >
              <!-- This will be the popover target (for the events and position) -->
              <button class="tooltip-target inline bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-1 px-1 rounded inline-flex items-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="icon-help inline h-4"><path class="secondary" d="M12 19.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm1-5.5a1 1 0 0 1-2 0v-1.41a1 1 0 0 1 .55-.9L14 10.5C14.64 10.08 15 9.53 15 9c0-1.03-1.3-2-3-2-1.35 0-2.49.62-2.87 1.43a1 1 0 0 1-1.8-.86C8.05 6.01 9.92 5 12 5c2.7 0 5 1.72 5 4 0 1.3-.76 2.46-2.05 3.24L13 13.2V14z"/></svg>
              </button>

              <!-- This will be the content of the popover -->
              <template slot="popover">
                <p>
                  Top 5 networks this brand aired on.
                </p>
              </template>
            </v-popover>
          </h3>
          <div class="flex">
            @foreach($airtimesByNetwork as $network)
                @if (Storage::disk('s3-logos')->exists($network->logo_location))
                <div class="flex-1 p-2">
                    <img class="max-w-xs mx-auto h-8" src="{{Storage::disk('s3-logos')->url($network->logo_location)}}">
                    <p class="text-center">{{$network->abbr}}</p>
                </div>
                @else
                <div class="flex-1 p-2">{{$network->abbr}}</div>
                @endif
            @endforeach
          </ul>
        </div>
      </div>
        @endif
        @if($mediaSpend->count() > 0)
      <div class="shadow p-2">
        <h3 class="font-medium text-center pt-4">
           Daily DRTV Media Spend (previous 60 Days)
          <v-popover
            offset="16"
            class="inline"
          >
            <!-- This will be the popover target (for the events and position) -->
            <button class="tooltip-target inline bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-1 px-1 rounded inline-flex items-center">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" class="icon-help inline h-4"><path class="secondary" d="M12 19.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm1-5.5a1 1 0 0 1-2 0v-1.41a1 1 0 0 1 .55-.9L14 10.5C14.64 10.08 15 9.53 15 9c0-1.03-1.3-2-3-2-1.35 0-2.49.62-2.87 1.43a1 1 0 0 1-1.8-.86C8.05 6.01 9.92 5 12 5c2.7 0 5 1.72 5 4 0 1.3-.76 2.46-2.05 3.24L13 13.2V14z"/></svg>
            </button>

            <!-- This will be the content of the popover -->
            <template slot="popover">
              <p>
                Average category spend line represents the average media dollars spent per brand in this brands' category. The spend line represents this specific brands’ media dollars spent. If the brand line goes above the average category spend line this brand is outpacing the average spend per brand.
              </p>
            </template>
          </v-popover>
        </h3>
        <media-spend :media-spend="{{$mediaSpend->toJson()}}" :product="{{$product->toJson()}}"></media-spend>
      </div>
        @endif
      <div class="p-2 text-right">
        @if(! request()->has('hideCTA'))
        <a href="{{route('user.register.winmo',['product_id' => $product->id])}}" target="_blank">
          Access Complete Profile: <img src="{!! asset('assets/img/logo.png') !!}" class="inline-block" />
        </a>
        @endif
      </div>
    </div>
  </main>
</div>
@endsection
