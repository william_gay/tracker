<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />
    <style>
    h3 {
        line-height: 26px;
    }
    table.table {
        margin-bottom: 5px;
    }
    table th {
        font-size: 12px;
        padding: 2px !important;
        text-align: left;
        border-bottom: 1px solid #000;
    }
    table td {
        font-size: 12px;
        padding: 2px !important;
        line-height: 15px !important;
    }
    table tfoot tr {
        border-bottom: 1px solid #000;
    }
    .bigger {
        font-size: 12px;
    }
    </style>
</head>
<body>
    <div id="content">
        <table style="width: 100%">
            <tr>
                <td style="width:50%;">
                    <table>
                        <tr>
                            <td><img src="{!! $absolute !!}assets/img/logo.png" /></td>
                            <td><h3>Top 5 Vertical Markets</h3></td>
                        </tr>
                    </table>
                </td>
                <td style="width:50%;">
                <table>
                    <tr>
                        <td><h4 class="text-right">National Cable Spot Rankings {!! $month !!}</h4></td>
                    </tr>
                    <tr>
                        <td><p class="bigger text-right"><img src="{!! $absolute !!}assets/img/logo.png" style="width:20px;" /> Logo indicates new offer, price, version, or marketer.</p></td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        @foreach($categories as $category)
        <div class="row-fluid">
            <div class="span12">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="span3">Category</th>
                            <th class="span1">Rank</th>
                            <th class="span3">Product Name</th>
                            <th class="span3">Marketing Company</th>
                            <th class="span1">Price</th>
                            <th class="span1">S/H</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $rank = 1; ?>
                        @foreach($category['spots'] as $spot)
                            <tr>
                                <td>{!! $spot->category_name !!}</td>
                                <td>{!! $rank !!}</td>
                                <td>{!! $spot->title !!}</td>
                                <td>
                                @if($spot->marketing_company)
                                    {!! $spot->marketing_company !!}
                                @endif
                                </td>
                                <td>
                                    @if($spot->price)
                                        ${!! $spot->price !!}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>
                                @if($spot->shipping_cost)
                                    ${!! $spot->shipping_cost !!}
                                @else
                                    N/A
                                @endif
                                </td>
                            </tr>
                            <?php $rank++; ?>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        @endforeach
        <div class="row-fluid">
            <table>
                <tr>
                    <td style="width:65px;"><img src="{!! $absolute !!}assets/img/logo.png" /></td>
                    <td style="width:400px;">
                        <ul class="unstyled bigger">
                            <li>Media Analytics</li>
                            <li>16 West Chestnut Street &bull; West Chester PA 19380</li>
                            <li>610-200-6041 &bull; www.imsreport.com</li>
                        </ul>
                    </td>
                    <td style="width: 500px;font-size: 8px;">
                    &copy; {!! date('Y') !!} Media Analytics, LLC. All right reserved. No part of this publication may be reproduced or transmitted in any form electronic or mechanical, including photocopy, or any information storage and retrieval system without permission in writing from IMS. IMS is not responsible for any loss due to errors or omissions in information provided, or for any loss of service due to technical or other difficulties. Any publication, or public display, or other use, without prior written permission of IMS is prohibited.
                    </td>
                    <td style="width:50px;text-align:right;vertical-align:bottom;" class="bigger"><strong>{!! $page_number !!}</strong></td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
