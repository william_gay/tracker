<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />
    <style>
    h3 {
        line-height: 26px;
    }
    table.table {
        margin-bottom: 5px;
    }
    table th {
        font-size: 11px;
        padding: 2px !important;
        text-align: left;
        border-bottom: 1px solid #000;
    }
    table td {
        font-size: 10px;
        padding: 2px !important;
        line-height: 14px !important;
    }
    .bigger {
        font-size: 12px;
    }
    </style>
</head>
<body>
    <div id="content">
        <table>
            <tr>
                <td><img src="{!! $absolute !!}assets/img/logo.png" /></td>
                <td><h3>National Cable DR Spot Ranking</h3></td>
            </tr>
            <tr>
                <td colspan="2"><p class="bigger">The 50 most frequently seen direct response spots on networks monitored, ranked for the week ending: <strong>{!! $weekEnding !!}</strong></p></td>
            </tr>
        </table>
        <div class="row-fluid" style="height: 550px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:49%;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Product Name</th>
                                    <th>Marketing Company</th>
                                    <th>Price</th>
                                    <th>Category</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($rank25 as $rank)
                            <tr>
                                <td>{!! $count !!}</td>
                                <td>
                                @if(strlen($rank->title) > 25)
                                    <font style="font-size:8px;">{!! $rank->title !!}</font>
                                @else
                                    {!! $rank->title !!}
                                @endif
                                </td>
                                <td>
                                @if(strlen($rank->marketing_company) > 25)
                                    <font style="font-size:8px;">{!! $rank->marketing_company !!}</font>
                                @else
                                    {!! $rank->marketing_company !!}
                                @endif
                                </td>
                                <td>${!! $rank->price !!}</td>
                                <td>
                                @if(strlen($rank->category) > 20)
                                    <font style="font-size:8px;">{!! $rank->category !!}</font>
                                @else
                                    {!! $rank->category !!}
                                @endif
                                </td>
                            </tr>
                            <?php $count++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                    <td style="width:49%;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Product Name</th>
                                    <th>Marketing Company</th>
                                    <th>Price</th>
                                    <th>Category</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($rank50 as $rank)
                            <tr>
                                <td>{!! $count !!}</td>
                                <td>
                                @if(strlen($rank->title) > 20)
                                    <font style="font-size:8px;">{!! $rank->title !!}</font>
                                @else
                                    {!! $rank->title !!}
                                @endif
                                </td>
                                <td>
                                @if(strlen($rank->marketing_company) > 20)
                                    <font style="font-size:8px;">{!! $rank->marketing_company !!}</font>
                                @else
                                    {!! $rank->marketing_company !!}
                                @endif
                                </td>
                                <td>${!! $rank->price !!}</td>
                                <td>
                                @if(strlen($rank->category) > 20)
                                    <font style="font-size:8px;">{!! $rank->category !!}</font>
                                @else
                                    {!! $rank->category !!}
                                @endif
                                </td>
                            </tr>
                            <?php $count++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="row-fluid">
            <table>
                <tr>
                    <td style="width:65px;"><img src="{!! $absolute !!}assets/img/logo.png" /></td>
                    <td style="width:400px;">
                        <ul class="unstyled bigger">
                            <li>Media Analytics</li>
                            <li>16 West Chestnut Street &bull; West Chester PA 19380</li>
                            <li>610-200-6041 &bull; www.imsreport.com</li>
                        </ul>
                    </td>
                    <td style="width: 500px;font-size: 8px;">
                    &copy; {!! date('Y') !!} Media Analytics, LLC. All right reserved. No part of this publication may be reproduced or transmitted in any form electronic or mechanical, including photocopy, or any information storage and retrieval system without permission in writing from IMS. IMS is not responsible for any loss due to errors or omissions in information provided, or for any loss of service due to technical or other difficulties. Any publication, or public display, or other use, without prior written permission of IMS is prohibited.
                    </td>
                    <td style="width:50px;text-align:right;vertical-align:bottom;" class="bigger"><strong>{!! $page_number !!}</strong></td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>
