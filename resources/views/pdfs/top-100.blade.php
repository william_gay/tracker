<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />
    <style>
    h3 {
        line-height: 26px;
    }
    table.table {
        margin-bottom: 5px;
    }
    table th {
        font-size: 10px;
        font-weight:normal !important;
        padding: 2px !important;
        text-align: left;
        border-bottom: 1px solid #000;
    }
    table td {
        font-size: 10px;
        padding: 2px !important;
        line-height: 14px !important;
    }
    .bigger {
        font-size: 12px;
    }
    </style>
</head>
<body>
    <div id="content">
        <table style="width:100%;">
            <tr>
                <td><img src="{!! $absolute !!}assets/img/logo.png" /></td>
                <td><h3 class="text-left">Top 100 National Cable DR Spot Ranking</h3></td>
                <td><h5 class="text-right">{!! $month !!} {!! $year !!}</h5></td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td><p class="bigger">The 100 most frequently seen direct response spots on networks monitored, ranked for the month.</p></td>
            </tr>
        </table>
        <div class="row-fluid" style="height: 550px;">
            <table style="width:100%;">
                <tr>
                    <td style="width:24%;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="30">Rank</th>
                                    <th>Product Name</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($spots25 as $rank)
                            <tr>
                                <td>{!! $count !!}</td>
                                <td>
                                @if(strlen($rank->title) > 20)
                                    <font style="font-size:8px;"><strong>{!! $rank->title !!}</strong></font>
                                @else
                                    <strong>{!! $rank->title !!}</strong>
                                @endif
                                </td>
                            </tr>
                            <?php $count++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                    <td style="width:24%;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="30">Rank</th>
                                    <th>Product Name</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($spots50 as $rank)
                            <tr>
                                <td>{!! $count !!}</td>
                                <td>
                                @if(strlen($rank->title) > 20)
                                    <font style="font-size:8px;"><strong>{!! $rank->title !!}</strong></font>
                                @else
                                    <strong>{!! $rank->title !!}</strong>
                                @endif
                                </td>
                            </tr>
                            <?php $count++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                    <td style="width:24%;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="30">Rank</th>
                                    <th>Product Name</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($spots75 as $rank)
                            <tr>
                                <td>{!! $count !!}</td>
                                <td>
                                @if(strlen($rank->title) > 20)
                                    <font style="font-size:8px;"><strong>{!! $rank->title !!}</strong></font>
                                @else
                                    <strong>{!! $rank->title !!}</strong>
                                @endif
                                </td>
                            </tr>
                            <?php $count++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                    <td style="width:24%;">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="30">Rank</th>
                                    <th>Product Name</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($spots100 as $rank)
                            <tr>
                                <td>{!! $count !!}</td>
                                <td>
                                @if(strlen($rank->title) > 20)
                                    <font style="font-size:8px;"><strong>{!! $rank->title !!}</strong></font>
                                @else
                                    <strong>{!! $rank->title !!}</strong>
                                @endif
                                </td>
                            </tr>
                            <?php $count++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="row-fluid">
            <table>
                <tr>
                    <td style="width:65px;"><img src="{!! $absolute !!}assets/img/logo.png" /></td>
                    <td style="width:400px;">
                        <ul class="unstyled bigger">
                            <li>Media Analytics</li>
                            <li>10 North Church Street &bull; West Chester PA 19380</li>
                            <li>610-328-6000 &bull; www.imsreport.com</li>
                        </ul>
                    </td>
                    <td style="width: 500px;font-size: 8px;">
                    &copy; {!! date('Y') !!} Media Analytics, LLC. All right reserved. No part of this publication may be reproduced or transmitted in any form electronic or mechanical, including photocopy, or any information storage and retrieval system without permission in writing from IMS. IMS is not responsible for any loss due to errors or omissions in information provided, or for any loss of service due to technical or other difficulties. Any publication, or public display, or other use, without prior written permission of IMS is prohibited.
                    </td>
                    <td style="width:50px;text-align:right;vertical-align:bottom;" class="bigger"><strong>{!! $page_number !!}</strong></td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>