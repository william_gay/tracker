<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />
    <style>
    h3 {
        line-height: 26px;
    }
    table.table {
        margin-bottom: 5px;
    }
    table th {
        font-size: 12px;
        padding: 2px !important;
        text-align: left;
        border-bottom: 1px solid #000;
    }
    table td {
        font-size: 12px;
        padding: 2px !important;
        line-height: 15px !important;
    }
    table tfoot tr {
        border-bottom: 1px solid #000;
    }
    .bigger {
        font-size: 12px;
    }
    </style>
</head>
<body>
@if(isset($count))
    <h5>There are {!! $count !!} new spots this week.</h5>
    <ul>
    <?php
        $skip = 0;
        $take = $perpage;
        $letter = 'a';
    ?>
    @for($i = 1; $i <= $pages; $i++)
        <li>{!! link_to_route('admin.spots.pdf', 'Page '.$i, array('report_type' => $report_type, 'weekEnding' => $weekEnding, 'page_number' => $page_number.$letter, 'skip' => $skip,'take' =>$take)) !!}</li>
        <?php
            $skip += $perpage;
            $letter++;
        ?>
    @endfor
    </ul>
@else
    <div id="content">
    <table style="width: 100%">
        <tr>
            <td style="width:50%;">
                <table>
                    <tr>
                        <td><img src="{!! $absolute !!}assets/img/logo.png" /></td>
                        <td><h3>Direct Response Spots</h3></td>
                    </tr>
                    <tr>
                        <td colspan="2"><p class="bigger">New spots from networks monitored, listed alphabetically.</p></td>
                    </tr>
                </table>
            </td>
            <td style="width:50%;">
            <table>
                <tr>
                    <td><h4 class="text-right">New for the Week Ending {!! $weekEnding !!}</h4></td>
                </tr>
                <tr>
                    <td><p class="bigger text-right"><img src="{!! $absolute !!}assets/img/logo.png" style="width:20px;" /> Logo indicates new offer, price, version, or marketer.</p></td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
        <div class="row-fluid" style="height: 550px;">
            <div class="span12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Product Name</th>
                            <th>Marketing Company</th>
                            <th>Description</th>
                            <th></th>
                            <th>TRT</th>
                            <th>Price</th>
                            <th>S/H</th>
                            <th>Phone</th>
                            <th>Category</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($spots as $spot)
                            <tr>
                                <td>{!! $spot->title !!}</td>
                                <td>
                                @if($spot->marketingCompany)
                                    {!! $spot->marketingCompany->name !!}
                                @endif
                                </td>
                                <td>
                                @if(strlen($spot->description) > 25)
                                    <font style="font-size:8px;">{!! $spot->description !!}</font>
                                @else
                                    {!! $spot->description !!}
                                @endif
                                </td>
                                <td>
                                    @if($spot->version > 1)
                                        <img src="{!! $absolute !!}assets/img/logo.png" style="width:20px;" />
                                    @endif
                                </td>
                                <td>:{!! $spot->length !!}</td>
                                <td>
                                    @if($spot->price != 0)
                                        ${!! $spot->price !!}
                                    @else
                                        N/A
                                    @endif
                                </td>
                                <td>
                                @if($spot->shipping_cost != 0)
                                    ${!! $spot->shipping_cost !!}
                                @else
                                    N/A
                                @endif
                                </td>
                                <td>
                                @if($spot->phone != '')
                                    {!! $spot->phone !!}
                                @else
                                    N/A
                                @endif
                                </td>
                                <td>
                                    @if($spot->category)
                                        {!! $spot->category->name !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row-fluid">
            <table>
                <tr>
                    <td style="width:65px;"><img src="{!! $absolute !!}assets/img/logo.png" /></td>
                    <td style="width:400px;">
                        <ul class="unstyled bigger">
                            <li>Media Analytics</li>
                            <li>16 West Chestnut Street &bull; West Chester PA 19380</li>
                            <li>610-200-6041 &bull; www.imsreport.com</li>
                        </ul>
                    </td>
                    <td style="width: 500px;font-size: 8px;">
                    &copy; {!! date('Y') !!} Media Analytics, LLC. All right reserved. No part of this publication may be reproduced or transmitted in any form electronic or mechanical, including photocopy, or any information storage and retrieval system without permission in writing from IMS. IMS is not responsible for any loss due to errors or omissions in information provided, or for any loss of service due to technical or other difficulties. Any publication, or public display, or other use, without prior written permission of IMS is prohibited.
                    </td>
                    <td style="width:50px;text-align:right;vertical-align:bottom;" class="bigger"><strong>{!! $page_number !!}</strong></td>
                </tr>
            </table>
        </div>
    </div>
@endif
</body>
</html>
