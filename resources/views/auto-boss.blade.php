<!DOCTYPE html>
<!--[if lt IE 7]>       <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>          <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>          <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->  <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Auto - BOSS</title>
    <meta name="description" content="{!! Config::get('app.site_config.description') !!}">
    <meta name="viewport" content="width=device-width">

    <link rel="shortcut icon" href="/favicon.png">
    <link rel="apple-touch-icon" href="/assets/img/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/img/apple-touch-icon-114x114-precomposed.png">

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="b3959fd0454692f587b9546eedf396bd"></script>
</head>
<body>
    <!-- #wrap -->
    <div id="wrap">
        <!-- #content -->
        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Type</th>
                                    <th>Detections</th>
                                    <th>Channels</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($shows as $show)
                                @if($show->day == Carbon\Carbon::now()->format('Y-m-d'))
                                    <tr class="success">
                                @else
                                    <tr>
                                @endif
                                    <td>{!! $show->day !!}</td>
                                    <td>Shows</td>
                                    <td>{!! $show->count !!}</td>
                                    <td>{!! $show->channels !!}</td>
                                </tr>
                                @endforeach
                                @foreach($spots as $spot)
                                @if($spot->day == Carbon\Carbon::now()->format('Y-m-d'))
                                    <tr class="success">
                                @else
                                    <tr>
                                @endif
                                    <td>{!! $spot->day !!}</td>
                                    <td>Spots</td>
                                    <td>{!! $spot->count !!}</td>
                                    <td>{!! $spot->channels !!}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.outer -->
        </div>
        <!-- /#content -->
        <!-- #push do not remove -->
        <div id="push"></div>
        <!-- /#push -->
    </div>
    <!-- /#wrap -->

    <div class="clearfix"></div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="//ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-41151800-3', 'imsreport.com');
      ga('send', 'pageview');
    </script>
</body>
</html>
