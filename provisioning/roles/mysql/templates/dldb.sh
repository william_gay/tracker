#!/bin/bash

PASSWORD=pgF8BGb1CKMQXdOh
HOST=ims-aurora-cluster.cluster-cwbzkngv7yjk.us-east-1.rds.amazonaws.com
USER=ims
DATABASE=ims

NOW=$(date +"%Y-%m-%d")
DB_FILE="$DATABASE-$NOW.sql"

EXCLUDED_TABLES=(
airtimes
spot_airtimes
live_shopping_products
live_shopping_airtimes
live_shopping_product_versions
site_analytics
retailer_products
spot_detections_channels
user_hashes
users_logins
clipsters
)

IGNORED_TABLES_STRING=''
for TABLE in "${EXCLUDED_TABLES[@]}"
do :
   IGNORED_TABLES_STRING+=" --ignore-table=${DATABASE}.${TABLE}"
done

echo "Dump structure"
mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} --single-transaction --no-data ${DATABASE} > ${DB_FILE}

echo "Dump content"
mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} ${DATABASE} --single-transaction --quick --no-create-info ${IGNORED_TABLES_STRING} >> ${DB_FILE}

# echo "Dump Airtimes"
mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} ${DATABASE} airtimes --where='id>4000000' >> ${DB_FILE}

# echo "Dump SpotAirtimes"
mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} ${DATABASE} spot_airtimes --where='id>25000000' >> ${DB_FILE}

# echo "Dump live_shopping_airtimes"
mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} ${DATABASE} live_shopping_airtimes --where='id>500000' >> ${DB_FILE}
