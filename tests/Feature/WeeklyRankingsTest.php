<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Carbon\Carbon;

class WeeklyRankingsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRuntime()
    {
        $saturdayAt945 = Carbon::now()->next(Carbon::SATURDAY)->hour(6)->minute(0)->second(0);
        Carbon::setTestNow($saturdayAt945);

        $this->artisan('rankings:run')
            ->assertExitCode(0);

    }
}
